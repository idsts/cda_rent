﻿import os, sys

import setuptools
from setuptools.command.install import install

module_path = os.path.join(os.path.dirname(__file__), 'cda_rent/__init__.py')
version_line = [line for line in open(module_path, encoding="utf-8-sig")
                if line.startswith('__version__')][0]

__version__ = version_line.split('__version__ = ')[-1][1:][:-2]
            
class custom_install(install):
    def run(self):
        self.install_bitbucket_dependencies()
        print("This is a custom installation")
        install.run(self)
        
    def install_bitbucket_dependencies(self):
        if sys.version_info.major == 2:
            #os.system("pip install git+https://bitbucket.org/")
            pass
        elif sys.version_info.major == 3:
            #os.system("pip3 install git+https://bitbucket.org/")
            pass

setuptools.setup(
    name="CDA Rent",
    version=__version__,
    url="https://bitbucket.org/idsts/cda_rent",

    author="Tim Gilbert",

    description="CDA Rent Predictor",
    long_description=open('README.md').read(),

    packages = setuptools.find_packages(),
    package_data={'': ["*.pyx", "*.py", "*.txt", "*.csv", "*.xz", "*.pkl", "data/*"]},
    py_modules=['cda_rent'],
    zip_safe=False,
    platforms='any',

    install_requires=[
        "cython",
        "numpy",
        "pandas",
        "scipy",
        "scikit-learn==0.23.2",
        "joblib==1.0.1",
        "tensorflow==2.5",
        "keras==2.4.3",

        "requests",
        "flask",
        "flask_restful",
        "flask_cors",
        "gunicorn",
        
        "pymysql",
        "pymongo",
        "sshtunnel",
        "usaddress",
        "googlemaps",
    ],
    cmdclass={'install': custom_install}
)


