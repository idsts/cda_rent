﻿import re

e_INTERIOR_FEATURES_SEARCHES = {
    "feature_floorplan_open": r"(open[- ]?(floor[- ]?)?plan|(floor[- ]?)?plan[- ]*open)",
    "feature_floorplan_traditional": r"(trad(itional|\.)?[- ]?(floor[- ]?)?plan|(floor[- ]?)?plan[- ]*trad(itional|\.)?)",
    "feature_floorplan_inlaw": r"((in[- ]?law|maid|guest)('s)?[- ]*((floor[- ]?)?plan|quarters|suite|apt|apartment)|(additional|addl)[- ]*living[- ]*(area|quarters))",
    "feature_floorplan_split": r"(split[- ]*(bed(room)?|br)?[ -]*(floor[- ]?)?plan|(floor[- ]?)?plan[- ]*split)",
    "feature_floorplan_roommate": r"(room[- ]?mate[- ]?(floor[- ]?)?plan|(floor[- ]?)?plan[- ]*room[- ]?mate)",
    
    "feature_kitchenette": r"(kitchenette|efficiency[- ]kitchen)",
    "feature_kitchen": r"kitchen(?!=ette)",
    "feature_kitchen_dining_combo": r"(eat[- ]?in kitchen|dining (combo|combination)|(combo|combination|comb.) (dining|kitchen)"
        r"|eating[- ]*(space|area)[- ]kitchen|kitchen[- /]*dining|dining[- /]*kitchen|liv[- /]din comb)",
    "feature_dining_room": r"(dining|(separated?|two|2|double)[- ]?eating[- ]?(area|room|nook)|^eating[- ]?(area|room|nook)$)",
    "feature_breakfast_area": r"breakfast[- ](area|room|nook|bar)",
    "feature_living_room": r"((living|family)[- ]*(room|area)|great[- ]*room|room[- /]*great)",
    "feature_foyer": r"(foyer|formal entry|^entry$|lobby)",
    "feature_office": r"(office|den|study|library|work[- ]?(space|room|area)|computer[- ]*(space|room|area))",
    "feature_stairs": r"(stairs|steps|stairway|stairwell|staircase)",
    "feature_no_common_walls": r"(no[- ]common[- ]wall|detached)",
    "feature_furnished": r"(?<!un)furnished",
    "feature_stereo": r"(stereo|sound[- ]?system)",
    
    "feature_penthouse": r"penthouse",
    "feature_loft": r"loft",
    "feature_balcony": r"balcony",
    
    "feature_bedroom_main_floor": r"((main|entry|1st|first|ground)[- ]*(on[- ]*)?(floor|level|fl\.?)[- ]*(bed(room)?|bdrm|master|mstr|mbr|br|br/ba|primary)s?|((bed(room)?|bdrm|master|mstr|mbr|br|br/ba|primary)s?[- ]*)(on[- ]*)?(down(stairs?)?|1st|entry|ground|main))",
    "feature_bedroom_master": r"((master|mstr|main|primary)[- ]*(bed(room)?|bdrm|suite)s?)",
    "feature_bedroom_split": r"(split[- ]*(bed(room)?|bdrm)s?|(bed(room)?|bdrm)[- ]*split)",
    "feature_murphy_bed": r"murphy[- ]*bed",
    
    "feature_high_ceilings": r"(ceil(ing)?s?[- ]*(9|10|11|12)\+? *('|ft|foot|feet|m\.|meter) *\+?"
        r"|(9|10|11|12)\+?[- ]*('|’|ft|foot|feet|m\.|meter)?[- ]*\+?([- ]high|[- ]flat|[- ]smooth)*[- ]*ceil(ing)?s?"
        r"|(2|two)[- ]*story[- ]*ceil(ing)?s?|(high|volume)[- ]*ceil(ing)?s?"
        r")",
    "feature_ceilings_vaulted": r"((vault(ed)?|cathedral|ctdrl)[- ]*ceil(ing)?s?|ceil(ing)?s?[- ]*(vault(ed)?|cathedral|ctdrl)|cathedral[- /]*vault(ed)?|vault(ed)?[- /]*cathedral)",
    "feature_ceilings_beamed": r"(beam(ed)?[- ]*ceil(ing)?s?|(exposed|visible|open)[- ]*beams)",
    "feature_ceilings_tray": r"(tr[ea]y[- ]*ceil(ing)?s?|ceil(ing)?s?[- ]*tr[ea]y|\btrey\b)",
    "feature_ceilings_coffered": r"(coffer(ed)?[- ]*ceil|ceil(ing)?s?[- ]*coffer(ed)?)",
    "feature_ceilings_smooth": r"(smooth[- ]*ceil(ing)?s?|ceil(ing)?s?[- ]*smooth)",
    
    "feature_hvac": r"(central[- ]*air|thermostat|hvac|central[- ]*heating|central[- ]*cooling|air[- ]*filter[- ]*system|central([- */](air|heat(ing)?|cool(ing)?))+)",
    "feature_heating": r"(central[- ]*heating|furnace|wood[- ]*stove|wood[- ]*burning[- ]*stove|central([- */](air|heat(ing)?))+)",
    "feature_ac": r"(air[- ]*conditioner|a\.c\.|air[- ]*conditioning|central[- ]*cooling)",
    "feature_ceiling_fan": "ceil(ing|\.)[- ]*fan",
    "feature_fireplace": r"(fireplace|(gas|electric)[- ]*logs|hearth)",
    
    "feature_smoke_detector": r"((smoke|fire|carbon[- ]*monoxide|co)[- ]*(detect|alarm)|alrm[- ]*fire)",
    "smoking_allowed": "(non[- ]*smoking|smoke[- ]*free)",
    "feature_fire_sprinklers": r"(fire[- ]*spr(in)?kler|spr(in)?kler[- ]*system)",
    "feature_security_system": r"((security|burglar|theft)[- ]*(alarm|system|sensor))",
    
    "feature_lighting_recessed": r"(recess(ed)?[- ]*light(ing|s)|light(ing|s)[- /:()]*recess(ed)?)",
    "feature_lighting_track": r"(track(ed)?[- ]*light(ing|s)|light(ing|s)[- /:()]*track(ed)?)",
    "feature_lighting_decorative": r"(chandelier|candelabra|decorative[- ]*light(ing|s)|light(ing|s)[- /:()]*decorative)",
    "feature_lighting_skylight": r"(sky[- ]*light|solar[- ]*tube)",
    "feature_lighting_overhead": r"(over[- ]?head[- ]*light(ing|s)|light(ing|s)[- /:()]*over[- ]?head)",
    "feature_lighting_fixtures": r"(fixtures?[- ]*light(ing|s)|light(ing|s)[- /:()]*fixtures?)",
    
    "feature_windows_bay": r"bay[- ]*window",
    "feature_windows_full_height": r"(floor[- ]*to[- ]*ceiling[- ]*window|(full[- ]*height|over-?sized?)[- ]*window)",
    "feature_windows_stained": r"(stain(ed)?[- ]*glass|stain(ed)?[- ]*window)",
    "feature_windows_treatments": r"(window[- ]*(treatment|covering)?|drapes|blinds)",
    
    "feature_doors_french": r"french[- ]*doors?",
    "feature_doors_sliding_glass": r"sliding[- ]*glass[- ]*doors?",
    
    "feature_closet": r"closet",
    "feature_closet_walkin": r"(walk[- ]*in[- ]*clo|wlkincls)",
    "feature_closet_cedar": r"(cedar([- ]*wood)?[- ]*closet|closet[- ]*cedar)",
    "feature_closet_linens": r"linen[- ]*closet",
    "feature_closet_dual": r"(2\+?|two|twin|dual|double|his([-/ ]|[- ]*and[- ]*)hers|hers([-/ ]|[- ]*and[- ]*)his)closet",
    "feature_attic": r"\battic",
    "feature_storage": r"\bstorage",
    "feature_pantry": r"pantry",
    "feature_pantry_walkin": r"(walk[- ]*in[- ]pantry|pantry[- /]*walk[- ]*in)",
    "feature_book_shelves": r"book[- ]*(shel(f|ves)|cases?)",
    "feature_built_ins": r"built[- ]?in(s|[- ]?features)",
    
    "feature_counters_stone": r"((stone|granite|quartz|quartzite|marbled?|slate|onyx|limestone|soapstone)[- ]*counter([- ]?top)?s?|"
        r"counter([- ]?top)?s?[^a-z0-9]*(stone|granite|quartz|quartzite|marbled?|slate|onyx|limestone|soapstone)"
        r"|^(granite|marble|quartz)$)",
    "feature_counters_corian": r"corian",
    "feature_counters_upgraded": r"("
        r"(upgraded?|updated?|improved?|stainless|steel|concrete|recycled[- ]*glass|tile|porcelain|ceramic|solid([- ]*surface)?|non[- ]?laminated?)[- ]*counter([- ]?top)?s?"
        r"|counter([- ]?top)?s?[- /()]*([a-z]+[- .]*)?(upgraded?|updated?|improved?|stainless|steel|concrete|recycled[- ]*glass|tile|porcelain|ceramic|solid([- ]*surface)?|non[- ]?laminated?))",
    "feature_counters_laminate": r"(((?<!non[- ])laminated?|formica|wilson[- ]art)[- ]*counter([- ]?top)?s?|counter([- ]?top)?s?[- /()]*((?<!non[- ])laminated?|formica|wilson[- ]art))",
    "feature_counters_wood": r"((wood|butcher|butcher[- ]block)[- ]*counter([- ]?top)?s?|counter([- ]?top)?s?[- /()]*(wood|butcher|butcher[- ]block))",
    "feature_counters_metal": r"((metal|(stainless[- ]*)steel|stainless|copper|brass)[- ]*counter([- ]?top)?s?|counter([- ]?top)?s?[- /()]*(metal|(stainless[- ]*)steel|stainless|copper|brass))",
    
    "feature_cabinets": r"cabinets?",
    "feature_cabinets_wood": r"((hard[- ]?wood|solid[- ]wood|oak|maple|cherry|walnut)[- ]*cabinet|cabinets?[- /()]*(hard[- ]?wood|solid[- ]wood|oak|maple|cherry|walnut))",
    "feature_kitchen_island": r"((kitchen|food[- ]*prep(ping)?|cook(ing)?|quartz|granite)[- ]island|^island$|island work)",
    "feature_wet_bar": r"((?<!breakfast )bar\b|wet[- /]dry[- ]?bar|wet[- ]?bar|dry[- ]?bar)",
    "feature_wine_storage": r"wine[- ]*(rack|storage|cellar|room)",
    "feature_wine_chiller": r"wine[- /]*(beverage[- /]*)?(refrigerator|chiller|cooler)",
    
    "feature_cable_available": r"(cable[- ]tv|cable[- ](pre-?wire|available|ready|connect)|(wire[sd]?|wiring|cable[sd]?|cabling)[- ]for[- ]cable|coaxial)",
    "feature_internet": r"((hs|high[- ]speed|fiber) (internet|cable)|dsl|fiber[- ]optic|internet[- ]fiber|fiber[- ]internet|broad[- ]?band)",
    "feature_wired_data": r"((wire[sd]?|wiring|cable[sd]?|cabling)[- ]for[- ]data|ethernet|network|ntwrk|cat[- ]?(5e?|6|7)|rj[- ]?45|(high[- ]tech|data|lan)[- ]*(wire[sd]?|wiring|cable[sd]?|cabling))",
    "feature_wired_audio": r"((wire[sd]?|wiring|cable[sd]?|cabling)[- ]for[- ](sound|audio|stereo)|(sound|audio|stereo)[- ]*(wire[sd]?|wiring|cable[sd]?|cabling))",
    "feature_wired_tv": r"((wire[sd]?|wiring|cable[sd]?|cabling)[- ]for[- ](tv|flat[- ]?screen|hdmi)|(tv|flat[- ]?screen|hdmi)[- ]*(wire[sd]?|wiring|cable[sd]?|cabling))",
    "feature_wired_alarm_system": r"((wire[sd]?|wiring|cable[sd]?|cabling)[- ]for[- ](alarm|security)|(alarm|security)[- ]*(syst(em|\.)?[- ]*)?(wire[sd]?|wiring|cable[sd]?|cabling))",
    "feature_smart_home": r"(smart[- ]?home|home[- ]?automation|automated[- ]?home)",
    
    "feature_bathroom_main_floor": r"((main|entry|1st|first|ground)[- ]*(on[- ]*)?(floor|level|fl\.?)[- ]*((full|half|quarter|1/2)[- ]*)?(bath(room)?|bthrm|br/ba)s?|((bath(room)?|bthrm|br/ba)s?[- ]*)(on[- ]*)?(down(stairs?)?|1st|entry|ground|main))",
    "feature_bathroom_master": r"((master|mstr|primary bedroom|en[- ]*suite|main|primary)[- ]*bath(room)?|bath(room)?[- ]*(in|off of|off)?[- ]*(master|mstr|primary|main)|M[- ]?Bdrm w/Bat)",
    "feature_bathroom_whirlpool": r"((jet(ted)?|hydro)[- ](tub|bath)|whirlpool)",
    "feature_bathroom_shower_tub": r"(tub[- /]shower|shower[- /]tub|(combo|combination)[- ]*(shower|bathtub|tub))",
    "feature_bathroom_walkin_shower": r"((separate|stall|solo|stand[- ]?(up|alone)|walk[- ]?in)[- ]?(shower|shwr)|(shower|shwr)[- ]*only|^shower$|sep tub/shwr)",
    "feature_bathroom_soaking_tub": r"((separated?|soaking|stand[- ]?alone)[- ]?(tub|bath|bathtub)|^bathtub$|sep tub/shwr)",
    "feature_bathroom_roman_tub": r"(roman|deep)[- ]*(bath)?tub",
    "feature_bathroom_garden_tub": r"(garden|wide)[- ]*(bath)?tub",
    "feature_bathroom_dual_sink": r"((2|two|twin|dual|double)[- ]*(sink|vanity|basin|wash[- ]*basin)|(sink|basin|vanity)[- ]*(2|two|twin|dual|double))",
    "feature_bathroom_jack_jill": r"jack[- ]*('?n'?|and|&|/)[- ]*jill",
    "feature_bathroom_tile": r"((bath(room)?|shower|shwr)[- ]*tile[ds]?|tile[sd]?[- ]*(bath(room)?|shower))",
    "feature_bathroom_stone": r"((bath(room)?|shower|shwr)[- ]*(marble|granite|quartz|stone)|(marble|granite|quartz|stone)[- ]*(in[- ]*)?(bath(room)?|shower|shwr))",
    "feature_plumbing_copper": r"(copper[- ]*plumbing|plumbing[- ]*copper)",
    "feature_water_softener": r"water[- ]*softener",
    "feature_sump_pump": r"sump[- /]*pump",
    
    "feature_powder_room": r"powder[- ]*room",
    "feature_basement": r"basement",
    "feature_basement_finished": r"(finished[- ]*basement|"
        r"(basement|below[- ]*grade|sub[- ]*grade)[- ]*(bed(room)?|bdrm|master|mstr|mbr|br|br/ba|ba|bath(room)?)s?"
        r"|((bed(room)?|bdrm|master|mstr|mbr|br|br/ba|ba|bath(room)?)s?[- ]*)(in[- ]*)?(basement|below[- ]*grade|sub[- ]*grade))",
    "feature_mud_room": r"mud[- ]*room",
    "feature_utility_room": r"utility[- ]*room",
    "feature_workshop": r"work[- ]*shop",
    "feature_sun_room": r"((sun|florida|garden|patio)[- ]*room|solarium|atrium|sun[- ]*porch|winter[- ]*garden|conservatory)",
    "feature_sitting_room": r"sitting[- ]*(room|area)",
    "feature_bonus_room": r"(bonus([- /]*plus)?[- ]*room)",
    "feature_media_room": r"(media|home[- ]*theater|game|hobby)[- ]*room",
    "feature_rec_room": r"((rec(reation)?|exercise)[- ]*room|gym)",

    "feature_laundry": r"(laundry|(dryer|wash(er)?)[- ]*(hook-?up|connect)|((hook-?up|connect(ion)?)[- ]*(wash(er)?|dryer)))",
    "feature_220v": r"220[- ]?(v\b|volt|ac|a\.c\.)",
    "feature_central_vacuum": r"central[- ]?vac",
    "feature_pestguard": r"(pest[- ]?guard|in[- ]?wall[- ]?pest)",
    
    "feature_flooring_carpet": r"carpet(ed)?",
    "feature_flooring_wood": r"(((hard[- ]?)?wood|oak|cherry|maple|walnut|strip|parquet|board|plank)[- ]*floor(ing)?s?|floor(ing)?s?[- /:()]*((hard[- ]?)?wood|oak|cherry|maple|walnut|strip|parquet|board|plank))",
    "feature_flooring_stone": r"((stone|granite|limestone)[- /]*floor(ing)?s?|floor(ing)?s?[- /:()]*(stone|granite|limestone))",
    "feature_flooring_laminate": r"(laminated?[- /]*floor(ing)?s?|floor(ing)?s?[- /:()]*laminated?)",
    "feature_flooring_linoleum": r"((lino(leum)?|vinyl)[- /]*floor(ing)?s?|floor(ing)?s?[- /:()]*(lino(leum)?|vinyl)|^lino(leum)?$)",
    "feature_flooring_tile": r"(tiled?[- /]*floor(ing)?s?|floor(ing)?s?[- /:()]*tiled?)",
    
    "feature_walls_wood": r"(wood[- ]?work|paneled[- ]?wall|paneling|wood[- ]?product wall)",
    "feature_walls_brick": r"brick[- ]?wall",
    "feature_walls_block": r"(block[- ]?wall|concrete[- ]?wall)",
    "feature_walls_drywall": r"(dry[- ]?wall|gypsum|plaster|sheet[- ]*rock)",
    "feature_walls_wainscotting": r"wainscott?ing",
    "feature_walls_crown_mold": r"(crown|decorative|dec\.?)?[- ]?mou?ld(ing)?",
    
    "feature_wheelchair_accessible": r"((wheel[- ]?chair|handi[- ]?cap)[- ]?(access(ible)?|friendly)|no[- ]interior[- ](step|stair)|elevator)",
    "feature_chair_railings": r"(chair[- ]?rail(ing)?s?|rail(ing)?s?[- ]?for[- ]?chairs?)",
    
    "feature_bbq_area": r"(built[- ]?in[- ]?(bbq|barbeque|barbecue|grill)|gas (stub )?for (bbq|barbeque|grilling|barbecue)|(fire|bbq|barbeque|barbecue|grilling|grill)[- ]*(area|pit))",
    "feature_elevator": r"elevator",
    "feature_ev_charger": r"(ev|electric[- ]?vehicle|electic[- ]?car)[- ]?(charger|charging)",
    
    "feature_pool_table": r"(pool[- ]?table|billiard)",
    
    "feature_wireless_internet": r"(wi[- ]?fi|wireless)",
    "feature_parking": r"parking",
    "feature_garage": r"garage",
    
    "feature_sauna": r"sauna",
    "feature_patio_or_balcony": r"(patio|deck|porch|veranda|(outdoor|second[- ]*story|2nd[- ]*story)[- ]*balcony)",
    
    #"feature_": r"",
    #"feature_": r"",
    #"feature_": r"",
    #"pets_allowed": r"((dog|pet|animal)[- ]*(allowed|ok|friendly)|\+dog|\+pet)",
    
    #"living_area": r"(\d+(,|\. ?))*\d+[-. /]*((sq\.?|square)[- /]*(ft|foot|feet|m\.(?! plot)|meter(?! plot)|\))|SF\b)",

}
    
e_LOT_FEATURES_SEARCHES = {
    "location_downtown": r"(downtown|city[- ]lot|historic[- ](district|quarter|neighbou?rhood)|(?<!not[- ])in[- ]?city|(^|in[- ]*)city[- ]*limits?|urban)",
    "location_suburb": r"(suburb|sub[- ]?division|cul[- ]de[- ]s[ae]c|outside[- ]?city[- ]?limits)",
    "location_village": r"(village|in[- ]?town|^city[- /]*town$)",
    "location_rural": r"(secluded|forest|\bwoods\b|\bwooded|\brural|pasture|farm|agricultural|in[- ]?county|country[- ]?setting|near[- ]*town|un-?incorporated|\branch)",
    
    "location_public_transportation": r"(public[- ]transit|city[- ]transit|(near(by)?|close to|on) (bus|train|metro|subway))",
    "location_parks": r"(botanical[- ]?garden|(quiet|local|neighborhood) park|disc[- ]?golf|frisbee|volley[- ]?ball|(near(by)?|close to) park|park nearby)",
    
    "feature_patio_or_balcony": r"(patio|deck|porch|veranda|balcony)",
    "feature_gazebo": r"gazebo",
    "feature_garden_or_backyard": r"(garden|(back|rear|front)[- ]?yard|enclosed[- ]?yard|^yard$)",
    "feature_trees_fruit_nut": r"((fruit|apple|orange|pear|plum|lemon|cherry|nut|pecan|walnut)[- ]tree)",
    "feature_trees": r"(^|\b)tree[sd]?",
    
    "feature_fenced": r"fenced",
    "feature_landscaped": r"^landscap(ed|ing)$",
    "feature_landscape_gravel": r"(landscap(ed|ing).*(gravel|desert)|(gravel|desert).*landscap(e|ing)|desert[- :()]*(front|rear|back))",
    "feature_landscape_grass": r"(landscap(ed|ing).*(grass|sod|turf)|(grass|sod|turf).*landscap(e|ing)|(grass|sod|turf)[- :()]*(front|rear|back)|lawn|grass[- ]*yard)",
    "feature_landscape_synthetic": r"((synth(etic)?|artificial)[- ]*(grass|turf|sod))",
    "feature_lawn_sprinkler": "(spr(in)?kler|auto[- ]*timer[- ]*(h2o|water)|wa?te?ring Sys|(?<!flood[- ])irrigation|spr[- ]*sys)",
    "feature_low_care": r"low[- ]*(care|maintenance)",
    "feature_flower_beds": r"(flower|raised|garden)[- ]*beds",
    
    "feature_lot_shape_regular": r"(((?<!ir)regular|standard|rectangle|rectangular|square)([- ]*shaped?)?([- ]*(lot|parcel|$))|shape[- :/()](regular|standard|rectangle|rectangular|square))",
    "feature_lot_shape_irregular": r"((irregular|l|flag|pie)([- ]*shaped?)?([- ]*(lot|parcel|$))|shape[- :/()](irregular|l|flag|pie))",
    "feature_lot_subdivided": r"(?<!n[o']t)sub[- ]?divided?",
    "feature_lot_location_corner": r"(corner[- ]* lot|^corner$)",
    "feature_lot_location_interior": r"interior[- ]*lot",
    "location_common_area": r"(common[- ]*(area|gr(ou)?nd)s?|(backs[- ]*to|border(s|ing)|abut(s|ing)|adjoin(s|ing)|adjacent([- ]*to)?)[- ]*(trees|public[- ]*land|park|public[- ]*park|meadow|open[- ]*area|open[- ]*space))",
    "feature_gated_community": r"(gated?|secured?|fenced-?in|guarded|private)[- ]*(community|entry|entrance)",
    
    "feature_north_south_exposure": r"(north(ern)?[- /]*south(ern)?[- ]|south(ern)?[- /]*north(ern)?|n[- /]+s[- ]|s[- /]+n|north(ern)?|south(ern)?)[- ]*exposure",
    "feature_east_west_exposure": r"(east(ern)?[- /]*west(ern)?[- ]|west(ern)?[- /]*east(ern)?|e[- /]+w[- ]|w[- /]+e|west(ern)?|east(ern)?)[- ]*exposure",
    
    "feature_cable_available": r"(cable[- ]tv|cable[- ](pre-?wire|available|ready|connect)|(wire[sd]?|wiring|cable[sd]?|cabling)[- ]for[- ]cable|coaxial)",
    "feature_internet": r"((hs|high[- ]?speed|fiber)[- ]*(internet|cable)|dsl|fiber[- ]optic|internet[- ]fiber|fiber[- ]internet|broad[- ]?band)",
    "feature_propane": r"propane",
    "feature_rv_parking": r"(r\.?v\.?|rec(creational)[- ]?vehicle|camper)[- ]?parking",
    
    "feature_outbuildings": r"(out[- ]?buildings?|^shed$|^shop$|^barn$)",
    
    "feature_easements": r"\beasements?",
    "feature_restrictions": r"(restriction|covenant)s?",
    
    "location_dead_end": r"(dead[- ]?end|cul[- ]de[- ]s[ae]c|no[- ]?thru[- ]?(street|access|traffic)|no[- ]?outlet[- ]?street)",
    
    "location_topography_level": r"level",
    "location_topography_sloped": r"(sloped?|sloping|hilly|rugged|rolling)",
    "location_topography_wooded": r"(wooded|many[- ]?trees|^tree[sd]$)",
    "location_topography_cleared": r"(cleared|^open$|open lot|few trees)",
    "location_topography_wetland": r"(wet[- ]*land|marsh|swamp|fen|bog)",
    
    "location_sidewalks": r"sidewalks?",
    "location_street_lights": r"street[- ]?lights?",
    "location_street_curbs": r"curbs?",
    "location_street_paved": r"paved",
    "location_major_road_access": r"(major[- ]?road[- ]?access|interstate[- ]?(exit|entrance|(on|off)[- ]?ramp|access))",
    
    
    "location_waterfront": r"((water|lake|ocean|river|bay|beach|shore|gulf)'?s?[- ]?(front|side|place|location|access|home|house|edge|area)"
        r"|(boat|canoe|kayak|lake)[- ]?(dock|slip|pier|mooring|launch)|private[- ]?(beach|lake|dock|slip|pier)|(on|looking) ([a-z]+ ){,3}(lake|water|river|bay|beach)|riverbank"
        r"|(near(by)?|close to) (water|lake|ocean|river|bay|beach|shore|gulf|marina)"
        r"|^(pier|dock|jetty)$|access(ible|[- ]*to)[- ]*shoreline|shore(line)?[- ]*access)",
    "location_stream": r"(stream|creek|brook(let)?|\brill)",
    
    "location_golf": r"(golf|driving[- ]*range|fairway)",
    "location_boating": r"(boat(ing)?|(jet|water)[- ]?ski|wind[- ]?surf|sail(ing|[- ]?boat)|kayak|ski[- ]?doo|canoe|water[- ]?ski|marina|harbor|jetty)",
    
    "location_floodzone": r"(flood[- ]*(zone|insurance|plain))",
    "location_hospital": r"hospital",
    "location_airport": r"airport",
    "location_school": r"school",
    "location_house_of_worship": r"((house|place)[- ]?of[- ]?worship|church|template|synagogue|mosque)",
    
    "feature_pool": r"(w|with|private|heated|in-?ground) *pool(?! table)",
    "feature_jacuzzi": r"(jacuzzi|hot[- ]?tub|whirl[- ]?pool|jetted[- ]?pool|\bspa\b)",
    "feature_sauna": r"sauna",
    
    "location_pool": r"(pool[- ]*side|(public|community|neighbou?rhood|shared|near(by)?|near[- ]*to|close[- ]*to)[- ]*pool)",
     
    "feature_pond": r"((w|with|private)[- /]*pond|^pond$)",
    "location_shopping": r"(shopping|\bmall\b|stores|shops|boutique)",
    "location_mountain_view": r"((mtns?\.?|mountains?|ridge)[- ]?(view|vista)s?|(mtns?\.?|mountains?)[- ]?(life|living|magic|side|top|home|house|over|retreat|hide-?away)"
        r"|foothill|hillside|hill[- ]top|mountain[- ]top|mountainous|(view|vista)s? of (the )?(mtns?\.?|mountains?))",
    
    "location_water_view": r"((lake|ocean|river|sea|bay|sound|water)[- ]?(view|vista)s?|(lake|ocean|river|sea|bay|sound)[- ]?(life|living)"
        r"|(view|vista)s? of ([a-z]+ ){,3}(lake|ocean|river|sea|bay|sound)"
        r"|^(lake|river|resevoir)$)",
    "location_waterfalls": r"water[- ]?fall",
    
    
    "location_laundromat": r"(laundro[- ]?mat|coin[- ]?(operated[- ]?)?laundry)",
    "location_laundry": r"((site|free) laundry|laundry (room|machine|facilit(y|ies)))",
    
    "location_library": r"library",
    "location_restaurants": r"(restaurants?|fine[ -]*dining|eateries)",
    
    "location_fishing": r"fishing|trout|salmon",
    "feature_horses_allowed": r"(horses?[- ()/]*(property|allowed|permitted|possible|set-?up|ok|zoned|$)|(zoned|suited|suitable) (for )?horses?)",
    "location_horseback_riding": r"horse[- ]*(back|trail|riding|stable|country)",
    "location_skiing": r"(skiing|ski[- ]?slopes?|snow[- ]?board(ing)?|ski the|ski (cabin|lodge|chalet|resort|ski-in))",
    "location_tennis": r"tennis",
    "location_winery_tours": r"wine[- ]?(country|tour)",
    "location_museums": r"(museum|art[- ]?galler(y|ies)|galler(y|ies))",
    "location_playground": r"play[- ]?(ground|area)",
    "location_resort": r"resort",
    "location_sight_seeing": r"sight[- ]?seeing",
    "location_adventure_activities": r"(surfing|para[- ]?sailing|(para|hang)[- ]?gliding|hot[- ]?air[- ]?balloon|rafting|kayaking|skiing|roller[- ]?(blade|skate)|tubing)",
    
    "location_swimming": r"swim",
    "location_walking": r"(walking|easy[- ]?walk|walk to|steps away)",
    "location_water_parks": r"water[- ]?park",
    "location_water_tubing": r"(water|inner)[- ]?tubing",
    "location_wildlife_viewing": r"(wild[- ]?life|national[- ]?park|state[- ]?park|whale|dolphin)",
    "location_zoo": r"\bzoo\b",
    "location_campus": r"(campus|university|college)",
    "location_recreation_center": r"(rec(reation)|fitness)?[- ]*center",
    "location_theme_parks": r"(disney([- ]?(world|land)?)|universal[- ]?studio|six[- ]?flags|magic[- ]?kingdom|silver[- ]?dollar[- ]?city|busch[- ]?gardens|lego[- ]?land|theme[- ]?park)",
    "location_hiking": r"(hiking|(park|forest|mountain|mtn\.?) trail|trail access|national[- ]?park|state[- ]?park)",
    
    #"lot_size": r"(\d+(,|\. ?))*\d+[-. _/]*(acre|ac\b|m\.? * plot|meter * plot)",
}

e_APPLIANCE_SEARCHES = {
    "feature_dishwasher": r"dishwasher",
    "feature_refrigerator": r"(refrigerator|(^|\b)fridge)",
    "feature_bar_refrigerator": r"bar[- ]*(refrigerator|fridge|cooler|chiller)",
    "feature_microwave": r"microwave",
    "feature_disposal": r"(^disposal|garbage[- ]*disposal)",
    "feature_dryer": r"dryer",
    "feature_washer": r"(^|\b)washer",
    "feature_water_heater": r"((water|h2o)[- ]*h(ea)?te?r|instant[- ]*hot[- ]*water|hot[- ]*water)",
    "feature_oven": r"((^|\b)oven|ov[-/]?rg)",
    "feature_stove": r"((^|\b)range|stove(top)?|cook(top)?|gas[- ]*cooking|ov[-/]?rg)",
    "feature_ice_maker": r"(?<!for )ice[- ]*maker",
    "feature_freezer": r"freezer",
    "feature_water_softener": r"(water[- ]*(softener|conditioner)|soft[- ]*water[- ]*loop|water[- ]*treat(ment)?|(iron|rust|calcium)[- ]*filter)",
    "feature_humidifier": r"(^|\b)humidifier",
    "feature_wine_chiller": r"wine[- /]*(beverage[- /]*)?(refrigerator|chiller|cooler)",
    "feature_water_purifier": r"(water[- /]*(purifier|filter|filtration)|(re?ve?rse?|wa?te?r)[- ]*osmosis)",
    "feature_exhaust_fan": r"exhaust[- ]*fan",
    "feature_indoor_grill": r"indoor[- ]*(grill|bbq|barbeque|barbecue)",
    "feature_trash_compactor": r"(trash|garbage|refuse)[- ]*compact[oe]r",
    "feature_central_vacuum": r"central[- ]?vac",
    "feature_sump_pump": r"sump[- /]*pump",
    "feature_intercom": r"inter[- ]*com",
    "feature_pantry": r"pantry",
    
    "feature_bbq_area": r"(bbq|barbecue|barbeque|grill|gas ((stub )?for )?(bbq|barbeque|barbecue|grill(ing)?)|(fire|bbq|barbeque|barbecue|grilling|grill)[- ]*(area|pit))",
}

#e_INTERIOR_FEATURES_RE = {
#    s_tag: re.compile(s_search, flags=re.I)
#    for s_tag, s_search in e_INTERIOR_FEATURES_SEARCHES.items()
#}
#e_LOT_FEATURES_RE = {
#    s_tag: re.compile(s_search, flags=re.I)
#    for s_tag, s_search in e_LOT_FEATURES_SEARCHES.items()
#}

e_INTERIOR_FEATURES_RE = {}
for s_tag, s_search in e_INTERIOR_FEATURES_SEARCHES.items():
    try:
        e_INTERIOR_FEATURES_RE[s_tag] = re.compile(s_search, flags=re.I)
    except:
        print(f"Couldn't compile interior feature regex for {s_tag}")

e_LOT_FEATURES_RE = {}
for s_tag, s_search in e_LOT_FEATURES_SEARCHES.items():
    try:
        e_LOT_FEATURES_RE[s_tag] = re.compile(s_search, flags=re.I)
    except:
        print(f"Couldn't compile lot feature regex for {s_tag}")

e_APPLIANCE_FEATURES_RE = {}
for s_tag, s_search in e_APPLIANCE_SEARCHES.items():
    try:
        e_APPLIANCE_FEATURES_RE[s_tag] = re.compile(s_search, flags=re.I)
    except:
        print(f"Couldn't compile appliance regex for {s_tag}")

e_AGE_SEARCHES = {
    
}

e_CONDITION_YEARS = {
    "Under Construction": 0,
    "Pre-Construction": 0,
    "New Construction": 0,
    '1 - 3 Years': -2,
    "0-5 Years": -2,
    "1-5 Years": -3,
    "6-10 Years": -8,
    "7-14 Years": -10,
    "11-15 Years": -13,
    '11-20 Years': -15,
    "16-20 Years": -18,
    '21+ Years': -21,
    "21-25 Years": -23,
    '21-30 Yrs': -25,
    "26-35 Years": -31,
    '31 - 40 Years': -35,
    '31 Yrs. and Older': -35,
    '31 Yrs. and Older,Resale': -35,
    '31-40 Yrs': -35,
    '36-49 Years': -40,
    '38 Years': -48,
    "36-49 Years": -42,
    '43 Years': -43,
    '41 - 50 Years': -45,
    '41-50 Yrs': -45,
    '50 Years': -50,
    "50+ Years": -50,
    '51+ Years': -51,
    '58 Years': -58,
    '51 Years to 70 Years': -60,
    "50-74 Years": -62,
    '51 - 75 Years': -63,
    '74 Years': -74,
    '75 Years': -75,
    '76 Years': -76,
    '81 Years': -81,
    '81 Years': -81,
    '81+ Years': -81,
    "75-99 Years": -87,
    '90 Years': -90,
    '99 Years': -99,
    
    "100+ Years": -100,
    "Age (100+ Years)": -100,
    
    'Age (1 to 5 years)': -3,
    'Age (1oo to 200 years)': -150,
    'Age (21 to 40 years)': -30,
    'Age (41 to 60 years)': -50,
    'Age (51-60 Years)': -55,
    'Age (6 to 10 years)': -8,
    'Age (61 to 100 years)': -80,
    'Age: 0-1': 0,
    'Age: 11-20': -15,
    'Age: 21-30': -25,
    'Age: 51+': -51,
    'Age: 51+ years,Existing Structure': -51,
    
    'Resale,21 to 30 Years,Good to Excellent': -25,
    'Resale,61 to 70 Years': -65,
    'Resale,71 to 80 Years,Condition: Other - See Remarks(Inspection Reports Available)': -75,
    
}

e_LOT_FEATURES_SIZES = {
    "zero lot attached": 0,
    "0 Up To 1/4 Acre": 0.125,
    "0-.25 Acres": 0.125,
    "under 1/4 acre": 0.25,
    "< 1/4 acre": 0.25,
    "1/4 acre or less": 0.25,
    "less than 1/4 acre lot": 0.25,
    "less than .25 acre": 0.25,
    "0 - .5 Acre": 0.25,
    ".25 to .5 acre": 0.375,
    "less than .5 acre": 0.375,
    "1/4 to less than 1/2 acre lot": 0.375,
    "1/4+ to 1/2 acre": 0.375,
    "1/4 to less than 1/2 acre lot": 0.375,
    "1/2 to less than 3/4 acre lot": 0.625,
    "1/4 to 1  Acre Lot": 0.625,
    "0-1 unit/acre": 0.5,
    "1/2 to less than 3/4 acre lot": 0.625,
    "3/4 to less than 1 acre lot": 0.875,
    '1 to less than 2 acre lot': 1.5,
    "acre": 1,
    "2 to less than 3 acre lot": 2.5,
    '2+ to 3 acres': 2.5,
    '2.5 to 5 acres': 3.25,
    '3 to less than 4 acre lot': 3.5,
    '4+ to 5 acres': 4.5,
    '4 to less than 5 acre lot': 4.5,
    '5+ to 10 acres': 7.5,
    '10+ acres': 10,
}

c_PUBLIC_TRANSIT = {
'Near Public Transit',
'Near Bus',
'Near Train',
' Near Public Transit',
'On Bus Line',
}


c_AUCTION = {
    "Auction",
    "AuctionForeclosure",
    "Auction Foreclosure",
    "Foreclosure",
}
c_WATERFRONTS = {
    True,
    'bay/harbor', 'water', 'waterfront', 'beach,water', 'oceanfront',
    'beach', 'oceanfront, sandy beach', 'ocean, canal',
    'lakefront', 'river', 'lakefront,waterfront', 'lagoon', 'lake', 'bay/harbor, waterfront',
    'yes,waterfront', 'nantucket sound, bay/harbor, beach, waterfront, oceanfront'
}

e_LEVEL_STORIES = {
    "1": 1,  # 8
    "2": 2,  # 12
    "3": 3,  # 3
    "2 Story": 2,  # 43
    "2 Story, Multi-Level": 2,  # 1
    "2 Story, One Story": 2,  # 1
    "3 or more Stories": 3,  # 5
    "Five or More": 5,  # 4
    "Four": 4,  # 11
    "Ground Level": 1,  # 23
    "Ground Level, Mid Level, Top Level": 3,  # 1
    "Multi/Split": 2,  # 643
    "Multi/Split,One": 1,  # 2
    "Multi/Split,Three Or More": 3,  # 4
    "Multi/Split,Tri-Level": 3,  # 1
    "Multi/Split,Two": 2,  # 13
    #"Multi-Level": 2,  # 1
    "One": 1,  # 12569
    "One Level": 1,  # 6
    "One Story": 1,  # 411
    "One Story,One": 1,  # 60
    "One Story,Two": 1,  # 6
    "One, Ground Level": 1,  # 6
    "One, Top Level": 1,  # 1
    "One,Multi/Split": 1,  # 14
    "One,Other/See Remarks": 1,  # 1
    "One,Three Or More": 3,  # 13
    "One,Two": 2,  # 75
    "One,Two,Multi/Split": 2,  # 1
    #"Other": ,  # 73
    "Split Level": 2,  # 1
    "Three": 3,  # 65
    "Three Levels": 3,  # 7
    "Three Or More": 3,  # 1582
    "Three or More Stories": 3,  # 110
    "Three or More Stories,One": 3,  # 5
    "Three or More Stories,Three Or More": 3,  # 16
    "Three Or More, Multi/Split": 3,  # 3
    "Three Or More, One": 3,  # 4
    "Three Or More, Two": 3,  # 3
    "Three Or More,Multi/Split": 3,  # 31
    "Three Or More,Two": 3,  # 1
    #"Top Level": ,  # 4
    "Tri-Level": 3,  # 27
    "Tri-Level,Three Or More": 3,  # 2
    "Two": 2,  # 8224
    "Two Levels": 2,  # 31
    "Two Story": 2,  # 391
    "Two Story,One": 2,  # 1
    "Two Story,Two": 2,  # 30
    "Two, Ground Level": 2,  # 1
    "Two, Multi/Split": 2,  # 6
    "Two, One": 2,  # 25
    "Two,Multi/Split":2 ,  # 54
    "Two,Three Or More": 3,  # 19
    "Two,Three Or More,Multi/Split": 3,  # 1
    #"Upper unit": ,  # 1

}
e_TOPO_REPLACE = {
    "sloping": "sloping",
    "up slope": "sloping",
    "upsloping": "sloping",
    "upslope": "sloping",
    "down slope": "sloping",
    "downslope": "sloping",
    "downsloping": "sloping",
    "slopes downhill": "sloping",
    "slope downhill": "sloping",
    "slopes uphill": "sloping",
    "slope uphill": "sloping",
    "sloped": "sloping",
    "lot sloped": "sloping",
    "slope": "sloping",
    'up slope lot': 'sloping',
    'south sloped': "sloping",
    'down slope lot': "sloping",
    'moderate': "sloping",
    'moderate slope': "sloping",
    'partial sideslope': 'sloping',
    'sloping to flat to sloping again': 'sloping',
    'sloped and wooded': 'sloping',
    'level to sloping': 'sloping',
    'modest slopes': 'sloping',
    
    'gentle': 'gentle',
    'slope gentle': 'gentle',
    'sloped gentle': 'gentle',
    'gently sloped': 'gentle',
    'gentle sloped': 'gentle',
    'gentle slope': 'gentle',
    'slight slope': 'gentle',
    'gently rolling': 'gentle',
    'gentle sloping': 'gentle',
    'terrain grad slope': 'gentle',
    'gentle roll': 'gentle',
    'gentle rolling': 'gentle',
    'gently sloping': 'gentle',
    'level to gently roll': 'gentle',
    'sloped gently': 'gentle',
    'level gently slope in rear': 'gentle',
    'slight bank to level area': 'gentle',
    'equestrian': 'gentle',
    
    'level': 'level',
    'flat': 'level',
    'fairly level': 'level',
    'mostly level': 'level',
    'terrain flat': 'level',
    'level lot': 'level',
    
    'canyonvalley': "canyon",
    'canyon rim': "canyon",
    
    'trees': 'trees',
    'forest': 'trees',
    'partially wooded': 'trees',
    
    'rocky': 'rocky',
    'rocks': 'rocky',
    'rock outcropping': 'rocky',
    
    'steep': 'steep',
    'steep upslope': 'steep',
    'steep downslope': 'steep',
    'steep slope': 'steep',
    'steep sloped': 'steep',
    'steep sloping': 'steep',
    'slope steep': 'steep',
    'sloped steep': 'steep',
    'sloping steep': 'steep',
    'level to steep': 'steep',
    'rough': 'steep',
    'terrain steep slope': 'steep',
    
    'rolling': 'rolling',
    'roll': 'rolling',
    'rolli': 'rolling',
    'rollin': 'rolling',
    'somewhat hilly': 'rolling',
    'terrain hilly': 'rolling',
    'hill': 'rolling',
    'hilltop': 'rolling',
    'hillside': 'rolling',
    'hilly': 'rolling',
    'rolling terrain': 'rolling',
    'rolled': 'rolling',
    'moderate roll': 'rolling',
    'level to rolling': 'rolling',
    'diverse rolling fields and pastures': 'rolling',
    
    'mountain': 'mountain',
    'mountainous': 'mountain',
    'ridge top': 'mountain',
    'terrain mountain': 'mountain',
    'cliff': 'mountain',
    
    'flood plain': 'flood_plain',
    'flood_plain': 'flood_plain',
    'flood': 'flood_plain',
    'some area flood plain': 'flood_plain',
    'flood plain partial': 'flood_plain',
    'flood plain area': 'flood_plain',
    
    'dunes': 'dunes',
    'sand dunes': 'dunes',
    'sand dune': 'dunes',
    'dune': 'dunes',
    
    'terraced': 'terraced',
    'terraces': 'terraced',
    
    'waterfront': 'waterfront',
    'water_front': 'waterfront',
    'water front': 'waterfront',
    'lake': 'waterfront',
    'beach': 'waterfront',
}

c_TOPO_IGNORE = {
    "varies",
    "varied",
    "cul-de-sac",
    "combo",
    "comb",
    "combination",
    "lot grade varies",
    "mixed",
    'street to access',
    'street to street',
    "remarks",
    "see remarks",
    "flag lot",
    "corner",
    "corner lot",
    'transitional',
    'cleared',
    'open',
    'seeremarks',
    'creek-winter',
    'partially wooded',
    'part wooded',
    'part wood',
    'hardwood',
    'heavily wooded',
    'undeveloped',
    'other',
    'snow line below',
    'snow line above',
    'meadow',
    'trees',
    'forest',
    'partially wooded',
    'wooded',
    'pad',
    'partially clear',
    'exceptional view',
    'elevation high',
    'other_(see_rmks)',
    'other (see rmks)',
    'see rmks',
    'existing wash(s)',
    'existing wash',
    'graded',
    'terrain',
    'road frontage',
    'view property',
    'surveyed',
    'pasture',
    'pond',
    'low',
    's',
    'specify in remarks',
    'stream',
    'cul',
    'mud',
    'town of vail gsa',
    'avalanche',
    'none known',
    'extreme topo',
    'rockfall',
    'clear',
    'filled',
    'planted pine',
    'high',
    'mixed topo',
    'topo mix',
    'mostly',
    'mountian view',
    'none',
    'some',
    'spring',
    'solar potential',
    'irregular',
    'notapplic',
    'not applicable',
    'notapplicable',
    'high ground',
    'walkout',
    'low land',
    'mostly wooded',
    'bottomland',
    'layered',
    'see attached topo',
    'desert',
    'scenic',
    'watershed',
    'partial',
    'year round view',
    '.5',
    'natural drainage',
    'brush', 'brush covered',
    'mostly open',
    'views',
    'creek',
}

e_TOPO_PRIORITY = {
    'mountain': 25,
    'canyon': 24,
    'steep': 23,
    'bluff': 22,
    'valley': 21,
    'plateau': 20,
    'sloping': 25,
    'rolling': 20,
    'gentle': 15,
    'dunes': 11,
    'flood_plain': 10,
    'terraced':5,
    'level': 2,
    'rocky': 1,
}


e_ZILLOW_PROPERTY_TYPES = {
    "House": [
        "House",
        "SingleFamily",
        "Single Family",
        "single_family",
        "Craftsman",
        "Contemporary",
        "Contemp",
        "Contemporary,Craftsman",
        "Contemporary/Modern",
        "Urban Contemporary",
        "Colonial,Contemporary",
        "Cape Cod",
        "Cape",
        "New Englander",
        "Federal",
        "Victorian",
        "Queen Anne / Victorian",
        "English",
        "Colonial",
        "Colonial,Two Story",
        "Conventional",
        "Traditional",
        "Contemporary,Traditional",
        "Mid-Century Modern",
        "TraditonalAmerican",
        "Two Story",
        "2 Story",
        "Transitional",
        "Three Story",
        "Split-Level",
        "Split Level",
        "Craftsman,Traditional",
        "Northwest",
        "NW Contemporary",
        "Modern",
        "Mountain Contemporary",
        "Straight Thru",
        "Reverse Floor Plan",
        "Florida",
        "Santa Barbara/Tuscan",
        "Spanish",
        "Spanish/Mediterranean",
        "Mediterranean",
        "Beach House",
        "Coastal",
        "Coastal,Contemporary",
        "Raised Beach",
        "Key West",
        "Coastal,Reverse Floor Plan",
        "Reverse Floor Plan,Coastal",
        "Old Style",
        "Stick Built",
        "House,Detached",
        "Detached",
        "Single Detached",
        "Single Family Residence",
        "Manufactured House",
        "SINGLE_FAMILY",
        "MANUFACTURED",
        "Single Family",
    ],
    "Rancher": [
        "Rancher",
        "Ranch",
        "Craftsman,Rancher",
        "Ranch,One Story",
        "One Story",
        "Ranch/Rambler",
        "Ranch / Rambler",
        "Raised Ranch",
        "Ranch,Traditional",
        "Rambler/Ranch",
        "Contemporary,Ranch",
        "Ranch,Traditional",
        "Southwestern",
        "Other,Ranch,One Story",
        "Pueblo",
        "Territorial/Santa Fe",
        "Santa Fe / Pueblo Style",
        "Ranch,Contemporary/Modern",
        "single story",
    ],
        
    "Other": [
        "Float Home", "Camp",
        "Other", "Other (See Remarks)",
        "Other-See Remarks", "See Remarks",
        "MobileManufactured", "Unknown", "VacantLand",
        "mobile home", "Prefab, Modular",
        "Unimproved Land", "Lot",
        "Spec", "Plan", "SpecUnderConstruction",
    ],
    
    "Chateau": [
        "Chateau",
        "Tudor",
        "French Country",
        "Craftsman,French Country",
        "Villa",
        "Victorian,Villa",
    ],
    "Estate": [
        "Estate",
        "Mansion",
        "Castle",
        "Entire castle"],
        
    "Cottage": [
        "Bungalow",
        "Cottage",
        "Other,Bungalow",
        "Florida Cottage",
        "Cottage/Bungalow",
        "Cabin/Cottage",
        "Bungalow/Cottage",
        "Cottage,Ranch",
        "Bungalow,Craftsman",
        "Bungalow,Traditional",
    ],

    "Cabin": [
        "Cabin",
        "Log,Craftsman",
        "Chalet",
        "A-Frame",
        "A-Frame,Chalet",
        "Chalet,Contemporary",
        "Chalet,Craftsman",
        "Chalet,French Country",
        "Mountain",
        "Cabin,Log",
        "Log Home",
        "Rustic",
        "Ranch,Log",
        "Mountain, Log",
        "Log",
        "Log Cabin",
        "Cabin,Country Rustic",
        "Log Cabin/Rustic",
    ],
    "Townhouse": [
        "Townhouse",
        "Townhome",
        "Townhouse,Traditional",
        "Contemporary,Townhouse",
        "Multi Level,Townhouse",
        "Contemporary/Modern,Townhouse",
        "Contemporary,Multi Level,Townhouse",
        "Craftsman,Townhouse",
        "End of Row/Townhouse",
        "Townhouse,Single Family Residence",
        "TOWNHOUSE",
        "townhomes",
    ],
    "Farmhouse": [
        "Farmhouse",
        "Farmhouse/National Folk",
        "Farm House",
    ],
    "Condo": [
        "High Rise",
        "Condo",
        "Condominium",
        "Condominium,Single Family Residence",
        "Condo,Contemporary",
        "High Rise Condo",
        "Penthouse",
        "Other,Penthouse,High Rise",
        "Penthouse,Mid Rise",
        "Flat,Penthouse",
        "Mid Rise",
        "Other,Mid Rise",
        "Loft",
        "Cooperative",
        "Interior Row/Townhouse",
        "Condo 5+ Stories,Condo,High Rise",
        "Low Rise (1-3)",
        "Condo 5+ Stories,Condo",
        "Condo (1 Level)",
        "High-Rise (8+ floors)",
        "High-rise",
        "CONDO",
        'condos',
    ],
    "Apartment": [
        "Apartment", "Flat", "ApartmentUnit",
        "Unit/Flat", "Unit/Flat/Apartment,Garden 1 - 4 Floors",
        "WaitlistApartment", "WaitlistApartmentUnit",
        "APARTMENT", "apartment",
    ],
    "Duplex": [
        "Duplex",
        "MultiFamily",
        "Multi Family",
        "duplex single",
        "Duplex,Single Family Residence",
        "Triplex",
        "Quadruplex",
        "duplex_triplex",
        "multi_family",
    ],
    'Tiny house': [
        "Tiny house", "Tinyhouse", "Tiny House", "Tiny House/Yurt",
        'Entire tiny house', 'Entire tinyhouse', 'Entire Tiny house', 'Entire Tinyhouse',
    ],
    'Earth house': [
        "Earth house", "Earthhouse", "Earth House",
        'Entire earth house', 'Entire earthhouse', 'Entire Earth house', 'Entire Earthhouse',
    ],
}

e_ZILLOW_PROP_TYPE_STD = {}
for s_std, a_vals in e_ZILLOW_PROPERTY_TYPES.items():
    for s_val in a_vals:
        e_ZILLOW_PROP_TYPE_STD[s_val.lower()] = s_std


e_FIXER = {
    0: 0,
    1: 1,
    2: 2,
    3: 3,
    4: 4,
    5: 5,
    6: 6,
    7: 7,
    8: 8,
    9: 9,
    10: 10,
    True: 3,
    False: 0,
    #none: 0,
    
    "new construction": 1,
    "excellent": 1,
    "excellant": 1,
    "exellent": 1,
    "execellent": 1,
    "excel": 1,
    "like new": 1,
    "brand new": 1,
    "brand": 1,
    "mint": 1,
    "aaa": 1,
    "mint aaa": 1,
    "perfect": 1,
    "pristine": 1,
    "new": 1,
    "diamond": 1,
    "excellent condition": 1,
    "like new condition": 1,
    "mint condition": 1,
    "perfect condition": 1,
    "new condition": 1,
    "diamond condition": 1,
    "never occupied": 1,
    "age new ready for occupancy": 1,
    "new completed construction": 1,
    "new constr": 1,
    "immaculate": 1,
    "new construction new foundation": 1,
    "completed never occupied": 1,
    "interior condition excellent": 1,
    "exterior condition excellent": 1,
    "interior excellent": 1,
    "exterior excellent": 1,
    "new building": 1,
    
    "new construction existing foundation": 2,
    "construction complete": 2,
    "construction completed": 2,
    "const complete": 2,
    "completed construction": 2,
    "vg": 2,
    "very good": 2,
    "very good condition": 2,
    "great": 2,
    "great condition": 2,
    "turnkey": 2,
    "turn key": 2,
    "fully remodeled": 2,
    "fully remodel": 2,
    "full remodel": 2,
    "refurbished": 2,
    "fully refurbished": 2,
    "completely renovated": 2,
    "newly renovated": 2,
    "updated remodeled": 2,
    "interior condition very good": 2,
    "exterior condition very good": 2,
    "interior very good": 2,
    "exterior very good": 2,
    "gorgeous": 2,
    "stunning": 2,
    "very nice": 2,
    "superb": 2,
    
    "inventory home": 2,
    "display home": 2,
    "complete spec home": 2,
    "model for sale": 2,
    "spec home": 2,
    "model home": 2,
    "model home for sale": 2,
    
    "above average": 3,
    "good": 3,
    "nice": 3,
    "good condition": 3,
    "clean": 3,
    "charming": 3,
    "beautiful": 3,
    "average to good": 3,
 
    "interior condition good": 3,
    "exterior condition good": 3,
    "interior condition above average": 3,
    "exterior condition above average": 3,
    "interior good": 3,
    "exterior good": 3,
    "interior above average": 3,
    "exterior above average": 3,
    "not new and not a model": 3,
    "additions alterations": 3,
    "additions alter": 3,
    "substantially remodeled": 3,
    "remodeled": 3,
    "restored": 3,
    "renovated": 3,
    "upgrades": 3,
    "never sold previously occupied": 3,
    
    #"resale": 3,
    "existing": 3,
    "existing home": 3,
    "blt standing": 3,
    "built standing": 3,
    "completed": 3,
    "complete": 3,
    "existing structure": 3,
    "actual": 3,
    "existingconstruction": 3,
    "existing construction": 3,
    "existing resale": 3,
    "move in": 3,
    #"": 3,
    
    "average": 4,
    "decent": 4,
    "average condition": 4,
    "interior condition average": 4,
    "exterior condition average": 4,
    "interior average": 4,
    "exterior average": 4,
    "not new": 4,
    "rehabbed": 4,
    "rehabilitated": 4,
    "partial remodel": 4,
    "partially remodel": 4,
    "partially remodeled": 4,
    "existing partly updated": 4,
    "updated": 4,
    "termite clearance": 4,
    "remodeled structural changes all major mechanical systems i e hvac": 4,
    
    "repairs cosmetic": 5,
    "repair cosmetic": 5,
    "renovated cosmetic changes of at least 50% total living area i e paint": 5,
    "minor remodel": 5,
    "model not for sale": 5,
    "fair": 5,
    "okay": 5,
    "ok": 5,
    "fair condition": 5,
    "interior condition fair": 5,
    "exterior condition fair": 5,
    "interior fair": 5,
    "exterior fair": 5,
    "used": 5,
    "existing construction not new": 5,
    "pre owned": 5,
    "previously owned": 5,
    "original": 5,
    "unknown age": 5,
    "not updated": 5,
    "no additions alter": 5,
    "no additions alterations": 5,
    "no additions alter": 5,
    "no additions": 5,
    "age unknown": 5,
    "age unknown": 5,
    "all original": 5,
    #"additions/alterations": ,
    #"additions/alter": ,
    #"minor remodel": ,
    
    "poor to fair": 6,
    'below average': 6,
    'below average condition': 6,
    "age new under construction": 6,
    "interior condition below average": 6,
    "exterior condition below average": 6,
    "interior below average": 6,
    "exterior below average": 6,
    "new under construction": 6,
    
    'poor': 7,
    'poor condition': 7,
    "interior condition poor": 7,
    "exterior condition poor": 7,
    "interior poor": 7,
    "exterior poor": 7,
    "as is": 7,
    "historic": 7,
    "historical": 7,
    "register of historic homes": 7,
    "cer historic": 7,
    "certified historic": 7,
    "sold as is": 7,
    "sheetrocked": 7,
    "drywall complete": 7,
    'under construction': 7,
    'under contruct': 7,
    'under renovation': 7,
    
    "fixer": 8,
    "fixer upper": 8,
    "fixer up": 8,
    "handyman": 8,
    "handyman special": 8,
    "tlc": 8,
    "needs work": 8,
    "interior needs work": 8,
    "exterior needs work": 8,
    "new construction incomplete": 8,
    "needs tlc": 8,
    "needs some tlc": 8,
    "unfinished": 8,
    
    'repairs major': 9,
    "major rehab needed": 9,
    "Incomplete": 9,
    "value mainly in land": 9,
    "potential tear down": 9,
    "foundation needs work": 9,
    "framed enclosed": 9,
    "framed in": 9,
    "needs reno": 9,
    "needs renovation": 9,
    "needs rehab": 9,
    "needs restoration": 9,
    "needs restoring": 9,
    "damaged": 9,
    
    'foundation only': 10,
    'home w o lot': 10,
    "tear down": 10,
    "tear down value in land": 10,
}

c_CONDITION_TBB = {
    "platted sub",
    #'building permit',
    #'permit ready',
    'estimated',
    'model not for sale',
    'entire subdivision',
    'tbb',
    't b b',
    'proposed',
    'proposed construction',
    'pre construction',
    'preconstruction',
    'to be built',
    'pre construction',
    'preconstruction',
    'const start upon sale',
    'age new proposed construction',
    'new to be built',
    'to be blt',
    'build to suit',
    'new construct to be built',
    'to be built lot home',
    'not yet built',
    'to be built floor plan',
    'presales',
    'presale',
    'pre sales',
    'pre sale',
    'new will build to suit',
    'to b built',
    'new t b b',
    'new tbb',
    'const start upon sale',
    'proposed build',
    'under construc spec homes',
    'design build package',
    'to be buil',
    'multi builder devel',
    'new proposed construction',
    'new proposed construction',
    'to be built',
    'to be constructed',
    'age new will build to suit',
    'under construction spec home',
    'tbb 2023',
    'tbb 2024',
    'tbb 2025',
    'tbb 2026',
    'tbb 2027',
    'tbb 2028',
    'tbb 2029',
    'tbb 2030',
    'tbb 2031',
    'tbb 2032',
    'tbb 2033',
    'tbb 2034',
    'tbb 2035',
    'tbb 2036',
    'tbb 2037',
    'tbb 2038',
    'tbb 2039',
    "we bring the plan",
}


c_CONDEMNED = {
    "Value Mainly In Land",
    'Fixer, Repairs Major, Additions/Alter, Tear Down, Value Mainly In Land',
    'Fixer, Repairs Major, Tear Down',
    'Fixer, Tear Down',
    'Fixer, Value Mainly In Land',
}

c_CONDITION_JUNK = {
    None, "unknown", "none", "true",
    "approximate", "standard", "other",
    "other see remarks", "see remarks",
    'not answered', 'unknown mixed',
}