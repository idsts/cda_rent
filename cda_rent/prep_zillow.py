﻿import os, sys, re, json
from time import time
import pandas as pd
import numpy as np

try:
    from . import utils, zillow_standardization, airbnb_standardization, hotel_proximity, location_analysis, db_tables, area_data, us_states
except:
    import utils, zillow_standardization, airbnb_standardization, hotel_proximity, location_analysis, db_tables, area_data, us_states

__all__ = [
    "prep_zillow_file", "get_area_info",
    "read_zillow_file", "standardize_zillow_file",
    "find_hotels", "write_output",
    "add_zcta",
]

e_VARS = None
e_LTR_VARS = None
e_AREAS, t_DEFAULT_AREA = None, None
o_AREA_LOOKUPS = None
o_LTR_AREA_LOOKUPS = None
o_HOTELS = None
o_DB = None
c_VALID_PROPERTY_CATEGORIES = {"House", "Vacation home", "Condo", "Townhouse", "Apartment"}
c_STR_TRAIN_COLS, c_LTR_TRAIN_COLS = None, None
c_COMMON_TRAIN_COLS, c_ALL_TRAIN_COLS, c_ALL_ALLOWED_COLS = None, None, None

e_ZIP_TO_ZCTA = {}
e_ZCTA = {}

e_ZILLOW_DTYPE = {
    "zpid": str, "address/zipcode": str, "features/onMarketDate": str, "features/parcelNumber": str, "parcelId": str,
}


def load_data(**kwargs):
    print("Loading Areas and Hotels")
    n_start = time()
    """
    global e_AREAS
    #e_AREAS = joblib.load(utils.get_local_path("areas_compressed_target2.pkl"))
    df_areas = pd.read_csv(utils.get_local_path("areas.csv"), encoding="utf-8-sig")
    df_areas["lat_area_copy"], df_areas["long_area_copy"] = df_areas["lat_area"], df_areas["long_area"]
    
    print(f"{len(df_areas):,} known areas")
    e_AREAS = {i_prec: {
        (d_lat if not pd.isnull(d_lat) else None, d_long if not pd.isnull(d_long) else None): e_area
        for (d_lat, d_long), e_area in df_prec.set_index(["lat_area_copy", "long_area_copy"]).to_dict(orient="index").items()}
        for i_prec, df_prec in df_areas.groupby("precision")}
    
    global i_MIN_PREC, i_MAX_PREC
    i_MIN_PREC, i_MAX_PREC = int(min(e_AREAS.keys())), int(max(e_AREAS.keys()))
    
    e_default_area = e_AREAS[i_MIN_PREC][(None, None)]
    global t_DEFAULT_AREA
    t_DEFAULT_AREA = (
        e_default_area["precision"], # area precision
        e_default_area["area_revenue"], # area revenue
        e_default_area["pt_area_revenue"], # area pt revenue
        #d_pt_area_diff_mean, # pt revenue vs area revenue
        e_default_area["pt_area_rev_diff"], # pt revenue vs area revenue
        e_default_area["properties"], #area properties
        e_default_area["rev_per_bedroom"], # revenue per bedroom
        e_default_area["rev_per_bathroom"], # revenue per bathroom
        e_default_area["rate_per_bedroom"], # listed rate per bedroom
    )
    """
    
    global e_VARS
    #with open(utils.get_local_path("model_variables_20210810.json"), "r", encoding="utf-8-sig") as f:
    with open(utils.get_local_path("model_variables_20220207_v2.json"), "r", encoding="utf-8-sig") as f:
        e_VARS = json.load(f)
    
    global e_LTR_VARS
    with open(utils.get_local_path("model_variables_long_term_rent_20220811.json"), "r", encoding="utf-8-sig") as f:
        e_LTR_VARS = json.load(f)
    
    global c_STR_TRAIN_COLS, c_LTR_TRAIN_COLS, c_COMMON_TRAIN_COLS, c_ALL_TRAIN_COLS, c_ALL_ALLOWED_COLS
    c_STR_TRAIN_COLS = set(e_VARS["train_cols"])
    c_LTR_TRAIN_COLS = set(e_LTR_VARS["train_cols"])
    c_COMMON_TRAIN_COLS = c_STR_TRAIN_COLS.intersection(c_LTR_TRAIN_COLS)
    c_ALL_TRAIN_COLS = c_STR_TRAIN_COLS.union(c_LTR_TRAIN_COLS)
    c_ALL_ALLOWED_COLS = set(e_VARS["allowed_cols"]).union(set(e_LTR_VARS["allowed_cols"]))
    
    global o_AREA_LOOKUPS
    #o_AREA_LOOKUPS = location_analysis.AreaLookups(utils.get_local_path("all_areas_20210810.csv.xz"))
    #o_AREA_LOOKUPS = location_analysis.AreaLookups(utils.get_local_path("all_areas_20220207_v2_condensed.csv.xz"))
    o_AREA_LOOKUPS = location_analysis.AreaLookups(utils.get_local_path("all_areas_20220207_v2.csv.xz"))
    
    global o_LTR_AREA_LOOKUPS
    o_LTR_AREA_LOOKUPS = location_analysis.AreaLookups(utils.get_local_path("all_areas_rent_20220809_shrunk.csv.xz"))
    
    global o_HOTELS
    o_HOTELS = hotel_proximity.HotelProximity(
        utils.get_local_path("hotels_20210810.csv.xz"),
        precision=0,
        offsets=[
                     (-2,-1), (-2,+0), (-2,+1),
            (-1,-2), (-1,-1), (-1,+0), (-1,+1), (-1,+2),
            (+0,-2), (+0,-1), (+0,+0), (+0,+1), (+0,+2),
            (+1,-2), (+1,-1), (+1,+0), (+1,+1), (+1,+2),
                     (+2,-1), (+2,+0), (+2,+1),
            ],
    )
    
    global o_DB
    if o_DB is None:
        if db_tables is None: db_tables.load_db(**kwargs)
        o_DB = db_tables.o_DB
    
    global e_ZIP_TO_ZCTA
    global e_ZCTA
    df_zip_to_zcta = o_DB.get_table_as_df(db_tables.zip_to_zcta)
    df_zcta = o_DB.get_table_as_df(db_tables.zcta)
    e_ZIP_TO_ZCTA = dict(zip(df_zip_to_zcta["zip_code"], df_zip_to_zcta["zcta"]))
    e_ZCTA = df_zcta.set_index("zcta")[["population", "houses", "population_density", "housing_density", "land_sqmi", "water_sqmi"]].to_dict(orient="index")
    
    
    area_data.load_data()
    
    print("\tLoading Areas and Hotels took:", utils.secondsToStr(time() - n_start))


def read_zillow_file(zillow_file, **kwargs):
    if isinstance(zillow_file, str) and zillow_file.lower().endswith(".xlsx"):
        df_zillow = pd.read_excel(zillow_file)
    elif isinstance(zillow_file, str) and zillow_file.lower().endswith(".csv"):
        df_zillow = pd.read_csv(zillow_file, encoding=utils.encoding_tester(zillow_file), low_memory=False, dtype=e_ZILLOW_DTYPE)
    elif isinstance(zillow_file, str) and zillow_file.lower().endswith(".json"):
        df_zillow = pd.read_json(zillow_file, orient="index")
    elif isinstance(zillow_file, pd.DataFrame):
        df_zillow = zillow_file
    else:
        raise Exception(f"Unknown file type for input zillow file: {zillow_file}")

    return df_zillow


def standardize_zillow_file(df_zillow, **kwargs):
    print("Starting Standardization")
    n_start = time()
    df_converted_zillow = zillow_standardization.convert_zillow(df_zillow)
    c_categories = kwargs.get("property_categories", c_VALID_PROPERTY_CATEGORIES)
    print("Include property categories:", c_categories)
    df_converted_zillow = df_converted_zillow[df_converted_zillow["property_category"].isin(c_categories)]
    
    if "state" not in df_converted_zillow.columns and "address_state" in df_converted_zillow.columns:
        df_converted_zillow["state"] = df_converted_zillow["address_state"].apply(lambda x: us_states.e_STATE_ABBREV.get(x.lower(), x) if isinstance(x, str) else x)
        #print('df_converted_zillow["state"]', dict(df_converted_zillow["state"].value_counts()))
    
    #print("df_converted_zillow.columns", list(df_converted_zillow.columns))
    
    #print('df_converted_zillow["living_area"]', dict(df_converted_zillow["living_area"].value_counts()))
    #print('df_converted_zillow["lot_size"]', dict(df_converted_zillow["lot_size"].value_counts()))
        
    print(f"{len(df_converted_zillow):,} Applicable Properties")
    print("\tStandardizing took:", utils.secondsToStr(time() - n_start))
    return df_converted_zillow


#def add_defaults(df_zillow, **kwargs):

def to_input(df_std, **kwargs):
    print("\nConverting to input format")
    n_start = time()
    if "property_type" in df_std.columns and "Property Type" in df_std.columns:
        raise Exception("property_type and Property Type both in df")
    #elif "property_type" not in df_std.columns and "Property Type" not in df_std.columns:
    #    raise Exception("property_type and Property Type not in df")
    
    df_std.rename(columns={"Property Type": "property_type"}, inplace=True)
    
    for col in df_std.columns:
        if col in {
            "Property Type", "Property Category", "property_type", "property_category",
            "address_state", "address_city", "address_zipcode", "address_street",
            "address", "city", "zipcode", "zip",
            "price", "rent", "property_tax_rate", "year_built",
            'nearest_hotel_dist_0', 'nearest_hotel_rate_0',
            'nearest_hotel_dist_1', 'nearest_hotel_rate_1',
            'nearest_hotel_dist_2', 'nearest_hotel_rate_2',
            'demand_seasonality', 'demand_season_length', 'demand_season_peak_month', 'demand_season_max_month',
            'availability_seasonality', 'availability_season_length', 'availability_season_peak_month', 'availability_season_max_month',
            'max_guests', 'reservation_length',
        }: continue
        if re.search(r"^-?\d_area_", col): continue
        
        #print("to_input.col", col)
        e_c_vals = dict(df_std[col].value_counts())
        if all(x in {True, False} for x in e_c_vals):
            if kwargs.get("b_debug"): print(f"to_input: col '{col}' already true/false")
            pass
        elif all(x in {1, 0} for x in e_c_vals):
            if kwargs.get("b_debug"): print(f"to_input: col '{col}' already 0/1")
            if not col.startswith(("location_", "feature_")): print("0/1 col", col)
            pass
        elif all(type(x) in {float, int} for x in e_c_vals):
            if kwargs.get("b_debug"): print(f"to_input: col '{col}' already numeric")
            pass
        elif all(type(x) in {float, int, bool} for x in e_c_vals):
            if kwargs.get("b_debug"): print(f"to_input: col '{col}' mixed boolean/numeric. Convert to numeric")
            df_std[col] = df_std[col].apply(lambda x: 0 if pd.isnull(x) else int(x) if isinstance(x, bool) else x)
        else:
            if kwargs.get("b_debug"): print(f"to_input: convert to boolean col '{col}'")
            #print(col, e_c_vals)
            df_std[col] = df_std[col].apply(lambda x: True if not pd.isnull(x) and x not in {False, "False", None, 0, "0", 0.0, "FALSE", "NO", "no", "No"} else False)
    
    #if kwargs.get("check_price"): assert "price" in list(df_std.columns)
    
    #print("cols into dummies", [col for col in df_std if col in e_VARS["allowed_cols"]])
    df_input = airbnb_standardization.prep_for_dummies(
        df_std[[
            col for col in df_std
            if col in c_ALL_ALLOWED_COLS and
            col not in {"address_state", "address_city", "address_zipcode", "address_street",
            "address", "city", "zipcode", "zip",}
        ]]
    )
    
    #if kwargs.get("check_price"): assert "price" in list(df_input.columns)
    
    if kwargs.get("check_na", True):
        print("Check NA", kwargs.get("check_na"))
        print("\tColumns with NA values:", set(df_input.columns) - set(df_input.dropna(how="any", axis=1).columns))
        i_rows, i_valid_rows = len(df_input), len(df_input.dropna(axis=0))
        
        print(f"\t{i_rows:,} rows, {i_valid_rows:,} valid, {i_rows - i_valid_rows:,} rows with NA values")
    
    print("\tConverting to input took:", utils.secondsToStr(time() - n_start))
    return df_input


def add_area_info(df_zillow, **kwargs):    
    df_property_areas_info = location_analysis.add_area_info(df_zillow, o_AREA_LOOKUPS)
    
    df_ltr_property_areas_info = location_analysis.add_area_info(df_zillow, o_LTR_AREA_LOOKUPS)
    df_ltr_property_areas_info.rename(columns={f'{i_prec}_area_properties': f'{i_prec}_area_ltr_properties' for i_prec in range(-3, 4 + 1)}, inplace=True)
    
    a_merged_cols = []
    a_merged_col_names = []
    for i_prec in range(-3, 4 + 1):
        if f'{i_prec}_area_ltr_properties' in df_ltr_property_areas_info:
            a_merged_cols.append(df_ltr_property_areas_info[f'{i_prec}_area_ltr_properties'])
            a_merged_col_names.append(f'{i_prec}_area_ltr_properties')
            #print(f'{i_prec}_area_ltr_properties')
        elif f'{i_prec}_area_properties' in df_ltr_property_areas_info:
            a_merged_cols.append(df_ltr_property_areas_info[f'{i_prec}_area_properties'])
            a_merged_col_names.append(f'{i_prec}_area_ltr_properties')
            #print(f'{i_prec}_area_properties')
        
        for s_area_col in ["price", "price_per_bedroom", "price_per_sqft", "price_per_acre"]:
            dc_area_col = None
            if (
                f'{i_prec}_area_{s_area_col}' in df_property_areas_info and f'{i_prec}_area_{s_area_col}' in df_ltr_property_areas_info
                and  f'{i_prec}_area_properties' in df_property_areas_info and f'{i_prec}_area_ltr_properties' in df_ltr_property_areas_info
            ):
                dc_area_col = ((
                    (df_property_areas_info[f'{i_prec}_area_{s_area_col}'] * df_property_areas_info[f'{i_prec}_area_properties'].fillna(0))
                    + (df_ltr_property_areas_info[f'{i_prec}_area_{s_area_col}'] * df_ltr_property_areas_info[f'{i_prec}_area_ltr_properties'].fillna(0))
                ) / (df_property_areas_info[f'{i_prec}_area_properties'].fillna(0) + df_ltr_property_areas_info[f'{i_prec}_area_ltr_properties'].fillna(0))).round(2)
            elif f'{i_prec}_area_{s_area_col}' in df_property_areas_info:
                dc_area_col = df_property_areas_info[f'{i_prec}_area_{s_area_col}']
            elif f'{i_prec}_area_{s_area_col}' in df_ltr_property_areas_info:
                dc_area_col = df_ltr_property_areas_info[f'{i_prec}_area_{s_area_col}']
            if dc_area_col is not None:
                a_merged_cols.append(dc_area_col)
                a_merged_col_names.append(f'{i_prec}_area_{s_area_col}')
        
        for s_area_col in ["rent_per_bedroom", "rent_per_price", "rent_per_sqft", "rent_per_acre"]:
            dc_area_col = None
            if f'{i_prec}_area_{s_area_col}' in df_property_areas_info and f'{i_prec}_area_{s_area_col}' in df_ltr_property_areas_info:
                dc_area_col = df_ltr_property_areas_info[f'{i_prec}_area_{s_area_col}'].fillna(df_property_areas_info[f'{i_prec}_area_{s_area_col}']).round(2)
            elif f'{i_prec}_area_{s_area_col}' in df_ltr_property_areas_info:
                dc_area_col = df_ltr_property_areas_info[f'{i_prec}_area_{s_area_col}']
            elif f'{i_prec}_area_{s_area_col}' in df_property_areas_info:
                dc_area_col = df_property_areas_info[f'{i_prec}_area_{s_area_col}']
            if dc_area_col is not None:
                a_merged_cols.append(dc_area_col)
                a_merged_col_names.append(f'{i_prec}_area_{s_area_col}')
        
    df_merged_area_cols = pd.concat(a_merged_cols, axis=1)
    df_merged_area_cols.columns = a_merged_col_names
    
    df_areas_info = df_property_areas_info[[col for col in df_property_areas_info.columns if col not in a_merged_col_names and "area_scale" not in col]].join(df_merged_area_cols)
    df_areas_info = df_areas_info.reindex(sorted(df_areas_info.columns, key=lambda s: int(re.search(r"^-?\d+", s).group())), axis=1)
    
    #df_zillow_with_areas = df_zillow.join(to_input(df_areas_info, check_na=False))
    df_zillow_with_areas = df_zillow.join(df_areas_info)
    #print("df_zillow_with_areas", list(df_zillow_with_areas.columns))
    #return df_zillow_with_areas, df_areas_info
    return df_zillow_with_areas, df_areas_info


def expand_out_area_info(df_inputs, i_req_level=3, **kwargs):
    for i_prec in range(-3, 4 + 1):
        if f"{i_prec}_area_properties" in df_inputs.columns:
            df_inputs[f"{i_prec}_area_properties"] = df_inputs[f"{i_prec}_area_properties"].fillna(0)
        if f"{i_prec}_area_ltr_properties" in df_inputs.columns:
            df_inputs[f"{i_prec}_area_ltr_properties"] = df_inputs[f"{i_prec}_area_ltr_properties"].fillna(0)
        
        for s_col in location_analysis.a_ALL_PRECISION_COLS:
            if s_col in {"area_properties"}: continue
            if f"{i_prec}_{s_col}" not in df_inputs.columns:
                if i_prec > i_req_level:
                    continue
                else:
                    if f"{i_prec - 1}_{s_col}" not in df_inputs.columns:
                        #print(f"Add zeroes: {i_prec}_{s_col}")
                        df_inputs[f"{i_prec}_{s_col}"] = 0
                        if kwargs.get("b_debug"): print("add missing column", f"{i_prec}_{s_col}")
                    else:
                        #print(f"Add area above: {i_prec - 1}_{s_col} -> {i_prec}_{s_col}")
                        df_inputs[f"{i_prec}_{s_col}"] = df_inputs[f"{i_prec - 1}_{s_col}"]
                        if kwargs.get("b_debug"): print("copy column from larger area", f"{i_prec}_{s_col}")
            else:
                if f"{i_prec - 1}_{s_col}" not in df_inputs.columns:
                    #print(f"Fill zeroes: {i_prec}_{s_col}")
                    df_inputs[f"{i_prec}_{s_col}"] = df_inputs[f"{i_prec}_{s_col}"].fillna(0)
                    if kwargs.get("b_debug"): print("no larger area, fill with zeros", f"{i_prec}_{s_col}")
                else:
                    #print(f"Fill area above: {i_prec - 1}_{s_col} -> {i_prec}_{s_col}")
                    df_inputs[f"{i_prec}_{s_col}"] = df_inputs[f"{i_prec}_{s_col}"].fillna(df_inputs[f"{i_prec - 1}_{s_col}"])
                    if kwargs.get("b_debug"): print("fillna with from larger area", f"{i_prec}_{s_col}")
        #df_inputs = df_inputs.copy()
    return df_inputs

def find_hotels(df_std, **kwargs):
    print("Finding Hotels")
    n_start = time()
    df_std_with_hotels = o_HOTELS.find_nearest_hotels(df_std)
    d_MAX_DIST = 50
    a_hotel_cols = []
    for i in range(10):
        if f"nearest_hotel_dist_{i}" in df_std_with_hotels.columns:
            #print(f"nearest_hotel_dist_{i}")
            a_hotel_cols.extend([f"nearest_hotel_dist_{i}", f"nearest_hotel_rate_{i}"])
            df_std_with_hotels[f"nearest_hotel_dist_{i}"] = df_std_with_hotels[f"nearest_hotel_dist_{i}"].fillna(d_MAX_DIST).clip(upper=d_MAX_DIST)
            df_std_with_hotels[f"nearest_hotel_rate_{i}"] = [
                rate if not pd.isnull(rate) and dist < d_MAX_DIST else 0
                for dist, rate in zip(df_std_with_hotels[f"nearest_hotel_dist_{i}"], df_std_with_hotels[f"nearest_hotel_rate_{i}"])]
    print("\tFinding Hotels took:", utils.secondsToStr(time() - n_start))
    return df_std_with_hotels


def zip_to_zcta(v_zip):
    if pd.isnull(v_zip): return None
    if isinstance(v_zip, str):
        if len(v_zip) < 5: v_zip = "{}{}".format("0" * (5 - len(v_zip)), v_zip)
    elif isinstance(v_zip, (int, float)):
        v_zip = f"{int(v_zip):05d}"
    return v_zip[:5]


def add_zcta(df_std, **kwargs):
    print("Adding population density")
    
    a_zip_cols = [
        col for col in [
        "address_zipcode", "address_zip_code", "address_postal_code", "address_post_code",
        "address/zipcode", "address/zip_code", "address/zip code", "address/postal_code", "address/post_code",
        "zipcode", "zip_code", "postal_code", "post_code",
    ] if col in df_std.columns]
    if not a_zip_cols:
        print('no zip cols found', [col for col in df_std.columns if "zip" in col])
        return df_std
    
    df_std["address_zcta"] = df_std[a_zip_cols[0]].apply(zip_to_zcta).apply(e_ZIP_TO_ZCTA.get)

    df_std["zcta_population"] = df_std["address_zcta"].apply(lambda x: e_ZCTA.get(x, {}).get("population", 0))
    df_std["zcta_houses"] = df_std["address_zcta"].apply(lambda x: e_ZCTA.get(x, {}).get("houses", 0))
    df_std["zcta_population_density"] = df_std["address_zcta"].apply(lambda x: e_ZCTA.get(x, {}).get("population_density", 0))
    df_std["zcta_housing_density"] = df_std["address_zcta"].apply(lambda x: e_ZCTA.get(x, {}).get("housing_density", 0))
    df_std["zcta_land_area"] = df_std["address_zcta"].apply(lambda x: e_ZCTA.get(x, {}).get("land_sqmi", 0))
    df_std["zcta_water_area"] = df_std["address_zcta"].apply(lambda x: e_ZCTA.get(x, {}).get("water_sqmi", 0))
    
    return df_std


def guess_property_info(df_std_with_area_info, b_apply=True, **kwargs):
    a_lot_size = []
    a_living_area = []
    a_price = []
    
    a_cols = [
        "state",
        "lot_size",
        "living_area",
    ]
    for col in ["adj_revenue", "rent", "rate", "price", "bedrooms", "bathrooms",]:
        if col in df_std_with_area_info.columns:
            a_cols.append(col)
    for i in range(0, 3 + 1):
        a_cols.extend([
            col for col in [
                f'{i}_area_lot_size',
                f'{i}_area_living_area',
                f'{i}_area_price',
    
                f'{i}_area_rev_per_acre',
                f'{i}_area_rate_per_acre',
                f'{i}_area_rent_per_acre',
                f'{i}_area_price_per_acre',
    
                f'{i}_area_rev_per_sqft',
                f'{i}_area_rate_per_sqft',
                f'{i}_area_rent_per_sqft',
                f'{i}_area_price_per_sqft',]
            if col in df_std_with_area_info.columns
        ])
    
    for e_row in df_std_with_area_info[a_cols].to_dict(orient="records"):
        e_row = {k: v for k, v in e_row.items() if not pd.isnull(v)}
        
        d_lot_size = e_row.get("lot_size")
        if pd.isnull(d_lot_size) or not isinstance(d_lot_size, (float, int)) or d_lot_size <= 0:
            d_lot_size = location_analysis.guess_lot_size(e_row)
            e_row["lot_size"] = d_lot_size
        
        d_living_area = e_row.get("living_area")
        if pd.isnull(d_living_area) or not isinstance(d_living_area, (float, int)) or d_living_area <= 0:
            d_living_area = location_analysis.guess_living_area(e_row)
            e_row["living_area"] = d_living_area
        
        d_price = e_row.get("price")
        if pd.isnull(d_price) or not isinstance(d_price, (float, int)) or d_price <= 0:
            d_price = e_row.get("calc_price")
            e_row["price"] = d_price
        
        if pd.isnull(d_price) or not isinstance(d_price, (float, int)) or d_price <= 0:
            d_price = location_analysis.guess_price(e_row)
            e_row["price"] = d_price
            #if pd.isnull(d_price):
            #print('guess price for e_row', e_row, d_price)
        
        a_lot_size.append(d_lot_size)
        a_living_area.append(d_living_area)
        a_price.append(d_price)
    
    #df_std_with_area_info["lot_size"] = df_std_with_area_info["lot_size"].replace(0, np.nan).fillna(pd.Series(a_lot_size, index=df_std_with_area_info.index))
    #df_std_with_area_info["living_area"] = df_std_with_area_info["living_area"].replace(0, np.nan).fillna(pd.Series(a_living_area, index=df_std_with_area_info.index))
    #df_std_with_area_info["calc_price"] = df_std_with_area_info["calc_price"].replace(0, np.nan).fillna(pd.Series(a_price, index=df_std_with_area_info.index))
    
    if b_apply:
        df_std_with_area_info["lot_size"] = a_lot_size
        df_std_with_area_info["living_area"] = a_living_area
        df_std_with_area_info["price"] = a_price
    
    return df_std_with_area_info, (a_lot_size, a_lot_size, a_lot_size)


def write_output(df_std_with_hotels, s_name, **kwargs):
    print(f"Writing {s_name}")
    n_start = time()
    s_output = kwargs["output_file"]
    if kwargs.get("always_csv"):
        s_output = os.path.splitext(s_output)[0] + ".csv"
    
    b_success = False
    print(f"Output to: {s_output}")
    if s_output.lower().endswith(".csv"):
        df_std_with_hotels.to_csv(s_output, index=kwargs.get("index", True), encoding="utf-8-sig")
        b_success = True
    elif s_output.lower().endswith(".json"):
        json.dump(
            {
                index: row.dropna().to_dict()
                for index,row in df_std_with_hotels.iterrows()},
            open(s_output, "w", encoding="utf-8-sig"))
        b_success = True
    elif s_output.lower().endswith(".xlsx"):
        o_writer = pd.ExcelWriter(s_output, options={'strings_to_urls': False})
        df_std_with_hotels.to_excel(o_writer, index=kwargs.get("index", True))
        o_writer.save()
        b_success = True
    else:
        print("Invalid output format:", s_output)
    print(f"\tWriting {s_name} took:", utils.secondsToStr(time() - n_start))
    return b_success


def prep_zillow_file(zillow_file, **kwargs):
    print(f"\n######## STANDARIZE AND PREP ZILLOW FILE FOR REVENUE PREDICTIONS ########\n")
    n_start = time()
    
    if o_AREA_LOOKUPS is None or o_LTR_AREA_LOOKUPS is None or o_HOTELS is None or e_ZCTA is None:
        load_data(**kwargs)
    
    df_zillow = read_zillow_file(zillow_file, **kwargs)
    
    df_zillow_std = standardize_zillow_file(df_zillow, **kwargs)
    #print("df_zillow_std.columns", list(df_zillow_std.columns))
    
    #df_zillow_std = add_defaults(df_zillow, **kwargs)
    
    df_zillow_std_with_hotels = find_hotels(df_zillow_std, **kwargs)
    
    df_zillow_std_with_hotels = add_area_info(df_zillow_std_with_hotels, **kwargs)
    
    #assert "demand_season_max_month" in df_zillow_std_with_hotels.columns
    #assert "area_demand_season_max_month" not in df_zillow_std_with_hotels.columns
    
    #print("df_zillow_std_with_hotels.columns", list(df_zillow_std_with_hotels.columns))
    
    add_zcta(df_zillow_std_with_hotels, **kwargs)
    df_zillow_std_with_hotels = df_zillow_std_with_hotels.copy()
    
    expand_out_area_info(df_zillow_std_with_hotels, **kwargs)
    #print("expand_out_area_info.columns 1", list(df_zillow_std_with_hotels.columns))
    
    df_zillow_std_with_hotels, t_guess_cols = guess_property_info(df_zillow_std_with_hotels, b_apply=True, **kwargs)
    
    if kwargs.get("output_file"):
        write_output(df_zillow_std_with_hotels, "Zillow Standardized with Hotel Distances", **kwargs)
    
    print("\n\tProcessing took:", utils.secondsToStr(time() - n_start))
    return df_zillow_std_with_hotels


"""
    This gets run when prep_zillow.py is run from the command prompt
    Args:
        args: {list} of the positional command line options.
            [0] is the full or relative path to the raw zillow file
        kwargs:
            --output_file:"c:/path/to/zillow_standardized.csv"  optional path to write the resulting output to
"""
if __name__ == "__main__":
    args, kwargs = utils.parse_command_line_args(sys.argv)
    
    if not args or len(args) < 1:
        raise Exception('Zillow spreadsheet not specified.')
        
    print('args', args, '\n')
    print('kwargs', kwargs, '\n')
    
    s_zillow = utils.check_path(
        args[0],
        s_default_folder=os.getcwd(),
        required_existence=True,
        required_file=True,
        required_extension=('.xlsx', '.csv'),
        s_name="Zillow Property File")
        
    if "always_csv" not in kwargs:
        kwargs["always_csv"] = True
    
    if not kwargs.get("output_file"):
        s_path, s_ext = os.path.splitext(s_zillow)
        kwargs["output_file"] = utils.get_new_write_filename(f"{s_path}_standardized{s_ext}")
    
    prep_zillow_file(s_zillow, **kwargs)