﻿# pip install googlemaps
import os, sys, re
from datetime import datetime, timedelta
from time import sleep
from math import sin, cos, sqrt, atan2, radians
from collections import defaultdict
import logging, traceback
import urllib3, requests, urllib
import googlemaps
import pandas as pd
import requests, json

try:
    from . import utils, paths, log
except:
    import utils, paths, log
s_API_KEY = os.environ.get("REVENUE_GOOGLE_MAPS_API_KEY", os.environ.get("GOOGLE_MAPS_API_KEY", "")) #Cory
assert s_API_KEY
#print("GMAPS API KEY", s_API_KEY)


e_STATES = {
    "AL": "Alabama",
    "AK": "Alaska",
    "AZ": "Arizona",
    "AR": "Arkansas",
    "CA": "California",
    "CO": "Colorado",
    "CT": "Connecticut",
    "DE": "Delaware",
    "DC": "District of Columbia",
    "FL": "Florida",
    "GA": "Georgia",
    "HI": "Hawaii",
    "ID": "Idaho",
    "IL": "Illinois",
    "IN": "Indiana",
    "IA": "Iowa",
    "KS": "Kansas",
    "KY": "Kentucky",
    "LA": "Louisiana",
    "ME": "Maine",
    "MD": "Maryland",
    "MA": "Massachusetts",
    "MI": "Michigan",
    "MN": "Minnesota",
    "MS": "Mississippi",
    "MO": "Missouri",
    "MT": "Montana",
    "NE": "Nebraska",
    "NV": "Nevada",
    "NH": "New Hampshire",
    "NJ": "New Jersey",
    "NM": "New Mexico",
    "NY": "New York",
    "NC": "North Carolina",
    "ND": "North Dakota",
    "OH": "Ohio",
    "OK": "Oklahoma",
    "OR": "Oregon",
    "PA": "Pennsylvania",
    "RI": "Rhode Island",
    "SC": "South Carolina",
    "SD": "South Dakota",
    "TN": "Tennessee",
    "TX": "Texas",
    "UT": "Utah",
    "VT": "Vermont",
    "VA": "Virginia",
    "WA": "Washington",
    "WV": "West Virginia",
    "WI": "Wisconsin",
    "WY": "Wyoming",
    "VI": "US Virgin Islands",
    "PR": "Puerto Rico",
    "GU": "Guam",
    "AS": "American Samoa",
    "MP": "Northern Mariana Islands",
}

i_GEOCODE_TRIES = 3
d_GEOCODE_RETRY_DELAY = 1
dt_GMAPS_LOADED = datetime.utcnow()

o_GMAPS = None

def retry_google_maps_geocode(s_address, **kwargs):
    global o_GMAPS, dt_GMAPS_LOADED
    if o_GMAPS is None or datetime.utcnow() - timedelta(hours=1) > dt_GMAPS_LOADED:
        o_GMAPS = googlemaps.Client(key=s_API_KEY)
        dt_GMAPS_LOADED = datetime.utcnow()
    
    #print("using retry_google_maps_geocode")
    i_try = 1
    while i_try <= i_GEOCODE_TRIES:
        if i_try > 2:
            o_GMAPS = googlemaps.Client(key=s_API_KEY)
            dt_GMAPS_LOADED = datetime.utcnow()
        
        try:
            a_geocode_results = o_GMAPS.geocode(s_address)
            print(f"using o_GMAPS.geocode, try: {i_try}")
            return a_geocode_results
        except (googlemaps.exceptions.TransportError, requests.exceptions.ConnectionError, urllib3.exceptions.ProtocolError):
            print(f'Google maps geocode retry {i_try}: "{s_address}"')
        sleep(d_GEOCODE_RETRY_DELAY)
        i_try += 1
    return []

def retry_google_maps_reverse_geocode(d_latitude, d_longitude, **kwargs):
    global o_GMAPS, dt_GMAPS_LOADED
    if o_GMAPS is None or datetime.utcnow() - timedelta(hours=1) > dt_GMAPS_LOADED:
        o_GMAPS = googlemaps.Client(key=s_API_KEY)
        dt_GMAPS_LOADED = datetime.utcnow()
    
    i_try = 1
    while i_try <= i_GEOCODE_TRIES:
        if i_try > 2:
            o_GMAPS = googlemaps.Client(key=s_API_KEY)
            dt_GMAPS_LOADED = datetime.utcnow()
        
        try:
            print(f"using o_GMAPS.reverse_geocode, try: {i_try}")
            a_reverse_geocode_results = o_GMAPS.reverse_geocode((d_latitude, d_longitude))
            return a_reverse_geocode_results
        except (googlemaps.exceptions.TransportError, requests.exceptions.ConnectionError, urllib3.exceptions.ProtocolError):
            print(f'Google maps reverse_geocode retry {i_try}: "{s_address}"')
        sleep(d_GEOCODE_RETRY_DELAY)
        i_try += 1
    return []


def get_distance(point1, point2):
    # get distance in kilometers
    R = 6371
    lat1 = radians(point1[0])  #insert value
    lon1 = radians(point1[1])
    lat2 = radians(point2[0])
    lon2 = radians(point2[1])

    dlon = lon2 - lon1
    dlat = lat2- lat1

    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1-a))
    distance = R * c
    return distance


def parse_components(e_geocode, **kwargs):
    e_address_components = defaultdict(list)
    for e_component in e_geocode["address_components"]:
        for s_type in e_component["types"]:
            e_address_components[s_type].append(e_component["short_name"])
    
    for s_admin in (
        e_address_components.get("administrative_area_level_1", []) +
        e_address_components.get("administrative_area_level_2", []) +
        e_address_components.get("administrative_area_level_3", [])):
        #print("s_admin", s_admin)
        if s_admin.upper() in e_STATES:
            e_address_components["state"] = [s_admin.upper()]
            break
    
    for s_locality in e_address_components.get("locality", []):
        e_address_components["city"].append(s_locality)
    
    for s_sublocality in e_address_components.get("sublocality", []):
        e_address_components["city"].append(s_sublocality)
    
    if not e_address_components.get("state"):
        for s_poli in reversed(e_address_components.get("political", [])):
            if s_poli.upper() in e_STATES:
                e_address_components["state"] = [s_poli.upper()]
                break
    
    e_address_components["formatted_address"] = e_geocode.get("formatted_address", "")
    return e_address_components


def address_to_coordinates(s_address, **kwargs):
    # Geocoding an address
    #a_geocode_results = o_GMAPS.geocode(s_address)
    a_geocode_results = retry_google_maps_geocode(s_address)
    
    a_coordinates = []
    for e_geocode in a_geocode_results:
        e_coord = e_geocode["geometry"]["location"].copy()
        
        e_coord["address_components"] = dict(parse_components(e_geocode))
        
        a_coordinates.append(e_coord)
    
    return a_coordinates


def filter_coords_by_countries(a_addresses_coordinates, c_countries, b_keep_unknown=False, **kwargs):
    a_filtered = []
    for e_coord in a_addresses_coordinates:
        a_countries = e_coord.get('address_components', {}).get("country", [])
        if any([s_country in c_countries for s_country in a_countries]):
            a_filtered.append(e_coord)
        elif b_keep_unknown and not a_countries:
            a_filtered.append(e_coord)
    return a_filtered


def filter_coords_without_street(a_addresses_coordinates, b_require_street_name=True, b_require_street_number=True,  **kwargs):
    a_filtered = []
    for e_coord in a_addresses_coordinates:
        a_street_number = e_coord.get('address_components', {}).get("street_number", [])
        a_street_name = e_coord.get('address_components', {}).get("route", [])
        if b_require_street_name and not a_street_name: continue
        if b_require_street_number and not a_street_number: continue
        a_filtered.append(e_coord)
    return a_filtered


def coordinates_to_address(d_latitude, d_longitude, b_require_street_name=True, b_require_street_number=False, d_max_distance=-1, **kwargs):
    # Geocoding an address
    #a_geocode_results = o_GMAPS.geocode(s_address)
    #a_reverse_geocode_results = o_GMAPS.reverse_geocode((d_latitude, d_longitude))
    a_reverse_geocode_results = retry_google_maps_reverse_geocode(d_latitude, d_longitude)
    
    a_coordinates = []
    for e_geocode in a_reverse_geocode_results:
        e_coord = e_geocode["geometry"]["location"].copy()
        e_coord["address_components"] = dict(parse_components(e_geocode))
        if b_require_street_name and not e_coord["address_components"].get("route"): continue
        if b_require_street_number and not e_coord["address_components"].get("street_number"): continue
        e_coord["distance_from_search"] = get_distance((d_latitude, d_longitude), (e_coord["lat"], e_coord["lng"]))
        if d_max_distance >=0 and d_max_distance < e_coord["distance_from_search"]: continue
        #break
        a_coordinates.append(e_coord)
    
    a_coordinates.sort(key=lambda e: (
        len(e.get("address_components", {}).get("street_number", [])),
        len(e.get("address_components", {}).get("route", [])),
        len(e.get("address_components", {}).get("locality", [])),
        ),
        reverse=True)
    
    c_coordinates = set()
    c_addresses = set()
    a_deduped_coordinates = []
    for e_coord in a_coordinates:
        t_coords = (round(e_coord["lat"], 5), round(e_coord["lng"], 5))
        if t_coords in c_coordinates: continue
        t_addr = (
            "-".join(sorted(set(e_coord.get("street_number", [])))),
            ", ".join(sorted(set(e_coord.get("route", [])))),
            ", ".join(sorted(set(e_coord.get("locality", [])))),
        )
        if t_addr in c_addresses: continue
        a_deduped_coordinates.append(e_coord)
        c_coordinates.add(t_coords)
        c_addresses.add(t_addr)
    #print(len(a_coordinates), len(a_deduped_coordinates))
    
    a_deduped_coordinates.sort(key=lambda e: e.get("distance_from_search", 99999))
    
    return a_deduped_coordinates


def check_coordinates_address(e_input, **kwargs):
    s_address = e_input.get("address", e_input.get("address_address"))
    s_city = e_input.get("city", e_input.get("address_city"))
    s_state = e_input.get("state", e_input.get("address_state"))
    s_zipcode = e_input.get("zipcode", e_input.get("address_zipcode"))
    d_latitude = e_input.get("latitude")
    d_longitude = e_input.get("longitude")
    
    #print("s_address", s_address)
    #print("s_city", s_city)
    #print("s_state", s_address)
    #print("s_zipcode", s_zipcode)
    
    if isinstance(d_latitude, str):
        if not d_latitude.strip():
            d_latitude = None
        else:
            d_latitude = utils.to_float(d_latitude)
    if isinstance(d_longitude, str):
        if not d_longitude.strip():
            d_longitude = None
        else:
            d_longitude = utils.to_float(d_longitude)
    
    if s_address and (not d_latitude or not d_longitude or kwargs.get("use_address")):
        if s_city and s_city not in s_address:
            s_address += f", {s_city}"
        if s_state and s_state not in s_address:
            s_address += f", {s_state}"
        if s_zipcode and str(s_zipcode) not in s_address:
            s_address += f" {s_zipcode}"
        #print("\ns_address", s_address)
        
        a_addr_coords = address_to_coordinates(s_address, **kwargs)
        a_street_coords = filter_coords_without_street(a_addr_coords, **kwargs)
        #a_us_street_coords = filter_coords_by_countries(a_street_coords, {"US", "VI", "GU"})
        a_us_coords = filter_coords_by_countries(a_addr_coords, {"US"}, **kwargs)
        
        if not a_us_coords:
            if a_street_coords:
                return {"address": f"Not a valid US street address \"{s_address}\". This API only currently supports predictions for US properties.", "matches": a_street_coords}, 400
            elif a_addr_coords:
                return {"address": f"Not a valid street address \"{s_address}\". Please double-check that it contains a street name, number, and state or zip code."}, 400
            else:
                return ({
                    "address": f"Unknown google maps response format or problem in filtering",
                    "a_addr_coords": a_addr_coords,
                    "a_street_coords": a_street_coords,
                    "a_us_coords": a_us_coords}, 400)
        #elif len(a_us_coords) > 1:
        #    return {"address": f"Found multiple potential matches for \"{s_address}\". Please clarify input.", "matches": a_us_coords}, 400
        
        return a_us_coords[0], 200
    
    elif not pd.isnull(d_latitude) and not pd.isnull(d_longitude):
        a_coord_addr = coordinates_to_address(
            d_latitude,
            d_longitude,
            #b_require_street_name=True,
            #b_require_street_number=False,
            d_max_distance=1,  # max distance between found address and coordinates in Km
            **kwargs
        )
        #print("a_coord_addr", a_coord_addr)
        #a_street_coords = filter_coords_without_street(a_addr_coords)
        a_us_coords = filter_coords_by_countries(a_coord_addr, {"US", "VI", "GU"}, **kwargs)
        
        if not a_us_coords and s_address:
            if s_city and s_city not in s_address:
                s_address += f", {s_city}"
            if s_state and s_state not in s_address:
                s_address += f", {s_state}"
            if s_zipcode and s_zipcode not in s_address:
                s_address += f" {s_zipcode}"
            #print("\ns_address", s_address)
            a_addr_coords = address_to_coordinates(s_address, **kwargs)
            a_street_coords = filter_coords_without_street(a_addr_coords, **kwargs)
            #a_us_street_coords = filter_coords_by_countries(a_street_coords, {"US", "VI", "GU"})
            a_us_coords = filter_coords_by_countries(a_addr_coords, {"US"}, **kwargs)
            
            if not a_us_coords:
                if a_street_coords:
                    return {"address": f"Not a valid US street address \"{s_address}\". This API only currently supports predictions for US properties.", "matches": a_street_coords}, 400
                elif a_addr_coords:
                    return {"address": f"Not a valid street address \"{s_address}\". Please double-check that it contains a street name, number, and state or zip code."}, 400
                else:
                    return ({
                        "address": f"Unknown google maps response format or problem in filtering",
                        "a_addr_coords": a_addr_coords,
                        "a_street_coords": a_street_coords,
                        "a_us_coords": a_us_coords}, 400)
        
        if a_coord_addr and not a_us_coords:
            return {"latitude/longitude": f"Not a valid US location. Coordinates lat:{d_latitude}, long:{d_longitude}"}, 400
        elif not a_coord_addr:
                return {"latitude/longitude": f"Not a valid rental location near a street. Coordinates lat:{d_latitude}, long:{d_longitude}"}, 400
        elif len(a_us_coords) > 1:
            return {"latitude/longitude": f"Found multiple potential house matches for coordinates lat:{d_latitude}, long:{d_longitude}. Please adjust coordinate input.", "matches": a_us_coords}, 400
        
        return a_us_coords[0], 200
    else:
        return {"location":f"Invalid input of either \"address\" (string/text value) or both \"latitude\" and \"longitude\" (floating point values)."}, 400

def get_elevation(latitude, longitude, source="local"):
    elevation = None
    if source in {"aster30", "aster"}:
        s_url = f"https://api.opentopodata.org/v1/aster30m?locations={latitude},{longitude}"
    elif source in {"mapzen", "aster"}:
        s_url = f"https://api.opentopodata.org/v1/mapzen?locations={latitude},{longitude}"
    else:
        s_url = f"http://192.168.10.210:5000/v1/mapzen?locations={latitude},{longitude}"
    
    o_response = requests.get(s_url)
    if o_response.status_code == 200:
        e_result = json.loads(o_response.text)
        if e_result.get("status") == "OK":
            a_results = e_result.get("results", [])
            if a_results:
                elevation = a_results[0].get("elevation", None)
                return elevation
    return None