﻿import os, sys, re
import requests, json
from urllib.request import quote
from datetime import datetime, timedelta
from dateutil.parser import parse
from time import time, sleep
from collections import defaultdict, Counter
import traceback
import pandas as pd
import numpy as np
import threading

import sshtunnel
import pymongo
from pymongo import DeleteOne, UpdateOne, InsertOne, ReadPreference
from pymongo.collation import Collation

try:
    from . import utils, zillow_standardization, airbnb_standardization, us_states
except ImportError:
    import utils, zillow_standardization, airbnb_standardization, us_states

o_DOCDB = None

s_SSH_HOST = os.environ.get("CDA_TUNNEL_HOST")
s_SSH_USER = os.environ.get("CDA_TUNNEL_USER")
s_SSH_PKEY = os.environ.get("CDA_TUNNEL_PKEY")

s_DOCDB_CA_FILE = os.environ.get("CDA_DOCDB_CA_FILE")
s_DOCDB_HOST = os.environ.get("CDA_DOCDB_HOST")
s_DOCDB_HOST_READONLY = os.environ.get("CDA_DOCDB_HOST_READONLY")
i_DOCDB_PORT = int(os.environ.get("CDA_DOCDB_PORT", "27017"))
s_DOCDB_SCHEMA = os.environ.get("CDA_DOCDB_SCHEMA")
s_DOCDB_USER = os.environ.get("CDA_DOCDB_USER")
s_DOCDB_PASS = os.environ.get("CDA_DOCDB_PASS")

s_COLL_SCRAPED_NATIONAL = "rent_us"
s_COLL_SCRAPED = "zillow_scraped"
s_COLL_FAILURES = "zillow_scraped_failed"
s_COLL_PROPERTIES = "properties"
s_COLL_RENTALS = "rentals"
s_COLL_PREDICTIONS = "predictions"

td_MIN_MAX_TTL = timedelta(days=0, hours=1, minutes=0)
e_MIN_MAX_CACHE = {}

d_DEFAULT_PROPERTIES_MAX_DISTANCE = 80000


def prune_min_max_cache():
    dt_now = datetime.utcnow()
    i = 0
    for t_key, (e_min_max, dt_exp) in list(e_MIN_MAX_CACHE.items()):
        i += 1
        if dt_now > dt_exp:
            del e_MIN_MAX_CACHE[t_key]
        if i >=100: break


def _delete_records_by_id(o_collection, a_ids, s_id_col="_id", **kwargs):
    assert o_collection
    assert s_id_col
    if a_ids:
         o_collection.delete_many({s_id_col: {"$in": a_ids}})

def _update_records_by_id(o_collection, a_updates, s_id_col="_id", **kwargs):
    assert o_collection
    assert s_id_col
    if a_updates:
    
        a_bulk_ops = []
        for v_id, e_updates in a_updates:
            
            a_bulk_ops.append(
                pymongo.UpdateOne(
                    {s_id_col: v_id},
                    {"$set": e_updates},
                ))
        if a_bulk_ops:
            o_collection.bulk_write(a_bulk_ops)


def _yield_mongo_batch(collection, match, batch_size, **kwargs):
    z_batch = collection.find(
        match,
        batch_size=750,
        limit=kwargs.get("limit", 0),
    )
    i = 0
    a_batch = []
    while True:
        try:
            a_batch.append(z_batch.next())
            if len(a_batch) >= batch_size:
                #print(i)
                yield a_batch
                a_batch = []
                sleep(2)
        except Exception as e:
            print(e)
            break
    return a_batch


def copy_between_collections(o_source, o_target, e_filter, i_copy_batch=500, a_unique_ids=[], b_drop_id=True, **kwargs):
    a_batch = []
    i_batch = 0
    e_total_ids = {}
    e_failed_ids = {}
    e_batch_ids = {}
    if a_unique_ids:
        e_total_ids = defaultdict(set)
        e_failed_ids = defaultdict(set)
        e_batch_ids = defaultdict(set)
    
    f_load = kwargs.get("load_function")
    f_close = kwargs.get("close_function")
    
    if f_load:
        sleep(0.5)
        f_load()
    
    for e_doc in o_source.find(e_filter, limit=0):
        b_already_existing = False
        for v_unique in a_unique_ids:
            
            if isinstance(v_unique, str):
                v_key = e_doc.get(v_unique)
            elif isinstance(v_unique, (list, tuple)):
                v_key = tuple(e_doc.get(x) for x in v_unique)
            
            if v_key in e_total_ids[v_unique] or v_key in e_batch_ids[v_unique]:
                b_already_existing = True
                break
            e_batch_ids[v_unique].add(v_key)
        if b_already_existing: continue
        
        if b_drop_id:
            del e_doc["_id"]
        
        a_batch.append(InsertOne(e_doc))
        if len(a_batch) >= i_copy_batch:
            i_batch += 1
            try:
                print(f"copying batch {i_batch:,}")
                if f_load:
                    sleep(1)
                    f_load()
                    sleep(1)
                
                o_target.bulk_write(a_batch)
    
                #if f_close: f_close(**kwargs)
    
                for v_unique in a_unique_ids:
                    e_total_ids[v_unique].update(e_batch_ids.get(v_unique, set()))
                e_batch_ids = defaultdict(set)
                a_batch = []
            except Except as e:
                s_trace = ''.join(traceback.format_exception(None, e, e.__traceback__))
                print("\tfailed to copy batch")
                print(s_trace)
                for v_unique in a_unique_ids:
                    e_failed_ids[v_unique].update(e_batch_ids.get(v_unique, set()))
    
    if a_batch:
        i_batch += 1
        try:
            print(f"copying batch {i_batch:,}")
            if f_load:
                sleep(1)
                f_load()
                sleep(1)
            
            o_target.bulk_write(a_batch)
    
            #if f_close: f_close()
    
            for v_unique in a_unique_ids:
                e_total_ids[v_unique].update(e_batch_ids.get(v_unique, set()))
            e_batch_ids = defaultdict(set)
            a_batch = []
        except Except as e:
            s_trace = ''.join(traceback.format_exception(None, e, e.__traceback__))
            print("\tfailed to copy batch")
            print(s_trace)
            for v_unique in a_unique_ids:
                e_failed_ids[v_unique].update(e_batch_ids.get(v_unique, set()))
                
    if f_close: f_close()
    return {
        "copied": e_total_ids,
        "failed": e_failed_ids}


def _find_duplicate_on_id(o_collection, s_id_field, s_date_field, **kwargs):
    a_duplicate_ids = list(o_collection.aggregate([
        {"$group" : { "_id": f"${s_id_field}", "count": { "$sum": 1 } } },
        {"$match": {"_id" :{ "$ne" : None } , "count" : {"$gt": 1} } }, 
        {"$project": {s_id_field : "$_id", "_id" : 0, "count": 1} }
    ]))
    a_duplicate_ids = [
        e_dupe[s_id_field]
        for e_dupe in a_duplicate_ids]
    print(f"Found {len(a_duplicate_ids):,} {s_id_field} with duplication")
    return a_duplicate_ids


def _deduplicate_on_id(o_collection, s_id_field, s_date_field, **kwargs):
    a_duplicate_ids = list(o_collection.aggregate([
        {"$group" : { "_id": f"${s_id_field}", "count": { "$sum": 1 } } },
        {"$match": {"_id" :{ "$ne" : None } , "count" : {"$gt": 1} } }, 
        {"$project": {s_id_field : "$_id", "_id" : 0, "count": 1} }
    ]))
    a_duplicate_ids = [
        e_dupe[s_id_field]
        for e_dupe in a_duplicate_ids]
    print(f"Found {len(a_duplicate_ids):,} {s_id_field} with duplication")
    
    a_duplicates = list(o_collection.find(
        {s_id_field: {"$in": a_duplicate_ids}}
    ).sort([(s_id_field, pymongo.ASCENDING), (s_date_field, pymongo.DESCENDING)]))
    
    c_ids = set()
    a_to_delete = []
    for e_record in a_duplicates:
        #print(e_record[s_id_field], e_record[s_date_field])
        if e_record[s_id_field] in c_ids:
            a_to_delete.append(e_record["_id"])
        else:
            c_ids.add(e_record[s_id_field])

    print(f"{len(a_to_delete):,} records to delete")
    
    if a_to_delete:
        #o_delete_result = o_collection.delete_many({"_id": {"$in": a_to_delete}})
        a_delete_ops = [pymongo.DeleteOne({"_id": x}) for x in a_to_delete]
        o_collection.bulk_write(a_delete_ops)
    return a_to_delete


def get_subset_metrics(o_collection, e_filter, a_metrics, a_aggs=["min", "max"], **kwargs):
    e_metrics = {
        "_id":None,
        "count":{"$sum":1},
    }
    a_aggs = [re.sub(r"\$", "", s_agg) for s_agg in a_aggs]
    
    for s_col in a_metrics:
        e_col_aggs = {
            f"{s_col}_{s_agg}": {f"${s_agg}": f"${s_col}"}
            for s_agg in a_aggs}
        if e_col_aggs:
            e_metrics.update(e_col_aggs)
    
    o_cursor = o_collection.aggregate(
        [
            {"$match": e_filter},
            {"$project": {x: 1 for x in ["_id"] + a_metrics}},
            {"$group": e_metrics},
        ]
    )
    return list(o_cursor)


class DocDBConnector(object):
    def __init__(self, **kwargs):
        if kwargs.get("read_only") and s_DOCDB_HOST_READONLY:
            self._host = kwargs.get("host", s_DOCDB_HOST_READONLY)
        else:
            self._host = kwargs.get("host", s_DOCDB_HOST)
        self._port = int(kwargs.get("port", i_DOCDB_PORT))
        self._user = kwargs.get("user", s_DOCDB_USER)
        self._password = kwargs.get("password", s_DOCDB_PASS)
        self._schema = kwargs.get("schema", s_DOCDB_SCHEMA)
        self._ca_file = kwargs.get("ca_file", s_DOCDB_CA_FILE)
        
        assert self._host
        assert self._port
        assert self._user
        assert self._password
        assert self._schema
        assert self._ca_file and os.path.isfile(self._ca_file)
        
        self._ssh_host = kwargs.get("tunnel_host", os.environ.get("CDA_TUNNEL_HOST"))
        self._ssh_user = kwargs.get("tunnel_user", os.environ.get("CDA_TUNNEL_USER"))
        self._ssh_pkey = kwargs.get("tunnel_key", os.environ.get("CDA_TUNNEL_PKEY"))
        
        if self._ssh_host:
            assert self._ssh_user
            assert self._ssh_pkey and os.path.isfile(self._ssh_pkey)
        
        self._tunnel, self._client, self._db = None, None, None
        self._loaded = False
        
        self._ttl = kwargs.get("connection_ttl", 30)
        self._connect_expire = datetime.utcnow() - timedelta(minutes=self._ttl)
    
    def _load_tunnel(self, **kwargs):
        if self._tunnel is not None:
            self._tunnel.stop()
        
        
        if self._ssh_host:
            self._tunnel = sshtunnel.open_tunnel(
                (self._ssh_host, 22),
                ssh_username=self._ssh_user,
                ssh_pkey=self._ssh_pkey,
                #ssh_private_key_password=s_SSH_PKEY_SECRET,
                remote_bind_address=(self._host, self._port),
                local_bind_address=('0.0.0.0', self._port),
            )
            self._tunnel.start()
    
    def _load(self, **kwargs):
        if self._loaded and self._db is not None and (isinstance(self._connect_expire, datetime) and self._connect_expire > datetime.utcnow()): return
        
        if kwargs.get("b_debug"): print('opening connection to documentdb')
        if self._ssh_host:
            self._load_tunnel(**kwargs)
            s_host = "127.0.0.1"
        else:
            s_host = self._host
        
        self._client = pymongo.MongoClient(
            f'mongodb://{self._user}:{quote(self._password)}@{s_host}:{self._port}/?authMechanism=DEFAULT&tls=true&tlsCAFile={quote(self._ca_file)}&retryWrites=false',
            tls=True,
            tlsAllowInvalidCertificates=True,
            tlsCAFile=self._ca_file,
            connect=True,
            directConnection=True,
            read_preference=ReadPreference.SECONDARY_PREFERRED,
        )
        self._db = self._client[self._schema]
        self._loaded = True
        self._connect_expire = datetime.utcnow() + timedelta(minutes=self._ttl)
    
    def _close(self, **kwargs):
        if kwargs.get("b_debug"): print('closing connection to documentdb')
        self._loaded = False
        self._connect_expire = datetime.utcnow() - timedelta(minutes=self._ttl)
        
        
        if self._tunnel is not None:
            self._tunnel.stop()
        self._tunnel = None
        self._client = None
        self._db = None
    
    #def __enter__(self):
    #    self._load()
    
    def __exit__(self, exc_type, exc_value, traceback):
        self._close()

##### End DocDBConnector #####


def get_subset_value(o_collection, e_filter, s_metric_col, b_reverse=False, **kwargs):
    n_start = time()
    if s_metric_col not in e_filter:
        e_filter[s_metric_col] = {"$ne": None}
    #print("o_collection", type(o_collection))
    a_search = list(o_collection.find(
        e_filter,
        projection=[s_metric_col],
        #read_preference=ReadPreference.SECONDARY_PREFERRED,
        #collation=Collation(locale='en', strength=1),
    ).sort(s_metric_col, pymongo.DESCENDING if b_reverse else pymongo.ASCENDING).limit(1))
    e_search = a_search[0] if a_search else {}
    v_val = e_search.get(s_metric_col) if e_search else None
    
    if "metrics" in kwargs:
        kwargs["metrics"][f"{s_metric_col}_max" if b_reverse else f"{s_metric_col}_min"] = v_val
        
    #print(s_metric_col, utils.secondsToStr(time() - n_start))
    return v_val


def get_subset_count(o_collection, e_filter, **kwargs):
    i_count = o_collection.count_documents(e_filter)
    if "metrics" in kwargs:
        kwargs["metrics"]["count"] = i_count
    return i_count


def combine_min_max_metrics(*a_metric_sets):
    if not a_metric_sets: return a_metric_sets
    e_combo = a_metric_sets[0].copy()
    for e_set in a_metric_sets[1:]:
        for k, v2 in e_set.items():
            v1 = e_combo.get(k)
            #print(k, v2, v1)
            
            if v2 is None:
                if v1 is None:
                    e_combo[k] = v2
                    continue
            else:
                if v1 is None or (k.endswith("_min") and v2 < v1) or (k.endswith("_max") and v2 > v1):
                    e_combo[k] = v2

    return e_combo


def get_min_max_mp(o_collection, e_filter, a_metrics, **kwargs):
    a_threads = []
    e_metrics = {}
    
    a_threads.append(
        threading.Thread(
            target=get_subset_count,
            args=(o_collection, e_filter,),
            kwargs={"metrics": e_metrics}
        )
    )
    
    for s_metric_col in a_metrics:
        a_threads.append(
            threading.Thread(
                target=get_subset_value,
                args=(o_collection, e_filter, s_metric_col),
                kwargs={"metrics": e_metrics}
            )
        )
        a_threads.append(
            threading.Thread(
                target=get_subset_value,
                args=(o_collection, e_filter, s_metric_col),
                kwargs={"metrics": e_metrics, "b_reverse": True}
            )
        )
    
    #print("start threads")
    # start threads
    for t in a_threads:
        t.start()
    
    # wait for the threads to complete
    for t in a_threads:
        t.join()
    #print("finish threads")
    
    #return a_metrics
    return e_metrics
    

def get_property_metric_min_max(collection, e_filter, a_fields, **kwargs):
    if isinstance(collection, str):
        s_coll = collection
    else:
        s_coll = collection.name
    
    t_cache_key = (s_coll,) + tuple(sorted([(str(k), str(v)) for k, v in e_filter.items()]))
    e_metric_cache, dt_metric_cache_exp = e_MIN_MAX_CACHE.get(t_cache_key, ({}, None))
    #print('cache check', e_metric_cache, dt_metric_cache_exp )
    if dt_metric_cache_exp is not None and dt_metric_cache_exp > datetime.utcnow():
        prune_min_max_cache()
        return e_metric_cache
    
    o_docdb = kwargs.get("o_docdb")
    if o_docdb is None:
        global o_DOCDB
        if o_DOCDB is None:
            o_DOCDB = DocDBConnector(read_only=True)
        o_docdb = o_DOCDB
    
    o_docdb._load()
    #print("loaded")
    
    if isinstance(collection, str):
        collection = o_docdb._db[collection]
    
    #print(collection)
    e_metrics = get_min_max_mp(
        collection,
        e_filter,
        a_fields,
        **kwargs
    )
    o_docdb._close()
    
    e_MIN_MAX_CACHE[t_cache_key] = (e_metrics, datetime.utcnow() + td_MIN_MAX_TTL)
    
    return e_metrics


def get_property_nearness_score(e_prop, s_dist_key="distance", s_score_key="revr_score_overall", d_max_distance=d_DEFAULT_PROPERTIES_MAX_DISTANCE, b_invert_score=False, d_distance_proportion=0, **kwargs):
    d_distance = e_prop.get(s_dist_key, 0)
    if d_distance < d_max_distance:
        d_dist_score = ((d_max_distance - d_distance) / d_max_distance) ** 0.125
    else:
        d_dist_score = 0
        
    if s_score_key == s_dist_key:
        return d_dist_score
    
    d_score = e_prop.get(s_score_key, 0)
    if b_invert_score:
        d_score = 1 - b_invert_score
    
    if d_distance_proportion:
        d_combined_score = (d_score * (1 - d_distance_proportion)) + (d_dist_score * d_distance_proportion)
    else:
        d_combined_score = d_score * d_dist_score
        
    #e_prop["distance_score"] = d_combined_score
    return d_combined_score


def get_top_nearby_properties(collection, latitude, longitude, **kwargs):
    if isinstance(collection, str):
        s_coll = collection
    else:
        s_coll = collection.name
    
    o_docdb = kwargs.get("o_docdb")
    if o_docdb is None:
        global o_DOCDB
        if o_DOCDB is None:
            o_DOCDB = DocDBConnector(read_only=True)
        o_docdb = o_DOCDB
    
    o_docdb._load()
    #print("loaded")
    
    if isinstance(collection, str):
        collection = o_docdb._db[collection]
    
    e_nearest = {
        "sort": {},
        "max_distance_in_meters": (
            round(kwargs["max_distance_miles"] * 1609.344) if kwargs.get("max_distance_miles")
            else kwargs["max_distance_kms"] * 1000 if kwargs.get("max_distance_kms")
            else kwargs.get("max_distance", 80467)),
    }
    
    e_geonear = {
        "near": { "type": "Point", "coordinates": [ longitude , latitude ] },
        "distanceField": "distance",
        #"uniqueDocs": True,
        "maxDistance" : min(8000000, e_nearest["max_distance_in_meters"]),
        #"key": "gps",
        "spherical": True,
        "limit": min(1000, kwargs.get("max_search", 100)),
    }
    if kwargs.get("filter"):
        e_geonear["query"] = kwargs["filter"]
        
    a_aggregate = [
        {"$geoNear" : e_geonear},
        #{"$limit": kwargs.get("max_search", 100)},
    ]
    
    if kwargs.get("projection"):
        e_projection = kwargs["projection"]
        if "distance" not in e_projection:
            e_projection["distance"] = 1
        a_aggregate.append({"$project": e_projection})
    elif kwargs.get("fields"):
        e_projection = {field: 1 for field in kwargs["fields"]}
        if "distance" not in e_projection:
            e_projection["distance"] = 1
        a_aggregate.append({"$project": e_projection})
    
    #print("e_geonear", e_geonear)
    a_nearest = []
    try:
        a_nearest = list(collection.aggregate(a_aggregate))
        e_nearest["nearest"] = a_nearest
    except Exception as e:
        print("Failed to find nearby properties")
        s_trace = ''.join(traceback.format_exception(None, e, e.__traceback__))
        print(s_trace)
    o_docdb._close()
    
    if kwargs.get("max_distance_miles"):
        for e_prop in a_nearest:
            e_prop["distance"] /= 1609.344
    elif kwargs.get("max_distance_kms"):
        for e_prop in a_nearest:
            e_prop["distance"] /= 1000
    
    if kwargs.get("sorter"):
        e_nearest["sort"]["by"] = "sort_by"
        e_nearest["sort"]["default_value"] = kwargs.get("sort_default_value", 0)
        e_nearest["sort"]["reverse"] = kwargs.get("sort_reverse", False)
        
        for e_doc in a_nearest:
            e_doc[e_nearest["sort"]["by"]] = kwargs["sorter"](e_doc, **kwargs)
        a_nearest.sort(key=lambda e_prop: e_prop.get(e_nearest["sort"]["by"], e_nearest["sort"]["default_value"]), reverse=e_nearest["sort"]["reverse"])
        
    elif kwargs.get("sort_by"):
        e_nearest["sort"]["by"] = kwargs["sort_by"]
        e_nearest["sort"]["default_value"] = kwargs.get("sort_default_value", 0)
        e_nearest["sort"]["reverse"] = kwargs.get("sort_reverse", False)
        a_nearest.sort(key=lambda e_prop: e_prop.get(kwargs.get("sort_by"), e_nearest["sort"]["default_value"]), reverse=kwargs.get("sort_reverse", True))
    else:
        e_nearest["sort"]["by"] = "distance"
        e_nearest["sort"]["default_value"] = kwargs.get("sort_default_value", 999999)
        e_nearest["sort"]["reverse"] = kwargs.get("sort_reverse", False)
        if e_nearest["sort"]["reverse"]:
            a_nearest.sort(key=lambda e_prop: e_prop.get("distance", e_nearest["sort"]["default_value"]), reverse=e_nearest["sort"]["reverse"])
    
    return e_nearest
