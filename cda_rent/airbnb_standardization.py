﻿import os, sys, re, csv
from datetime import time as dt_time
from collections import defaultdict, Counter
import pandas as pd
import numpy as np
#import FwFreq

try:
    from . import utils
except ImportError:
    import utils


e_DESCRIPTION_SEARCHES = {
    #house styles
    "Estate": r"((?<!real )estate\b|(acre|private|country|waterfront|luxury|grand|beautiful|legacy|gorgeous|secluded|gated|oceanfront|lakefront|beach|beachfront|modern|historic|large) estate|palace|estate w(ith)? pool|estate near|castle|manor|mansion)",
    "Condo": r"(condo|condominium|condos|resident'?s'?[- ]*lounge)",
    "Rancher": r"(rancher|ranch house|cowboy house)",
    "Farmhouse": r"(farm[- ]*house)",
    "Apartment": r"(apartment|\bapt\b)",
    "Cottage": r"(cottage|bungalow|dacha|summer[- ]*house)",
    "Cabin": r"(((log[- ]*)?cabin|log[- ]*home|log[- ]*house)|chalet|shack|hut|shanty)",
    'Chateau': r"(chateau|country[- ]house'|vacation[- ]home|villa|château)",
    'Tiny house': r"tiny[- ]*house",
    'Townhouse': r"town[- ]*house",
    'Earth house': r"earth[- ]*house",
    'House': r"(home|dwelling|model[- ]*home|house|(one|two|three|1|2|3)[- ]*story)",
    'Duplex': r"(duplex|(2|two|dual|twin|multi)[- /]*((\d+|one|two|three|four|five|six)[- /]*be?d(r(oo)?m)?s?[- /]*)?(family|unit|apartment|suite))",
    
    "guesthouse": r"((guest|maid)('?s)?[- /_]*(house|cottage)|(M|Mother)-in-law[- /_]*(house|apt))",
    
    "new_construction": (
        r"(new[- ]+(construction|build|built)|newly[- ]+(built|constructed|finished|completed)|new[- ]*(house|condo|single|one|two|three|1|2|3)[- ]*(story|bed|room))"
    ),
    "fixer_upper": (
        r"(fixer[- /]*upper|fix[- /]*up"
        r"|needs?[- ]*((some|a little|lots of|a lot|tons of|a ton of|complete|total|full|major|top[- ]*to[- ]*bottom|needs? ground[- ]*up)[- ]*)*(love|tlc|help|work|repair|restoration|re[- ]?hab|re[- ]?model|renovation|construction)"
        r"|(repairs?|tlc|love|work|fixing|helped|restoration|re[- ]?hab|re[- ]?model|renovation)[- ](needed|project|special)"
        r"|need(s?|[- ]of)[- ](repairs?|fixing|work|tlc)|revitalize|tlc|sweat[- ]?equity|flip"
        r"|(structural|electrical|plumbing) (issues|problems)|this fixer|bring your toolbelt|under(going)?[- ]?repair"
        r"|rehabber|(rehabilitation|construction)([- ]?loan|- ]?project|((wrapping|finishing)[- ]*up|under( [a-z]+)?|of)[- ]+construction)"
        r"|restoration potential|expensive and arduous|value add property|termite(?!=(bond|service|control))|(flood|fire|tornado|hurricane)[- _/]*damage|had a fire|tear[- ]*down"
        r"|project[- ]*(home|house)|investment|investor|\barv\b|(finish( up)?|take over|complete the) (the )?(reno|rehab|re-?store|re-?build|re-?construct|repair|work)"
        r"|handy[- ]*person|hand[iy][- ]*man" #|attention[- ]*to[- ]*detail"
        r"|(to|be|will|needs?|in|during) +(renovat|rehab|restoration)|add[- ]*value[- ]*to"
        #r"|(^|\b)as[- /]+is(\b|$)|current[- ]*condition|no[- ]*warrant(y|ies))"),
        r"|(^|\b)as[- /]+is condition|(move[- ]*in|sold|rent(ed)?|listed|showing)[- /]+\"?as[- /]+is\"?|no[- ]*warrant(y|ies))"),
    "condemned": r"(condemned|(not|un|in)[- ]*inhabitable|not[- ]liveable|demo work"
        r"|needs?( a)? (total|complete|full|top[- ]to[- ]bottom|lot of|ton of|under|ground[- ]*up)[- ]*(renovation|restoration|rebuild|rework|rehabilitation|refurbish)"
        r"|being[- ]*(re-?novated|restored|re-?built|re-?worked|re-?habilitated)"
        r"|(structural|foundation)[- ]*(concerns|problems|issues)|(un|not)[- ]*safe[- ]*(to[- ]*(enter|live|move)|for[- ]*(entry|living|dwelling|inhabit))"
        r"|(severe|serious)[a-z -]*damage|debris|(de|missing|lacking|salvage)[- ]*titled?|gutted|start(ing)[- ]*from[- ]*scratch|needs? ground[- ]*up|divide([- ]*up)?[- ]*the[- ]*lot)",
    "renovated": r"((mint|platinum)[- ]*condition|(fully|newly|completely|totally|just|recent(ly)?|professionally|tastefully|beautiful(ly)?|been|extensively)[- ](re-?modeled|renovated|repaired|refurbished)"
        r"|((re-?modeled|renovated|repaired|refurbished)[- _]?(home|house|cabin|cottage|condo|apartment|farmhouse|bungalow|chateau|chalet|villa|complete|done|finished|(1|2|3|4|one|two|three|four)[- ]*(bed|room))))",
    "manufactured_home": r"(manu(\.?|f\.?|factured?)?[- _]*(\d+[- _]*be?dr(oo)?ms?[- _]*)?(home|house|housing)"
        r"|(mfr\.?|mobile|trailer)[- _]?(home|house)|(trailer|mobile home|rv|recreational vehicle)[- /]*park(?!=ing)"
        r"|(champion|clayton|fleetwood|skyline|adventure|tru|palm[- ]*harbor|oak[- _]*creek|silver[- _]*crest|kit[- _]*custom|sunshine|cavco|franklin|meridian|friendship)[- _]*(home(s|builder)|model|design|#)"
        r"|deer valley|cappaert manufactured)",
    "starter_home": r"starter[- ]*(home|house)",
    "auction": r"((at[- _]|for[- _]|real[- _]*estate)auction|auction[- _](date|day)|[*]+auction[*]+a"
        r"|price for (the )?auction|(this|live|trust|probate|foreclosure|luxury) auction|foreclose"
        r"|\d+[- _]*(hour|hr\.?)[- _]*(home|house|fire|rush)[- _]*sale)",
    "shared_ownership": r"((shared|co)[- _]*ownership|share[- _]of[- _]ownership|part(ial|ly)[- _]own(ed|er)"
        r"|(1/\d+(th)?|fractional|one[- /]+(half|third|quarter|fifth|sixth|seventh|eigth|ninth|tenth))[- _]*(owner[- _]*ship|interest|share|joint)"
        r"|co[- ]*(own|op\b|investment|venture)|joint[- ]*(own|op|investment\b|venture)|room[- ]*mate)",
    "timeshare": r"(time[- _]*share|week[- _]*\d+|(1st|2nd|3rd|[4-9]th)[- _]*week|annual (\d+,)*\d+ points|(\d+,)*\d+ annual points|vacation club|fixed[- _]*weekend"
        r"|part[- /]*time[- /]*(deeded[- ]*)?owner[- ]*ship|gift[- _]*shop|hotel[- ]*bar|each share|weeks?[- _]*(per|a|each)[- _]*year"
        r"|owner'?s?[- /]*rate|any (1|2|3|4|one|two|three|four)[- ]*week[- ]*period)",
    "move_home": r"(home has to be moved|must move home|move the home|(home|cabin)[- _/]*kit|kit[- _/]*(home|cabin)"
        r"|no[- ]*(property|parcel|land|lot)[- ]*included|(property|parcel|land|lot)[- ]*not[- ]*included"
        r"|for[- ]*(home|house|building|cabin|chalet|structure)[- ]*only"
        r"|does[- ]*n[o']t include (the )?(amount of|cost of|price of|purchas(e|ing)|buying|sale of) (land|lot|site)|does[- ]*n[o']t include (the )? (land|lot|site) (cost|price|amount))",
    "lot_only": r"(ready[- _]to[- _]build|lot[- _]*only|(empty|vacant|unimproved)[- _]*lot\b|doesn't include (land|lot|property|plat)|(land|lot|property|plat) not included|no (land|lot|property|plat) included)",
    "potential_issues": r"(historical[- ]*(committee|society|property|landmark)|no purchase contingencies|solely responsible|(^|\b)as[- /]+is(\b|$)|current[- ]*condition"
        r"|potential|possibilities|outstanding[- ]*(condo(minium)?|h\.?o\.?a\.?|asso?c(iation)?)[- ]*(dues|fees)|\blien\b"
        r"|assess(ment|ed)[- ]*balance|missing|due[- ]*diligence|homeless|disability|cash|only)",
    "no_rentals": r"(no[- ]*(short[- ]*term|over[- ]*night|monthly|month[- ]*to[- ]*month|sub[- ]*let)*[- ]*rental|air[- ]*bnb"
        r"|rent(als?_int)[- ]*(not[- ]*allow|not[- ]*permit|forbid|prohibit)|(forbidden|prohibited|not[- ]*(allowed|permitted))[- ]*to[- ]*rent|(storage|garage|shed)[- ]*only)",
    "short_term_rental": r"(short[- ]*term([- ]+rental)?[- ]+only|\$\d(\.\d+)?[- /]*((per|a)[- ]+(day|night|week)|daily|weekly)|(daily|weekly)[- ]*(rent|rate|sub-?let)"
        r"only[- ]*short[- ]*term|short[- ]*term[- ]*only|shorter[- ]*term|short[- ]*term rental|short term or one year lease"
        r"|not (available|allowed|permitted|to be used) for short(er)?[- /]*term rental"
        r"|rental for the month of|for the season|ski lease"
        r"|for a working vacation|cancel after|cancel before|experience package tickets"
        r"|(available )?(january|february|march|april|may|june|july|august|september|october|november|december)[- ]*(|through|thru)?[- ]*(january|february|march|april|may|june|july|august|september|october|november|december)"
        r"20\d\d season|(summer|spring|fall|winter)[- ]*(20\d\d[- ]*)?rental|\d+[- ]*day[- ]*minimum|seasonal[- ]*guests"
        r"seasonal[- ]*lease)",
    "rent_control": r"(rent[- ]*control|income[- ]*restriction|based[- ]*on[- ]*income|sect(ion)?[- /+]*(8|eight)|senior[- /+]*living|55 *\+ *(community|condo|assoc|subdivis))",
    "pets_allowed": r"((dog|pet|animal)[- ]*(allowed|ok|friendly)|\+dog|\+pet)",
    
    "feature_floorplan_open": r"(open[- ]?(floor[- ]?)?plan|(floor[- ]?)?plan[- ]*open)",
    "feature_floorplan_traditional": r"(trad(itional|\.)?[- ]?(floor[- ]?)?plan|(floor[- ]?)?plan[- ]*trad(itional|\.)?)",
    "feature_floorplan_inlaw": r"((in[- ]?law|maid|guest)('s)?[- ]*((floor[- ]?)?plan|quarters|suite|apt|apartment)|(additional|addl)[- ]*living[- ]*(area|quarters))",
    "feature_floorplan_split": r"(split[- ]*(bed(room)?|br)?[ -]*(floor[- ]?)?plan|(floor[- ]?)?plan[- ]*split)",
    "feature_floorplan_roommate": r"(room[- ]?mate[- ]?(floor[- ]?)?plan|(floor[- ]?)?plan[- ]*room[- ]?mate)",
    
    "feature_jacuzzi": r"(jacuzzi|hot[- _]?tub|whirl[- _]?pool|jetted[- _]?pool|\bspa\b)",
    "feature_sauna": r"(sauna|steam[- ]*room)",
    "feature_kitchenette": r"kitchenette",
    
    "feature_pool_table": r"(pool[- ]?table|billiard)",
    "feature_bbq_area": r"(built[- _]?in[- _]?(bbq|barbeque|grill)|gas (stub )?for (bbq|barbeque|grilling)|(fire|bbq|barbeque|grilling|grill)[- _]*(area|pit))",
    "feature_elevator": r"elevator",
    "feature_wheelchair_accessible": r"(wheel[- _]?chair|handi[- _]?cap)[- _]?(access(ible)?|friendly)",
    
    "feature_ev_charger": r"(e-?v|e\.v\.|electric[- _]?vehicle|electic[- _]?car)[- _]?(charger|charging|station)",
    
    "feature_garden_or_backyard": r"(garden|back[- _]?yard|enclosed[- _]?yard)",
    "feature_gated_community": r"((gated?|secured?|fenced-?in|guarded|private)[- ]*community|guard(ed)?[- ]*gates?|country[- ]*club"
        r"|private[- _]?enclave"
        r"|(24[- /]*h(ou)?r|full[- ]*time|round[- ]*the[- ]*clock)[- /]*((private[- ]*)?security|guards?))",
    "feature_trees_fruit_nut": r"((fruit|apple|orange|pear|plum|lemon|cherry|nut|pecan|walnut)[- ]trees?)",
    "feature_lawn_sprinkler": "(spr(in)?kler|auto[- ]*timer[- ]*(h2o|water)|wa?te?ring Sys|(?<!flood[- ])irrigation|spr[- ]*sys)",
    "feature_patio_or_balcony": r"(patio|balcony|deck|porch|veranda|terrace|lanai)",
    "feature_loft": r"loft",
    "feature_gazebo": r"(gazebo|cabana)",
    "feature_flower_beds": r"(flower|raised|garden)[- ]*beds",
    "feature_fountain": r"fountain",
    
    "feature_wireless_internet": r"(wi[- ]?fi|wireless)",
    "feature_parking": r"parking",
    "feature_garage": r"garage",
    "feature_ac": r"(air[- ]*conditioner|a\.c\.)",
    
    "feature_closet_walkin": r"(walk[- ]*in[- ]*closet|wa?lk-?in-?cls)",
    "feature_closet_cedar": r"(cedar([- ]*wood)?[- ]*closet|closet[- ]*cedar)",
    "feature_closet_dual": r"(2\+?|two|twin|dual|double|his([-/ ]|[- ]*and[- ]*)hers|hers([-/ ]|[- ]*and[- ]*)his)closet",
    "feature_attic": r"\battic",
    "feature_pantry": r"pantry",
    "feature_pantry_walkin": r"(walk[- ]*in[- ]pantry|pantry[- /]*walk[- ]*in)",
    "feature_book_shelves": r"book[- ]*(shel(f|ves)|cases?)",
    
    "feature_counters_stone": r"((stone|granite|quartz|quartzite|marbled?|slate|onyx|limestone|soapstone)[- ]*counter([- ]?top)?s?|"
        r"counter([- ]?top)?s?[^a-z0-9]*(stone|granite|quartz|quartzite|marbled?|slate|onyx|limestone|soapstone)"
        r"|^(granite|marble|quartz)$)",
    "feature_counters_upgraded": r"("
        r"(upgraded?|updated?|improved?|stainless|steel|concrete|recycled[- ]*glass|tile|corian|porcelain|ceramic|solid([- ]*surface)?|non[- ]?laminated?)[- ]*counter([- ]?top)?s?"
        r"|counter([- ]?top)?s?[- /()]*([a-z]+[- .]*)?(upgraded?|updated?|improved?|stainless|steel|concrete|recycled[- ]*glass|tile|porcelain|ceramic|solid([- ]*surface)?|non[- ]?laminated?))",
    "feature_counters_wood": r"((wood|butcher|butcher[- ]block)[- ]*counter([- ]?top)?s?|counter([- ]?top)?s?[- /()]*(wood|butcher|butcher[- ]block))",
    "feature_counters_metal": r"((metal|(stainless[- ]*)steel|stainless|copper|brass)[- ]*counter([- ]?top)?s?|counter([- ]?top)?s?[- /()]*(metal|(stainless[- ]*)steel|stainless|copper|brass))",
    
    "feature_cabinets_wood": r"((hard[- ]?wood|solid[- ]wood|oak|maple|cherry|walnut)[- ]*cabinet|cabinets?[- /()]*(hard[- ]?wood|solid[- ]wood|oak|maple|cherry|walnut))",
    "feature_kitchen_island": r"((kitchen|food[- ]*prep(ping)?|cook(ing)?|quartz|granite|center|eat[- ]?in)[- ]island|^island$|island work)",
    "feature_wet_bar": r"((?<!breakfast )bar\b|wet[- /]dry[- ]?bar|wet[- ]?bar|dry[- ]?bar)",
    "feature_wine_storage": r"wine[- ]*(rack|storage|cellar|room)",
    "feature_wine_chiller": r"wine[- /]*(beverage[- /]*)?(refrigerator|chiller|cooler|fridge)",
    
    "feature_bathroom_whirlpool": r"((jet(ted)?|hydro)[- ](tub|bath)|whirlpool)",
    "feature_bathroom_roman_tub": r"(roman|deep)[- ]*(bath)?tub",
    "feature_bathroom_garden_tub": r"(garden|wide)[- ]*(bath)?tub",
    "feature_bathroom_dual_sink": r"((2|two|twin|dual|double)[- ]*(sink|vanity|basin|wash[- ]*basin)|(sink|basin|vanity)[- ]*(2|two|twin|dual|double))",
    "feature_bathroom_jack_jill": r"jack[- ]*('?n'?|and|&|/)[- ]*jill",
    "feature_bathroom_stone": r"((bath(room)?|shower|shwr)[- ]*(marble|granite|quartz|stone)|(marble|granite|quartz|stone)[- ]*(in[- ]*)?(bath(room)?|shower|shwr))",
    "feature_plumbing_copper": r"(copper[- ]*plumbing|plumbing[- ]*copper)",
    "feature_water_softener": r"water[- ]*softener",
    "feature_sump_pump": r"sump[- /]*pump",
    
    
    "feature_high_ceilings": r"(ceil(ing)?s?[- ]*(9|10|11|12)\+? *('|ft|foot|feet|m\.|meter) *\+?"
        r"|(9|10|11|12)\+?[- ]*('|’|ft|foot|feet|m\.|meter)?[- ]*\+?([- ]high|[- ]flat|[- ]smooth)*[- ]*ceil(ing)?s?"
        r"|(2|two)[- ]*story[- ]*ceil(ing)?s?|(high|volume)[- ]*ceil(ing)?s?"
        r"|(towering|soaring)[- ]*ceiling)",
    "feature_ceilings_vaulted": r"((vault(ed)?|cathedral|ctdrl)[- ]*ceil(ing)?s?|ceil(ing)?s?[- ]*(vault(ed)?|cathedral|ctdrl)|cathedral[- /]*vault(ed)?|vault(ed)?[- /]*cathedral)",
    "feature_ceilings_beamed": r"(beam(ed)?[- ]*ceil(ing)?s?|(exposed|visible|open)[- ]*beams)",
    "feature_ceilings_tray": r"(tr[ea]y[- ]*ceil(ing)?s?|ceil(ing)?s?[- ]*tr[ea]y|\btrey\b)",
    "feature_ceilings_coffered": r"(coffer(ed)?[- ]*ceil|ceil(ing)?s?[- ]*coffer(ed)?)",
    "feature_ceilings_smooth": r"(smooth[- ]*ceil(ing)?s?|ceil(ing)?s?[- ]*smooth)",
    
    "feature_lighting_recessed": r"(recess(ed)?[- ]*light(ing|s)|light(ing|s)[- /:()]*recess(ed)?)",
    "feature_lighting_track": r"(track(ed)?[- ]*light(ing|s)|light(ing|s)[- /:()]*track(ed)?)",
    "feature_lighting_decorative": r"(chandelier|candelabra|decorative[- ]*light(ing|s)|light(ing|s)[- /:()]*decorative)",
    "feature_lighting_skylight": r"(sky[- ]*light|solar[- ]*tube)",
    "feature_lighting_overhead": r"(over[- ]?head[- ]*light(ing|s)|light(ing|s)[- /:()]*over[- ]?head)",
    "feature_lighting_fixtures": r"(fixtures?[- ]*light(ing|s)|light(ing|s)[- /:()]*fixtures?)",
    
    "feature_windows_bay": r"bay[- ]*window",
    "feature_windows_full_height": r"(floor[- ]*to[- ]*ceiling[- ]*window|(full[- ]*height|over-?sized?)[- ]*window)",
    "feature_windows_stained": r"(stain(ed)?[- ]*glass|stain(ed)?[- ]*window)",
    "feature_windows_treatments": r"(window[- ]*(treatment|covering)?|drapes|blinds)",
    
    "feature_doors_french": r"french[- ]*doors?",
    "feature_doors_sliding_glass": r"sliding[- ]*glass[- ]*doors?",
    
    "feature_penthouse": r"penthouse",
    "feature_powder_room": r"powder[- ]*room",
    "feature_basement": r"basement",
    "feature_basement_finished": r"(finished[- ]*basement|"
        r"(basement|below[- ]*grade|sub[- ]*grade)[- ]*(bed(room)?|suite|bdrm|master|mstr|mbr|br|br/ba|ba|bath(room)?)s?"
        r"|((bed(room)?|bdrm|master|mstr|mbr|br|br/ba|ba|bath(room)?)s?[- ]*)(in[- ]*)?(basement|below[- ]*grade|sub[- ]*grade))",
    "feature_mud_room": r"mud[- ]*room",
    "feature_utility_room": r"utility[- ]*room",
    "feature_workshop": r"work[- ]*shop",
    "feature_sun_room": r"((sun|florida|garden|patio)[- ]*room|solarium|atrium|sun[- ]*porch|winter[- ]*garden|conservatory)",
    "feature_sitting_room": r"(sitting[- ]*(room|area)|lounge)",
    "feature_bonus_room": r"(bonus([- /]*plus)?[- ]*room)",
    "feature_media_room": r"(media|home[- ]*theater|game|hobby)[- ]*room",
    "feature_rec_room": r"((rec(reation)?|exercise)[- ]*room|gym)",
    
    #####
    
    "location_water_view": r"((lake|ocean|river|sea|bay|sound)[- _]?(view|vista)s?"
        r"|(lake|ocean|river|sea|bay|sound)[- _]?(life|living)|(view|vista)s? of ([a-z]+ ){,3}(lake|ocean|river|sea|bay|sound))",
    "location_waterfront": r"((water|lake|ocean|river|bay|beach|shore|gulf)'?s?[- _]*(front|side|place|location|access|home|house|edge)"
        "|(boat|canoe|kayak|lake)[- _]?(dock|slip|pier|mooring|launch)|private[- _]?(beach|lake|dock|slip|pier)|(on|looking) ([a-z]+ ){,3}(lake|water|river|bay|beach)|riverbank)",
    "location_waterfalls": r"water[- _]?fall",
    
    "location_boating": r"(boat(ing)?|(jet|water)[- _]?ski|wind[- _]?surf|sail(ing|[- _]?boat)|kayak|ski[- _]?doo|canoe|water[- _]?ski|marina|harbor|jetty)",
    "location_fishing": r"fishing|trout|salmon",
    "location_golf": r"(golf|driving[- ]*range)",
    "location_horseback_riding": r"(horse[- ]*(back|trail|riding|stable|country)|equestrian)",
    "location_laundromat": r"(laundro[- _]?mat|coin[- _]?(operated[- _]?)?laundry)",
    "location_laundry": r"((site|free) laundry|laundry (room|machine|facilit(y|ies)))",
    "location_library": r"library",
    "location_mountain_view": r"((mtns?\.?|mountains?|ridge)[- _]?(view|vista)s?|(mtns?\.?|mountains?)[- _]?(life|living|magic|side|top|home|house|over|retreat|hide-?away)|(view|vista)s? of (the )?(mtns?\.?|mountains?))",
    "location_pool": r"(?<!whirl)pool(?! table)",
    "feature_pool": r"(w|with|private)[- /]*(heated|in-?ground)? *pool(?! table)",
    "location_restaurants": r"(restaurants?|fine[ -]*dining|eateries)",
    "location_shopping": r"(shopping|\bmall\b|stores|shops|boutique)",
    "location_skiing": r"(skiing|ski[- _]?slopes?|snow[- _]?board(ing)?|ski the|ski (cabin|lodge|chalet|resort|ski-in))",
    "location_tennis": r"tennis",
    "location_hospital": r"hospital",
    "location_downtown": r"downtown",
    "location_village": r"village",
    "location_rural": r"(secluded|forest|\bwoods\b|\bwooded|rural)",
    "location_winery_tours": r"(wine[- _]?(country|tour)|vineyard)",
    "location_museums": r"(museum|art[- _]?galler(y|ies)|galler(y|ies))",
    "location_playground": r"play[- _]?(ground|area)",
    "location_resort": r"resort",
    "location_sight_seeing": r"sight[- _]?seeing",
    "location_adventure_activities": r"(surfing|para[- _]?sailing|(para|hang)[- _]?gliding|hot[- _]?air[- _]?balloon|rafting|kayaking|skiing|roller[- _]?(blade|skate)|tubing)",
    #"location_wind_surfing": r"water[- _]?surf",
    "location_swimming": r"swim",
    "location_walking": r"(walking|easy[- _]?walk|walk to|steps away)",
    "location_parks": r"(botanical[- _]?garden|(quiet|local|neighborhood) park|disc[- _]?golf|frisbee|volley[- _]?ball)",
    "location_water_parks": r"water[- _]?park",
    "location_water_tubing": r"(water|inner)[- _]?tubing",
    "location_wildlife_viewing": r"(wild[- _]?life|national[- _]?park|state[- _]?park|whale|dolphin)",
    "location_zoo": r"\bzoo\b",
    "location_campus": r"(campus|university|college)",
    "location_recreation_center": r"(rec(reation)|fitness)?[- _]*center",
    "location_theme_parks": r"(disney([- _]?(world|land)?)|universal[- _]?studio|six[- _]?flags|magic[- _]?kingdom|silver[- _]?dollar[- _]?city|busch[- _]?gardens|lego[- _]?land|theme[- _]?park)",
    "location_hiking": r"(hiking|(park|forest|mountain|mtn\.?) trail|trail access|national[- _]?park|state[- _]?park)",
    
    "location_public_transportation": r"(public[- ]transit|city[- ]transit|(near(by)?|close to|on|walking[- ]*distance[- ]*(to|from))[- ]*(bus|train|metro|subway)|multiple forms of transportation)",
    
    "living_area": r"(\d+(,|\. ?))*\d+[-. /]*((sq\.?|square)[- /]*(ft|foot|feet|m\.(?! plot)|meter(?! plot)|\))|SF\b)",
    "lot_size": r"(\d+(,|\. ?))*\d+[-. _/]*(acre|ac\b|m\.? * plot|meter * plot)",
    
}

#e_DESCRIPTION_RE = {
#    s_tag: re.compile(s_search, flags=re.I)
#    for s_tag, s_search in e_DESCRIPTION_SEARCHES.items()
#}
e_DESCRIPTION_RE = {}
for s_tag, s_search in e_DESCRIPTION_SEARCHES.items():
    try:
        e_DESCRIPTION_RE[s_tag] = re.compile(s_search, flags=re.I)
    except:
        print(f"Couldn't compile description regex {s_tag}")


e_ADDRESS_SEARCHES = {
    "guesthouse": r"((guest|maid)('?s)?[- /_]*(house|cottage)|(M|Mother)-in-law[- /_]*(house|apt))",
    
    "condemned": r"(condemned|(not|un|in)[- ]*inhabitable|not[- ]liveable|demo work"
        r"|needs?( a)? (total|complete|full|top[- ]to[- ]bottom|lot of|ton of|under|ground[- ]*up)[- ]*(renovation|restoration|rebuild|rework|rehabilitation|refurbish)"
        r"|being[- ]*(re-?novated|restored|re-?built|re-?worked|re-?habilitated)"
        r"|(structural|foundation)[- ]*(concerns|problems|issues)|(un|not)[- ]*safe[- ]*(to[- ]*(enter|live|move)|for[- ]*(entry|living|dwelling|inhabit))"
        r"|(severe|serious)[a-z -]*damage|debris|(de|missing|lacking|salvage)[- ]*titled?|gutted|start(ing)[- ]*from[- ]*scratch|needs? ground[- ]*up|divide([- ]*up)?[- ]*the[- ]*lot)",
    "manufactured_home": r"(manu(\.?|f\.?|factured?)?[- _]*(\d+[- _]*be?dr(oo)?ms?[- _]*)?(home|house|housing)"
        r"|(mfr\.?|mobile|trailer)[- _]?(home|house)|(trailer|mobile home|rv|recreational vehicle)[- /]*park(?!=ing)"
        r"|(champion|clayton|fleetwood|skyline|adventure|tru|palm[- ]*harbor|oak[- _]*creek|silver[- _]*crest|kit[- _]*custom|sunshine|cavco|franklin|meridian|friendship)[- _]*(home(s|builder)|model|design|#)"
        r"|deer valley|cappaert manufactured)",
    "auction": r"((at[- _]|for[- _]|real[- _]*estate[- _]*)auction|auction[- _]*(date|day)|[*]+auction[*]+a"
        r"|price for (the )?auction|(this|live|trust|probate|foreclosure|luxury) auction|foreclose"
        r"|\d+[- _]*(hour|hr\.?)[- _]*(home|house|fire|rush)[- _]*sale)",
    "shared_ownership": r"((shared|co)[- _]*ownership|share[- _]of[- _]ownership|part(ial|ly)[- _]own(ed|er)"
        r"|(1/\d+(th)?|fractional|one[- /]+(half|third|quarter|fifth|sixth|seventh|eigth|ninth|tenth))[- _]*(owner[- _]*ship|interest|share|joint)"
        r"|co[- ]*(own|op\b|investment|venture)|joint[- ]*(own|op|investment\b|venture)|room[- ]*mate)",
    "timeshare": r"(time[- _]*share|week[- _]*\d+|(1st|2nd|3rd|[4-9]th)[- _]*week|annual (\d+,)*\d+ points|(\d+,)*\d+ annual points|vacation club|fixed[- _]*weekend"
        r"|part[- /]*time[- /]*(deeded[- ]*)?owner[- ]*ship|gift[- _]*shop|hotel[- ]*bar|each share|weeks?[- _]*(per|a|each)[- _]*year"
        r"|owner'?s?[- /]*rate|any (1|2|3|4|one|two|three|four)[- ]*week[- ]*period)",
    "move_home": r"(home has to be moved|must move home|move the home|(home|cabin)[- _/]*kit|kit[- _/]*(home|cabin)"
        r"|no[- ]*(property|parcel|land|lot)[- ]*included|(property|parcel|land|lot)[- ]*not[- ]*included"
        r"|for[- ]*(home|house|building|cabin|chalet|structure)[- ]*only"
        r"|does[- ]*n[o']t include (the )?(amount of|cost of|price of|purchas(e|ing)|buying|sale of) (land|lot|site)|does[- ]*n[o']t include (the )? (land|lot|site) (cost|price|amount))",
    "lot_only": r"(ready[- _]to[- _]build|lot[- _]*only|(empty|vacant|unimproved)[- _]*lot\b|doesn't include (land|lot|property|plat)|(land|lot|property|plat) not included|no (land|lot|property|plat) included)",
    #"potential_issues": r"(historical[- ]*(committee|society|property|landmark)|no purchase contingencies|solely responsible|(^|\b)as[- /]+is(\b|$)|current[- ]*condition"
    #    r"|potential|possibilities|outstanding[- ]*(condo(minium)?|h\.?o\.?a\.?|asso?c(iation)?)[- ]*(dues|fees)|\blien\b"
    #    r"|assess(ment|ed)[- ]*balance|missing|due[- ]*diligence|homeless|disability|cash|only)",
    #"no_rentals": r"(no[- ]*(short[- ]*term|over[- ]*night|monthly|month[- ]*to[- ]*month|sub[- ]*let)*[- ]*rental|air[- ]*bnb"
    #    r"|rent(als?_int)[- ]*(not[- ]*allow|not[- ]*permit|forbid|prohibit)|(forbidden|prohibited|not[- ]*(allowed|permitted))[- ]*to[- ]*rent|(storage|garage|shed)[- ]*only)",
}
e_ADDRESS_RE = {}
for s_tag, s_search in e_ADDRESS_SEARCHES.items():
    try:
        e_ADDRESS_RE[s_tag] = re.compile(s_search, flags=re.I)
    except:
        print(f"Couldn't compile address regex {s_tag}")

a_PROPERTY_TYPE_ORDER = [
    'Tiny house',
    'Earth house',
    "Condo",
    #"Guesthouse",
    "Farmhouse",
    'Townhouse',
    "Estate",
    "Duplex",
    "Cottage",
    "Cabin",
    'Chateau',
    "Rancher",
    "Apartment",
    "House",
]
c_PROPERTY_TYPE_ORDERS = set(a_PROPERTY_TYPE_ORDER)


e_PROPERTY_TYPES = {
    'House': ['House', 'Entire house', 'house', 'Entire residential home', 'Entire place', 'Entire home/apt', 'Place'],
    'Cabin': ['Cabin', 'Entire cabin', 'Chalet', 'Entire chalet', 'Lodge', 'lodge' ,'Nature lodge'],
    'Cottage': ['Cottage', 'Entire cottage', 'Bungalow', 'Entire bungalow'],
    'Townhouse': ['Townhouse', 'Townhome', 'townhome', 'Entire townhouse'],
    
    'Chateau': [
        'Chateau', 'Chateau / Country House', 'Country house/chateau',
        'Vacation home', 'Entire vacation home', 'Villa', 'Entire villa'],
    'Estate': ['Estate', 'Castle', 'Entire castle'],
    
    'Tiny house': ['Tiny house', 'Entire tiny house'],
    'Earth house': ['Earth House', 'Earth house', 'Entire earth house'],
    
    'Guesthouse': ['Guesthouse', 'Guest House', 'In-law', 'Entire guesthouse', 'Entire in-law'],
    
    'Bed and breakfast': [
        'Bed and breakfast', 'Bed & Breakfast', 'Bed And Breakfast', 'Bed &amp; Breakfast', 'Bed & breakfast',
        'Entire bed and breakfast', 'Entire bed & breakfast',
        'Private room in bed and breakfast', 'Private room in bed & breakfast', 'Room in bed and breakfast',
        'Hotel room in bed and breakfast',
    ],
    
    'Hotel': [
        'Hotel', 'Room in hotel', 'Private room in hotel', 'Hotel room in hotel',
        'Aparthotel', 'Room in aparthotel'],
    'Boutique hotel': ['Boutique hotel', 'Room in boutique hotel', 'Private room in boutique hotel', 'Heritage hotel (india)', 'Entire boutique hotel'],
    'Motel': ['Motel', 'Room in motel'],
    'Hostel': ['Hostel', 'Room in hostel', 'Hotel room in hostel', 'Private room in hostel', 'Entire hostel'],
    'Dorm': ['Dorm', 'Private room in dorm', 'Shared room in dorm', 'Entire dorm'],
    
    'Resort': ['Resort', 'Room in resort', 'entire resort'],
    'Timeshare': ['Timeshare', 'Entire timeshare', 'Private room in timeshare'],
    
    'Condo': ['Condo', 'Condominium', 'Entire condominium', 'condo', 'Entire condominium (condo)', 'Entire condo'],
    'Apartment': ['Apartment', 'apartment', 'Entire apartment', 'Serviced apartment', 'Entire serviced apartment', 'Entire rental unit'],
    'Studio': ['Studio'],
    
    'Camper/RV': ['Camper/RV', 'Recreational Vehicle', 'Entire camper/RV', 'Private room in camper/rv'],
    'Car': ['Bus', 'Van', 'Car', 'Train'],
    
    'Farmhouse': ['Farmhouse', 'Farm stay', 'Ranch'],
    'Barn': ['Barn'],
    
    #'Place': ['Place', 'Entire place'],
    'Other': ['Other'],
    'Parking Space': ['Parking Space'],
    
    'Boat': ['Boat', 'Yacht', 'Entire boat', 'Shared room in boat', 'Private room in boat'],
    'Plane': ['Plane', 'Airplane', 'Entire plane'],
    'Houseboat': ['Houseboat', 'House Boat'],
    'Island': ['Island', 'Entire island'],
    'Lighthouse': ['Lighthouse'],
    
    'Treehouse': ['Treehouse', 'Entire treehouse', 'Private room in treehouse'],
    'Campsite': ['Campsite'],
    'Hut': ['Hut', 'Shepherd&#39;s hut', 'Shepherd\'s hut', 'Private room in hut', 'Entire hut'],
    'Yurt': ['Yurt', 'Entire yurt', 'Dome house',],
    'Tent': ['Tent', 'Private room in tent', 'Shared room in tent', 'Entire tent'],
    'Tipi': ['Tipi', 'Tepee', 'Teepee', 'Entire tipi', 'Private room in tipi'],
    'Igloo': ['Igloo', 'Entire igloo'],
    'Cave': ['Cave', 'Cavern', 'Entire cave', 'Entire cavern'],
    
    'Private room': [
        'Private room', 'Private room in house', 'Private room in residential home',
        'Private room in loft', 'Private room in guest suite', 'Private room in guesthouse',
        'Private room in townhouse', 'Private room in vacation home',
        'Private room in villa', 'Private room in bungalow', 'Private room in castle',
        'Private room in farm stay', 'Private room in resort',
         'Private room in cottage',
         'Private room in rental unit',
         'Private room in nature lodge',
         'Shared room in hostel',
         'Private room in condominium (condo)',
         'Private room in tiny house',
         'Private room in yurt',
         'Private room in barn',
         'Private room in earth house',
         'Private room in dome house',
        ],
    'Private room in cabin': ['Private room in cabin', 'Room in nature lodge', 'Private room in chalet'],
    'Private room in apartment': [
        'Private room in apartment', 'Private room in condominium',
        'Private room in serviced apartment', 'Room in serviced apartment',
        ],
    'Shared room': [
        'Shared room', 'Shared room in house', 'Shared room in villa',
        'Shared room in townhouse', 'Shared room in bungalow',
        'Shared room in bed and breakfast', 'Shared room in bed & breakfast'],
    'Shared room in apartment': [
        'Shared room in apartment', 'Shared room in condominium', 'Shared room in serviced apartment', 'Shared room in loft'],
    
    'Suite': ['Loft', 'Entire loft', 'Guest suite', "Suite", 'Entire guest suite', 'Entire Floor', 'Floor', 'Home/apt'],
}

e_PROP_TYPE_STD = {}
for s_std, a_vals in e_PROPERTY_TYPES.items():
    for s_val in a_vals:
        e_PROP_TYPE_STD[s_val.lower()] = s_std

e_PROPERTY_CATEGORIES = {
    'House': ['House', 'Rancher', 'rancher'],
    'Vacation home': ['Vacation home', 'Cabin', 'Cottage', 'Chateau', 'Estate', 'Tiny house', 'Farmhouse'],
    'Townhouse': ['Townhouse', 'Townhome', 'townhome', ],
    
    'Guesthouse': ['Guesthouse', 'Guest House', 'In-law', 'Entire guesthouse'],
    
    'Hotel': [
        'Bed and breakfast', 'Hotel', 'Boutique hotel', 'Resort', 'Motel'],
    'Timeshare': ["Timeshare"],
    'Hostel': ['Hostel', 'Dorm'],
    
    'Apartment': ['Apartment', 'Studio'],
    'Condo': ['Condo'],
    
    'Camper/RV': ['Camper/RV', 'Recreational Vehicle', 'Car', 'Plane'],
    
    'Rustic': ['Rustic', 'Barn', 'Treehouse', 'Campsite', 'Yurt', 'Tent', 'Tipi', 'Igloo', 'Cave', 'Hut', 'Earth house', 'Island',],
    
    'Other': ['Other', 'Place', 'Lighthouse', 'Parking Space'],
    
    'Boat': ['Boat', 'Houseboat'],
    
    'Room': ['Room', 'Private room', 'Private room in cabin', 'Private room in apartment'],
    'Shared Room': ['Shared room', 'Shared room in apartment'],
    
    'Suite': ['Suite', 'Loft', 'Guest suite'],
}
e_PROP_CAT_STD = {}
for s_std, a_vals in e_PROPERTY_CATEGORIES.items():
    for s_val in a_vals:
        e_PROP_CAT_STD[s_val.lower()] = s_std


e_FEATURE_STDS = {
    "airbnb": "airbnb",
    "homeaway": "homeaway",
    "bedrooms": "bedrooms",
    "bathrooms": "bathrooms",
    "max_guests": "max_guests",
    "property_type": "property_type",
    "property_category": "property_category",
    "feature_single_level_home": "feature_single_level_home",
    "annual_revenue_ltm": "annual_revenue_ltm",
    "annual_revenue_ltm_(usd)": "annual_revenue_ltm",
    "number_of_reviews": "number_of_reviews",
    "number_of_photos": "number_of_photos",
    "published_nightly_rate": "published_nightly_rate",
    "published_nightly_rate_(usd)": "published_nightly_rate",
    "response_rate": "response_rate",
    "overall_rating": "overall_rating",
    "feature_wheelchair_accessible": "feature_wheelchair_accessible",
    "feature_suitability_wheelchair_accessible": "feature_wheelchair_accessible",
    #"": "",
    
    "feature_full_kitchen": "feature_kitchen",
    "feature_kitchen": "feature_kitchen",
    "feature_kitchenette": "feature_kitchenette",
    "feature_kitchen_kitchenette": "feature_kitchenette",
    "feature_kitchen_kitchen": "feature_kitchen",
    "location_kitchen": "feature_kitchen",
    "feature_essentials": "feature_essentials",
    "feature_essential_towels_provided": "feature_essentials",
    "feature_towels_provided": "feature_essentials",
    "feature_towels_provided": "feature_essentials",
    "feature_heating": "feature_heating",
    "feature_essential_heating": "feature_heating",
    "feature_general_heating": "feature_heating",
    "feature_washer": "feature_washer",
    "feature_washing_machine": "feature_washer",
    "feature_general_washing_machine": "feature_washer",
    "feature_essential_washing_machine": "feature_washer",
    "feature_dryer": "feature_dryer",
    "feature_general_clothes_dryer": "feature_dryer",
    "feature_clothes_dryer": "feature_dryer",
    "feature_essential_clothes_dryer": "feature_dryer",
    "feature_tumble_dryer": "feature_dryer",
    
    "feature_wireless_internet": "feature_wireless_internet",
    "feature_free_wifi": "feature_wireless_internet",
    "feature_wifi": "feature_wireless_internet",
    "feature_general_free_wifi": "feature_wireless_internet",
    "feature_pocket_wifi": "feature_wireless_internet",
    "feature_flexcation_free_wifi": "feature_wireless_internet",
    "feature_flexcation_wifi_speed": "feature_wireless_internet",
    "feature_general_wireless_internet": "feature_wireless_internet",
    "feature_essential_wireless_internet": "feature_wireless_internet",
    "feature_flexcation_wireless_internet": "feature_wireless_internet",
    
    "feature_hair_dryer": "feature_hair_dryer",
    "feature_general_hair_dryer": "feature_hair_dryer",
    "feature_essential_hair_dryer": "feature_hair_dryer",
    "feature_smoke_detector": "feature_smoke_detector",
    "feature_exclusive_smoke_detector_smoke_detector": "feature_smoke_detector",
    "feature_smoke_alarm": "feature_smoke_detector",
    "feature_safety_smoke_detector": "feature_smoke_detector",
    "feature_pdp_safety_smoke_detector": "feature_smoke_detector",
    "feature_general_air_conditioning": "feature_ac",
    "feature_air_conditioning": "feature_ac",
    "feature_essential_air_conditioning": "feature_ac",
    
    "pets_allowed": "pets_allowed",
    "feature_allows_pets": "pets_allowed",
    "feature_pets_considered": "pets_allowed",
    "feature_pets_allowed": "pets_allowed",
    "feature_has_pets": "pets_allowed",
    "feature_suitability_pets_considered": "pets_allowed",
    
    "feature_ethernet_connection": "feature_internet",
    "feature_internet": "feature_internet",
    "feature_general_internet": "feature_internet",
    "feature_broadband_access": "feature_internet",
    
    "feature_ac": "feature_ac",
    "feature_cooling": "feature_ac",
    "feature_cable": "feature_cable",
    "feature_cable_tv": "feature_cable",
    "feature_entertainment_all_ages_satellite_cable": "feature_cable",
    "feature_entertainment_satellite_cable": "feature_cable",
    "feature_family_friendly": "feature_family_friendly",
    "feature_general_wood_stove": "feature_wood_stove",
    "feature_fireplace": "feature_fireplace",
    "feature_general_fireplace": "feature_fireplace",
    "feature_wood_fireplace": "feature_fireplace",
    "feature_gas_fireplace": "feature_fireplace",
    "feature_indoor_fireplace": "feature_fireplace",
    "feature_laptop_friendly": "feature_laptop_friendly",
    "feature_laptop_friendly_workspace": "feature_laptop_friendly",
    "feature_breakfast": "feature_breakfast",
    "feature_shampoo": "feature_bathroom_essentials",
    "feature_general_shampoo": "feature_bathroom_essentials",
    "feature_shower_gel": "feature_bathroom_essentials",
    "feature_shampoo": "feature_bathroom_essentials",
    "feature_body_soap": "feature_bathroom_essentials",
    "feature_basic_soaps": "feature_bathroom_essentials",
    
    "feature_conditioner": "feature_bathroom_essentials",
    "feature_general_conditioner": "feature_bathroom_essentials",
    "feature_hangers": "feature_hangers",
    "feature_hot_water": "feature_hot_water",
    "feature_coffee_maker": "feature_coffee_maker",
    "feature_kitchen_coffee_maker": "feature_coffee_maker",
    "feature_coffee_grinder": "feature_coffee_grinder",
    "feature_kitchen_coffee_grinder": "feature_coffee_grinder",
    "feature_cooking_basics": "feature_cooking_basics",
    "location_coffee_shops": "location_coffee_shops",
    "location_coffee_shop": "location_coffee_shops",
    "location_coffee": "location_coffee_shops",
    "location_coffee_houses": "location_coffee_shops",
    "location_coffee_spots": "location_coffee_shops",
    "location_starbucks_coffee": "location_coffee_shops",
    "location_a_coffee_shop": "location_coffee_shops",
    "location_coffee_bars": "location_coffee_shops",
    
    "feature_kitchen_dishes_&_utensils": "feature_dishes_and_silverware",
    "feature_dishes_and_silverware": "feature_dishes_and_silverware",
    "feature_dishes_&_utensils": "feature_dishes_and_silverware",
    "feature_dishes_utensils": "feature_dishes_and_silverware",
    "feature_barbecue_utensils": "feature_dishes_and_silverware",
    "feature_barbeque_utensils": "feature_dishes_and_silverware",
    
    "feature_kitchen_dishwasher": "feature_dishwasher",
    "feature_dishwasher": "feature_dishwasher",
    "feature_microwave": "feature_microwave",
    "feature_kitchen_microwave": "feature_microwave",
    "feature_refrigerator": "feature_refrigerator",
    "feature_fridge": "feature_refrigerator",
    "feature_kitchen_refrigerator": "feature_refrigerator",
    "feature_kitchen_fridge": "feature_refrigerator",
    "feature_freezer": "feature_refrigerator",
    "feature_mini_fridge": "feature_mini_fridge",
    "feature_mini_refrigerator": "feature_mini_fridge",
    "feature_oven": "feature_oven",
    "feature_kitchen_oven": "feature_oven",
    "feature_stove": "feature_stove",
    "feature_kitchen_stove": "feature_stove",
    "feature_private_entrance": "feature_private_entrance",
    "feature_bed_linens": "feature_bed_linens",
    "feature_general_linens_provided": "feature_bed_linens",
    "feature_essential_linens_provided": "feature_bed_linens",
    "feature_extra_pillows_and_blankets": "feature_extra_pillows_and_blankets",
    "feature_bathtub": "feature_bathtub",
    "feature_childrens_books_and_toys": "feature_family_friendly",
    "feature_children_s_books_and_toys": "feature_family_friendly",
    "feature_crib": "feature_crib",
    "feature_travel_crib": "feature_crib",
    "feature_pack_n_play_travel_crib": "feature_crib",
    "feature_flexcation_travel_crib": "feature_crib",
    "feature_luggage_dropoff_allowed": "feature_luggage_dropoff_allowed",
    "feature_long_term_stays_allowed": "feature_long_term_stays_allowed",
    "feature_lock_on_bedroom_door": "feature_lock_on_bedroom_door",
    "feature_lockbox": "feature_smart_lock",
    "feature_smartlock": "feature_smart_lock",
    "feature_smart_lock": "feature_smart_lock",
    "feature_deadbolt_lock": "feature_deadbolt_lock",
    "feature_pdp_safety_deadbolt_lock": "feature_deadbolt_lock",
    "feature_safety_deadbolt_lock": "feature_deadbolt_lock",
    
    
    "feature_keypad": "feature_keypad",
    "feature_toaster": "feature_toaster",
    "feature_kitchen_toaster": "feature_toaster",
    "feature_clothing_storage": "feature_wardrobe_or_closet",
    "feature_wardrobe_or_closet": "feature_wardrobe_or_closet",
    'feature_trash_compacter': 'feature_trash_compactor',
    'feature_trash_compactor': 'feature_trash_compactor',
    
    "feature_dining_checkbox_dining": "feature_dining_area",
    "feature_dining": "feature_dining_area",
    "feature_dining_area": "feature_dining_area",
    "feature_spaces_dining_area": "feature_dining_area",
    "feature_spaces_dining_room": "feature_dining_area",
    "feature_dining_room": "feature_dining_area",
    "feature_dining_dining_area": "feature_dining_area",
    "feature_dining_dining_room": "feature_dining_area",
    "location_dining_room": "feature_dining_area",
    "feature_dining_dining": "feature_dining_area",
    "feature_flexcation_dining_table": "feature_dining_area",
    "feature_dining_table": "feature_dining_area",
    "location_dining_area": "feature_dining_area",
    "feature_living_room": "feature_living_room",

    "feature_garage": "feature_garage",
    "feature_covered_parking": "feature_garage",
    "feature_parking": "feature_parking",
    "feature_free_parking_on_premises": "feature_parking",
    "feature_general_parking": "feature_parking",
    "feature_parking_off_street": "feature_parking",
    "feature_general_garage": "feature_parking",
    "feature_free_parking": "feature_parking",
    "feature_paid_parking": "feature_paid_parking",
    "feature_paid_parking_on_premises": "feature_paid_parking",
    "feature_paid_parking": "feature_paid_parking",
    "feature_paid_parking_off_premises": None,
    "feature_street_parking": "feature_street_parking",
    "feature_parking_on_street": "feature_street_parking",
    "feature_free_street_parking": "feature_street_parking",
    "feature_disabled_parking_spot": "feature_disabled_parking_spot",
    "feature_parking_for_rv_boat_trailer": "feature_parking_for_rv",
    
    
    "feature_pantry_items": "feature_pantry_items",
    "feature_linens_provided": "feature_linens_provided",
    "feature_children_welcome": "feature_family_friendly",
    "allows_children": "feature_family_friendly",
    "location_great_for_kids": "feature_family_friendly",
    "feature_non_smoking_only": "feature_non_smoking_only",
    "feature_dining_checkbox_dining": "feature_dining_checkbox_dining",
    "feature_ice_maker": "feature_ice_maker",
    "feature_minimum_age_limit_for_renters": "feature_minimum_age_limit_for_renters",
    "feature_suitability_minimum_age_limit_for_renters": "feature_minimum_age_limit_for_renters",
    "feature_suitability_chkbox_minimum_age_limit_for_renters": "feature_minimum_age_limit_for_renters",
    "feature_exclusive_police_emergency_contact_police_contact": "feature_exclusive_police_emergency_contact_police_contact",
    "feature_exclusive_medical_emergency_contact_hospital_contact": "feature_exclusive_medical_emergency_contact_hospital_contact",
    "feature_suitability_pets_not_allowed": "feature_suitability_pets_not_allowed",
    "feature_books": "feature_books",
    "feature_books_for_kids": "feature_books",
    "feature_flexcation_books_for_kids": "feature_books",
    "feature_entertainment_all_ages_books": "feature_books",
    "feature_entertainment_books": "feature_books",
    "feature_video_library": "feature_video_library",
    "feature_paper_towels": "feature_paper_towels",
    "feature_general_paper_towels": "feature_paper_towels",
    "feature_blender": "feature_blender",
    "feature_kitchen_blender": "feature_blender",
    "feature_basic_soaps": "feature_basic_soaps",
    "feature_toilet_paper": "feature_toilet_paper",
    "feature_essential_toilet_paper": "feature_toilet_paper",
    "feature_exterior_lighting": "feature_exterior_lighting",
    "feature_path_to_entrance_lit_at_night": "feature_exterior_lighting",
    "feature_elevator": "feature_elevator",
    "feature_accessibility_elevator": "feature_elevator",
    "feature_general_elevator": "feature_elevator",
    "feature_clothes_drying_rack": "feature_clothes_drying_rack",
    "feature_drying_rack_for_clothing": "feature_clothes_drying_rack",
    
    "feature_tv": "feature_tv",
    "feature_television": "feature_tv",
    "feature_smart_tv": "feature_tv",
    "feature_entertainment_all_ages_smart_tv": "feature_tv",
    "feature_entertainment_television": "feature_tv",
    "feature_entertainment_all_ages_television": "feature_tv",
    "feature_vcr": "feature_dvd_player",
    "feature_dvd_player": "feature_dvd_player",
    "feature_entertainment_dvd_player": "feature_dvd_player",
    "feature_entertainment_all_ages_dvd_player": "feature_dvd_player",
    "feature_property_amenities_entertainment_dvd_player": "feature_dvd_player",
    "feature_satellite_cable": "feature_cable",
    "feature_gym": "location_recreation_center",
    "location_gym": "location_recreation_center",
    
    "feature_pond": "feature_pond",
    "feature_attractions_pond": "feature_pond",
    "location_water_view_pond": "feature_pond",
    "location_waterfront_pond_view": "feature_pond",
    "location_pond": "feature_pond",
    "location_a_pond": "feature_pond",
    
    "feature_lake_access": "location_waterfront",
    "location_lake_access": "location_waterfront",
    "location_lakefront": "location_waterfront",
    "location_waterfront": "location_waterfront",
    "location_the_waterfront": "location_waterfront",
    "feature_waterfront": "location_waterfront",
    "feature_walkway_to_water": "location_waterfront",
    #"location_lake_view": "location_waterfront",
    "location_lake": "location_waterfront",
    "location_lakes": "location_waterfront",
    "location_lake_lake": "location_waterfront",
    "location_waterfront_on_the_lake": "location_waterfront",
    "location_private_waterfront": "location_waterfront",
    
    "location_beach": "location_waterfront",
    "feature_beachfront": "location_waterfront",
    "location_beachfront": "location_waterfront",
    "location_beach_access": "location_waterfront",
    "location_beach_view": "location_waterfront",
    "feature_nude_beach": "location_waterfront",
    "location_beach_yes": "location_waterfront",
    "location_beach_view_yes": "location_waterfront",
    "location_beach_view_yes": "location_waterfront",
    "location_beach_beach_view": "location_waterfront",
    "location_private_beach": "location_waterfront",
    "feature_beachcombing": "location_waterfront",
    "location_beaches": "location_waterfront",
    "location_ocean_view": "location_waterfront",
    "location_near_the_ocean": "location_waterfront",
    "location_oceanfront": "location_waterfront",
    "location_near_the_ocean_yes": "location_waterfront",
    "location_ocean_view_yes": "location_waterfront",
    "location_oceanfront_yes": "location_waterfront",
    "location_ocean_view_ocean_view": "location_waterfront",
    "location_waterfront_ocean_front": "location_waterfront",
    "feature_ocean_view": "location_waterfront",
    "location_near_the_ocean_gulf_of_mexico": "location_waterfront",
    "location_oceanfront_oceanfront": "location_waterfront",
    "location_near_the_ocean_oceanfront": "location_waterfront",
    "location_ocean": "location_waterfront",
    "location_oceanfront_gulf_of_mexico": "location_waterfront",
    "location_waterfront_oceanfront": "location_waterfront",
    "location_waterfront_ocean_block": "location_waterfront",
    "location_waterfront_bay_side": "location_waterfront",
    "location_waterfront_bay_water": "location_waterfront",
    "location_beach_oceanfront": "location_waterfront",
    "location_chesapeake_bay": "location_waterfront",
    "location_deep_sea": "location_waterfront",
    "location_river": "location_waterfront",
    "location_lazy_river": "location_waterfront",
    "location_waterfront_riverfront": "location_waterfront",
    "location_rivers": "location_waterfront",
    "location_lake_water": "location_waterfront",
    "location_waterfront_creekfront": "location_stream",
    "location_waterfront_creek": "location_stream",
    "location_waterfront_stream": "location_stream",
    "location_waterfront_streamfront": "location_stream",
    "location_water_view_creek_view": "location_stream",
    "location_right_on_the_water": "location_waterfront",
    "location_just_yards_from_the_water": "location_waterfront",
    
    "feature_tennis": "location_tennis",
    "feature_tennis_court": "location_tennis",
    "feature_outdoor_features_tennis": "location_tennis",
    "feature_outside_tennis": "location_tennis",
    "location_tennis_court": "location_tennis_courts",
    "location_tennis_courts": "location_tennis_courts",
    "feature_golf": "location_golf",
    "feature_golf_course": "location_golf",
    "location_golf": "location_golf",
    "location_golf_course": "location_golf",
    "feature_outside_golf": "location_golf",
    "location_golf": "location_golf",
    "location_golf_course": "location_golf",
    "feature_golf_privileges_optional": "location_golf",
    "feature_sports_&_adventure_activities_golf": "location_golf",
    "feature_outdoor_features_golf": "location_golf",
    "location_golfing": "location_golf",
    "location_with_golf": "location_golf",
    "feature_golf_cart": "location_golf",
    "location_golf_cart": "location_golf",
    
    "location_hiking": "location_hiking",
    "location_hiking_trails": "location_hiking",
    "location_mountain_biking": "location_mountain_biking",
    "location_mountain_view": "location_mountain_view",
    'location_mountain': 'location_mountain_view',
    "location_town": "location_downtown",
    "location_downtown": "location_downtown",
    "location_main_street": "location_downtown",
    "feature_water_view": "location_water_view",
    "location_water_view": "location_water_view",
    "location_lake_view": "location_water_view",
    "location_water_view_oceanfront": "location_water_view",
    "feature_waterfalls": "location_waterfalls",
    
    "location_playground": "location_playground",
    "location_playgrounds": "location_playground",
    "feature_attractions_playground": "location_playground",
    "location_children_playground!": "location_playground",
    "feature_outdoor_play_area": "location_playground",
    "feature_playground": "location_playground",
    
    "feature_churches": "location_churches",
    "feature_attractions_churches": "location_churches",
    "feature_synagogues": "location_churches",
    "feature_mosques": "location_churches",
    "feature_cinemas": "location_theater",
    "feature_attractions_cinemas": "location_theater",
    "location_live_theater": "location_theater",
    "feature_live_theater": "location_theater",
    "location_theater": "location_theater",
    "location_theaters": "location_theater",
    "feature_live_theater": "location_theater",
    "location_movie_theater": "location_theater",
    "location_movie_theatre": "location_theater",
    "location_and_theater": "location_theater",
    
    
    "feature_library": "location_library",
    "feature_museums": "location_museums",
    "feature_attractions_museums": "location_museums",
    "location_monument_view": "location_museums",
    "location_galleries": "location_museums",
    "feature_galleries": "location_museums",
    "feature_attractions_galleries": "location_museums",
    "location_art_galleries": "location_museums",
    "feature_art_galleries": "location_museums",
    "feature_attractions_art_galleries": "location_museums",
    "location_restaurant": "location_restaurants",
    "feature_restaurants": "location_restaurants",
    "feature_local_restaurant": "location_restaurants",
    "feature_local_restaurants": "location_restaurants",
    "feature_attractions_restaurants": "location_restaurants",
    "location_dining": "location_restaurants",
    "location_parks": "location_parks",
    "location_park": "location_parks",
    "botanical_garden": "location_parks",
    "feature_attractions_botanical_garden": "location_parks",
    "feature_botanical_garden": "location_parks",
    "location_park_with_playground": "location_parks",
    "location_dog_park": "location_parks",
    "location_disc_golf": "location_parks",
    "feature_theme_parks": "location_theme_parks",
    "feature_attractions_theme_parks": "location_theme_parks",
    "feature_water_parks": "location_water_parks",
    "feature_attractions_water_parks": "location_water_parks",
    "location_fun_splash_zone_for_kids": "location_water_parks",
    "location_water_slide": "location_water_parks",
    "feature_antiquing": "location_antiquing",
    "feature_bird_watching": "location_wildlife_viewing",
    "feature_whale_watching": "location_wildlife_viewing",
    "location_deer": "location_wildlife_viewing",
    "location_ducks": "location_wildlife_viewing",
    "location_dolphins": "location_wildlife_viewing",
    "location_turtles": "location_wildlife_viewing",
    "feature_gambling_casinos": "location_gambling_casinos",
    "location_gambling_casinos": "location_gambling_casinos",
    "feature_scenic_drives": "location_scenic_drives",
    "feature_attractions_autumn_foliage": "location_scenic_drives",
    "feature_autumn_foliage": "location_scenic_drives",
    "feature_shopping": "location_shopping",
    "feature_outlet_shopping": "location_shopping",
    "location_stores": "location_shopping",
    "feature_duty_free_shops": "location_shopping",
    "feature_duty_free_shops": "location_shopping",
    "feature_attractions_duty_free_shops": "location_shopping",
    "location_shops": "location_shopping",
    "location_boutiques": "location_shopping",
    
    "feature_sight_seeing": "location_sight_seeing",
    "feature_walking": "location_walking",
    "feature_wildlife_viewing": "location_wildlife_viewing",
    "location_and_wildlife": "location_wildlife_viewing",
    "location_atm": "location_atm_bank",
    "location_atms": "location_atm_bank",
    "location_bank": "location_atm_bank",
    "location_banks": "location_atm_bank",
    "feature_atm_bank": "location_atm_bank",
    "feature_local_services_&_businesses_atm_bank": "location_atm_bank",
    "feature_local_services_and_businesses_atm_bank": "location_atm_bank",
    "feature_fitness_center": "location_recreation_center",
    "feature_recreation_center": "location_recreation_center",
    "location_arcade": "location_recreation_center",
    "feature_groceries": "location_groceries",
    "location_and_a_large_supermarket": "location_groceries",
    "location_supermarket": "location_groceries",
    "location_market": "location_groceries",
    "location_farmers_market": "location_groceries",
    "location_farmer&#x27_s_market	": "location_groceries",
    "location_supermarket": "location_groceries",
    "feature_hospital": "location_hospital",
    "feature_medical_services": "location_hospital",
    "feature_local_services_&_businesses_hospital": "location_hospital",
    "feature_laundromat": "location_laundromat",
    "feature_coin_laundry": "location_laundromat",
    "feature_laundromat_nearby": "location_laundromat",
    "feature_local_services_&_businesses_laundromat": "location_laundromat",
    "feature_attractions_coin_laundry": "location_laundromat",
    "location_coin_laundry": "location_laundromat",
    "feature_essential_coin_laundry": "location_laundromat",
    "location_laundry": "location_laundry",
    "location_on_site_laundry": "location_laundry",
    "location_laundry_facilities": "location_laundry",
    
    "location_fishing": "location_fishing",
    "feature_fishing": "location_fishing",
    "feature_pier_fishing": "location_fishing",
    "feature_fly_fishing": "location_fishing",
    "feature_freshwater_fishing": "location_fishing",
    "feature_sports_and_adventure_activities_freshwater_fishing": "location_fishing",
    "feature_sports_&_adventure_activities_freshwater_fishing": "location_fishing",
    "location_pier_fishing": "location_fishing",
    "feature_deepsea_fishing": "location_fishing",
    "feature_sound_bay_fishing": "location_fishing",
    "location_fish": "location_fishing",
    "feature_hiking": "location_hiking",
    "feature_sports_&_adventure_activities_hiking": "location_hiking",
    
    "location_boating": "location_boating",
    #"location_jet_skiing": "location_jet_skiing",
    "location_jet_skiing": "location_boating",
    "feature_boating": "location_boating",
    #"feature_jet_skiing": "location_jet_skiing",
    "feature_jet_skiing": "location_boating",
    #"feature_kayaking": "location_kayaking",
    #"location_kayaking": "location_kayaking",
    "feature_kayaking": "location_boating",
    "location_kayaking": "location_boating",
    "location_kayak": "location_boating",
    "location_kayaks": "location_boating",
    "location_canoe": "location_boating",
    "location_canoeing": "location_boating",
    "feature_mountain_biking": "location_mountain_biking",
    "feature_cycling": "location_cycling",
    "feature_biking": "location_cycling",
    "location_cycling": "location_cycling",
    "location_biking": "location_cycling",
    "feature_sports_&_adventure_activities_cycling": "location_cycling",
    "feature_sports_and_adventure_activities_cycling": "location_cycling",
    "feature_bicycles": "location_cycling",
    "feature_bikes": "location_cycling",
    "feature_bicycle": "location_cycling",
    "feature_outside_bicycles": "location_cycling",
    "feature_outdoor_features_bicycles": "location_cycling",
    "location_bike": "location_cycling",
    
    #"feature_rafting": "location_rafting",
    "feature_rafting": "location_boating",
    #"feature_sailing": "location_sailing",
    "feature_sailing": "location_boating",
    "feature_sports_&_adventure_activities_sailing": "location_boating",
    "feature_skiing": "location_skiing",
    "feature_snowboarding": "location_skiing",
    "feature_outside_ski_&_snowboard": "location_skiing",
    "feature_ski_&_snowboard": "location_skiing",
    "feature_ski_privileges_optional": "location_skiing",
    "feature_ski_lift_privileges": "location_skiing",
    "feature_sports_&_adventure_activities_skiing": "location_skiing",
    "feature_swimming": "location_swimming",
    "location_swim": "location_swimming",
    "feature_water_skiing": "location_boating",
    "location_water_skiing": "location_boating",
    "feature_sports_&_adventure_activities_water_skiing": "location_boating",
    "location_ski_in_ski_out_water_ski": "location_boating",
    "feature_water_tubing": "location_adventure_activities",
    "location_tubing": "location_adventure_activities",
    #"feature_marina": "location_marina",
    #"location_marina": "location_marina",
    "feature_marina": "location_boating",
    "location_marina": "location_boating",
    "feature_attractions_marina": "location_boating",
    "feature_horseback_riding": "location_horseback_riding",
    "feature_equestrian_events": "location_horseback_riding",
    "feature_horses": "location_horseback_riding",
    "location_horses": "location_horseback_riding",
    "feature_photography": "location_photography",
    'feature_sports_&_adventure_activities_swimming': 'location_adventure_activities',
    'feature_leisure_activities_water_sports': 'location_adventure_activities',
    "feature_paddle_boating": "location_boating",
    'feature_water_sports': 'location_adventure_activities',
    'feature_outside_water_sports_gear': 'location_adventure_activities',
    'location_water_sports': 'location_adventure_activities',
    'feature_surfing': 'location_adventure_activities',
    'feature_wind_surfing': 'location_adventure_activities',
    'location_wind_surfing': 'location_adventure_activities',
    'location_surfing': 'location_adventure_activities',
    'feature_scuba_diving_or_snorkeling': 'location_adventure_activities',
    'feature_reefs': 'location_adventure_activities',
    'feature_snorkeling': 'location_adventure_activities',
    "feature_snorkeling_diving": 'location_adventure_activities',
    "location_snorkeling": 'location_adventure_activities',
    "feature_spelunking": 'location_adventure_activities',
    "feature_caves": 'location_adventure_activities',
    "feature_attractions_caves": 'location_adventure_activities',
    "feature_roller_blading": 'location_adventure_activities',
    "feature_jogging": 'location_adventure_activities',
    "location_roller_blading": 'location_adventure_activities',
    "location_jogging": 'location_adventure_activities',
    "feature_hot_air_ballooning": 'location_adventure_activities',
    "location_volleyball": 'location_adventure_activities',
    "location_volley_courts": 'location_adventure_activities',
    "location_basketball_court": "location_basketball_court",
    "feature_basketball_court": "location_basketball_court",
    "location_basketball": "location_basketball_court",
    
    "feature_sledding": "location_skiing",
    "location_sledding": "location_skiing",
    "feature_cross_country_skiing": "location_skiing",
    "location_cross_country_skiing": "location_skiing",
    "feature_hunting": "location_hunting",
    "feature_ice_skating": "location_ice_skating",
    "feature_mountain_climbing": "location_mountain_climbing",
    "feature_mountaineering": "location_mountain_climbing",
    "location_mountaineering": "location_mountain_climbing",
    "feature_parasailing": "location_adventure_activities",
    "location_parasailing": "location_adventure_activities",
    "feature_paragliding": "location_adventure_activities",
    "location_paragliding": "location_adventure_activities",
    "feature_rock_climbing": "location_mountain_climbing",
    "location_rock_climbing": "location_mountain_climbing",
    "feature_snowmobiling": "location_adventure_activities",
    "feature_whitewater_rafting": "location_adventure_activities",
    "location_whitewater_rafting": "location_adventure_activities",
    "feature_miniature_golf": "location_miniature_golf",
    "feature_mini_golf": "location_miniature_golf",
    "location_mini_golf": "location_miniature_golf",
    "location_miniature_golf": "location_miniature_golf",
    "location_putt_putt_golf": "location_miniature_golf",
    "feature_putt_putt_golf": "location_miniature_golf",
    "feature_racquetball": "location_adventure_activities",
    "feature_toys": "family_friendly",
    "location_toys": "family_friendly",
    "feature_entertainment_toys": "family_friendly",
    "feature_massage": "location_massage",
    "feature_massage_therapist": "location_massage",
    "feature_onsite_services_massage": "location_massage",
    "location_massage_therapist": "location_massage",
    "feature_winery_tours": "location_winery_tours",
    "feature_attractions_winery_tours": "location_winery_tours",
    
    "feature_alfresco_shower": "feature_outdoor_shower",
    "feature_outdoor_shower": "feature_outdoor_shower",
    
    "feature_pool": "feature_pool",
    "feature_pool_spa_private_pool": "feature_pool",
    "feature_pool_spa_indoor_pool": "feature_pool",
    "feature_together_private_pool": "feature_pool",
    "feature_heated_pool": "feature_pool",
    "feature_pool_spa_heated_pool": "feature_pool",
    "feature_indoor_pool": "feature_pool",
    "feature_private_pool": "feature_pool",
    "feature_fenced_pool": "feature_pool",
    "feature_together_indoor_pool": "feature_pool",
    "feature_together_heated_pool": "feature_pool",
    "feature_pool_spa_outdoor_pool": "feature_pool",
    "feature_together_outdoor_pool": "feature_pool",
    "feature_outdoor_pool": "feature_pool",
    "feature_pool_spa_pool": "feature_pool",
    "feature_flexcation_fenced_pool": "feature_pool",
    "feature_in_ground_pool": "feature_pool",
    "location_pool": "location_pool",
    "feature_communal_pool": "location_pool",
    "feature_pool_spa_communal_pool": "location_pool",
    "feature_together_communal_pool": "location_pool",
    "location_swimming_pool": "location_pool",
    
    "feature_jacuzzi": "feature_jacuzzi",
    "feature_hot_tub": "feature_jacuzzi",
    "feature_shared_hot_tub": "feature_jacuzzi",
    "feature_pool_spa_shared_hot_tub": "feature_jacuzzi",
    "feature_spa_whirlpool": "feature_jacuzzi",
    "feature_pool_spa_whirlpool": "feature_jacuzzi",
    "feature_whirlpool": "feature_jacuzzi",
    "feature_pool_spa_spa_whirlpool": "feature_jacuzzi",
    "feature_pool_spa_jacuzzi": "feature_jacuzzi",
    "feature_pool_spa_hot_tub": "feature_jacuzzi",
    "feature_pool_spa_jetted_tub": "feature_jacuzzi",
    "feature_jetted_tub": "feature_jacuzzi",
    "feature_pool_spa_hot_tub": "feature_jacuzzi",
    "feature_outdoor_hot_tub": "feature_jacuzzi",
    "feature_private_hot_tub": "feature_jacuzzi",
    
    "feature_baby_monitor": "feature_baby_monitor",
    
    "feature_sauna" : "feature_sauna",
    "feature_pool_spa_sauna" : "feature_sauna",
    "location_sauna" : "location_sauna",
    
    "feature_health_beauty_spa": "location_health_beauty_spa",
    "feature_attractions_health_beauty_spa": "location_health_beauty_spa",
    "location_aveda_beauty_spa": "location_health_beauty_spa",
    "location_spa": "location_health_beauty_spa",
    "location_spas": "location_health_beauty_spa",
    
    "feature_iron": "feature_iron_&_board",
    "feature_iron_&_board": "feature_iron_&_board",
    "feature_essential_iron_&_board": "feature_iron_&_board",
    "feature_iron_&_board": "feature_iron_&_board",
    "feature_linens_provided": "feature_bed_linens",
    
    "feature_patio_or_balcony": "feature_patio_or_balcony",
    "feature_deck_patio": "feature_patio_or_balcony",
    "feature_deck_patio": "feature_patio_or_balcony",
    "location_deck": "feature_patio_or_balcony",
    "feature_patio": "feature_patio_or_balcony",
    "feature_together_deck_patio": "feature_patio_or_balcony",
    "feature_outside_deck_patio": "feature_patio_or_balcony",
    "feature_outdoor_features_deck_patio": "feature_patio_or_balcony",
    "feature_balcony": "feature_patio_or_balcony",
    "feature_together_balcony": "feature_patio_or_balcony",
    "feature_outside_balcony": "feature_patio_or_balcony",
    "feature_outdoor_features_balcony": "feature_patio_or_balcony",
    "feature_wood_deck": "feature_patio_or_balcony",
    "feature_terrace": "feature_patio_or_balcony",
    "feature_porch_veranda": "feature_patio_or_balcony",
    "feature_together_porch_veranda": "feature_patio_or_balcony",
    "feature_unscreened_porch_veranda": "feature_patio_or_balcony",
    "feature_screen_porch_veranda": "feature_patio_or_balcony",
    "feature_outside_porch_veranda": "feature_patio_or_balcony",
    "feature_outdoor_features_porch_veranda": "feature_patio_or_balcony",
    
    "feature_porch_lanai": "feature_patio_or_balcony",
    "feature_unscreened_porch_lanai": "feature_patio_or_balcony",
    "feature_screened_porch_lanai": "feature_patio_or_balcony",
    "feature_outside_porch_lanai": "feature_patio_or_balcony",
    "feature_outside_screened_porch_lanai": "feature_patio_or_balcony",
    "feature_alarm_clock": "feature_alarm_clock",
    "feature_general_alarm_clock": "feature_alarm_clock",
    
    "feature_cooking_utensils": "feature_cooking_basics",
    "feature_kitchen_cooking_utensils": "feature_cooking_basics",
    
    "feature_barbecue": "feature_bbq_area",
    "feature_bbq_area": "feature_bbq_area",
    "feature_outside_barbecue": "feature_bbq_area",
    "feature_grill": "feature_bbq_area",
    "feature_grill": "feature_bbq_area",
    "feature_bbq_grill": "feature_bbq_area",
    "feature_outdoor_grill": "feature_bbq_area",
    "feature_outdoor_grill": "feature_bbq_area",
    "feature_outside_outdoor_grill": "feature_bbq_area",
    "feature_outdoor_grill_gas": "feature_bbq_area", 
    "feature_fire_pit": "feature_bbq_area", 
    "feature_together_fire_pit": "feature_bbq_area",
    
    "feature_garden_or_backyard": "feature_garden_or_backyard",
    "feature_backyard": "feature_garden_or_backyard",
    "feature_yard": "feature_garden_or_backyard",
    "feature_lawn_garden": "feature_garden_or_backyard",
    "feature_together_lawn_garden": "feature_garden_or_backyard",
    "feature_outside_lawn_garden": "feature_garden_or_backyard",
    "feature_outdoor_features_lawn_garden": "feature_garden_or_backyard",
    "feature_fenced_yard": "feature_garden_or_backyard",
    "feature_flexcation_fenced_yard": "feature_garden_or_backyard",
    
    'feature_gated_community': 'feature_gated_community',
    'feature_private_community': 'feature_gated_community',
    'location_private_community': 'feature_gated_community',
    'feature_doorman_entry': 'feature_doorman',
    'feature_doorman': 'feature_doorman',
    
    "feature_suitability_chkbox_long_term_renters_welcome": "feature_long_term_stays_allowed",
    "feature_long_term_renters_welcome": "feature_long_term_stays_allowed",
    "feature_suitability_long_term_renters_welcome": "feature_long_term_stays_allowed",
    "feature_suitability_children_welcome": "feature_family_friendly",
    "feature_children_welcome": "feature_family_friendly",
    "feature_high_chair": "feature_high_chair",
    "feature_dining_checkbox_childs_highchair": "feature_high_chair",
    "feature_child&#39_s_highchair": "feature_high_chair",
    "feature_dining_child&#39_s_highchair": "feature_high_chair",
    "feature_dining_checkbox_child&#39_s_highchair": "feature_high_chair",
    "feature_child's_highchair": "feature_high_chair",
    "feature_dining_child's_highchair": "feature_high_chair",
    "feature_dining_checkbox_child's_highchair": "feature_high_chair",
    "feature_childs_highchair": "feature_childs_highchair",
    "feature_childrens_dinnerware": "feature_family_friendly",
    "feature_children_s_dinnerware": "feature_family_friendly",
    "feature_dishes_&_utensils_for_kids": "feature_family_friendly",
    "feature_flexcation_dishes_&_utensils_for_kids": "feature_family_friendly",
    "feature_stair_gates": "feature_stair_gates",
    "feature_baby_toddler_baby_gate": "feature_stair_gates",
    "feature_baby_gate": "feature_stair_gates",
    "feature_baby_safety_gates": "feature_stair_gates",
    "feature_babysitter_recommendations": "feature_babysitter_recommendations",
    "feature_local_services_and_businesses_babysitter": "feature_babysitter_recommendations",
    "feature_babysitter": "feature_babysitter_recommendations",
    
    "feature_event_friendly": "feature_event_friendly",
    "feature_suitability_chkbox_events_allowed": "feature_event_friendly",
    "feature_house_rules_events_allowed": "feature_event_friendly",
    "feature_events_allowed": "feature_event_friendly",
    "feature_suitability_events_allowed": "feature_event_friendly",
    "feature_suitable_for_events": "feature_event_friendly",
    "feature_festivals": "location_festivals",
    
    "feature_games": "feature_games",
    "feature_games_for_kids": "feature_games",
    "feature_entertainment_games_for_kids": "feature_games",
    "feature_entertainment_all_ages_game_room": "feature_games",
    "feature_entertainment_game_room": "feature_games",
    "feature_game_console": "feature_games",
    "feature_video_game_console": "feature_games",
    "feature_video_games": "feature_games",
    "feature_board_games": "feature_games",
    "feature_entertainment_games": "feature_games",
    "feature_entertainment_all_ages_video_games": "feature_games",
    "feature_foosball": "feature_games",
    "feature_entertainment_all_ages_foosball": "feature_games",
    "location_horseshoes": "feature_games",
    "location_horse_shoes": "feature_games",
    "feature_horseshoes": "feature_games",
    "feature_horse_shoes": "feature_games",
    "feature_ping_pong_table": "feature_games",
    "feature_ping_pong": "feature_games",
    "feature_shuffleboard": "feature_games",
    "location_shuffleboard": "feature_games",
    "feature_entertainment_all_ages_games": "feature_games",
    "location_shuffle_board": "feature_games",
    "feature_pool_table": "feature_pool_table",
    "feature_entertainment_pool_table": "feature_pool_table",
    "feature_entertainment_all_ages_pool_table": "feature_pool_table",
    
    "feature_beach_essentials": "feature_beach_essentials",
    "feature_outside_beach_towels": "feature_beach_essentials",
    "feature_beach_towels": "feature_beach_essentials",
    "feature_private_living_room": "feature_living_room",
    "feature_spaces_living_room": "feature_living_room",
    "location_living_room": "feature_living_room",
    
    "feature_outdoor_seating": "feature_outdoor_seating",
    "feature_outside_outdoor_furniture": "feature_outdoor_seating",
    "feature_outdoor_features_outdoor_furniture": "feature_outdoor_seating",
    "feature_deck_furniture": "feature_outdoor_seating",
    "feature_outdoor_furniture": "feature_outdoor_seating",
    "feature_porch_furniture": "feature_outdoor_seating",
    "feature_beach_chairs": "feature_outdoor_seating",
    "feature_outside_beach_chairs": "feature_outdoor_seating",
    "feature_outdoor_dining_area": "feature_outdoor_seating",
    "feature_alfresco_dining": "feature_outdoor_seating",
    
    "feature_ceiling_fan": "feature_ceiling_fan",
    "feature_ceiling_fans": "feature_ceiling_fan",
    "feature_allows_smoking": "smoking_allowed",
    "feature_smoking_allowed": "smoking_allowed",
    "feature_suitability_smoking_allowed": "smoking_allowed", 
    
    "feature_ski_in_ski_out": "feature_ski_in_ski_out",
    "location_ski_out": "feature_ski_in_ski_out",
    "location_ski_in": "feature_ski_in_ski_out",
    
    "feature_exterior_lighting": "feature_exterior_lighting",
    "feature_exterior_lighting": "feature_exterior_lighting",
    "feature_pdp_safety_exterior_lighting": "feature_exterior_lighting",
    "feature_safety_exterior_lighting": "feature_exterior_lighting",
    "feature_pdp_safety_safe": "feature_safe",
    "feature_safe": "feature_safe",
    
    "feature_kettle": "feature_hot_water_kettle",
    "feature_kitchen_coffee_maker": "feature_coffee_maker",
    "feature_keurig_coffee_machine": "feature_coffee_maker",
    "feature_pour_over_coffee": "feature_coffee_maker",
    "feature_nespresso_machine": "feature_coffee_maker",
    
    "feature_sound_system": "feature_sound_system",
    "feature_stereo": "feature_sound_system",
    "feature_record_player": "feature_sound_system",
    "feature_entertainment_all_ages_stereo": "feature_sound_system",
    "feature_cd_player": "feature_sound_system",
    "feature_entertainment_cd_player": "feature_sound_system",
    "feature_entertainment_stereo": "feature_sound_system",
    "feature_music_library": "feature_sound_system",
    "feature_entertainment_all_ages_music_library": "feature_sound_system",
    
    "feature_kayak": "feature_kayak",
    "feature_outside_kayak_canoe": "feature_kayak",
    "feature_kayak_canoe": "feature_kayak",
    "feature_outdoor_features_kayak_canoe": "feature_kayak",
    'feature_canoe': "feature_kayak",
    "feature_boat_slip": "feature_boat_slip",
    "feature_boat_dock": "feature_boat_slip",
    "feature_boat_pier": "feature_boat_slip",
    "feature_boat": "feature_boat",
    "feature_outside_boat": "feature_boat",
    "feature_outdoor_features_boat": "feature_boat",
    "feature_boat_with_paddles": "feature_boat",
    "feature_outdoor_features_boat": "feature_boat",
    "feature_outdoor_features_boat": "feature_boat",
    
    "feature_lanai_gazebo": "feature_gazebo",
    "feature_outside_lanai_gazebo": "feature_gazebo",
    "feature_gazebo": "feature_gazebo",
    "feature_outside_gazebo": "feature_gazebo",
    "feature_outdoor_features_lanai_gazebo": "feature_gazebo",
    "feature_outside_lanai_gazebo": "feature_gazebo",
    
    "feature_office": "feature_office",
    "feature_dedicated_workspace": "feature_office",
    
    "feature_24hr_checkin": "feature_self_checkin",
    "feature_self_checkin": "feature_self_checkin",
    "feature_self_check_in": "feature_self_checkin",
    
    "feature_host_checkin": "feature_host_checkin",
    "feature_host_greets_you": "feature_host_checkin",
    "feature_cleaning_before_checkout": "feature_cleaning_before_checkout",
    "feature_fitness_room_equipment": "location_recreation_center",
    "feature_general_fitness_room_equipment": "location_recreation_center",
    "feature_essential_fitness_room_equipment": "location_recreation_center",
    
    "feature_staff": "feature_building_staff",
    "feature_building_staff": "feature_building_staff",
    "feature_housekeeper_included": "feature_building_staff",
    "feature_onsite_services_staff": "feature_building_staff",
    "feature_onsite_services_concierge": "feature_concierge",
    "feature_concierge": "feature_concierge",
    "location_concierge": "feature_concierge",
    "location_concierge_service": "feature_concierge",
    "feature_onsite_services_private_chef": "feature_private_chef",
    "feature_private_chef": "feature_private_chef",
    "feature_chauffeur": "feature_chauffeur",
    "feature_onsite_services_chauffeur": "feature_chauffeur",
    
    "feature_zoo": "location_zoo",
    "feature_attractions_zoo": "location_zoo",
    "feature_bidet": "feature_bidet",
    
    "feature_forests": "location_rural",
    "feature_rain_forests": "location_rural",
    "feature_ruins": "location_ruins",
    "feature_kitchen_island": "feature_kitchen_island",
    "feature_flexcation_kitchen_island": "feature_kitchen_island",
    "location_kitchen_island": "feature_kitchen_island",
    
    "feature_rice_maker": "feature_rice_maker",
    "feature_bread_maker": "feature_bread_maker",
    "feature_baby_bath": "feature_baby_bath",
    "feature_window_guards": "feature_window_guards",
    "feature_changing_table": "feature_changing_table",
    "location_campus": "location_campus",
    
    "feature_minimum_1_day_vacancy_between_guest_stays": "feature_day_vacancy_between_stays",
    "feature_minimum_2_day_vacancy_between_guest_stays": "feature_day_vacancy_between_stays",
    "feature_minimum_3_day_vacancy_between_guest_stays": "feature_day_vacancy_between_stays",
    "feature_minimum_health_1_day_vacancy_between_guest_stays": "feature_day_vacancy_between_stays",
    "feature_minimum_health_2_day_vacancy_between_guest_stays": "feature_day_vacancy_between_stays",
    "feature_minimum_health_3_day_vacancy_between_guest_stays": "feature_day_vacancy_between_stays",
    "feature_health_minimum_1_day_vacancy_between_guest_stays": "feature_day_vacancy_between_stays",
    "feature_health_minimum_2_day_vacancy_between_guest_stays": "feature_day_vacancy_between_stays",
    "feature_health_minimum_3_day_vacancy_between_guest_stays": "feature_day_vacancy_between_stays",
    
    "feature_general_telephone": "feature_telephone",
    "feature_telephone": "feature_telephone",
    "feature_game_room": "feature_game_room",
    "feature_desk": "feature_desk",
    "feature_flexcation_desk": "feature_desk",
    "feature_printer": "feature_printer",
    "feature_flexcation_printer": "feature_printer",
    "feature_meal_delivery": "location_meal_delivery",
    "location_meal_delivery": "location_meal_delivery",
    "feature_piano": "feature_piano",
    "feature_eco_tourism": "location_eco_tourism",
    "feature_paid_wifi": "feature_paid_wifi",
    "location_bar": "location_bars",
    "location_bars": "location_bars",
    "location_pubs": "location_bars",
    "location_taverns": "location_bars",
    "feature_sofa_bed": "feature_sofa_bed",
    "feature_hide_a_bed": "feature_sofa_bed",
    "feature_hideabed": "feature_sofa_bed",
    "feature_hide_away_bed": "feature_sofa_bed",
    "feature_futon": "feature_sofa_bed",
    "location_resort": "location_resort",
    "location_village": "location_village",
    
    "feature_onsite_services_car_available": "feature_car_available",
    "feature_car_available": "feature_car_available",
    "feature_room_darkening_shades": "feature_room_darkening_shades",
    "location_near_ocean": "location_near_ocean",
    #"": "",
    
    #"feature_ceilings_vaulted": "feature_ceilings_vaulted_beamed",
    #"feature_ceilings_beamed": "feature_ceilings_vaulted_beamed",
    #"": "feature_extra_room",
    #"": "feature_extra_wiring",
    
    "feature_wine_glasses": None,
    "feature_cleaning_products": None,
    "feature_portable_fans": None,
    "feature_fireplace_guards": None,
    "feature_wifi_speed": None,
    "feature_water_sports_gear": None,
    "feature_kitchen_grill": None,
    "feature_bedroom_comforts": None,
    "feature_computer_monitor": None,
    "feature_fax": None,
    "feature_table_corner_guards": None,
    "feature_mosquito_net": None,
    "feature_safety_police_contact": None,
    "feature_flexcation_desk_chair": None,
    "location_etc": None,
    "feature_childcare": None,
    "feature_cabinet_locks": None,
    "feature_dining_amenities_seating": None,
    "location": None,
    "feature_radio": None,
    "feature_cleaned_with_disinfectant": None,
    "feature_flexcation_cabinet_locks": None,
    "feature_entertainment_all_ages_video_library": None,
    "feature_pool_toys": None,
    "feature_safety_card": None,
    "feature_local_maps": None,
    "feature_laundry_soap": None,
    "feature_essential_basic_soaps": None,
    "feature_enhanced_cleaning_practices": None,
    "feature_baking_sheet": None,
    "feature_dining_table": None,
    
    "feature_first_aid_kit": None,
    "feature_pdp_safety_first_aid_kit": None,
    "feature_safety_first_aid_kit": None,
    "feature_fire_extinguisher": None,
    "feature_pdp_safety_fire_extinguisher": None,
    "feature_safety_fire_extinguisher": None,
    "feature_carbon_monoxide_detector": None,
    "feature_carbon_monoxide_alarm": None,
    "feature_pdp_safety_carbon_monoxide_detector": None,
    "safety_carbon_monoxide_detector ": None,
    "feature_safety_carbon_monoxide_detector": None,
    
    "feature_vrbo_specific_content_house": None,
    "feature_vrbo_specific_content_condo": None,
    "feature_emergency_exit_route": None,
    "feature_safety_emergency_exit_route": None,
    "feature_lobster_pot": None,
    "feature_outlet_covers": None,
    "feature_desk_chair": None,
    "feature_ev_charger": "feature_ev_charger",
    "feature_exclusive_ev_car_charger": "feature_ev_charger",
    "feature_exclusive_ev_car_charger_ev_car_charger": "feature_ev_charger",
    "feature_fire_station_contact": None,
    "feature_police_contact": None,
    "feature_hospital_contact": None,
    "feature_safety_fire_station_contact": None,
    "feature_safety_hospital_contact": None,
    "feature": None,
    "feature_breakfast_meal_type_included_in_price": None,
    "feature_continental_breakfast_meal": None,
    "feature_continental_breakfast_meal_included_in_price": None,
    "feature_enhanced_cleaning_practices": None,
}


def clean_feature_name(s_feature):
    s_feature = re.sub(r"[-_ \\/.,\"\[\]:;'’]+", "_", s_feature.lower())
    s_feature = re.sub(r"(^_+|_+$)", "", s_feature)
    s_feature = re.sub(r"_(true|yes)$", "", s_feature, flags=re.I)
    
    s_feature = e_FEATURE_STDS.get(s_feature, s_feature)
    
    if s_feature is not None:
        if s_feature == "feature":
            feature = None
        elif s_feature.startswith((
            "location_waterfront_", "location_river_", "location_lake_water_", "location_lake_", "location_the_lake_",
            "location_beach_", "location_beachfront_", "location_oceanfront_", "location_sandy_beach_",
            "location_second_beach", "location_the_beach",
        )) or "waterfront" in s_feature:
            s_feature = "location_waterfront"
        elif s_feature.startswith(("location_water_view_", "location_beach_view_", "location_ocean_view",)):
            s_feature = "location_water_view"
        elif s_feature.startswith(("location_ski_in_", "location_mountain_ski_",)):
            s_feature = "feature_ski_in_ski_out"
        elif s_feature.startswith(("location_mountain_",)):
            s_feature = "location_mountain_view"
        elif s_feature.startswith(("location_downtown_",)):
            s_feature = "location_downtown"
        elif s_feature.startswith(("feature_leisure_activities_",)):
            s_feature = "location_adventure_activities"
        elif s_feature.startswith(("location_kayaking_",)):
            #s_feature = "location_kayak"
            s_feature = "location_boating"
        elif s_feature.startswith(("location_rural_",)):
            s_feature = "location_rural"
        elif s_feature.startswith(("location_resort_",)):
            s_feature = "location_resort"
        elif s_feature.startswith(("location_shopping_", "location_stores_", "location_boutiques_")) or "shopping" in s_feature:
            s_feature = "location_shopping"
        elif s_feature.startswith(("location_village_", "location_town_",)):
            s_feature = "location_village"
        elif s_feature.startswith(("location_marina_",)):
            #s_feature = "location_marina"
            s_feature = "location_boating"
        elif s_feature.startswith(("location_wildlife_",)):
            s_feature = "location_wildlife_viewing"
        elif s_feature.startswith(("location_golf_","location_golfing",)) or "golf_course" in s_feature or "location_golf" in s_feature:
            s_feature = "location_golf"
        elif "hot_tub" in s_feature or "jacuzzi" in s_feature:
            s_feature = "feature_jacuzzi"
        elif "restaurant" in s_feature:
            s_feature = "location_restaurants"
        elif "fitness_center" in s_feature or "gym" in s_feature:
            s_feature = "location_recreation_center"
        elif "laundromat" in s_feature or "coin_laundry" in s_feature or "laundry_facility" in s_feature:
            s_feature = "location_laundromat"
        elif "free_laundry" in s_feature or "site_laundry" in s_feature:
            s_feature = "location_laundry"
        elif "supermarket" in s_feature or "grocer" in s_feature or "food_market" in s_feature:
            s_feature = "location_groceries"
        elif "museum" in s_feature or "galleries" in s_feature:
            s_feature = "location_museums"
        elif "fishing" in s_feature:
            s_feature = "location_fishing"
        elif "theater" in s_feature or "theatre" in s_feature:
            s_feature = "location_theater"
        elif "near_the_ocean" in s_feature or "on_the_ocean" in s_feature:
            s_feature = "location_near_ocean"
        elif "water_ski" in s_feature or "jet_ski" in s_feature or "boat_launch" in s_feature:
            s_feature = "location_boating"
        elif "play_area" in s_feature or "playground" in s_feature:
            s_feature = "location_playground"
        elif "horseback" in s_feature or "horses_" in s_feature or "horse_rid" in s_feature:
            s_feature = "location_horseback_riding"
        elif "ping_pong" in s_feature or "foosball" in s_feature or "pool_table" in s_feature or "billiard" in s_feature:
            s_feature = "feature_games"
        elif "walking_distance" in s_feature:
            s_feature = "location_walking"
        
        elif "comfy_seating" in s_feature:
            s_feature = None
        elif "_own_meals" in s_feature:
            s_feature = None
        elif "vrbo_specific" in s_feature:
            s_feature = None
        elif "breakfast_meal_type_booking" in s_feature:
            s_feature = None
    
    return s_feature


def check_val(s_feature, value=None):
    if s_feature is None: return None
    if re.search("_false$", s_feature, flags=re.I): return False
    if s_feature in {"feature_pets_not_allowed", "feature_suitability_pets_not_allowed"}:
        return "pets_allowed", False if value == True else True if value == False else None
    if s_feature in {"feature_wheelchair_inaccessible", "feature_suitability_wheelchair_inaccessible"}: 
        return "feature_wheelchair_accessible", False if value == True else True if value == False else None
    if s_feature == "feature_check_in_and_check_out_with_no_person_to_person_contact":
        return "feature_host_checkin", False if value == True else True if value == False else None
        
    if s_feature in {"feature_suitability_not_suitable_for_children", "feature_not_suitable_for_children", "feature_children_not_allowed"}:
        return "feature_family_friendly", False if value == True else True if value == False else None
    
    if s_feature in {"feature_house_cleaning_housekeeper_included", "feature_house_cleaning_housekeeper_optional",}:
        return "feature_cleaning_before_checkout", False if value == True else True if value == False else None
    
    if s_feature in {"feature_non_smoking_only", "feature_suitability_non_smoking_only",}:
        return "feature_allows_smoking", False if value == True else True if value == False else None
        
    if s_feature in {"feature_limited_accessibility", "feature_unsuitable_for_elderly_or_infirm"}:
        return "feature_wheelchair_accessible", False if value == True else True if value == False else None
        
    return s_feature, value


def parse_description(s_description):
    if not isinstance(s_description, str) or not s_description: return {}
    e_description = {}
    for s_tag, re_search in e_DESCRIPTION_RE.items():
        a_matches = list(re_search.finditer(s_description))
        if a_matches:
            e_description[s_tag] = [re_match.group() for re_match in a_matches]
    return e_description


def parse_address(s_address):
    if not isinstance(s_address, str) or not s_address: return {}
    e_address_tags = {}
    for s_tag, re_search in e_ADDRESS_RE.items():
        a_matches = list(re_search.finditer(s_address))
        if a_matches:
            e_address_tags[s_tag] = [re_match.group() for re_match in a_matches]
    return e_address_tags


def get_desc_prop_type(e_description):
    for s_type in a_PROPERTY_TYPE_ORDER:
        if e_description.get(s_type): return s_type
    return None


def standardize_features(
    df_extended_file,
    a_text_cols = [
        "Property Type",],
    a_amount_cols = [
        "Bedrooms",
        "Bathrooms",
        "Max Guests",
        "Overall Rating",
        "Annual Revenue LTM (USD)", "Published Nightly Rate (USD)", "Occupancy Rate LTM",
        "Response Rate", "Number of Reviews", "Number of Photos",],
    a_bool_cols = [
        "Pets Allowed", "Exact Location",],
    a_list_cols = [
        ("Amenities", "feature"),
        ("HomeAway Location Type", "location")],
    a_desc_cols = ["Listing Title"],
):
    e_types = {"ab": "airbnb", "ha": "homeaway"}
    
    e_features = defaultdict(lambda: defaultdict(lambda: defaultdict()))
    #e_features["ha"]["Dining"]
    
    a_prop_info_rows = []
    i_total_rows = len(df_extended_file)
    for i_row, (s_prop, row) in enumerate((df_extended_file.set_index("Property ID").iterrows())):
        if i_row > 0 and (i_row % 10000 == 0 or i_row == i_total_rows):
            print(f"\tRow {i_row:,}/{i_total_rows:,} ({(i_row + 1) / i_total_rows:0.1%})")
        e_row = {"Property ID": s_prop}
        s_type = ""
        if s_prop.startswith("ab-"):
            e_row["airbnb"] = True
            s_type = "airbnb"
        elif s_prop.startswith("ha-"):
            e_row["homeaway"] = True
            s_type = "homeaway"
        
        for col in a_text_cols:
            if pd.isnull(row[col]): continue
            s_col = clean_feature_name(col)
            if s_col is not None:
                e_row[s_col] = str(row[col])
        
        for col in a_amount_cols:
            if pd.isnull(row[col]): continue
            s_col = clean_feature_name(col)
            if s_col is not None:
                if s_col == "overall_rating":
                    e_row[s_col] = round(float(row[col]) * 2, -1) / 2
                else:
                    e_row[s_col] = float(row[col])
        
        for col in a_bool_cols:
            if pd.isnull(row[col]): continue
            s_col = clean_feature_name(col)
            if s_col is not None:
                e_row[s_col] = bool(row[col])
        
        for col, std in a_list_cols:
            if pd.isnull(row[col]): continue
            try:
                features = json.loads(row[col])
                b_alt = False
            except:
                #features = FwFreq.flatten(list(csv.reader([re.sub(r"(^\{|\}$)", "", row[col])])))
                features = utils.flatten(list(csv.reader([re.sub(r"(^\{|\}$)", "", row[col])])))
                b_alt = True
            
            if isinstance(features, list):
                for s_val in features:
                    s_feature = clean_feature_name(f"{std}_{s_val}")
                    
                    if not s_feature: continue
                    s_feature, e_row[s_feature] = check_val(s_feature, True)
                    
                    if e_row[s_feature] is not None and e_features[s_type].get(s_val) != True:
                        e_features[s_type][s_val] = True
                #break
            elif isinstance(features, dict):
                for x, y in features.items():
                    if isinstance(y, str):
                        s_y = re.sub(r"(^[\[\" ']+|[\"' \]]+$)", "", y)
                        if s_y.lower() in {"true",  "yes"}:
                            y = True
                        elif y in {"no", "false"}:
                            y = False
                        elif re.search(r"^\d+(,\d+)?(\.\d+)?$"):
                            y = utils.to_float(s_y)
                    
                    if isinstance(y, str):
                        print(col, x, y)
                        s_feature = clean_feature_name(f"{std}_{x}_{y}")
                        if not s_feature: continue
                        s_feature, e_row[s_feature] = check_val(s_feature, True)
                        
                        if e_row[s_feature] is not None and e_features[s_type].get(x, {}).get(y) != True:
                            e_features[s_type][x][y] = e_row[s_feature]
                        raise Exception("break")
                        
                    elif isinstance(y, list):
                        #print(col, x, y)
                        for z in y:
                            #e_row[f"{std}_{x}_{z}"] = True
                            #e_row[clean_feature_name(f"{std}_{z}")] = True
                            #e_features[s_type][x][z] = True
                            #e_features[s_type][z] = True
                            
                            if z in {True, "true", "True"}:
                                s_feature = clean_feature_name(f"{std}")
                            else:
                                s_feature = clean_feature_name(f"{std}_{z}")
                            
                            if not s_feature: continue
                            s_feature, e_row[s_feature] = check_val(s_feature, True)
    
                            if e_row[s_feature] is not None and e_features[s_type].get(z) != True:
                                e_features[s_type][z] = e_row[s_feature]
                                
                    elif isinstance(y, dict):
                        print(col, x, y)
                        #e_row[clean_feature_name(f"{std}_{x}")] = y
                        #e_features[s_type][x][y] = True
                        
                        if x in {True, "true", "True"}:
                            s_feature = clean_feature_name(f"{std}")
                        else:
                            s_feature = clean_feature_name(f"{std}_{x}")
                        if not s_feature: continue
                        s_feature, e_row[s_feature] = check_val(s_feature, y)
    
                        if e_row[s_feature] is not None and e_features[s_type].get(x, {}).get(y) != True:
                            e_features[s_type][x][y] = e_row[s_feature]
                        
                        raise Exception("break")
                        
                    elif y in {True, False}:
                        e_row[clean_feature_name(f"{std}_{x}")] = True
                        
                        s_feature = clean_feature_name(f"{std}_{x}")
                        if not s_feature: continue
                        s_feature, e_row[s_feature] = check_val(s_feature, True)
                        
                        if e_row[s_feature] is not None:
                            if b_alt:
                                if e_features[s_type].get("_alt_", {}).get(x) != True:
                                    e_features[s_type]["_alt_"][x] = e_row[s_feature]
                            else:
                                if e_features[s_type].get(x) != True:
                                    e_features[s_type][x] = e_row[s_feature]
                    
                        
                    else:
                        #e_row[clean_feature_name(f"{std}_{s_val}")] = True
                        #e_features[s_type][s_val] = True
                        
                        s_feature = clean_feature_name(f"{std}_{s_val}")
                        if not s_feature: continue
                        s_feature, e_row[s_feature] = check_val(s_feature, True)
    
                        if e_row[s_feature] is not None and e_features[s_type].get(s_val) != True:
                            e_features[s_type][s_val] = e_row[s_feature]
                        raise Exception("break")
            
            #for s_val in str(row[col]).split(","):
            #    s_val = re.sub(r"(^[\[\" ']+|[\"' \]]+$)", "", s_val)
            #    e_row[f"{std}{s_val}"] = True
        
        for col in a_desc_cols:
            if pd.isnull(row.get(col)): continue
            e_desc = parse_description(row[col])
            
            if not e_desc:
                e_row["insufficient_description"] = max(1, e_row.get("insufficient_description", 0))
            elif len(e_desc) == 1:
                e_row["insufficient_description"] = max(0.75, e_row.get("insufficient_description", 0))
            elif len(e_desc) == 2:
                e_row["insufficient_description"] = max(0.55, e_row.get("insufficient_description", 0))
            #if e_row.get("insufficient_description"):
            #    print('e_row["insufficient_description"]', e_row["insufficient_description"], e_desc)
            
            #elif len(e_desc) == 3:
            #    e_row["insufficient_description"] = 0.40
            #elif len(e_desc) == 4:
            #    e_row["insufficient_description"] = 0.25
            #elif len(e_desc) == 5:
            #    e_row["insufficient_description"] = 0.10
            #elif len(e_desc) == 6:
            #    e_row["insufficient_description"] = 0.05
            #elif len(e_desc) == 7:
            #    e_row["insufficient_description"] = 0.025
            #else:
            #    e_row["insufficient_description"] = 0.025
            
            #print("parse description", row[col], e_desc)
            
            for col, a_val in e_desc.items():
                if col in c_PROPERTY_TYPE_ORDERS: continue
                a_val = [x for x in a_val if x]
                
                if col in ("fixer_upper"):
                    e_row[col] = 3 if a_val else np.nan
                    continue
                        
                if a_val:
                    if col in ("living_area", "lot_size"):
                        if col == "living_area":
                            e_row[col] = clean_living_area(a_val[0])
                        elif col == "lot_size":
                            e_row[col] = clean_lot_size(a_val[0])
                    else:
                        #e_features[col] = ", ".join(a_val)
                        e_row[col] = True
            
            #s_col = clean_feature_name(col)
            #if s_col is not None:
            #   e_row[s_col] = bool(row[col])
        
        if e_row.get("Property Type") in {"Rancher", "rancher"} and e_row.get("feature_single_level_home") != False:
            e_row["feature_single_level_home"] = True
        
        a_prop_info_rows.append(e_row)
        #break
    return a_prop_info_rows


def clean_living_area(living_area, d_min_sqft=50, d_max_sqft=100000):
    if pd.isnull(living_area): return np.nan
    
    
    if isinstance(living_area, str):
        living_area = re.sub(r"\. ", "", living_area)
        d_area = utils.to_float(living_area)

        if not re.search(r"(sq\.?[- /]?ft\.?|square[- /]?(foot|feet))", living_area, flags=re.I):
            if re.search(r"(ac|acre)", living_area, flags=re.I) or d_area < 2:
                d_area /= 0.00002295682
            if re.search(r"(sq\.?[- /]?m\.?|square[- /]?(meter|metre))", living_area, flags=re.I) or d_area < 2:
                d_area /= 0.09290304
        if d_area <= d_min_sqft or d_area >= d_max_sqft:
            return np.nan
        return round(d_area, 4)
    else:
        if living_area <= d_min_sqft or living_area >= d_max_sqft:
            return np.nan
        elif living_area < 2:
            living_area /= .00002295682
        return round(living_area, 4)


def clean_lot_size(lot_size, d_min_acre=0.01, d_max_acre=50000):
    if pd.isnull(lot_size): return np.nan
    
    if isinstance(lot_size, str):
        d_area = utils.to_float(lot_size)
        
        
        if not re.search(r"(ac|acre)", lot_size, flags=re.I):
            if re.search(r"(sq\.?[- /]?ft\.?|square[- /]?(foot|feet))", lot_size, flags=re.I) or d_area > 1000:
                d_area *= 0.00002295682
            elif re.search(r"(sq\.?[- /]?m\.?|square[- /]?meter)", lot_size, flags=re.I) or d_area > 1000:
                d_area *= 0.0002471052
        if d_area <= d_min_acre or d_area >= d_max_acre: return np.nan
        return round(d_area, 4)
    else:
        if lot_size <= d_min_acre or lot_size >= d_max_acre:
            return np.nan
        elif lot_size > 5000:
            lot_size *= .00002295682
        return round(lot_size, 4)
    #df_features = pd.DataFrame(a_prop_info_rows).set_index("Property ID")
    #print(f"{df_features.shape[0]:,} property rows with {df_features.shape[1]:,} features")
    #return df_features


def find_common_features(df_features, d_minimum_coverage=0.025, c_known_features=None, c_always_include={}, c_always_exclude={}):
    e_col_feature_values = {}
    i_coverage, i_req = 0, 0
    for col in df_features.columns:
        if col in c_always_exclude: continue
        e_col_counts = dict(df_features[col].value_counts())  
        i_uniq = len(e_col_counts.values())
        i_vals = sum(e_col_counts.values())
        if i_vals >= d_minimum_coverage * len(df_features):
            i_coverage += 1
            e_col_feature_values[col] = e_col_counts
        elif col in c_always_include:
            i_req += 1
            e_col_feature_values[col] = e_col_counts
            #if col not in e_FEATURE_STDS and (c_known_features is None or col not in c_05_features):
            #    print(col, i_uniq, i_vals
    print(f"{len(e_col_feature_values):,} common features. {i_coverage} with coverage >= {d_minimum_coverage:0.2%}, and {i_req} always included")
    return e_col_feature_values


def filter_common_features(df_features, e_col_feature_values):
    df_common_features = df_features[e_col_feature_values.keys()].copy()
    return df_common_features


def get_feature_col_report(e_col_feature_values):
    e_col_reports = {}
    i_max = int(max([len(x) for x in e_col_feature_values.values()]))
    print(i_max)
    for s_col, e_vals in e_col_feature_values.items():
        a_values, a_counts = map(list, zip(*list(e_vals.items())))
        a_values += [None] * (i_max - len(a_values))
        a_counts += [None] * (i_max - len(a_counts))
        e_col_reports[("Values", f"{s_col}")] = a_values
        e_col_reports[("Counts", f"{s_col}")] = a_counts
        #break
    df_feature_col_reports = pd.DataFrame.from_dict(e_col_reports)
    return df_feature_col_reports


def average_area_revenues(df_inputs, s_rev="adj_revenue", s_lat="Latitude", s_long="Longitude", i_max_round=-1, i_min_round=8):
    df_prop_revenues = df_inputs[[s_rev, s_lat, s_long]].copy()
    df_prop_revenues["properties"] = 1
    a_area_cols = []
    for i_area in range(i_max_round, i_min_round + 1):
        s_area_col = f"area_{i_area}"
        s_area_rev_col = f"area_rev_{i_area}"

        df_prop_revenues[s_area_col] = [f"{lat},{long}" for lat, long in zip(df_prop_revenues[s_lat].round(i_area), df_prop_revenues[s_long].round(i_area))]
        a_area_cols.append((s_area_col, s_area_rev_col))
    
    e_all_area_revenues = {}
    for s_area_col, s_area_rev_col in a_area_cols:
        df_area = df_prop_revenues.groupby(s_area_col).agg({"adj_revenue": 'median', "properties": np.sum}).reset_index()
        #print(df_area.columns)
        e_area_avg_revenues = {
            area: rev
            for idx, area, rev, props in df_area.itertuples()
            if props >= 3
        }

        df_prop_revenues[s_area_rev_col] = df_prop_revenues[s_area_col].apply(e_area_avg_revenues.get).fillna(0)
        e_all_area_revenues.update(e_area_avg_revenues)
    
    df_prop_revenues["area_revenue"] = df_prop_revenues[[s_area_rev_col for s_area_col, s_area_rev_col in a_area_cols]].mean(axis=1)
    df_prop_revenues.drop([x[0] for x in a_area_cols] + [x[1] for x in a_area_cols], axis=1, errors="ignore")
    return df_prop_revenues, e_all_area_revenues


def prep_for_dummies(df_input):
    df_output = df_input.copy()
    for col in df_output.columns:
        e_vals = dict(df_output[col].value_counts())
        if len(e_vals) > 2:
            continue
        elif all(x in {True, False, "True", "False", "TRUE", "FALSE", "yes", "YES", "Yes", "no", "NO", "No"} for x in e_vals.keys()):
            df_output[col] = df_output[col].apply(lambda x: 1 if x in {True, "True", "TRUE", "true", "yes", "YES", "Yes", 1, 1.0, "1"} else 0)
    
    #print(df_output)
    #assert "price" in list(df_output.columns)
    return pd.get_dummies(df_output)


def myround(x, prec=2, base=.05):
    return round(base * round(float(x) / base),prec)


def modulus_subdivisions(d_min, d_max, n_subdivisions=8, d_subdivision_ratio=0.5, **kwargs):
    d_diff = d_max - d_min
    a_mods = []
    for i in range(n_subdivisions):
        a_mods.append(d_diff)
        d_diff *= d_subdivision_ratio
    return a_mods


def subdivide_by_modulus(x, a_mods, d_min, **kwargs):
    if not isinstance(x, float):
        print(x)
    d_from_min = x - d_min
    a_subs = []
    for d_mod in a_mods:
        d_sub = (d_from_min % d_mod) / d_mod
        #d_sub = (d_from_min - d_mod)
        #print(d_from_min, d_mod, d_sub)
        a_subs.append(d_sub)
    return a_subs


def subdivide_latitude_longitude_cols(df, d_min_lat, d_max_lat, d_min_long, d_max_long, lat_col="latitude", long_col="longitude", b_copy=False, **kwargs):
    if b_copy:
        df = df.copy()
    #d_min_lat = min_lat # df[lat_col].min()
    #d_max_lat = max_lat #, df[lat_col].max()
    #d_min_long = min_long # df[long_col].min()
    #d_max_long = max_long# , df[long_col].max()

    a_lat_mods = modulus_subdivisions(d_min_lat, d_max_lat, **kwargs)
    a_lat_subs = list(df[lat_col].apply(lambda x: subdivide_by_modulus(x, a_lat_mods, d_min_lat, **kwargs)))
    df_lat_subs = pd.DataFrame(a_lat_subs)
    a_lat_sub_cols = [f"lat_sub_{i}" for i in range(len(df_lat_subs.columns))]
    df_lat_subs.columns = a_lat_sub_cols

    a_long_mods = modulus_subdivisions(d_min_long, d_max_long, **kwargs)
    a_long_subs = list(df[long_col].apply(lambda x: subdivide_by_modulus(x, a_long_mods, d_min_long, **kwargs)))
    df_long_subs = pd.DataFrame(a_long_subs)
    a_long_sub_cols = [f"long_sub_{i}" for i in range(len(df_long_subs.columns))]
    df_long_subs.columns = a_long_sub_cols
    
    for col in df_lat_subs.columns:
        df[col] = list(df_lat_subs[col])
    for col in df_long_subs.columns:
        df[col] = list(df_long_subs[col])
    
    d_min_lat, d_max_lat
    
    return df, a_lat_sub_cols, a_long_sub_cols


def subdivide_latitude_longitude_dict(e_input, d_min_lat, d_max_lat, d_min_long, d_max_long, lat_col="latitude", long_col="longitude", b_copy=False, **kwargs):
    if b_copy:
        e_input = e_input.copy()
    
    lat = e_input[lat_col]
    long = e_input[long_col]
    #d_min_lat, d_max_lat = df[lat_col].min(), df[lat_col].max()
    #d_min_long, d_max_long = df[long_col].min(), df[long_col].max()

    a_lat_mods = modulus_subdivisions(d_min_lat, d_max_lat, **kwargs)
    a_lat_subs = subdivide_by_modulus(lat, a_lat_mods, d_min_lat, **kwargs)
    e_lat_subs = {
        col: x for x, col in zip(
            a_lat_subs, [f"lat_sub_{i}" for i in range(len(a_lat_subs))])}

    a_long_mods = modulus_subdivisions(d_min_long, d_max_long, **kwargs)
    a_long_subs = subdivide_by_modulus(long, a_long_mods, d_min_long, **kwargs)
    e_long_subs = {
        col: x for x, col in zip(
            a_long_subs, [f"long_sub_{i}" for i in range(len(a_long_subs))])}
    
    return e_lat_subs, e_long_subs


def standardize_checkin_time(x):
    if pd.isnull(x): return None
    
    if isinstance(x, str) and re.search(r"^\d(\.\d+)?$", x):
        x = utils.to_float(x)
    
    if isinstance(x, (float, int)):
        i_hours = int(x)
        i_minutes = round((x - i_hours) * 60)
        dt = dt_time(hour=i_hours, minute=i_minutes)
        if i_minutes >= 15 and i_minutes <= 45:
            x = "{}".format(dt.strftime("%I:%M%p"))
        else:
            x = "{}".format(dt.strftime("%I%p"))
    elif isinstance(x, str):
        x = re.sub(r"(\((noon|midnight|next day)\)|:00 *|Anytime *|after *|%\{time\}|%\{start_time\}|%\{end_time\})", "", x.strip(), flags=re.I).strip() #.upper()
    x = re.sub(r"^0", "", x)
    if re.search(r"\d", x) and not re.search("(AM|PM)", x, flags=re.I):
        d_hour = int(re.search(r"^\d+", x).group(0))
        if 6 < d_hour < 12:
            x = f"{x}AM"
        else:
            x = f"{x}PM"
    if not re.search("[a-zA-Z0-9]", x): return None
    x = re.sub(r"  +", " ", x)
    return x


def standardize_checkout_time(x):
    if pd.isnull(x): return None
    
    if isinstance(x, str) and re.search(r"^\d(\.\d+)?$", x):
        x = utils.to_float(x)
    
    if isinstance(x, (float, int)):
        i_hours = int(x)
        i_minutes = round((x - i_hours) * 60)
        dt = dt_time(hour=i_hours, minute=i_minutes)
        if i_minutes >= 15 and i_minutes <= 45:
            x = "{}".format(dt.strftime("%I:%M%p"))
        else:
            x = "{}".format(dt.strftime("%I%p"))
    elif isinstance(x, str):
        x = re.sub(r"(\((noon|midnight|next day)\)|:00 *|Anytime *|after *|%\{time\}|%\{start_time\}|%\{end_time\})", "", x.strip(), flags=re.I).strip() #.upper()
    x = re.sub(r"^0", "", x)
    if re.search(r"\d", x) and not re.search("(AM|PM)", x, flags=re.I):
        d_hour = int(re.search(r"^\d+", x).group(0))
        if d_hour <= 9:
            x = f"{x}PM"
        else:
            x = f"{x}AM"
    if not re.search(r"[a-zA-Z0-9]", x): return None
    x = re.sub(r"  +", " ", x)
    return x