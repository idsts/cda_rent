﻿
e_ENDPOINTS = {
    "EndPoints": {
        "url": "/endpoints",
        "methods": {
            "GET": {
                "description": "To list the available endpoints",
                "header": {},
                "arguments": {},
            },
        }
    },
    "Upload-Zillow-CSV-Batch": {
        "url": "/input/batch",
        "methods": {
            "POST": {
                "description": f"Upload a zillow csv file to start running the predictions for.",
                "header": {"api_key": "text of the API Key with permissions."},
                "arguments": {"file": "a csv file containing zillow properties"},
            },
        },
    },
    "Upload-Zillow-URL-Batch": {
        "url": "/input/url",
        "methods": {
            "POST": {
                "description": f"Upload an apify url for zillow dataset file to start running the predictions for.",
                "header": {"api_key": "text of the API Key with permissions."},
                "arguments": {"url": "an apify url containing a dataset of zillow properties"},
            },
        },
    },
    #"Get-Prediction-Batch": {
    #    "url": "/output/batch",
    #    "method": "GET",
    #    "description": "Get the result csv file for a predicted Zillow batch.",
    #    "header": {"api_key": "text of the API Key with permissions."},
    #    "arguments": {},
    #},
    "Recent-Predictions": {
        "url": "/predictions",
        "methods": {
            "GET": {
                "description": "See recent prediction result files.",
                "header": {"api_key": "text of the API Key with permissions."},
                "arguments": {},
            },
        },
    },
    "Single Property Predictor": {
        "url": "/interactive",
        "methods": {
            "GET": {
                "description": "An example form with required and optional property information in order to make a short-term rental revenue projection.",
                "header": {"api_key": "text of the API Key with permissions."},
                "arguments": {},
            },
            "POST": {
                "description": """
                    Takes a json dict of required and optional property information in order to make a short-term rental revenue projection.
                    See /interactive/examples for examples of both the minimum required fields, and all the potential optional fields that could be included.
                    Field names are all lowercase. Either address or latitude/longitude are required, but you don't have to include both.
                    Excluded features and location values will default to whatever is most common in the property's area.
                    Excluded amenities default to true.
                    Excluded conditions default to false.
                    """,
                "header": {"api_key": "text of the API Key with permissions."},
                "arguments": {
                    "property_type": "{str} of property type (e.g. House, Condo, etc)",
                    "bedrooms": "{int} of number of bedrooms",
                    "bathrooms": "{float} of number of bathrooms",
                    "address": "{str} of the street address of the property (required if latitude/longitude are missing)",
                    "latitude": "{float} the latitude of the property (required if address is missing)",
                    "longitude": "{float} the longitude of the property (required if address is missing)",
                    "price": "{float} a recent sale price or estimated value of the property in dollars",
                    "living_area": "{float} the interior living space of the property in square feet",
                    "lot_size": "{float} the property's lot size in acres",
                    "year_built": "{int} the year the structure was built}",
                    "options": "{dict} of options that a owner will choose for short-term renting",
                    "features": "{dict} of features about the structure",
                    "locations": "{dict} of location information about things nearby the property",
                    "amenities": "{dict} of services that a owner will provide for short-term rental guests",
                    "financing": "{dict} of values that will override the defaults for calculating the loan and debt service information",
                    "conditions": "{dict} of values about the condition of the structure",
                },
            },
        },
    },
    
    "Comparable Property Searcher": {
        "url": "/comps",
        "methods": {
            "GET": {
                "description": "An example form with required and optional property information in order to find the most comparable properties in the surrounding area.",
                "header": {"api_key": "text of the API Key with permissions."},
                "arguments": {},
            },
            "POST": {
                "description": """
                    Takes a json dict of required and optional property information in order to find the most comparable properties in the surrounding area.
                    See /interactive/examples for examples of both the minimum required fields, and all the potential optional fields that could be included.
                    Field names are all lowercase. Either address or latitude/longitude are required, but you don't have to include both.
                    Excluded features and location values will default to whatever is most common in the property's area.
                    Excluded amenities default to true.
                    Excluded conditions default to false.
                    """,
                "header": {"api_key": "text of the API Key with permissions."},
                "arguments": {
                    "show_as_table": "{bool} to optionally return the results as an html table for easier debugging",
                    "property_type": "{str} of property type (e.g. House, Condo, etc)",
                    "bedrooms": "{int} of number of bedrooms",
                    "bathrooms": "{float} of number of bathrooms",
                    "address": "{str} of the street address of the property (required if latitude/longitude are missing)",
                    "latitude": "{float} the latitude of the property (required if address is missing)",
                    "longitude": "{float} the longitude of the property (required if address is missing)",
                    "price": "{float} a recent sale price or estimated value of the property in dollars",
                    "living_area": "{float} the interior living space of the property in square feet",
                    "lot_size": "{float} the property's lot size in acres",
                    "year_built": "{int} the year the structure was built}",
                    "options": "{dict} of options that a owner will choose for short-term renting",
                    "features": "{dict} of features about the structure",
                    "locations": "{dict} of location information about things nearby the property",
                    "amenities": "{dict} of services that a owner will provide for short-term rental guests",
                    "financing": "{dict} of values that will override the defaults for calculating the loan and debt service information",
                    "conditions": "{dict} of values about the condition of the structure",
                },
            },
        },
    },
    "Single Locale Lookup": {
        "url": "/locale_lookup",
        "methods": {
            "POST": {
                "description": f"Look up the information for specific locale of state/city/zipcode if the databases contain enough data about it",
                "header": {"api_key": "text of the API Key with permissions."},
                "arguments": {
                    "zipcode": "{int} the 5 digit zipcode of the locale. Use to return a zipcode locale.",
                    "city": "{str} the city name of the locale. Use to return a city locale.",
                    "state": "{str} the state name of the locale. Use to return a state locale, or together with city to disambiguate between towns of similar names across locations.",
                    "min_properties": "{int} optionally specify the minimum number of rental properties in a given locale to be considered sufficient information",
                    "bedrooms": "{int} optionally specify the number of bedrooms to get only the locale's information for properties with that number of bedrooms",
                    "property_type": "{str} optionally specify the property type (e.g. House, Condo, etc). This isn't active yet as a filter, but will be when the database tables are created for it",
                },
            },
        },
    },
    "Multiple Locales Lookup": {
        "url": "/locales_lookup",
        "methods": {
            "POST": {
                "description": f"Look up the information for multiple locales of state/city/zipcode if the databases contain enough data about them",
                "header": {"api_key": "text of the API Key with permissions."},
                "arguments": {
                    "locales": [
                        {
                            "zipcode": "{int} the 5 digit zipcode of the locale. Use to return a zipcode locale.",
                            "city": "{str} the city name of the locale. Use to return a city locale. Preferably also include a state to make sure the correct locale is selected when the same city/town name exists in multiple places.",
                            "state": "{str} the state name of the locale. Use to return a state locale, or together with city to disambiguate between towns of similar names across locations.",
                            "bedrooms": "{int} optionally specify the number of bedrooms to get only the locale's information for properties with that number of bedrooms",
                            "property_type": "{str} optionally specify the property type (e.g. House, Condo, etc). This isn't active yet as a filter, but will be when the database tables are created for it",
                        }
                    ],
                    "min_properties": "{int} optionally specify the minimum number of rental properties in a given locale to be considered sufficient information",
                },
            },
        },
    },
}
