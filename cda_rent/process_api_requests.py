﻿import os, sys, re, shutil, time
from datetime import datetime, timedelta
import dateutil
import requests, json
import pandas as pd
import logging
import traceback

try:
    from . import utils, paths, log
    from . import prep_zillow
    from . import predict_zillow
    #from .paths import *
    from .log import LOGGER
    from . import aws_ses
except:
    import utils, paths, log
    import prep_zillow
    import predict_zillow
    #from paths import *
    from log import LOGGER
    import aws_ses

if LOGGER is None:
    LOGGER = log.LOGGER

#s_TARGET_URL = "https://system.runrevr.com/api/listings/predictions"
s_TARGET_URL = "https://system.revr.com/api/listings/predictions"
s_TARGET_METHOD = "post"
b_SEND_RESULT = False
b_EMAIL_RESULT = True
s_EMAIL_RECIPIENT = "timothy@idsts.com"

b_COMBINED_OUTPUT=False
i_MAX_BATCH_SIZE = 1000 #split working files into batches of this in to process separately to reduce peak RAM load

c_EXTS = {".xlsx", ".csv"}


#print("Checking processor log file")
#filename = os.path.splitext(__file__)[0]
#s_LOG_FILE = os.path.join(os.getcwd(), f'{filename}.log')
#utils.check_log_file(s_LOG_FILE)
#
#logging.basicConfig(filename=s_LOG_FILE, level=os.environ.get("LOGLEVEL", "INFO"))
#LOGGER = logging.getLogger("IDSTS_CDA_REVENUE_PREDICTIONS")


def download_apify_zillow_dataset(s_url, b_force_csv=True):
    if b_force_csv:
        s_url = re.sub(r"(?<=format=)[^&]+", "csv", s_url, flags=re.I)
        s_format = "csv"
    else:
        a_formats = re.findall(r"(?<=format=)[^&]+", s_url, flags=re.I)
        s_format = a_formats[0]
    
    a_datasets = re.findall(r"(?<=datasets/)[^/]+", s_url, flags=re.I) + []
    s_file = a_datasets[0] if a_datasets else datetime.now().strftime("%Y%m%d_%H%M")
    
    s_path = utils.download_file(s_url, filename=os.path.join(paths.s_INPUT_PATH, f"{s_file}.{s_format}"), b_skip_headers=True)
    return s_path


def check_input(logger=None, **kwargs):
    # check of a new file in the input directory
    # if there is one, move it to working and start processing it
    print("Checking input folder")
    if logger is None: logger = LOGGER
    #if logger is not None: logger.info(f"{utils.get_time()}\tChecking input folder")
    
    for (s_dirpath, a_dirnames, a_filenames) in os.walk(paths.s_INPUT_PATH):
        for s_file in a_filenames:
            if os.path.splitext(s_file.lower())[1] in c_EXTS:
                
                s_path = os.path.join(s_dirpath, s_file)
                s_working_path = os.path.join(paths.s_WORKING_PATH, s_file)
                s_complete_path = os.path.join(paths.s_COMPLETE_PATH, s_file)
                s_fail_path = os.path.join(paths.s_FAILED_PATH, s_file)
                
                shutil.move(s_path, s_working_path)
                s_filename, s_ext = os.path.splitext(s_file)
                
                s_output_path = os.path.join(paths.s_PREDICTION_PATH, f"{s_filename}_predictions.csv")
                if os.path.isfile(s_output_path): os.remove(s_output_path)
                
                s_combined_path = os.path.join(paths.s_PREDICTION_PATH, f"{s_filename}_inputs_with_prediction_summary.csv")
                if os.path.isfile(s_combined_path): os.remove(s_combined_path)
                
                a_result_paths = []
                if s_ext.lower() in {".csv", ".txt"}:
                    z_working = pd.read_csv(
                        s_working_path,
                        chunksize=i_MAX_BATCH_SIZE,
                        encoding=utils.encoding_tester(s_working_path),
                        low_memory=False,
                        dtype=prep_zillow.e_ZILLOW_DTYPE,
                    )
                    for i_chunk, df_chunk in enumerate(z_working):
                        print(f"Processing chunk {i_chunk} in file \"{s_filename}_{i_chunk}.csv\"")
                        if logger is not None: logger.info(f"{utils.get_time()}\tProcessing chunk {i_chunk} in file \"{s_filename}_{i_chunk}.csv\"")
                        s_chunk_path = os.path.join(paths.s_WORKING_PATH, f"{s_filename}_{i_chunk}{s_ext}")
                        df_chunk.to_csv(s_chunk_path, encoding="utf-8-sig", index=False)
                        b_success = process_one_input(s_chunk_path, s_output_path=s_output_path, s_combined_path=s_combined_path, logger=logger)
                        a_results.append(b_success)
                        
                        if b_success:
                            os.remove(s_chunk_path)
                        df_chunk = None
                    
                    if all(a_results):
                        if kwargs.get("move_to_complete", True):
                            shutil.move(s_working_path, s_complete_path)
                        return s_output_path
                        
                        e_email_result = aws_ses.send_email(
                            [s_EMAIL_RECIPIENT],
                            f"Completed processing {s_file}",
                            f"""Processed {s_file}.\n\nDownload:\nhttp://predictor.revr.com/files/predictions/{s_filename}_predictions.csv?api_key=6c8a-016e-e0cb-f008-cbad-a621-1d06-3079""",
                            cc=[],
                            bcc=[],
                            boto=True,
                        )
                        
                    else:
                        if kwargs.get("move_to_failed"):
                            shutil.move(s_working_path, s_fail_path)
                        
                        i_fail = len([x for x in a_results if not x])
                        i_success = len([x for x in a_results if x])
                        print(f"Didn't complete all chunks successfully. {i_success} processed and {i_fail} failed.")
                        
                        e_email_result = aws_ses.send_email(
                            [s_EMAIL_RECIPIENT],
                            f"Failed to process {s_file}",
                            f"""Didn't complete all chunks successfully of {s_file}.
{i_success} processed and {i_fail} failed.\n
Check files:\nhttp://predictor.revr.com/files?api_key=6c8a-016e-e0cb-f008-cbad-a621-1d06-3079""",
                            cc=[],
                            bcc=[],
                            boto=True,
                        )
                        if os.path.isfile(s_output_path):
                            return s_output_path
                        else:
                            return None
                else:
                    print(f"Processing file \"{s_file}\"")
                    if logger is not None: logger.info(f"{utils.get_time()}\tProcessing file \"{s_file}\"")
                    b_success = process_one_input(s_working_path, s_output_path=s_output_path, s_combined_path=s_combined_path, logger=logger)
                    if b_success:
                        if kwargs.get("move_to_complete", True):
                            shutil.move(s_working_path, s_complete_path)
                    return s_output_path
                
                break # only process first file
        break # ignore subfolders
    
    clear_old(paths.s_INPUT_PATH, i_max_days=7, i_max_files=100, logger=logger)
    clear_old(paths.s_COMPLETE_PATH, i_max_days=7, i_max_files=50, logger=logger)
    clear_old(paths.s_PREDICTION_PATH, i_max_days=30, i_max_files=100, logger=logger)
        
    return None


def process_one_input(s_working_path, s_output_path=None, s_combined_path=None, **kwargs):
    print("Start processing", s_working_path)
    s_folder, s_file = os.path.split(s_working_path)
    s_filename, s_ext = os.path.splitext(s_file)
    if not s_output_path: s_output_path = os.path.join(paths.s_PREDICTION_PATH, f"{s_filename}_predictions.csv")
    if not s_combined_path: s_combined_path = os.path.join(paths.s_PREDICTION_PATH, f"{s_filename}_inputs_with_prediction_summary.csv")
    #s_complete_path = os.path.join(paths.s_COMPLETE_PATH, s_file)
    s_fail_path = os.path.join(paths.s_FAILED_PATH, s_file)
    
    try:
        if kwargs.get("combined_output", b_COMBINED_OUTPUT) and s_combined_path:
            e_results = predict_zillow.predict_zillow_file_revenue(
                s_working_path,
                original_file=s_working_path)
                
            df_combined = e_results["original_with_predictions"]
            print("Writing combined output to:", s_output_path)
            if not os.path.isfile(s_combined_path):
                df_combined.to_csv(s_combined_path, encoding="utf-8-sig")
            else:
                df_combined.to_csv(s_combined_path, encoding="utf-8-sig", mode="a", header=False)
        else:
            e_results = predict_zillow.predict_zillow_file_revenue(s_working_path)
        
        df_output = e_results["standardized_with_predictions"]
        df_output.index.name = "zpid"
        
        print(list(df_output.columns))
        
        df_output_bare = df_output[[col for col in predict_zillow.a_PREDICTION_OUTPUT_COLS if col in df_output.columns]].copy()
        print("Writing prediction output to:", s_output_path)
        if not os.path.isfile(s_output_path):
            df_output_bare.to_csv(s_output_path, encoding="utf-8-sig")
        else:
            df_output_bare.to_csv(s_output_path, encoding="utf-8-sig", mode="a", header=False)
        
        if kwargs.get("send_result", b_SEND_RESULT) and len(df_output_bare) > 0 and os.path.isfile(s_output_path):
            o_send_response = send_result(s_output_path, **kwargs)
        return s_output_path
    
    except Exception as e:
        if kwargs.get("move_to_failed"):
            shutil.move(s_working_path, s_fail_path)
        logger = kwargs.get("logger", LOGGER)
        print("Failed to process:", s_file)
        print("\n", traceback.print_exc(), "\n", )
        if logger is not None: logger.warning(f"{utils.get_time()}\tFailed to process: {s_fail_path}. \n\n\n{traceback.print_exc()}")
        return ""
        

def send_result(s_output_path, **kwargs):
    logger = kwargs.get("logger", LOGGER)
    files = {"file": open(s_output_path, "r", encoding="utf-8-sig").read()}
    print("Trying to send:", s_output_path)
    if logger is not None: logger.info(f"{utils.get_time()}\tTrying to send: {s_output_path}")
    
    e_post_header = {
    #    "Content-type": "text/csv",
    #    "Accept": "text/csv",
    #    "api_key": '5602-03ea-909f-742d-4c3f-9a6d-6104-9258',
    }
    if s_TARGET_URL and s_TARGET_METHOD.lower() == "post":
        o_response = requests.post(
            s_TARGET_URL,
            files=files,
            headers=e_post_header,
        )
        print(o_response.text)
        e_response = json.loads(o_response.text)
        if e_response.get("success") == True:
            if logger is not None: logger.info(f"{utils.get_time()}\tSent result: {o_response.text}")
        else:
            if logger is not None: logger.error(f"{utils.get_time()}\tFailed to send result: {o_response.text}")
        
        return o_response
    elif s_TARGET_URL and s_TARGET_METHOD.lower() == "put":
        o_response = requests.put(
            s_TARGET_URL,
            files=files,
            headers=e_post_header,
        )
        print(o_response.text)
        e_response = json.loads(o_response.text)
        if e_response.get("success") == True:
            if logger is not None: logger.info(f"{utils.get_time()}\tSent result: {o_response.text}")
        else:
            if logger is not None: logger.error(f"{utils.get_time()}\tFailed to send result: {o_response.text}")
        return o_response
    else:
        print("Send target or send method not specified")
        if logger is not None: logger.info(f"{utils.get_time()}\tSend target or send method not specified")
        return None

def resend_results(dt_earliest=None, s_folder=None, **kwargs):
    if s_folder is None: s_folder = paths.s_PREDICTION_PATH
    
    if dt_earliest is not None:
        if isinstance(dt_earliest, str):
            dt_earliest = dateutil.parser.parse(dt_earliest)
    
    a_files = utils.get_files_between_dates(
        utils.get_file_list(s_folder, c_exts={".csv"}),
        dt_earliest=dt_earliest)
    
    for s_file in a_files:
        print("Resending file:", s_file)
        time.sleep(1)
        send_result(s_file, **kwargs)
    

def clear_old(s_folder, i_max_days=7, i_max_files=100, logger=None, b_include_subfolders=False):
    a_files = utils.get_file_list(s_folder, b_include_subfolders=True)
    a_dates = utils.get_file_list_dates(a_files)
    
    a_remaining = []
    dt_before = datetime.now() - timedelta(days=i_max_days)
    
    for s_file, dt_date in zip(a_files, a_dates):
        if dt_date < dt_before:
            #print("delete old", s_file)
            if logger is not None: logger.info(f"{utils.get_time()}\tRemove old file: {s_file}")
            os.remove(s_file)
        else:
            a_remaining.append((s_file, dt_date))
    
    a_remove = []
    a_file_dates = sorted(a_remaining, key=lambda t: t[1])
    if len(a_file_dates) > i_max_files:
        a_remove = a_file_dates[:len(a_file_dates) - i_max_files]
    for s_file, s_date in reversed(a_remove):
        if logger is not None: logger.info(f"{utils.get_time()}\tRemove oldest file: {s_file}")
        #print("delete oldest", s_file)
        os.remove(s_file)




if __name__ == "__main__":
    args, kwargs = utils.parse_command_line_args(sys.argv)
    print("Running process_api_requests.py from shell")
    #if LOGGER is not None: logger.info(f"{utils.get_time()}\tRunning process_api_requests.py from shell")
    
    if kwargs.get("resend_results"):
        resend_results(**kwargs)
    else:
        check_input(**kwargs)
