﻿import os, requests, re, binascii, json, bson, math, socket
from decimal import Decimal
import codecs, io, sys, gzip
from datetime import datetime, timedelta
from time import time
from collections import defaultdict, Counter
from functools import reduce, wraps
import hashlib
from zipfile import ZipFile
import pandas as pd
import numpy as np
from pymongo.bulk import ObjectId
from math import log10, floor, sin, cos, sqrt, atan2, radians
import shutil
import logging

try:
    import pyxlsb
    b_PYXLSB = True
except:
    b_PYXLSB = False


__all__ = [
    "download_file", "get_local_path", "create_folder",
    "clear_sub_folders", "get_new_write_filename", "generate_temp_filename",
    "get_file_list", "get_file_list_dates", "get_files_between_dates",
    "get_old_files", "get_file_in_path",
    "zip_directory",
    "detect_bom", "encoding_tester",
    "open_workbook", "open_sheet", "fwf_header_col_width",
    "split_dataframe",
    "to_float", "to_string",
    "flatten_dict", "flatten",
    "df_to_sparse", "json_dict_prep", "pymongo_dict_prep",
    "trim", "search_and_remove",
    "read_dictionary", "ENGLISH", "STOP_WORDS",
    #"read_ngram_freqs",
    "WORD_FREQS", "MAX_WORD_FREQ",
    "start_grams", "end_grams", "ngrams",
    "word_tokenize",
    "jaccard_similarity", "edit_distance",
    "convert_date",
    "ZippedSheets", "check_path",
    "convert_command_line_arg",
    "parse_command_line_args",
    "list_numbers", "value_list_stddev",
    "moving_average", "smooth", "count_peaks",
    "mean_of_products", "pearson_correlation", "trend_magnitude",
    "period_diff", "find_periodicity", "make_interval", "add_interval",
    "secondsToStr", "timer",
    "conjunction", "significant_digits",
    "unique", "yield_batches",
    "get_time",
    
    "diff_month",
    "value_list_avg",
    "value_list_stddev", "value_list_herfindahl",
    "value_list_similarity", "value_list_variation",
    
    "check_log_file",
    
    "get_distance",
    "free_port",
]

# if a file encoding is not found, try discovering it manually, then adding to this list
a_OTHER_FORMATS = ['utf-16',]
s_DATA_FOLDER = ''

re_WHITESPACE = re.compile(r"[ \t\r\n]+", flags=re.I)

re_NONDIGITS_SIGNS = re.compile(r'[^\d./ +-]+')

re_DECIMAL_SIG = re.compile(r"\.([0-9]+)(?:[%]?\W*$)")
re_CURRENCY_SIG = re.compile(r"([$€؋£¥ƒ₼៛₡₱¢﷼₪₩₭₮ƒ₦ł฿₴₫]|лв|ден)")

e_PERIOD_NAMES = {
    0: 'year',
    1: 'month',
    2: 'week',
    3: 'day',
    4: 'hour',
    5: 'minute',
    6: 'second',
}
e_PERIOD_INDEXES = {val: key for key, val in e_PERIOD_NAMES.items()}

e_DAYS_PER_MONTH = {
    1: 31,
    2: 28,
    3: 31,
    4: 30,
    5: 31,
    6: 30,
    7: 31,
    8: 31,
    9: 30,
    10: 31,
    11: 30,
    12: 31,
}

e_MONTH_NAMES = {
    1: "January", "1": "January",
    2: "February", "2": "February",
    3: "March", "3": "March",
    4: "April", "4": "April",
    5: "May", "5": "May",
    6: "June", "6": "June",
    7: "July", "7": "July",
    8: "August", "8": "August",
    9: "September", "9": "September",
    10: "October", "10": "October",
    11: "November", "11": "November",
    12: "December", "12": "December",}

re_FRACTION = re.compile(r'(?P<sign>[+-]?)((?P<whole>\d+)([ +-]| [+-] ))?(?P<num>\d+)/(?P<denom>[1-9]\d*)')
re_DECIMAL = re.compile(r'(?P<sign>[+-]?)(?P<thousands>(\d{1,6},)+)?(?P<whole>(?<!\.)\d+)?(?P<dec>\.\d+)?(?P<percent> ?%)?')

c_DIGITS = set(r'0123456789')

def download_file(url, filename='', b_skip_headers=False, b_print_chunks=False):
    ''' download web url
        Args:
            url: {str} the http link to the file to be downloaded
            filename: {str} optionally specify the local file name to use.
                      if omitted, it will use the filename from the url and
                      the directory of the python script file
        Returns:
            {str} the local filename that the file was successfully downloaded
            or if there is already a local file matching the remote file
            return an empty str if download failed
    '''
    if not url or url[:4].lower() != 'http':
        print('not an http url')
        return ''
    local_filename = url.split('/')[-1]
    if filename:
        s_folder, s_file = os.path.split(filename)
        
        b_folder = True if s_folder and os.path.isdir(s_folder) else False
        if b_folder and s_file:
            local_filename = filename
        elif b_folder and not s_file:
            local_filename = os.path.join(s_folder, local_filename)
        elif s_file:
            local_filename = s_file
    if not local_filename:
        raise Exception("no local filename/path specified")
    
    if b_skip_headers:
        b_download = True
    elif not os.path.isfile(local_filename):
        try:
            head = requests.head(url)
            if head.status_code != 200:
                print('Trouble accessing url: code {} from "{}"'.format(head.status_code, url))
                print('Headers:', head.headers)
                if isinstance(head.headers, dict):
                    e_head = head.headers
                else:
                    e_head = {}
                #return head
            else:
                e_head = head.headers
        except:
            print('no URL head from request from "{}"'.format(url))
            return ''
        if not e_head:
            print('no URL head from request from "{}"'.format(url))
            return ''
        if e_head.get('Content-Length', 0) == 0:
            print('no content length in URL head from request from "{}"'.format(url))
            return ''
        b_download = True
    else:
        b_download = False
        try:
            head = requests.head(url)
            if head.status_code != 200:
                print('Trouble accessing url: code {} from "{}"'.format(head.status_code, url))
                print('Headers:', head.headers)
                if isinstance(head.headers, dict):
                    e_head = head.headers
                else:
                    e_head = {}
                #return ''
            #e_head = head.headers
        except:
            print('no status code in URL head from request from "{}"'.format(url))
            return ''
        
        if not e_head:
            print('no URL head from request from "{}"'.format(url))
            return ''
        
        if isinstance(e_head, dict) and e_head:
            if e_head.get('Content-Length', 0) == 0:
                print('no content length in URL head from request from "{}"'.format(url))
                return ''
            
            remote_time = datetime.strptime (e_head.get("Last-Modified"), '%a, %d %b %Y %H:%M:%S %Z')
            local_time = os.path.getmtime(local_filename)
            if time.mktime(remote_time.timetuple()) > local_time:
                # remote is newer
                print('remote file is newer. redownload')
                b_download = True
        
            remote_hash = re.sub(r'^[\'"]|[\'"]$', "", e_head.get("ETag", ""))
            local_hash = hashlib.md5(open(local_filename,'rb').read()).hexdigest()
            
            if remote_hash and remote_hash != local_hash:
                # remote has different hash
                print('remote file has changed. redownload')
                b_download = True
        
    #print('file: {}, download: {}'.format(local_filename, b_download))
    if b_download:
        r = requests.get(url, stream=True)
        
        if not os.path.isdir(os.path.split(local_filename)[0]):
            raise Exception('Download folder doesn\'t exist: "{}"'.format(os.path.split(local_filename)[0]))
        
        with open(local_filename, 'wb') as f:
            i_chunk = 0
            for chunk in r.iter_content(chunk_size=1024): 
                if chunk: # filter out keep-alive new chunks
                    i_chunk += 1
                    if b_print_chunks: print(f"Downloaded chunk {i_chunk}")
                    f.write(chunk)
    else:
        print('local file is current')
    return local_filename


def get_local_path(s_path=None):
    ''' get the path if a folder isn't specified and if the file is in the local data directory
        Arguments:
            s_file: {str} a filename
        Returns:
            {str} the absolute or relative path
    '''
    global s_DATA_FOLDER
    if not s_DATA_FOLDER:
        s_DATA_FOLDER = os.path.join(os.path.dirname(__file__), 'data')
    
    #if not s_path: return s_path
    if s_path is None: return s_DATA_FOLDER
    if not s_path: return s_path
    s_folder, s_file = os.path.split(s_path.strip())
    if s_folder: return s_path
    
    s_new_path = os.path.join(s_DATA_FOLDER, s_file)
    if os.path.isfile(s_new_path) or os.path.isdir(s_new_path):
        return s_new_path
    
    print('not local file', s_path, s_file)
    return s_path


def create_folder(s_path):
    ''' Create a folder if it doesn't already exist,
        even if multiple nested folders must be created
        Arguments:
            s_path: {str} of the folder path to be created (must end in separator if last element is folder)
        Returns:
            {bool} if folder is successfully made
    '''
    if os.path.exists(s_path):
        return True
    s_drive, s_folder_path = os.path.splitdrive(s_path)
    s_folder_path, s_file = os.path.split(s_folder_path)
    s_drive, s_folder_path, s_file
    
    if os.path.altsep:
        re_path_split = re.compile(r"[{}{}]".format(re.escape(os.path.sep), re.escape(os.path.altsep)))
    else:
        re_path_split = re.compile(r"[{}]".format(re.escape(os.path.sep)))
    
    a_folders = [
        x for x in re_path_split.split(s_folder_path)
        if x]
    
    for i_len in range(1, len(a_folders) + 1):
        s_check_path = os.path.join(s_drive, os.path.sep, *a_folders[:i_len])
        if not os.path.isdir(s_check_path):
            os.mkdir(s_check_path)
        #print(s_path, os.path.isdir(s_check_path))
    
    return os.path.isdir(os.path.join(s_drive, os.path.sep, s_folder_path))


def clear_sub_folders(s_sub_folder, s_folder, b_only_if_empty=True):
    ''' Clear each level under s_sub_folder
        Arguments:
            s_sub_folder: {str} relative subfolders to remove like "folder1/folder2"
            s_folder: {str} the rest of the path not to remove
            b_only_if_empty: {bool} only remove the subfolders if they are already empty
    '''
    #print('s_sub_folder', s_sub_folder)
    s_sub_folder = os.path.split(s_sub_folder)[0]  # strip of any actual filenames
    if not s_sub_folder or not s_folder or not os.path.isdir(os.path.join(s_folder, s_sub_folder)):
        #print("Folder + subfolder path doesn't exist: '{}' '{}'".format(s_folder, s_sub_folder,))
        return
    #print(s_sub_folder)
    a_sub_folders = re.split(r"[/\\]", s_sub_folder)
    for i in reversed(range(1, len(a_sub_folders) + 1)):
        #print(a_sub_folders[:i])
        s_path = os.path.join(s_folder, *a_sub_folders[:i])
        #print(s_path)
        a_existing_files = os.listdir(s_path)
        if a_existing_files:
            if b_only_if_empty:
                #print("Files in directory: {}".format(s_path))
                return
            
            for s_existing_file in a_existing_files:
                if os.path.isdir(os.path.join(s_path, s_existing_file)):
                    #print("remove subfolder:",os.path.join(s_path, s_existing_file))
                    clear_sub_folders(os.path.join(s_existing_file, ''), s_path, b_only_if_empty=b_only_if_empty)
                else:
                    os.remove(os.path.join(s_path, s_existing_file))
        os.rmdir(s_path)


def get_new_write_filename(s_path, b_dot_format=True):
    """ Get a new, unoccupied filename. If a file already exists, add a number
        to the end of the filename and increment it upwards until no existing file is found.
        Then return new filename to write to."""
    if not isinstance(s_path, str) or not s_path:
        return ""
    #s_folder = folder_from_path(s_path)
    s_folder = os.path.split(s_path)[0]
    s_file = os.path.basename(s_path)

    if not os.path.isdir(s_folder):  # if the folder doesn't exists
        return ""
    if not os.path.isfile(os.path.join(s_folder, s_file)):  # if a file doesn't exist with that name
        # Filepaths were coming out doubled, when only a filename was passed to this function
        return os.path.join(s_folder, s_file) if s_folder != s_file else s_file

    s_base, s_ext = os.path.splitext(s_file)
    i_count = 1
    if b_dot_format:
        s_format = "{}.{}{}"
    else:
        s_format = "{} ({}){}"
    
    while os.path.isfile(os.path.join(s_folder, s_format.format(s_base, str(i_count), s_ext))):
        i_count += 1
    return os.path.join(s_folder, s_format.format(s_base, str(i_count), s_ext))


def generate_temp_filename(ext="", folder="", **kwargs):
    if not folder: folder = os.getcwd()
    
    s_key = binascii.hexlify(os.urandom(8)).decode()
    a_pieces = []
    for i_start in range(0, len(s_key), 4):
        a_pieces.append(s_key[i_start: i_start + 4])
    
    s_name = ""
    if kwargs.get("include_date", True):
        s_name = "{}_".format(datetime.now().strftime("%Y%m%d"))
    s_name += "".join(a_pieces)
    if ext and not ext[0] == ".": ext = f".{ext}"
    s_file = os.path.join(folder, f"_{s_name}{ext}")
    return s_file


def get_file_list(s_folder, c_exts=None, b_include_subfolders=False):
    """ Pass in a folder path and a set of extensions.
        Return a list of files matching any extension as absolute paths.
        Arguments:
            s_folder: {str} folder to get files
            c_exts: {set} only include files with these extensions
            b_include_subfolders: {bool} also walk through subdirectories
        Returns:
            {list} of file paths (folder + filename)
    """
    a_files = []
    if c_exts:
        e_valid_exts = set(
            [
                s_ext if s_ext.startswith(".") else ".{}".format(s_ext)
                for s_ext in c_exts
            ]
        )

        for (dirpath, dirnames, filenames) in os.walk(s_folder):
            a_files.extend(
                [
                    os.path.join(dirpath, f)
                    for f in filenames
                    if os.path.splitext(f)[1] in e_valid_exts
                ]
            )
            if not b_include_subfolders: break
    else:
        for (dirpath, dirnames, filenames) in os.walk(s_folder):
            a_files.extend([os.path.join(dirpath, f) for f in filenames])
            if not b_include_subfolders: break
    return a_files


def get_file_list_dates(files, c_exts=None, b_include_subfolders=False, b_modified_time=False):
    if isinstance(files, str):
        if os.path.isfile(files):
            return datetime.fromtimestamp(os.path.getctime(s_log_file))
        elif os.path.isdir(files):
            a_files = get_file_list(files)
        else:
            return None
    elif isinstance(files, (list, tuple, set)):
        a_files = list(files)
    
    if b_modified_time:
        a_dates = [
            datetime.fromtimestamp(os.path.getmtime(s_file))
            if os.path.isfile(s_file) else None
            for s_file in a_files]
    else:
        a_dates = [
            datetime.fromtimestamp(os.path.getctime(s_file))
            if os.path.isfile(s_file) else None
            for s_file in a_files]
    return a_dates


def get_files_between_dates(a_files, dt_earliest=None, dt_latest=None):
    if dt_earliest is None and dt_latest is None: return a_files
    
    a_file_dates = get_file_list_dates(a_files, b_modified_time=True)
    a_filter = zip(a_files, a_file_dates)
    
    if dt_earliest is not None:
        a_filter = [(s_file, dt_file) for s_file, dt_file in a_filter if dt_file >= dt_earliest]
    if dt_latest is not None:
        a_filter = [(s_file, dt_file) for s_file, dt_file in a_filter if dt_file <= dt_latest]
    return [t_file[0] for t_file in a_filter]


def get_old_files(s_folder, dt_before):
    ''' Get a list of the files before a certain date '''
    a_files = get_file_list(s_folder)
    a_dates = get_file_list_dates(a_files)
    a_old = []
    for s_file, dt_date in zip(a_files, a_dates):
        if dt_date < dt_before:
            a_old.append(s_file)
    return a_old


def get_file_in_path(s_file):
    ''' Check if a file exists in one of the folders specified in the
        system's environment variable "PATH" '''
    a_folders = os.environ.get("PATH", "").split(";")
    for s_folder in a_folders:
        a_files = get_file_list(s_folder)
        e_files = {os.path.split(s.lower())[1]: s for s in a_files}
        #print("e_files", e_files)
        if s_file.lower() in e_files:
            return e_files[s_file.lower()]
        #break
    return ""



def zip_directory(s_directory, s_zip=None, **kwargs):
    ''' zip the contents of a directory '''
    if not s_directory: s_directory = os.getcwd()
    if not os.path.isdir(s_directory): raise Exception(f'Not a valid directory to zip up: "{s_directory}"')
    s_drive, s_dir = os.path.splitdrive(s_directory)
    a_folder = [x for x in re.split(r"[/\\:]", s_dir) if x]
    
    c_include_exts = {str(x).lower() for x in kwargs.get("include_extensions", [])}
    c_exclude_exts = {str(x).lower() for x in kwargs.get("exclude_extensions", [])}
    
    #print(len(c_include_exts), "c_include_exts", c_include_exts)
    #print(len(c_exclude_exts), "c_exclude_exts", c_exclude_exts)
    
    s_date = datetime.now().strftime("%Y-%m-%d")
    if not s_zip:
        if kwargs.get("zip_cwd"):
            s_zip_dir = os.getcwd()
        else:
            if kwargs.get("store_in_directory", True):
                s_zip_dir = os.path.join(*a_folder)
            else:
                s_zip_dir = os.path.join(*a_folder[:-1])
        
        if s_drive and s_drive[-1] != os.path.sep: s_drive += os.path.sep
        if not s_drive: s_zip_dir = os.path.sep + s_zip_dir
        s_zip = os.path.join(s_drive, s_zip_dir, f"{a_folder[-1]}_{s_date}.zip")
    
    a_files = []
    with ZipFile(s_zip, "w") as o_zip:
        for s_folder, a_subfolders, a_filenames in os.walk(s_directory):
            for s_filename in a_filenames:
                _, s_ext = os.path.splitext(s_filename)
                s_file = os.path.join(s_directory, s_filename)
                
                if c_include_exts and s_ext.lower() in c_include_exts:
                    o_zip.write(s_file, os.path.basename(s_file))
                    a_files.append(s_file)
                    continue
                
                if c_exclude_exts and s_ext.lower() not in c_exclude_exts:
                    o_zip.write(s_file, os.path.basename(s_file))
                    a_files.append(s_file)
                    continue
    
                if not c_include_exts and not c_exclude_exts:
                    o_zip.write(s_file, os.path.basename(s_file))
                    a_files.append(s_file)
                    continue
                #print("skipping:", s_filename)
    return s_zip, a_files


def detect_bom(file, default=None):
    if isinstance(file, str):
        with open(file, 'rb') as f:
            raw = f.read(8) #will read less if the file is smaller
    elif isinstance(file, (io.BufferedReader, gzip.GzipFile)):
        raw = file.read(8) #will read less if the file is smaller
        file.seek(0)
    elif isinstance(file, io.TextIOWrapper) and os.path.isfile(file.name):
        with open(file.name, 'rb') as f:
            raw = f.read(8) #will read less if the file is smaller
        file.seek(0)
    else:
        print("Can't detect BOM. Pass in path or binary file object.")
        return default
    
    for enc, boms in [
            #('utf-32', [codecs.BOM_UTF32]),
            ('utf-32-le', [codecs.BOM_UTF32_LE]),
            ('utf-32-be', [codecs.BOM_UTF32_BE]),
            ('utf-16', [codecs.BOM_UTF16]),
            ('utf-16-le', [codecs.BOM_UTF16_LE]),
            ('utf-16-be', [codecs.BOM_UTF16_BE]),
            ('utf-8', [codecs.BOM_UTF8]),
        ]:
        if any(raw.startswith(bom) for bom in boms if bom): return enc
        
    if default == 'utf-8': return 'utf-8-sig'
    return default


def encoding_tester(file, s_format='',
                    a_default_formats=['utf-8', 'ISO-8859-2', 'Windows-1252', 'cp1252', 'latin_1']):
    a_formats = [s_format]
    
    s_bom_encoding = detect_bom(file)
    if not s_bom_encoding in a_formats: a_formats.append(s_bom_encoding)
    #print('s_bom_encoding', s_bom_encoding)
    
    a_formats.extend([s_default for s_default in a_default_formats if not s_default == s_format])
    a_formats.extend([s_default for s_default in a_OTHER_FORMATS if not s_default == s_format])
    
    s_sys_encoding = sys.getdefaultencoding()
    if not s_sys_encoding in a_formats: a_formats.append(s_sys_encoding)
        
    for s_encoding in a_formats:
        if not s_encoding: continue
        if not s_bom_encoding and s_encoding == 'utf-8':
            s_encoding = 'utf-8-sig'
        try:
                
            if isinstance(file, str):
                with open(file, "r", encoding=s_encoding) as f:
                    for line in f: pass
                return s_encoding
            elif isinstance(file, io.TextIOWrapper):
                for line in file: pass
                return s_encoding
            elif isinstance(file, io.BufferedReader):
                with open(file.name, 'r', encoding=s_encoding) as f:
                    for line in f: pass
                return s_encoding
            elif isinstance(file, gzip.GzipFile):
                with gzip.GzipFile(file.name) as o_gz:
                    with io.TextIOWrapper(o_gz, encoding=s_encoding) as f:
                        for line in f: pass
                return s_encoding
        except UnicodeDecodeError:
            #print("Couldn't read file with encoding: {}".format(s_encoding))
            s_encoding = ''
            if not isinstance(file, str): file.seek(0)
            pass
            
    print("No successful encoding found.")
    return None


def open_workbook(s_path):
    ''' Open an excel workbook that may contain multiple sheets and
        return as dictionary of {sheetname: dataframe} '''
    o_xl = pd.ExcelFile(s_path)
    e_sheets = {}
    for s_sht in o_xl.sheet_names:  # see all sheet names
        e_sheets[s_sht] = o_xl.parse(sheet_name=s_sht)
    return e_sheets


def fwf_header_col_width(s_path, **kwargs):
    enc = kwargs.get('encoding')
    if enc is None: enc = encoding_tester(s_path)
    with open(s_path, "r", encoding=enc) as f:
        line = f.readline()
    
    colwidths = []
    i_col_start, i_col_end = 0, 0
    
    split_character = kwargs.get('split_character')
    if split_character:
        for x in line.split(split_character):
            i_col_end = i_col_start + len(x) #+ 1
            colwidths.append((i_col_start, i_col_end))
            i_col_start = i_col_end #+ 1
    else:
        for x in re.findall(r"(\S+[\s\$]+)", line):
            i_col_end = i_col_start + len(x)
            colwidths.append((i_col_start, i_col_end))
            i_col_start = i_col_end
    return colwidths


def open_sheet(s_path, **kwargs):
    ''' Arguments:
            s_path: {str} path to open
            fwf: optional {bool} to try reading text format as fixed width format instead of delimited
            fwf_use_header_widths: optional {bool} to use header widths only instead of having pandas infer widths from values
            kwargs: {dict} kwargs to pass to read_excel or read_csv
        Returns:
            {pandas.DataFrame}
    '''
    if s_path.lower().endswith(('.xlsx', '.xls', '.xlsm')):
        df = pd.read_excel(s_path, **kwargs)
    elif s_path.lower().endswith('.xlsb'):
        if b_PYXLSB:
            df = pd.read_excel(s_path, engine='pyxlsb', **kwargs)
        else:
            raise Exception("""
                The old versions of the pythoin pandas excel library don't support binary .xlsb files.
                Either save a copy of the file as ".xlsm"/".xlsx", or update pandas to at least 1.0.0 and run "pip install pyxlsb"
            """)
    else:
        enc = encoding_tester(s_path)
        if not enc:
            raise("Couldn't detect encoding of sheet: {}".format(s_path))
        
        if kwargs.get('fwf', False):
            # try reading the file as fixed width format first
            if kwargs.get('fwf_use_header_widths', True):
                colwidths = fwf_header_col_width(s_path)
                df = pd.read_fwf(s_path, encoding=enc, colspecs=colwidths, **kwargs)
            else:
                df = pd.read_fwf(s_path, encoding=enc, **kwargs)
        else:
        
            if s_path.lower().endswith(('.txt', '.tsv', '.tab')):
                try:
                    df = pd.read_csv(s_path, sep='\t', encoding=enc, **kwargs)
                except UnicodeDecodeError:
                    df = pd.read_csv(s_path, sep='\t', encoding='ISO-8859-2', **kwargs)
                except pd.errors.ParserError:
                    df = pd.read_fwf(s_path, encoding=enc, **kwargs)

            else:
                try:
                    df = pd.read_csv(s_path, encoding=enc, **kwargs)
                except UnicodeDecodeError:
                    df = pd.read_csv(s_path, encoding='ISO-8859-2', **kwargs)
    return df


def delete_extra_header_rows(df, a_headers=["item_sku", "item_name"]):
    ''' For an amazon listing with multiple header rows, delete the extra ones '''
    # find actual header row
    for i_row in range(max(10, len(df))):
        a_row = list(df.iloc[i_row])
        if any(col in a_row for col in a_headers):
            break
    else:
        print("Not an amazon listing file.")
        return
    #print(f"Header row = {i_row}")
    #print(list(a_row))
    
    if i_row > 0:
        # drop extra rows above it and make it the header
        df.columns = a_row
        df.drop(range(i_row + 1), axis=0, inplace=True)


def split_dataframe(df, rows_per_df=1000): 
    ''' Split a dataframe into sub dataframes using the specified max row count of a sub df
        Arguments:
            df: {pandas.DataFrame} the dataframe to split
            rows_per_df: {int} the maximum rows per sub-df
        Returns:
            {list} of {pandas.DataFrame}
    '''
    if len(df) == 0: return [df]
    a_sub_dfs = []
    i_sub_df_count = len(df) // rows_per_df + 1
    for i in range(i_sub_df_count):
        a_sub_dfs.append(df[i * rows_per_df: (i + 1) * rows_per_df])
    return a_sub_dfs


def unique_url_id(customized_url):
    ''' Return the unique id portion of amazon's "customized-url" field
        Args:
            customized_url: {str} the value from amazon's "customized-url" where the unique id
                            is the folder before the filename of "1"
        Returns:
            {str} the unique id if it is at least 20 characters, or the original whole value
            cleaned of any filename safe chars
    '''
    #if not customized_url or pd.notna(customized_url): return customized_url
    if not isinstance(customized_url, str): return customized_url
    a_customized_url = customized_url.split('/')
    #print('a_customized_url', a_customized_url)
    if len(a_customized_url) > 1 and len(a_customized_url[-2]) >= 20:
        return clean_filename(a_customized_url[-2])
    else:
        return clean_filename(customized_url)


def clean_filename(s_filename, s_char="_", b_remove_space=True):
    s_BAD = "\"'#%&{}\\<>*?/$!:|@"
    if b_remove_space: s_BAD += ' '
    
    s_filename = re.sub(r"[{}]+([-_+=;.,]+)[{}]+".format(s_BAD, s_BAD), r"\1", s_filename)
    s_filename = re.sub(r"(^[{}]+|[{}]+$)".format(s_BAD, s_BAD), "", s_filename)
    s_filename = re.sub(r"[{}]+".format(s_BAD), s_char, s_filename)
    return s_filename


def to_float(v_fraction):
    ''' Take a fraction and convert it into a float.
        Can accept formats like "-1/2", "11-1/2", "-11-1/2", "11 1/2", "11 1/2"
    '''
    if isinstance(v_fraction, str):
        o_match = re_FRACTION.search(v_fraction.strip())
        if o_match:
            e_fract = o_match.groupdict()

            if 'num' in e_fract and 'denom' in e_fract:
                if e_fract.get('whole', 0.0):
                    d_value = float(e_fract['whole'])
                else:
                    d_value = 0.0

                d_num = float(e_fract.get('num', 0.0))
                d_denom = float(e_fract.get('denom', 0.0))
                if d_denom > 0:
                    d_value += (d_num / d_denom)

                if d_value > 0:
                    if e_fract.get('sign') == '-':
                        return -d_value
                    else:
                        return d_value
                else:
                    return np.nan
            else:
                return np.nan
            
        else:
            o_match = re_DECIMAL.search(v_fraction.strip())
            if o_match:
                e_fract = o_match.groupdict()
                if 'whole' not in e_fract and 'dec' not in e_fract:
                    return np.nan
                d_value = 0
                
                i_digits = 0
                if e_fract.get('whole', 0.0):
                    d_value += float(e_fract['whole'])
                    i_digits = len(e_fract['whole'])
                    
                if e_fract.get('thousands', 0.0):
                    d_value += float(e_fract['thousands'].replace(",", "")) * (10**i_digits)
                    
                if e_fract.get('dec', 0.0):
                    d_value += float(e_fract['dec'])
                
                if e_fract.get('percent'):
                    d_value /= 100
                
                if e_fract.get('sign') == '-':
                    return -d_value
                else:
                    return d_value
    
    elif isinstance(v_fraction, (float, np.float32)):
        return v_fraction
    elif isinstance(v_fraction, int):
        return float(v_fraction)
    elif isinstance(v_fraction, Decimal):
        return float(v_fraction)
    else:
        return np.nan


def to_string(s):
    try:
        return str(s)
    except:
        #Change the encoding type if needed
        return s.encode('utf-8')


def flatten_dict(key, value, new_dict=None):
    ''' Recursively flatten the JSON '''
    if new_dict is None:
        new_dict = {}
            
    #Reduction Condition 1
    if type(value) is list:
        i = 0
        for sub_item in value:
            flatten_dict('{}_{}'.format(key, to_string(i)), sub_item, new_dict)
            i += 1

    #Reduction Condition 2
    elif type(value) is dict:
        sub_keys = value.keys()
        for sub_key in sub_keys:
            flatten_dict('{}_{}'.format(key, to_string(sub_key)), value[sub_key], new_dict)
    
    #Base Condition
    else:
        new_dict[to_string(key)] = to_string(value)


def flatten(a_nested, filter_blanks=False):
    """ Flatten a nested list.
        Made this 3 to 5x more efficient by using a single list reference
        to add all the new elements to before returning. """
    a_flat = []
    for elem in a_nested:
        if isinstance(elem, list) and (filter_blanks is False or elem):
            flatten_sub(elem, a_flat, filter_blanks=filter_blanks)
        elif filter_blanks is False or elem:
            a_flat.append(elem)
    return a_flat


def flatten_sub(a_nested, a_flat, filter_blanks=False):
    ''' sub function to add recursive elements to the created list '''
    for elem in a_nested:
        if isinstance(elem, list) and (filter_blanks is False or elem):
            flatten_sub(elem, a_flat, filter_blanks=filter_blanks)
        elif filter_blanks is False or elem:
            a_flat.append(elem)


def df_to_sparse(df):
    ''' Convert a dataframe into a dict of sparse dicts, with indexes as key1 and columns as key2
        Returns:
            {dict} of {row_index1: sparse_dict_row1, row_index2: sparse_dict_row2...}
    '''
    if df.index.nlevels == 1:
        e_df = defaultdict(dict)
        for k, v in df.stack().items():
            e_df[k[:-1]][k[-1]] = v
    return dict(e_df)


def json_dict_prep(variable, **kwargs):
    ''' recursively check through a dict to make sure it can be dumped into a json string
        by converting datetime to isoformat, and dropping custom object variables.
        Arguments:
            variable: any python variable
        Returns:
            variable in type that can be jsonified or None
    '''
    if isinstance(variable, str):
        return variable
    elif isinstance(variable, int):
        if isinstance(variable, np.int64):
            return int(variable)
        else:
            return variable
    elif isinstance(variable, (float)):
        if pd.isnull(variable):
            return None
        else:
            if isinstance(variable, np.float64):
                return float(variable)
            else:
                return variable
    
    elif isinstance(variable, (Decimal)):
        return float(variable)
    
    elif isinstance(variable, datetime):
        if kwargs.get("raw_dates"):
            return variable
        else:
            return variable.isoformat()
    
    elif isinstance(variable, list):
        return [json_dict_prep(x, **kwargs) for x in variable]
    elif isinstance(variable, tuple):
        return tuple(json_dict_prep(x, **kwargs) for x in variable)
    elif isinstance(variable, set):
        return {json_dict_prep(x, **kwargs) for x in variable}
    elif isinstance(variable, dict):
        return {k: json_dict_prep(v, **kwargs) for k, v in variable.items()}
    elif variable is None:
        return None
    elif isinstance(variable, np.int64):
        return int(variable)
    elif isinstance(variable, np.float64):
        return float(variable)
    elif isinstance(variable, bson.objectid.ObjectId):
        return str(variable)
    else:
        print(f"Unknown variable type in json_dict_prep:", type(variable))
        return None



def pymongo_dict_prep(variable, **kwargs):
    ''' recursively check through a dict to make sure it can be dumped into a pymongo document
        by converting datetime to isoformat, and dropping custom object variables.
        Arguments:
            variable: any python variable
        Returns:
            variable in type that can be jsonified or None
    '''
        
    if isinstance(variable, ObjectId):
        return variable
    elif isinstance(variable, str):
        return variable
    elif isinstance(variable, int):
        if isinstance(variable, np.int64):
            return int(variable)
        else:
            return variable
    elif isinstance(variable, (float)):
        if pd.isnull(variable):
            return None
        else:
            if isinstance(variable, np.float64):
                return float(variable)
            else:
                return variable
    
    elif isinstance(variable, (Decimal)):
        return float(variable)
    
    elif isinstance(variable, datetime):
        if kwargs.get("raw_dates"):
            return variable
        else:
            return variable.isoformat()
    
    elif isinstance(variable, list):
        return [pymongo_dict_prep(x, **kwargs) for x in variable]
    elif isinstance(variable, tuple):
        return tuple(pymongo_dict_prep(x, **kwargs) for x in variable)
    elif isinstance(variable, set):
        return {pymongo_dict_prep(x, **kwargs) for x in variable}
    elif isinstance(variable, dict):
        return {k: pymongo_dict_prep(v, **kwargs) for k, v in variable.items()}
    elif variable is None:
        return None
    elif isinstance(variable, np.int64):
        return int(variable)
    elif isinstance(variable, np.float64):
        return float(variable)
    elif isinstance(variable, bson.objectid.ObjectId):
        return variable
    else:
        print(f"Unknown variable type in pymongo_dict_prep:", type(variable))
        return None


def trim(text):
    ''' Remove leading and trailing whitespace characters,
        and replace multiple consecutive ones inside text with single space. '''
    return re_WHITESPACE.sub(' ', text).strip()


def search_and_remove(re_pattern, text, replacement="", **kwargs):
    ''' replace all instances of a pattern in a string, and 
        Arguments:
            re_pattern: precompiled regex pattern or regex string
            text: {str} of text to remove instances of pattern from
            replacement: {str} of text to put in place of removed text
        Returns:
            {tuple} of the altered string, and list of the matches found '''
    if isinstance(re_pattern, str):
        if "flags" in kwargs:
            re_pattern = re.compile(re_pattern, flags=kwargs["flags"])
        else:
            re_pattern = re.compile(re_pattern)
        
    if kwargs.get("finditer", False):  # return regex match objects instead of strings
        a_matches = list(re_pattern.finditer(text))
    else:
        a_matches = re_pattern.findall(text)
    text = re_pattern.sub(replacement, text)
    return text, a_matches


def read_dictionary(s_path):
    s_path = get_local_path(s_path)
    
    #s_folder, s_file = os.path.split(s_path)
    #if not s_folder:
    #    s_folder = os.path.dirname(__file__)
    #    s_path = os.path.join(s_folder, s_file)
        
    enc = encoding_tester(s_path)
    c_words = set()
    with open(s_path, 'r', encoding=enc) as f:
        for line in f:
            c_words.add(line.lower().strip())

    return c_words

ENGLISH = read_dictionary("english3.txt")
STOP_WORDS = read_dictionary("stop_words.txt")

#def read_ngram_freqs(s_file, b_tuples=True):
#    e_ngrams = defaultdict(int)
#    with open(s_file, 'r') as f:
#        for line in f:
#            a_line = line.split('\t')
#            t_ngram = tuple([trim(s_word) for s_word in a_line[1:]])
#            if b_tuples:
#                e_ngrams[t_ngram] += float(a_line[0])
#            else:
#                e_ngrams['_'.join(t_ngram)] += float(a_line[0])
#    return dict(e_ngrams)


#COMBINED_UNIGRAMS = read_ngram_freqs(
#    os.path.join(os.path.dirname(__file__),
#                 "combined-unigrams.txt"))

def read_word_freqs(s_path):
    ''' Read in a text file of word frequencies with rows in the format
        "12345\tword"
        Arguments:
            s_path: {str} the path of the text file to read
        Returns:
            {dict} for word: freq
    '''
    s_path = get_local_path(s_path)
    e_words = Counter()
    with open(s_path, 'r', encoding='utf-8') as f:
        for line in f:
            a_line = line.split('\t')
            e_words[a_line[1].strip()] += int(a_line[0])
    return dict(e_words)

#WORD_FREQS = read_word_freqs(
#    os.path.join(os.path.dirname(__file__), "english_word_freqs.txt"))
WORD_FREQS = read_word_freqs("english_word_freqs.txt")
MAX_WORD_FREQ = max(WORD_FREQS.values())


def start_grams(words, i_max_length=4, return_tuples=None):
    ''' pass in list of words and return the ngrams anchored on end word '''
    if not words: return []
    if isinstance(words, str):
        a_words = words.split()
        if return_tuples is None: return_tuples = False
    elif isinstance(words, (list, tuple)):
        a_words = words
        if return_tuples is None: return_tuples = True
    else:
        return []
    
    a_ngrams = []
    if return_tuples:
        for i_size in reversed(range(1, min(i_max_length, len(a_words)) + 1)):
            a_ngrams.append(tuple(a_words[:i_size]))
    else:
        for i_size in reversed(range(1, min(i_max_length, len(a_words)) + 1)):
            a_ngrams.append(' '.join(a_words[:i_size]))
    return a_ngrams


def end_grams(words, i_max_length=4, return_tuples=None):
    ''' pass in list of words and return the ngrams anchored on end word '''
    if not words: return []
    if isinstance(words, str):
        a_words = words.split()
        if return_tuples is None: return_tuples = False
    elif isinstance(words, (list, tuple)):
        a_words = words
        if return_tuples is None: return_tuples = True
    else:
        return []
    
    a_ngrams = []
    if return_tuples:
        for i_size in reversed(range(1, min(i_max_length, len(a_words)) + 1)):
            a_ngrams.append(tuple(a_words[-i_size:]))
    else:
        for i_size in reversed(range(1, min(i_max_length, len(a_words)) + 1)):
            a_ngrams.append(' '.join(a_words[-i_size:]))
    return a_ngrams


def ngrams(text, i_max_size=6, i_min_size=1, b_remove_stop_words=False, b_remove_punct=True, b_as_tuples=False):
    ''' Returns a list of ngrams strings from longest to shortest from first to last for each size.'''

    if isinstance(text, (list, tuple)):
        a_words = text
    else:
        a_words = word_tokenize(text, b_remove_stop_words=b_remove_stop_words, b_remove_punct=b_remove_punct)

    a_all_ngrams = []
    for i_size in reversed(range(i_min_size, min(i_max_size + 1, len(a_words) + 1))):
        z_ngrams = zip(*[a_words[i:] for i in range(i_size)])
        a_ngrams = [tuple(x) for x in z_ngrams]
        a_all_ngrams.extend(a_ngrams)
    
    if not b_as_tuples:
        a_all_ngrams = tuple([' '.join(x) for x in a_all_ngrams])
    return a_all_ngrams


def word_tokenize(s_text, b_remove_punct=False, b_remove_stop_words=False, return_lower=False):
    if b_remove_punct:
        s_text = re.sub(r"[^A-Z0-9_']", " ", s_text, flags=re.I)
    
    a_tokenized = s_text.split()
    if b_remove_stop_words:
        a_tokenized = [
            word for word in a_tokenized
            if not word.lower() in STOP_WORDS]
    
    if return_lower:
        a_tokenized = [word.lower() for word in a_tokenized]
                       
    return a_tokenized


def jaccard_similarity(c_set1, c_set2):
    i_intersect = len(c_set1.intersection(c_set2))
    d_union = float(len(c_set1.union(c_set2)))
    if not d_union: return 0.0
    return i_intersect / d_union


def edit_distance(s_1, s_2, b_case_insensitive=False):
    ''' returns: character levenshtein distance '''    
    if s_1 == s_2: return 0
    i_len1, i_len2 = len(s_1), len(s_2)
    if i_len1 == 0: return i_len2
    if i_len2 == 0: return i_len1
    
    np_dist = np.zeros([i_len1 + 1, i_len2 + 1], dtype=int)
    
    if b_case_insensitive:
        s_1, s_2 = s_1.lower(), s_2.lower()
      
    np_dist[:, 0] = np.array(range(i_len1 + 1), dtype=int)
    np_dist[0, :] = np.array(range(i_len2 + 1), dtype=int)
    
    for i_x, s_x in enumerate(s_1):
        x = i_x + 1
        for i_y, s_y in enumerate(s_2):
            y = i_y + 1
            if s_x == s_y:
                np_dist[x, y] = np_dist[x - 1, y - 1]
            else:
                np_dist[x, y] = 1 + min(np_dist[x - 1, y], np_dist[x, y - 1], np_dist[x - 1, y - 1])

    return np_dist[-1, -1]


def convert_date(s_date):
    ''' https://www.tutorialspoint.com/python/time_strptime.htm '''
    t_date = re.split(r"[/\\]", s_date)
    t_delimiter = re.findall(r"[/\\]", s_date)
    
    s_month_format = "%m"
    s_day_format = "%d"
    s_year_format = "%Y" if len(t_date[2]) == 4 else "%y"
    
    s_format = ''.join([
        s_month_format,
        t_delimiter[0],
        s_day_format,
        t_delimiter[0],
        s_year_format])
    
    dt_date = datetime.strptime(
        s_date,
        s_format
    )
    return dt_date


class ZippedSheets:
    ''' work with a set of spreadsheet files inside of a zip file.
        If the files are xlsx, the worksheet to be read must be the first sheet in the workbook.
    '''
    def __init__(self, s_zip_path, s_temp_folder='', **kwargs):
        ''' Arguments:
            s_zip_path: {str} the full path to the zip file
            s_temp_folder: {str} optional path to use to temporarily extract the spreadsheet files
                instead of using the same folder that the zip file is in
        '''
        if not os.path.isfile(s_zip_path):
            raise Exception('Zip file does not exist:', s_zip_path)
        
        self._zip_path = s_zip_path
        self._file_paths, self._files = [], []
        self._temp_folder = s_temp_folder if s_temp_folder else os.path.split(s_zip_path)[0]
        if not os.path.isdir(self._temp_folder):
            raise Exception('Temporary folder does not exist:', self._temp_folder)
        self.__is_open__ = False
    
    def __extract__(self):
        ''' extract the files to the temp subdirectory '''
        # extract files and get relative path/filenames of the files inside
        with ZipFile(self._zip_path, 'r') as o_zip:
            self._files = o_zip.namelist()
            o_zip.extractall(self._temp_folder)

        # absolute paths
        self._file_paths = [os.path.join(self._temp_folder, s_file) for s_file in self._files]
        self.__is_open__ = True
        
    def iterfilenames(self, **kwargs):
        ''' Returns:
                {iterator} of tuples of (filename, pandas sheet) '''
        if not self.__is_open__:
            self._open()
            self.__is_open__ = False
        
        for s_file in self._file_paths:
            #if not s_file.lower().endswith(('.xlsx', '.csv', '.txt', '.tab', 'tsv')):
            #    continue
            #pd_sheet = open_sheet(s_file, **kwargs)
            yield s_file
        
        if not self.__is_open__:
            self._close()
        
    def itersheets(self, **kwargs):
        ''' Returns:
                {iterator} of tuples of (filename, pandas sheet) '''
        if not self.__is_open__:
            self._open()
            self.__is_open__ = False
        
        for s_file in self._file_paths:
            if not s_file.lower().endswith(('.xlsx', '.csv', '.txt', '.tab', 'tsv')):
                continue
            pd_sheet = open_sheet(s_file, **kwargs)
            yield (s_file, pd_sheet)
        
        if not self.__is_open__:
            self._close()
            
    def allsheets(self, **kwargs):
        ''' Returns:
                {dict} of {filename1: pandas_sheet_1...} '''
        if not self.__is_open__:
            self._open()
            self.__is_open__ = False
            print(self._file_paths)
            print(self._files)
        
        e_sheets = {}
        for s_path, s_file in zip(self._file_paths, self._files):
            if not s_file.lower().endswith(('.xlsx', '.csv', '.txt', '.tab', '.tsv')):
                continue
            e_sheets[s_file] = open_sheet(s_path, **kwargs)
        
        if not self.__is_open__:
            self._close()
        
        return e_sheets
    
    def __erase__(self):
        ''' remove the temporary extracted files '''
        for s_file in self._file_paths:
            if s_file and os.path.isfile(s_file):
                os.remove(s_file)
    
    def _open(self):
        self.__extract__()
        return self
    
    def _close(self):
        ''' remove the temporary files and clean up any temp directories created'''
        self.__erase__()
        c_paths = {
            os.path.split(s_file)[0]
            for s_file in self._files if s_file}
        #print(c_paths)
        for s_file in c_paths:
            if not s_file: continue
            if s_file[-1] not in "\\/": s_file += "/"
            clear_sub_folders(s_file, self._temp_folder)
        self._file_paths = []
        #self._files = []
        self.__is_open__ = False
        return self
        
    def __enter__(self):
        ''' For use when opening the class via a with statement '''
        #print('start with')
        self.__extract__()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        ''' For use when closing the class via a with statement '''
        self._close()
        #print('close with')
        return self


def check_path(s_path, s_default_folder, required_existence=True, required_file=True, required_extension=None, s_name=''):
    ''' Check if path is an absolute path. If not, add its filename to the default folder
        If required_extension is specified, check that that
    '''
    if not s_path:
        raise Exception('{} path not specified'.format(s_name).strip())
    
    
    t_path = os.path.split(s_path)
    if required_file:
        assert t_path[1]
        if not s_name: s_name = t_path[1]
    else:
        if not s_name:
            if t_path[1]:
                s_name = t_path[1]
            else:
                s_name = t_path[0]
    
    if not t_path[0] and s_default_folder:
        # if folder is not included, use the default folder
        s_path = os.path.join(s_default_folder, s_path)
    
    if required_existence:
        if required_file and not os.path.isfile(s_path):
            raise Exception('{} file path is broken: "{}"'.format(s_name, s_path))
        elif not os.path.exists(s_path):
            raise Exception('{} path is broken: "{}"'.format(s_name, s_path))
    
    if required_extension and not s_path.lower().endswith(required_extension):    
        #assert s_path.lower().endswith(required_extension)
        raise Exception('{} file has incorrect extension: {}'.format(s_name, required_extension))
    return s_path


def convert_command_line_arg(s_value, b_bool=True, b_int=True, b_float=True):
    ''' Convert the value of a str command line argument value into bool, int, float or list if appropriate '''
    if isinstance(s_value, list):
        return [convert_command_line_arg(x, b_bool=b_bool, b_int=b_int, b_float=b_float) for x in s_value]
    elif isinstance(s_value, tuple):
        return tuple([convert_command_line_arg(x, b_bool=b_bool, b_int=b_int, b_float=b_float) for x in s_value])
    elif not isinstance(s_value, str):
        return s_value
    
    s_value = s_value.strip()
    if s_value.lower() in {'yes', 'true', 'on'}:
        if b_bool: return True
        else: return s_value
    elif s_value.lower() in {'no', 'false', 'off'}:
        if b_bool: return False
        else: return s_value
    elif b_int or b_float and re.search(r"\d", s_value) and not re.search(r"[a-zA-Z]", s_value):
        if b_int and re.match(r"^(\d{1,3},)*\d+$", s_value):
            return int(re.sub(r"[^\d]", "", s_value))
        elif b_float and re.match(r"^((\d{1,3},)*\d+(\.\d*)?|.\d+)$", s_value):
            return float(re.sub(r"[^\d.]", "", s_value))
        else:
            return s_value
    else:
        return s_value
    

def parse_command_line_args(sys_argv):
    ''' parse the command line arguments and keyword arguments into a list and dict
        Arguments:
            {list} from sys.argv
        Returns:
            {tuple} of ({list} arguments, {dict} keyword arguments)
    '''
    a_args, e_kwargs = [], {}
    
    idx = 1
    while idx < len(sys_argv):
        #print(idx, sys_argv[idx])
        if sys_argv[idx].startswith('--'):
            s_key = sys_argv[idx][2:]
            #print(f'sys_argv[{idx}]: "{s_key}"')
            if ":" in s_key:
                if re.search(r": *[\"'([]? *$", s_key):  # only the key was captured, not the value
                    s_key = s_key.strip()
                    #print(f'only key in sys_argv[{idx}]: "{s_key}"')
                    a_values = []
                    s_type = ""
                    if s_key[-1] == ":":
                        s_key = s_key[:-1]
                        while idx + 1 < len(sys_argv) and not sys_argv[idx + 1].startswith('--'):  # no indication of start of list, and next item(s) could be value
                            idx += 1
                            val = sys_argv[idx].strip()
                            if val: a_values.append(val)
                    elif s_key[-1] == "[":
                        s_key = re.sub(r": *\[$", "", s_key[:-1])
                        s_type = "list"
                        while idx + 1 < len(sys_argv) and not sys_argv[idx + 1].startswith('--'):  # start of list and and next item(s) could be value
                            idx += 1
                            val = sys_argv[idx].strip()
                            if "]" in val:
                                val = re.sub(r"\].*$", "", val)
                                sys_argv[idx] = re.sub(r"^.*\]", "", sys_argv[idx])
                                if val: a_values.append(val)
                                idx -= 1
                                break
                            else:
                                if val: a_values.append(val)
                    elif s_key[-1] == "(":
                        s_key = re.sub(r": *\($", "", s_key[:-1])
                        s_type = "tuple"
                        while idx + 1 < len(sys_argv) and not sys_argv[idx + 1].startswith('--'):  # start of list and and next item(s) could be value
                            idx += 1
                            val = sys_argv[idx].strip()
                            if ")" in val:
                                val = re.sub(r"\).*$", "", val)
                                sys_argv[idx] = re.sub(r"^.*\)", "", sys_argv[idx])
                                if val: a_values.append(val)
                                idx -= 1
                                break
                            else:
                                if val: a_values.append(val)
                    #else:
                    #    if idx + 1 < len(sys_argv) and not sys_argv[idx + 1].startswith('--'):
                        
                        
                    if a_values:
                        if a_values[0].startswith("["):
                            a_values[0] = a_values[0][1:]
                            s_type = "list"
                        elif a_values[0].startswith("("):
                            a_values[0] = a_values[0][1:]
                            s_type = "tuple"
                        for i_val, s_val in enumerate(a_values):
                            #print(f'a_values[{i_val}] == "{s_val}"')
                            if s_val.endswith("]"):
                                a_values[i_val] = s_val[:-1]
                                if i_val < len(a_values) - 1:
                                    idx -= ((len(a_values) - 1) - i_val)
                            elif s_val.endswith(")"):
                                a_values[i_val] = s_val[:-1]
                                if i_val < len(a_values) - 1:
                                    idx -= ((len(a_values) - 1) - i_val)
                                    
                    if s_type == "list":
                        val = a_values
                    elif s_type == "tuple":
                        val = tuple(a_values)
                    elif s_type == "" and len(a_values) == 1:
                        val = a_values[0]
                    else:
                        val = a_values
                    
                    e_kwargs[s_key] = val
                        
                else:
                    s_key, val = s_key.split(':', 1)
                    val = val.strip()
                    if val.startswith(("[", "(")):
                        s_start = val[0]
                        if s_start == "[" and val.endswith("]"):
                            a_values = val[1:-1].split(",")
                            s_type = "list"
                        elif s_start == "(" and val.endswith(")"):
                            a_values = tuple(val[1:-1].split(","))
                            s_type = "tuple"
                        else:
                            if s_start == "(":
                                s_type = "tuple"
                            elif s_start == "[":
                                s_type = "list"
                            
                            #print("val", val)
                            a_values = [x.strip() for x in val[1:].split(",") if x.strip()]
                            
                            while idx + 1 < len(sys_argv) and not sys_argv[idx + 1].startswith('--'):  # next item(s) could be more values
                                idx += 1
                                val = sys_argv[idx].strip()
                                if s_type == "tuple" and ")" in val:
                                    val = re.sub(r"\).*$", "", val)
                                    sys_argv[idx] = re.sub(r"^.*\)", "", sys_argv[idx])
                                    if val: a_values.append(val)
                                    idx -= 1
                                    break
                                elif s_type == "list" and "]" in val:
                                    val = re.sub(r"\].*$", "", val)
                                    sys_argv[idx] = re.sub(r"^.*\]", "", sys_argv[idx])
                                    if val: a_values.append(val)
                                    idx -= 1
                                    break
                                else:
                                    if val: a_values.append(val)
                        
                        if s_type == "list":
                            val = a_values
                        elif s_type == "tuple":
                            val = tuple(a_values)
                        elif s_type == "" and len(a_values) == 1:
                            val = a_values[0]
                        else:
                            val = a_values
                    
                    if s_key in e_kwargs:
                        if isinstance(e_kwargs[s_key], list):
                            e_kwargs[s_key].append(val)
                        else:
                            e_kwargs[s_key] = [e_kwargs[s_key], val]
                    else:
                        e_kwargs[s_key] = val
                    
            elif idx + 1 < len(sys_argv) and not sys_argv[idx + 1].startswith('--'):
                val = sys_argv[idx + 1]
                if s_key in e_kwargs:
                    if isinstance(e_kwargs[s_key], list):
                        e_kwargs[s_key].append(val)
                    else:
                        e_kwargs[s_key] = [e_kwargs[s_key], val]
                else:
                    e_kwargs[s_key] = sys_argv[idx + 1]
                idx += 1
            
            elif s_key not in e_kwargs:
                if ':' in s_key:
                    s_key, val = s_key.split(':', 1)
                else:
                    val = None
                
                if s_key in e_kwargs and val:
                    if isinstance(e_kwargs[s_key], list):
                        e_kwargs[s_key].append(val)
                    else:
                        e_kwargs[s_key] = [e_kwargs[s_key], val]
                else:
                    e_kwargs[s_key] = val
                
        else:
            a_args.append(convert_command_line_arg(sys_argv[idx]))
        idx += 1
        
    e_kwargs = {key.lower(): convert_command_line_arg(val) for key, val in e_kwargs.items()}
    
    return a_args, e_kwargs


def list_numbers(a_values):
    ''' Pass in flat mixed list and get back numbers (integers and non-nan floats)'''
    ad_numbers = []
    
    for v_val in a_values:
        if isinstance(v_val, int):
            ad_numbers.append(v_val)
        elif isinstance(v_val, (float, np.float32)):
            if np.isnan(v_val):
                pass
            else:
                ad_numbers.append(v_val)
        elif isinstance(v_val, str):
            try:
                ad_numbers.append(float(v_val.strip()))
            except:
                if '/' in v_val:
                    s_val = re_NONDIGITS_SIGNS.sub('', v_val)
                    v_val = to_float(s_val)
                    if not np.isnan(v_val):
                        ad_numbers.append(v_val)
                else:
                    s_val = re_NONDIGITS.sub('', v_val)
                    try:
                        ad_numbers.append(float(s_val.strip()))
                    except:
                        pass
    return ad_numbers


def value_list_stddev(a_values):
    ''' Return StdDev of non np.nan numbers. '''
    ad_vals = list_numbers(a_values)
    try:
        return np.std(ad_vals)
    except:
        return np.nan

def moving_average(a_values, d_smoothing_factor=0.5):
    ''' A form of a moving average that adjusts each subsequent
        number based on the one before it and a smoothing factor.
        This particular implementation has very little lag.
        Arguments:
            a_values: {list} of numbers
            d_smoothing_factor: {float} between 0-1 of how much use of the
                previous number vs the current number
                Smaller number means being closer to the original list
        Returns:
            {list} of smoothed numbers
    '''
    if len(a_values) < 2 or d_smoothing_factor <= 0: return a_values
    
    d_inverse = 1 - d_smoothing_factor
    d_moving = a_values[0]
    a_new = []
    
    for d_val in a_values:
        d_new = (d_val * d_inverse) + (d_moving * d_smoothing_factor)
        a_new.append(d_new)
        d_moving = d_new
    return a_new


def smooth(a_values, d_smoothing_factor=0.5):
    ''' Smooth a list of numbers using bi-directional moving averages
        going forward and backward
        Arguments:
            a_values: {list} of numbers
            d_smoothing_factor: {float} between 0-1 of how much use of the
                previous number vs the current number
                Smaller number means being closer to the original list
        Returns:
            {list} of smoothed numbers
    '''
    a_smooth = [
        round((x + y) / 2, 2)
        for x, y in zip(
            moving_average(a_values, d_smoothing_factor),
            reversed(moving_average(list(reversed(a_values)), d_smoothing_factor)))
    ]
    return a_smooth


def count_peaks(a_values, d_smooth_factor=0.5, d_peak_height=1.025):
    if len(a_values) < 3: return 0
    if d_smooth_factor > 0:
        a_values = smooth(a_values, d_smooth_factor)
    #print('a_values', a_values)
    a_loop = a_values + a_values + a_values
    i_peaks = 0
    for idx in range(len(a_values), len(a_values) * 2):
        d_pp, d_p, d_c, d_n, d_nn = a_loop[idx - 2], a_loop[idx - 1], a_loop[idx], a_loop[idx + 1], a_loop[idx + 2]
        #print(idx, d_pp, d_p, d_c, d_n, d_nn)
        if d_pp < d_p < d_c / d_peak_height and d_nn < d_n < d_c / d_peak_height:
            i_peaks +=1 
            #print('idx', idx, i_peaks)
    return i_peaks


def mean_of_products(a_values1, a_values2):
    """ The mean of the products of the corresponding values of bivariate data
        Arguments:
            a_values1: {list} of numbers
            a_values2: {list} of numbers
    """
    i_len = min(len(a_values1), len(a_values2))
    if i_len == 0: return 0.0
    total = 0.0
    
    for x, y in zip(a_values1, a_values2):
        total += (x * y)
    return total / i_len


def pearson_correlation(a_independent, a_dependent):
    """ Implements Pearson's Correlation, using several utility functions to
        calculate intermediate values before calculating and returning rho.
        Arguments:
            a_independent: {list} of numbers to check for correlation against a_dependent
            a_dependent: {list} of numbers to check for correlation against a_independent
        Returns:
            {float}
    """
    if not a_independent or not a_dependent: return 0.0
    
    if len(a_independent) != len(a_dependent):
        #if len(a_independent) < len(a_dependent):
        a_dependent = a_dependent[:len(a_independent)]
        a_independent = a_independent[:len(a_dependent)]
    
    # covariance
    independent_mean = np.mean(a_independent)
    dependent_mean = np.mean(a_dependent)
    products_mean = mean_of_products(a_independent, a_dependent)
    covariance = products_mean - (independent_mean * dependent_mean)

    # standard deviations of independent values
    independent_standard_deviation = value_list_stddev(a_independent)

    # standard deviations of dependent values
    dependent_standard_deviation = value_list_stddev(a_dependent)

    # Pearson Correlation Coefficient
    stdevs = independent_standard_deviation * dependent_standard_deviation
    rho = covariance / stdevs if stdevs != 0 else 0.0
    return rho

def trend_magnitude(a_values):
    ''' calculate the magnitude of the trend as a percentage of the difference between start and end averages
        the direction of the trend doesn't matter here
        Arguments:
            a_values: {list} of numbers
        Returns:
            {float} of the magnitude as a factor between 0.0 and 1.0
    '''
    if not a_values or len(a_values) < 2: return 0.0
    d_start, d_end = 0.0, 0.0
    a_factors = [(i) / (len(a_values) - 1) for i in range(len(a_values))]
    for idx, val in enumerate(a_values):
        d_factor = a_factors[idx]
        d_start += (val * (1 - d_factor))
        d_end += (val * d_factor)
    
    if d_start == 0 or d_end == 0:
        return 0.0
    #print(a_values, d_start, d_end)
    return 1 - (min(d_start, d_end) / max(d_start, d_end))


def period_diff(date1, date2):
    ''' Get a tuple of the component period differences between two dates '''
    return (
        date2.year - date1.year,
        date2.month - date1.month,
        date2.week - date1.week,
        date2.day - date1.day,
        date2.hour - date1.hour,
        date2.minute - date1.minute,
        date2.second - date1.second,
    )


def find_periodicity(a_period_diffs):
    ''' figure out the component interval periodicities in a list of time period_diff
        Return:
            {list} sorted by the most likely interval element(s) first
    '''
    d_year, d_month, d_week, d_day, d_hour, d_minute, d_second = 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
    ad_components = [0.0] * 7
    ea_components = defaultdict(list)
    for t_diff in a_period_diffs:
        for i_comp, i_diff in enumerate(t_diff):
            ea_components[i_comp].append(i_diff)
    
    e_components = {
        s_interval: (d_avg, d_std, d_avg % 1.0)
        for s_interval, d_avg, d_std in [
            (
                e_PERIOD_NAMES[i_comp],
                np.mean(a_vals),
                np.std(a_vals),
            )
            for i_comp, a_vals in ea_components.items()
            if min(a_vals) != 0 or max(a_vals) != 0]
    }
    return sorted(e_components.items(), key=lambda x: x[1][1] + x[1][2], reverse=False)


def make_interval(interval=None, quantity=None):
    ''' turn a specified interval name/index and quantity into a time object
        Arguments:
            interval: {str} like "month", 
    '''
    print('interval', interval)
    print('quantity', quantity)
    if interval is None: interval = 'days'
    interval = interval.lower()
    if quantity is None: quantity = 1
    
    if interval[-1] != 's': interval = "{}s".format(interval)
        
    if interval == 'months':
        interval = 'days'
        quantity = quantity * 30.4375
    elif interval == 'years':
        interval = 'days'
        quantity = quantity * 365
    e_kwargs = {interval: quantity}
    return timedelta(**e_kwargs)


def add_interval(date, interval=None, quantity=None, subtract_smaller_interval=True):
    ''' Add a specified interval name/index and quantity to a datetime object
        Arguments:
            date: {datetime.datetime}
            interval: {str} like "month", "hour", or {int} of 0-6
            quantity: {int}
    '''
    #print('interval', interval)
    #print('quantity', quantity)
    if interval is None: interval = 'day'
    if isinstance(interval, int): interval = e_PERIOD_NAMES[interval]
    interval_index = e_PERIOD_INDEXES[interval]
    interval = interval.lower()
    if quantity is None: quantity = 1
    
    if interval[-1] in {'s', 'S'}: interval = interval[:-1]
    
    if interval == 'week':
        date += timedelta(days=7)
    else:
        e_kwargs = {interval: date.__getattribute__(interval) + int(quantity)}
            
        #print('e_kwargs', e_kwargs)
        date = date.replace(**e_kwargs)
        
    if subtract_smaller_interval and interval_index < len(e_PERIOD_NAMES) - 1:
        if 0 <= interval_index < 3:
            #i_smaller = 3
            date -= timedelta(days=1)
        elif 3 <= interval_index < 6:
            #i_smaller = 6
            date -= timedelta(seconds=1)
        #s_small = e_PERIOD_NAMES[i_smaller]
        #e_kwargs[s_small] = date.__getattribute__(s_small) - 1
    return date


def secondsToStr(t):
    ''' convert seconds into a human-readable time string '''
    return "%d:%02d:%02d.%03d" % \
        reduce(lambda ll,b : divmod(ll[0],b) + ll[1:],
            [(t*1000,),1000,60,60])


def timer(func):
    ''' timing function decorator '''
    @wraps(func)
    def wrap(*args, **kwargs):
        n_start = time()
        if "function_time" in kwargs:
            s_func = kwargs["function_time"]
            #del kwargs["function_time"]
            kwargs = {key: val for key, val in kwargs if key != "function_time"}
        else:
            s_func = func.__name__
        result = func(*args, **kwargs)
        print("{} took: {}".format(s_func, secondsToStr(time() - n_start)))
        return result
    return wrap


def parse_special_chars(s_rule_piece, b_already_escaped=True):
    ''' Convert wildcard chars from sales_analysis format into
        regex patterns for compiling with re.compile()
        For special use characters for this function
            ? means 0 or 1 alphanumeric character
            * means 0 or more alphanumeric characters
            ~ means return the pieces separately
            | means alternate options
        Arguments:
            s_rule_piece: {str} the pattern to work on
            b_already_escaped: {bool} if pattern was already escaped with re.escape first
        Returns:
            {str} piece ready to be compiled
    '''
    if b_already_escaped:
        single_char = re.finditer(r"(\\\?)+", s_rule_piece)
    else:
        single_char = re.finditer(r"\?+", s_rule_piece)

    for re_single in reversed(list(single_char)):
        i_single_chars = len(re.sub(r"\\", "", re_single.group()))
        s_rule_piece = ''.join([
            s_rule_piece[:re_single.start()],
            "[a-zA-Z0-9]{,%s}" % (i_single_chars),
            s_rule_piece[re_single.end():]])
    #print(s_rule_piece)

    if b_already_escaped:
        #multi_char = re.finditer(r"(\\\*)+", s_rule_piece)
        s_rule_piece = re.sub(r"(\\\*)+", r"[a-zA-Z0-9]*", s_rule_piece)
        s_rule_piece = re.sub(r"\\([|^$])", r"\g<1>", s_rule_piece)
    else:
        #multi_char = re.finditer(r"\*+", s_rule_piece)
        s_rule_piece = re.sub(r"\*+", r"[a-zA-Z0-9]*", s_rule_piece)
    
    #if (
    #    re.search(r"[^\\]\|", s_rule_piece) and
    #    #(s_rule_piece[0] != "(" or s_rule_piece[-1] != ")")
    #    (not re.search(r"^\^?\(", s_rule_piece) or not re.search(r"\)\$?$", s_rule_piece))
    #):
    if  re.search(r"[^\\]\|", s_rule_piece):
        if s_rule_piece[0] == "^" and s_rule_piece[1] != "(":
            s_rule_piece = "^(" + s_rule_piece[1:]
        elif s_rule_piece[0] != "(":
            s_rule_piece = "(" + s_rule_piece
        
        
        if s_rule_piece[-1] == "$" and s_rule_piece[-2] != ")":
            s_rule_piece = s_rule_piece[:-1] + ")$"
        elif s_rule_piece[-1] != "(":
            s_rule_piece = s_rule_piece + ")"
        
        #print('s_rule_piece', s_rule_piece)
        #s_rule_piece = "({})".format(s_rule_piece)
    
    return s_rule_piece


def get_regex_pieces(s_rule_text, no_split=False):
    ''' turn a pattern with optional wild cards and split point into a compiled regex
        For special use characters for this function
            ? means 0 or 1 alphanumeric character
            * means 0 or more alphanumeric characters
            ~ means return the pieces separately
            | means alternate options
        Arguments:
            s_rule_text: {str} that looks like "_HRR", or "??
            no_split: {bool} don't split up pieces using "~"
        Returns:
            {list} of the string regex pattern pieces
    '''
    if no_split:
        s_rule_text.replace("~", "")
    
    #a_regex_pieces = map(parse_special_chars, map(re.escape, s_rule_text.split(("~"))))
    a_regex_pieces = map(
        parse_special_chars,
        map(
            re.escape,
            filter(lambda x: x,
                re.split(r"[~]", s_rule_text))
        )
    )
    return a_regex_pieces


def get_regex(s_rule_text, no_split=False, s_prefix="", s_suffix="", b_case_insensitive=False):
    ''' turn a pattern with optional wild cards and split point into a compiled regex
        For special use characters for this function
            ? means 0 or 1 alphanumeric character
            * means 0 or more alphanumeric characters
            ~ means return the pieces separately
            | means alternate options
            ^ means at start
            $ means at end
        Arguments:
            s_rule_text: {str} that looks like "_HRR", or "??
            no_split: {bool} don't split up pieces using "~"
        Returns:
            {_sre.SRE_Pattern} compiled regex pattern for rule text
    '''
    
    if s_prefix == "^":
        s_rule_text = replace_all_but_first(s_rule_text, "~")
    elif s_suffix == "$":
        s_rule_text = replace_all_but_last(s_rule_text, "~")
    
    a_regex_pieces = list(get_regex_pieces(s_rule_text, no_split=no_split))
    if not a_regex_pieces: return None
    
    if len(a_regex_pieces) > 1:
        s_regex_pattern = "".join([
            "({})".format(s_piece)
            for s_piece in a_regex_pieces
            if s_piece])
    else:
        s_regex_pattern = a_regex_pieces[0]
    
    if s_regex_pattern:
        s_regex_pattern = "".join([s_prefix, s_regex_pattern, s_suffix,])
        if b_case_insensitive:
            return re.compile(s_regex_pattern, flags=re.I)
        else:
            return re.compile(s_regex_pattern)
    else:
        return None


def conjunction(operator, *conditions):
    ''' create a list of conditions to use as an operator for things like pandas dataframe filtering
        by concating individual conditions with an operator like np.logical_or or np.logical_and
        
        Example:
            e_mins = {
                "price": 10.99},
            e_maxes = {
                "quantity": 99},
            a_conditions = [df[col] >= minimum for col, minimum in e_mins.items()]
            a_conditions.append([df[col] <= minimum for col, minimum in e_maxes.items()])
            pd_filtered = df[conjunction(np.logical_or, *a_conditions, *exclusions)]
    '''
    return reduce(operator, conditions)


def round_sig(x, sig=2):
    if x == 0: return 0.0
    return round(x, sig - int(floor(log10(abs(x)))) - 1)


def round_log(x, prec=2):
    assert x > 0
    d_rounded = 10 ** (round(math.log10(x) / 2, 1) * 2)
    return round(d_rounded, prec)

def round_to(x, prec=2, base=.05):
    d_rounded = round(base * round(float(x)/base),prec)
    return d_rounded

def significant_digits(number, i_max_sig=-1):
    ''' Calculate significant digits for decimal numbers, with normal maximums
        for currency and percentages. Can also specify a maximum number of
        significant digits.
        Arguments:
            number: {int}, {float} or {str} decimal number
        Returns:
            {int} suggested number of significant digits
    '''
    if isinstance(number, int): return 0
    if isinstance(number, float):
        if np.isnan(number): return 0
    
    if isinstance(number, str): 
        if re_CURRENCY_SIG.search(number):
            i_max_sig = min(2, i_max_sig) if i_max_sig != -1 else 2
        elif "%" in number:
            i_max_sig = min(4, i_max_sig) if i_max_sig != -1 else 4
        else:
            if i_max_sig < 0: i_max_sig = 8
        re_dec = re_DECIMAL_SIG.search(number)
        if not re_dec: return 0

        s_dec = re_dec.groups()[0]
        i_digits = len(s_dec)
    else:
        number = str(number)
        i_max_sig = min(8, i_max_sig) if i_max_sig != -1 else 8
        
        re_dec = re_DECIMAL_SIG.search(number)
        if not re_dec: return 0

        s_dec = re_dec.groups()[0]
        i_digits = len(s_dec)
        if i_digits == 1 and s_dec == "0": i_digits = 0
    
    return min(i_digits, i_max_sig)


def unique(items, b_blanks=True):
    ''' Get the unique items from an iterator in the form of a list
        in order of their first original appearences. '''
    a_unique = []
    c_unique = set()
    for x in items:
        if x not in c_unique:
            if b_blanks or x or isinstance(x, (int, float)):
                a_unique.append(x)
                c_unique.add(x)
    return a_unique
    

def yield_batches(a_input, i_min_batches=10, i_max_batch_size=100000, **kwargs):
    ''' split an input into either a minimum number of batches, or into batches of a maximum size. '''
    assert i_min_batches >= 1 or i_max_batch_size >= 1
    
    if i_min_batches <= 0 or i_max_batch_size <= (len(a_input) / i_min_batches):
        i_batches = int(len(a_input) / i_max_batch_size) if len(a_input) % i_max_batch_size ==  0 else int(len(a_input) / i_max_batch_size) + 1
        i_batch_size = i_max_batch_size
    elif i_max_batch_size <= 0 or i_min_batches >= int(len(a_input) / i_max_batch_size):
        i_batches = i_min_batches
        i_batch_size = int(len(a_input) / i_min_batches) if len(a_input) % i_min_batches ==  0 else int(len(a_input) / i_min_batches) + 1
    
    if kwargs.get("b_debug"):
        print(f"{i_batches} batches of up {i_batch_size} items.")
        
    i_start = 0
    while i_start < len(a_input):
        yield a_input[i_start: i_start + i_batch_size]
        i_start += i_batch_size


def get_time(format="%Y-%m-%d %H:%M:%S"):
    return datetime.now().strftime(format)


def diff_month(d1, d2):
    return (d1.year - d2.year) * 12 + d1.month - d2.month

def value_list_avg(a_values):
    ''' Return average of non np.nan numbers. '''
    ad_vals = list_numbers(a_values)
    if len(ad_vals) > 0:
        return np.mean(ad_vals)
    else:
        return np.nan

def value_list_stddev(a_values):
    ''' Return StdDev of non np.nan numbers. '''
    ad_vals = list_numbers(a_values)
    try:
        return np.std(ad_vals)
    except:
        return np.nan


def value_list_herfindahl(a_values):
    ''' Calculate the HHI from a list of numbers '''
    ad_vals = list_numbers(a_values)

    d_total = sum(ad_vals)
    if d_total != 0:
        return sum([(x / d_total)**2 for x in ad_vals])
    return 0


def value_list_similarity(a_values):
    ''' Return similarity of non np.nan numbers as factor. '''
    ad_vals = list_numbers(a_values)
    if len(ad_vals) > 0:
        d_mean = np.mean(ad_vals)
        if d_mean != 0:
            return 1 - (np.std(ad_vals) / np.mean(ad_vals))
        else:
            # for now return 0.0.
            # The ideal solution would be to adjust the numbers to all possitive then run again
            return 0.0
    else:
        return np.nan


def value_list_variation(a_values):
    ''' Return variation of non np.nan numbers as % of largest number. '''
    ad_vals = list_numbers(a_values)
    if len(ad_vals) > 0:
        d_max, d_min = max(ad_vals), min(ad_vals)
        if d_max == 0: return -d_min
        if d_max < 0: d_max, d_min = -d_min, -d_max
        return ((d_max - d_min) / d_max)
    else:
        return np.nan


def check_log_file(s_log_file, i_max_size=1048576, i_days_old=-1, b_monthly=False, b_force=False, **kwargs):
    ''' If log file is over a certain size or was created too long ago, copy it to another file and start a new log. '''
    if not os.path.isfile(s_log_file):
        with open(s_log_file, "w") as f: pass
        
        #return
    else:
        b_start_new_file = False
        i_size = os.path.getsize(s_log_file)
        
        dt_created = datetime.fromtimestamp(os.path.getctime(s_log_file))
        dt_current = datetime.now()
        i_age = (dt_current - dt_created).days
        
        if (i_max_size > 0 and i_size > i_max_size) or (i_days_old > 0 and i_age > i_days_old) or (b_monthly and dt_current.month != dt_created.month) or b_force:
    #        if logger is not None: logger.info(f"{get_time()}\tAPI User: {s_USER}. Incrementing log file: {s_reason}")
        
            s_copy_to = get_new_write_filename(s_log_file, b_dot_format=True)
            shutil.copy2(s_log_file, s_copy_to)
            
            try:
                logging.shutdown()
                os.remove(s_log_file)
                with open(s_log_file, "w") as f: pass
            except:
                with open(s_log_file, "w") as f: pass
    


def get_distance(point1, point2):
    # get distance in kilometers
    R = 6371
    lat1 = radians(point1[0])  #insert value
    lon1 = radians(point1[1])
    lat2 = radians(point2[0])
    lon2 = radians(point2[1])

    dlon = lon2 - lon1
    dlat = lat2- lat1

    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1-a))
    distance = R * c
    return distance


def free_port():
    """
    Determines a free port using sockets.
    """
    free_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    free_socket.bind(('0.0.0.0', 0))
    free_socket.listen(5)
    port = free_socket.getsockname()[1]
    free_socket.close()
    return port