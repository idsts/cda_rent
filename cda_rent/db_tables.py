﻿from datetime import datetime, timedelta
import pandas as pd
try:
    from . import mysql_db
except:
    import mysql_db

property_info = "property_info_new"

state_info = "state_info_new"
county_info = "county_info_new"
city_info = "city_info_new"
zipcode_info = "zipcode_info_new"

state_bedrooms = "state_bedrooms_new"
county_bedrooms = "county_bedrooms_new"
city_bedrooms = "city_bedrooms_new"
zipcode_bedrooms = "zipcode_bedrooms_new"

state_ptypes = "state_ptype_new"
county_ptypes = "county_ptype_new"
city_ptypes = "city_ptype_new"
zipcode_ptypes = "zipcode_ptype_new"

state_ptype_bedrooms = "state_ptype_bedrooms_new"
county_ptype_bedrooms = "county_ptype_bedrooms_new"
city_ptype_bedrooms = "city_ptype_bedrooms_new"
zipcode_ptype_bedrooms = "zipcode_ptype_bedrooms_new"

area_info = "area_info"
hotels = "hotels"

interactive_requests = "interactive_requests"
predictor_results = "predictor_results"

zcta = "zcta"
zip_to_zcta = "zip_to_zcta"
score_weights = "score_weights"
state_utility_costs = "state_utility_costs"


o_DB = None

a_TABLES = None
a_ZIP_INFO_COLS = None
a_CITY_INFO_COLS = None
a_COUNTY_INFO_COLS = None
a_STATE_INFO_COLS = None
a_PROP_INFO_COLS = None

a_ZIP_BEDROOM_COLS = None
a_CITY_BEDROOM_COLS = None
a_COUNTY_BEDROOM_COLS = None
a_STATE_BEDROOM_COLS = None

a_ZIP_PTYPE_COLS = None
a_CITY_PTYPE_COLS = None
a_COUNTY_PTYPE_COLS = None
a_STATE_PTYPE_COLS = None

a_ZIP_PTYPE_BEDROOM_COLS = None
a_CITY_PTYPE_BEDROOM_COLS = None
a_COUNTY_PTYPE_BEDROOM_COLS = None
a_STATE_PTYPE_BEDROOM_COLS = None

dt_CONFIG = datetime.utcnow()
dt_CONFIG_RELOAD = dt_CONFIG + timedelta(hours=1)
e_CONFIG = {}

i_CURRENT_YEAR = datetime.utcnow().year


def get_connection():
    o_db = mysql_db.mySqlDbConn(
        s_host=mysql_db._host,
        s_schema="predictor",
        s_user=mysql_db._user,
        s_pass=mysql_db._password,
    )
    return o_db

def load_db(**kwargs):
    global o_DB
    if o_DB is None or kwargs.get("reload"):
        #o_DB = mysql_db.mySqlDbConn(
        #    s_host=mysql_db._host,
        #    s_schema="predictor",
        #    s_user=mysql_db._user,
        #    s_pass=mysql_db._password,
        #)
        o_DB = get_connection()
    
    global a_TABLES
    if not a_TABLES or kwargs.get("reload"):
        a_TABLES = o_DB.list_tables()
    c_table_names = set([t[0] for t in a_TABLES])
    
    assert property_info in c_table_names
    assert state_info in c_table_names
    assert county_info in c_table_names
    assert city_info in c_table_names
    assert zipcode_info in c_table_names
    assert state_bedrooms in c_table_names
    assert county_bedrooms in c_table_names
    assert city_bedrooms in c_table_names
    assert zipcode_bedrooms in c_table_names
    assert state_ptypes in c_table_names
    assert county_ptypes in c_table_names
    assert city_ptypes in c_table_names
    assert zipcode_ptypes in c_table_names
    assert state_ptype_bedrooms in c_table_names
    assert county_ptype_bedrooms in c_table_names
    assert city_ptype_bedrooms in c_table_names
    assert zipcode_ptype_bedrooms in c_table_names
    assert area_info in c_table_names
    assert hotels in c_table_names
    
    assert zcta in c_table_names
    assert zip_to_zcta in c_table_names
    assert interactive_requests in c_table_names
    assert predictor_results in c_table_names
    assert property_info in c_table_names

    global a_ZIP_INFO_COLS, a_COUNTY_INFO_COLS, a_CITY_INFO_COLS, a_STATE_INFO_COLS
    if not a_ZIP_INFO_COLS or kwargs.get("reload"): a_ZIP_INFO_COLS = o_DB.get_table_cols(zipcode_info)
    if not a_CITY_INFO_COLS or kwargs.get("reload"): a_CITY_INFO_COLS = o_DB.get_table_cols(city_info)
    if not a_COUNTY_INFO_COLS or kwargs.get("reload"): a_COUNTY_INFO_COLS = o_DB.get_table_cols(county_info)
    if not a_STATE_INFO_COLS or kwargs.get("reload"): a_STATE_INFO_COLS = o_DB.get_table_cols(state_info)
    
    global a_PROP_INFO_COLS
    if not a_PROP_INFO_COLS or kwargs.get("reload"):
        a_PROP_INFO_COLS = o_DB.get_table_cols(property_info)
    
    global a_ZIP_BEDROOM_COLS, a_COUNTY_BEDROOM_COLS, a_CITY_BEDROOM_COLS, a_STATE_BEDROOM_COLS
    if not a_ZIP_BEDROOM_COLS or kwargs.get("reload"): a_ZIP_BEDROOM_COLS = o_DB.get_table_cols(zipcode_bedrooms)
    if not a_CITY_BEDROOM_COLS or kwargs.get("reload"): a_CITY_BEDROOM_COLS = o_DB.get_table_cols(city_bedrooms)
    if not a_COUNTY_BEDROOM_COLS or kwargs.get("reload"): a_COUNTY_BEDROOM_COLS = o_DB.get_table_cols(county_bedrooms)
    if not a_STATE_BEDROOM_COLS or kwargs.get("reload"): a_STATE_BEDROOM_COLS = o_DB.get_table_cols(state_bedrooms)
    
    global a_ZIP_PTYPE_BEDROOM_COLS, a_COUNTY_PTYPE_BEDROOM_COLS, a_CITY_PTYPE_BEDROOM_COLS, a_STATE_PTYPE_BEDROOM_COLS
    if not a_ZIP_PTYPE_BEDROOM_COLS or kwargs.get("reload"): a_ZIP_PTYPE_BEDROOM_COLS = o_DB.get_table_cols(zipcode_ptype_bedrooms)
    if not a_CITY_PTYPE_BEDROOM_COLS or kwargs.get("reload"): a_CITY_PTYPE_BEDROOM_COLS = o_DB.get_table_cols(city_ptype_bedrooms)
    if not a_COUNTY_PTYPE_BEDROOM_COLS or kwargs.get("reload"): a_COUNTY_PTYPE_BEDROOM_COLS = o_DB.get_table_cols(county_ptype_bedrooms)
    if not a_STATE_PTYPE_BEDROOM_COLS or kwargs.get("reload"): a_STATE_PTYPE_BEDROOM_COLS = o_DB.get_table_cols(state_ptype_bedrooms)
    
    global a_ZIP_PTYPE_COLS, a_COUNTY_PTYPE_COLS, a_CITY_PTYPE_COLS, a_STATE_PTYPE_COLS
    if not a_ZIP_PTYPE_COLS or kwargs.get("reload"): a_ZIP_PTYPE_COLS = o_DB.get_table_cols(zipcode_ptypes)
    if not a_CITY_PTYPE_COLS or kwargs.get("reload"): a_CITY_PTYPE_COLS = o_DB.get_table_cols(city_ptypes)
    if not a_COUNTY_PTYPE_COLS or kwargs.get("reload"): a_COUNTY_PTYPE_COLS = o_DB.get_table_cols(county_ptypes)
    if not a_STATE_PTYPE_COLS or kwargs.get("reload"): a_STATE_PTYPE_COLS = o_DB.get_table_cols(state_ptypes)
    
    global i_CURRENT_YEAR
    i_CURRENT_YEAR = datetime.utcnow().year


def load_config(b_reload=False, **kwargs):
    global e_CONFIG
    global dt_CONFIG, dt_CONFIG_RELOAD
    
    
    if b_reload or not e_CONFIG or not dt_CONFIG or not dt_CONFIG_RELOAD or dt_CONFIG_RELOAD <= dt_CONFIG:
        print("Loading config from db")
        df_config = o_DB.get_table_as_df("config").set_index("key")
        e_CONFIG = {}
        for key, vals in df_config.iterrows():
            for val_type, val in dict(vals).items():
                if val_type.startswith("value_") and not pd.isnull(val):
                    if val_type == "value_date":
                        #e_CONFIG[key] = datetime.fromtimestamp(val)
                        e_CONFIG[key] = val
                        break
                    elif val_type == "value_int":
                        e_CONFIG[key] = int(val)
                        if e_CONFIG[key] != 0: break
                    elif val_type == "value_float":
                        e_CONFIG[key] = float(val)
                        if e_CONFIG[key] != 0: break
                    else:
                        e_CONFIG[key] = val
                        break
        
        dt_CONFIG = datetime.utcnow()
        dt_CONFIG_RELOAD = dt_CONFIG + timedelta(hours=e_CONFIG.get("config_reload_hours", 1), minutes=e_CONFIG.get("config_reload_minutes", 0))
    
    if not e_CONFIG.get("latest_dataset_quarter"):
        e_CONFIG["latest_dataset_quarter"] = datetime(year=2022, month=2, day=7)
    
    global i_CURRENT_YEAR
    i_CURRENT_YEAR = datetime.utcnow().year
    
load_db()
load_config()