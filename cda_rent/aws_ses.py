﻿import os, sys, re, smtplib, ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

try:
    from . import utils, mysql_db
except:
    import utils, mysql_db

try:
    import boto3
    from botocore.exceptions import ClientError
except:
    boto3 = None
    ClientError = None


o_DB = None
s_BLOCKED_TABLE = "blocked_emails"

s_SMTP_SERVER = "email-smtp.us-east-1.amazonaws.com"
s_PORT = "465"  # For SSL")

#ses-smtp-user.20211020-141306
s_USERNAME = os.environ.get("CDA_AWS_SES_USER", "")
s_PASSWORD = os.environ.get("CDA_AWS_SES_PASS", "")
assert s_USERNAME and s_PASSWORD

#s_FROM = "Revr Rent Prediction <api@idsts.com>"
s_FROM = "Revr Rent Prediction <info@revr.com>"
#AWS_REGION = "us-east-2"
AWS_REGION = "us-east-1"
CHARSET = "UTF-8"  # The character encoding for the email.

re_ALLOWED_FROM = re.compile(r"(.+@Revr\.com|.+@RunRevr\.com|.+@cdacommercial\.com|timothy@idsts\.com|api@idsts\.com|.+@idsts.net)$", flags=re.I)

re_EMAIL_FROM_RECIPIENT = re.compile("(^.*<|>.*$)")


def send_email(v_to, s_subject, s_text, s_html="", s_from=None, v_cc=None, v_bcc=None, **kwargs):
    ''' takes email information, cleans/filters it, and tries to send via SMTP or BOTO3 library
    '''
    global o_DB
    if o_DB is None: load_db()

    # Check inputs
    if s_from:
        s_from_email = re_EMAIL_FROM_RECIPIENT.sub("", s_from)
        #if not re_ALLOWED_FROM.match(s_from_email):
        #    raise Exception(f'Cannot send an email from "{s_from}" until it is allowed on AWS SES Identities as an individual address or whole domain')
    else:
        s_from = s_FROM
    
    e_recipients = {
        "to": filter_recipients(clean_recipients(v_to)),
        "cc": filter_recipients(clean_recipients(v_cc)),
        "bcc": filter_recipients(clean_recipients(v_bcc)),
    }
    for k, e in e_recipients.items():
        if e.get("invalid"):
            print(f"Invalid {k} email(s):", e.get("invalid"))
    
    if not e_recipients.get("to", {}).get("valid", []):
        raise Exception(f'Must include at least one valid "to" recipient.', e_recipients)
    
    if sum([len(e_recipients.get("valid", [])) for e_recipients in e_recipients.values()]) >= 50:
        raise Exception(f'Too many recipients for AWS SES. Split into smaller batches of < 50 recipients at a time.')
    
    if s_subject is None or not s_subject.strip():
        raise Exception(f'Must include a subject.')
    
    if s_text is None or not s_text.strip():
        raise Exception(f'Must include a raw text version of the email.')
        
    if s_html is None or not s_html.strip():
        s_html = re.sub("(\r?\n){2,}", "</p>\n        <p>", s_text)
        s_html = f"""
<html>
    <body>
        <p>{s_html}</p>
    </body>
</html>
"""
    if not kwargs.get("boto") and (kwargs.get("smtp") or boto3 is None):
        o_result = _send_smtp(s_from, e_recipients, s_subject, s_text, s_html=s_html, **kwargs)
    else:
        o_result = _send_boto3(s_from, e_recipients, s_subject, s_text, s_html=s_html, **kwargs)
    return {"recipients": e_recipients, "email": o_result}


def _send_smtp(s_from, e_recipients, s_subject, s_text, s_html="", **kwargs):
    ''' takes the cleaned/filtered arguments from send_email() and
        sends an email via AWS SES SMTP
    '''
    # Create Message
    o_message = MIMEMultipart("alternative")
    o_message["Subject"] = s_subject
    o_message["From"] = s_from
    s_to = ", ".join(e_recipients.get("to", {}).get("valid", []))
    o_message["To"] = s_to
    
    if e_recipients.get("cc", {}).get("valid", []):
        o_message["Cc"] = ", ".join(e_recipients["cc"]["valid"])
    if e_recipients.get("bcc", {}).get("valid", []):
        o_message["Bcc"] = ", ".join(e_recipients["bcc"]["valid"])
    
    o_message.attach(MIMEText(s_text, "plain"))
    o_message.attach(MIMEText(s_html, "html"))
    
    # Create a secure SSL context
    o_context = ssl.create_default_context()
    
    with smtplib.SMTP_SSL(host=s_SMTP_SERVER, port=str(s_PORT), context=o_context) as server:
        server.login(s_USERNAME, s_PASSWORD)
        server.sendmail(s_from, s_to, o_message.as_string())
    return o_message.as_string()


def _send_boto3(s_from, e_recipients, s_subject, s_text, s_html="", **kwargs):
    # Create a new SES resource and specify a region.
    session = boto3.Session(profile_name="cory")
    
    client = session.client('ses', region_name=AWS_REGION)
    
    e_destination = {'ToAddresses': e_recipients.get("to", {}).get("valid", [])}
    a_cc = e_recipients.get("cc", {}).get("valid", [])
    if a_cc: e_destination['CcAddresses'] = a_cc
    a_bcc = e_recipients.get("bcc", {}).get("valid", [])
    if a_bcc: e_destination['BccAddresses'] = a_bcc
    
    e_message = {
        'Body': {
            'Html': {'Charset': CHARSET, 'Data': s_html},
            'Text': {'Charset': CHARSET, 'Data': s_text},
        },
        'Subject': {'Charset': CHARSET, 'Data': s_subject},
    }
    # Try to send the email.
    try:
        #Provide the contents of the email.
        response = client.send_email(
            Destination=e_destination,
            Message=e_message,
            Source=s_from,
            #ConfigurationSetName=CONFIGURATION_SET,  # If you are not using a configuration set, comment or delete the
        )
    # Display an error if something goes wrong.	
    except ClientError as e:
        if kwargs.get("b_debug"): print(e.response['Error']['Message'])
        return e.response['Error']
    else:
        if kwargs.get("b_debug"):
            print("Email sent! Message ID:"),
            print(response['MessageId'])
        
        return response


def clean_recipients(v_recipients):
    ''' clean and split list of email recipient addresses, whether initially as a CSV or list '''
    if not v_recipients:
        return []
    if isinstance(v_recipients, str): 
        return [x.strip() for x in v_recipients.split(",")]
    else:
        return list(v_recipients)


def filter_recipients(a_recipients):
    a_valid, a_invalid = [], []
    if a_recipients:
        global o_DB
        if o_DB is None: load_db()
        
        a_emails = [re_EMAIL_FROM_RECIPIENT.sub("", s_recipient) for s_recipient in a_recipients]
        c_blocked = set([
            x[0].lower()
            for x in o_DB.get_rows(s_BLOCKED_TABLE, "email", tuple(a_emails), columns="email")])
        
        for s_recipient in a_recipients:
            s_email = re_EMAIL_FROM_RECIPIENT.sub("", s_recipient)
            if s_email.lower() in c_blocked:
                a_invalid.append(s_recipient)
            else:
                a_valid.append(s_recipient)
    return {"valid": a_valid, "invalid": a_invalid}


def block_emails(a_to_block, s_reason="unknown reason"):
    ''' Add email(s) to list of blocked recipients that cannot be emailed
        Because of either consent or delivery failures
        Arguments:
            a_to_block: {list} of either {str} emails or {tuple} of (email, reason)
    '''
    a_emails_and_reason = []
    for contact in a_to_block:
        if not contact: continue
        if isinstance(contact, str) and "@" in contact:
            a_emails_and_reason.append((contact, s_reason))
        elif isinstance(contact, (tuple, list)) and "@" in contact[0]:
            a_emails_and_reason.append((contact[0], contact[1] if len(contact) > 1 else s_reason))
        else:
            raise Exception(f"Unrecognized format of contacts to block. Pass in either a list of {str} emails, or a {tuple} (email, block_reason)")
    
    if a_emails_and_reason:
        global o_DB
        if o_DB is None: load_db()
        o_DB.insert_batch(
            s_BLOCKED_TABLE,
            ["email", "reason"],
            a_emails_and_reason)


def get_sns_block_recipient(data):
    ''' take in the 'data' from an SNS SES notification and look for email addresses to block '''
    a_block = []
    if not data: return a_block
    
    if isinstance(data, str): data = json.loads(data)
    message = data.get("Message", {})
    if isinstance(message, str): message = json.loads(message)
    if not message: message = data
        
    s_notification_type = message.get("notificationType")
    
    #if s_notification_type.lower() == "complaint":
    a_targets = message.get("complaint", {}).get("complainedRecipients", [])
    for e_target in a_targets:
        if e_target.get("emailAddress"):
            a_block.append((e_target["emailAddress"], "complaint"))
    
    #elif s_notification_type.lower() == "bounce":
    if message.get("bounce", {}).get("bounceType", "").lower() not in {"transient"}:
        a_targets = message.get("bounce", {}).get("bouncedRecipients", [])
        for e_target in a_targets:
            if e_target.get("emailAddress"):
                a_block.append((e_target["emailAddress"], "bounce"))
        return a_block


def load_db():
    global o_DB
    o_DB = mysql_db.mySqlDbConn(
        s_host = mysql_db._host,
        i_port = mysql_db._port,
        s_user = mysql_db._user,
        s_pass = mysql_db._password,
        s_schema = mysql_db._schema,
    )
    e_tables = dict(o_DB.list_tables())
    if s_BLOCKED_TABLE not in e_tables:
        create_table()
    
    return o_DB


def create_table():
    o_DB.load()
    o_DB.create_table(
        s_BLOCKED_TABLE,
        [
            "id int AUTO_INCREMENT PRIMARY KEY",
            "email varchar(255)",
            "reason varchar(255)",
            "created datetime default now()",
        ])
    #o_DB.cursor.execute(f"CREATE INDEX id_index ON {s_BLOCKED_TABLE}(id)")
    o_DB.cursor.execute(f"CREATE INDEX email_index ON {s_BLOCKED_TABLE}(email(255))")
    o_DB.cursor.execute(f"CREATE INDEX reason_index ON {s_BLOCKED_TABLE}(reason(255))")
    o_DB.cursor.execute(f"CREATE INDEX created_index ON {s_BLOCKED_TABLE}(created)")
    o_DB.cursor.execute(f"ALTER TABLE {s_BLOCKED_TABLE} ADD CONSTRAINT unique_items UNIQUE (email)")
    o_DB.close()
