﻿import math
from collections import defaultdict
import numpy as np
import pandas as pd

#d_PREDICT_OCCUPANCY_RATE_LTM = 0.9
d_PREDICT_RECORDS = 6
d_PREDICT_AVAILABILITY = 0.95
d_PREDICT_OVERALL_RATING = 100 #96.12
d_PREDICT_RESPONSE_RATE = 100  #90.6
d_PREDICT_OWNER_PROPERTIES = 100
d_PREDICT_NUMBER_OF_PHOTOS = 75 #22
d_PREDICT_NUMBER_OF_REVIEWS = 75 #20
#d_PREDICT_DAYS = 365

#d_PREDICT_DEFAULT_OCCUPANCY = 0.26396
#d_PREDICT_DEFAULT_OCCUPANCY = 0.4
d_PREDICT_DEFAULT_OCCUPANCY = 0.47

d_PREDICT_DEFAULT_PROPERTY_CONDITION = 4

# use an adjustment scale factor on the occupancy number to account for owner-type and price-grab optimization
#d_PREDICT_OCCUPANCY_OPT_EXP = 0.65
e_PREDICT_OCCUPANCY_ADJ_EXP = {
    -3: 2.0,
    -2: 1.6,
    -1: 1.35,
    0: 1.05,
    1: 1,
    2: 1,
    3: 1,
    4: 1,
}
e_PREDICT_OCCUPANCY_OPT_EXP = {
    -3: 1,
    -2: 0.97,
    -1: 0.95,
    0: 0.92,
    1: 0.875,
    2: 0.82,
    3: 0.78,
    4: 0.75,
}

d_PREDICT_AVAILABILITY_SEASONALITY = 0.0  # total and per-bedroom revenue is highest for either completely season, or non-seasonal properties
d_PREDICT_AVAILABILITY_SEASON_LENGTH = 12
d_PREDICT_AVAILABILITY_SEASON_PEAK_MONTH = -1
d_PREDICT_AVAILABILITY_SEASON_MAX_MONTH = 7
d_PREDICT_AVAILABILITY_SEASON_TYPE_MONTH = 0
d_PREDICT_AVAILABILITY_SEASON_TYPE_QUARTER = 0


a_ALWAYS_AMENITIES = [
    "feature_washer", "feature_dryer",
    "feature_dishwasher", "feature_oven", "feature_stove",
    "feature_refrigerator", "feature_hot_water", "feature_microwave",
    "feature_hot_water", "feature_kitchen",
]
a_ALWAYS_SERVICES = [
    "feature_internet", ]

a_NEVER_ASSUME_AMENITIES = [
    "feature_jacuzzi", "feature_sauna", "feature_boat_slip",
    "feature_ev_charger", "feature_pool_table", "feature_pool", "feature_outdoor_shower",
]
a_NEVER_ASSUME_SERVICES = [
    "feature_breakfast", "feature_building_staff",
    "feature_cleaning_before_checkout", "feature_host_checkin", "feature_event_friendly",
]

e_LOCATION_DISTANCES = {
    "location_campus": 5,
    "location_hospital": 2,
    "location_restaurants": 2,
    "location_shopping": 5,
    "location_laundromat": 1,
    "location_walking": 2,
    "location_recreation_center": 1,
    "location_museums": 15,
    "location_zoo": 15,
    "location_library": 5,
    "location_playground": 1,
    "location_parks": 2,
    "location_pool": 0.125,
    "location_swimming": 1,
    "location_boating": 15,
    "location_fishing": 5,
    "location_skiing": 20,
    "location_golf": 1,
    "location_tennis": 1,
    "location_hiking": 5,
    "location_cycling": 5,
    "location_horseback_riding": 5,
    "location_wildlife_viewing": 10,
    "location_downtown": 10,
    "location_village": 5,
    "location_rural": 50,
    "location_adventure_activities": 20,
    "location_winery_tours": 50,
    "location_theme_parks": 50,
    "location_water_parks": 20,
    "location_resort": 1,
    "location_waterfront": 0.5,
    "location_water_view": 2,
    "location_waterfalls": 0.5,
    "location_mountain_view": 10,
}
a_DISTANCE_LOCATIONS = list(sorted(e_LOCATION_DISTANCES.values()))
e_DISTANCE_LOCATIONS = defaultdict(list)
for s_loc, d_dist in e_LOCATION_DISTANCES.items():
    e_DISTANCE_LOCATIONS[d_dist].append(s_loc)
    

def calc_rev_cap_std(revenue):
    return 0.0227448131 * math.log(revenue) - 0.1830469103

def calc_rev_cap(revenue):
    return 0.0642895779 * math.log(revenue) - 0.5602704788


def calc_rent_cap(price):
    d_log = 89.445 * price ** -0.758
    d_exp = 0.0106 * math.exp(-0.0000003 * price)
    d_ratio = max(0, min(1, ((price - 25000) / 3500000)))
    d_cap = d_log * (1 - d_ratio) + d_exp * d_ratio
    return d_cap

def calc_rent_cap_std(price):
    d_log = 11122 * price ** -1.287
    d_exp = 0.0044 * math.exp(-0.0000002 * price)
    d_ratio = max(0, min(1, ((price - 25000) / 105000)))
    d_cap_std = d_log * (1 - d_ratio) + d_exp * d_ratio
    return d_cap_std
    

def calc_price_cap(d_price, d_low_bound=200000, d_upper_bound=500000):
    if pd.isnull(d_price) or d_price <= 0: return np.nan
    
    d_low = 189.082858355 * d_price ** -.056584788499
    d_high = max(0.01, 0.135 * math.exp(-0.000000213458 * d_price))
    
    if d_price < 80000:
        return 0.318
    elif d_price <= d_low_bound:
        return d_low
    elif d_price >= d_upper_bound:
        return d_high
    else:
        d_bound = d_upper_bound - d_low_bound
        d_combo = (d_low * (1 - ((d_price - d_low_bound) / d_bound))) + (d_high * ((d_price - d_low_bound) / d_bound))
        return d_combo


def calc_rate_occupancy(rate):
    if rate < 105:
        return 0.62508247 * math.exp(-0.00142434 * rate)
    else:
        return 0.54339014 * math.exp(-0.00013257 * rate)


def calc_population_density_occupancy(population_density):
    #return 0.00000106 * population_density + 0.33399950
    return 0.5267


def calc_housing_density_occupancy(housing_density):
    #return 0.00000088 * housing_density + 0.33584849
    return 0.5238


def calc_price_occupancy(d_price):
    if d_price < 150000:
        return max(0.4, 0.057111160151 * math.log(d_price) - 0.1645602)
    elif d_price >= 1800000:
        return 0.839816792370 * d_price ** -0.027695126479
    else:
        return 0.000000032460 * d_price + 0.508720618953


def predict_price_revenue(d_price, d_low_bound=4000000, d_upper_bound=6500000):
    ''' predict a sanity-check revenue using the price of the house '''
    if pd.isnull(d_price) or d_price <= 0: return np.nan
    
    d_low = max(25000, 53223.97 * math.log(d_price) - 613454.54)
    d_high = max(150000, 515381581.28 * d_price ** -0.50958449)
    
    if d_price <= d_low_bound:
        return d_low
    elif d_price >= d_upper_bound:
        return d_high
    else:
        d_bound = d_upper_bound - d_low_bound
        d_combo = (d_low * (1 - ((d_price - d_low_bound) / d_bound))) + (d_high * ((d_price - d_low_bound) / d_bound))
        return d_combo


def predict_price_revenue_median(d_price, d_low_bound=1500000, d_upper_bound=6500000):
    ''' predict a sanity-check revenue using the price of the house '''
    if pd.isnull(d_price) or d_price <= 0: return np.nan
    
    d_low = max(25000, 30990.29 * math.log(d_price) - 341337.14)
    d_high = max(75000, 603920467.108 * d_price ** -0.55296835)
    
    if d_price <= d_low_bound:
        return d_low
    elif d_price >= d_upper_bound:
        return d_high
    else:
        d_bound = d_upper_bound - d_low_bound
        d_combo = (d_low * (1 - ((d_price - d_low_bound) / d_bound))) + (d_high * ((d_price - d_low_bound) / d_bound))
        return d_combo


def predict_price_rent(d_price, d_low_bound=1000000, d_upper_bound=6500000):
    ''' predict a sanity-check rent using the price of the house '''
    if pd.isnull(d_price) or d_price <= 0: return np.nan
    
    d_low = max(500, 1775.6471 * d_price ** -0.00908652)
    return d_low
    #d_high = max(500, 1131.7527 * math.log(d_price) - 886.78086443)
    
    #if d_price <= d_low_bound:
    #    return d_low
    #elif d_price >= d_upper_bound:
    #    return d_high
    #else:
    #    d_bound = d_upper_bound - d_low_bound
    #    d_combo = (d_low * (1 - ((d_price - d_low_bound) / d_bound))) + (d_high * ((d_price - d_low_bound) / d_bound))
    #    return d_combo


def get_days_on_market_price_adjuments(i_days_on_market):
    try:
        return min([1, max([0, ((i_days_on_market - 30) / 365) ** 0.33])])
    except:
        return 0


def calc_rent_to_expected_days(d_rent):
    return round(0.0033 * d_rent + 9.9537)


def calc_percent_diff_to_extra_days(d_diff):
    d_ratio = min(1, max(0, ((d_diff + 0.5) / 1.5)))
    d1 = 8.4268 * d_diff + 18.034
    d2 = 14.017 * math.exp(16 * d_diff)
    return min(3650, round((d1 + d2) / 2) - 16)


def calc_percent_diff_to_percent_extra_days(d_diff, d_days_on_market=None):
    d_ratio = min(1, max(0, ((d_diff + 0.5) / 1.5)))
    d1 = 8.4268 * d_diff + 18.034
    d2 = 14.017 * math.exp(16 * d_diff)
    
    d_percent = round(((d1 + d2) / 2)  / 16, 4)
    if d_days_on_market:
        return round(min(3650, d_percent * d_days_on_market))
    else:
        return d_percent


def suspicious_bed_bath_ratio(i_beds, d_baths):
    if i_beds < 1: i_beds = 1
    if d_baths < 1:
        return min(1, (d_baths / (i_beds - 0.25))) ** 0.5
    elif (i_beds / 2.5) > d_baths and (i_beds - 2.5) > d_baths:
        return min(1, (d_baths / (i_beds - 2.75))) #** 0.5
    #elif d_baths > (i_beds * 2) and d_baths > (i_beds + 2):
     #   return min(1, ((i_beds + 1.75) / d_baths)) ** 0.5
    elif d_baths >= (i_beds * 1.5) and d_baths >= (i_beds + 2):
        return min(1, ((i_beds + 1.75) / d_baths)) ** 1.5
    
    return 1.0


def get_sanity_factors(e_inputs, **kwargs):
    d_max = 1.0
    a_sanity_factors = []
    
    
    if e_inputs.get("no_rentals") in {1, True, "True", "true"}:
        #a_sanity_factors.append(0)
        return 0, 0
    if e_inputs.get("move_home") in {1, True, "True", "true"}:
        #a_sanity_factors.append(0)
        return 0, 0
    if e_inputs.get("shared_ownership") in {1, True, "True", "true"}:
        #a_sanity_factors.append(0)
        return 0, 0
    if e_inputs.get("timeshare") in {1, True, "True", "true"}:
        #a_sanity_factors.append(0)
        return 0, 0
    if e_inputs.get("condemned") in {1, True, "True", "true"}:
        #a_sanity_factors.extend(0.1)
        return 0, 0
    
    if e_inputs.get("price", 0) in {0, None}:
        pass
    elif e_inputs.get("price", 0) < 70000:
        a_sanity_factors.append(0.5)
    elif e_inputs.get("price", 0) < 80000:
        a_sanity_factors.append(0.6)
    elif e_inputs.get("price", 0) < 100000:
        a_sanity_factors.append(0.7)
    elif e_inputs.get("price", 0) < 150000:
        a_sanity_factors.append(0.8)
    elif e_inputs.get("price", 0) < 200000:
        a_sanity_factors.append(0.9)
        
    d_bed_bath_sanity = suspicious_bed_bath_ratio(e_inputs["bedrooms"], e_inputs["bathrooms"])
    #print("d_bed_bath_sanity", d_bed_bath_sanity, "bedrooms", e_inputs["bedrooms"], "bathrooms", e_inputs["bathrooms"])
    
    #if isinstance(d_bed_bath_sanity, (float, int)) and d_bed_bath_sanity < 1.0:
    if isinstance(d_bed_bath_sanity, (float, int)) and d_bed_bath_sanity < 1.0:
        a_sanity_factors.append(d_bed_bath_sanity)
    
    
    if e_inputs.get("days_on_market", 0) > 30:
        #a_sanity_factors.append(0.2 * min([1, max([0, ((e_inputs["days_on_market"] - 30) / 365) ** 0.33])]))
        a_sanity_factors.append(1 - get_days_on_market_price_adjuments(e_inputs["days_on_market"] * 0.2))
    
    if e_inputs.get("manufactured_home") in {1, True, "True", "true"}:
        a_sanity_factors.append(0.5)
        d_max = 0.5
    elif e_inputs.get("starter_home") in {1, True, "True", "true"}:
        a_sanity_factors.append(0.6)
    
    if e_inputs.get("potential_issues") in {1, True, "True", "true"}:
        a_sanity_factors.append(0.75)
        
    if e_inputs.get("insufficient_description"):
        a_sanity_factors.append(1 - (0.5 * e_inputs.get("insufficient_description")))
    if e_inputs.get("fixer_upper") in {1, True, "True", "true"}:
        a_sanity_factors.append(0.4)
        d_max = min(d_max, 0.75)
    if e_inputs.get("property_condition") > 4:
        if e_inputs["property_condition"] >= 8:
            a_sanity_factors.append(0.1)
            d_max = min(d_max, 0.25)
        elif e_inputs["property_condition"] == 7:
            a_sanity_factors.append(0.2)
            d_max = min(d_max, 0.4)
        elif e_inputs["property_condition"] == 6:
            a_sanity_factors.append(0.4)
            d_max = min(d_max, 0.6)
        elif e_inputs["property_condition"] == 5:
            a_sanity_factors.append(0.6)
            d_max = min(d_max, 0.8)
    if e_inputs.get("auction") in {1, True, "True", "true"}:
        a_sanity_factors.append(0.5)
    
    if e_inputs.get("property_type_Estate"):
        a_sanity_factors.append(1.6)
    elif e_inputs.get("property_type_Tiny house"):
        a_sanity_factors.append(0.4)
        d_max = min(d_max, 0.8)
    elif e_inputs.get("property_type_Farmhouse"):
        a_sanity_factors.append(0.65)
    elif e_inputs.get("property_type_Townhouse"):
        a_sanity_factors.append(0.65)
    
    if e_inputs.get("rent_control") in {1, True, "True", "true"}:
        a_sanity_factors.append(0.6)
    if e_inputs.get("feature_hoa") in {1, True, "True", "true"}:
        a_sanity_factors.append(0.9)
    
    if e_inputs.get("location_campus") in {1, True, "True", "true"}:
        a_sanity_factors.append(0.70)
    
    if e_inputs.get("feature_event_friendly"):
        a_sanity_factors.append(1.4)
    if e_inputs.get("feature_pool"):
        a_sanity_factors.append(1.25)
    elif e_inputs.get("feature_jacuzzi"):
        a_sanity_factors.append(1.17)
    elif e_inputs.get("feature_sauna"):
        a_sanity_factors.append(1.16)
    elif e_inputs.get("location_pool"):
        a_sanity_factors.append(1.12)
    if e_inputs.get("location_winery_tours"):
        a_sanity_factors.append(1.32)
    if e_inputs.get("location_water_view"):
        a_sanity_factors.append(1.3)
    elif e_inputs.get("location_waterfront"):
        a_sanity_factors.append(1.22)
    if e_inputs.get("feature_ski_in_ski_out"):
        a_sanity_factors.append(1.25)
    elif e_inputs.get("location_skiing"):
        a_sanity_factors.append(1.2)
    if e_inputs.get("location_mountain_view"):
        a_sanity_factors.append(1.2)
    
    if a_sanity_factors:
        d_sanity_adjustment_avg = np.mean(a_sanity_factors)
        if len(a_sanity_factors) >= 4:
            d_sanity_adjustment_prod = np.product(a_sanity_factors) ** 0.5
        elif len(a_sanity_factors) >= 4:
            d_sanity_adjustment_prod = np.product(a_sanity_factors) ** 0.6
        elif len(a_sanity_factors) >= 3:
            d_sanity_adjustment_prod = np.product(a_sanity_factors) ** 0.7
        elif len(a_sanity_factors) >= 2:
            d_sanity_adjustment_prod = np.product(a_sanity_factors) ** 0.8
        else:
            d_sanity_adjustment_prod = np.product(a_sanity_factors)
        
        if d_sanity_adjustment_prod > 1:
            d_sanity_adjustment_prod = d_sanity_adjustment_prod ** 0.75
        
        if kwargs.get("b_debug"):
            print("\ta_sanity_factors", a_sanity_factors)
            #print("d_sanity_adjustment_avg", d_sanity_adjustment_avg)
            print("\td_sanity_adjustment_prod", d_sanity_adjustment_prod)
    else:
        d_sanity_adjustment_avg, d_sanity_adjustment_prod = 1.0, 1.0
    
    if d_max < 1:
        d_sanity_adjustment_avg = min(d_sanity_adjustment_avg, d_max)
        d_sanity_adjustment_prod = min(d_sanity_adjustment_prod, d_max)
    
    return d_sanity_adjustment_avg, d_sanity_adjustment_prod
