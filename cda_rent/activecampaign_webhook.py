﻿
try:
    from . import aws_ses
except:
    import aws_ses


s_TEMPLATE_PLAIN = """
Hello {s_name},

 
Thank you for the inquiry.  Here is what we came up with for the property in {s_area}:


    {s_area_properties}
    
    
    {s_season}
    
    
    On average the area annual-revenue per short-term rental property is ${d_area_avg_revenue:,}, which is {d_area_vs_mean:0.0%} of the national average.
    
    Our approximation of Optimized revenue would be ${d_predicted_revenue_optimized:,}, assuming you use dynamic pricing to maximize revenue and reduce vacancy.
    The predicted potential revenue if this property were open to guests year-round and advertised properly is ${d_predicted_revenue:,}.

    
    This compares to a predicted ${d_long_term_rent:,} revenue if the property were to be leased on a year-to-year basis.
    

Any feedback is greatly appreciated and let us know if you have any questions or other properties you would like revenues for. We hope to have the fully automated site up this week, we will keep you updated.
"""


s_TEMPLATE_HTML = """
<p>Hello {s_name},<br><br>
 
Thank you for the inquiry.  Here is what we came up with for the property in {s_area}:</p>
<UL>

    <LI>{s_area_properties}</LI>
    
    <LI>{s_season}</LI>
    
    <LI>On average the area annual-revenue per short-term rental property is ${d_area_avg_revenue:,}, which is {d_area_vs_mean:0.0%} of the national average.</LI>
    
    <LI>
    Our approximation of Optimized revenue would be <b>${d_predicted_revenue_optimized:,}</b>, assuming you use dynamic pricing to maximize revenue and reduce vacancy.<br>
    The predicted potential revenue if this property were open to guests year-round and advertised properly is ${d_predicted_revenue:,}.</LI>
    
    <LI>This compares to a predicted ${d_long_term_rent:,} revenue if the property were to be leased on a year-to-year basis.</LI>
</UL>
<P>
Any feedback is greatly appreciated and let us know if you have any questions or other properties you would like revenues for. We hope to have the fully automated site up this week, we will keep you updated.
</P>
"""


e_MONTHS = {
    1: "January",
    2: "February",
    3: "March",
    4: "April",
    5: "May",
    6: "June",
    7: "July",
    8: "August",
    9: "September",
    10: "October",
    11: "November",
    12: "December",
}

e_ACTIVE_CAMPAIGN_CONVERSIONS = {
    "bedrooms": {
        "1 Bedroom": 1,
        "2 Bedrooms": 2,
        "3 Bedrooms": 3,
        "4 Bedrooms": 4,
        "5 Bedrooms": 5,
        "6 Bedrooms": 6,
        "7 Bedrooms": 7,
        "8 Bedrooms": 8,
        "9 Bedrooms": 9,
        "10 Bedrooms": 10,
        "10+ Bedrooms (we will assume 12)": 12,
    },
    "bathrooms": {
        "0.5 Bath": 0.5,
        "1 Bath": 1,
        "1.5 Baths": 1.5,
        "2 Baths": 2,
        "2.5 Baths": 2.5,
        "3 Baths": 3,
        "3.5 Baths": 3.5,
        "4 Baths": 4,
        "4.5 Baths": 4.5,
        "5 Baths": 5,
        "5.5 Baths": 5.5,
        "6 Baths": 6,
        "6.5 Baths": 6.5,
        "7 Baths": 7,
        "8 Baths": 8,
        "9 Baths": 9,
        "10 Baths": 10,
        "10+ Baths": 12,
    },
    "features": {
        'Elevator': "elevator",
        'Pool': "feature_pool",
        'Community Pool': "location_pool",
        'Jacuzzi': "jacuzzi",
        'Patio/Balcony': "patio_or_balcony",
        'Fireplace': "fireplace",
        'Garden/Backyard': "garden_or_backyard",
        'Air Conditioning': "ac",
        'Garage': "garage",
        'Private Entrance': "private_entrance"
    },
    "locations": {
        'Waterfront': "waterfront",
        'Water View': "water_view",
        'Mountain View': "mountain_view",
        'Wildlife Viewing': "wildlife_viewing",
        'Skiing': "skiing",
        'Downtown (Nearby)': "location_downtown",
        'Restaurants (Nearby)': "location_restaurants",
        'Shopping (Nearby)': "location_shopping",
        'Hospital/Medical (Nearby)': "location_hospital",
        'Swimming/Beach (Nearby)': "location_swimming",
        'Water/Adventure Parks (Nearby)': "location_water_parks",
    },                                             
}

def convert_to_input(e_data):
    e_input = {
        "property_type": e_data.get("contact[fields][residence_type]", "House"),
        "bedrooms": e_ACTIVE_CAMPAIGN_CONVERSIONS["bedrooms"].get(e_data.get("contact[fields][bedrooms]", 3), 3),
        "bathrooms": e_ACTIVE_CAMPAIGN_CONVERSIONS["bathrooms"].get(e_data.get("contact[fields][bathroom]", 2), 2),
        "address": ", ".join([x for x in [
            e_data.get("contact[fields][property_address]", ""),
            e_data.get("contact[fields][property_city]", ""),
            e_data.get("contact[fields][property_state]", ""),
            e_data.get("contact[fields][property_zip_code]", ""),
        ] if x]),
        "address": e_data.get("contact[fields][property_address]", ""),
        "city": e_data.get("contact[fields][property_city]", ""),
        "state": e_data.get("contact[fields][property_state]", ""),
        "zip_code": e_data.get("contact[fields][property_zip_code]", ""),
        "price": e_data.get("contact[fields][home_value_]", 0),
        #"living_area": e_data.get("contact[fields][home_value_]", 0),
        "lot_size": e_data.get("contact[fields][lot_size_acres]", 0),
    }
    
    for feature in e_data.get("contact[fields][property_information]", []):
        e_input[e_ACTIVE_CAMPAIGN_CONVERSIONS["features"].get(feature, feature)] = True
    
    for location in e_data.get("contact[fields][location_information]", []):
        e_input[e_ACTIVE_CAMPAIGN_CONVERSIONS["locations"].get(location, location)] = True
    
    return e_input


def results_into_template(e_data, e_address, e_predictions, e_input_prepped, e_monthly_data, e_local_data, ** kwargs):
    s_name = " ".join((e_data.get("contact[first_name]", ""), e_data.get("contact[last_name]", ""))).strip()

    d_area_seasonality = e_predictions.get("area_seasonality", 0)
    s_peak_month = e_MONTHS.get(e_predictions.get("area_season_peak_month", 0), "")
    i_season_months = round(e_predictions.get("area_season_length", 0))
    
    if d_area_seasonality < (1/12) or i_season_months < 1:
        s_season = f"Your area is not seasonal, but experiences average rental revenues throughout the year."
    else:
        s_season = f"Your area is {d_area_seasonality:0.0%} seasonal, with a normal season that's {i_season_months} months long. The best month for revenue is usually {s_peak_month}."
    
    s_address = e_address.get("address_components", {}).get("formatted_address", "")
    if not s_address:
        s_address = ", ".join([x for x in [e_address.get("lat", ""), e_address.get("long", "")] if x])
    
    if e_address.get("address_components", {}).get("locality", []):
        s_area = e_address["address_components"]["locality"][0]
    elif e_address.get("address_components", {}).get("administrative_area_level_2", []):
        s_area = e_address["address_components"]["administrative_area_level_2"][0]
    elif e_address.get("address_components", {}).get("political", []):
        s_area = e_address["address_components"]["political"][0]
    elif e_address.get("address_components", {}).get("administrative_area_level_1", []):
        s_area = e_address["address_components"]["administrative_area_level_1"][0]
    elif e_address.get("address_components", {}).get("state", []):
        s_area = e_address["address_components"]["state"][0]
    else:
        s_area = s_address
    
    
    i_area_properties, s_area_scale = 0, ""
    for i in reversed(range(-1, 3 + 1)):
        if e_input_prepped.get(f"{i}_area_properties", 0) > 0:
            i_area_properties = e_input_prepped[f"{i}_area_properties"]
            s_area_scale = e_input_prepped[f"{i}_area_scale"]
            break
    if i_area_properties:
        s_area_properties = f"We've found {i_area_properties} properties in the surrounding {s_area_scale} area that have been active for a least 6 months in the last several years."
    else:
        s_area_properties = f"We don't have data on any existing short-term rental properties in your local area so we're using the national averages based on our dataset of 600k+ properties across the united states."
    
    s_subject = f"Predicted Short-Term Rent for {s_address}"
    
    e_pieces = {
        "s_name": s_name,
        "s_area": s_area,
        "s_area_properties": s_area_properties,
        "s_season": s_season,
        "d_area_vs_mean": round(e_predictions.get("area_vs_mean", 1), 2),
        "d_area_avg_revenue": round(e_predictions.get("area_revenue", 0)),
        "d_predicted_revenue": round(e_predictions.get("predicted_revenue", 0)),
        "d_predicted_occupancy": round(e_predictions.get("predicted_occupancy", 0), 2),
        "d_predicted_revenue_optimized": round(e_predictions.get("predicted_revenue_optimized", 0)),
        "d_long_term_rent": round(e_predictions.get("predicted_rent_annual", 0)),
    }
    s_plain = s_TEMPLATE_PLAIN.format(**e_pieces)
    s_html = s_TEMPLATE_HTML.format(**e_pieces)
    
    return s_subject, s_plain, s_html