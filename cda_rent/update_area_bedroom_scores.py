﻿import sys, os, re, math
from datetime import datetime, timedelta
from decimal import Decimal
from collections import defaultdict
import pandas as pd
import numpy as np

try:
    from . import utils, db_tables, scoring
except:
    import utils, db_tables, scoring

o_DB = None

def load_data(**kwargs):
    global o_DB
    if o_DB is None:
        if db_tables is None: db_tables.load_db(**kwargs)
        o_DB = db_tables.o_DB
load_data()

i_DB_UPDATE_BATCH_SIZE = max(1, db_tables.e_CONFIG.get("score_update_batch_size", 500))
i_SCORE_WEIGHT_RECENT_DAYS = max(1, db_tables.e_CONFIG.get("score_weight_recent_days", 1))

e_STATE_INFOS = {}
a_SCORE_COLS = [
    'score_overall',
    'score_relative',
    'score_absolute',
    'score_area',
    'score_relative_roi',
    'score_relative_cap',
    'score_relative_expected',
    'score_absolute_revenue',
    'score_absolute_net',
    'score_absolute_rate',
    'score_absolute_occupancy',
    'score_area_cap',
    'score_area_rate',
    'score_area_penetration',
    'score_area_opportunity',
    'score_area_size',
    'score_area_seasonality',
    'score_area_optimization',
    'estimated_costs',
    'estimated_net',
    'estimated_roi',
]


def update_all(b_update_states=True, b_update_cities=True, b_update_zipcodes=True, **kwargs):
    print("Will update these score columns:")
    for col in a_SCORE_COLS:
        print(f"\t{col}")
    print("")

    df_states = o_DB.get_table_as_df(db_tables.state_bedrooms) #.set_index("state")
    
    for col in df_states.columns:
        if col.startswith("score_") and col not in a_SCORE_COLS:
            print(f"Missing score col from updates: {col}")
            a_SCORE_COLS.append(col)
    
    df_states["state"] = df_states["state"].str.lower()
    for col in df_states.columns:
        if col in {"country", "state", "state_name", "dataset_quarter"} or col.lower().startswith(("feature_", "location_")): continue
        df_states[col] = df_states[col].apply(utils.to_float)
    
    global e_STATE_INFOS
    e_STATE_INFOS = df_states.set_index(["state", "bedrooms"]).to_dict(orient="index")
    
    if b_update_states:
        e_state_scores = {}
        for row in df_states.to_dict(orient="records"):
            print("Scoring state:", row["state"])
            e_state_scores[(row["state"], row["bedrooms"])] = scoring.score_area(row, s_type="state", state_info=row, b_debug=True)
        
        a_state_score_updates = []
        for (s_state, i_bedrooms), e_sc in e_state_scores.items():
            a_state_score_updates.append(tuple([e_sc.get(k, None) for k in a_SCORE_COLS] + [s_state.upper(), int(i_bedrooms)]))
        
        i_updated_rows = o_DB.update_batch(db_tables.state_bedrooms, a_SCORE_COLS, ["state", "bedrooms"], a_state_score_updates)
        print(f"{i_updated_rows:,} states have been updated\n")
    
    if b_update_cities:
        update_cities()
    
    if b_update_zipcodes:
        update_zipcodes()
    

def update_cities():
    df_cities = o_DB.get_table_as_df(db_tables.city_bedrooms) #.set_index("state")
    for col in df_cities.columns:
        if col in {"country", "state", "city", "zipcode", "alt_city_name", "place_type", "dataset_quarter"} or col.lower().startswith(("feature_", "location_")): continue
        df_cities[col] = df_cities[col].apply(utils.to_float)
    
    e_city_scores = {}
    for row in df_cities.to_dict(orient="records"):
        if not row.get("state") or not row.get("bedrooms") or row["state"] == "_all_": continue
        e_st_row = e_STATE_INFOS.get((row["state"].lower(), int(row["bedrooms"])))
        if not e_st_row: continue
        
        e_score = scoring.score_area(row, s_type="city", state_info=e_st_row)
        e_city_scores[(row["city"], row["state"], row["bedrooms"])] = e_score
        
    print(f"{len(e_city_scores):,} cities/bedrooms have been scored")
    
    a_city_score_updates = []
    i_batch, i_updated_rows = 0, 0
    for (s_city, s_state, i_bedrooms), e_sc in e_city_scores.items():
        a_city_score_updates.append(tuple([e_sc.get(k, None) for k in a_SCORE_COLS] + [s_city, s_state.upper(), int(i_bedrooms)]))
        if len(a_city_score_updates) >= i_DB_UPDATE_BATCH_SIZE:
            i_batch += 1
            i_batch_updated_rows = o_DB.update_batch(db_tables.city_bedrooms, a_SCORE_COLS, ["city", "state", "bedrooms"], a_city_score_updates)
            i_updated_rows += i_batch_updated_rows
            a_city_score_updates = []
            print(f"\t{i_batch_updated_rows:,} cities/bedrooms have been updated in batch {i_batch}\n")
        
    if len(a_city_score_updates) >= i_DB_UPDATE_BATCH_SIZE:
        i_batch += 1
        i_batch_updated_rows = o_DB.update_batch(db_tables.city_bedrooms, a_SCORE_COLS, ["city", "state", "bedrooms"], a_city_score_updates)
        i_updated_rows += i_batch_updated_rows
        print(f"\t{i_batch_updated_rows:,} cities/bedrooms have been updated in batch {i_batch}\n")
    
    print(f"{i_updated_rows:,} cities have been updated\n")


def update_zipcodes():
    df_zipcodes = o_DB.get_table_as_df(db_tables.zipcode_bedrooms) #.set_index("state")
    for col in df_zipcodes.columns:
        if col in {"country", "state", "city", "zipcode", "alt_city_name", "place_type", "dataset_quarter"} or col.lower().startswith(("feature_", "location_")): continue
        df_zipcodes[col] = df_zipcodes[col].apply(utils.to_float)
    
    e_zip_scores = {}
    for row in df_zipcodes.to_dict(orient="records"):
        if not row.get("state") or not row.get("bedrooms") or row["state"] == "_all_": continue
        e_st_row = e_STATE_INFOS.get((row["state"].lower(), int(row["bedrooms"])))
        if not e_st_row: continue
    
        e_score = scoring.score_area(row, s_type="zip", state_info=e_st_row)
        e_zip_scores[(row["zipcode"], row["state"], row["bedrooms"])] = e_score
        
    print(f"{len(e_zip_scores):,} zipcodes/bedrooms have been scored")
    
    a_zipcode_score_updates = []
    i_batch, i_updated_rows = 0, 0
    for (s_zipcode, s_state, i_bedrooms), e_sc in e_zip_scores.items():
        a_zipcode_score_updates.append(tuple([e_sc.get(k, None) for k in a_SCORE_COLS] + [s_zipcode, s_state.upper(), int(i_bedrooms)]))
        if len(a_zipcode_score_updates) >= i_DB_UPDATE_BATCH_SIZE:
            i_batch += 1
            i_batch_updated_rows = o_DB.update_batch(db_tables.zipcode_bedrooms, a_SCORE_COLS, ["zipcode", "state", "bedrooms"], a_zipcode_score_updates)
            i_updated_rows += i_batch_updated_rows
            a_zipcode_score_updates = []
            print(f"\t{i_batch_updated_rows:,} zipcodes/bedrooms have been updated in batch {i_batch}\n")
        
    if len(a_zipcode_score_updates) >= i_DB_UPDATE_BATCH_SIZE:
        i_batch += 1
        i_batch_updated_rows = o_DB.update_batch(db_tables.zipcode_bedrooms, a_SCORE_COLS, ["zipcode", "state", "bedrooms"], a_zipcode_score_updates)
        i_updated_rows += i_batch_updated_rows
        print(f"\t{i_batch_updated_rows:,} zipcodes/bedrooms have been updated in batch {i_batch}\n")
    

if __name__ == "__main__":
    args, kwargs = utils.parse_command_line_args(sys.argv)
    print("Running update_area_bedroom_scores.py from shell")
    
    c_trues = {True, "yes", "Yes", 1}
    
    if kwargs.get("recent_only", True):
        try:
            dt_last_score_change = o_DB.execute(f"SELECT MAX(updated) FROM {db_tables.score_weights};")[0][0]
            if dt_last_score_change < (datetime.utcnow() - timedelta(days=i_SCORE_WEIGHT_RECENT_DAYS)):
                print(f"Score weights haven't changed in the last {i_SCORE_WEIGHT_RECENT_DAYS} day(s). Skipping update")
                quit()
        except Exception as e:
            print("Couldn't check score weights for recent changes")
            pass
    
    if kwargs.get("b_update_states") in c_trues or kwargs.get("update_states") in c_trues or kwargs.get("states") in c_trues:
        b_update_states = True
        print("Will update state scores")
    else:
        b_update_states = False
    
    if kwargs.get("b_update_cities") in c_trues or kwargs.get("update_cities") in c_trues or kwargs.get("cities") in c_trues:
        b_update_cities = True
        print("Will update city scores")
    else:
        b_update_cities = False
    
    if kwargs.get("b_update_zipcodes") in c_trues in c_trues or kwargs.get("update_zipcodes") in c_trues or kwargs.get("zipcodes") in c_trues:
        b_update_zipcodes = True
        print("Will update zipcode scores")
    elif kwargs.get("b_update_zips") in c_trues or kwargs.get("update_zips") in c_trues or kwargs.get("zips") in c_trues:
        b_update_zipcodes = True
        print("Will update zipcode scores")
    else:
        b_update_zipcodes = False
    
    if b_update_states or b_update_cities or b_update_zipcodes:
        update_all(
            b_update_states=b_update_states,
            b_update_cities=b_update_cities,
            b_update_zipcodes=b_update_zipcodes,
        )
    else:
        print("No updates selected. Try using kwargs --update_states:True, --update_cities:True, or --update_zipcodes:True")
    