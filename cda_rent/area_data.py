﻿import os, sys, re
from time import time, perf_counter
from datetime import datetime, timedelta
from math import cos, radians
from decimal import Decimal
import threading
import pandas as pd
import numpy as np

try:
    from . import utils, db_tables, us_states, location_analysis
except:
    import utils, db_tables, us_states, location_analysis


o_DB = None

e_NATIONAL_MONTH_OCCUPANCIES = {}
e_NATIONAL_MONTH_RATES = {}
e_STATE_MONTH_OCCUPANCIES = {}
e_STATE_MONTH_RATES = {}

e_ZIP_AGGS = None

d_INITIAL_RANGE_IN_DEGREES = db_tables.e_CONFIG.get("search_range_area_initial_in_degrees", 0.5) # 69 miles. How far to search from latitude and longitude on first attempt
d_MAX_RANGE_IN_DEGREES = db_tables.e_CONFIG.get("search_range_area_max_in_degrees", 8) # how far to expand search before giving up
d_EXPAND_RANGE = db_tables.e_CONFIG.get("search_range_area_expansion", 2) # how much to expand search each time if insufficient closest options are found
d_DEG_TO_M = 69.172
d_DEFAULT_ZIPCODES_MAX_DISTANCE = 50

a_OCCUPANCY_COLS = [
    'occupancy_month_1',
    'occupancy_month_2',
    'occupancy_month_3',
    'occupancy_month_4',
    'occupancy_month_5',
    'occupancy_month_6',
    'occupancy_month_7',
    'occupancy_month_8',
    'occupancy_month_9',
    'occupancy_month_10',
    'occupancy_month_11',
    'occupancy_month_12',]

a_RATE_COLS = [
    'rate_month_1',
    'rate_month_2',
    'rate_month_3',
    'rate_month_4',
    'rate_month_5',
    'rate_month_6',
    'rate_month_7',
    'rate_month_8',
    'rate_month_9',
    'rate_month_10',
    'rate_month_11',
    'rate_month_12',]

e_NATIONAL_INFO = {}
e_STATE_INFO_CACHE = {}
e_CITY_INFO_CACHE = {}
e_ZIPCODE_INFO_CACHE = {}
i_MAX_ZIP_CACHE, i_MAX_CITY_CACHE = db_tables.e_CONFIG.get("area_max_zip_cached", 500), db_tables.e_CONFIG.get("area_max_city_cached", 500)

dt_NAT_LOADED = datetime.utcnow()
dt_NAT_RELOAD = dt_NAT_LOADED + timedelta(hours=db_tables.e_CONFIG.get("area_national_reload_hours", 24), minutes=db_tables.e_CONFIG.get("area_national_reload_minutes", 0))


e_MARKET_GPS = {}
e_MARKET_INFO_ROWS = {}
a_MARKET_COLS = []
a_MARKET_SUMMARY_COLS = []
a_MARKET_AVERAGE_COLS = []
a_MARKET_ARCHIVE_COLS = []


#@utils.timer
def load_data(**kwargs):
    print("Loading Average Occupancy and Nightly Rates")
    n_start = time()
    
    global o_DB
    if o_DB is None:
        if db_tables is None: db_tables.load_db(**kwargs)
        o_DB = db_tables.o_DB
    
    e_month_averages = get_average_occupancies_and_rates()
    global e_NATIONAL_MONTH_OCCUPANCIES, e_NATIONAL_MONTH_RATES, e_STATE_MONTH_OCCUPANCIES, e_STATE_MONTH_RATES
    e_NATIONAL_MONTH_OCCUPANCIES = e_month_averages["national_occupancies"]
    e_NATIONAL_MONTH_RATES = e_month_averages["national_rates"]
    e_STATE_MONTH_OCCUPANCIES = e_month_averages["state_occupancies"]
    e_STATE_MONTH_RATES = e_month_averages["state_rates"]
    
    global e_MARKET_GPS
    a_market_coordinates = o_DB.execute(r"""SELECT id, latitude, longitude from `revr`.`market`""")
    e_MARKET_GPS = {market_id: (float(latitude), float(longitude)) for market_id, latitude, longitude in a_market_coordinates if not pd.isnull(latitude) and not pd.isnull(longitude)}
    
    load_national(**kwargs)
    print("\tLoading Average Occupancy and Nightly Rates took:", utils.secondsToStr(time() - n_start))


#@utils.timer
def load_national(**kwargs):
    global e_NATIONAL_INFO
    a_national_row = o_DB.execute(r"SELECT * FROM {} WHERE state = '_all_'  AND dataset_quarter = '{}';".format(db_tables.state_info, db_tables.e_CONFIG.get("latest_dataset_quarter")))
    if a_national_row and a_national_row[0]:
        if a_national_row and (a_national_row[0] is None or db_tables.a_STATE_INFO_COLS is None or len(a_national_row[0]) != len(db_tables.a_STATE_INFO_COLS)):
            db_tables.load_db(reload=True)
    
        e_NATIONAL_INFO = {
            k: float(v) if isinstance(v, Decimal) else v
            for k, v in zip(db_tables.a_STATE_INFO_COLS, a_national_row[0])
            if k not in {"type_rev_diff", "type_revenue", "rent_control", "auction", "fixer_upper", "condemned", "airbnb", "homeaway"} 
        }
    else:
        raise Exception("couldn't load national data")
    
    global dt_NAT_LOADED, dt_NAT_RELOAD
    dt_NAT_LOADED = datetime.utcnow()
    dt_NAT_RELOAD = dt_NAT_LOADED + timedelta(hours=db_tables.e_CONFIG.get("area_national_reload_hours", 24), minutes=db_tables.e_CONFIG.get("area_national_reload_minutes", 0))  


def market_id_to_name(market_id, **kwargs):
    if not market_id or market_id in {"undefined", "null", "None", "nan"}: return None, None
    
    if isinstance(market_id, str):
        market_id = int(market_id)
    if market_id in e_MARKET_INFO_ROWS:
        return e_MARKET_INFO_ROWS[market_id]
    
    if o_DB is None:
        load_data()
    
    t_market_row = o_DB.search_row("revr.market", {"id": market_id}, i_max_rows=1)
    if not t_market_row: return None, None
    
    global a_MARKET_COLS
    if not a_MARKET_COLS:
        a_MARKET_COLS = o_DB.get_table_cols("market", s_schema="revr")
    
    e_market = dict(zip(a_MARKET_COLS, t_market_row))
    
    if e_market.get("type") == "city":
        s_table = "city_info_new"
        e_loc = {"state": e_market["state"], "city": e_market["location"]}
        a_cols = db_tables.a_CITY_INFO_COLS
    elif e_market.get("type") == "zipcode":
        s_table = "zipcode_info_new"
        e_loc = {"state": e_market["state"], "zipcode": e_market["location"]}
        a_cols = db_tables.a_ZIP_INFO_COLS
    elif e_market.get("type") == "state":
        s_table = "state_info_new"
        e_loc = {"state": e_market["state"]}
        a_cols = db_tables.a_STATE_INFO_COLS
    elif e_market.get("type") == "county":
        s_table = "county_info_new"
        e_loc = {"state": e_market["state"], "county": e_market["location"]}
        a_cols = []
    else:
        s_table = None
        e_loc = None
        a_cols = []
    
    #s_dataset_quarter = e_CONFIG["latest_dataset_quarter"]
    #if s_table and e_loc and a_cols:
    #    a_info_rows = o_DB.search_row(s_table, e_loc)
    #    if a_info_rows:
    #        a_info_rows.sort(key=itemgetter(a_cols.index("dataset_quarter")))
    #        s_dataset_quarter = a_info_rows[-1][a_cols.index("dataset_quarter")]
    #e_loc["dataset_quarter"] = s_dataset_quarter
    
    e_MARKET_INFO_ROWS[market_id] = (s_table, e_loc)
    return s_table, e_loc


def get_market_id_info(market_id, **kwargs):
    s_table, e_loc = market_id_to_name(market_id)
    if not s_table or not e_loc: return {}
    
    s_cond_cols = f" AND ".join([f'{t[0]} = "{t[1]}"' for t in e_loc.items()])
    a_info_rows = o_DB.execute(f"""SELECT * from {s_table} WHERE {s_cond_cols} ORDER BY dataset_quarter DESC LIMIT 1;""" )
    if not a_info_rows: return {}
    
    if s_table == "city_info_new":
        a_cols = db_tables.a_CITY_INFO_COLS
        if len(a_info_rows[0]) != len(a_cols):
            db_tables.load_db(reload=True)
            a_cols = db_tables.a_CITY_INFO_COLS
    elif s_table == "zipcode_info_new":
        a_cols = db_tables.a_ZIP_INFO_COLS
        if len(a_info_rows[0]) != len(a_cols):
            db_tables.load_db(reload=True)
            a_cols = db_tables.a_ZIP_INFO_COLS
    elif s_table == "state_info_new":
        a_cols = db_tables.a_STATE_INFO_COLS
        if len(a_info_rows[0]) != len(a_cols):
            db_tables.load_db(reload=True)
            a_cols = db_tables.a_STATE_INFO_COLS
    else:
        a_cols = []
    
    return dict(zip(a_cols, a_info_rows[0]))


def get_market_id_summary(market_id, **kwargs):
    #s_table, e_loc = market_id_to_name(market_id)
    #if not s_table or not e_loc: return {}
    
    if o_DB is None:
        load_data()
    
    global a_MARKET_SUMMARY_COLS
    if not a_MARKET_SUMMARY_COLS:
        a_MARKET_SUMMARY_COLS = o_DB.get_table_cols("current_market_summaries", s_schema="revr")
    
    a_summary_rows = o_DB.execute(f"""SELECT * from `revr`.`current_market_summaries` WHERE market_id = '{market_id}';""" )
    if not a_summary_rows: return {}
    
    if len( a_summary_rows[0]) != len(a_MARKET_SUMMARY_COLS):
        a_MARKET_SUMMARY_COLS = o_DB.get_table_cols("current_market_summaries", s_schema="revr")
    
    return dict(zip(a_MARKET_SUMMARY_COLS, a_summary_rows[0]))
    

def get_market_id_average(market_id, **kwargs):
    if o_DB is None:
        load_data()
    
    global a_MARKET_AVERAGE_COLS
    if not a_MARKET_AVERAGE_COLS:
        a_MARKET_AVERAGE_COLS = o_DB.get_table_cols("average_market_summaries", s_schema="revr")
    
    a_summary_rows = o_DB.execute(f"""SELECT * from `revr`.`average_market_summaries` WHERE market_id = '{market_id}';""" )
    if not a_summary_rows: return {}
    
    if len( a_summary_rows[0]) != len(a_MARKET_AVERAGE_COLS):
        a_MARKET_AVERAGE_COLS = o_DB.get_table_cols("average_market_summaries", s_schema="revr")
    
    return dict(zip(a_MARKET_AVERAGE_COLS, a_summary_rows[0]))
    
    

#@utils.timer
def get_average_occupancies_and_rates(df_zipcodes=None, **kwargs):
    if df_zipcodes is None:
        if o_DB is None: load_data()
        df_zipcodes = o_DB.get_table_as_df(db_tables.zipcode_info, where=r"dataset_quarter = '{}'".format(db_tables.e_CONFIG.get("latest_dataset_quarter")))
    
    e_national_month_occupancies = {
        k: float(v) if isinstance(v, Decimal) else v
        for k, v in dict(df_zipcodes[a_OCCUPANCY_COLS].multiply(df_zipcodes["area_properties"], axis="index").sum(axis=0) / df_zipcodes["area_properties"].sum()).items()}
    e_national_month_rates = {
        k: float(v) if isinstance(v, Decimal) else v
        for k, v in dict(df_zipcodes[a_RATE_COLS].multiply(df_zipcodes["area_properties"], axis="index").sum(axis=0) / df_zipcodes["area_properties"].sum()).items()}
    
    e_state_month_occupancies = {}
    e_state_month_rates = {}
    for s_st, df_st in df_zipcodes.groupby("state"):
        e_state_month_occupancies[s_st] = dict(df_st[a_OCCUPANCY_COLS].multiply(df_st["area_properties"], axis="index").sum(axis=0) / df_st["area_properties"].sum())
        e_state_month_rates[s_st] = dict(df_st[a_RATE_COLS].multiply(df_st["area_properties"], axis="index").sum(axis=0).astype(float) / df_st["area_properties"].sum().astype(float))
    
    return {
        "national_occupancies": e_national_month_occupancies,
        "national_rates": e_national_month_rates,
        "state_occupancies": e_state_month_occupancies,
        "state_rates": e_state_month_rates,
    }


#@utils.timer
def get_state(s_state, e_locale_results=None, **kwargs):
    if not s_state: return {}
    if not e_NATIONAL_MONTH_OCCUPANCIES or o_DB is None: load_data()
    
    s_state = us_states.e_STATE_ABBREV.get(s_state.lower(), s_state)
    
    t_cached = e_STATE_INFO_CACHE.get(s_state)
    if t_cached:
        e_state_cache, dt_state_cache_loaded, dt_state_cache_reload = t_cached
        if e_state_cache and dt_state_cache_loaded and dt_state_cache_reload and dt_state_cache_reload > dt_state_cache_loaded:
            if isinstance(e_locale_results, dict):
                e_locale_results["state_info"] = e_state_cache
            return e_state_cache
    
    if isinstance(e_locale_results, dict):
        o_db = db_tables.get_connection()
    else:
        o_db = o_DB
    
    dataset_quarter = kwargs.get("dataset_quarter", db_tables.e_CONFIG.get("latest_dataset_quarter"))
    
    t_matched_row = o_db.search_row(
        db_tables.state_info,
        {
            "state": s_state,
            "dataset_quarter": dataset_quarter
        },
        i_max_rows=1)
    if not t_matched_row:
        if isinstance(e_locale_results, dict):
            o_db.close()
            o_db = None
            e_locale_results["state_info"] = {}
        return {}
    
    if len(t_matched_row) != len(db_tables.a_STATE_INFO_COLS):
        db_tables.load_db(reload=True)
    
    e_state_cache = {
        k: v if not isinstance(v, Decimal) else float(v)
        for k, v in zip(db_tables.a_STATE_INFO_COLS, t_matched_row)}
    
    dt_state_cache_loaded = datetime.utcnow()
    dt_state_cache_reload = dt_state_cache_loaded + timedelta(hours=db_tables.e_CONFIG.get("area_state_reload_hours", 24), minutes=db_tables.e_CONFIG.get("area_state_reload_minutes", 0))
    e_STATE_INFO_CACHE[s_state] = (e_state_cache, dt_state_cache_loaded, dt_state_cache_reload)
    
    if isinstance(e_locale_results, dict):
        o_db.close()
        o_db = None
        e_locale_results["state_info"] = e_state_cache
    
    return e_state_cache


def prune_city_cache():
    dt_now = datetime.utcnow()
    if len(e_CITY_INFO_CACHE) > i_MAX_CITY_CACHE:
        for t_loc, (e_city_cache, dt_city_cache_loaded, dt_city_cache_reload) in list(e_CITY_INFO_CACHE.items()):
            if dt_city_cache_reload < dt_city_cache_loaded:
                del e_CITY_INFO_CACHE[t_loc]
            
        if len(e_CITY_INFO_CACHE) > i_MAX_CITY_CACHE + 100:
            a_oldest = sorted(list(e_CITY_INFO_CACHE.items()), key=lambda t: t[1][1]) # sort from oldest to newest cached
            for t_loc, (e_city_cache, dt_city_cache_loaded, dt_city_cache_reload) in a_oldest[:i_MAX_CITY_CACHE]:
                del e_CITY_INFO_CACHE[t_loc]

#@utils.timer
def get_city(s_city, s_state, lat=None, long=None, e_locale_results=None, **kwargs):
    if not e_NATIONAL_MONTH_OCCUPANCIES or o_DB is None: load_data()
    s_state = us_states.e_STATE_ABBREV.get(s_state.lower(), s_state)
    
    if e_CITY_INFO_CACHE: prune_city_cache()
    
    t_cached = e_CITY_INFO_CACHE.get((s_state, s_city))
    if t_cached:
        e_city_cache, dt_city_cache_loaded, dt_city_cache_reload = t_cached
        if e_city_cache and dt_city_cache_loaded and dt_city_cache_reload and dt_city_cache_reload > dt_city_cache_loaded:
            if isinstance(e_locale_results, dict):
                e_locale_results["city_info"] = e_city_cache
            return e_city_cache
    
    if isinstance(e_locale_results, dict):
        o_db = db_tables.get_connection()
    else:
        o_db = o_DB
    
    dataset_quarter = kwargs.get("dataset_quarter", db_tables.e_CONFIG.get("latest_dataset_quarter"))
    a_matched_rows = o_db.search_row(
        db_tables.city_info,
        {
            "state":s_state,
            "city": s_city,
            "dataset_quarter": dataset_quarter,
        })
    
    if not a_matched_rows:
        a_matched_rows = o_db.search_row(
            db_tables.city_info,
            {
                "state": s_state,
                "alt_city_name": s_city,
                "dataset_quarter": dataset_quarter,
            }
        )
    
    if not a_matched_rows or not a_matched_rows[0]:
        if isinstance(e_locale_results, dict):
            o_db.close()
            o_db = None
            e_locale_results["city_info"] = {}
        return {}
    
    if a_matched_rows and len(a_matched_rows[0]) != len(db_tables.a_CITY_INFO_COLS):
        db_tables.load_db(reload=True)
    
    a_matched_rows = [
        {
            k: v if not isinstance(v, Decimal) else float(v)
            for k, v in zip(db_tables.a_CITY_INFO_COLS, t_match)}
        for t_match in a_matched_rows]
    
    if len(a_matched_rows) == 1:
        e_match = a_matched_rows[0]
        dt_city_cache_loaded = datetime.utcnow()
        dt_city_cache_reload = dt_city_cache_loaded + timedelta(hours=db_tables.e_CONFIG.get("area_city_reload_hours", 3), minutes=db_tables.e_CONFIG.get("area_city_reload_minutes", 0))
        e_CITY_INFO_CACHE[(s_state, s_city)] = (e_match, dt_city_cache_loaded, dt_city_cache_reload)
    
    elif len(a_matched_rows) > 1 and lat and long:
        e_match = min(a_matched_rows, key=lambda e_city: get_distance(lat, long, e_city["latitude"], e_city["longitude"]))
        #a_matched_rows.sort(key=lambda e_city: get_distance(lat, long, e_city["latitude"], e_city["longitude"]))
    else:
        e_match = a_matched_rows[0]
    
    if isinstance(e_locale_results, dict):
        o_db.close()
        o_db = None
        e_locale_results["city_info"] = e_match
    return e_match


def prune_zip_cache():
    dt_now = datetime.utcnow()
    if len(e_ZIPCODE_INFO_CACHE) > i_MAX_ZIP_CACHE:
        for t_loc, (e_zip_cache, dt_zip_cache_loaded, dt_zip_cache_reload) in list(e_ZIPCODE_INFO_CACHE.items()):
            if dt_zip_cache_reload < dt_now:
                del e_ZIPCODE_INFO_CACHE[t_loc]
            
        if len(e_ZIPCODE_INFO_CACHE) > i_MAX_ZIP_CACHE + 100:
            a_oldest = sorted(list(e_ZIPCODE_INFO_CACHE.items()), key=lambda t: t[1][1]) # sort from oldest to newest cached
            for t_loc, (e_zip_cache, dt_zip_cache_loaded, dt_zip_cache_reload) in a_oldest[:i_MAX_ZIP_CACHE]:
                del e_ZIPCODE_INFO_CACHE[t_loc]


#@utils.timer
def get_zipcode(s_zip, s_state, lat=None, long=None, e_locale_results=None, **kwargs):
    #if s_zip is None: return {}
    if s_zip is None or (not s_zip and (not lat or not long)): return {}
    
    if not e_NATIONAL_MONTH_OCCUPANCIES or o_DB is None: load_data()
    
    prune_zip_cache()
    
    t_cached = e_ZIPCODE_INFO_CACHE.get((s_state, s_zip))
    if t_cached:
        e_zip_cache, dt_zip_cache_loaded, dt_zip_cache_reload = t_cached
        if e_zip_cache and dt_zip_cache_loaded and dt_zip_cache_reload and dt_zip_cache_reload > dt_zip_cache_loaded:
            if isinstance(e_locale_results, dict):
                e_locale_results["zipcode_info"] = e_zip_cache
            return e_zip_cache
    
    if isinstance(e_locale_results, dict):
        o_db = db_tables.get_connection()
    else:
        o_db = o_DB
    
    dataset_quarter = kwargs.get("dataset_quarter", db_tables.e_CONFIG.get("latest_dataset_quarter"))
    s_state = us_states.e_STATE_ABBREV.get(s_state.lower(), s_state)
    
    a_matched_rows = []
    
    if s_zip:
        a_matched_rows = [
            o_db.search_row(
                db_tables.zipcode_info,
                {
                    "state": s_state,
                    "zipcode": s_zip,
                    "dataset_quarter": dataset_quarter,
                },
                i_max_rows=1
            )
        ]
    
    if a_matched_rows and (db_tables.a_ZIP_INFO_COLS is None or a_matched_rows[0] is None or len(a_matched_rows[0]) != len(db_tables.a_ZIP_INFO_COLS)):
        db_tables.load_db(reload=True)
    
    if len(a_matched_rows) == 1 and a_matched_rows[0]:
        e_match = {
                k: v if not isinstance(v, Decimal) else float(v)
                for k, v in zip(db_tables.a_ZIP_INFO_COLS, a_matched_rows[0])}
        a_matched_rows = [e_match]
        dt_zip_cache_loaded = datetime.utcnow()
        dt_zip_cache_reload = dt_zip_cache_loaded + timedelta(hours=db_tables.e_CONFIG.get("area_zipcode_reload_hours", 3), minutes=db_tables.e_CONFIG.get("area_zipcode_reload_minutes", 0))
        e_ZIPCODE_INFO_CACHE[(s_state, s_zip)] = (e_match, dt_zip_cache_loaded, dt_zip_cache_reload)
    
    elif (not a_matched_rows or not a_matched_rows[0]) and kwargs.get("search_nearby", True):
        a_matched_rows, d_matched_distance = get_closest_zipcodes(lat, long, e_locale_results=e_locale_results, **kwargs)
    
    if not a_matched_rows or a_matched_rows[0] is None:
        if isinstance(e_locale_results, dict):
            o_db.close()
            o_db = None
            e_locale_results["zipcode_info"] = {}
        return {}
    
    if len(a_matched_rows) > 1:
        #a_matched_rows.sort(key=lambda e_city: get_distance(lat, long, e_city["latitude"], e_city["longitude"]))
        #df_nearby = pd.DataFrame(a_matched_rows, columns=["distance"] + db_tables.a_ZIP_INFO_COLS)
        df_nearby = pd.DataFrame(
            [
                tuple(None if isinstance(v, float) and np.isinf(v) else v if not isinstance(v, Decimal) else float(v) for v in t_match)
                for t_match in a_matched_rows[:10]],
            columns=["distance"] + db_tables.a_ZIP_INFO_COLS)
        global e_ZIP_AGGS
        if not e_ZIP_AGGS:
            e_ZIP_AGGS = location_analysis.select_aggregators(df_nearby)
        #return dict(df_nearby.dropna(axis=1, how='all').agg(e_ZIP_AGGS))
        #print(f"s_zip: '{s_zip}'")
        #print(f"lat, long: '{lat}, {long}'")
        #print(df_nearby)
        #return df_nearby, e_ZIP_AGGS
        e_zip_avgs = dict(df_nearby.agg(e_ZIP_AGGS))
        if isinstance(e_locale_results, dict):
            o_db.close()
            o_db = None
            e_locale_results["zipcode_info"] = e_zip_avgs
            e_locale_results["closest_zipcodes"] = a_matched_rows
        return e_zip_avgs
    else:
        if isinstance(a_matched_rows[0], dict):
            e_matched = a_matched_rows[0]
        else:
            e_matched = {
                k: None if isinstance(v, float) and np.isinf(v) else v if not isinstance(v, Decimal) else float(v)
                for k, v in zip(db_tables.a_ZIP_INFO_COLS, a_matched_rows[0])}
        
        if isinstance(e_locale_results, dict):
            o_db.close()
            o_db = None
            e_locale_results["zipcode_info"] = e_matched
        return e_matched


def get_zipcode_nearness_score(e_prop, s_dist_key="distance", s_score_key="score_overall", d_max_distance=d_DEFAULT_ZIPCODES_MAX_DISTANCE, b_invert_score=False, d_distance_proportion=0, **kwargs):
    d_distance = e_prop.get(s_dist_key, 0)
    if d_distance < d_max_distance:
        d_dist_score = ((d_max_distance - d_distance) / d_max_distance) ** 0.125
    else:
        d_dist_score = 0
        
    if s_score_key == s_dist_key:
        return d_dist_score
    
    d_score = float(e_prop.get(s_score_key, 0))
    if b_invert_score:
        d_score = 1 - b_invert_score
    
    if d_distance_proportion:
        d_combined_score = (d_score * (1 - d_distance_proportion)) + (d_dist_score * d_distance_proportion)
    else:
        d_combined_score = d_score * d_dist_score
        
    #e_prop["distance_score"] = d_combined_score
    return d_combined_score


#@utils.timer
def get_closest_zipcodes(lat=None, long=None, i_min_closest=3, e_locale_results=None, **kwargs):
    if lat is None or long is None: return [], 0
    
    i_matches = 0
    d_range = kwargs.get("initial_range", d_INITIAL_RANGE_IN_DEGREES)
    d_expansion = kwargs.get("expand_range", d_EXPAND_RANGE)
    d_max_range = kwargs.get("max_range", d_MAX_RANGE_IN_DEGREES)
    
    if not db_tables.e_CONFIG: load_data()
    #if kwargs.get("o_db"):
    #    o_db = kwargs["o_db"]
    #else:
    #    o_db = o_DB
    
    if isinstance(e_locale_results, dict):
        o_db = db_tables.get_connection()
    else:
        o_db = o_DB
    
    dataset_quarter = kwargs.get("dataset_quarter", db_tables.e_CONFIG.get("latest_dataset_quarter"))
    
    while i_matches < i_min_closest and d_range <= d_max_range:
        min_lat = (lat - d_range) * 10000
        max_lat = (lat + d_range) * 10000
        min_long = (long - (d_range * cos(radians(lat)))) * 10000
        max_long = (long + (d_range * cos(radians(lat)))) * 10000
        
        if kwargs.get("fields"):
            if isinstance(kwargs["fields"], (list, tuple, set)):
                a_fields = list(kwargs["fields"])
                s_other_fields = ", ".join([f"{db_tables.zipcode_info}.{s_field}" for s_field in a_fields])
            else:
                a_fields = [s.strip() for s in kwargs["fields"].split(",")]
                s_other_fields = kwargs["fields"]
        else:
            s_other_fields = f"{db_tables.zipcode_info}.*"
            
        s_sql = f"""
        SELECT GCDistDeg(latitude, longitude, {lat}, {long}) * 69.172 as distance, {s_other_fields}
        FROM {db_tables.zipcode_info}
        WHERE
            long_hash BETWEEN {min_long} AND {max_long}
            AND lat_hash BETWEEN {min_lat} AND {max_lat}
            AND dataset_quarter = '{dataset_quarter}'
        ORDER BY distance ASC
        """
        #if kwargs.get("b_debug"): print(s_sql)

        n_start = time()
        a_closest = o_db.execute(s_sql)
        if kwargs.get("b_debug"): print(f"Finding {len(a_closest):,} matches within {d_range * 69.172:0.1f} took: {utils.secondsToStr(time() - n_start)}")
    
        i_matches = len(a_closest)
        d_range *= d_expansion
        
    if isinstance(e_locale_results, dict):
        o_db.close()
        o_db = None
    
    return a_closest, d_range / d_expansion


#@utils.timer
def get_closest_cities(lat=None, long=None, i_min_closest=3, **kwargs):
    i_matches = 0
    d_range = kwargs.get("initial_range", d_INITIAL_RANGE_IN_DEGREES)
    d_expansion = kwargs.get("expand_range", d_EXPAND_RANGE)
    d_max_range = kwargs.get("max_range", d_MAX_RANGE_IN_DEGREES)
    
    if o_DB is None: load_data()
    if kwargs.get("o_db"):
        o_db = kwargs["o_db"]
    else:
        o_db = o_DB
    
    
    dataset_quarter = kwargs.get("dataset_quarter", db_tables.e_CONFIG.get("latest_dataset_quarter"))
    
    while i_matches < i_min_closest and d_range <= d_max_range:
        min_lat = (lat - d_range) * 10000
        max_lat = (lat + d_range) * 10000
        min_long = (long - (d_range * cos(radians(lat)))) * 10000
        max_long = (long + (d_range * cos(radians(lat)))) * 10000

        s_sql = f"""
        SELECT GCDistDeg(latitude, longitude, {lat}, {long}) * 69.172 as distance, {db_tables.city_info}.*
        FROM {db_tables.city_info}
        WHERE
            long_hash BETWEEN {min_long} AND {max_long}
            AND lat_hash BETWEEN {min_lat} AND {max_lat}
            AND dataset_quarter = '{dataset_quarter}'
        ORDER BY distance ASC
        """
        if kwargs.get("b_debug"): print(s_sql)

        n_start = time()
        a_closest = o_db.execute(s_sql)
        if kwargs.get("b_debug"): print(f"Finding {len(a_closest):,} matches within {d_range * 69.172:0.1f} took: {utils.secondsToStr(time() - n_start)}")
    
        i_matches = len(a_closest)
        d_range *= d_expansion
    
    return a_closest, d_range / d_expansion


#@utils.timer
def get_per_month_occupancies_from_local_data(d_annual_occupancy, e_local_data, **kwargs):
    e_state = e_local_data.get("state")
    e_city = e_local_data.get("city")
    e_zipcode = e_local_data.get("zipcode")
    
    e_occupancies = {}
    if e_city and e_zipcode:
        for i in range(1, 13):
            s_key = f"occupancy_month_{i}"
            d_city = e_city.get(s_key, 0)
            d_zip = e_zipcode.get(s_key, 0)
            if pd.isnull(d_city): d_city = 0
            if pd.isnull(d_zip): d_zip = 0
            e_occupancies[i] = float((d_city + d_zip) / 2)
    
    elif e_city and e_state:
        for i in range(1, 13):
            s_key = f"occupancy_month_{i}"
            d_city = e_city.get(s_key, 0)
            d_state = e_state.get(s_key, 0)
            if pd.isnull(d_city): d_city = 0
            if pd.isnull(d_state): d_state = 0
            e_occupancies[i] = float((d_city + d_state) / 2)
    
    elif e_zipcode and e_state:
        for i in range(1, 13):
            s_key = f"occupancy_month_{i}"
            d_zip = e_zipcode.get(s_key, 0)
            d_state = e_state.get(s_key, 0)
            if pd.isnull(d_zip): d_zip = 0
            if pd.isnull(d_state): d_state = 0
            e_occupancies[i] = float((d_zip + d_state) / 2)
    
    elif e_state:
        for i in range(1, 13):
            s_key = f"occupancy_month_{i}"
            d_state = e_state.get(s_key, 0)
            if pd.isnull(d_state): d_state = 0
            e_occupancies[i] = float(d_state)
    else:
        e_occupancies = e_NATIONAL_MONTH_OCCUPANCIES
    
    d_area_occupancy = utils.value_list_avg(list(e_occupancies.values()))
    
    if not pd.isnull(d_area_occupancy):
        d_area_occ_adjustment = d_annual_occupancy / d_area_occupancy
        
        #e_predicted_per_month = {i_month: round(d_month * d_area_occ_adjustment, 8) for i_month, d_month in e_occupancies.items()}
        e_predicted_per_month = {f"occupancy_month_{i_month}": round(d_month * d_area_occ_adjustment, 8) for i_month, d_month in e_occupancies.items()}
    else:
        #e_predicted_per_month = {i_month: round(d_month, 8) for i_month, d_month in e_occupancies.items()}
        e_predicted_per_month = {f"occupancy_month_{i_month}": round(d_month, 8) for i_month, d_month in e_occupancies.items()}
    
    return e_predicted_per_month


#@utils.timer
def get_per_month_rate_from_local_data(d_daily_rate_occupancy, e_local_data, **kwargs):
    e_state = e_local_data.get("state")
    e_city = e_local_data.get("city")
    e_zipcode = e_local_data.get("zipcode")
    
    e_rates = {}
    if e_city and e_zipcode:
        for i in range(1, 13):
            s_key = f"rate_month_{i}"
            d_city = e_city.get(s_key)
            d_zip = e_zipcode.get(s_key)
            b_null_city =  pd.isnull(d_city)
            b_null_zip =  pd.isnull(d_zip)
            if not b_null_city and not b_null_zip:
                e_rates[i] = float((d_city + d_zip) / 2)
            elif not b_null_city:
                e_rates[i] = float(d_city)
            elif not b_null_zip:
                e_rates[i] = float(d_zip)
            else:
                e_rates[i] = np.nan
    
    elif e_city and e_state:
        for i in range(1, 13):
            s_key = f"rate_month_{i}"
            d_city = e_city.get(s_key)
            d_state = e_state.get(s_key)
            b_null_city =  pd.isnull(d_city)
            b_null_state =  pd.isnull(d_state)
            if not b_null_city and not b_null_state:
                e_rates[i] = float((d_city + d_state) / 2)
            elif not b_null_city:
                e_rates[i] = float(d_city)
            elif not b_null_state:
                e_rates[i] = float(d_state)
            else:
                e_rates[i] = np.nan
    
    elif e_zipcode and e_state:
        for i in range(1, 13):
            s_key = f"rate_month_{i}"
            d_zip = e_zipcode.get(s_key)
            d_state = e_state.get(s_key)
            b_null_zip =  pd.isnull(d_zip)
            b_null_state =  pd.isnull(d_state)
            if not b_null_zip and not b_null_state:
                e_rates[i] = float((d_zip + d_state) / 2)
            elif not b_null_zip:
                e_rates[i] = float(d_zip)
            elif not b_null_state:
                e_rates[i] = float(d_state)
            else:
                e_rates[i] = np.nan
    
    elif e_state:
        for i in range(1, 13):
            s_key = f"rate_month_{i}"
            d_state = e_state.get(s_key, 0)
            if pd.isnull(d_state): d_state = 0
            e_rates[i] = float(d_state)
    else:
        e_rates = e_NATIONAL_MONTH_RATES
    
    d_area_rate = utils.value_list_avg(list(e_rates.values()))
    
    if not pd.isnull(d_area_rate):
        d_area_rate_adjustment = d_daily_rate_occupancy / d_area_rate
        
        #e_predicted_per_month = {i_month: round(d_month * d_area_rate_adjustment, 2) for i_month, d_month in e_rates.items()}
        e_predicted_per_month = {f"rate_month_{i_month}": round(d_month * d_area_rate_adjustment, 2) for i_month, d_month in e_rates.items()}
    else:
        #e_predicted_per_month = {i_month: round(d_month, 2) for i_month, d_month in e_rates.items()}
        e_predicted_per_month = {f"rate_month_{i_month}": round(d_month, 2) for i_month, d_month in e_rates.items()}
    return e_predicted_per_month


#@utils.timer
def get_per_month_occupancies(d_annual_occupancy, df_nearby_zips=None, s_state=None, **kwargs):
    if df_nearby_zips is not None and len(df_nearby_zips) > 0:
        e_occupancies = dict(df_nearby_zips[:3][a_OCCUPANCY_COLS].mean(axis=0))
        #print("use nearby zip occupancy", df_nearby_zips[:5][a_OCCUPANCY_COLS])
    elif s_state is not None and s_state in e_STATE_MONTH_OCCUPANCIES:
        #print("use state occupancy")
        e_occupancies = e_STATE_MONTH_OCCUPANCIES[s_state]
    else:
        #print("use national occupancy")
        e_occupancies = e_NATIONAL_MONTH_OCCUPANCIES
    
    d_area_occupancy = utils.value_list_avg(list(e_occupancies.values()))
    #d_area_occupancy = np.mean(list(e_occupancies.values()))
    
    if not pd.isnull(d_area_occupancy):
        d_area_occ_adjustment = d_annual_occupancy / d_area_occupancy
        
        e_predicted_per_month = {s_month: round(d_month * d_area_occ_adjustment, 8) for s_month, d_month in e_occupancies.items()}
    else:
        e_predicted_per_month = {s_month: round(d_month, 8) for s_month, d_month in e_occupancies.items()}
    
    return e_predicted_per_month


#@utils.timer
def get_per_month_rate(d_daily_rate_occupancy, df_nearby_zips=None, s_state=None, **kwargs):
    
    if df_nearby_zips is not None and len(df_nearby_zips) > 0:
        e_rates = dict(df_nearby_zips[:3][a_RATE_COLS].mean(axis=0))
        #print("use nearby zip rates", df_nearby_zips[:2][a_RATE_COLS])
    elif s_state is not None and s_state in e_STATE_MONTH_RATES:
        #print("use state rates")
        e_rates = e_STATE_MONTH_RATES[s_state]
    else:
        #print("use national rates")
        e_rates = e_NATIONAL_MONTH_RATES
    #print("e_rates", e_rates)
    
    d_area_rate = utils.value_list_avg(list(e_rates.values()))
    #d_area_rate = np.mean(list(e_rates.values()))
    #print("d_area_rate", d_area_rate)
    
    if not pd.isnull(d_area_rate):
        d_area_rate_adjustment = d_daily_rate_occupancy / d_area_rate
        #print("d_area_rate_adjustment", d_area_rate_adjustment)
        
        e_predicted_per_month = {i_month: round(d_month * d_area_rate_adjustment, 2) for i_month, d_month in e_rates.items()}
    else:
        e_predicted_per_month = {i_month: round(d_month, 2) for i_month, d_month in e_rates.items()}
    return e_predicted_per_month


#@utils.timer
def get_monthly_area_data(d_lat, d_long, d_occupancy, d_rate, s_state=None, d_total_rev=None, e_local_data=None, **kwargs):
    if s_state:
        s_state = us_states.e_STATE_ABBREV.get(s_state.lower(), s_state).upper()
    
    if d_total_rev is None: d_total_rev = round(d_occupancy * 365 * d_rate, 2)
    
    
    if e_local_data and (e_local_data.get("city") or e_local_data.get("zipcode")):
        e_monthly_occupancies = get_per_month_occupancies_from_local_data(d_occupancy, e_local_data, **kwargs)
        e_monthly_rates = get_per_month_rate_from_local_data(d_rate, e_local_data, **kwargs)
    else:
        if e_local_data.get("closest_zipcodes"):
            a_closest_zip_codes = e_local_data["closest_zipcodes"]
        elif isinstance(d_lat, (float, int)) and isinstance(d_long, (float, int)) and kwargs.get("search_closest_zipcodes", True):
            a_closest_zip_codes, d_search_range = get_closest_zipcodes(d_lat, d_long, b_debug=False, **kwargs)
        else:
            a_closest_zip_codes, d_search_range = [], 0
        
        #print(len(a_closest_zip_codes), d_search_range)
        
        #if a_closest_zip_codes and len(a_closest_zip_codes[0]) != len(db_tables.a_ZIP_INFO_COLS) + 1:
        if a_closest_zip_codes and (db_tables.a_ZIP_INFO_COLS is None or a_closest_zip_codes[0] is None or len(a_closest_zip_codes[0]) != len(db_tables.a_ZIP_INFO_COLS) + 1):
            db_tables.load_db(reload=True)
        
        df_nearby = pd.DataFrame(a_closest_zip_codes, columns=["distance"] + db_tables.a_ZIP_INFO_COLS) if len(a_closest_zip_codes) > 0 else None
        #print(df_nearby)
        e_monthly_occupancies = get_per_month_occupancies(d_occupancy, df_nearby, s_state=s_state)
        e_monthly_rates = get_per_month_rate(d_rate, df_nearby, s_state=s_state)
    
    #print("e_monthly_occupancies", e_monthly_occupancies)
    #print("e_monthly_rates", e_monthly_rates)
    e_monthly_revs = {
        f"revenue_month_{i_month}": float(e_monthly_occupancies.get(f"occupancy_month_{i_month}", 0)) * float(e_monthly_rates.get(f"rate_month_{i_month}", 0)) * utils.e_DAYS_PER_MONTH[i_month]
        for i_month in range(1, 12 + 1)
    }
    a_mon_revs = utils.list_numbers(e_monthly_revs.values())
    d_adj = d_total_rev / sum(a_mon_revs) if a_mon_revs else 1
    e_monthly_rev = {
        i: round(x * d_adj, 2) if not pd.isnull(d_adj) else 0 for i, x in e_monthly_revs.items()}

    if kwargs.get("month_names", False):
        return {
            "monthly_occupancy": {
                utils.e_MONTH_NAMES.get(re.sub(r"[^\d]", "", mon), mon): val
                for mon, val in e_monthly_occupancies.items()},
            "monthly_rate": {
                utils.e_MONTH_NAMES.get(re.sub(r"[^\d]", "", mon), mon): val
                for mon, val in e_monthly_rates.items()},
            "monthly_revenue": {
                utils.e_MONTH_NAMES.get(re.sub(r"[^\d]", "", mon), mon): val
                for mon, val in e_monthly_rev.items()},
        }
    elif kwargs.get("month_numbers", False):
        return {
            "monthly_occupancy": {
                int(re.sub(r"[^\d]", "", mon)) if re.search(r"[^\d]", mon) else mon: val
                for mon, val in e_monthly_occupancies.items()},
            "monthly_rate": {
                int(re.sub(r"[^\d]", "", mon)) if re.search(r"[^\d]", mon) else mon: val
                for mon, val in e_monthly_rates.items()},
            "monthly_revenue": {
                int(re.sub(r"[^\d]", "", mon)) if re.search(r"[^\d]", mon) else mon: val
                for mon, val in e_monthly_rev.items()},
        }
    else:
        return {
            "monthly_occupancy": e_monthly_occupancies,
            "monthly_rate": e_monthly_rates,
            "monthly_revenue": e_monthly_rev,
        }


#@utils.timer
def get_local_data(e_address, **kwargs):
    s_city = e_address["address_components"].get("locality", [""])[0]
    s_state = e_address["address_components"].get("state", [""])[0]
    s_zip = e_address["address_components"].get("postal_code", [""])[0]
    
    if kwargs.get("e_locale_results"):
        e_locale_results = kwargs["e_locale_results"]
    else:
        e_locale_results = {}
        
        thr_state = threading.Thread(target=get_state, args=(s_state,), kwargs={"e_locale_results": e_locale_results,})
        thr_city = threading.Thread(target=get_city, args=(s_city, s_state,), kwargs={"e_locale_results": e_locale_results,})
        thr_zip = threading.Thread(target=get_zipcode, args=(s_zip, s_state,), kwargs={"e_locale_results": e_locale_results, "lat": e_address["lat"], "long": e_address["lng"]})
        
        thr_state.start()
        thr_city.start()
        thr_zip.start()
        
        thr_state.join()
        thr_city.join()
        thr_zip.join()
        
    e_state_info = e_locale_results.get("state_info", {})
    e_city_info = e_locale_results.get("city_info", {})
    e_zip_info = e_locale_results.get("zipcode_info", {})
    e_closest_zips = e_locale_results.get("closest_zipcodes", None)
    
    if not e_NATIONAL_INFO or not dt_NAT_LOADED or not dt_NAT_RELOAD or dt_NAT_RELOAD <= dt_NAT_LOADED:
        load_national()
    e_national_info = e_NATIONAL_INFO
    
    if kwargs.get("exclude_features", True) and kwargs.get("exclude_locations", True):
        e_national_info = {k: v for k, v in e_national_info.items() if not k.startswith(("feature_", "smoking_allowed", "pets_allowed", "location_"))}
        e_state_info = {k: v for k, v in e_state_info.items() if not k.startswith(("feature_", "smoking_allowed", "smoking_allowed", "pets_allowed", "location_"))}
        e_city_info = {k: v for k, v in e_city_info.items() if not k.startswith(("feature_", "smoking_allowed", "smoking_allowed", "pets_allowed", "location_"))}
        e_zip_info = {k: v for k, v in e_zip_info.items() if not k.startswith(("feature_", "smoking_allowed", "smoking_allowed", "pets_allowed", "location_"))}
    else:
        if kwargs.get("exclude_features", True):
            e_national_info = {k: v for k, v in e_national_info.items() if not k.startswith(("feature_", "smoking_allowed", "pets_allowed"))}
            e_state_info = {k: v for k, v in e_state_info.items() if not k.startswith(("feature_", "smoking_allowed", "smoking_allowed", "pets_allowed"))}
            e_city_info = {k: v for k, v in e_city_info.items() if not k.startswith(("feature_", "smoking_allowed", "smoking_allowed", "pets_allowed"))}
            e_zip_info = {k: v for k, v in e_zip_info.items() if not k.startswith(("feature_", "smoking_allowed", "smoking_allowed", "pets_allowed"))}
        
        elif kwargs.get("exclude_locations", True):
            e_national_info = {k: v for k, v in e_national_info.items() if not k.startswith("location_")}
            e_state_info = {k: v for k, v in e_state_info.items() if not k.startswith("location_")}
            e_city_info = {k: v for k, v in e_city_info.items() if not k.startswith("location_")}
            e_zip_info = {k: v for k, v in e_zip_info.items() if not k.startswith("location_")}
    
    e_local_data = {
        "national": e_national_info,
        "state": e_state_info,
        "city": e_city_info,
        "zip": e_zip_info,
        "closest_zipcodes": e_closest_zips,
    }
    
    if kwargs.get("area_scores", True):
        a_score_cols = [k for k in e_national_info.keys() if k.startswith("score_")]
        
        if e_state_info and e_city_info and e_zip_info:
            e_area_scores = {
                k: (
                    e_state_info[k] if e_state_info.get(k, 0) is not None else 0
                    + e_city_info[k] if e_city_info.get(k, 0) is not None else 0
                    + e_zip_info.get(k, 0) if d_zip.get(k, 0) is not None else 0
                ) / 3
                for k in a_score_cols}
        elif e_state_info and e_city_info:
            e_area_scores = {
                k: (
                    e_state_info[k] if e_state_info.get(k, 0) is not None else 0
                    + e_city_info[k] if e_city_info.get(k, 0) is not None else 0
                ) / 2
                for k in a_score_cols}
        elif e_state_info and e_zip_info:
            e_area_scores = {
                k: (
                    e_state_info[k] if e_state_info.get(k, 0) is not None else 0
                    + e_zip_info.get(k, 0) if d_zip.get(k, 0) is not None else 0
                ) / 2
                for k in a_score_cols}
        elif e_state_info:
            e_area_scores = {k: e_state_info[k] if e_state_info.get(k, 0) is not None else 0 for k in a_score_cols}
        else:
            e_area_scores = {k: e_national_info.get(k, 0) for k in a_score_cols}
        
        e_local_data["area_scores"] = e_area_scores
    
    return e_local_data


def get_local_property_tax_rate(e_local_data):
    e_property_tax_rates = {}
    if e_local_data.get("zip", {}).get("property_tax_rate"):
        e_property_tax_rates["zip"] = round(e_local_data["zip"]["property_tax_rate"] / 100, 8)
    if e_local_data.get("city", {}).get("property_tax_rate"):
        e_property_tax_rates["city"] = round(e_local_data["city"]["property_tax_rate"] / 100, 8)
    if e_local_data.get("state", {}).get("property_tax_rate"):
        e_property_tax_rates["state"] = round(e_local_data["state"]["property_tax_rate"] / 100, 8)
    if e_local_data.get("national", {}).get("property_tax_rate"):
        e_property_tax_rates["national"] = round(e_local_data["national"]["property_tax_rate"] / 100, 8)
    
    if e_property_tax_rates.get("zip") and e_property_tax_rates.get("city"):
        return np.mean([e_property_tax_rates["zip"], e_property_tax_rates["city"]])
    elif e_property_tax_rates.get("zip") and e_property_tax_rates.get("state"):
        return np.mean([e_property_tax_rates["zip"], e_property_tax_rates["state"]])
    elif e_property_tax_rates.get("city") and e_property_tax_rates.get("state"):
        return np.mean([e_property_tax_rates["city"], e_property_tax_rates["state"]])
    elif e_property_tax_rates.get("state"):
        return e_property_tax_rates["state"]
    else:
        return 0.0095
