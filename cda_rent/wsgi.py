﻿try:
    from .utils import get_time
    from .revenue_api import app, LOGGER
except:
    from utils import get_time
    from revenue_api import app, LOGGER

if __name__ == "__main__":
    app.run()
    LOGGER.info(f"{get_time()}\tStart revenue_api.py through WSGI")