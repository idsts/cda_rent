﻿import re
import urllib
#from flask import request


def interpret_get_data(v_request):
    if not isinstance(v_request, str):
        v_request = v_request.get_data(as_text=True)
    
    e_data = {}
    
    a_fields = v_request.split("&")
    for s_field in a_fields:
        t = s_field.split("=", 1)
        if len(t) == 2:
            s_name, s_vals = t
            s_name_cleaned = urllib.parse.unquote(s_name)
            s_vals_cleaned = urllib.parse.unquote(s_vals.replace("+", " "))
            
            if "||" in s_vals_cleaned:
                a_clean_vals = []
                for s_val in s_vals_cleaned.split("||"):
                    if not s_val.strip(): continue
                    #s_val_cleaned = urllib.parse.unquote(s_val).strip()
                    s_val_cleaned = s_val.strip()
                    if s_val_cleaned:
                        a_clean_vals.append(s_val_cleaned)
                e_data[s_name_cleaned] = a_clean_vals
            else:
                e_data[s_name_cleaned] = s_vals_cleaned
    return e_data
