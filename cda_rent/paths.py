﻿import os
try:
    from . import utils
except:
    import utils

s_FILES = os.path.join(os.path.dirname(__file__), 'files')
print("s_FILES=", s_FILES)
s_LOG_PATH = os.path.join(s_FILES, "logs")
assert utils.create_folder(os.path.join(s_LOG_PATH, ""))
s_INPUT_PATH = os.path.join(s_FILES, "input")
assert utils.create_folder(os.path.join(s_INPUT_PATH, ""))
s_URL_PATH = os.path.join(s_FILES, "urls")
assert utils.create_folder(os.path.join(s_URL_PATH, ""))
s_OLD_URL_PATH = os.path.join(s_FILES, "old_urls")
assert utils.create_folder(os.path.join(s_OLD_URL_PATH, ""))
s_WORKING_PATH = os.path.join(s_FILES, "working")
assert utils.create_folder(os.path.join(s_WORKING_PATH, ""))
s_COMPLETE_PATH = os.path.join(s_FILES, "completed")
assert utils.create_folder(os.path.join(s_COMPLETE_PATH, ""))
s_PREDICTION_PATH = os.path.join(s_FILES, "predictions")
assert utils.create_folder(os.path.join(s_PREDICTION_PATH, ""))
s_FAILED_PATH = os.path.join(s_FILES, "failed")
assert utils.create_folder(os.path.join(s_FAILED_PATH, ""))
s_INTERACTIVE_EXAMPLES_PATH = os.path.join(s_FILES, "interactive_examples")
assert utils.create_folder(os.path.join(s_INTERACTIVE_EXAMPLES_PATH, ""))

s_DEV_WEBHOOKS_PATH = os.path.join(s_FILES, "dev_webhooks")
assert utils.create_folder(os.path.join(s_DEV_WEBHOOKS_PATH, ""))