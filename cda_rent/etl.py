﻿import os, sys, re, csv
from collections import defaultdict, Counter, namedtuple
from math import sin, cos, sqrt, atan2, radians
from datetime import datetime
from calendar import monthrange
import pandas as pd
import numpy as np

try:
    from . import airbnb_standardization, zillow_standardization, trends
    from .airbnb_standardization import *
    from .zillow_dicts import e_FIXER, c_CONDEMNED
except ImportError:
    import airbnb_standardization, zillow_standardization, trends
    from airbnb_standardization import *
    from zillow_dicts import e_FIXER, c_CONDEMNED

e_EXTENDED_COL_DTYPES = {
    "Property ID": "str",
    "Listing Title": "str",
    "Property Type": "str",
    "Listing Type": "str",
    "Listing Type": "str",
    "Created Date": "str",
    "Last Scraped Date": "str",
    "Country": "str",
    "State": "str",
    "City": "str",
    "Zipcode": "str",
    "Neighborhood": "str",
    "Metropolitan Statistical Area": "str",
    "Currency Native": "str",
    "Average Daily Rate (USD)": "float",
    "Average Daily Rate (Native)": "float",
    "Annual Revenue LTM (USD)": "float",
    "Annual Revenue LTM (Native)": "float",
    "Occupancy Rate LTM": "float",
    "Number of Bookings LTM": "float",
    "Number of Reviews": "float",
    "Bedrooms": "float",
    "Bathrooms": "float",
    "Max Guests": "float",
    "Calendar Last Updated": "str",
    "Response Rate": "float",
    "Airbnb Response Time (Text)": "str",
    #"Airbnb Superhost": "bool",
    #"HomeAway Premier Partner": "bool",
    "Cancellation Policy": "str",
    "Security Deposit (USD)": "float",
    "Security Deposit (Native)": "float",
    "Cleaning Fee (USD)": "float",
    "Cleaning Fee (Native)": "float",
    "Extra People Fee (USD)": "float",
    "Extra People Fee (Native)": "float",
    "Published Nightly Rate (USD)": "float",
    "Published Monthly Rate (USD)": "float",
    "Published Weekly Rate (USD)": "float",
    "Check-in Time": "str",
    "Checkout Time": "str",
    "Minimum Stay": "float",
    "Count Reservation Days LTM": "float",
    "Count Available Days LTM": "float",
    "Count Blocked Days LTM": "float",
    "Number of Photos": "float",
    #"Instantbook Enabled": "bool",
    "Listing URL": "str",
    "Listing Main Image URL": "str",
    "Listing Images": "str",
    "Latitude": "float",
    "Longitude": "float",
    #"Exact Location": "bool",
    "Overall Rating": "float",
    "Airbnb Communication Rating": "float",
    "Airbnb Accuracy Rating": "float",
    "Airbnb Cleanliness Rating": "float",
    "Airbnb Checkin Rating": "float",
    "Airbnb Location Rating": "float",
    "Airbnb Value Rating": "float",
    #"Pets Allowed": "bool",
    #"Integrated Property Manager": "bool",
    "Amenities": "str",
    "HomeAway Location Type": "str",
    "Airbnb Property Plus": "str",
    "Airbnb Home Collection": "str",
    "License": "str",
    "Airbnb Property ID": "str",
    "Airbnb Host ID": "str",
    "HomeAway Property ID": "str",
    "HomeAway Property Manager ID": "str",
}

e_MONTHLY_COL_DTYPES = {
    "Property ID": "str",
    "Property Type": "str",
    "Listing Type": "str",
    
    "records": "int",
    
    "Bedrooms": "float",
    "Reporting Month": "str",
    "Occupancy Rate": "float",
    "Revenue (USD)": "float",
    "Revenue (Native)": "float",
    "ADR (USD)": "float",
    "ADR (Native)": "float",
    "Number of Reservations": "float",
    "Reservation Days": "float",
    "Available Days": "float",
    "Blocked Days": "float",
    
    "Country": "str",
    "State": "str",
    "City": "str",
    "Zipcode": "str",
    "Neighborhood": "str",
    "Metropolitan Statistical Area": "str",
    
    "Latitude": "float",
    "Longitude": "float",
    "Active": "bool",
    "Scraped During Month": "bool",
    
    "Currency Native": "str",
    "Airbnb Property ID": "str",
    "Airbnb Host ID": "str",
    "HomeAway Property ID": "str",
    "HomeAway Property Manager": "str",
}
e_SCRAPED_COL_DTYPES = {
    'Unnamed: 0': 'int',
    'Property_ID': 'str',
    'airbnb_id': 'str',
    'id': 'str',
    'zpid': 'float',
    'homeStatus': 'str',
    'address_streetAddress': 'str',
    'address_city': 'str',
    'address_state': 'str',
    'address_zipcode': 'float',
    'address_neighborhood': 'str',
    'address_subdivision': 'str',
    "address": "str",
    "city": "str",
    "state": "str",
    "zip": "str",
    "home_status": "str",
    'price': 'float',
    "zestimate": 'float',
    "last_sold_price": 'float',
    "tax_assessed_value": 'float',
    "property_tax_rate": 'float',
    "monthly_hoa_fee": 'float',
    'yearBuilt': 'float',
    'latitude': 'float',
    'longitude': 'float',
    'currency': 'str',
    'livingArea': 'str',
    'timeZone': 'str',
    'homeType': 'str',
    'postingProductType': 'str',
    'priceChangeDate': 'float',
    'propertyTaxRate': 'float',
    "rentzestimate": 'float',
    'rentZestimate': 'float',
    'lotSize': 'str',
    'daysOnZillow': 'float',
    "property_type": "str",
    "broker_name": "str",
    'agent_agent_reason': 'float',
    'agent_badge_type': 'str',
    'agent_display_name': 'str',
    'agent_encoded_zuid': 'str',
    'agent_first_name': 'str',
    'agent_image_data_height': 'float',
    'agent_image_data_url': 'str',
    'agent_image_data_width': 'float',
    'agent_phone_areacode': 'float',
    'agent_phone_number': 'float',
    'agent_phone_prefix': 'float',
    'agent_profile_url': 'str',
    'agent_rating_average': 'float',
    'agent_recent_sales': 'float',
    'agent_review_count': 'float',
    'agent_reviews_url': 'str',
    'agent_services_offered_0': 'float',
    'agent_services_offered_1': 'float',
    'agent_write_review_url': 'str',
    'priceHistory_0_date': 'str',
    'priceHistory_0_price': 'float',
    'priceHistory_0_event': 'str',
    'priceHistory_1_date': 'str',
    'priceHistory_1_price': 'float',
    'priceHistory_1_event': 'str',
    'priceHistory_2_date': 'str',
    'priceHistory_2_price': 'float',
    'priceHistory_2_event': 'str',
    'priceHistory_3_date': 'str',
    'priceHistory_3_price': 'float',
    'priceHistory_3_event': 'str',
    'priceHistory_4_date': 'str',
    'priceHistory_4_price': 'float',
    'priceHistory_4_event': 'str',
    'priceHistory_5_date': 'str',
    'priceHistory_5_price': 'float',
    'priceHistory_5_event': 'str',
    'priceHistory_6_date': 'str',
    'priceHistory_6_price': 'float',
    'priceHistory_6_event': 'str',
    'priceHistory_7_date': 'str',
    'priceHistory_7_price': 'float',
    'priceHistory_7_event': 'str',
    'priceHistory_8_date': 'str',
    'priceHistory_8_price': 'float',
    'priceHistory_8_event': 'str',
    'priceHistory_9_date': 'str',
    'priceHistory_9_price': 'float',
    'priceHistory_9_event': 'str',
    'priceHistory_10_date': 'str',
    'priceHistory_10_price': 'float',
    'priceHistory_10_event': 'str',
    'priceHistory_11_date': 'str',
    'priceHistory_11_price': 'float',
    'priceHistory_11_event': 'str',
    'priceHistory_12_date': 'str',
    'priceHistory_12_price': 'float',
    'priceHistory_12_event': 'str',
    'priceHistory_13_date': 'str',
    'priceHistory_13_price': 'float',
    'priceHistory_13_event': 'str',
    'priceHistory_14_date': 'str',
    'priceHistory_14_price': 'float',
    'priceHistory_14_event': 'str',
    'priceHistory_15_date': 'str',
    'priceHistory_15_price': 'float',
    'priceHistory_15_event': 'str',
    'priceHistory_16_date': 'str',
    'priceHistory_16_price': 'float',
    'priceHistory_16_event': 'str',
    'priceHistory_17_date': 'str',
    'priceHistory_17_price': 'float',
    'priceHistory_17_event': 'str',
    'priceHistory_18_date': 'str',
    'priceHistory_18_price': 'float',
    'priceHistory_18_event': 'str',
    'priceHistory_19_date': 'str',
    'priceHistory_19_price': 'float',
    'priceHistory_19_event': 'str',
    'priceHistory_20_date': 'str',
    'priceHistory_20_price': 'float',
    'priceHistory_20_event': 'str',
    'priceHistory_21_date': 'str',
    'priceHistory_21_price': 'float',
    'priceHistory_21_event': 'str',
    'priceHistory_22_date': 'str',
    'priceHistory_22_price': 'float',
    'priceHistory_22_event': 'str',
    'priceHistory_23_date': 'str',
    'priceHistory_23_price': 'float',
    'priceHistory_23_event': 'str',
    'priceHistory_24_date': 'str',
    'priceHistory_24_price': 'float',
    'priceHistory_24_event': 'str',
    'priceHistory_25_date': 'str',
    'priceHistory_25_price': 'float',
    'priceHistory_25_event': 'str',
    'priceHistory_26_date': 'str',
    'priceHistory_26_price': 'float',
    'priceHistory_26_event': 'str',
    'priceHistory_27_date': 'str',
    'priceHistory_27_price': 'float',
    'priceHistory_27_event': 'str',
    'priceHistory_28_date': 'str',
    'priceHistory_28_price': 'float',
    'priceHistory_28_event': 'str',
    'priceHistory_29_date': 'str',
    'priceHistory_29_price': 'float',
    'priceHistory_29_event': 'str',
    'priceHistory_30_date': 'str',
    'priceHistory_30_price': 'float',
    'priceHistory_30_event': 'str',
    'bathroomsFull': 'float',
    'bathroomsThreeQuarter': 'float',
    'bathroomsHalf': 'float',
    'bathroomsOneQuarter': 'float',
    'bedrooms': 'float',
    'bathrooms': 'float',
    'basement': 'str',
    'flooring': 'str',
    'heating': 'str',
    'cooling': 'str',
    'appliances': 'str',
    'laundryFeatures': 'str',
    'fireplaces': 'float',
    'fireplaceFeatures': 'str',
    'commonWalls': 'str',
    'buildingArea': 'str',
    'aboveGradeFinishedArea': 'str',
    'virtualTour': 'str',
    'parking': 'float',
    'parkingFeatures': 'str',
    'garageSpaces': 'float',
    'coveredSpaces': 'float',
    'openParkingSpaces': 'float',
    'carportSpaces': 'float',
    'otherParking': 'str',
    'accessibilityFeatures': 'str',
    'levels': 'str',
    'stories': 'float',
    'entryLevel': 'str',
    'entryLocation': 'str',
    'spaFeatures': 'str',
    'exteriorFeatures': 'str',
    'patioAndPorchFeatures': 'str',
    'fencing': 'str',
    'view': 'str',
    'waterfrontFeatures': 'str',
    'frontageType': 'str',
    'frontageLength': 'str',
    'topography': 'str',
    'vegetation': 'str',
    'lotSizeDimensions': 'str',
    'otherStructures': 'str',
    'additionalParcelsDescription': 'str',
    'parcelNumber': 'str',
    'landLeaseAmount': 'float',
    'zoning': 'str',
    'zoningDescription': 'str',
    'architecturalStyle': 'str',
    'constructionMaterials': 'str',
    'foundationDetails': 'str',
    'roofType': 'str',
    'windowFeatures': 'str',
    'propertyCondition': 'str',
    'developmentStatus': 'str',
    'onMarketDate': 'float',
    'builderModel': 'str',
    'builderName': 'str',
    'electric': 'str',
    'gas': 'str',
    'sewer': 'str',
    'utilities': 'str',
    'greenBuildingVerificationType': 'str',
    'greenEnergyEfficient': 'str',
    'greenIndoorAirQuality': 'str',
    'greenSustainability': 'str',
    'greenWaterConservation': 'str',
    'numberOfUnitsInCommunity': 'float',
    'storiesTotal': 'float',
    'buildingFeatures': 'str',
    'structureType': 'str',
    'buildingName': 'str',
    'elementarySchool': 'str',
    'elementarySchoolDistrict': 'str',
    'middleOrJuniorSchool': 'str',
    'middleOrJuniorSchoolDistrict': 'str',
    'highSchool': 'str',
    'highSchoolDistrict': 'str',
    'securityFeatures': 'str',
    'communityFeatures': 'str',
    'cityRegion': 'str',
    'associationFee': 'str',
    'associationAmenities': 'str',
    'associationFeeIncludes': 'str',
    'associationName': 'str',
    'associationPhone': 'str',
    'associationFee2': 'str',
    'associationName2': 'str',
    'associationPhone2': 'str',
    'taxAssessedValue': 'float',
    'taxAnnualAmount': 'float',
    'buildingAreaSource': 'str',
    'specialListingConditions': 'str',
    'availabilityDate': 'float',
    'buyerAgencyCompensation': 'float',
    'buyerAgencyCompensationType': 'str',
    'interiorFeatures': 'str',
    'leaseTerm': 'str',
    'lotFeatures': 'str',
    'otherEquipment': 'str',
    'poolFeatures': 'str',
    'propertySubType': 'str',
    'virtualTourURLUnbranded': 'str',
    'waterView': 'str',
    'livingAreaRange': 'str',
    'livingAreaRangeUnits': 'str',
    'exclusions': 'str',
    'subdivisionName': 'str',
    'bodyType': 'str',
    'elevationUnits': 'str',
    'horseAmenities': 'str',
    'inclusions': 'str',
    'listingTerms': 'str',
    'ownership': 'str',
    'ownershipType': 'str',
    'roadSurfaceType': 'str',
    'tenantPays': 'str',
    'totalActualRent': 'float',
    'waterSource': 'str',
    'listAOR': 'str',
    'agent_services_offered_2': 'str',
    'agent_services_offered_3': 'str',
    'agent_business_name': 'str',
    'agent_services_offered_4': 'str',
    'agent_services_offered_5': 'str',
    'agent_description': 'str',
}

e_SCRAPED_IMPORTANT_COLS = {
    "Property ID": ["Property_ID"],
    "airbnb_id": [],
    "zpid": ['id'],
    "stories": [],
    "distance": [],
    "latitude": [],
    "longitude": [],
    'levels': ["stories"],
    'property_type': [],
    'property_sub_type': ['propertySubType'],
    'home_type': [],
    'architectural_style': ['architecturalStyle'],
    'structure_type': ['structureType'],
    'bedrooms': [],
    'bathrooms': [],
    'construction_materials': ['constructionMaterials'],
    'zestimate': [],
    'rentzestimate': ['rentZestimate'],
    "last_sold_price": [],
    "tax_assessed_value": ["taxAssessedValue"],
    'year': ['yearBuilt'],
    'property_condition': ['propertyCondition'],
    "date_sold": [],
    "tax_annual_amount": ["taxAnnualAmount"],
    "tax_assessed_year": [],
    "property_tax_rate": ['propertyTaxRate'],
    "address": ["address_streetAddress"],
    "city": ["address_city"],
    "state": ["address_state"],
    "zip": ["address_zipcode"],
    'has_garage': ['hasGarage', 'has_attached_garage'],
    'parking_features': ['parking'],
    'has_carport': ['hasCarport'],
    'has_open_parking': ['hasOpenParking'],
    'has_fireplace': ['fireplaces'],
    "has_private_pool": ['hasPrivatePool'],
    'has_spa': ['hasSpa'],
    'has_pets_allowed': ['hasPetsAllowed'],
    'monthly_hoa_fee': [],
    #'hoa_fee': [],
    'has_rent_control': ['hasRentControl'],
    'has_waterfront_view': ['waterViewYN'],
    'has_view': [],
    'horse_YN': ['horseYN', 'can_raise_horses', 'canRaiseHorses'],
    'elevation': [],
    'living_area': ['livingArea'],
    'building_area': ['buildingArea', 'area'],
    'lot_size': ['lotSize'],
}


tn_GPS = namedtuple("gps_box", ["min_longitude", "max_longitude", "min_latitude", "max_latitude"])
i_CURRENT_YEAR = datetime.utcnow().year


def find_area(lat, long, e_areas):
    for t_lat, e_lngs in e_areas.items():
        if t_lat[0] <= lat <= t_lat[1]:
            for t_lng, t_gps in e_lngs.items():
                if t_lng[0] <= long <= t_lng[1]:
                    return t_gps
    return None


def get_adjacent_areas(tn_area, d_lat_inc=0, d_long_inc=0, b_include_diagonals=True):
    if tn_area.__class__.__name__ == "tuple": tn_area = tn_GPS(*tn_area)
    
    if not d_lat_inc:
        d_lat_inc = abs(tn_area.max_latitude - tn_area.min_latitude)
    if not d_long_inc:
        d_long_inc = abs(tn_area.max_longitude - tn_area.min_longitude)
    
    a_adjacent = [
        tn_GPS(tn_area.min_longitude, tn_area.max_longitude, tn_area.min_latitude - d_lat_inc, tn_area.max_latitude - d_lat_inc),
        tn_GPS(tn_area.min_longitude - d_long_inc, tn_area.max_longitude - d_long_inc, tn_area.min_latitude, tn_area.max_latitude),
        tn_GPS(tn_area.min_longitude, tn_area.max_longitude, tn_area.min_latitude + d_lat_inc, tn_area.max_latitude + d_lat_inc),
        tn_GPS(tn_area.min_longitude + d_long_inc, tn_area.max_longitude + d_long_inc, tn_area.min_latitude, tn_area.max_latitude),
    ]
    
    if b_include_diagonals:
        a_adjacent.extend([
            tn_GPS(tn_area.min_longitude - d_long_inc, tn_area.max_longitude - d_long_inc, tn_area.min_latitude - d_lat_inc, tn_area.max_latitude - d_lat_inc),
            tn_GPS(tn_area.min_longitude + d_long_inc, tn_area.max_longitude + d_long_inc, tn_area.min_latitude - d_lat_inc, tn_area.max_latitude - d_lat_inc),
            tn_GPS(tn_area.min_longitude - d_long_inc, tn_area.max_longitude - d_long_inc, tn_area.min_latitude + d_lat_inc, tn_area.max_latitude + d_lat_inc),
            tn_GPS(tn_area.min_longitude + d_long_inc, tn_area.max_longitude + d_long_inc, tn_area.min_latitude + d_lat_inc, tn_area.max_latitude + d_lat_inc),
        ])
        return a_adjacent


def get_distance(point1, point2):
    # get distance in kilometers
    R = 6371
    lat1 = radians(point1[0])  #insert value
    lon1 = radians(point1[1])
    lat2 = radians(point2[0])
    lon2 = radians(point2[1])

    dlon = lon2 - lon1
    dlat = lat2- lat1

    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1-a))
    distance = R * c
    return distance


def create_buckets(tn_area, i_lat_buckets=20, i_long_buckets=20, e_avg_row={}, **kwargs):
    d_lat_span = abs(tn_area.max_latitude - tn_area.min_latitude)
    d_long_span = abs(tn_area.max_longitude - tn_area.min_longitude)
    
    d_lat_inc = d_lat_span / (i_lat_buckets - 1)
    d_long_inc = d_long_span / (i_long_buckets - 1)
    #print(round(d_lat_span, 4), round(d_long_span, 4), round(d_lat_inc, 4), round(d_long_inc, 4))
    
    print(f"{i_lat_buckets * i_long_buckets:,} buckets ({i_lat_buckets:,} x {i_long_buckets:,})")
    
    a_bucket_rows = []
    d_lat = min(tn_area.max_latitude, tn_area.min_latitude) #+ d_lat_inc / 2
    for i_lat in range(i_lat_buckets):
        d_long = min(tn_area.max_longitude, tn_area.min_longitude) #+ d_long_inc / 2
        for i_long in range(i_long_buckets):
            e_row = e_avg_row.copy()
            e_row.update({
                "latitude": d_lat,
                "longitude": d_long,
                "latitude_end": d_lat + d_lat_inc,
                "longitude_end": d_long+ d_long_inc,
            })
            a_bucket_rows.append(e_row)
            d_long += d_long_inc
        
        d_lat += d_lat_inc
        #break
        
    df_buckets = pd.DataFrame(a_bucket_rows)
    
    #df_buckets["area"] = list(zip(df_buckets["latitude"], df_buckets["longitude"], df_buckets["latitude_end"], df_buckets["longitude_end"]))
    df_buckets["area"] = list(zip(df_buckets["longitude"], df_buckets["longitude_end"], df_buckets["latitude"], df_buckets["latitude_end"]))
    df_buckets = df_buckets.set_index("area")
    
    #df_buckets["properties"] = 0
    
    df_buckets.shape
    
    return df_buckets, d_lat_inc, d_long_inc


def get_airbnb_monthly(df_monthly_file, **kwargs):
    if isinstance(df_monthly_file, str):
        df_monthly_file = pd.read_csv(
            df_monthly_file,
            low_memory=True,
            dtype=e_MONTHLY_COL_DTYPES,
            parse_dates=["Reporting Month",],)
        
    #standardize property types
    df_monthly_file["Property Type"] = df_monthly_file["Property Type"].apply(lambda x: e_PROP_TYPE_STD.get(str(x).lower(), x))
    if kwargs.get("b_debug"): print(dict(df_monthly_file["Property Type"].value_counts()))
    
    # turn property types into broader categories
    if "Property Category" not in df_monthly_file.columns:
        df_monthly_file.insert(2, "Property Category", df_monthly_file["Property Type"].apply(lambda x: e_PROP_CAT_STD.get(str(x).lower(), "Other")))
    else:
        df_monthly_file["Property Category"] = df_monthly_file["Property Type"].apply(lambda x: e_PROP_CAT_STD.get(str(x).lower(), "Other"))
    df_monthly_file["Property Category"].value_counts()
    
    # filter to entire listings, and relevant categories, and records with enough data
    #df_monthly = df_monthly_file[(df_monthly_file["Listing Type"].isin(["Entire home/apt"])) & (df_monthly_file["Property Type"].isin(["Entire house", "House"]))]
    df_monthly = df_monthly_file[
        (df_monthly_file["Listing Type"].isin(["Entire home/apt"]))
        #& (df_monthly_file["Property Type"].isin(["House", "Vacation home"]))
        & (df_monthly_file["Property Category"].isin(["House", "Vacation home", "Townhouse"]))
        & ((df_monthly_file["Available Days"] > 0) | (df_monthly_file["Reservation Days"] > 0))
    ].copy()
    df_monthly.insert(0, "records", 1)
    print(df_monthly.shape)
    
    # correct for records with more actual reservation days than listed available days to avoid issues later
    df_monthly["Available Days"] = df_monthly["Available Days"].clip(lower=df_monthly["Reservation Days"])
    return df_monthly


def get_airbnb_extended(df_extended_file, **kwargs):
    if isinstance(df_extended_file, str):
        df_extended_file = pd.read_csv(
            df_extended_file,
            low_memory=True,
            dtype=e_EXTENDED_COL_DTYPES,
            parse_dates=["Created Date"], #, "Last Scraped Date", "Calendar Last Updated"],
        )
    
    df_extended_file["Property Type"] = df_extended_file["Property Type"].apply(lambda x: e_PROP_TYPE_STD.get(str(x).lower(), x))
    
    # turn property types into broader categories
    if "Property Category" not in df_extended_file.columns:
        df_extended_file.insert(2, "Property Category", df_extended_file["Property Type"].apply(lambda x: e_PROP_CAT_STD.get(str(x).lower(), x)))
    else:
        df_extended_file["Property Category"] = df_extended_file["Property Type"].apply(lambda x: e_PROP_CAT_STD.get(str(x).lower(), x))
    
    df_features = airbnb_standardization.standardize_features(df_extended_file)
    return df_extended_file, df_features


def adjust_revenue(revenue, availability, max_availability=0.95, min_availability=.08333, adjust_factor=0.75):
    d_adjustment = max(min_availability, min(1, (availability / max_availability)))
    return round(revenue / (d_adjustment ** adjust_factor), 2)


def process_monthly(
    df_monthly,
    a_metadata_cols=[
        "Property ID", "Latitude", "Longitude",
        "City", "State", "Zipcode", "Property Type", "Bedrooms", "Host ID"],
    a_revenue_cols=[
        "Property ID", "records", "Revenue (USD)", "Number of Reservations",
        "Reservation Days", "Available Days", "Blocked Days", ],
    a_annualize_cols=[
        "Revenue (USD)", "Number of Reservations",
        "Reservation Days", "Available Days", "Blocked Days"],
    **kwargs
):
    if "records" not in df_monthly:
        df_monthly["records"] = 1
    
    df_monthly["calendar_month"] = df_monthly["Reporting Month"].dt.month
    
    np_zero_rev = df_monthly[~(df_monthly["Revenue (USD)"] > 0)].index

    df_monthly.loc[np_zero_rev, "Occupancy Rate"] = 0
    df_monthly.loc[np_zero_rev, "Available Days"] += df_monthly.loc[np_zero_rev, "Reservation Days"]
    df_monthly.loc[np_zero_rev, "Reservation Days"] = 0
    
    np_rev_zero_res = df_monthly[(df_monthly["Revenue (USD)"] > 0) & ~(df_monthly["Number of Reservations"] > 0)].index
    df_monthly.loc[np_rev_zero_res, "Number of Reservations"] = 1
    
    df_monthly["days"] = df_monthly["Reporting Month"].apply(lambda x: monthrange(x.year, x.month)[1])
    df_monthly["open_days"] = (df_monthly["days"] - df_monthly['Blocked Days']).replace(np.inf, 0).clip(
        lower=df_monthly['Reservation Days'])
    if "days" not in a_revenue_cols: a_revenue_cols.append("days")
    if "open_days" not in a_revenue_cols: a_revenue_cols.append("open_days")
    
    df_monthly["rate"] = (df_monthly['Revenue (USD)'] / df_monthly['Reservation Days']).replace(np.inf, 0).fillna(0)
    if "rate" not in a_revenue_cols: a_revenue_cols.append("rate")
    
    df_monthly["Host ID"] = df_monthly["Airbnb Host ID"].fillna(df_monthly["HomeAway Property Manager"])
    e_host_props = {}
    for host_id, df_host in df_monthly[["Property ID", "Host ID"]].groupby("Host ID"):
        i_props = len(set(df_host["Property ID"]))
        e_host_props[host_id] = i_props
    
    df_monthly_metadata = df_monthly[a_metadata_cols].drop_duplicates(subset=["Property ID"])
    df_monthly_metadata["Owner Properties"] = df_monthly_metadata["Host ID"].apply(lambda x: e_host_props.get(x, 1))
    df_monthly_metadata = df_monthly_metadata.set_index("Property ID")
    
    df_monthly_available = df_monthly[
        (df_monthly["Reservation Days"] > 0)
        | (df_monthly["Available Days"] > 0)
    ].copy()
    assert "Reporting Month" in df_monthly_available.columns
    
    
    df_property_revenue = df_monthly_available[a_revenue_cols].groupby(["Property ID"]).sum()
    df_property_revenue["rate"] = (df_property_revenue['Revenue (USD)'] / df_property_revenue['Reservation Days']).replace(np.inf, 0).fillna(0)
    #print(list(df_property_revenue.columns))
    #assert "Reporting Month" in df_property_revenue.columns
    
    df_dates = df_monthly_available[["Property ID", "Reporting Month"]].copy()
    df_dates["Reporting Month1"] = df_dates["Reporting Month"]
    df_date_summary = df_dates.groupby("Property ID").agg(
        {"Reporting Month": "min", "Reporting Month1": "max",}
    ).rename(columns={"Reporting Month": "first_date", "Reporting Month1": "last_date"})
    
    
    df_rate_by_month = df_monthly_available[[
        "Property ID", "calendar_month", "ADR (USD)", "Occupancy Rate"
    ]].rename(columns={"ADR (USD)": "rate", "Occupancy Rate": "occupancy"}).groupby(
        ["Property ID", "calendar_month"]).mean().unstack()#.reset_index()
    df_rate_by_month.columns = [f"{t[0]}_month_{t[1]}" for t in df_rate_by_month.columns]
    
    df_property_revenue["occupancy"] = df_property_revenue['Reservation Days'] / df_property_revenue["days"]
    df_property_revenue["occupancy_vs_available"] = df_property_revenue['Reservation Days'] / df_property_revenue["open_days"]
    df_property_revenue["availability"] = df_property_revenue["open_days"] / df_property_revenue["days"]
    
    
    df_property_revenue["reservation_length"] = (df_property_revenue["Reservation Days"] / df_property_revenue["Number of Reservations"]).replace(np.inf, np.nan).fillna(0).round(2)
    df_property_revenue["revenue_per_reservation"] = (df_property_revenue["Revenue (USD)"] / df_property_revenue["Number of Reservations"]).replace(np.inf, np.nan).fillna(0).round(2)
    
    # turn numbers into annual
    for col in a_annualize_cols:
        #df_property_revenue[col] /= df_property_revenue["records"]
        #df_property_revenue[col] *= 12
        df_property_revenue[col] = (df_property_revenue[col] / df_property_revenue["days"]) * 365
    
    # rough adjustment assuming property was available year round. Not exact because if availability is during peak season won't translate to same performance off peak.
    #df_property_revenue["availability"] = [min(1, x / 365) for x in df_property_revenue["Available Days"]]
    #df_property_revenue["adj_revenue"] = (df_property_revenue['Revenue (USD)'] / df_property_revenue['availability']).fillna(0)
    
    #df_property_revenue["occupancy_vs_available"] = df_property_revenue["Reservation Days"] / (df_property_revenue["Reservation Days"] + df_property_revenue["Available Days"])
    
    #df_property_revenue["occupancy"] = df_property_revenue["Reservation Days"] / (df_property_revenue["Reservation Days"] + df_property_revenue["Available Days"] +  df_property_revenue["Blocked Days"])
    #df_property_revenue["occupancy_vs_available"] = df_property_revenue["Reservation Days"] / (df_property_revenue["Reservation Days"] + df_property_revenue["Available Days"])
    

    #df_property_revenue["availability"] = [1 - min(1, x / 365) for x in df_property_revenue["Blocked Days"]]
    df_property_revenue["adj_revenue"] = [
        adjust_revenue(revenue, availability, max_availability=0.95, min_availability=.08333, adjust_factor=1)
        for revenue, availability in zip(df_property_revenue['Revenue (USD)'], df_property_revenue['availability'])]
        
    #df_property_revenue["adj_reservation_days"] = (df_property_revenue['Reservation Days'] / df_property_revenue['availability']).fillna(0)
    #df_property_revenue["adj_available_days"] = (df_property_revenue['Available Days'] / df_property_revenue['availability']).fillna(0)
    df_property_revenue["adj_reservation_days"] = 365 * df_property_revenue['occupancy'].fillna(0)
    df_property_revenue["adj_available_days"] = 365 * df_property_revenue['availability'].fillna(0)
    df_property_revenue["adj_blocked_days"] = 365 - df_property_revenue["adj_available_days"]
    
    e_prop_seasonality = defaultdict(dict)
    for prop_id, df_prop in df_monthly[["Property ID", "Reporting Month", "Revenue (USD)", "Available Days", "Reservation Days", "open_days"]].groupby("Property ID"):
    #    e_ta = trends.trend_season_analysis(list(df_prop["Revenue (USD)"]), list(df_prop["Reporting Month"]))
    
        e_ta_demand = trends.quick_seasonality(list(df_prop["Revenue (USD)"]), list(df_prop["Reporting Month"]))
        #e_ta_availability = trends.quick_seasonality(list(df_prop["Available Days"] + df_prop["Reservation Days"]), list(df_prop["Reporting Month"]))
        e_ta_availability = trends.quick_seasonality(list(df_prop["open_days"]), list(df_prop["Reporting Month"]))
        
        e_prop_seasonality[prop_id] = {
            "demand_seasonality": e_ta_demand["seasonality"],
            "demand_season_type": e_ta_demand.get("season_type"),
            "demand_season_length": e_ta_demand.get("season_length"),
            "demand_season_peak_month": e_ta_demand.get("season_peak_month"),
            "demand_season_max_month": e_ta_demand.get("season_max_month"),
            "availability_seasonality": e_ta_availability["seasonality"],
            "availability_season_type": e_ta_availability.get("season_type"),
            "availability_season_length": e_ta_availability.get("season_length"),
            "availability_season_peak_month": e_ta_availability.get("season_peak_month"),
            "availability_season_max_month": e_ta_availability.get("season_max_month"),
            
            #"season_name": e_ta.get("season_type"),
            #"season_start": e_ta["season_start"].month if e_ta.get("season_start") else None,
            #"season_end": e_ta["season_end"].month if e_ta.get("season_end") else None,
        }
    df_seasonality = pd.DataFrame.from_dict(e_prop_seasonality, orient="index")
    
    return df_monthly_metadata, df_property_revenue, df_seasonality, df_date_summary, df_rate_by_month


def add_relative_calcs(df_input):

    df_input["avg_rate"] = (((df_input["revenue"] / df_input["occupancy"] / 365) + (df_input["revenue"] / df_input["reserved_days"])) / 2).round(1)
    
    df_input["actual_rate"] = df_input["revenue"] / (df_input["occupancy"] * 365)
    df_input["adj_rate"] = df_input["adj_revenue"] / (df_input["occupancy"] * 365)

    df_input["rev_per_bedroom"] = df_input["adj_revenue"] / df_input["bedrooms"]
    df_input["rev_per_bathroom"] = df_input["adj_revenue"] / df_input["bathrooms"]
    df_input["rev_per_sqft"] = df_input["adj_revenue"] / df_input["living_area"]
    df_input["rev_per_acre"] = df_input["adj_revenue"] / df_input["lot_size"]
    df_input["rev_per_price"] = df_input["adj_revenue"] / df_input["price"]

    df_input["rate_per_bedroom"] = df_input["actual_rate"] / df_input["bedrooms"]
    df_input["rate_per_bathroom"] = df_input["actual_rate"] / df_input["bathrooms"]
    df_input["rate_per_sqft"] = df_input["actual_rate"] / df_input["living_area"]
    df_input["rate_per_acre"] = df_input["actual_rate"] / df_input["lot_size"]
    df_input["rate_per_price"] = df_input["actual_rate"] / df_input["price"]

    df_input["listed_rate_per_bedroom"] = df_input["listed_rate"] / df_input["bedrooms"]
    df_input["listed_rate_per_bathroom"] = df_input["listed_rate"] / df_input["bathrooms"]
    df_input["listed_rate_per_sqft"] = df_input["listed_rate"] / df_input["living_area"]
    df_input["listed_rate_per_acre"] = df_input["listed_rate"] / df_input["lot_size"]
    df_input["listed_rate_per_price"] = df_input["listed_rate"] / df_input["price"]

    df_input["rent_per_bedroom"] = df_input["rent_estimate"] / df_input["bedrooms"]
    df_input["rent_per_bathroom"] = df_input["rent_estimate"] / df_input["bathrooms"]
    df_input["rent_per_sqft"] = df_input["rent_estimate"] / df_input["living_area"]
    df_input["rent_per_acre"] = df_input["rent_estimate"] / df_input["lot_size"]
    df_input["rent_per_price"] = df_input["rent_estimate"] / df_input["price"]

    df_input["price_per_bedroom"] = df_input["price"] / df_input["bedrooms"]
    df_input["price_per_bathroom"] = df_input["price"] / df_input["bathrooms"]
    df_input["price_per_sqft"] = df_input["price"] / df_input["living_area"]
    df_input["price_per_acre"] = df_input["price"] / df_input["lot_size"]

    for col in [
        "rev_per_bedroom", "rev_per_bathroom", "rev_per_sqft", "rev_per_acre", "rev_per_price", 
        "rent_per_bedroom", "rent_per_bathroom", "rent_per_sqft", "rent_per_acre", "rent_per_price", 
        "price_per_bedroom", "price_per_bathroom", "price_per_sqft", "price_per_acre",
        "rate_per_bedroom", "rate_per_bathroom", "rate_per_sqft", "rate_per_acre", "rate_per_price", 
        "listed_rate_per_bedroom", "listed_rate_per_bathroom", "listed_rate_per_sqft", "listed_rate_per_acre", "listed_rate_per_price", 
    ]:
        df_input[col] = df_input[col].replace(np.inf, np.nan).replace(0, np.nan)


def merge_monthly_extended(df_monthly=None, df_common_features=None, df_scraped_clean=None, **kwargs):
    if kwargs.get("df_monthly_metadata") is not None and kwargs.get("df_property_revenue") is not None:
        df_monthly_metadata, df_property_revenue = kwargs["df_monthly_metadata"], kwargs["df_property_revenue"]
        df_date_summary, df_rate_by_month = kwargs["df_date_summary"], kwargs["df_rate_by_month"]
    else:
        df_monthly_metadata, df_property_revenue, df_date_summary, df_rate_by_month = process_monthly(df_monthly)
    
    
    if df_monthly_metadata.index.name != "Property ID":
        df_monthly_metadata = df_monthly_metadata.set_index("Property ID")
    if df_property_revenue.index.name != "Property ID":
        df_property_revenue = df_property_revenue.set_index("Property ID")
    if df_date_summary.index.name != "Property ID":
        df_date_summary = df_date_summary.set_index("Property ID")
    if df_common_features.index.name != "Property ID":
        df_common_features = df_common_features.set_index("Property ID")
    
    #print("df_common_features", df_common_features.shape)
    #print("df_scraped_clean", df_scraped_clean.shape)
    #print("df_monthly_metadata", df_monthly_metadata.shape)
    #print("df_property_revenue", df_date_summary.shape)
    #print("df_date_summary", df_date_summary.shape)
    #print("df_rate_by_month", df_rate_by_month.shape)
    
    if df_scraped_clean is not None:
        df_features_with_zillow = df_common_features.copy()
        a_scraped_meta = [
            'Property ID',
            'airbnb_id',
            'homeaway_id',
            'zpid',
            'address',
            'city',
            'state',
            'zip',
            'zcta',
            'latitude',
            'longitude',
            'date_sold',
            #'property_tax_rate',
            #'monthly_hoa_fee',
            #'gps_distance',
            #'price',
            'construction_materials',
        ]
        a_scraped_features = [col for col in df_scraped_clean.columns if col not in a_scraped_meta]
        #print("a_scraped_features", a_scraped_features)
        a_overlapped_features = [col for col in df_scraped_clean if col in df_common_features.columns and col not in a_scraped_meta]
        a_non_overlapped_features = [col for col in df_scraped_clean if col not in df_common_features.columns and col not in a_scraped_meta]
        #print(f"{len(a_overlapped_features)} cols in both airbnb/homeaway and scraped zillow data, {len(a_non_overlapped_features)} new scraped columns")
        df_features_with_zillow.update(df_scraped_clean[a_overlapped_features], overwrite=False)
        
        #df_common_features = df_features_with_zillow.merge(df_scraped_clean[a_non_overlapped_features], how="inner", on='Property ID')
        df_common_features = df_features_with_zillow.merge(df_scraped_clean[a_non_overlapped_features], how="left", on='Property ID')
        
        
    #print("df_common_features", df_common_features.shape, list(df_common_features.columns))
        
        #df_monthly_metadata = df_monthly_metadata.merge(df_scraped_clean[a_scraped_meta], how="inner", on="Property ID")
    
    #print("df_date_summary", df_date_summary.shape)
    
    df_property_revenue.rename(
        columns={
            "Revenue (USD)": "revenue",
            "Number of Reservations": "reservations",
            #"Reservation Days": "reserved_days",
            #"Available Days": "available_days",
            #"Blocked Days": "blocked_days",
            "adj_reservation_days": "reserved_days",
            "adj_available_days": "available_days",
            "adj_blocked_days": "blocked_days",
        },
        inplace=True)
    
    df_property_revenue = df_property_revenue.join(df_date_summary, how="inner")
    
    #print("df_property_revenue", df_property_revenue.shape, list(df_property_revenue.columns))
    
    a_property_rev_cols = [
        "revenue", "adj_revenue", "records",
        "availability", "occupancy", "occupancy_vs_available",
        "reserved_days", "available_days", "blocked_days", "days",
        "first_date", "last_date", "rate",
        "reservations", "reservation_length", "revenue_per_reservation",
    ]
    a_monthly_metadata_cols = ["Latitude", "Longitude", "Owner Properties"]
    a_monthly_metadata_cols.extend([
        "demand_seasonality", "demand_season_type", "demand_season_length",
        "demand_season_peak_month", "demand_season_max_month",
        "availability_seasonality", "availability_season_type", "availability_season_length",
        "availability_season_peak_month", "availability_season_max_month",
    ])
    
    #print("df_monthly_metadata.columns", list(a_monthly_metadata_cols))
    
    print("\tGetting common feature dummies")
    df_common_dummies = airbnb_standardization.prep_for_dummies(df_common_features)
    #print("df_common_dummies",  df_common_dummies.shape, list(df_common_dummies))
    
    print("\tGetting monthly metadata dummies")
    df_monthly_metadata_dummies = airbnb_standardization.prep_for_dummies(df_monthly_metadata[a_monthly_metadata_cols])
    #print("df_monthly_metadata_dummies", df_monthly_metadata_dummies.shape, list(df_monthly_metadata_dummies))
    
    print("\tMerging")
    df_inputs = df_property_revenue[
        (df_property_revenue["reservations"] >= 1)
        & (df_property_revenue["records"] >= 3)
    ][a_property_rev_cols].join(
    #df_inputs = df_property_revenue[a_property_rev_cols].join(
        df_monthly_metadata_dummies, how="left").join(
            df_common_dummies, how="inner") #.join(df_date_summary, how="left")
    return df_inputs


def zip_to_zcta(v_zip):
    if pd.isnull(v_zip): return None
    if isinstance(v_zip, str):
        if len(v_zip) < 5: v_zip = "{}{}".format("0" * (5 - len(v_zip)), v_zip)
    elif isinstance(v_zip, (int, float)):
        v_zip = f"{int(v_zip):05d}"
    return v_zip[:5]


def clean_scraped_input(df_scraped, **kwargs):
    for col, a_versions in e_SCRAPED_IMPORTANT_COLS.items():
        #if col in df_scraped.columns: continue
        for version in a_versions:
            if not version in df_scraped.columns: continue
            if col not in df_scraped.columns:
                df_scraped.rename(columns={version: col}, inplace=True)
            else:
                df_scraped[col] = df_scraped[col].fillna(df_scraped[version])
    
    df_scraped = df_scraped[[col for col in df_scraped.columns if col in e_SCRAPED_IMPORTANT_COLS.keys()]].copy()
    #print(df_scraped.columns)
    if 'Property_ID' in df_scraped.columns and 'Property ID' not in df_scraped.columns:
        df_scraped.rename(columns={'Property_ID': 'Property ID'}, inplace=True)
    
    if 'Property ID' not in df_scraped.columns:
        df_scraped['Property ID'] = None
    if 'airbnb_id' in df_scraped.columns:
        df_scraped['Property ID'] = df_scraped['Property ID'].fillna(df_scraped['airbnb_id'].apply(
            lambda x:
                x if isinstance(x, str) and x.startswith("ab") else
                f"ab-{x}" if isinstance(x, str) else
                f"ab-{str(int(x))}" if isinstance(x, (int, float)) and x > 0 else None))
    if 'homeaway_id' in df_scraped.columns:
        df_scraped['Property ID'] = df_scraped['Property ID'].fillna(df_scraped['homeaway_id'].apply(
            lambda x:
                x if isinstance(x, str) and x.startswith("ha") else
                f"ha-{x}" if isinstance(x, str) else
                f"ha-{str(int(x))}" if isinstance(x, (int, float)) and x > 0 else None))
    
    i_year = datetime.now().year
    if "year" in df_scraped.columns and 'property_condition' in df_scraped.columns:
        a_years = [
            max(1492, min(year, i_CURRENT_YEAR))  if not pd.isnull(year) else
            max(1492, min(i_CURRENT_YEAR, i_year + zillow_standardization.e_CONDITION_YEARS[condition])) if condition in zillow_standardization.e_CONDITION_YEARS else
            np.nan
            for idx, year, condition in df_scraped[["year", 'property_condition']].itertuples()]
        df_scraped["year_built"] = a_years
    
    if 'property_condition' in df_scraped.columns:
        df_scraped["fixer_upper"] = df_scraped['property_condition'].apply(lambda x: e_FIXER.get(x, 0)).fillna(0).astype(float)
        df_scraped["condemned"] = df_scraped['property_condition'].apply(lambda x: True if x in c_CONDEMNED else False)
    
    #if 'postingProductType' in df_scraped.columns:
    #    # check this
    #    df_scraped["auction"] = df_scraped['postingProductType'].apply(lambda x: True if x in zillow_standardization.c_AUCTION else False)
    
    
    if 'has_waterfront_view' in df_scraped.columns:
        df_scraped["location_waterfront"] = df_scraped['has_waterfront_view'].apply(lambda x: True if str(x).lower() in zillow_standardization.c_WATERFRONTS else False)
        
    if 'frontageType' in df_scraped.columns:
        df_scraped["location_waterfront"] = df_scraped['frontageType'].apply(lambda x: True if str(x).lower() in zillow_standardization.c_WATERFRONTS else False)
    
    if 'living_area' in df_scraped.columns:
        df_scraped["living_area"] = df_scraped['living_area'].apply(airbnb_standardization.clean_living_area)
    if 'building_area' in df_scraped.columns:
        df_scraped["building_area"] = df_scraped['building_area'].apply(airbnb_standardization.clean_living_area)
        df_scraped["living_area"] = df_scraped["living_area"].fillna(df_scraped["building_area"])
    
    if 'lot_size' in df_scraped.columns:
        df_scraped["lot_size"] = df_scraped['lot_size'].apply(airbnb_standardization.clean_lot_size)
    
    #if "stories" in df_scraped.columns:
    #    df_scraped["num_levels"] = df_scraped["stories"]
    #else:
    #    df_scraped["num_levels"] = None
    
    if "levels" in df_scraped.columns:
        df_scraped["levels"] = df_scraped["levels"].apply(
            lambda x: x if isinstance(x, (int, float)) and 0 < x < 100 else zillow_standardization.e_LEVEL_STORIES.get(x, None))
    
    a_parking = [
        True if s_parking or b_garage or b_carport or b_open else False
        for idx, s_parking, b_garage, b_carport, b_open in df_scraped[["parking_features", "has_garage", "has_carport", "has_open_parking"]].itertuples()]
    df_scraped["feature_parking"] = a_parking
    
    #if 'patioAndPorchFeatures' in df_scraped.columns:
    #    df_scraped["feature_patio_or_balcony"] = df_scraped['patioAndPorchFeatures'].apply(lambda x: True if not pd.isnull(x) and x else False)
    
    if 'has_fireplace' in df_scraped.columns:
        df_scraped["feature_fireplace"] = df_scraped['has_fireplace'].apply(lambda x: True if x in {True, 1, "True", "Yes"} else False)
    
    if 'has_pool' in df_scraped.columns:
        df_scraped["feature_pool"] = df_scraped['has_pool'].apply(lambda x: True if x in {True, 1, "True", "Yes"} else False)
    
    if 'has_spa' in df_scraped.columns:
        df_scraped["feature_jacuzzi"] = df_scraped['has_spa'].apply(lambda x: True if x in {True, 1, "True", "Yes"} else False)
    
    if 'horse_YN' in df_scraped.columns:
        df_scraped["location_horseback_riding"] = df_scraped['horse_YN'].apply(lambda x: True if x in {True, 1, "True", "Yes"} else False)
    
    if 'has_pets_allowed' in df_scraped.columns:
        df_scraped["pets_allowed"] = df_scraped['has_pets_allowed'].apply(lambda x: True if x in {True, 1, "True", "Yes"} else False)
    
    #if 'accessibilityFeatures' in df_scraped.columns:
    #    df_scraped["feature_wheelchair_accessible"] = df_scraped['accessibilityFeatures'].apply(lambda x: True if not pd.isnull(x) and "wheelchair" in x.lower() else False)
        
    #if "topography" in df_scraped.columns:
    #    df_scraped["topography"] = df_scraped["topography"].apply(zillow_standardization.standardize_topography)
        
    a_property_types = []
    for col in ["property_type", "structure_type", "architectural_style", "property_sub_type", 'home_type']:
        if col not in df_scraped.columns: df_scraped[col] = None
    for idx, ptype, struct, arch, subtype, hometype in df_scraped[["property_type", "structure_type", "architectural_style", "property_sub_type", 'home_type']].itertuples():
        s_ptype = ptype if not pd.isnull(ptype) else ""
        s_arch = arch if not pd.isnull(arch) else ""
        s_struct = struct if not pd.isnull(struct) else ""
        s_subtype = subtype if not pd.isnull(subtype) else ""
        s_hometype = hometype if not pd.isnull(hometype) else ""
        
        if not s_ptype and not s_arch and not s_struct and not s_subtype:
            a_property_types.append(None)
            continue
        
        s_ptype_posting = zillow_standardization.e_ZILLOW_PROP_TYPE_STD.get(s_ptype.lower())
        s_ptype_struct = zillow_standardization.e_ZILLOW_PROP_TYPE_STD.get(s_struct.lower())
        s_ptype_arch = zillow_standardization.e_ZILLOW_PROP_TYPE_STD.get(s_arch.lower())
        s_ptype_subtype = zillow_standardization.e_ZILLOW_PROP_TYPE_STD.get(s_subtype.lower())
        s_ptype_hometype = zillow_standardization.e_ZILLOW_PROP_TYPE_STD.get(s_hometype.lower())
        
        s_ptype = None
        for s_priority in airbnb_standardization.a_PROPERTY_TYPE_ORDER:
            if s_priority == s_ptype_struct:
                s_ptype = s_priority
                break
            elif s_priority == s_ptype_arch:
                s_ptype = s_priority
                break
            elif s_priority == s_ptype_subtype:
                s_ptype = s_priority
                break
            elif s_priority == s_ptype_posting:
                s_ptype = s_priority
                break
            elif s_priority == s_ptype_hometype:
                s_ptype = s_priority
                break
        
        a_property_types.append(s_ptype)
    
    #####
    
    a_rent_prices, a_list_prices = [], []
    a_price_cols = ["price", "zestimate", "tax_assessed_value", "last_sold_price", "rentzestimate"] + [col for col in df_scraped.columns if col.startswith("priceHistory")]
    for idx, e_row in df_scraped[[col for col in a_price_cols if col in df_scraped.columns]].to_dict(orient="index").items():
        
        a_price_inputs = []
        a_rent_inputs = []
        
        if e_row.get("price"):
            if e_row["price"] > 25000:
                a_price_inputs.append((e_row["price"], 10))
            else:
                a_rent_inputs.append((e_row["price"], 10))
            
        if e_row.get("tax_assessed_value") and e_row["tax_assessed_value"] > 25000:
            a_price_inputs.append((e_row["tax_assessed_value"], 2))
        if e_row.get("zestimate") and e_row["zestimate"] > 25000:
            a_price_inputs.append((e_row["zestimate"], 3))
        if e_row.get("last_sold_price") and e_row["last_sold_price"] > 25000:
            a_price_inputs.append((e_row["last_sold_price"], 3))
        if e_row.get("rentzestimate") and e_row["rentzestimate"] > 50:
            a_rent_inputs.append((e_row["rentzestimate"], 3))
        
        for i in range(20):
            if e_row.get(f"priceHistory/{i}/price") and e_row.get(f"priceHistory/{i}/event") in {"Sold", "Listed for sale", "Pending sale"}:
                d_price = e_row[f"priceHistory/{i}/price"]
                dt_price = e_row.get("priceHistory/{i}/date")
                
                if dt_price is not None:
                    if isinstance(dt_price, str):
                        dt_price = dateutil.parser.parse(dt_price)
                    
                    i_age = dt_TODAY.year - dt_price.year
                    if dt_TODAY.year - dt_price.year > timedelta(days=365):
                        dt_price *= (1 + d_REAL_ESTATE_INFLATION) ** i_age
                else:
                    i_age = 0
                
                a_price_inputs.append((d_price, 5 if i_age <=1 else  4 if i_age < 3 else 3 if i_age < 6 else 2 if i_age < 12 else 1))
            elif e_row.get(f"priceHistory_{i}_price") and e_row.get(f"priceHistory_{i}_event") in {"Listed for rent", "Price change"}:
                d_price = e_row[f"priceHistory/{i}/price"]
                if d_price > 10000 and e_row.get(f"priceHistory_{i}_event") != "Listed for rent": continue
                dt_price = e_row.get("priceHistory/{i}/date")
                
                if dt_price is not None:
                    if isinstance(dt_price, str):
                        dt_price = dateutil.parser.parse(dt_price)
                    
                    i_age = dt_TODAY.year - dt_price.year
                    if dt_TODAY.year - dt_price.year > timedelta(days=365):
                        dt_price *= (1 + d_REAL_ESTATE_INFLATION) ** i_age
                else:
                    i_age = 0
                
                a_rent_inputs.append((d_price, 5 if i_age <=1 else  4 if i_age < 3 else 3 if i_age < 6 else 2 if i_age < 12 else 1))
            
        
        
        if a_price_inputs:
            try:
                a_prices, a_price_weights = zip(*a_price_inputs)
                d_price = sum([t[0] * t[1] for t in a_price_inputs]) / sum(a_price_weights)
                a_list_prices.append(round(d_price, 2))
            except:
                print(a_price_inputs)
                raise Exception("")
        else:
            a_list_prices.append(np.nan)
        
        if a_rent_inputs:
            try:
                a_rents, a_rent_weights = zip(*a_rent_inputs)
                d_rent = sum([t[0] * t[1] for t in a_rent_inputs]) / sum(a_rent_weights)
                a_rent_prices.append(round(d_rent, 2))
            except:
                print(a_rent_inputs)
                raise Exception("")
        else:
            a_rent_prices.append(np.nan)
    #####
    df_scraped["calc_price"] = a_list_prices
    df_scraped["calc_rent"] = a_rent_prices
    
    if "zpid" in df_scraped.columns:
        #df_scraped["zpid"] = df_scraped["zpid"].str.replace("_zpid", "")
        df_scraped["zpid"] = df_scraped["zpid"].apply(lambda x: x if not isinstance(x, str) else x.replace("_zpid", ""))
    
    if kwargs.get("e_zip_to_zcta"):
        e_zip_to_zcta = kwargs["e_zip_to_zcta"]
        
        df_scraped["zcta"] = df_scraped["zip"].apply(zip_to_zcta).apply(e_zip_to_zcta.get)

        if kwargs.get("e_zcta"):
            e_zcta = kwargs["e_zcta"]
            df_scraped["ztca_population"] = df_scraped["zcta"].apply(lambda x: e_zcta.get(x, {}).get("population", 0))
            df_scraped["ztca_houses"] = df_scraped["zcta"].apply(lambda x: e_zcta.get(x, {}).get("houses", 0))
            df_scraped["ztca_population_density"] = df_scraped["zcta"].apply(lambda x: e_zcta.get(x, {}).get("population_density", 0))
            df_scraped["ztca_housing_density"] = df_scraped["zcta"].apply(lambda x: e_zcta.get(x, {}).get("housing_density", 0))
    
    df_scraped.drop(columns=[
        "ownershipType", "postingProductType", "architecturalStyle", "structureType", "propertySubType",
        'property_type', 'property_sub_type', 'home_type', 'architectural_style', 'structure_type',
        "propertyCondition", "isNewConstruction", 'property_condition', 'year', #'date_sold',
        "tax_assessed_year",
        "frontageType",
        "livingArea", "lotSize", "lotSizeDimensions", "buildingArea", "building_area",
        "yearBuilt", "yearBuiltEffective",
        "stories", "storiesTotal",
        "parking", "hasGarage", "hasCarport", "hasOpenParking",
        'waterfrontFeatures',
        'frontageLength',
        'waterViewYN', 'waterView',
        #'topography',
        'woodedArea',
        'canRaiseHorses', 'horse_YN',
        "patioAndPorchFeatures", "accessibilityFeatures", "fireplaces",
        'has_garage', 'parking_features', 'has_carport', 'has_open_parking', 'has_fireplace', "has_private_pool", 'has_spa', 'has_pets_allowed',
        'calc_rent',
        'has_view',
        'has_waterfront_view',
        'last_sold_price',
        'zestimate',
        'tax_assessed_value',
    ], inplace=True, errors="ignore")
    df_scraped.rename(columns={
        "Property_ID": "Property ID",
        "Property_ID": "Property ID",
        "distance": "gps_distance",
        #"id": "zpid",
        "address_streetAddress": "address_street",
        #"address_streetAddress": "address_street",
        #"num_levels": "levels",
        #"hasPetsAllowed": "pets_allowed",
        #"horseYN": "feature_horses_allowed",
        #"hasRentControl": "rent_control",
        #"rentZestimate": "rent_estimate",
        #"totalActualRent": "actual_rent",
        "calc_price": "price",
        #"calc_rent": "rent_actual",
        "rentzestimate": "rent",
        #"hasPrivatePool": "feature_pool",
        #"hasSpa": "feature_jacuzzi",
        "has_rent_control": "feature_rent_control",
    }, inplace=True)
    return df_scraped.set_index("Property ID")