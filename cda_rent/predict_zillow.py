﻿import os, sys, re, json, math
from time import time
from collections import Counter
from decimal import Decimal
import pandas as pd
import numpy as np
import joblib
#import keras
import tensorflow as tf
from tensorflow.keras import models

gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
  try:
    tf.config.experimental.set_virtual_device_configuration(
        gpus[0],[tf.config.experimental.VirtualDeviceConfiguration(memory_limit=5120)])
  except RuntimeError as e:
    print(e)

try:
    from . import utils, airbnb_standardization, zillow_standardization, hotel_proximity, prep_zillow
    from . import location_analysis, area_webhook, area_data, scoring, defaults
    from .defaults import *
except:
    import utils, airbnb_standardization, zillow_standardization, hotel_proximity, prep_zillow
    import location_analysis, area_webhook, area_data, scoring, defaults
    from defaults import *

__all__ = []

a_INCLUDE_EXTRACTED_METADATA_COLS = [
    "fixer_upper", "renovated", "condemned",
    "manufactured_home", "starter_home",
    "auction", "shared_ownership", "move_home", "timeshare", "lot_only",
]

a_INPUT_TO_OUTPUT_COLS = [
    "latitude", "longitude",
    "predicted_revenue_mape",
    #"predicted_revenue_mae",
    "predicted_avg_rate_mape",
    #"predicted_listed_rate_mae",
    "predicted_revenue_avg_rate",
    #"predicted_revenue_listed_rate",
    "calc_revenue_from_price",
    "predicted_occupancy",
    "predicted_revenue",
    "avg_rate_at_predicted_revenue",
    
    "predicted_revenue_optimized",
    "predicted_occupancy_optimized",
    
    "calc_revenue_avg_rate",
    #"calc_revenue_listed_rate",
    
    "area_occupancy",
    "area_revenue",
    "area_rev_per_bedroom",
    "area_rev_per_bathroom",
    "area_avg_rate",
    #"area_listed_rate",
    "area_rate_per_bedroom",
    #"area_listed_rate_per_bedroom",
    
    "area_seasonality",
    "area_season_length",
    "area_season_peak_month",
    "area_season_max_month",
    
    'area_precision',
    'area_properties',
    'area_scale',
    'area_vs_mean',
    'area_rent_vs_mean',
    
    "predicted_rent_mape",
]

a_PREDICTION_OUTPUT_COLS = [
    "pred_vs_price",
    
    "predicted_revenue",
    "predicted_occupancy",
    
    "avg_rate_at_predicted_revenue",
    
    "predicted_revenue_optimized",
    "predicted_occupancy_optimized",
    "cap_sqft",
    "price_per_sqft", 
    "rev_per_sqft",
    "optimized_rev_per_sqft",
    
    "area_rev_per_bedroom",
    "area_rev_per_bathroom",
    "area_rate_per_bedroom",
    "area_rate_per_bathroom",
    "area_revenue",
    "area_avg_rate",
    "area_cap_sqft",
    "area_occupancy", 
    "area_occupancy_vs_mean", 
    "area_price_per_sqft",
    "area_score_overall",
    
    "area_vs_mean",
    "area_precision",
    "area_properties",
     
    "area_seasonality",
    "area_season_length",
    
    "nearest_hotel_dist_0", "nearest_hotel_rate_0",
    "nearest_hotel_dist_1", "nearest_hotel_rate_1",
    "nearest_hotel_dist_2", "nearest_hotel_rate_2",
    
    "annual_expenses", "annual_expenses_long_term",
    
    "score_overall",
    "score_absolute",
    "score_relative",
    "score_area",
]
a_PREDICTION_OUTPUT_COLS.extend(a_INCLUDE_EXTRACTED_METADATA_COLS)


e_VARS = None
e_LTR_VARS = None
#o_MAE_MODEL = None
o_MAPE_MODEL = None
o_MAPE_RATE_MODEL = None
o_MAPE_RENT_MODEL = None
o_MAPE_OCCUPANCY_MODEL = None
o_SCALER = None
o_LTR_SCALER = None

c_STR_TRAIN_COLS, c_LTR_TRAIN_COLS = None, None
c_COMMON_TRAIN_COLS, c_ALL_TRAIN_COLS, c_ALL_ALLOWED_COLS = None, None, None

#e_RENT_VARS = None
#o_RENT_MAPE_MODEL = None
#o_RENT_SCALER = None


def load_data(**kwargs):
    print("Loading Area Average Features, Keras Models, and Scaler")
    n_start = time()
    
    global e_VARS, e_LTR_VARS
    #with open(utils.get_local_path("model_variables_20210810.json"), "r", encoding="utf-8-sig") as f:
    with open(utils.get_local_path("model_variables_20220207_v2.json"), "r", encoding="utf-8-sig") as f:
        e_VARS = json.load(f)
    e_VARS["bedroom_guest"].update({
        float(k): int(v)
        for k, v in e_VARS["bedroom_guest"].items()
    })
    e_VARS["bedroom_guest"].update({
        int(float(k)): int(v)
        for k, v in e_VARS["bedroom_guest"].items()
    })
    
    with open(utils.get_local_path("model_variables_long_term_rent_20220811.json"), "r", encoding="utf-8-sig") as f:
        e_LTR_VARS = json.load(f)
    e_LTR_VARS["bedroom_guest"].update({
        float(k): int(v)
        for k, v in e_LTR_VARS["bedroom_guest"].items()
    })
    e_LTR_VARS["bedroom_guest"].update({
        int(float(k)): int(v)
        for k, v in e_LTR_VARS["bedroom_guest"].items()
    })
    
    global c_STR_TRAIN_COLS, c_LTR_TRAIN_COLS, c_COMMON_TRAIN_COLS, c_ALL_TRAIN_COLS, c_ALL_ALLOWED_COLS
    c_STR_TRAIN_COLS = set(e_VARS["train_cols"])
    c_LTR_TRAIN_COLS = set(e_LTR_VARS["train_cols"])
    c_COMMON_TRAIN_COLS = c_STR_TRAIN_COLS.intersection(c_LTR_TRAIN_COLS)
    c_ALL_TRAIN_COLS = c_STR_TRAIN_COLS.union(c_LTR_TRAIN_COLS)
    c_ALL_ALLOWED_COLS = set(e_VARS["allowed_cols"]).union(set(e_LTR_VARS["allowed_cols"]))
    
    #global o_MAE_MODEL
    #print("\nMAE_MODEL path:", utils.get_local_path(r"MAE_20220207_v2"))
    #assert os.path.isdir(utils.get_local_path(r"MAE_20220207_v2"))
    #if o_MAE_MODEL is None:
    #    o_MAE_MODEL = models.load_model(utils.get_local_path(r"MAE_20220207_v2"))
    #assert o_MAE_MODEL is not None
    
    global o_MAPE_MODEL
    print("\nMAPE_MODEL path:", utils.get_local_path(r"MAPE_20220207_v2"))
    assert os.path.isdir(utils.get_local_path(r"MAPE_20220207_v2"))
    if o_MAPE_MODEL is None:
        o_MAPE_MODEL = models.load_model(utils.get_local_path(r"MAPE_20220207_v2"))
    assert o_MAPE_MODEL is not None
    
    global o_MAPE_RATE_MODEL
    print("\nMAPE_RATE_MODEL path:", utils.get_local_path(r"MAPE_RATE_20220207_v2"))
    assert os.path.isdir(utils.get_local_path(r"MAPE_RATE_20220207_v2"))
    if o_MAPE_RATE_MODEL is None:
        o_MAPE_RATE_MODEL = models.load_model(utils.get_local_path(r"MAPE_RATE_20220207_v2"))
    assert o_MAPE_RATE_MODEL is not None
    
    #global o_MAPE_RENT_MODEL
    #print("\nMAPE_RENT_MODEL path:", utils.get_local_path(r"MAPE_RENT_20220207_v2"))
    #assert os.path.isdir(utils.get_local_path(r"MAPE_RENT_20220207_v2"))
    #if o_MAPE_RENT_MODEL is None:
    #    o_MAPE_RENT_MODEL = models.load_model(utils.get_local_path(r"MAPE_RENT_20220207_v2"))
    #assert o_MAPE_RENT_MODEL is not None
    
    global o_MAPE_RENT_MODEL
    print("\nMAPE_RENT_MODEL path:", utils.get_local_path(r"MAPE_LONG_TERM_RENT_20220811"))
    assert os.path.isdir(utils.get_local_path(r"MAPE_LONG_TERM_RENT_20220811"))
    if o_MAPE_RENT_MODEL is None:
        o_MAPE_RENT_MODEL = models.load_model(utils.get_local_path(r"MAPE_LONG_TERM_RENT_20220811"))
    assert o_MAPE_RENT_MODEL is not None
    
    global o_SCALER, o_LTR_SCALER
    #o_SCALER = joblib.load(utils.get_local_path("scaler_minmax_20210810.pkl"))
    o_SCALER = joblib.load(utils.get_local_path("scaler_robust_20220207_v2c.pkl"))
    o_LTR_SCALER = joblib.load(utils.get_local_path("scaler_rent_robust_20220811.pkl"))
    
    print("\tLoading Average Average Features and Models and Scaler took:", utils.secondsToStr(time() - n_start))


def add_area_and_defaults(df_input, **kwargs):
    print("\nAdding area average and default values")
    n_start = time()
    
    df_input['records'] = d_PREDICT_RECORDS
    df_input['owner_properties'] = d_PREDICT_OWNER_PROPERTIES
    df_input['overall_rating'] = d_PREDICT_OVERALL_RATING
    df_input['response_rate'] = d_PREDICT_RESPONSE_RATE
    df_input['number_of_photos'] = d_PREDICT_NUMBER_OF_PHOTOS
    df_input['number_of_reviews'] = d_PREDICT_NUMBER_OF_REVIEWS
    
    df_input['availability'] = d_PREDICT_AVAILABILITY
    #df_input['occupancy_rate_ltm'] = df_input[["0_area_occupancy", "1_area_occupancy", "2_area_occupancy"]].mean(axis=1)
    
    #print("density cols", [col for col in df_input.columns if "density" in col or "zcta" in col])
    
    df_input["area_rate_occupancy"] = df_input[["-1_area_rate", "0_area_rate", "1_area_rate", "2_area_rate"]].mean(axis=1).apply(defaults.calc_rate_occupancy)
    
    #if "zcta_population_density" in df_input.columns:
    #    df_input["population_density_occupancy"] = df_input["zcta_population_density"].apply(defaults.calc_population_density_occupancy)
    #else:
    #    df_input["population_density_occupancy"] = df_input["2_area_population_density"].apply(defaults.calc_population_density_occupancy)
        
    #if "zcta_housing_density" in df_input.columns:
    #    df_input["housing_density_occupancy"] = df_input["zcta_housing_density"].apply(defaults.calc_housing_density_occupancy)
    #else:
    #    df_input["housing_density_occupancy"] = df_input["2_area_housing_density"].apply(defaults.calc_housing_density_occupancy)
    
    if "price" in df_input.columns:
        df_input["price_occupancy"] = df_input["price"].apply(defaults.calc_price_occupancy)
    else:
        df_input["price_occupancy"] = df_input["2_area_price"].apply(defaults.calc_price_occupancy)
    
    #df_input['calc_occupancy'] = df_input[["area_rate_occupancy", "population_density_occupancy", "housing_density_occupancy"]].mean(axis=1)
    #df_input['calc_occupancy'] = df_input[["area_rate_occupancy", "price_occupancy"]].mean(axis=1)
    
    df_input['occupancy'] = df_input[["0_area_occupancy", "1_area_occupancy", "2_area_occupancy", "area_rate_occupancy", "price_occupancy"]].mean(axis=1)
    
    df_input['availability_seasonality'] = d_PREDICT_AVAILABILITY_SEASONALITY
    df_input['availability_season_length'] = d_PREDICT_AVAILABILITY_SEASON_LENGTH  #df_input["0_area_availability_season_peak_month"].round(0)
    df_input['availability_season_peak_month'] = d_PREDICT_AVAILABILITY_SEASON_PEAK_MONTH  #df_input["0_area_availability_season_peak_month"].round(0)
    df_input['availability_season_max_month'] = d_PREDICT_AVAILABILITY_SEASON_MAX_MONTH  #df_input["0_area_availability_season_max_month"].round(0)
    df_input['availability_season_type_Month'] = d_PREDICT_AVAILABILITY_SEASON_TYPE_MONTH  #df_input["0_area_availability_season_type_Month"].round(0)
    df_input['availability_season_type_Quarter'] = d_PREDICT_AVAILABILITY_SEASON_TYPE_QUARTER  #df_input["0_area_availability_season_type_Month"].round(0)
    
    #print("\n\n", list(df_input.columns), "\n\n")
    
    df_input['demand_seasonality'] = df_input["0_area_demand_seasonality"]
    df_input['demand_season_length'] = df_input["0_area_demand_season_length"].round(0)
    df_input['demand_season_peak_month'] = df_input["0_area_demand_season_peak_month"].round(0)
    df_input['demand_season_max_month'] = df_input["0_area_demand_season_max_month"].round(0)
    #df_input['demand_season_type_Month'] = df_input["0_area_demand_season_type_Month"].round(0)
    #df_input['demand_season_type_Quarter'] = df_input["0_area_demand_season_type_Month"].round(0)
    
    df_input["max_guests"] = [
        e_VARS["bedroom_guest"].get(i_bedrooms, i_bedrooms * 2) if not pd.isnull(i_bedrooms) else 0
        for i_bedrooms in df_input["bedrooms"]]
    
    for s_amenity in a_ALWAYS_AMENITIES:
        df_input[s_amenity] = 1
    
    for s_service in a_ALWAYS_SERVICES:
        df_input[s_service] = 1
    
    df_input.drop(columns=[col for col in df_input.columns if col.startswith(("lat_sub_", "long_sub_"))], inplace=True)
    df_input, _, _ = airbnb_standardization.subdivide_latitude_longitude_cols(
        df_input,
        e_VARS["gps_min_latitude"],
        e_VARS["gps_max_latitude"],
        e_VARS["gps_min_longitude"],
        e_VARS["gps_max_longitude"],
        lat_col="latitude",
        long_col="longitude",
        n_subdivisions=e_VARS["gps_subdivisions"],)
    
    ae_zillow_areas = [
        prep_zillow.o_AREA_LOOKUPS.find_area(latitude, longitude, i_max_precision=1)
        for idx, latitude, longitude in df_input[["latitude", "longitude"]].itertuples()]
    
    ae_zillow_ltr_areas = [
        prep_zillow.o_LTR_AREA_LOOKUPS.find_area(latitude, longitude, i_max_precision=1)
        for idx, latitude, longitude in df_input[["latitude", "longitude"]].itertuples()]
    
    df_input["area_precision"] = [
        e_area_row.get("area_precision", -3)
        for t_area, e_area_row in ae_zillow_areas]
    
    df_input["area_properties"] = [
        e_area_row.get("area_properties", 0)
        for t_area, e_area_row in ae_zillow_areas]
    
    df_input["area_ltr_precision"] = [
        e_area_row.get("area_ltr_precision", -3)
        for t_area, e_area_row in ae_zillow_ltr_areas]
    
    df_input["area_ltr_properties"] = [
        e_area_row.get("area_ltr_properties", 0)
        for t_area, e_area_row in ae_zillow_ltr_areas]
    
    df_input['demand_seasonality'] = df_input['demand_seasonality'].fillna(df_input['3_area_demand_seasonality'])
    df_input['demand_season_length'] = df_input['demand_season_length'].fillna(df_input['3_area_demand_season_length'])
    df_input['demand_season_peak_month'] = df_input['demand_season_peak_month'].fillna(df_input['3_area_demand_season_peak_month'])
    df_input['demand_season_max_month'] = df_input['demand_season_max_month'].fillna(df_input['3_area_demand_season_max_month'])
    
    #df_input['rent'] = df_input['rent_estimate'].fillna(df_input['3_area_rent'])
    df_input['rent'] = df_input['rent'].fillna(df_input['3_area_rent'])
    df_input['year_built'] = df_input['year_built'].fillna(df_input['3_area_year_built']).clip(lower=1492, upper=datetime.utcnow().year)
    
    df_input["occupancy"] = [
        d_occ ** e_PREDICT_OCCUPANCY_ADJ_EXP.get(i_precision, 1)
        for idx, d_occ, i_precision in df_input[["occupancy", "area_precision"]].itertuples()]
    #df_input["occupancy"] = df_input["occupancy"].apply(lambda x: x ** 0.7)
    
    if "property_condition" in df_input:
        df_input["property_condition"] = df_input["property_condition"].fillna(d_PREDICT_DEFAULT_PROPERTY_CONDITION)
    
    #return df_input
    
    if kwargs.get("area_defaults_all_features", True):
        a_area_cols = c_ALL_TRAIN_COLS - set([col for col in df_input.columns if not col.startswith(("feature_", "location_"))])
        if kwargs.get("b_debug"): print(f"Adding/filling area defaults for:")
        
        for col in a_area_cols:
            if col.startswith("property_type_"):
                continue
                    
            if col in df_input.columns:
                #if col.startswith("property_type_"):
                #    df_input[col] = df_input[col].fillna(0)
                #    continue
                
                if kwargs.get("b_debug"): print(f"\tFill blanks {col}")
                df_input[col] = df_input[col].fillna(pd.Series([
                    (e_area_row.get(col, 0) if e_area_row else 0 + e_ltr_area_row.get(col, 0) if e_ltr_area_row else 0) / 2 if col in c_COMMON_TRAIN_COLS
                    else e_area_row.get(col) if e_area_row else 0 + e_ltr_area_row.get(col, 0) if e_ltr_area_row else 0
                    for ((t_area, e_area_row), (t_ltr_area, e_ltr_area_row)) in zip(ae_zillow_areas, ae_zillow_ltr_areas)], index=df_input.index))
            else:
                if kwargs.get("b_debug"): print(f"\tAdd {col}")
                df_input[col] = [
                    (e_area_row.get(col, 0) if e_area_row else 0 + e_ltr_area_row.get(col, 0) if e_ltr_area_row else 0) / 2 if col in c_COMMON_TRAIN_COLS
                    else e_area_row.get(col) if e_area_row else 0 + e_ltr_area_row.get(col, 0) if e_ltr_area_row else 0
                    for ((t_area, e_area_row), (t_ltr_area, e_ltr_area_row)) in zip(ae_zillow_areas, ae_zillow_ltr_areas)]
    else:
        a_area_cols = c_ALL_TRAIN_COLS - set(df_input.columns)
        if kwargs.get("b_debug"): print(f"Adding area defaults for:")
        for col in a_area_cols:
            if col.startswith("property_type_"):
                #df_input[col] = df_input[col].fillna(0)
                continue
            
            if kwargs.get("b_debug"): print(f"\t{col}")
            
            df_input[col] = [
                (e_area_row.get(col, 0) if e_area_row else 0 + e_ltr_area_row.get(col, 0) if e_ltr_area_row else 0) / 2 if col in c_COMMON_TRAIN_COLS
                else e_area_row.get(col) if e_area_row else 0 + e_ltr_area_row.get(col, 0) if e_ltr_area_row else 0
                for ((t_area, e_area_row), (t_ltr_area, e_ltr_area_row)) in zip(ae_zillow_areas, ae_zillow_ltr_areas)]
            #df_input[col]
    
    print("Missing training cols:", c_ALL_TRAIN_COLS - set(df_input.columns))
    
    print("\tAdding area average and default values took:", utils.secondsToStr(time() - n_start))
    return df_input


@utils.timer
def get_local_datas(df_locales, **kwargs):
    if area_data.o_DB is None: area_data.load_data()
    
    df_states = df_locales[["address_state"]].drop_duplicates().copy()
    df_cities = df_locales[["address_city", "address_state"]].drop_duplicates().copy()
    df_zipcodes = df_locales[["address_zipcode", "address_state"]].drop_duplicates().copy()
    
    a_locales = []
    for idx, s_zipcode, s_state in df_zipcodes.itertuples():
        a_locales.append({"state": s_state, "zipcode": s_zipcode, "market_type": "zipcode", "min_properties": 1})
    #print(len(a_locales))
    
    for idx, s_city, s_state in df_cities.itertuples():
        a_locales.append({"state": s_state, "city": s_city, "market_type": "city", "min_properties": 1})
    #print(len(a_locales))
    
    for idx, s_state in df_states.itertuples():
        a_locales.append({"state": s_state, "market_type": "state", "min_properties": 1})
    
    #print(len(a_locales))
    
    #n_start = time()
    a_markets = area_webhook.process_multiple_locale_batches(a_locales)
    #print(utils.secondsToStr(time() - n_start))

    e_state_results = {}
    e_city_results = {}
    e_zipcode_results = {}
    for e_market in a_markets:
        if not e_market: continue

        e_market = {
            k: v if not isinstance(v, Decimal) else float(v)
            for k, v in e_market.items()}
        
        if e_market.get("market_type") == "state":
            e_state_results[e_market["state"]] = e_market
        elif e_market.get("market_type") == "city":
            e_city_results[(e_market["state"], e_market["city"])] = e_market
        if e_market.get("market_type") == "zipcode":
            e_zipcode_results[(e_market["state"], e_market["zipcode"])] = e_market

        #del e_market["market_type"]
        #del e_market["filter_type"]
        #break
    #print(len(e_state_results) + len(e_city_results) + len(e_zipcode_results))
    #print(len(e_state_results), len(e_city_results), len(e_zipcode_results))
    
    #n_start = time()

    a_local_datas = []
    for row in df_locales.to_dict(orient="records"):
        e_locale_results = {
            "state_info": e_state_results.get(row["address_state"], {}),
            "city_info": e_city_results.get((row["address_state"], row["address_city"]), {}),
            "zipcode_info": e_zipcode_results.get((row["address_state"], row["address_zipcode"]), {}),
        }

        e_local_data = area_data.get_local_data(
            {
                "address_components": {
                    "locality": [row["address_city"]],
                    "state": [row["address_state"]],
                    "postal_code": [row["address_zipcode"]],
                },
            },
            e_locale_results=e_locale_results,
            **kwargs)
        a_local_datas.append(e_locale_results)
        #break
    #print(utils.secondsToStr(time() - n_start))
    
    return a_local_datas


@utils.timer
def get_monthly_datas(df_zillow_output, a_local_datas, **kwargs):
    if area_data.o_DB is None: area_data.load_data()
    #n_start = time()
    
    a_cols = ["address_city", "address_state", "address_zipcode", "latitude", "longitude", "predicted_occupancy_optimized", "avg_rate_at_predicted_revenue"]
    a_monthly_datas = []
    for row, e_ld in zip(
        df_zillow_output[a_cols].to_dict(orient="records"),
        a_local_datas
    ):
        e_monthly_data = area_data.get_monthly_area_data(
            row["latitude"],
            row["longitude"],
            row["predicted_occupancy_optimized"],
            row["avg_rate_at_predicted_revenue"],
            row=["address_state"],
            e_local_data={
                "state": e_ld.get("state_info", {}),
                "city": e_ld.get("city_info", {}),
                "zipcode": e_ld.get("zipcode_info", {}),
            },
            search_closest_zipcodes=False,
            **kwargs)
        a_monthly_datas.append(e_monthly_data)
        #break
    #print(utils.secondsToStr(time() - n_start))
    return a_monthly_datas



@utils.timer
def get_monthly_costs(df_zillow_output, a_monthly_datas, **kwargs):
    #if area_data.o_DB is None: area_data.load_data()
    a_cols = ["address_state", "predicted_occupancy_optimized", "living_area", "lot_size", "bedrooms", "bathrooms", "feature_pool", "feature_jacuzzi", "latitude"]
    
    a_monthly_costs = []
    a_annual_costs, a_annual_long_term_costs = [], []
    for (idx, row), e_md in zip(df_zillow_output[a_cols].iterrows(), a_monthly_datas):
        e_per_month_costs, a_cost_vals = scoring.get_per_month_costs(
            row["address_state"] if row["address_state"] else "_all_",
            #d_revenue=e_predictions["predicted_revenue_optimized"],
            d_occupancy=row["predicted_occupancy_optimized"],
            d_living_area=row["living_area"],
            d_lot_size=row["lot_size"],
            d_bedrooms=row["bedrooms"],
            d_bathrooms=row["bathrooms"],
            d_latitude=row["latitude"],
            a_monthly_occupancy=list(e_md['monthly_occupancy'].values()),
            #b_pool=True if e_input_prepped.get("features", {}).get("pool") in {True, 1, "1", "Yes", "true", "True", "TRUE"} else False
            b_pool=True if row.get("feature_pool") in {True, 1, "1", "Yes", "true", "True", "TRUE"} else False,
            b_hot_tub=True if row.get("feature_jacuzzi") in {True, 1, "1", "Yes", "true", "True", "TRUE"} else False,
            **kwargs
        )
        e_md["monthly_costs"] = {
            #f"cost_month_{i_mon}": round(e_mon_cost["total"] + e_monthly_data.get("monthly_revenue", {}).get(f"revenue_month_{i_mon}", 0) * scoring.d_LISTING_FEE_RATE, 2)
            f"cost_month_{i_mon}": round(e_mon_cost["total"], 2)
            for i_mon, e_mon_cost in e_per_month_costs.items()}
        d_annual_cost = sum(e_md["monthly_costs"].values())
        
        e_annual_costs = Counter()
        for i_mon, e_mon_cost in e_per_month_costs.items():
            for k, v in e_mon_cost.items():
                if isinstance(v, (int, float)):
                    e_annual_costs[k] += v
        
        if "utilities" not in e_annual_costs:
            a_utilities = [e_annual_costs.get(x, 0) for x in scoring.a_UTILITY_COSTS]
            if a_utilities:
                e_annual_costs["utilities"] = sum(a_utilities)
        
        e_annual_costs = {k: round(v, 2) for k, v in e_annual_costs.items()}
        e_avg_monthly_costs = {k: round(v / 12, 2) for k, v in e_annual_costs.items()}
        e_avg_monthly_long_term_costs = {k: round(v / 12, 2) for k, v in e_annual_costs.items() if k in scoring.a_LONG_TERM_COSTS}
        
        d_long_term_costs = round(sum([
            sum([v for k,v in e_mon_cost.items() if k in scoring.a_LONG_TERM_COSTS])
            for i_mon, e_mon_cost in e_per_month_costs.items()]
        ), 2)
        
        #e_predictions["annual_expenses"] = round(d_annual_cost, 2)
        #e_predictions["annual_expenses_long_term"] = round(d_long_term_costs, 2)
        
        a_monthly_costs.append((e_per_month_costs, e_avg_monthly_costs, e_avg_monthly_long_term_costs))
        a_annual_costs.append(d_annual_cost)
        a_annual_long_term_costs.append(d_long_term_costs)
            
        #break
    return a_monthly_costs, a_annual_costs, a_annual_long_term_costs


def predict_zillow_file_revenue(zillow_file, **kwargs):
    print(f"\n######## STANDARIZE AND PREP ZILLOW FILE FOR REVENUE PREDICTIONS ########\n")
    n_start = time()
    if prep_zillow.o_AREA_LOOKUPS is None or prep_zillow.o_HOTELS is None:
        prep_zillow.load_data(**kwargs)
    
    if e_VARS is None or e_LTR_VARS is None or o_MAPE_MODEL is None or o_MAPE_RATE_MODEL is None or o_MAPE_RENT_MODEL:
        load_data()
    
    df_zillow = prep_zillow.read_zillow_file(zillow_file, **kwargs)
    print(f"{df_zillow.shape[0]:,} rows in input zillow file")
    
    if any(col not in df_zillow.columns for col in ['property_type', 'property_category',]):
        df_zillow = prep_zillow.prep_zillow_file(df_zillow, **kwargs)
    else:
        df_zillow = df_zillow.set_index("Property ID")
    #print("Standardized columns:\n", "\n\t".join(list(df_zillow.columns)), "\n")
    
    #print("\nConvert into input to model, add normal area information and defaults")
    for col in ["fixer_upper", "renovated", "condemned", "manufactured_home", "starter_home", "auction", "shared_ownership", "move_home", "timeshare", "lot_only"]:
        if col in df_zillow.columns:
            df_zillow[col] = df_zillow[col].fillna(False)
        else:
            df_zillow[col] = False
    
    df_zillow["auction"] = [
        True if b_auction or d_price < 20000 else False
        for idx, b_auction, d_price in df_zillow[["auction", "price"]].itertuples()]
    
    df_zillow = add_area_and_defaults(df_zillow, **kwargs)
    #if not 'availability_season_peak_month' in df_zillow.columns:
    #    print(list(df_zillow.columns))
    #    raise Exception("missing availability_season_peak_month column")
    
    #print("demand_season_max_month", dict(df_zillow["demand_season_max_month"].value_counts()))
    
    if "property_type" not in df_zillow.columns and "Property Type" not in df_zillow.columns:
        raise Exception("property_type and Property Type not in df")
    
    df_zillow_inputs = prep_zillow.to_input(df_zillow, check_na=False, check_price=True, **kwargs)
    #if not 'availability_season_peak_month' in df_zillow.columns:
    #    print(list(df_zillow.columns))
    #    raise Exception("missing availability_season_peak_month column")
    
    for col in c_ALL_TRAIN_COLS:
        if col.startswith("property_type_"):
            if col not in df_zillow_inputs.columns:
                df_zillow_inputs[col] = 0
            else:
                df_zillow_inputs[col] = df_zillow_inputs[col].fillna(0)
    
    #print("property_type cols 3", [col for col in df_zillow.columns if "property_type" in col])
    
    df_zillow_inputs["occupancy"] = df_zillow_inputs["occupancy"].fillna(d_PREDICT_DEFAULT_OCCUPANCY).round(3)
    df_zillow_inputs["demand_seasonality"] = df_zillow_inputs["demand_seasonality"].fillna(0)
    df_zillow_inputs["demand_season_peak_month"] = df_zillow_inputs["demand_season_peak_month"].fillna(-1)
    df_zillow_inputs["demand_season_max_month"] = df_zillow_inputs["demand_season_max_month"].fillna(-1)
    df_zillow_inputs["demand_season_length"] = df_zillow_inputs["demand_season_length"].fillna(12)
    
    for col in c_ALL_TRAIN_COLS - set(df_zillow_inputs.columns):
        df_zillow_inputs[col] = 0
    
    print("Columns with NA values:", c_ALL_TRAIN_COLS - set(df_zillow_inputs[c_ALL_TRAIN_COLS].dropna(how="any", axis=1).columns))
    if kwargs.get("b_debug"):
        print(df_zillow_inputs.dropna().shape, df_zillow_inputs[c_ALL_TRAIN_COLS].dropna().shape, len(c_ALL_TRAIN_COLS))
    
    #df_raw = df_zillow_inputs[e_VARS["train_cols"]].to_csv(kwargs.get("output_file"), encoding="utf-8-sig")
    #return
    
    print("\nMaking predictions")
    #print("Duplicate columns:", {k: v for k, v in Counter(df_zillow_inputs.columns).items() if v > 1})
    np_str_raw = df_zillow_inputs[e_VARS["train_cols"]].values
    X_str = o_SCALER.transform(np_str_raw)
    
    np_ltr_raw = df_zillow_inputs[e_LTR_VARS["train_cols"]].values
    X_ltr = o_LTR_SCALER.transform(np_ltr_raw)
    
    Yhat_zillow_mape = o_MAPE_MODEL.predict(X_str)
    df_zillow_inputs["predicted_revenue_mape"] = Yhat_zillow_mape
    df_zillow_inputs["predicted_revenue_mape"] = df_zillow_inputs["predicted_revenue_mape"].round(2)
    print("\tpredicted_revenue_mape", round(df_zillow_inputs["predicted_revenue_mape"].mean(), 2))

    Yhat_zillow_mape_rate = o_MAPE_RATE_MODEL.predict(X_str)
    df_zillow_inputs["predicted_avg_rate_mape"] = Yhat_zillow_mape_rate
    df_zillow_inputs["predicted_avg_rate_mape"] = df_zillow_inputs["predicted_avg_rate_mape"].round(2)
    print("\tpredicted_avg_rate_mape", round(df_zillow_inputs["predicted_avg_rate_mape"].mean(), 2))

    Yhat_zillow_mape_long_term_rent = o_MAPE_RENT_MODEL.predict(X_ltr)
    df_zillow_inputs["predicted_rent_mape"] = Yhat_zillow_mape_long_term_rent
    df_zillow_inputs["predicted_rent_mape"] = df_zillow_inputs["predicted_rent_mape"].round(2)
    print("\tpredicted_rent_mape", round(df_zillow_inputs["predicted_rent_mape"].mean(), 2))
    
    df_zillow_inputs["area_occupancy"] = df_zillow_inputs[[f"{i}_area_occupancy" for i in range(-1, 1 + 1)]].mean(axis=1).round(3)
    
    df_zillow_inputs["rate_occupancy"] = df_zillow_inputs["predicted_avg_rate_mape"].apply(defaults.calc_rate_occupancy)
    
    if "price" in df_zillow_inputs.columns:
        df_zillow_inputs["price_occupancy"] = df_zillow_inputs["price"].apply(defaults.calc_price_occupancy)
    else:
        df_zillow_inputs["price_occupancy"] = df_zillow_inputs["2_area_price"].apply(defaults.calc_price_occupancy)
        
    df_zillow_inputs["predicted_occupancy"] = ((
        (
            (df_zillow_inputs["predicted_revenue_mape"] / df_zillow_inputs["predicted_avg_rate_mape"]) / 365
        ) + 
        df_zillow_inputs["rate_occupancy"] +
        #df_zillow_inputs["housing_density_occupancy"]
        #df_zillow_inputs["population_density_occupancy"] +
        df_zillow_inputs["price_occupancy"]
        ) / 3).clip(upper=d_PREDICT_AVAILABILITY)
    
    
    #print(list(df_zillow_inputs.columns))
    df_zillow_inputs["predicted_occupancy"] = [
        d_occ ** e_PREDICT_OCCUPANCY_ADJ_EXP.get(i_precision, 1)
        for idx, d_occ, i_precision, i_properties in df_zillow_inputs[["predicted_occupancy", "area_precision", "area_properties"]].itertuples()]
    
    #df_zillow_inputs["predicted_occupancy_optimized"] = df_zillow_inputs["predicted_occupancy"] ** d_PREDICT_OCCUPANCY_OPT_EXP
    df_zillow_inputs["predicted_occupancy_optimized"] = [
        d_occ ** e_PREDICT_OCCUPANCY_OPT_EXP.get(d_precision, 1)
        for d_occ, d_precision in zip(df_zillow_inputs["predicted_occupancy"], df_zillow_inputs["area_precision"])]
    
    df_zillow_inputs["area_bedrooms"] = (df_zillow_inputs[[f"{i}_area_bedrooms" for i in range(-1, 3 + 1) if f"{i}_area_bedrooms" in df_zillow_inputs.columns]].mean(axis=1)).round(1)
    df_zillow_inputs["area_bathrooms"] = (df_zillow_inputs[[f"{i}_area_bathrooms" for i in range(-1, 3 + 1) if f"{i}_area_bathrooms" in df_zillow_inputs.columns]].mean(axis=1)).round(1)
    df_zillow_inputs["area_occupancy"] = df_zillow_inputs[[f"{i}_area_occupancy" for i in range(-1, 1 + 1) if f"{i}_area_occupancy" in df_zillow_inputs.columns]].mean(axis=1).round(3)
    df_zillow_inputs["area_revenue"] = df_zillow_inputs[[f"{i}_area_revenue" for i in range(-1, 2 + 1) if f"{i}_area_revenue" in df_zillow_inputs.columns]].mean(axis=1).round(2)
    df_zillow_inputs["area_rent"] = df_zillow_inputs[[f"{i}_area_rent" for i in range(-2, 2 + 1) if f"{i}_area_revenue" in df_zillow_inputs.columns]].mean(axis=1).round(2)
    df_zillow_inputs["area_rent_vs_mean"] = df_zillow_inputs["area_rent"] / prep_zillow.o_AREA_LOOKUPS.areas[prep_zillow.o_AREA_LOOKUPS.default_location]["area_rent"]
    df_zillow_inputs["area_vs_mean"] = df_zillow_inputs[[f"{i}_area_rate_vs_mean" for i in range(-1, 1 + 1) if f"{i}_area_rate_vs_mean" in df_zillow_inputs.columns]].mean(axis=1).round(3)
    df_zillow_inputs["area_occupancy_vs_mean"] = round(df_zillow_inputs["area_occupancy"] / prep_zillow.o_AREA_LOOKUPS.areas[prep_zillow.o_AREA_LOOKUPS.default_location]["area_occupancy"], 3)
    #df_zillow_inputs["area_occupancy_vs_mean"] = round(df_zillow_inputs["area_occupancy"] / df_zillow_inputs[f"-3_area_occupancy"], 3)
    df_zillow_inputs["area_rev_per_bedroom"] = df_zillow_inputs[[f"{i}_area_rev_per_bedroom" for i in range(-1, 3 + 1) if f"{i}_area_rev_per_bedroom" in df_zillow_inputs.columns]].mean(axis=1).round(2)
    df_zillow_inputs["area_rev_per_bathroom"] = df_zillow_inputs[[f"{i}_area_rev_per_bathroom" for i in range(-1, 3 + 1) if f"{i}_area_rev_per_bathroom" in df_zillow_inputs.columns]].mean(axis=1).round(2)
    
    df_zillow_inputs["area_avg_rate"] = df_zillow_inputs[[f"{i}_area_rate" for i in range(-1, 3 + 1) if f"{i}_area_rate" in df_zillow_inputs.columns]].mean(axis=1).round(2)
    #print("\n\tarea_avg_rate", round(df_zillow_inputs["area_avg_rate"].mean(), 2))
    df_zillow_inputs["area_rate_per_bedroom"] = df_zillow_inputs[[f"{i}_area_rate_per_bedroom" for i in range(-1, 3 + 1) if f"{i}_area_rate_per_bedroom" in df_zillow_inputs.columns]].mean(axis=1).round(2)
    #print("\tarea_rate_per_bedroom", round(df_zillow_inputs["area_rate_per_bedroom"].mean(), 2))
    df_zillow_inputs["area_rate_per_bathroom"] = df_zillow_inputs[[f"{i}_area_rate_per_bathroom" for i in range(-1, 3 + 1)]].mean(axis=1).round(2)
    #df_zillow_inputs["area_rate_per_bathroom"] = (df_zillow_inputs["area_avg_rate"] / df_zillow_inputs["area_bathrooms"]).round(2)
    #df_zillow_inputs["area_listed_rate"] = df_zillow_inputs[[f"{i}_area_listed_rate" for i in range(1, 3 + 1) if f"{i}_area_listed_rate" in df_zillow_inputs.columns]].mean(axis=1).round(2).apply(lambda x: x if x < 100000 else np.nan)
    #df_zillow_inputs["area_listed_rate_per_bedroom"] = (df_zillow_inputs["area_listed_rate"] / df_zillow_inputs["area_bedrooms"]).round(2)
    
    df_zillow_inputs["area_rev_per_sqft"] = df_zillow_inputs[[f"{i}_area_rev_per_sqft" for i in range(-1, 3 + 1)]].mean(axis=1).round(2)
    df_zillow_inputs["area_price_per_sqft"] = df_zillow_inputs[[f"{i}_area_rev_per_sqft" for i in range(-1, 3 + 1)]].mean(axis=1).round(2)
    df_zillow_inputs["area_cap_sqft"] = (df_zillow_inputs["area_rev_per_sqft"] / df_zillow_inputs["area_rev_per_sqft"]).replace(np.inf, 0).fillna(0)
    
    #print("\tarea_listed_rate", round(df_zillow_inputs["area_listed_rate"].mean(), 2))
    #print("\tarea_listed_rate_per_bedroom", round(df_zillow_inputs["area_listed_rate_per_bedroom"].mean(), 2))
    
    df_zillow_inputs["area_seasonality"] = df_zillow_inputs[[f"{i}_area_demand_seasonality" for i in range(-1, 1 + 1) if f"{i}_area_demand_seasonality" in df_zillow_inputs.columns]].mean(axis=1).round(2)
    df_zillow_inputs["area_seasonality_normalized"] = df_zillow_inputs["area_seasonality"].apply(lambda x: max(0, 0.368 * math.log(x) + 0.9771 if x > 0 else 0))
    
    
    df_zillow_inputs["area_season_length"] = df_zillow_inputs[[f"{i}_area_demand_season_length" for i in range(-1, 1 + 1) if f"{i}_area_demand_season_length" in df_zillow_inputs.columns]].mean(axis=1).round(1)
    df_zillow_inputs["area_season_peak_month"] = df_zillow_inputs[[f"{i}_area_demand_season_peak_month" for i in range(-1, 1 + 1)]].mean(axis=1).round(2)
    df_zillow_inputs["area_season_max_month"] = df_zillow_inputs[[f"{i}_area_demand_season_max_month" for i in range(-1, 1 + 1)]].mean(axis=1).round(2)
    
    df_zillow_inputs["area_season_peak_month_name"] = df_zillow_inputs["area_season_peak_month"].apply(lambda x: utils.e_MONTH_NAMES.get(int(x), "") if not pd.isnull(x) and x > 0 else "")
    df_zillow_inputs["area_season_max_month_name"] = df_zillow_inputs["area_season_max_month"].apply(lambda x: utils.e_MONTH_NAMES.get(int(x), "") if not pd.isnull(x) and x > 0 else "")
    
    #df_zillow_inputs["predicted_revenue_rate"] = (df_zillow_inputs["area_avg_rate"] * (df_zillow_inputs["area_occupancy"] * (365 * d_PREDICT_AVAILABILITY)))
    
    df_zillow_inputs["predicted_revenue_avg_rate"] = (
        (df_zillow_inputs["predicted_avg_rate_mape"] *
        df_zillow_inputs["predicted_occupancy"] * 365)
    ).round(2)
    #df_zillow_inputs["predicted_revenue_listed_rate"] = (
    #    (df_zillow_inputs["predicted_listed_rate_mae"] *
    #    df_zillow_inputs["predicted_occupancy"] * 365)
    #).round(2)
    
    df_zillow_inputs["calc_revenue_avg_rate"] = (
        (((df_zillow_inputs["area_rate_per_bedroom"] * df_zillow_inputs["bedrooms"]) +
         (df_zillow_inputs["area_rate_per_bathroom"] * df_zillow_inputs["bathrooms"])) / 2) *
        (df_zillow_inputs["predicted_occupancy"] * 365)).round(2)
        #(df_zillow_inputs["area_occupancy"] * 365)).round(2)
    
    #df_zillow_inputs["calc_revenue_listed_rate"] = (
    #    (df_zillow_inputs["area_listed_rate_per_bedroom"] * df_zillow_inputs["bedrooms"]) *
    #    (df_zillow_inputs["predicted_occupancy"] * 365)).round(2)
        #(df_zillow_inputs["area_occupancy"] * 365)).round(2)
    
    #df_zillow_inputs["calc_revenue_listed_rate_generic_house"] = (df_zillow_inputs["area_listed_rate"] * (df_zillow_inputs["area_occupancy"] * 365)).round(2)
    
    df_zillow_inputs["calc_revenue_from_price"] = df_zillow_inputs["price"].apply(defaults.predict_price_revenue_median)
    #df_zillow_inputs["calc_revenue_from_price"] *= df_zillow_inputs["area_vs_mean"]
    df_zillow_inputs["calc_revenue_from_price"] = df_zillow_inputs["calc_revenue_from_price"].round(2)
    
    print("\tpredicted_revenue_avg_rate", round(df_zillow_inputs["predicted_revenue_avg_rate"].mean(), 2))
    #print("\tpredicted_revenue_listed_rate", round(df_zillow_inputs["predicted_revenue_listed_rate"].mean(), 2))
    print("\tcalc_revenue_avg_rate", round(df_zillow_inputs["calc_revenue_avg_rate"].mean(), 2))
    #print("\tcalc_revenue_listed_rate", round(df_zillow_inputs["calc_revenue_listed_rate"].mean(), 2))
    
    print("\tcalc_revenue_from_price", round(df_zillow_inputs["calc_revenue_from_price"].mean(), 2))
    
    
    a_sanity_adjustment_avg, a_sanity_adjustment_prod = [], []
    for idx, row in df_zillow_inputs.iterrows():
        d_sanity_adjustment_avg, d_sanity_adjustment_prod = defaults.get_sanity_factors(row)
        a_sanity_adjustment_avg.append(d_sanity_adjustment_avg)
        a_sanity_adjustment_prod.append(d_sanity_adjustment_prod)
    df_zillow_inputs["sanity_adjustment_avg"] = a_sanity_adjustment_avg
    df_zillow_inputs["sanity_adjustment_prod"] = a_sanity_adjustment_prod
    
    df_zillow_inputs["calc_revenue_from_price"] = df_zillow_inputs["calc_revenue_from_price"] * df_zillow_inputs["sanity_adjustment_prod"]
    df_zillow_inputs["calc_revenue_avg_rate"] = df_zillow_inputs["calc_revenue_avg_rate"] * df_zillow_inputs["sanity_adjustment_prod"]
    
    df_zillow_inputs["predicted_revenue"] = df_zillow_inputs[[
        "predicted_revenue_mape",
        "predicted_revenue_mape",
        #"predicted_revenue_mae",
        "predicted_revenue_avg_rate",
        #"predicted_revenue_listed_rate",
        "calc_revenue_from_price",
        "calc_revenue_avg_rate",
        #"calc_revenue_listed_rate",
    ]].mean(axis=1).round()
    
    df_zillow_inputs["predicted_revenue"] = [
        d_occ ** e_PREDICT_OCCUPANCY_ADJ_EXP.get(i_precision, 1)
        for idx, d_occ, i_precision, i_properties in df_zillow_inputs[["predicted_revenue", "area_precision", "area_properties"]].itertuples()]
    
    # use sanity check to clip predictions
    df_zillow_inputs["revenue_sanity_number"] = df_zillow_inputs[[
        "calc_revenue_from_price",
        "calc_revenue_avg_rate",
        #"calc_revenue_listed_rate",
    ]].mean(axis=1)
    
    df_zillow_inputs["revenue_sanity_std"] = df_zillow_inputs["revenue_sanity_number"].apply(defaults.calc_rev_cap_std) * df_zillow_inputs["revenue_sanity_number"]
    
    df_zillow_inputs["predicted_revenue"] = df_zillow_inputs["predicted_revenue"].clip(
        lower=df_zillow_inputs["revenue_sanity_number"] - (df_zillow_inputs["revenue_sanity_std"] * 2),
        upper=df_zillow_inputs["revenue_sanity_number"] + (df_zillow_inputs["revenue_sanity_std"] * 2))
    
    df_zillow_inputs["predicted_revenue"] = [
        d_rev ** 0.95 if d_area_rev < 1000 else
        d_rev ** 0.98 if i_precision < 1 or (i_precision == 0 and i_properties < 10) else
        d_rev
        #for d_rev, d_area_rev in zip(df_zillow_inputs["predicted_revenue"], df_zillow_inputs["area_revenue"])]
        for idx, d_rev, d_area_rev, i_precision, i_properties in df_zillow_inputs[["predicted_revenue", "area_revenue", "area_precision", "area_properties"]].itertuples()]
        
    df_zillow_inputs["avg_rate_at_predicted_revenue"] = (df_zillow_inputs["predicted_revenue"] / (df_zillow_inputs["predicted_occupancy"] * 365)).round(2)
    
    df_zillow_inputs["predicted_revenue_optimized"] = (
        df_zillow_inputs["avg_rate_at_predicted_revenue"] * df_zillow_inputs["predicted_occupancy_optimized"] * 365).round(2)
    
    print("\n\tpredicted_revenue", round(df_zillow_inputs["predicted_revenue"].mean(), 2))
    print("\tpredicted_occupancy", round(df_zillow_inputs["predicted_occupancy"].mean(), 2))
    print("\tavg_rate_at_predicted_revenue", round(df_zillow_inputs["avg_rate_at_predicted_revenue"].mean(), 2))
    
    print("\n\tpredicted_revenue_optimized", round(df_zillow_inputs["predicted_revenue_optimized"].mean(), 2))
    print("\tpredicted_occupancy_optimized", round(df_zillow_inputs["predicted_occupancy_optimized"].mean(), 2))
    
    print("\n\tarea_occupancy", round(df_zillow_inputs["area_occupancy"].mean(), 2), "\n")
    print("\n\tarea_precision", round(df_zillow_inputs["area_precision"].mean(), 2), "\n")
    print("\tarea_seasonality", round(df_zillow_inputs["area_seasonality"].mean(), 2), "\n")
    #"area_demand_season_type",
    #"area_demand_season_length",
    #"area_demand_season_peak_month",
    
    
    if "price" in df_zillow_inputs.columns and "living_area" in df_zillow_inputs.columns:
        df_zillow_inputs["price_per_sqft"] = (df_zillow_inputs["price"] / df_zillow_inputs["living_area"]).replace(np.inf, None).fillna(df_zillow_inputs["area_price_per_sqft"]).round(2)
    
    if "living_area" in df_zillow_inputs.columns:
        df_zillow_inputs["rev_per_sqft"] = (
            df_zillow_inputs["predicted_revenue"] / df_zillow_inputs["living_area"]).replace(np.inf, None).round(2)
        df_zillow_inputs["optimized_rev_per_sqft"] = (
            df_zillow_inputs["predicted_revenue_optimized"] / df_zillow_inputs["living_area"]).replace(np.inf, None).round(2)
        
        if "price_per_sqft" in df_zillow_inputs.columns:
            df_zillow_inputs["cap_sqft"] = (
                df_zillow_inputs["rev_per_sqft"] / df_zillow_inputs["price_per_sqft"]).replace(np.inf, None).round(8)
            df_zillow_inputs["cap_sqft_optimized"] = (
                df_zillow_inputs["rev_per_sqft"] / df_zillow_inputs["optimized_rev_per_sqft"]).replace(np.inf, None).round(8)
    
    
    if "area_scale" not in df_zillow_inputs.columns:
        df_zillow_inputs["area_scale"] = df_zillow_inputs["area_precision"].apply(lambda x: location_analysis.e_PREC_SIZE_IMP.get(x, "nationwide"))
    
    
    if kwargs.get("input_output_file"):
        prep_zillow.write_output(df_zillow_inputs, "Input/Output File", output_file=kwargs.get("input_output_file"))
    
    e_results = {
        "input_with_predictions": df_zillow
    }

    df_zillow_std = df_zillow.reset_index().drop(
        columns=["area_rev_per_bedroom", "area_rev_per_bathroom"], errors="ignore").copy()
        
    for col in a_INPUT_TO_OUTPUT_COLS:
        if col not in df_zillow_inputs.columns:
            print("Missing df_zillow_inputs column:", col)
    
    df_zillow_output = df_zillow_std[[col for col in df_zillow_std.columns if col not in a_INPUT_TO_OUTPUT_COLS or col in {"latitude", "longitude"}]].merge(
        df_zillow_inputs[a_INPUT_TO_OUTPUT_COLS],
        on=["latitude", "longitude"],
        how="left").set_index("Property ID")
    e_results["standardized_with_predictions"] = df_zillow_output
    
    for col in a_INPUT_TO_OUTPUT_COLS:
        if col not in df_zillow_output.columns:
            print("Missing df_zillow_output column:", col)
    
    for col in a_INCLUDE_EXTRACTED_METADATA_COLS:
        if col not in df_zillow_output.columns:
            df_zillow_output[col] = None
    
    if "starter_home" in df_zillow_output.columns:
        df_zillow_output["predicted_revenue"] *= df_zillow_output["starter_home"].apply(lambda x: 1 if not x else 0.75)
        df_zillow_output["predicted_revenue_optimized"] *= df_zillow_output["starter_home"].apply(lambda x: 1 if not x else 0.75)
    
    if "manufactured_home" in df_zillow_output.columns:
        df_zillow_output["predicted_revenue"] *= df_zillow_output["manufactured_home"].apply(lambda x: 1 if not x else 0.6)
        df_zillow_output["predicted_revenue_optimized"] *= df_zillow_output["manufactured_home"].apply(lambda x: 1 if not x else 0.6)
    
    if "condemned" in df_zillow_output.columns:
        df_zillow_output["predicted_revenue"] *= df_zillow_output["condemned"].apply(lambda x: 1 if not x else 0.0)
        df_zillow_output["predicted_revenue_optimized"] *= df_zillow_output["condemned"].apply(lambda x: 1 if not x else 0.0)
    
    df_zillow_output["calc_rent_from_price"] = np.nan
    df_zillow_output["calc_rent_from_lot_size"] = np.nan
    df_zillow_output["calc_rent_from_living_area"] = np.nan
    df_zillow_output["calc_rent_from_bathrooms"] = np.nan
    df_zillow_output["calc_rent_from_bedrooms"] = np.nan
    
    if "price" in df_zillow_output.columns:
        df_zillow_output.insert(
            list(df_zillow_output.columns).index("predicted_revenue") + 1,
            "pred_vs_price",
            [
                rev / price if price > 1000 and rev > 0 else np.nan
                for idx, rev, price in df_zillow_output[["predicted_revenue", "price"]].itertuples()]
        )
        df_zillow_output.insert(
            list(df_zillow_output.columns).index("predicted_revenue_optimized") + 1,
            "pred_vs_price_optimized",
            [
                rev / price if price > 1000 and rev > 0 else np.nan
                for idx, rev, price in df_zillow_output[["predicted_revenue_optimized", "price"]].itertuples()]
        )
    
        df_zillow_output["pred_vs_price"] *= df_zillow_output["fixer_upper"].apply(lambda x: 1 if pd.isnull(x) or not x else 0.45)
        df_zillow_output["predicted_revenue"] *= df_zillow_output["fixer_upper"].apply(lambda x: 1 if pd.isnull(x) or not x else 0.45)
        df_zillow_output["pred_vs_price_optimized"] *= df_zillow_output["fixer_upper"].apply(lambda x: 1 if pd.isnull(x) or not x else 0.45)
        df_zillow_output["predicted_revenue_optimized"] *= df_zillow_output["fixer_upper"].apply(lambda x: 1 if pd.isnull(x) or not x else 0.45)
        
        df_zillow_output["pred_vs_price"] *= df_zillow_output["auction"].apply(lambda x: 1 if pd.isnull(x) or not x else 0)
        df_zillow_output["pred_vs_price"] *= df_zillow_output["shared_ownership"].apply(lambda x: 1 if pd.isnull(x) or not x else 0)
        df_zillow_output["pred_vs_price"] *= df_zillow_output["move_home"].apply(lambda x: 1 if pd.isnull(x) or not x else 0)
        df_zillow_output["pred_vs_price"] *= df_zillow_output["lot_only"].apply(lambda x: 1 if pd.isnull(x) or not x else 0)
        df_zillow_output["pred_vs_price"] *= df_zillow_output["timeshare"].apply(lambda x: 1 if pd.isnull(x) or not x else 0)
        
        print("\tpred_vs_price", round(df_zillow_output["pred_vs_price"].dropna().mean(), 2), "\n")
        
        df_zillow_output["calc_rent_from_price"] = df_zillow_output["area_rent_vs_mean"] * ((df_zillow_output["price"] * 0.006014) + 603.87)
    
    if "bedrooms" in df_zillow_output.columns:
        for col in [
            "pred_vs_price", "predicted_revenue", "avg_rate_at_predicted_revenue", "predicted_occupancy", #"pred_rev_per_bedroom",
            "predicted_revenue_optimized", "predicted_occupancy_optimized",
            "predicted_revenue_mape",
            #"predicted_revenue_mae",
            "predicted_avg_rate_mape", 
            #"predicted_listed_rate_mae",
            "predicted_revenue_avg_rate", #"predicted_revenue_listed_rate",
            "calc_revenue_from_price", "calc_revenue_avg_rate", #"calc_revenue_listed_rate"
        ]:
            
            df_zillow_output[col] = [
                d_rev if i_beds > 0 else np.nan
                for d_rev, i_beds in zip(df_zillow_output[col], df_zillow_output["bedrooms"])]
    
    
        df_zillow_output.insert(
            list(df_zillow_output.columns).index("predicted_revenue") + 2,
            "pred_rev_per_bedroom",
            [
                rev / beds if beds > 0 else rev
                for idx, rev, beds in df_zillow_output[["predicted_revenue", "bedrooms"]].itertuples()]
        )
        
        df_zillow_output["calc_rent_from_bedrooms"] = df_zillow_output["area_rent_vs_mean"] * (874.88 * np.exp(0.4798 * df_zillow_output["bedrooms"].clip(upper=10)))
    
    if "bathrooms" in df_zillow_output.columns:
        df_zillow_output["calc_rent_from_bathrooms"] = df_zillow_output["area_rent_vs_mean"] * (1191.7 * np.exp(0.5114* df_zillow_output["bathrooms"].clip(upper=9)))
    
    if "living_area" in df_zillow_output.columns:
        df_zillow_output["calc_rent_from_living_area"] = df_zillow_output["area_rent_vs_mean"] * (1283.956 * np.exp(0.000538 * df_zillow_output["living_area"].clip(upper=5750)))
    
    if "lot_size" in df_zillow_output.columns:
        df_zillow_output["calc_rent_from_lot_size"] = df_zillow_output["area_rent_vs_mean"] * (6912.6 * df_zillow_output["lot_size"].clip(upper=0.75) + 1717.1)
    
    df_zillow_output["calculated_rent"] = df_zillow_output[[
        "calc_rent_from_price",
        "calc_rent_from_bathrooms",
        "calc_rent_from_living_area",
        "calc_rent_from_lot_size",
        "calc_rent_from_bedrooms",
    ]].mean(axis=1).round(2)
    
    df_zillow_inputs["calculated_rent"] = df_zillow_inputs["calculated_rent"] * df_zillow_inputs["sanity_adjustment_prod"]
    
    df_zillow_output["predicted_rent"] = df_zillow_output[[
        "predicted_rent_mape",
        "predicted_rent_mape",
        "calculated_rent",
    ]].mean(axis=1).round()
    
    df_zillow_output["predicted_rent_annual"] = df_zillow_output["predicted_rent"] * 12
    
    #e_duplicate_output_cols = {k: i for k, i in Counter(df_zillow_output.columns).items() if i > 1}
    #if e_duplicate_output_cols: print("Duplicated output cols: ", e_duplicate_output_cols)
    
    
    ##### #####
    
    #df_locales = df_zillow[["address_city", "address_state", "address_zipcode"]]
    
    a_local_datas = get_local_datas(df_zillow[["address_city", "address_state", "address_zipcode"]], **kwargs)
    assert len(a_local_datas) == df_zillow.shape[0]
    e_results["local_datas"] = a_local_datas
    
    a_monthly_datas = get_monthly_datas(df_zillow_output, a_local_datas, **kwargs)
    assert len(a_monthly_datas) == df_zillow_output.shape[0]
    e_results["monthly_datas"] = a_monthly_datas
    
    #(e_per_month_costs, e_avg_monthly_costs, e_avg_monthly_long_term_costs)
    a_monthly_costs, a_annual_costs, a_annual_long_term_costs = get_monthly_costs(df_zillow_output, a_monthly_datas, **kwargs)
    assert len(a_monthly_costs) == df_zillow_output.shape[0]
    assert len(a_annual_costs) == df_zillow_output.shape[0]
    assert len(a_annual_long_term_costs) == df_zillow_output.shape[0]
    
    e_results["monthly_costs"] = a_monthly_costs
    
    df_zillow_output["annual_expenses"] = a_annual_costs
    df_zillow_output["annual_expenses_long_term"] = a_annual_long_term_costs
    
    ##### #####
    
    
    
    if kwargs.get("output_file"):
        prep_zillow.write_output(df_zillow_output, "Output File", output_file=kwargs.get("output_file"))

    if "original_file" in kwargs:
        df_zillow_orig = prep_zillow.read_zillow_file(kwargs["original_file"], **kwargs).set_index("zpid")
        
        a_added_output_cols = a_PREDICTION_OUTPUT_COLS[:]
        
        a_added_output_cols.extend([col for col in a_INCLUDE_EXTRACTED_METADATA_COLS if col in df_zillow_output.columns])
        a_missing_output = [col for col in a_added_output_cols if col not in df_zillow_output.columns]
        if a_missing_output:
            print("Missing output cols:", a_missing_output)
        
        e_duplicate_output_cols = {k: i for k, i in Counter(df_zillow_output.columns).items() if i > 1}
        if e_duplicate_output_cols: print("Duplicated output cols: ", e_duplicate_output_cols)
        
        df_zillow_orig_with_output = df_zillow_orig[[col for col in df_zillow_orig.columns if col not in a_added_output_cols]].join(
            df_zillow_output[a_added_output_cols]).reindex(
                ["price"] +
                a_added_output_cols +
                [col for col in df_zillow_orig.columns if col not in ["price"] + a_added_output_cols],
                axis=1)
        
        #if "price" in df_zillow_orig_with_output.columns:
        #    df_zillow_orig_with_output.insert(
        #        list(df_zillow_orig_with_output.columns).index("predicted_revenue") + 1,
        #        "pred_vs_price",
        #        df_zillow_orig_with_output["predicted_revenue"] / df_zillow_orig_with_output["price"])
        
        df_zillow_orig_with_output = df_zillow_orig_with_output.reset_index().rename(columns={"index":"zpid"})
        
        e_results["original_with_predictions"] = df_zillow_orig_with_output
    
        if kwargs.get("original_output_file"):
            prep_zillow.write_output(df_zillow_orig_with_output, "Original + Output", output_file=kwargs.get("original_output_file"), index=False)
        
     #, df_zillow_with_predictions, df_output

    print("\n\tProcessing took:", utils.secondsToStr(time() - n_start))
    return e_results #, df_zillow_output


"""
    This gets run when predict_zillow.py is run from the command prompt
    Args:
        args: {list} of the positional command line options.
            [0] is the full or relative path to the Zillow File
        kwargs:
            --output_file:"c:/path/to/output.csv"  path to write the resulting output to
"""
if __name__ == "__main__":
    args, kwargs = utils.parse_command_line_args(sys.argv)
    
    if not args or len(args) < 1:
        raise Exception('Zillow spreadsheet not specified.')
        
    print('args', args, '\n')
    
    s_zillow = utils.check_path(
        args[0],
        s_default_folder=os.getcwd(),
        required_existence=True,
        required_file=True,
        required_extension=('.xlsx', '.csv', '.json'),
        s_name="Zillow Property File")
    
    if not kwargs.get("output_file"):
        s_path, s_ext = os.path.splitext(s_zillow)
        kwargs["output_file"] = utils.get_new_write_filename(f"{s_path}_results{s_ext}")
    
    if kwargs.get("original_file") and not kwargs.get("original_output_file"):
        s_path, s_ext = os.path.splitext(kwargs.get("original_file"))
        kwargs["original_output_file"] = utils.get_new_write_filename(f"{s_path}_results{s_ext}")
        #kwargs["original_output_file"] = utils.get_new_write_filename(f"{s_path}_results{s_ext}")
    
    print('kwargs', kwargs, '\n')

    #if kwargs.get("occupancy_rate"):
    #    global d_PREDICT_OCCUPANCY_RATE_LTM
    #    d_PREDICT_OCCUPANCY_RATE_LTM = kwargs["occupancy_rate"]
    #    print("d_PREDICT_OCCUPANCY_RATE_LTM =", d_PREDICT_OCCUPANCY_RATE_LTM)
    if kwargs.get("availability"):
        #global d_PREDICT_AVAILABILITY
        d_PREDICT_AVAILABILITY = kwargs["availability"]
        print("d_PREDICT_AVAILABILITY =", d_PREDICT_AVAILABILITY)
    if kwargs.get("owner_properties"):
        d_predict_owner_properties = kwargs["owner_properties"]
        print("d_PREDICT_OWNER_PROPERTIES =", d_PREDICT_OWNER_PROPERTIES)
    if kwargs.get("overall_rating"):
        #global d_PREDICT_OVERALL_RATING
        d_PREDICT_OVERALL_RATING = kwargs["overall_rating"]
        print("d_PREDICT_OVERALL_RATING =", d_PREDICT_OVERALL_RATING)
    if kwargs.get("response_rate"):
        #global d_PREDICT_RESPONSE_RATE
        d_PREDICT_RESPONSE_RATE = kwargs["response_rate"]
        print("d_PREDICT_RESPONSE_RATE =", d_PREDICT_RESPONSE_RATE)
    if kwargs.get("number_of_photos"):
        #global d_PREDICT_NUMBER_OF_PHOTOS
        d_PREDICT_NUMBER_OF_PHOTOS = kwargs["number_of_photos"]
        print("d_PREDICT_NUMBER_OF_PHOTOS =", d_PREDICT_NUMBER_OF_PHOTOS)
    if kwargs.get("number_of_reviews"):
        #global d_PREDICT_NUMBER_OF_REVIEWS
        d_PREDICT_NUMBER_OF_REVIEWS = kwargs["number_of_reviews"]
        print("d_PREDICT_NUMBER_OF_REVIEWS =", d_PREDICT_NUMBER_OF_REVIEWS)
    
    #if kwargs.get("occupancy_adj_exp"):
    #    d_PREDICT_OCCUPANCY_ADJ_EXP = kwargs["occupancy_adj_exp"]
    #    print("d_PREDICT_OCCUPANCY_ADJ_EXP =", d_PREDICT_OCCUPANCY_ADJ_EXP)
    #if kwargs.get("occupancy_opt_exp"):
    #    d_PREDICT_OCCUPANCY_OPT_EXP = kwargs["occupancy_opt_exp"]
    #    print("d_PREDICT_OCCUPANCY_OPT_EXP =", d_PREDICT_OCCUPANCY_OPT_EXP)
    
    if kwargs.get("availability_seasonality"):
        d_PREDICT_AVAILABILITY_SEASONALITY = kwargs["availability_seasonality"]
        print("d_PREDICT_AVAILABILITY_SEASONALITY =", d_PREDICT_AVAILABILITY_SEASONALITY)
    
    e_results = predict_zillow_file_revenue(s_zillow, **kwargs)
