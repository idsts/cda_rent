﻿import os, sys, re

try:
    from . import utils, webhook_utils, db_tables, state_property_tax, us_states, scoring
except:
    import utils, webhook_utils, db_tables, state_property_tax, us_states, scoring


if not  db_tables.e_CONFIG.get("financing_default_apr"):
    db_tables.load_config()

d_DEFAULT_APR = db_tables.e_CONFIG.get("financing_default_apr", 0.05)
d_DEFAULT_LOAN_YEARS = db_tables.e_CONFIG.get("financing_loan_term", 30)
d_DEFAULT_DOWN_PAYMENT = db_tables.e_CONFIG.get("financing_down_payment", 0.2)
d_AVG_INSURANCE = db_tables.e_CONFIG.get("scoring_avg_insurance", 1800)
d_AVG_SHORT_TERM_INSURANCE_MULTIPLIER = db_tables.e_CONFIG.get("scoring_short_term_insurance_multiplier", 1.5)


class MortgageCalculator:
    def __init__(self, principal, apr=d_DEFAULT_APR, loan_years=d_DEFAULT_LOAN_YEARS, **kwargs):
        self.yearly_percentage_rate = apr
        self.loan_term = loan_years
        self.principal = principal
        self.monthly_percentage_rate = self.yearly_percentage_rate / 12
        self.number_monthly_payments = self.loan_term * 12
    
    def calculate_monthly_payment(self):
        # Calculate monthly payment
        if self.monthly_percentage_rate != 0:
            monthly_payment = ((self.monthly_percentage_rate * self.principal) / (1 - (1 + self.monthly_percentage_rate) ** -self.number_monthly_payments))
        else:
            monthly_payment = self.principal / self.number_monthly_payments

        return round(monthly_payment, 2)

    def calculate_debt_schedule(self):
        # Shows the amount owed every month
        e_month_payments = {}
        for i in range(self.number_monthly_payments):
            p = self.principal
            x = (1 + self.monthly_percentage_rate)
            # Polynomial of x
            pn_x = ((x ** i - 1) / (x - 1))
            # Monthly payment
            c = ((self.monthly_percentage_rate * self.principal) / (1 - (1 + self.monthly_percentage_rate) ** -self.number_monthly_payments))
            # Calculates the amount owed
            amount_owed = x ** i * p - pn_x * c
            #print("Amount owed at month {} = {:.2f}".format(i + 1, amount_owed))
            e_month_payments[i + 1] = round(amount_owed, 2)
            i += 1
        return e_month_payments


def calc_finances(annual_revenue, price, **kwargs):
    global d_DEFAULT_APR, d_DEFAULT_LOAN_YEARS, d_DEFAULT_DOWN_PAYMENT, d_AVG_INSURANCE, d_AVG_SHORT_TERM_INSURANCE_MULTIPLIER
    
    if not d_DEFAULT_APR:
        db_tables.load_config()
        
        d_DEFAULT_APR = db_tables.e_CONFIG.get("financing_default_apr", 0.05)
        d_DEFAULT_LOAN_YEARS = db_tables.e_CONFIG.get("financing_loan_term", 30)
        d_DEFAULT_DOWN_PAYMENT = db_tables.e_CONFIG.get("financing_down_payment", 0.2)
        d_AVG_INSURANCE = db_tables.e_CONFIG.get("scoring_avg_insurance", 1800)
        d_AVG_SHORT_TERM_INSURANCE_MULTIPLIER = db_tables.e_CONFIG.get("scoring_short_term_insurance_multiplier", 1.5)
    
    #short_term_factor = kwargs.get("short_term_factor", 0.30)
    #long_term_factor = kwargs.get("long_term_factor", 0.05)
    expense_estimate_factor = kwargs.get("expense_factor", 0.30)
    #long_term_factor = kwargs.get("long_term_factor", 0.05)
    
    s_state = kwargs.get("state", "").lower()
    if kwargs.get("state", "").lower() in us_states.e_STATE_ABBREV:
        s_state = us_states.e_STATE_ABBREV.lower()
    
    principal = kwargs.get("principal", 0)
    down_payment = kwargs.get("down_payment", 0)
    if 0 < down_payment <= 1:
        if price:
            down_payment = down_payment * price
        elif principal:
            down_payment = principal / (1 - down_payment)
    
    if principal and down_payment and not price:
        price = principal + down_payment
    
    if not down_payment and principal:
        down_payment = price - principle
    
    if not principal and down_payment:
        principal = price - down_payment
    
    if not principal and not down_payment:
        down_payment = price * d_DEFAULT_DOWN_PAYMENT
        principal = price - down_payment
    
    if "principal" not in kwargs:
        kwargs["principal"] = principal
    if "down_payment" not in kwargs:
        kwargs["down_payment"] = round(down_payment, 4)
    
    d_down_payment_percent = down_payment / price if price else 0
    
    apr = kwargs.get("apr", d_DEFAULT_APR)
    if not apr: apr = d_DEFAULT_APR
    kwargs["apr"] = apr
    
    if kwargs.get("property_tax_rate"):
        #print("financing property_tax_rate", kwargs.get("property_tax_rate"))
        if kwargs["property_tax_rate"] > 100:
            annual_property_tax = kwargs["property_tax_rate"]
            property_tax_rate = annual_property_tax / price if price > 0 else state_property_tax.e_MEDIAN_PROPERTY_TAX.get(s_state, 0.0095)
        elif kwargs["property_tax_rate"] >= .1:
            property_tax_rate = kwargs["property_tax_rate"] / 100
            annual_property_tax = property_tax_rate * price
        else:
            property_tax_rate = kwargs["property_tax_rate"]
            annual_property_tax = property_tax_rate * price
    elif s_state:
        property_tax_rate = state_property_tax.e_MEDIAN_PROPERTY_TAX.get(s_state, 0.0095)
        #print("financing s_state", s_state)
        annual_property_tax = property_tax_rate * price
    else:
        #print("financing national", 0.0095)
        property_tax_rate = 0.0095
        annual_property_tax = property_tax_rate * price
    
    monthly_income = annual_revenue / 12
    
    o_calc = MortgageCalculator(**kwargs)
    if o_calc.principal > 0 and o_calc.yearly_percentage_rate > 0 and o_calc.loan_term > 0:
        monthly_payment = o_calc.calculate_monthly_payment()
    else:
        monthly_payment = 0
    
    d_short_term_insurance = None
    d_homeowners_insurance = None
    d_utilities = 0
    d_cleaning = 0
    d_management_fee_rate = kwargs.get("management_fee_rate", None)
    d_listing_fee_rate = kwargs.get("listing_fee_rate", None)
    
    d_management_fee = 0
    d_listing_fee = 0
    
    if kwargs.get("monthly_expenses"):
        if isinstance(kwargs.get("monthly_expenses"), dict):
            if kwargs["monthly_expenses"].get("total"):
                d_monthly_expense = kwargs["monthly_expenses"]["total"]
            else:
                d_monthly_expense = sum([
                    x for x in kwargs["monthly_expenses"].values()
                    if x not in scoring.a_TOTAL_COLS_COSTS and isinstance(x, (int, float))])
        
            d_short_term_insurance = kwargs["monthly_expenses"].get("short_rental_insurance", 0)
            d_homeowners_insurance = kwargs["monthly_expenses"].get("homeowners_insurance", 0)
            d_utilities = kwargs["monthly_expenses"].get("utilities", 0)
            if not isinstance(d_utilities, (int, float)):
                a_utilities = [kwargs["monthly_expenses"].get(x, 0) for x in scoring.a_UTILITY_COSTS]
                d_utilities = sum(a_utilities) if a_utilities else 0
            d_cleaning = kwargs["monthly_expenses"].get("cleaning", 0)
            d_management_fee = kwargs["monthly_expenses"].get("management_fee", 0)
            d_listing_fee = kwargs["monthly_expenses"].get("listing_fee", 0)
        else:
            d_monthly_expense = kwargs["monthly_expenses"]
            
        d_annual_expense = d_monthly_expense * 12
    elif kwargs.get("annual_expenses"):
        if isinstance(kwargs.get("annual_expenses"), dict):
            if kwargs["annual_expenses"].get("total"):
                d_annual_expense = kwargs["annual_expenses"]["total"]
            else:
                d_annual_expense = sum([
                    x for x in kwargs["annual_expenses"].values()
                    if x not in scoring.a_TOTAL_COLS_COSTS and isinstance(x, (int, float))])
            
            d_short_term_insurance = kwargs["annual_expenses"].get("short_rental_insurance", 0)
            d_homeowners_insurance = kwargs["annual_expenses"].get("homeowners_insurance", 0)
            if d_short_term_insurance: d_short_term_insurance = round(d_short_term_insurance / 12, 2)
            if d_homeowners_insurance: d_homeowners_insurance = round(d_homeowners_insurance / 12, 2)
            
            d_utilities = kwargs["annual_expenses"].get("utilities", None)
            if not isinstance(d_utilities, (int, float)):
                a_utilities = [kwargs["annual_expenses"].get(x, 0) for x in scoring.a_UTILITY_COSTS]
                d_utilities = sum(a_utilities) if a_utilities else 0
            
            if d_utilities: d_utilities = round(d_utilities / 12, 2)
            
            d_cleaning = kwargs["annual_expenses"].get("cleaning", 0)
            if d_cleaning: d_cleaning = round(d_cleaning / 12, 2)
            
            d_management_fee = kwargs["annual_expenses"].get("management_fee", 0)
            if d_management_fee: d_management_fee = round(d_management_fee / 12, 2)
            
            d_listing_fee = kwargs["annual_expenses"].get("listing_fee", 0)
            if d_listing_fee: d_listing_fee = round(d_listing_fee / 12, 2)
        
        else:
            d_annual_expense = kwargs["annual_expenses"]
        d_monthly_expense = round(kwargs["annual_expenses"] / 12, 2)
    else:
        d_annual_expense = round(annual_revenue * expense_estimate_factor + annual_property_tax, 2)
        d_monthly_expense = round(d_annual_expense / 12, 2)
    
    if isinstance(d_management_fee_rate, str) and re.search("\d", d_management_fee_rate):
        d_management_fee_rate = utils.to_float(d_management_fee_rate)
    
    if isinstance(d_management_fee_rate, (int, float)):
        if d_management_fee_rate > 1:
            d_management_fee_rate /= 100
        d_management_fee = round((d_management_fee_rate * annual_revenue) / 12, 2)
    
    elif isinstance(d_management_fee, (int, float)):
        d_management_fee_rate = (d_management_fee * 12) / annual_revenue if annual_revenue else db_tables.e_CONFIG.get("cost_management_fee_rate", 0.2)
        
    else:
        d_management_fee_rate = db_tables.e_CONFIG.get("cost_management_fee_rate", 0.2)
        d_management_fee = round((d_management_fee_rate * annual_revenue) / 12, 2)
    
    if isinstance(d_listing_fee_rate, str) and re.search("\d", d_listing_fee_rate):
        d_listing_fee_rate = utils.to_float(d_listing_fee_rate)
    
    if isinstance(d_listing_fee_rate, (int, float)):
        if d_listing_fee_rate > 1:
            d_listing_fee_rate /= 100
        d_listing_fee = round((d_listing_fee_rate * annual_revenue) / 12, 2)
    
    elif isinstance(d_listing_fee, (int, float)):
        d_listing_fee_rate = (d_listing_fee * 12) / annual_revenue if annual_revenue else db_tables.e_CONFIG.get("cost_listing_fee_rate", 0.2)
        
    else:
        d_listing_fee_rate = db_tables.e_CONFIG.get("cost_listing_fee_rate", 0.2)
        d_listing_fee = round((d_listing_fee_rate * annual_revenue) / 12, 2)
    
    if not isinstance(d_short_term_insurance, (int, float)) or not isinstance(d_homeowners_insurance, (int, float)):
    
        d_avg_insurance = db_tables.e_CONFIG.get("scoring_avg_insurance", d_AVG_INSURANCE)
        d_state_insurance = us_states.e_STATE_INSURANCE.get(s_state, d_avg_insurance)
        
        d_short_term_insurance_multiplier = db_tables.e_CONFIG.get("scoring_short_term_insurance_multiplier", d_AVG_SHORT_TERM_INSURANCE_MULTIPLIER)
        if not isinstance(d_short_term_insurance, (int, float)):
            d_short_term_insurance = (((d_state_insurance + d_avg_insurance) / 2) * d_short_term_insurance_multiplier) / 12
        if not isinstance(d_homeowners_insurance, (int, float)):
            d_homeowners_insurance = d_state_insurance / 12
    
    e_calc = {
        "annual_revenue": round(annual_revenue, 2),
        "price": round(price, 2),
        "principle": round(principal, 2),
        "down_payment": round(down_payment, 2),
        "down_payment_percent": round(d_down_payment_percent, 8),
        "apr": round(o_calc.yearly_percentage_rate, 8),
        "loan_years": round(o_calc.loan_term, 2),
        
        "gross_cap_rate": round(annual_revenue / price, 8) if price else 0.0,
        "utilities": round(d_utilities * 12, 2),
        "short_rental_insurance": round(d_short_term_insurance * 12, 2),
        "homeowners_insurance": round(d_homeowners_insurance * 12, 2),
        "property_tax_rate": round(property_tax_rate, 8),
        "property_tax": round(annual_property_tax, 2),
        "management_fee_rate": round(d_management_fee_rate, 4),
        "listing_fee_rate": round(d_listing_fee_rate, 4),
        
        "monthly_income": round(monthly_income, 2),
        "monthly_expenses": round(d_monthly_expense, 2),
        "monthly_short_rental_insurance": round(d_short_term_insurance, 2),
        "monthly_homeowners_insurance": round(d_homeowners_insurance, 2),
        "monthly_management_fee": round(d_management_fee, 2),
        "monthly_utilities": round(d_utilities, 2),
        "monthly_cleaning": round(d_cleaning, 2),
        "monthly_property_tax": round(annual_property_tax / 12, 2),
        "monthly_listing_fee": round(d_listing_fee, 2),
        
        "non_financed": {
            "net_cap_rate": round((annual_revenue - d_annual_expense) / price, 8) if price else 0.0,
            "monthly_expenses": round(d_monthly_expense, 2),
        },
    }
    if o_calc.principal > 0 and o_calc.yearly_percentage_rate > 0 and o_calc.loan_term > 0:
        e_calc["financed"] = {
            "net_cap_rate": round((annual_revenue - d_annual_expense) / price, 8) if price else 0.0,
            "monthly_expenses": round(d_monthly_expense, 2),
            "monthly_payment": round(monthly_payment, 2),
        }
    
    
        e_calc["financed"]["monthly_net_cash_flow"] = round(monthly_income - e_calc["financed"]["monthly_expenses"] - monthly_payment, 2)
        e_calc["financed"]["net_operating_income"] = round(e_calc["financed"]["monthly_net_cash_flow"] * 12, 2)
        e_calc["financed"]["cash_on_cash_return"] = round(e_calc["financed"]["net_operating_income"] / kwargs["down_payment"], 2) if kwargs.get("down_payment") else 0
        e_calc["financed"]["dscr"] =  round((e_calc["financed"]["net_operating_income"] / (monthly_payment * 12)), 8) if monthly_payment else 0

    #e_calc["financed_long"]["monthly_net_cash_flow"] = round(monthly_income - e_calc["financed_long"]["monthly_expenses"] - monthly_payment, 2)
    #e_calc["financed_long"]["net_operating_income"] = round(e_calc["financed_long"]["monthly_net_cash_flow"] * 12, 2)
    #e_calc["financed_long"]["cash_on_cash_return"] = round(e_calc["financed_long"]["net_operating_income"] / kwargs["down_payment"], 2) if kwargs.get("down_payment") else 0
    #e_calc["financed_long"]["dscr"] =  (e_calc["financed_long"]["net_operating_income"] / (monthly_payment * 12)) if monthly_payment else 0

    e_calc["non_financed"]["monthly_net_cash_flow"] = round(monthly_income - e_calc["non_financed"]["monthly_expenses"], 2)
    e_calc["non_financed"]["net_operating_income"] = round((e_calc["non_financed"]["monthly_net_cash_flow"] * 12), 2)
    e_calc["non_financed"]["cash_on_cash_return"] = round(e_calc["non_financed"]["net_operating_income"] / price, 8) if price else 0.0
    
    #e_calc["non_financed_long"]["monthly_net_cash_flow"] = round(monthly_income - e_calc["non_financed_long"]["monthly_expenses"], 2)
    #e_calc["non_financed_long"]["net_operating_income"] = round((e_calc["non_financed_long"]["monthly_net_cash_flow"] * 12), 2)
    #e_calc["non_financed_long"]["cash_on_cash_return"] = round(e_calc["non_financed_long"]["net_operating_income"] / price, 8) if price else 0.0
    
    return e_calc
