﻿import re
from collections import Counter, namedtuple, defaultdict
from time import time
import pandas as pd
import numpy as np
import scipy
from scipy import spatial


try:
    from . import airbnb_standardization, etl, utils
    from .airbnb_standardization import *
except ImportError:
    import airbnb_standardization, etl, utils
    from airbnb_standardization import *

i_CLOSEST = 3
i_HOTEL_PRECISION = 0

#a_OFFSETS = [
#    (-1, -1), # northwest
#    (-1, 0), #north
#    (-1, 1), #northeast
#    (0, -1), #west
#    (0, 0), # same area
#    (0, 1), #east
#    (1, -1), #southwest
#    (1, 0), #south
#    (1, 1), #southeast
#    
#    (-2, 0), # west2
#    (0, -2), # north2
#    (2, 0), # east2
#    (0, 2), # south2
#]
a_OFFSETS = [
                      (-3,-1), (-3,+0), (-3,+1),
             (-2,-2), (-2,-1), (-2,+0), (-2,+1), (-2,+2),
    (-1,-3), (-1,-2), (-1,-1), (-1,+0), (-1,+1), (-1,+2), (-1,+3),
    (+0,-3), (+0,-2), (+0,-1), (+0,+0), (+0,+1), (+0,+2), (+0,+3),
    (+1,-3), (+1,-2), (+1,-1), (+1,+0), (+1,+1), (+1,+2), (+1,+3),
             (+2,-2), (+2,-1), (+2,+0), (+2,+1), (+2,+2),
                      (+3,-1), (+3,+0), (+3,+1),
]
#a_OFFSETS = [
#                                       (-4,+0),
#                     (-3,-2), (-3,-1), (-3,+0), (-3,+1), (-3,+2),
#            (-2,-3), (-2,-2), (-2,-1), (-2,+0), (-2,+1), (-2,+2), (-2,+3),
#            (-1,-3), (-1,-2), (-1,-1), (-1,+0), (-1,+1), (-1,+2), (-1,+3),
#    (0,-4), (+0,-3), (+0,-2), (+0,-1), (+0,+0), (+0,+1), (+0,+2), (+0,+3), (0,+4),
#            (+1,-3), (+1,-2), (+1,-1), (+1,+0), (+1,+1), (+1,+2), (+1,+3),
#            (+2,-3), (+2,-2), (+2,-1), (+2,+0), (+2,+1), (+2,+2), (+2,+3),
#                     (+3,-2), (+3,-1), (+3,+0), (+3,+1), (+3,+2),
#                                       (+4,+0),
#]
#a_OFFSETS = [
#    (-5,-1), (-5,0), (-5,1),
#    (-4,-3), (-4,-2), (-4,-1), (-4,0), (-4,1), (-4,2), (-4,3),
#    (-3,-4), (-3,-3), (-3,-2), (-3,-1), (-3,0), (-3,1), (-3,2), (-3,3), (-3,4),
#    (-2,-4), (-2,-3), (-2,-2), (-2,-1), (-2,0), (-2,1), (-2,2), (-2,3), (-2,4),
#    (-1,-5), (-1,-4), (-1,-3), (-1,-2), (-1,-1), (-1,0), (-1,1), (-1,2), (-1,3), (-1,4), (-1,5),
#    (0,-5), (0,-4), (0,-3), (0,-2), (0,-1), (0,0), (0,1), (0,2), (0,3), (0,4), (0,5),
#    (1,-5), (1,-4), (1,-3), (1,-2), (1,-1), (1,0), (1,1), (1,2), (1,3), (1,4), (1,5),
#    (2,-4), (2,-3), (2,-2), (2,-1), (2,0), (2,1), (2,2), (2,3), (2,4),
#    (3,-4), (3,-3), (3,-2), (3,-1), (3,0), (3,1), (3,2), (3,3), (3,4),
#    (4,-3), (4,-2), (4,-1), (4,0), (4,1), (4,2), (4,3),
#    (5,-1), (5,0), (5,1),
#]


class HotelProximity():
    def __init__(self, s_hotel_csv, **kwargs):
        ''' Process the hotels from an AirBnB/Homeaway extended file into area groups '''
        
        self._precision = kwargs["precision"] if kwargs.get("precision") is not None else i_HOTEL_PRECISION
        self._offsets = kwargs["offsets"] if kwargs.get("offsets") else a_OFFSETS
        
        if not s_hotel_csv:
            s_hotel_csv = utils.get_local_path("hotels_20210616.csv.xz")
        
        df_hotels = pd.read_csv(s_hotel_csv, encoding="utf-8-sig", dtype=etl.e_EXTENDED_COL_DTYPES).drop_duplicates(["Latitude", "Longitude"])
        
        #print(df_hotels.shape)
        #df_hotels = df_hotels[df_hotels["Property Type"].isin(["Hotel", "Motel"])].dropna(subset=['Published Nightly Rate (USD)', 'Occupancy Rate LTM'])
        df_hotels = df_hotels[df_hotels["Property Type"].isin(["Hotel", "Motel"])].dropna(subset=['Published Nightly Rate (USD)'])
        df_hotels = df_hotels[df_hotels['Published Nightly Rate (USD)'] > 0]
        print(f"{df_hotels.shape[0]:,} hotels")
        
        df_hotels["lat_area"] = df_hotels["Latitude"].round(self._precision)
        df_hotels["long_area"] = df_hotels["Longitude"].round(self._precision)
        df_hotels["hotels"] = 1
        
        df_hotel_area_counts = df_hotels[["lat_area", "long_area", "hotels"]].groupby(["lat_area", "long_area"]).sum()
        
        e_hotel_areas = {
            t_area: e_hc["hotels"]
            for t_area, e_hc in df_hotel_area_counts.sort_values("hotels", ascending=False).to_dict(orient="index").items()}
        
        self._hotel_adjacent_areas = defaultdict(list)
        for t_area, i_hotel_count in e_hotel_areas.items():
            for i_lat_offset, i_long_offset in self._offsets:
                t_offset = (t_area[0] + i_lat_offset, t_area[1] + i_long_offset)
                if t_offset in e_hotel_areas:
                    self._hotel_adjacent_areas[t_area].append(t_offset)
                    self._hotel_adjacent_areas[t_offset].append(t_area)
        
        #e_area_hotel_rates = {
        #    t_area: round(e_rate["Published Nightly Rate (USD)"], 2)
        #    for t_area, e_rate in df_hotels[["Published Nightly Rate (USD)", "lat_area", "long_area"]].groupby(["lat_area", "long_area"]).mean().to_dict(orient="index").items()}
        
        e_hotels_in_area = defaultdict(list)
        for idx, lat, long, lat_area, long_area, hotel_rate in df_hotels[["Latitude", "Longitude", "lat_area", "long_area", "Published Nightly Rate (USD)"]].itertuples():
            for lat_offset, long_offset in self._offsets:
                e_hotels_in_area[(round(lat_area + lat_offset, self._precision), round(long_area + long_offset, self._precision))].append((lat, long, hotel_rate))
                #break
            #break
        
        self._hotel_blocks = {
            (lat_area, long_area): pd.DataFrame(a_hotel_coords, columns=["Latitude", "Longitude", "Hotel Rate"]).values
            for (lat_area, long_area), a_hotel_coords in e_hotels_in_area.items()}
        
    
    def find_nearest_hotels(self, df_houses, i_closest=i_CLOSEST):
        ''' Calculate Hotel Proximity: Find the closest hotels using the "Latitude", "Longitude" columns in the df_houses dataframe parameter '''
        s_lat_col = "Latitude" if "Latitude" in df_houses.columns else "latitude" if "latitude" in df_houses.columns else ""
        s_long_col = "Longitude" if "Longitude" in df_houses.columns else "longitude" if "longitude" in df_houses.columns else ""
        
        assert s_lat_col
        assert s_long_col
        
        df_houses["lat_hotel_area"] = df_houses[s_lat_col].round(self._precision)
        df_houses["long_hotel_area"] = df_houses[s_long_col].round(self._precision)
        
        
        i_hotel_areas = len(set(zip(df_houses["lat_hotel_area"], df_houses["long_hotel_area"])))
        print(f"{i_hotel_areas:,} hotel areas to search")
        
        n_start = time()
        a_house_hotel_proximities = []
        for i_area, (t_area, df_house_group) in enumerate(df_houses.groupby(["lat_hotel_area", "long_hotel_area"])):
            if i_area % 10 == 0 or i_area >= i_hotel_areas:
                print(f"\t{i_area + 1:,}/{i_hotel_areas:,} ({(i_area + 1) / i_hotel_areas:0.1%}): {len(df_house_group):,} houses")
            
            if t_area in self._hotel_blocks:
                np_houses = df_house_group[[s_lat_col, s_long_col]].values
                np_hotels = self._hotel_blocks[t_area][:,:2]
                np_hotelprices = self._hotel_blocks[t_area][:,2]
                
                np_proximity = spatial.distance.cdist(
                    np_houses,
                    np_hotels,
                    etl.get_distance)
                np_closest_index = np.argsort(np_proximity, axis=1)[:,:i_closest]
                #np_closest_dist = np.sort(np_proximity, axis=1)[:,:i_closest]
                i_actual_closest = min(i_closest, np_proximity.shape[1])
                np_closest_dist = np.array([
                    np_proximity[i_house, np_closest_index[i_house, i_hotel]]
                    for i_house in range(np_proximity.shape[0])
                    for i_hotel in range(i_actual_closest)]).reshape((np_proximity.shape[0], i_actual_closest))
                np_closest_prices = np.array([
                    np_hotelprices[np_closest_index[i_house, i_hotel]]
                    for i_house in range(np_proximity.shape[0])
                    for i_hotel in range(i_actual_closest)]).reshape((np_proximity.shape[0], i_actual_closest))
                #for i in range(np_closest_index.shape[1]):
                for i in range(i_closest):
                    if i < np_closest_index.shape[1]:
                        df_house_group[f"nearest_hotel_dist_{i}"] = np_closest_dist[:,i]
                        df_house_group[f"nearest_hotel_rate_{i}"] = np_closest_prices[:,i]
                        df_house_group[f"nearest_hotel_dist_{i}"] = df_house_group[f"nearest_hotel_dist_{i}"].round(3)
                    else:
                        df_house_group[f"nearest_hotel_dist_{i}"] = None
                        df_house_group[f"nearest_hotel_rate_{i}"] = None
            elif t_area in self._hotel_adjacent_areas:
                c_adj_areas = set(self._hotel_adjacent_areas[t_area])
                print(t_area, len(c_adj_areas), c_adj_areas)
                raise Exception("need to add code to handle this if it occurs")
                
                for i in range(i_closest):
                    df_house_group[f"nearest_hotel_dist_{i}"] = None
                    df_house_group[f"nearest_hotel_rate_{i}"] = None
            else:
                for i in range(i_closest):
                    df_house_group[f"nearest_hotel_dist_{i}"] = None
                    df_house_group[f"nearest_hotel_rate_{i}"] = None
                #break
            a_house_hotel_proximities.append(df_house_group)
        
        print(f"Calculating hotel proximities took: {utils.secondsToStr(time() - n_start)}")
        if a_house_hotel_proximities:
            df_houses_with_hotel_proximities = pd.concat(a_house_hotel_proximities)
            df_houses.drop(columns=["lat_hotel_area", "long_hotel_area"], inplace=True)
            df_houses_with_hotel_proximities.drop(columns=["lat_hotel_area", "long_hotel_area"], inplace=True)
        
            return df_houses_with_hotel_proximities
        else:
            print("No hotel areas found")
            return df_houses

    def find_nearest_hotels_to_by_coordinates(self, latitude, longitude, i_closest=i_CLOSEST):
        ''' Calculate Hotel Proximity: Find the closest hotels using the latitude and longitude arguments '''
        
        d_lat_area = round(latitude, self._precision)
        d_long_area = round(longitude, self._precision)
        
        #print(f"{i_hotel_areas:,} hotel areas to search")
        
        n_start = time()
        a_house_hotel_proximities = []
        
        t_area = (d_lat_area, d_long_area)
        
        e_nearest = {}
        if t_area in self._hotel_blocks:
            np_houses = np.array([[latitude, longitude]])
            np_hotels = self._hotel_blocks[t_area][:,:2]
            np_hotelprices = self._hotel_blocks[t_area][:,2]
            
            np_proximity = scipy.spatial.distance.cdist(
                np_houses,
                np_hotels,
                etl.get_distance)
            np_closest_index = np.argsort(np_proximity, axis=1)[:,:i_closest]
            #np_closest_dist = np.sort(np_proximity, axis=1)[:,:i_closest]
            i_actual_closest = min(i_closest, np_proximity.shape[1])
            np_closest_dist = np.array([
                np_proximity[i_house, np_closest_index[i_house, i_hotel]]
                for i_house in range(np_proximity.shape[0])
                for i_hotel in range(i_actual_closest)]).reshape((np_proximity.shape[0], i_actual_closest))
            np_closest_prices = np.array([
                np_hotelprices[np_closest_index[i_house, i_hotel]]
                for i_house in range(np_proximity.shape[0])
                for i_hotel in range(i_actual_closest)]).reshape((np_proximity.shape[0], i_actual_closest))
            #for i in range(np_closest_index.shape[1]):
            for i in range(i_closest):
                if i < np_closest_index.shape[1]:
                    e_nearest[f"nearest_hotel_dist_{i}"] = round(np_closest_dist[0,i], 3)
                    e_nearest[f"nearest_hotel_rate_{i}"] = np_closest_prices[0,i]
                    #e_nearest[f"nearest_hotel_dist_{i}"] = df_house_group[f"nearest_hotel_dist_{i}"].round(3)
                else:
                    e_nearest[f"nearest_hotel_dist_{i}"] = None
                    e_nearest[f"nearest_hotel_rate_{i}"] = None
        elif t_area in self._hotel_adjacent_areas:
            c_adj_areas = set(self._hotel_adjacent_areas[t_area])
            print(t_area, len(c_adj_areas), c_adj_areas)
            raise Exception("need to add code to handle this if it occurs")
            
            for i in range(i_closest):
                e_nearest[f"nearest_hotel_dist_{i}"] = None
                e_nearest[f"nearest_hotel_rate_{i}"] = None
        else:
            for i in range(i_closest):
                e_nearest[f"nearest_hotel_dist_{i}"] = None
                e_nearest[f"nearest_hotel_rate_{i}"] = None
        
        return e_nearest
