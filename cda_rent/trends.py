﻿import re, heapq
from datetime import datetime
import pandas as pd
import numpy as np

try:
    from .utils import (
        pearson_correlation, smooth,
        list_numbers, value_list_avg, value_list_herfindahl,
        value_list_similarity, value_list_variation)
except:
    from utils import (
        pearson_correlation, smooth,
        list_numbers, value_list_avg, value_list_herfindahl,
        value_list_similarity, value_list_variation)
    
__all__ = [
    "convert_keyword_dates",
    "count_peaks", "count_peaks_and_dips",
    "trend_magnitude", "trend", "peaks",
    "period_diff", "find_periodicity",
    "split_into_years", "get_period_averages",
    "value_list_avg_variation",
    "percent_in_top", "percent_in_top_consecutive", "top_consecutive", "top",
    "trend_season_analysis", "quick_seasonality",
    "e_PERIOD_NAMES", "e_PERIOD_INDEXES"
]

e_PERIOD_NAMES = {
    0: 'year',
    1: 'month',
    2: 'week',
    3: 'day',
    4: 'hour',
    5: 'minute',
    6: 'second',
}
e_PERIOD_INDEXES = {val: key for key, val in e_PERIOD_NAMES.items()}

re_MONTH = re.compile(r"^Searches: *", flags=re.I)


def convert_keyword_dates(a_columns):
    ''' take the column names from a Google keyword historical metrics files and
        convert the monthly "Searches: Mon-Year" columns into python datetimes
        Arguments:
            a_columns: {list} of columns with some starting with "Searches:
        Returns:
            {list} of date
    '''
    a_months = [col for col in a_columns if re_MONTH.search(col)]
    a_month_dates = [datetime.strptime(re_MONTH.sub('', s_month), '%b %Y') for s_month in a_months]
    return a_month_dates

def count_peaks(a_values, d_smooth_factor=0.5, d_peak_height=1.025, b_sensitive=False, i_max_peak_width=2):
    ''' count the number of peaks in list of values (only peaks, not dips)
        Arguments:
            a_values: {list} of at least 3 integer or float numbers
            d_smooth_factor: {float} between 0.0 to 1.0, where 0 won't do any smoothing and 1.0 will be completely smooth
            d_peak_height: {float} above 1.0 that specifies how much higher a point has to be than it's neighbours to be considered a peak
            b_sensitive: {bool} if true, only consider nearest neighbor on each side instead 2 nearest neighbors on each side
            i_max_peak_width: {int} of 1 or 2 to control whether a peak can have
                              the same value across two months for less smoothed numbers
                              only applies to non-sensitive approach
    '''
    if len(a_values) < 3: return 0
    if d_smooth_factor > 0:
        a_values = smooth(a_values, d_smooth_factor)
    #print('a_values', a_values)
    a_loop = a_values + a_values + a_values
    i_peaks = 0
    if not b_sensitive:
        for idx in range(len(a_values), len(a_values) * 2):
            d_pp, d_p, d_c, d_n, d_nn = a_loop[idx - 2], a_loop[idx - 1], a_loop[idx], a_loop[idx + 1], a_loop[idx + 2]
            
            if d_pp <= d_p < d_c / d_peak_height:
                i_peak_width = 0
                #print("narrow before")
            #elif d_pp < d_p <= d_c:
            #    i_peak_width = 1
            #    print("wide before")
            else:
                #i_peak_width = 3
                continue
            
            if d_nn <= d_n < d_c / d_peak_height:
                i_peak_width += 1
                #print("narrow after")
            elif d_nn < d_n == d_c:
                i_peak_width += 2
                #print("wide after")
            else:
                i_peak_width += 3
            
            #print(f'{i_peak_width}, [{d_pp}, {d_p}, {d_c}, {d_n}, {d_nn}]')
            #if d_pp < d_p < d_c / d_peak_height and d_nn < d_n < d_c / d_peak_height:
            if i_peak_width <= i_max_peak_width:
                i_peaks +=1 
                #print('idx', idx, i_peaks)
    else:
        if i_max_peak_width == 1:
            for idx in range(len(a_values), len(a_values) * 2):
                d_p, d_c, d_n = a_loop[idx - 1], a_loop[idx], a_loop[idx + 1]
                if d_p < d_c / d_peak_height and d_n < d_c / d_peak_height:
                    i_peaks +=1 
        else:
            i_direction = 0
            for idx in range(len(a_values), len(a_values) * 2):
                d_p, d_c = a_loop[idx - 1], a_loop[idx]
                
                if d_c / d_peak_height < d_p < d_c * d_peak_height:
                    continue
                elif d_c * d_peak_height > d_p: # up
                    if i_direction <= 0:
                        i_peaks +=1
                    i_direction = 1
                elif d_c / d_peak_height < d_p: # down
                    i_direction = -1
    return i_peaks


def count_peaks_and_dips(a_values, d_smooth_factor=0.5, d_peak_height=1.025, b_sensitive=False, i_max_peak_width=2):
    ''' count the number of peaks and dips in list of values
        Arguments:
            a_values: {list} of at least 3 integer or float numbers
            d_smooth_factor: {float} between 0.0 to 1.0,
                             where 0 won't do any smoothing and 1.0 will be completely smooth
            d_peak_height: {float} above 1.0 that specifies how much higher/lower a point
                           has to be than it's neighbours to be considered a peak/dip
            b_sensitive: {bool} if true, only consider nearest neighbor on each side instead
                         of 2 nearest neighbors on each side
            i_max_peak_width: {int} of 1 or 2 to control whether a peak can have
                              the same value across two months for less smoothed numbers
                              only applies to non-sensitive approach
    '''
    if len(a_values) < 3: return 0
    if d_smooth_factor > 0:
        a_values = smooth(a_values, d_smooth_factor)
    #print('a_values', a_values)
    a_loop = a_values + a_values + a_values
    i_peaks, i_dips = 0, 0
    b_up = False
    if not b_sensitive:
        for idx in range(len(a_values), len(a_values) * 2):
            d_pp, d_p, d_c, d_n, d_nn = a_loop[idx - 2], a_loop[idx - 1], a_loop[idx], a_loop[idx + 1], a_loop[idx + 2]
            
            
            if d_pp <= d_p < d_c / d_peak_height or d_pp >= d_p > d_c * d_peak_height:
                i_peak_width = 0
                b_up = d_c > d_p
            #elif d_pp < d_p == d_c / d_peak_height or d_pp > d_p >= d_c * d_peak_height:
            #    i_peak_width = 1
            else:
                #i_peak_width = 3
                continue
            
            if b_up and d_nn < d_n < d_c / d_peak_height:
                i_peak_width += 1
                print("narrow after")
            elif not b_up and d_nn > d_n > d_c * d_peak_height:
                i_peak_width += 1
                print("narrow after")
            elif b_up and d_nn < d_n == d_c:
                i_peak_width += 1
            elif not b_up and d_nn > d_n == d_c * d_peak_height:
                i_peak_width += 1
            else:
                i_peak_width += 2
            
            if i_peak_width <= i_max_peak_width:
                if b_up:
                    i_peaks +=1
                else:
                    i_dips +=1
            #if d_pp < d_p < d_c / d_peak_height and d_nn < d_n < d_c / d_peak_height:
            #
            #    i_peaks +=1 
            #elif d_pp > d_p > d_c / d_peak_height and d_nn > d_n > d_c / d_peak_height:
            #    i_dips +=1 
            #    #print('idx', idx, i_peaks)
    else:
        if i_max_peak_width == 1:
            for idx in range(len(a_values), len(a_values) * 2):
                d_p, d_c, d_n = a_loop[idx - 1], a_loop[idx], a_loop[idx + 1]
                if d_p < d_c / d_peak_height and d_n < d_c / d_peak_height:
                    i_peaks +=1
                elif d_p > d_c / d_peak_height and d_n > d_c / d_peak_height:
                    i_dips +=1
        else:
            i_direction = 0
            for idx in range(len(a_values), len(a_values) * 2):
                d_p, d_c = a_loop[idx - 1], a_loop[idx]
                
                if d_c / d_peak_height < d_p < d_c * d_peak_height:
                    #i_direction = 0
                    continue
                
                elif d_c * d_peak_height > d_p: # up
                    if i_direction <= 0:
                        i_peaks +=1
                    i_direction = 1
                elif d_c / d_peak_height < d_p: # down
                    if i_direction >= 0:
                        i_dips +=1
                    i_direction = -1
    
    return i_peaks, i_dips


def trend_magnitude(a_values):
    ''' calculate the magnitude of the trend as a percentage of the difference between start and end averages
        the direction of the trend doesn't matter here
        Arguments:
            a_values: {list} of numbers
        Returns:
            {float} of the magnitude as a factor between 0.0 and 1.0
    '''
    #a_values = list_numbers(list(a_values))
    if not a_values or len(a_values) < 2: return 0.0
    d_start, d_end = 0.0, 0.0
    a_factors = [(i) / (len(a_values) - 1) for i in range(len(a_values))]
    for idx, val in enumerate(a_values):
        d_factor = a_factors[idx]
        d_start += (val * (1 - d_factor))
        d_end += (val * d_factor)
    
    if d_start == 0 or d_end == 0:
        return 0.0
    #print(a_values, d_start, d_end)
    return 1 - (min(d_start, d_end) / max(d_start, d_end))


def trend(row):
    row = list_numbers(list(row))
    #print(sum(row), max(row), min(row))
    #if sum(row) == 0 and max(row) == 0 and min(row) == 0: return 0
    if not row: return 0
    d_correlation = pearson_correlation(list(row), list(range(len(row))))
    d_magnitude = trend_magnitude(list(row))
    #print(type(row), d_correlation, list(row), )
    return d_correlation * d_magnitude


def peaks(row):
    i_peaks = count_peaks(list(row), d_smooth_factor=0.25, d_peak_height=1.0125)
    #print(type(row), d_correlation, list(row), )
    return i_peaks


def period_diff(date1, date2):
    ''' Get a tuple of the component period differences between two dates '''
    return (
        date2.year - date1.year,
        date2.month - date1.month,
        date2.week - date1.week,
        date2.day - date1.day,
        date2.hour - date1.hour,
        date2.minute - date1.minute,
        date2.second - date1.second,
    )


def find_periodicity(a_period_diffs):
    ''' figure out the component interval periodicities in a list of time period_diff
        Return:
            {list} sorted by the most likely interval element(s) first
    '''
    d_year, d_month, d_week, d_day, d_hour, d_minute, d_second = 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
    ad_components = [0.0] * 7
    ea_components = defaultdict(list)
    for t_diff in a_period_diffs:
        for i_comp, i_diff in enumerate(t_diff):
            ea_components[i_comp].append(i_diff)
    
    e_components = {
        s_interval: (d_avg, d_std, d_avg % 1.0)
        for s_interval, d_avg, d_std in [
            (
                e_PERIOD_NAMES[i_comp],
                np.mean(a_vals),
                np.std(a_vals),
            )
            for i_comp, a_vals in ea_components.items()
            if min(a_vals) != 0 or max(a_vals) != 0]
    }
    return sorted(e_components.items(), key=lambda x: x[1][1] + x[1][2], reverse=False)


def split_into_years(a_period_numbers, i_annual_periods=12):
    ''' take a list of numbers per period and split into sub-lists by year '''
    a_year_numbers = []
    i_years = round(int(len(a_period_numbers)) / i_annual_periods)
    for i_yr in range(1, i_years + 1):
        a_year_numbers.append(a_period_numbers[(i_yr - 1) * i_annual_periods: i_yr * i_annual_periods])
    return a_year_numbers


def get_period_averages(a_year_period_numbers):
    ''' take a list of years containing sub-lists of numbers per period and
        calculate the average for each period across those years
    '''
    return [value_list_avg(list(t_period))
        for t_period in zip(*a_year_period_numbers)
    ]


def value_list_avg_variation(a_values):
    ''' Return variation of the average of the non np.nan numbers as % of largest number. '''
    ad_vals = [abs(x) for x in list_numbers(a_values)]
    if len(ad_vals) > 0:
        d_max = max(ad_vals)
        if d_max == 0: return 0
        
        d_avg = np.mean(ad_vals)
        
        return d_avg / d_max
    else:
        return np.nan


def percent_in_top(a_values, top=1):
    ''' calculate the percentage of total values that are represented in the top n items
    '''
    total = sum(list_numbers(a_values))
    d_adjustment = 0
    if any([x < 0 for x in a_values]):
        d_adjustment = min(a_values)
        a_values = [x + a_values for x in a_values]
    
    if total == 0: return 0
    
    if isinstance(top, float) and top < 1:
        top = max(1, round(top * len(a_values)))
    a_largest = heapq.nlargest(top, a_values)
    #print(a_largest)
    top_total = sum(a_largest)
    return top_total / total


def percent_in_top_consecutive(a_values, top=2):
    ''' calculate the percentage of total values that are represented in the top n items as long as they are consecutive with the highest
    '''
    if top == 0: return 0.0
    total = sum(list_numbers(a_values))
    d_adjustment = 0
    if any([x < 0 for x in a_values]):
        d_adjustment = min(a_values)
        a_values = [x + a_values for x in a_values]
    if total == 0: return 0.0
    if isinstance(top, float) and top < 1:
        top = max(1, round(top * len(a_values)))
    
    a_largest = heapq.nlargest(top, a_values)
    c_largest = set(a_largest)
    
    i_first = a_values.index(a_largest[0])
    i_last = i_first
    
    #a_top = [(i_first, a_values[i_first])]
    while i_first > 0:
        i_first -= 1
        if a_values[i_first] not in c_largest:
            i_first += 1
            break
        #else:
        #    a_top.append((i_first, a_values[i_first]))
            
    while i_last < len(a_values) - 1:
        i_last += 1
        if a_values[i_last] not in c_largest:
            break
        #else:
        #    a_top.append((i_last, a_values[i_last]))
    i_last += 1
    
    #print(i_first, i_last, a_values[i_first: i_last], sum(a_values[i_first: i_last]), sum(a_values[i_first: i_last ]) / total, a_top)
     
    #top_total = sum(a_largest)
    top_total = sum(a_values[i_first: i_last])
    return top_total / total


def top_consecutive(a_values, top=2):
    ''' get the indexes/values of the top n items as long as they are consecutive with the highest
    '''
    if top == 0: return 0.0
    total = sum(list_numbers(a_values))
    d_adjustment = 0
    if any([x < 0 for x in a_values]):
        d_adjustment = min(a_values)
        a_values = [x + a_values for x in a_values]
    if total == 0: return 0.0
    if isinstance(top, float) and top < 1:
        top = max(1, int(round(top * len(a_values))))
    #print(top, type(top))
    a_largest = heapq.nlargest(top, a_values)
    c_largest = set(a_largest)
    
    i_first = a_values.index(a_largest[0])
    i_last = i_first + 1
    
    a_top = [(i_first, a_values[i_first])]
    
    while i_first > 0:
        i_first -= 1
        if a_values[i_first] not in c_largest:
            i_first += 1
            break
        else:
            a_top.append((i_first, a_values[i_first]))
            
    while i_last < len(a_values) - 1:
        if a_values[i_last] not in c_largest:
            break
        else:
            a_top.append((i_last, a_values[i_last]))
        i_last += 1
    i_last += 1
    
    a_top.sort(key=lambda t: t[1], reverse=True)
    #print(a_top)
    return a_top[:top]


def top(a_values, top=2):
    ''' get the indexes/values of the top n items
    '''
    if top == 0: return 0.0
    total = sum(list_numbers(a_values))
    d_adjustment = 0
    if any([x < 0 for x in a_values]):
        d_adjustment = min(a_values)
        a_values = [x + a_values for x in a_values]
    if total == 0: return 0.0
    if isinstance(top, float) and top < 1:
        top = max(1, int(round(top * len(a_values))))
        
    #print(top, type(top))
    a_largest = heapq.nlargest(top, a_values)
    c_largest = set(a_largest)
    
    a_top = [
        (idx, val)
        for idx, val in enumerate(a_values)
        if val in c_largest]
    
    a_top.sort(key=lambda t: t[1], reverse=True)
    return a_top[:top]


def trend_season_analysis(a_monthly_numbers, a_month_dates=None):
    ''' take an input of multiple years of monthly data and calculate trend and seasonality metadata from it
        Arguments:
            a_monthly_numbers: {list} of values by month
    '''
    a_monthly_numbers = [x if not pd.isnull(x) else 0 for x in a_monthly_numbers]
    total_numbers = sum(a_monthly_numbers)
    
    a_year_numbers = split_into_years(a_monthly_numbers)  # split into lists per year
    a_month_averages = get_period_averages(a_year_numbers)  # the average volume per calendar month
    
    i_years = len(a_year_numbers)
    i_months = len(a_monthly_numbers)
    
    i_years_with_data = sum([1 for a_year in a_year_numbers if sum(a_year) > 0])
    
    overall_corr = pearson_correlation(a_monthly_numbers, list(range(len(a_monthly_numbers))))
    
    a_monthly_changes = [x2 - x1 for x1, x2 in zip(a_monthly_numbers, a_monthly_numbers[1:])]
    a_year_changes = [
        [x2 - x1 for x1, x2 in zip(year, year[1:])]
        for year in a_year_numbers]
    
    avg_change = value_list_avg(a_monthly_changes)
    overall_change_variation = avg_change / max(a_monthly_numbers, key=lambda x: abs(x)) if total_numbers != 0 else 0
    
    month_change_variation = value_list_avg([
        value_list_avg(list(a_month)) / max(a_month, key=lambda x: abs(x))
        for a_month in zip(*a_year_changes)
        if max(a_month, key=lambda x: abs(x) > 0)]) if total_numbers != 0 else 0
    
    noiseness = (sum([abs(x) for x in a_monthly_changes]) / sum(a_monthly_numbers)) ** 0.25 if total_numbers else 0.0
    
    year_similarity = value_list_avg([
        1 - value_list_similarity(a_year) for a_year in a_year_numbers])
    #year_variation = value_list_avg([
    #    value_list_variation(a_year) for a_year in a_year_numbers])
    year_variation = value_list_avg([
        1 - value_list_avg_variation(a_year) for a_year in a_year_numbers])
    year_corr = value_list_avg([
        pearson_correlation(a_year, list(range(12))) for a_year in a_year_numbers])
        
    #year_hhi = value_list_avg([
    #    value_list_herfindahl(a_year) for a_year in a_year_numbers])
    year_hhi = value_list_herfindahl(a_month_averages)
    average_variation = value_list_variation(a_month_averages)
    
    i_total_peaks = count_peaks(a_monthly_numbers, d_peak_height=1.1)
    a_year_noisy_peaks = [
        count_peaks_and_dips(a_year, d_smooth_factor=0.25, b_sensitive=True, d_peak_height=1.2)
        for a_year in a_year_numbers]
    i_year_noisy_peaks = sum([x[0] for x in a_year_noisy_peaks])
    i_year_noisy_dips = sum([x[1] for x in a_year_noisy_peaks])
    i_year_total_peaks = sum([
        count_peaks(a_year, d_smooth_factor=0.5, d_peak_height=1.1)
        for a_year in a_year_numbers])
    
    i_peaks = count_peaks(a_month_averages)
    a_year_peaks = [x for x in [
        count_peaks(a_year)
        for a_year in a_year_numbers if a_year]]
    #if len(a_year_peaks) <= 1 or sum(a_year_peaks) == 0:
    #    year_peaks = 0
    #elif len(a_year_peaks) <= 1:
    #    year_peaks = (1 / value_list_avg(a_year_peaks)) / 2
    #else:
    #    year_peaks = 1 / value_list_avg(a_year_peaks)
        
    d_avg_peaks = value_list_avg(a_year_peaks) if a_year_peaks else 0
    d_avg_peaks = (d_avg_peaks + i_peaks) / 2
    year_peaks = d_avg_peaks ** 2 if d_avg_peaks <= 1 else 1 / d_avg_peaks
    if i_total_peaks > i_years:
        d_total_peaks = (i_years / i_total_peaks) ** 2
    elif i_total_peaks < i_years:
        d_total_peaks = (i_total_peaks / i_years) ** 2
    else:
        d_total_peaks = 1.0
        
    d_year_noiseness = max(0, i_year_noisy_peaks + i_year_noisy_dips - i_years) / (i_months - i_years)
    
    #d_in_top_quarter = [
    #    percent_in_top(year, 0.25)
    #    for year in a_year_numbers]
    c_high_months = set()
    c_top_consecutive_months = set()
    a_year_percent_in_top = []
    a_non_top_vals = []
    i_season_max = 4
    for a_year in a_year_numbers:
        year_total = sum(a_year)
        if not year_total:
            a_year_percent_in_top.append(0.0)
            a_non_top_vals.append(0.0)
            continue
        a_top_months, a_top_values = zip(*top_consecutive(a_year, i_season_max))
        c_top_consecutive_months = c_top_consecutive_months.union(set(a_top_months))
        
        a_high_months, a_high_values = zip(*top(a_year, i_season_max))
        c_high_months = c_high_months.union(set(a_high_months))
        
        a_year_percent_in_top.append(sum(a_top_values) / sum(a_year))
        
        a_year_non_top_months = [x for x in a_year if x not in a_top_values]
        a_non_top_vals.append(
            max(0, min(1, len(a_year_non_top_months) / (len(a_year) - i_season_max))))
        
    in_top_months = value_list_avg(a_year_percent_in_top)
    #print(a_non_top_vals)
    non_top_months = value_list_avg(a_non_top_vals)
    #same_year_season = 1.0 if len(c_top_consecutive_months) <= 3 else 3 / len(c_top_consecutive_months)
    same_year_season = sum([
        1.0 if len(c_top_consecutive_months) <= i_season_max else 1 / (len(c_top_consecutive_months) - (i_season_max - 1)),
        (1.0 if len(c_high_months) <= i_season_max else 1 / (len(c_high_months) - (i_season_max - 1))) * 2
    ]) / 3
    
    a_year_totals = [sum(a_year) for a_year in a_year_numbers]
    max_year = max(a_year_totals)
    non_max_adjustment = total_numbers / i_years
    if i_years_with_data > 1:
        year_concentration = 1 - ((total_numbers - max_year - non_max_adjustment) / (total_numbers - non_max_adjustment))
    else:
        year_concentration = 1.0
    
    month_similarity = value_list_avg([
        value_list_similarity(list(a_month)) for a_month in zip(*a_year_numbers)])
    month_variation = value_list_avg([
        value_list_variation(list(a_month)) for a_month in zip(*a_year_numbers)])
    month_corr = value_list_avg([
        pearson_correlation(list(a_month), list(range(i_years)))
        for a_month in zip(*a_year_numbers)])
    month_hhi = value_list_avg([
        value_list_herfindahl(list(a_month))
        for a_month in zip(*a_year_numbers)])
    
    a_year_avg = [value_list_avg(year) for year in a_year_numbers]
    a_year_growths = [
        0 if year1 == 0 and year2 == 0
        else 1.0 if year1 == 0
        else -1.0 if year2 == 0
        else 1 - (year1 / year2) for year1, year2 in zip(a_year_avg, a_year_avg[1:])]
    a_year_pearson = [
        pearson_correlation(a_year, list(range(12))) for a_year in a_year_numbers]
    
    d_years_with_data = min(1.0, max(0, i_years_with_data - 1) / 3)
    e_trend_factors = {
        "overall_correlation": round(overall_corr, 4),
        "overall_change_variation": round(overall_change_variation, 4),
        "year_similarity": round(year_similarity, 4),
        "year_variation": round(year_variation, 4),
        "year_correlation": round(year_corr, 4),
        "year_concentration": round(year_concentration, 4),
        "year_hhi": round(year_hhi, 4),
        "year_peaks": round(year_peaks, 4),
        "total_peaks": round(d_total_peaks, 4),
        "month_similarity": round(month_similarity, 4),
        "month_variation": round(month_variation, 4),
        "month_correlation": round(month_corr, 4),
        "month_change_variation": round(month_change_variation, 4),
        "month_hhi": round(month_hhi, 4),
        #"year_pearson_1": a_year_pearson[0],
        #"year_pearson_2": a_year_pearson[1],
        #"year_pearson_3": a_year_pearson[2],
        #"year_pearson_4": a_year_pearson[3],
        #"year_average_1": a_year_avg[0],
        #"year_average_2": a_year_avg[1],
        #"year_average_3": a_year_avg[2],
        #"year_average_4": a_year_avg[3],
        #"year_growth_1_to_2": a_year_growths[0],
        #"year_growth_2_to_3": a_year_growths[1],
        #"year_growth_3_to_4":  a_year_growths[2],
        #"year_growth_3_to_4":  a_year_growths[2],
        "percent_in_last_year": round((a_year_avg[-1] / sum(a_year_avg)), 4) if sum(a_year_avg) > 0 else 0,
        "total_growth": round(a_year_avg[-1] - a_year_avg[0], 4),
        "total_growth_percent": round((a_year_avg[-1] - a_year_avg[0]) / max(a_year_avg), 4) if total_numbers != 0 else 0,
        "month_over_month": round(month_corr ** 4 if month_corr >= 0 else -1 * month_corr ** 4, 4),
        "noiseness": round(noiseness, 4),
        "year_noiseness": round(d_year_noiseness, 4),
        "same_year_season": round(same_year_season, 4),
        "in_top_months": round(in_top_months, 4),
        "non_top_months": round(non_top_months, 4),
        "years_with_data": round(d_years_with_data, 4),
        "average_variation": round(average_variation, 4),
    }
    e_trend_factors['trend_magnitude'] = trend_magnitude(a_monthly_numbers)
        
    e_trend_factors['trending'] = (sum([
        (e_trend_factors['month_over_month'] * 2.5),
        e_trend_factors['overall_correlation'],
        e_trend_factors['percent_in_last_year'] * .5
    ]) / 4)
    
    e_trend_factors['trend_volume'] = e_trend_factors['trend_magnitude'] * e_trend_factors['trending'] # ** 0.75
    
    e_trend_factors['season_magnitude'] = (
        (d_years_with_data) * 
        #year_variation *  # amount of variation inside each year on average
        (sum([
            (year_hhi ** 0.5),  # the years uneveness of distribution
            (in_top_months * 2)  # the percentage of the sales in the top consecutive months
        ]) / 3) #* 
        #(1 - d_year_noiseness)
        #((1 - noiseness) / 2)
    )
    
    d_seasonality = max(0,
        (
            d_years_with_data *  # how many years actually have numbers
            (1 - year_concentration ** 2) *  #how much the numbers are concentrated in just one year(
            (1 - d_year_noiseness) * 
            average_variation *
            (sum([
                #((year_hhi ** 0.5) * 0.5),  # the years uneveness of distribution
                #average_variation * 0.5,
                (year_peaks * 1.0),  # the number of peaks in each year
                ((non_top_months ** 0.5) * same_year_season * 2),  # numbers peak in the same season each year
            #(d_total_peaks * 0.5) +  # the total number of peaks in across years
            #(abs(month_similarity) * 0.5)  # how similar the calendar months are across years
            ]) / 3)
        )
    )
    if d_seasonality > 0.2:
        e_trend_factors['seasonality'] = round(0.2 + (d_seasonality - 0.2) ** 0.5, 4)
    else:
        e_trend_factors['seasonality'] = round(d_seasonality, 4)
    
    e_trend_factors['season_volume'] = (round(sum([
        e_trend_factors['season_magnitude'] * e_trend_factors['seasonality'],
        #e_trend_factors['season_magnitude'] * 0.5,
        e_trend_factors['seasonality'] * 2
        ]) / 3), #** 0.5
        4)
    
    if e_trend_factors['seasonality'] > 0.1:
        
        a_top_shares = [round(percent_in_top_consecutive(a_month_averages, top=i), 4) for i in range(1, 7)]
        
        d_prior_change, d_change = 0, 0
        for i_season_length, (x, y) in enumerate(zip(a_top_shares, a_top_shares[1:])):
            d_prior_change, d_change = d_change, x - y
            if d_change > (d_prior_change / 4):
                break
        e_trend_factors['season_length'] = i_season_length
        
        # calculate the percentage of total amounts in the top 1 month, 2 months, 3 months...
        #print("a_top_shares", a_top_shares)
        if (a_top_shares[0] >= 0.45 and a_top_shares[1] >= 0.8) or i_season_length < 3:
            s_season_type = "Month"
            i_season_length = min(i_season_length, 2)
        elif (a_top_shares[2] >= 0.5) or i_season_length < 6:
            s_season_type = "Quarter"
            i_season_length = min(i_season_length, 4)
        elif a_top_shares[5] >= 0.5:
            s_season_type = "Semi Annual"
            i_season_length = min(i_season_length, 7)
        else:
            s_season_type = ""
        e_trend_factors['season_type'] = s_season_type
    
        a_top_fall_offs = [round(x - y, 2) for x, y in zip(a_top_shares, a_top_shares[1:])]
        #a_all_tops.append(a_top_shares)
        
        if a_month_dates:
            a_top_consecutives = sorted(top_consecutive(smooth(a_month_averages), top=i_season_length), key=lambda x: x[0])
            dt_period_start, dt_period_end = a_month_dates[a_top_consecutives[0][0]], a_month_dates[a_top_consecutives[-1][0]]
            e_trend_factors['season_start'] = dt_period_start
            e_trend_factors['season_end'] = dt_period_end
            if i_season_length == 1 or dt_period_start == dt_period_end:
                s_season_name = dt_period_start.strftime("%b")
            else:
                s_season_name = "{}-{}".format(dt_period_start.strftime("%b"), dt_period_end.strftime("%b"))
            e_trend_factors['season_name'] = s_season_name
    
    return e_trend_factors


def quick_seasonality(a_revenue, a_dates, **kwargs):
    d_total = sum(list_numbers(a_revenue))
    
    e_seasonality = {
        "seasonality": 0,
        "season_length": 12,
        "season_type": None,
        "season_peak_month": None,
        "season_max_month": None,
        #"season_name": None,
        #"season_start": None,
        #"season_end": None,
    }
    if a_revenue and a_dates:
        i_max_month = a_dates[np.argmax(a_revenue)].month
        e_seasonality["season_max_month"] = i_max_month
    
    if len(a_revenue) >= 8 and d_total > 0:
        a_year_numbers = split_into_years(a_revenue)  # split into lists per year
        #if kwargs.get("b_debug"): print("a_year_numbers", a_year_numbers)
        
        a_month_averages = get_period_averages(a_year_numbers)  # the average volume per calendar month
        #if kwargs.get("b_debug"): print("a_month_averages", a_month_averages)
        i_top_month = a_dates[np.argmax(a_month_averages)].month
        #if kwargs.get("b_debug"): print("i_top_month", i_top_month)

        i_peaks = count_peaks(smooth(a_month_averages + a_month_averages + a_month_averages, 0.8)[12:24])
        #if kwargs.get("b_debug"): print("i_peaks", i_peaks)
        
        a_year_peaks = [x for x in [
            count_peaks(a_year)
            for a_year in a_year_numbers if a_year]]
        d_avg_peaks = value_list_avg(a_year_peaks) if a_year_peaks else 0
        d_avg_peaks = (d_avg_peaks + i_peaks) / 2

        d_month_hhi = value_list_herfindahl(a_month_averages)
        a_top_months = [
            d_mon / d_total
            for d_mon in sorted(a_month_averages, reverse=True)]
        #if kwargs.get("b_debug"): print("a_top_months", a_top_months)
        a_top_share = [
            sum(a_top_months[:i_months])
            for i_months in range(1, 7)]

        d_prior_change, d_change = 0, 0
        for i_season_length, (x, y) in enumerate(zip(a_top_share, a_top_share[1:])):
            d_prior_change, d_change = d_change, x - y
            if d_change > (d_prior_change / 4):
                break
        #e_seasonality['season_length'] = i_season_length
        
        d_seasonality = (d_avg_peaks + d_month_hhi * 2) / 3
        e_seasonality["seasonality"] = round(d_seasonality, 2)
        e_seasonality["season_peak_month"] = i_top_month
        if (a_top_share[0] >= 0.45 and a_top_share[1] >= 0.8) or i_season_length < 3:
            e_seasonality["season_type"] = "Month"
            e_seasonality["season_length"] = min(i_season_length, 2)
        elif (a_top_share[2] >= 0.5) or i_season_length < 6:
            e_seasonality["season_type"] = "Quarter"
            e_seasonality["season_length"] = min(i_season_length, 4)
        elif a_top_shares[5] >= 0.5:
            e_seasonality["season_type"] = "Semi Annual"
            e_seasonality["season_length"] = min(i_season_length, 7)
    return e_seasonality