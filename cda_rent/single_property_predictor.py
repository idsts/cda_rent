﻿import os, sys, re, shutil, math
from datetime import datetime, timedelta
from time import time
import dateutil
import requests, json
from collections import Counter, defaultdict
from operator import itemgetter
import pandas as pd
import numpy as np
import logging
import traceback

try:
    from . import utils, paths, log
    from . import db_tables, defaults
    from . import prep_zillow
    from . import predict_zillow
    from . import location_analysis
    from . import airbnb_standardization
    from . import google_maps, us_states
    from . import interactive_inputs
    from . import area_data
    from . import scoring
    from . import financing_webhook
    from .defaults import *
    #from .log import LOGGER
except:
    import utils, paths, log
    import db_tables, defaults
    import prep_zillow
    import predict_zillow
    import location_analysis
    import airbnb_standardization
    import google_maps, us_states
    import interactive_inputs
    import area_data
    import scoring
    import financing_webhook
    from defaults import *
    #from log import LOGGER


if prep_zillow.o_AREA_LOOKUPS is None or prep_zillow.o_HOTELS is None or not prep_zillow.e_ZCTA:
    prep_zillow.load_data() #**kwargs)

if predict_zillow.e_VARS is None or predict_zillow.e_LTR_VARS is None or predict_zillow.o_MAPE_MODEL is None or predict_zillow.o_MAPE_RATE_MODEL is None or predict_zillow.o_MAPE_RENT_MODEL:
    predict_zillow.load_data() #**kwargs)

e_VARS = predict_zillow.e_VARS
e_LTR_VARS = predict_zillow.e_LTR_VARS
c_OUTPUT_COLS = set([
    k for k in e_VARS["train_cols"] + e_LTR_VARS["train_cols"] + list(interactive_inputs.c_EXTRA_OUTPUTS)
    if not k.startswith(("lat_sub_", "long_sub_"))])

c_STR_IMPROVEMENT_COLS = set(e_VARS.get("improvement_checks", []))
c_LTR_IMPROVEMENT_COLS = set(e_LTR_VARS.get("improvement_checks", []))
a_STR_FEATURES = [
    (i, col)
    for i, col in enumerate(e_VARS["train_cols"])
    if col.startswith("feature_") and col in c_STR_IMPROVEMENT_COLS]
a_LTR_FEATURES = [
    (i, col)
    for i, col in enumerate(e_LTR_VARS["train_cols"])
    if col.startswith("feature_") and col in c_LTR_IMPROVEMENT_COLS]

e_TARGET_IMPROVEMENTS = {
    #"feature": (expected_improvement, minimum_improvement),
    "feature_event_friendly": (0.1333, 0.005), #1.4
    "feature_pool": (0.0833, 0.008), #1.25
    "feature_jacuzzi": (0.0567, 0.005), #1.17
    "feature_sauna": (0.0533, 0.0075), #1.16
    "feature_ski_in_ski_out": (0.0833, 0.0075), #1.25
    "feature_trees_fruit_nut": (0.0333, 0.0),
    "feature_wet_bar": (0.0333, 0.001),
    "guesthouse": (0.25, 0.05),
    "feature_furnished": (0.16, 0.025),
    "feature_pool_table": (0.0267, 0.001),
    "feature_extra_room": (0.01, 0.005), #1.4
    "feature_counters_stone": (0.0075, 0.004), #1.4
}

#if predict_zillow.e_RENT_VARS is None or predict_zillow.o_RENT_MAPE_MODEL is None:
#    predict_zillow.load_rent_data() #**kwargs)
#e_RENT_VARS = predict_zillow.e_RENT_VARS

e_MONTHS = {
    1: "January",
    2: "February",
    3: "March",
    4: "April",
    5: "May",
    6: "June",
    7: "July",
    8: "August",
    9: "September",
    10: "October",
    11: "November",
    12: "December",
}

e_LOCATION_STDS = {}
dt_LOCATION_STD_EXP = datetime.utcnow()
e_POTENTIAL_LOCATION_STDS = defaultdict(Counter)

def load_location_stds():
    global e_LOCATION_STDS, dt_LOCATION_STD_EXP
    if not e_LOCATION_STDS or dt_LOCATION_STD_EXP < datetime.utcnow():
        o_DB = db_tables.get_connection()
        df_locations = o_DB.get_table_as_df("location_translation", cols=["state", "name", "standardized_name"], where='location_type="city"')
        e_LOCATION_STDS = {
            (s_state, s_name.lower()): s_std
            for idx, s_state, s_name, s_std in df_locations.itertuples()
        }
        dt_LOCATION_STD_EXP = datetime.utcnow() + timedelta(hours=6)   
        o_DB.close()
    
    global e_POTENTIAL_LOCATION_STDS
    if e_POTENTIAL_LOCATION_STDS is None:
        e_POTENTIAL_LOCATION_STDS = defaultdict(Counter)


def check_geocode(e_address):
    return (200 if all([
        e_address.get("lat"),
        e_address.get("lng"),
        e_address.get("address_components", {}).get("state"),
        e_address.get("address_components", {}).get("city"),
        e_address.get("address_components", {}).get("postal_code"),
        e_address.get("address_components", {}).get("formatted_address"),
    ]) else 400)


def to_geocode_format(e_input):
    s_state = us_states.e_STATE_ABBREV.get(e_input.get("state", "").lower(), e_input.get("state", "")).upper()
    s_city = e_input.get("city", "")
    load_location_stds()
    s_city_std = e_LOCATION_STDS.get((s_state, s_city.lower()), s_city)
    
    e_address = {
            'lat': e_input.get("latitude"),
            'lng': e_input.get("longitude"),
            'address_components': {
                'street_number': [e_input.get("street_number")],
                'route': [e_input.get("route")],
                'neighborhood': [e_input.get("")],
                'political': [e_input.get("political", [s_city_std, s_state, "US"])],
                'locality': [s_city_std],
                'administrative_area_level_2': [e_input.get("county")],
                'administrative_area_level_1': [s_state],
                'country': [e_input.get("country", "US")],
                'postal_code': [e_input.get("postal_code", e_input.get("zipcode"))],
                'state': [s_state],
                'city': [s_city_std],
                'formatted_address': e_input.get("address")
            },
            'distance_from_search': None,
            'zcta': e_input.get("zcta", e_input.get("postal_code", e_input.get("zipcode")))
        }
    i_status = check_geocode(e_address)
    
    return e_address, i_status


##### DEAL WITH SINGLE PROPERTY REQUEST PREDICTIONS #####

#@utils.timer
def prep_single_property(e_input, **kwargs):
    e_data, e_pai, e_address, e_valid_financing = {}, {}, {}, {}
    e_result = {
        "message": {},
        "data": {},
        "pai": {},
        "address": {},
        "financing": {},
        "status": 400,
    }
    
    if e_input is None:
        e_missing_req = {
            s_col: f"Missing required input \"{s_col}\". Should be a {dtype}." 
            for s_col, dtype in interactive_inputs.e_REQUIRED_INPUTS_DTYPES.items()}
        e_result["message"] = {"No input": e_missing_req}
        return e_result
    
    e_missing_req = interactive_inputs.check_required_input(e_input)
    if e_missing_req:
        e_result["message"] = {"Invalid input": e_missing_req}
        return e_result

    e_address, i_status = {}, 500
    
    if isinstance(kwargs.get("address_dict"), dict) and check_geocode(kwargs.get("address_dict")) == 200:
        e_address, i_status = kwargs["address_dict"], 200
        if kwargs.get("b_debug"): print("use address dict")
    elif kwargs.get("skip_geocode"):
        e_address, i_status = to_geocode_format(e_input)
        if i_status == 200 and kwargs.get("b_debug"): 
            print("use to_geocode_format")
    
    if i_status != 200:
        if kwargs.get("b_debug"): print("google maps lookup")
        e_address, i_status = google_maps.check_coordinates_address(e_input, **kwargs)
    
    if i_status != 200:
        e_result["message"] = {"Invalid address": e_address}
        e_result["status"] = i_status
        return e_result
    
    elif pd.isnull(e_address.get("lat")) or pd.isnull(e_address.get("lng")):
        e_result["message"] = {"Invalid address": e_address}
        e_result["status"] = i_status
        return e_result
    
    if pd.isnull(e_input.get("latitude")) or e_input["latitude"] in {'', "0", 0}:
        e_input["latitude"] = e_address["lat"]
    if pd.isnull(e_input.get("longitude")) or e_input["longitude"] in {'', "0", 0}:
        e_input["longitude"] = e_address["lng"]
    
    if e_input.get("city") and e_address.get("address_components", {}).get("city") and e_address.get("address_components", {}).get("state"):
        s_input_city = e_input.get("city")
        if isinstance(s_input_city, (tuple, list)): s_input_city = s_input_city[0]
        assert isinstance(s_input_city, str)
        
        s_state = e_address.get("address_components", {}).get("state")
        if isinstance(s_state, (tuple, list)): s_state = s_state[0]
        assert isinstance(s_state, str)
        
        s_city = e_address.get("address_components", {}).get("city")
        if isinstance(s_city, (tuple, list)): s_city = s_city[0]
        assert isinstance(s_city, str)
        
        e_POTENTIAL_LOCATION_STDS[(s_state, s_input_city.lower())][(s_state, s_city.lower())] += 1
        if (s_state, s_input_city.lower()) not in e_LOCATION_STDS:
            if sum(e_POTENTIAL_LOCATION_STDS[(s_state, s_input_city.lower())].values()) > 12:
                if max(e_POTENTIAL_LOCATION_STDS[(s_state, s_input_city.lower())].values()) >= 9:
                    e_LOCATION_STDS[(s_state, s_input_city.lower())] = s_city
        
                    o_DB = db_tables.get_connection()
                    o_DB.insert(
                        "location_translation",
                        {
                            "state": s_state,
                            "name": s_input_city,
                            "standardized_name": s_city,
                            "location_type": "city"
                        })
                    o_DB.close()
    
    e_result["address"] = e_address
    
    assert e_input.get("latitude")
    assert e_input.get("longitude")
    
    if pd.isnull(e_input.get("elevation")):
        e_input["elevation"] = google_maps.get_elevation(e_input["latitude"], e_input["longitude"], source="aster30")
    #print("elevation", e_input["elevation"])
    
    e_input = interactive_inputs.stack_input_dict(e_input)
    
    #print("\n\ne_input", e_input)
    
    e_valid_optional, e_invalid_optional = interactive_inputs.check_optional_input(e_input)
    e_valid_amenities, e_invalid_amenities = interactive_inputs.check_feature_input(e_input, "amenities", section_prefix="amenity_", col_prefix="feature_")
    e_valid_features, e_invalid_features = interactive_inputs.check_feature_input(e_input, "features", section_prefix="feature_", col_prefix="feature_")
    e_valid_locations, e_invalid_locations = interactive_inputs.check_feature_input(e_input, "locations", section_prefix="location_", col_prefix="location_")
    e_valid_conditions, e_invalid_conditions = interactive_inputs.check_feature_input(e_input, "conditions", section_prefix="condition_", col_prefix='')
    e_valid_financing, e_invalid_financing = interactive_inputs.check_financing_input(e_input)
    
    e_result["financing"] = e_valid_financing
    
    #print('\n\ne_valid_optional', e_valid_optional)
    
    e_valid_optional = interactive_inputs.add_optional_defaults(e_valid_optional, b_fill_missing=True)
    
    if e_invalid_optional: print("\n", "e_invalid_optional", e_invalid_optional)
    if e_invalid_amenities: print("\n", "e_invalid_amenities", e_invalid_amenities)
    if e_invalid_features: print("\n", "e_invalid_features", e_invalid_features)
    if e_invalid_locations: print("\n", "e_invalid_locations", e_invalid_locations)
    if e_invalid_conditions: print("\n", "e_invalid_conditions", e_invalid_conditions)
    if e_invalid_financing: print("\n", "e_invalid_financing", e_invalid_financing)
    
    e_std = interactive_inputs.standardize_input(e_input, e_valid_optional, e_valid_amenities, e_valid_features, e_valid_locations, e_valid_conditions)
    #print("e_std", e_std, "\n")
    #print("e_std ptype", e_std.get("property_type"), "\n")
    #print("elevation", e_std["elevation"])
    
    if not isinstance(e_std.get("max_guests"), (int, float)) or e_std["max_guests"] < e_std["bedrooms"]:
        e_std["max_guests"] = e_VARS["bedroom_guest"].get(e_std["bedrooms"], e_std["bedrooms"] * 2)
    
    b_guess_price = True if "price" not in e_std or pd.isnull(e_std["price"]) or e_std["price"] <= 0 else False
    b_guess_lot_size = True if "lot_size" not in e_std or pd.isnull(e_std["lot_size"]) or e_std["lot_size"] <= 0 else False
    b_guess_living_area = True if "living_area" not in e_std or pd.isnull(e_std["living_area"]) or e_std["living_area"] <= 0 else False
    b_guess_rent = True if "rent" not in e_std or pd.isnull(e_std["rent"]) or e_std["rent"] <= 0 else False
    
    for s_amenity in a_ALWAYS_AMENITIES:
        if s_amenity not in e_std: e_std[s_amenity] = 1
    for s_amenity in a_NEVER_ASSUME_AMENITIES:
        if s_amenity not in e_std: e_std[s_amenity] = 0
    
    for s_service in a_ALWAYS_SERVICES:
        if s_service not in e_std: e_std[s_service] = 1
    for s_service in a_NEVER_ASSUME_SERVICES:
        if s_service not in e_std: e_std[s_service] = 0
    
    
    
    #e_output, e_pai = location_analysis.add_area_info_to_dict(e_std, prep_zillow.o_AREA_LOOKUPS, **kwargs)
    e_output, e_pai = location_analysis.add_multi_area_info_to_dict(e_std, prep_zillow.o_AREA_LOOKUPS, prep_zillow.o_LTR_AREA_LOOKUPS, **kwargs)
    
    e_result["data"] = e_output
    e_result["pai"] = e_pai
    #print("e_output", e_output)
    
    if e_output.get("feature_pool"):
        e_output["location_pool"] = e_output["feature_pool"]
    
    if not b_guess_price:
        assert e_output["price"] > 0
    if not b_guess_lot_size:
        assert e_output["lot_size"] > 0
    if not b_guess_living_area:
        assert e_output["living_area"] > 0
    if not b_guess_rent:
        assert e_output["rent"] > 0
    
    for col in e_VARS["train_cols"]:
        if col not in e_output and col.startswith(("property_type", "topography_")):
            e_output[col] = 0
    
    if e_std.get("property_type"):
        e_output[f'property_type_{e_std["property_type"]}'] = 1
    
    e_hotel_input = prep_zillow.o_HOTELS.find_nearest_hotels_to_by_coordinates(e_std["latitude"], e_std["longitude"])
    e_output.update(e_hotel_input)
    
    e_lat_subs, e_long_subs = airbnb_standardization.subdivide_latitude_longitude_dict(
        e_output,
        e_VARS["gps_min_latitude"],
        e_VARS["gps_max_latitude"],
        e_VARS["gps_min_longitude"],
        e_VARS["gps_max_longitude"],
        n_subdivisions=e_VARS["gps_subdivisions"])
    e_output.update(e_lat_subs)
    e_output.update(e_long_subs)
    
    #print("b_guess_price", b_guess_price)
    if b_guess_price and (b_guess_lot_size or b_guess_living_area):
        e_output["price"] = location_analysis.guess_price(e_output)
        if b_guess_lot_size:
            e_output["lot_size"] = location_analysis.guess_lot_size(e_output)
        if b_guess_living_area:
            e_output["living_area"] = location_analysis.guess_living_area(e_output)
        e_output["price"] = location_analysis.guess_price(e_output)
    else:
        if b_guess_price:
            e_output["price"] = location_analysis.guess_price(e_output)
            assert not pd.isnull(e_output["price"]) and e_output["price"] > 0
        if b_guess_lot_size:
            e_output["lot_size"] = location_analysis.guess_lot_size(e_output)
            assert not pd.isnull(e_output["lot_size"]) and e_output["lot_size"] > 0
        if b_guess_living_area:
            e_output["living_area"] = location_analysis.guess_living_area(e_output)
            assert not pd.isnull(e_output["living_area"]) and e_output["living_area"] > 0
    #e_output["price_actual"] = e_output["price"]
    
    if b_guess_rent:
        e_output["rent"] = location_analysis.guess_rent(e_output)
        assert not pd.isnull(e_output["rent"]) and e_output["rent"] > 0
        
    a_zip_vals = e_address.get("address_components", {}).get("postal_code", [])
    #print("a_zip_vals", a_zip_vals)
    
    if e_output.get("zipcode"):
        a_zip_vals.append(e_output["zipcode"])
    if e_input.get("zipcode"):
        a_zip_vals.append(e_input["zipcode"])
    if e_input.get("zip_code"):
        a_zip_vals.append(e_input["zip_code"])
    if e_input.get("zip"):
        a_zip_vals.append(e_input["zip"])
    if e_input.get("postal_code"):
        a_zip_vals.append(e_input["postal_code"])
    
    if a_zip_vals:
        s_zip, s_zcta = None, None
        for x in a_zip_vals:
            s_zip = prep_zillow.zip_to_zcta(x)
            #print("s_zip", s_zip)
            s_zcta = prep_zillow.e_ZIP_TO_ZCTA.get(s_zip)
            if s_zcta: break
            #print("s_zcta", s_zcta)
        if s_zcta:
            e_address["zcta"] = s_zcta
            e_zcta = prep_zillow.e_ZCTA.get(s_zcta, {})
            #print("e_zcta", e_zcta)
            e_output["zcta_population"] = e_zcta.get("population", 0)
            e_output["zcta_houses"] = e_zcta.get("houses", 0)
            e_output["zcta_population_density"] = e_zcta.get("population_density", 0)
            e_output["zcta_housing_density"] = e_zcta.get("housing_density", 0)
            
            d_rate_occupancy = predict_zillow.calc_rate_occupancy(np.mean([e_output[f"{i}_area_rate"] for i in range (-1, 3) if e_output.get(f"{i}_area_rate")]))
            #d_population_density_occupancy = predict_zillow.calc_population_density_occupancy(e_output["zcta_population_density"])
            #d_housing_density_occupancy = predict_zillow.calc_housing_density_occupancy(e_output["zcta_housing_density"])
            d_price_occupancy = predict_zillow.calc_price_occupancy(e_output["price"])
            
            #print('e_output["occupancy"]', e_output["occupancy"])
            #print("d_rate_occupancy", d_rate_occupancy)
            #print("d_population_density_occupancy", d_population_density_occupancy)
            #print("d_housing_density_occupancy", d_housing_density_occupancy)
            
            #e_output["occupancy"] = np.mean([e_output["occupancy"], d_rate_occupancy, d_population_density_occupancy, d_housing_density_occupancy])
            e_output["occupancy"] = np.mean([e_output["occupancy"], d_rate_occupancy, d_price_occupancy])
            #print('e_output["occupancy"]', e_output["occupancy"])
        else:
            e_output["zcta"] = None
            e_output["zcta_population"] = 0
            e_output["zcta_houses"] = 0
            e_output["zcta_population_density"] = 0
            e_output["zcta_housing_density"] = 0
            
            pass
            print("zip_vals but no s_zcta:", a_zip_vals)
    else:
        print("no a_zip_vals")
        #print("e_address", e_address)
    
    #print("e_output max_guests", e_output["max_guests"])
    e_result["status"] = 200
    return e_result


def mean(a_vals, i_round=None):
    a_actual = [x for x in a_vals if not pd.isnull(x)]
    d_mean = sum(a_actual) / len(a_actual) if a_actual else np.nan
    if i_round is not None:
        return round(d_mean, i_round)
    else:
        return d_mean


#@utils.timer
def predict_single_property(e_input_prepped, e_pai, **kwargs):
    e_input_prepped = e_input_prepped.copy()
    #e_input_prepped.update(expand_pai(e_pai))
    
    #print("e_input_prepped", list(e_input_prepped.items()))
    
    df_zillow_inputs = pd.DataFrame([e_input_prepped])
    
    df_zillow_inputs.rename(columns={
        "latitude": 'Latitude',
        "longitude": 'Longitude',
        "property_category": "Property Category",
        "area_reservation_length": 'reservation_length',
        #"area_sqft_cap": 'sqft_cap',
        "area_cap_sqft": 'cap_sqft',
        #'property_type'
    }, inplace=True)
    
    df_zillow_inputs = airbnb_standardization.prep_for_dummies(df_zillow_inputs[[col for col in df_zillow_inputs if col in e_VARS["allowed_cols"] or col in e_LTR_VARS["allowed_cols"]]])
    
    df_zillow_inputs = df_zillow_inputs.T.reset_index().drop_duplicates(subset="index", keep='last').set_index("index").T
    
    c_missing = set(e_VARS["train_cols"]) - set(df_zillow_inputs.columns)
    if c_missing:
        print("missing STR training cols", sorted(c_missing))
    
    c_ltr_missing = set(e_LTR_VARS["train_cols"]) - set(df_zillow_inputs.columns)
    if c_ltr_missing:
        print("missing LTR training cols", sorted(c_ltr_missing))
    assert not c_missing and not c_ltr_missing
    
    np_str_raw = df_zillow_inputs[e_VARS["train_cols"]].values
    #print("np_str_raw", np_str_raw.shape)
    np_str_raw1 = np.resize(np_str_raw, (1 + len(a_STR_FEATURES), np_str_raw.shape[1]))
    
    np_ltr_raw = df_zillow_inputs[e_LTR_VARS["train_cols"]].values
    #print("np_ltr_raw", np_ltr_raw.shape)
    np_ltr_raw1 = np.resize(np_ltr_raw, (1 + len(a_LTR_FEATURES), np_ltr_raw.shape[1]))
    
    # try out other possible improvements
    for i_feature, (i_col, s_col) in enumerate(a_STR_FEATURES):
        i_row = i_feature + 1
        if np_str_raw1[i_row, i_col] < 1:
            np_str_raw1[i_row, i_col] = 1
    for i_feature, (i_col, s_col) in enumerate(a_LTR_FEATURES):
        i_row = i_feature + 1
        if np_ltr_raw1[i_row, i_col] < 1:
            np_ltr_raw1[i_row, i_col] = 1
    
    X_str = predict_zillow.o_SCALER.transform(np_str_raw1)
    X_ltr = predict_zillow.o_LTR_SCALER.transform(np_ltr_raw1)
    
    e_predictions = {}
    Yhat_str_mape = predict_zillow.o_MAPE_MODEL.predict(X_str)
    e_predictions["predicted_revenue_mape"] = round(float(Yhat_str_mape[0, 0]), 2)
    #print("\tpredicted_revenue_mape", e_predictions["predicted_revenue_mape"])
    
    Yhat_str_mape_rate = predict_zillow.o_MAPE_RATE_MODEL.predict(X_str)
    e_predictions["predicted_avg_rate_mape"] = round(float(Yhat_str_mape_rate[0, 0]), 2)
    #print("\tpredicted_avg_rate_mape", e_predictions["predicted_avg_rate_mape"])
    
    #Yhat_str_mape_long_term_rent = predict_zillow.o_MAPE_RENT_MODEL.predict(X_str)
    #e_predictions["predicted_rent_mape"] = round(float(Yhat_str_mape_long_term_rent[0, 0]), 2)
    #a_rent_factors = [e_predictions["predicted_rent_mape"]]
    ##print("\tpredicted_rent_mape", e_predictions["predicted_rent_mape"])
    
    Yhat_ltr_mape_long_term_rent = predict_zillow.o_MAPE_RENT_MODEL.predict(X_ltr)
    e_predictions["predicted_rent_mape"] = round(float(Yhat_ltr_mape_long_term_rent[0, 0]), 2)
    a_rent_factors = [e_predictions["predicted_rent_mape"]]
    #print("\tpredicted_rent_mape", e_predictions["predicted_rent_mape"])
    
    # find top features
    df_str_mape = pd.DataFrame(Yhat_str_mape, columns=["predicted_revenue_mape"])
    df_str_mape["feature"] = [None] + [feature for col, feature in a_STR_FEATURES]
    df_str_mape["delta"] = (df_str_mape["predicted_revenue_mape"] - float(Yhat_str_mape[0, 0])) / float(Yhat_str_mape[0, 0])
    df_str_mape = df_str_mape[df_str_mape["delta"] > db_tables.e_CONFIG.get("top_improvements_str_min_percent", 0.0)] #.sort_values("delta", ascending=False)
    e_str_improvements = df_str_mape.set_index('feature')["delta"].to_dict()
    #print("e_str_improvements", e_str_improvements)
    a_str_improvements = [
        (
            re.sub("^feature_", "", s_feature).replace("_", " "),
            round(d_improv, 8) if s_feature not in e_TARGET_IMPROVEMENTS
            else round(max(e_TARGET_IMPROVEMENTS[s_feature][1], (d_improv + e_TARGET_IMPROVEMENTS[s_feature][0]) / 2), 8)
        )
        for s_feature, d_improv in e_str_improvements.items()]
    a_str_improvements.sort(key=itemgetter(1), reverse=True)
    
    df_ltr_mape = pd.DataFrame(Yhat_ltr_mape_long_term_rent, columns=["predicted_rent_mape"])
    df_ltr_mape["feature"] = [None] + [feature for col, feature in a_LTR_FEATURES]
    df_ltr_mape["delta"] = (df_ltr_mape["predicted_rent_mape"] - float(Yhat_ltr_mape_long_term_rent[0, 0])) / float(Yhat_ltr_mape_long_term_rent[0, 0])
    df_ltr_mape = df_ltr_mape[df_ltr_mape["delta"] > db_tables.e_CONFIG.get("top_improvements_ltr_min_percent", 0.0)]#.sort_values("delta", ascending=False)
    e_ltr_improvements = df_ltr_mape.set_index('feature')["delta"].to_dict()
    a_ltr_improvements = [
        (
            re.sub("^feature_", "", s_feature).replace("_", " "),
            round(d_improv, 8) if s_feature not in e_TARGET_IMPROVEMENTS
            else round(max(e_TARGET_IMPROVEMENTS[s_feature][1], (d_improv + e_TARGET_IMPROVEMENTS[s_feature][0]) / 2), 8)
        )
        for s_feature, d_improv in e_ltr_improvements.items()]
    a_ltr_improvements.sort(key=itemgetter(1), reverse=True)
    
    e_predictions.update({
        "area_occupancy": round(e_input_prepped["area_occupancy"], 3),
        "area_bedrooms": round(e_input_prepped["area_bedrooms"], 1),
        "area_bathrooms": round(e_input_prepped["area_bathrooms"], 1),
        "area_revenue": round(e_input_prepped["area_revenue"], 2),
        "area_vs_mean": round(e_input_prepped["area_rate_vs_mean"], 3),
        "area_precision": e_input_prepped["area_precision"],
        "area_scale": location_analysis.e_PREC_SIZE_IMP.get(e_input_prepped["area_precision"], ""),
        "area_properties": e_input_prepped["area_properties"],
        #"area_vs_mean": round(e_input_prepped["area_rate_vs_mean"], 3),
    })
    
    #d_rate_occupancy = predict_zillow.calc_rate_occupancy((e_predictions["predicted_avg_rate_mape"] + e_predictions["predicted_listed_rate_mae"]) / 2)
    d_rate_occupancy = predict_zillow.calc_rate_occupancy(e_predictions["predicted_avg_rate_mape"])
    #d_population_density_occupancy = predict_zillow.calc_population_density_occupancy(e_input_prepped["zcta_population_density"])
    #d_housing_density_occupancy = predict_zillow.calc_housing_density_occupancy(e_input_prepped["zcta_housing_density"])
    d_price_occupancy = predict_zillow.calc_price_occupancy(e_input_prepped["price"])
    
    a_occupancy_factors = [
        e_predictions["predicted_revenue_mape"] / (e_predictions["predicted_avg_rate_mape"]) / 365,
        np.mean([
            d_rate_occupancy,
            #d_population_density_occupancy,
            #d_housing_density_occupancy
            d_price_occupancy,
        ]),
        e_predictions["area_occupancy"],
        ]
    
    e_predictions["predicted_occupancy"] = min(predict_zillow.d_PREDICT_AVAILABILITY, sum(a_occupancy_factors) / len(a_occupancy_factors))
    
    e_predictions["predicted_occupancy"] = round(e_predictions["predicted_occupancy"] ** e_PREDICT_OCCUPANCY_ADJ_EXP.get(e_input_prepped["area_precision"], 1), 3)
    
    if kwargs.get("b_debug"): print("\tpredicted_occupancy", e_predictions["predicted_occupancy"])
    
    #df_zillow_inputs["predicted_occupancy_optimized"] = df_zillow_inputs["predicted_occupancy"] ** d_PREDICT_OCCUPANCY_OPT_EXP
    e_predictions["predicted_occupancy_optimized"] = round(e_predictions["predicted_occupancy"] ** e_PREDICT_OCCUPANCY_OPT_EXP.get(e_input_prepped["area_precision"], 1), 3)
    
    #e_pai_filled = expand_pai(e_pai)
    #print("e_pai", e_pai)
    
    e_predictions["area_bedrooms"] = mean([e_pai.get(f"{i}_area_bedrooms") for i in range(-1, 3 + 1)], i_round=1)
    e_predictions["area_bathrooms"] = mean([e_pai.get(f"{i}_area_bathrooms") for i in range(-1, 3 + 1)], i_round=1)
    e_predictions["area_occupancy"] = mean([e_pai.get(f"{i}_area_occupancy") for i in range(-1, 1 + 1)], i_round=3)
    e_predictions["area_revenue"] = mean([e_pai.get(f"{i}_area_revenue") for i in range(-1, 2 + 1)], i_round=2)
    e_predictions["area_rent"] = mean([e_pai.get(f"{i}_area_rent") for i in range(-2, 2 + 1) if e_pai.get(f"{i}_area_rent", 0) > 0][-2:], i_round=2)
    e_predictions["area_rent_vs_mean"] = e_predictions["area_rent"] / prep_zillow.o_AREA_LOOKUPS.areas[prep_zillow.o_AREA_LOOKUPS.default_location]["area_rent"]
    e_predictions["area_vs_mean"] = mean([e_pai.get(f"{i}_area_rate_vs_mean") for i in range(-1, 1 + 1)], i_round=2)
    e_predictions["area_occupancy_vs_mean"] = round(e_predictions["area_occupancy"] / e_pai.get(f"-3_area_occupancy", 0.21), 3)
    #e_predictions["area_occupancy_vs_mean"] = mean([e_pai.get(f"{i}_area_occupancy_vs_mean") for i in range(0, 3 + 1)], i_round=1)
    e_predictions["area_rev_per_bedroom"] = mean([e_pai.get(f"{i}_area_rev_per_bedroom") for i in range(-1, 3 + 1)], i_round=2)
    e_predictions["area_rev_per_bathroom"] = mean([e_pai.get(f"{i}_area_rev_per_bathroom") for i in range(-1, 3 + 1)], i_round=2)
    
    e_predictions["area_avg_rate"] = mean([e_pai.get(f"{i}_area_rate") for i in range(-1, 3 + 1)], i_round=2)
    e_predictions["area_rate_per_bedroom"] = mean([e_pai.get(f"{i}_area_rate_per_bedroom") for i in range(-1, 3 + 1)], i_round=2)
    #e_predictions["area_rate_per_bathroom"] = round(e_predictions["area_rate"] / e_predictions["area_bathrooms"], 2)
    e_predictions["area_rate_per_bathroom"] = mean([e_pai.get(f"{i}_area_rate_per_bathroom") for i in range(-1, 3 + 1)], i_round=2)
    #e_predictions["area_listed_rate"] = mean([e_pai.get(f"{i}_area_listed_rate") for i in range(-1, 3 + 1)], i_round=2)
    #e_predictions["area_listed_rate_per_bedroom"] = round(e_predictions["area_listed_rate"] / e_predictions["area_bedrooms"], 2)
    #e_predictions["area_listed_rate_per_bedroom"] = mean([e_pai.get(f"{i}_area_listed_rate_per_bedroom") for i in range(0, 3 + 1)], i_round=2)
    
    e_predictions["area_rev_per_sqft"] = mean([e_pai.get(f"{i}_area_rev_per_sqft") for i in range(-1, 3 + 1)], i_round=2)
    e_predictions["area_price_per_sqft"] = mean([e_pai.get(f"{i}_area_price_per_sqft") for i in range(-1, 3 + 1)], i_round=2)
    e_predictions["area_cap_sqft"] = e_predictions["area_rev_per_sqft"] / e_predictions["area_price_per_sqft"] if e_predictions["area_price_per_sqft"] > 0 else 0
    
    e_predictions["area_seasonality"] = mean([e_pai.get(f"{i}_area_demand_seasonality") for i in range(-1, 1 + 1)], i_round=2)
    e_predictions["area_seasonality_normalized"] = max(0, 0.368 * math.log(e_predictions["area_seasonality"]) + 0.9771 if e_predictions["area_seasonality"] > 0 else 0)
    
    e_predictions["area_season_length"] = mean([e_pai.get(f"{i}_area_demand_season_length") for i in range(-1, 1 + 1)], i_round=1)
    a_scale_peak_months = [
        month for month in [
            e_pai.get(f"{i}_area_demand_season_peak_month") for i in range(-2, 1 + 1)]
        if not pd.isnull(month) and month > 0 and month <= 12
    ]
    if a_scale_peak_months:
        e_predictions["area_season_peak_month"] = a_scale_peak_months[-1]
    e_predictions["area_season_peak_month_name"] = e_MONTHS.get(int(e_predictions.get("area_season_peak_month", 0)), "")
    
    a_scale_max_months = [
        month for month in [
            e_pai.get(f"{i}_area_demand_season_max_month") for i in range(-2, 1 + 1)]
        if not pd.isnull(month) and month > 0 and month <= 12
    ]
    if a_scale_max_months:
        e_predictions["area_season_max_month"] = a_scale_max_months[-1]
    e_predictions["area_season_max_month_name"] = e_MONTHS.get(int(e_predictions.get("area_season_max_month", 0)), "")
    
    e_predictions["predicted_revenue_avg_rate"] = round(
        (e_predictions["predicted_avg_rate_mape"] * e_predictions["predicted_occupancy"] * 365), 2)
    
    #e_predictions["predicted_revenue_listed_rate"] = round(
    #    (e_predictions["predicted_listed_rate_mae"] * e_predictions["predicted_occupancy"] * 365), 2)
    
    #e_predictions["calc_revenue_avg_rate"] = round(
    #    (((e_predictions["area_rate_per_bedroom"] * e_input_prepped["bedrooms"]) +
    #     (e_predictions["area_rate_per_bathroom"] * e_input_prepped["bathrooms"])) / 2) *
    #    (e_predictions["area_occupancy"] * 365), 2)
    
    #e_predictions["calc_revenue_listed_rate"] = round(
    #    (e_predictions["area_listed_rate_per_bedroom"] * e_input_prepped["bedrooms"]) *
    #    (e_predictions["area_occupancy"] * 365), 2)
    
    e_predictions["calc_revenue_avg_rate"] = round(
        (((e_predictions["area_rate_per_bedroom"] * e_input_prepped["bedrooms"]) +
         (e_predictions["area_rate_per_bathroom"] * e_input_prepped["bathrooms"])) / 2) *
        (e_predictions["predicted_occupancy"] * 365), 2)
    if kwargs.get("b_debug"): print("\tcalc_revenue_avg_rate", e_predictions["calc_revenue_avg_rate"])
    
    #e_predictions["calc_revenue_listed_rate"] = round(
    #    (e_predictions["area_listed_rate_per_bedroom"] * e_input_prepped["bedrooms"]) *
    #    (e_predictions["predicted_occupancy"] * 365), 2)
    
    #df_zillow_inputs["calc_revenue_listed_rate_generic_house"] = (df_zillow_inputs["area_listed_rate"] * (df_zillow_inputs["area_occupancy"] * 365)).round(2)
    
    e_predictions["calc_revenue_from_price"] = predict_zillow.predict_price_revenue_median(e_input_prepped["price"])
    e_predictions["calc_revenue_from_price"] *= e_predictions["area_vs_mean"]
    e_predictions["calc_revenue_from_price"] = round(e_predictions["calc_revenue_from_price"], 2)
    if kwargs.get("b_debug"): print("\tcalc_revenue_from_price", e_predictions["calc_revenue_from_price"])
    
    d_sanity_adjustment_avg, d_sanity_adjustment_prod = defaults.get_sanity_factors(e_input_prepped)
    
    if d_sanity_adjustment_prod < 1:
        e_predictions["calc_revenue_from_price"] *= d_sanity_adjustment_prod
        if kwargs.get("b_debug"): print("\tcalc_revenue_from_price adjusted for sanity factors", e_predictions["calc_revenue_from_price"])
        e_predictions["calc_revenue_avg_rate"] *= d_sanity_adjustment_prod
        if kwargs.get("b_debug"): print("\tcalc_revenue_avg_rate adjusted for sanity factors", e_predictions["calc_revenue_avg_rate"])
    elif d_sanity_adjustment_prod > 1:
        e_predictions["calc_revenue_from_price"] *= d_sanity_adjustment_prod ** 0.75
        if kwargs.get("b_debug"): print("\tcalc_revenue_from_price adjusted for sanity factors", e_predictions["calc_revenue_from_price"])
        e_predictions["calc_revenue_avg_rate"] *= d_sanity_adjustment_prod ** 0.75
        if kwargs.get("b_debug"): print("\tcalc_revenue_avg_rate adjusted for sanity factors", e_predictions["calc_revenue_avg_rate"])
    
    e_predictions["predicted_revenue"] = mean([
        x for x in [
            e_predictions["predicted_revenue_mape"],
            e_predictions["predicted_revenue_mape"],
            #e_predictions["predicted_revenue_mae"],
            e_predictions["predicted_revenue_avg_rate"],
            #e_predictions["predicted_revenue_listed_rate"],
            e_predictions["calc_revenue_from_price"],
            e_predictions["calc_revenue_avg_rate"],
            #e_predictions["calc_revenue_listed_rate"],
            ]
        if not pd.isnull(x) and x > 0
        ],
        i_round=2)
    
    e_predictions["predicted_revenue"] = round(e_predictions["predicted_revenue"] ** e_PREDICT_OCCUPANCY_ADJ_EXP.get(e_input_prepped["area_precision"], 1), 2)
        
    if kwargs.get("b_debug"): print("\tpredicted_revenue", e_predictions["predicted_revenue"])
    
    d_sanity_number = mean([
        x for x in [
            e_predictions["calc_revenue_from_price"],
            e_predictions["calc_revenue_avg_rate"],
            #e_predictions["calc_revenue_listed_rate"],
            ]
        if not pd.isnull(x) and x > 0
        ],
        i_round=2)
    if kwargs.get("b_debug"): print("\tsanity_number", d_sanity_number)
    
    #if d_sanity_adjustment_avg < 1:
    #    #d_sanity_number *= np.mean([d_sanity_adjustment_avg, d_sanity_adjustment_prod]) ** 0.75
    #    d_sanity_number *= d_sanity_adjustment_prod ** 0.75
    #elif d_sanity_adjustment_avg > 1:
    #    #d_sanity_number *= np.mean([d_sanity_adjustment_avg, d_sanity_adjustment_prod]) ** 0.5
    #    d_sanity_number *= d_sanity_adjustment_prod ** 0.5
    
    
    d_rev_std = defaults.calc_rev_cap_std(d_sanity_number) * d_sanity_number
    #d_cap_mean = predict_zillow.calc_cap_mean(d_sanity_number)
    #d_cap_std = predict_zillow.calc_cap_std(d_sanity_number)
    #print("\td_rev_std", round(d_rev_std, 2))
    
    # use sanity check to clip predictions
    e_predictions["predicted_revenue"] = min(
        e_predictions["predicted_revenue"],
        e_input_prepped["price"] * db_tables.e_CONFIG.get("predict_max_percent_of_price", 0.5) if e_input_prepped.get("price") else e_predictions["predicted_revenue"],
        round(d_sanity_number + d_rev_std * 5, 2))
        #round(d_sanity_number * 2.5, 2))
    
    e_predictions["predicted_revenue"] = max(
        e_predictions["predicted_revenue"],
        round(d_sanity_number - d_rev_std * 5, 2))
        #round(d_sanity_number * 0.4, 2))
    
    if kwargs.get("b_debug"): print("\tpredicted_revenue adjusted for price", e_predictions["predicted_revenue"])
    
    d_rev = e_predictions["predicted_revenue"]
    #print("predicted_revenue", d_rev)
    assert isinstance(d_rev, float)
    d_area_rev = e_predictions["area_revenue"]
    assert isinstance(d_area_rev, float)
    i_precision = e_input_prepped["area_precision"]
    i_properties = e_input_prepped["area_properties"]
    #print({
    #    "d_area_rev": d_area_rev,
    #    "i_precision": i_precision,
    #    "i_properties": i_properties,
    #})
    
    e_predictions["predicted_revenue"] = round(
        d_rev ** 0.95 if d_area_rev < 1000 else
        d_rev ** 0.98 if i_precision < 1 or (i_precision == 0 and i_properties < 5) else
        d_rev, 2)
    if kwargs.get("b_debug"): print("\tpredicted_revenue adjusted for area properties and precision", e_predictions["predicted_revenue"])
    
    e_predictions["avg_rate_at_predicted_revenue"] = round(e_predictions["predicted_revenue"] / (e_predictions["predicted_occupancy"] * 365), 2)
    
    e_predictions["predicted_revenue_optimized"] = round(
        e_predictions["avg_rate_at_predicted_revenue"] * e_predictions["predicted_occupancy_optimized"] * 365, 2)
    
    # allow for configurable adjustment factors in db
    e_predictions["predicted_revenue"] = round(e_predictions["predicted_revenue"] * db_tables.e_CONFIG.get("predict_adjust_str_daily_rate", 1.0) * db_tables.e_CONFIG.get("predict_adjust_str_occupancy", 1.0), 2)
    e_predictions["predicted_revenue_optimized"] = round(e_predictions["predicted_revenue_optimized"] * db_tables.e_CONFIG.get("predict_adjust_str_daily_rate", 1.0) * db_tables.e_CONFIG.get("predict_adjust_str_occupancy", 1.0), 2)
    e_predictions["avg_rate_at_predicted_revenue"] = round(e_predictions["avg_rate_at_predicted_revenue"] * db_tables.e_CONFIG.get("predict_adjust_str_daily_rate", 1.0), 2)
    e_predictions["predicted_occupancy"] = e_predictions["predicted_occupancy"] * db_tables.e_CONFIG.get("predict_adjust_str_occupancy", 1.0)
    e_predictions["predicted_occupancy_optimized"] = e_predictions["predicted_occupancy_optimized"] * db_tables.e_CONFIG.get("predict_adjust_str_occupancy", 1.0)
    
    if "price" in e_input_prepped:
        e_predictions["pred_vs_price"] = e_predictions["predicted_revenue"] / e_input_prepped["price"]
        e_predictions["pred_vs_price_optimized"] = e_predictions["predicted_revenue_optimized"] / e_input_prepped["price"]
        
        if e_input_prepped.get("fixer_upper") in {1, True, "True", "true"}:
            e_predictions["pred_vs_price"] *= 0.45
            e_predictions["predicted_revenue"] *= 0.45
            e_predictions["pred_vs_price_optimized"] *= 0.45
            e_predictions["predicted_revenue_optimized"] *= 0.45
        
        if e_input_prepped.get("manufactured_home") in {1, True, "True", "true"}:
            e_predictions["pred_vs_price"] *= 0.5
            e_predictions["predicted_revenue"] *= 0.5
            e_predictions["pred_vs_price_optimized"] *= 0.5
            e_predictions["predicted_revenue_optimized"] *= 0.5
        
        if e_input_prepped.get("potential_issues") in {1, True, "True", "true"}:
            e_predictions["pred_vs_price"] *= 0.75
            e_predictions["predicted_revenue"] *= 0.75
            e_predictions["pred_vs_price_optimized"] *= 0.75
            e_predictions["predicted_revenue_optimized"] *= 0.75
        
        if (
            e_input_prepped.get("condemned") in {1, True, "True", "true"}
            or e_input_prepped.get("auction") in {1, True, "True", "true"}
            or e_input_prepped.get("shared_ownership") in {1, True, "True", "true"}
            or e_input_prepped.get("move_home") in {1, True, "True", "true"}
            or e_input_prepped.get("lot_only") in {1, True, "True", "true"}
            or e_input_prepped.get("timeshare") in {1, True, "True", "true"}
            or e_input_prepped.get("no_rentals") in {1, True, "True", "true"}
        ):
            e_predictions["pred_vs_price"] = 0.0
            e_predictions["predicted_revenue"] = 0.0
            e_predictions["pred_vs_price_optimized"] = 0.0
            e_predictions["predicted_revenue_optimized"] = 0.0
    else:
        if e_input_prepped.get("fixer_upper") in {1, True, "True", "true"}:
            e_predictions["predicted_revenue"] *= 0.45
            e_predictions["predicted_revenue_optimized"] *= 0.45
        
        if e_input_prepped.get("manufactured_home") in {1, True, "True", "true"}:
            e_predictions["predicted_revenue"] *= 0.5
            e_predictions["predicted_revenue_optimized"] *= 0.5
        
        if e_input_prepped.get("potential_issues") in {1, True, "True", "true"}:
            e_predictions["predicted_revenue"] *= 0.75
            e_predictions["predicted_revenue_optimized"] *= 0.75
        
        if e_input_prepped.get("insufficient_description"):
            e_predictions["predicted_revenue"] *= 1 - (0.5 * e_input_prepped["insufficient_description"])
            e_predictions["predicted_revenue_optimized"] *= 1 - (0.5 * e_input_prepped["insufficient_description"])
        
        if (
            e_input_prepped.get("condemned") in {1, True, "True", "true"}
            or e_input_prepped.get("auction") in {1, True, "True", "true"}
            or e_input_prepped.get("shared_ownership") in {1, True, "True", "true"}
            or e_input_prepped.get("move_home") in {1, True, "True", "true"}
            or e_input_prepped.get("lot_only") in {1, True, "True", "true"}
            or e_input_prepped.get("timeshare") in {1, True, "True", "true"}
            or e_input_prepped.get("no_rentals") in {1, True, "True", "true"}
        ):
            e_predictions["predicted_revenue"] = 0.0
            e_predictions["predicted_revenue_optimized"] = 0.0
    
    e_predictions["predicted_revenue"] = round(e_predictions["predicted_revenue"], 2)
    e_predictions["predicted_revenue_optimized"] = round(e_predictions["predicted_revenue_optimized"], 2)
    
    a_rent_factors.append(e_predictions["area_rent_vs_mean"] * ((e_input_prepped["price"] * 0.006014) + 603.87))
    a_rent_factors.append(defaults.calc_rent_cap(e_input_prepped["price"]) * e_input_prepped["price"])
    
    e_predictions["price_per_sqft"] = 0
    e_predictions["cap_sqft"] = 0
    e_predictions["cap_sqft_optimized"] = 0
    
    if e_input_prepped.get("living_area", 0) > 0 and e_input_prepped.get("price", 0) > 0:
        e_predictions["price_per_sqft"] = round(e_input_prepped["price"] / e_input_prepped["living_area"], 2)
    
    if e_input_prepped.get("living_area", 0) > 0 and e_predictions["predicted_revenue"] > 0 :
        e_predictions["rev_per_sqft"] = round(e_predictions["predicted_revenue"] / e_input_prepped["living_area"], 2)
        e_predictions["optimized_rev_per_sqft"] = round(e_predictions["predicted_revenue_optimized"] / e_input_prepped["living_area"], 2)
        
        if e_predictions.get("price_per_sqft", 0) > 0:
            e_predictions["cap_sqft"] = round(e_predictions["rev_per_sqft"] / e_predictions["price_per_sqft"], 8)
            e_predictions["cap_sqft_optimized"] = round(e_predictions["optimized_rev_per_sqft"] / e_predictions["price_per_sqft"], 8)
        
        if e_input_prepped["living_area"] <= 5750:
            #a_rent_factors.append(1272.9 * math.exp(0.000549 * e_input_prepped["living_area"]))
            a_rent_factors.append(e_predictions["area_rent_vs_mean"] * (1283.956 * math.exp(0.000538 * e_input_prepped["living_area"])))
        else:
            a_rent_factors.append(e_predictions["area_rent_vs_mean"] * (1283.956 * math.exp(0.000538 * 5750)))
    
    if e_input_prepped.get("lot_size") and e_input_prepped["lot_size"] > 0:
        if e_input_prepped["lot_size"] <= 0.75:
            a_rent_factors.append(e_predictions["area_rent_vs_mean"] * ((e_input_prepped["lot_size"] * 6912.6) + 1717.1))
        else:
            a_rent_factors.append(e_predictions["area_rent_vs_mean"] * ((0.75 * 6912.6) + 1717.1))
    
    if e_input_prepped["bedrooms"] <= 10:
        a_rent_factors.append(e_predictions["area_rent_vs_mean"] * (874.88 * math.exp(0.4798 * e_input_prepped["bedrooms"])))
    else:
        a_rent_factors.append(e_predictions["area_rent_vs_mean"] * (874.88 * math.exp(0.4798 * 10)))
    
    if e_input_prepped["bathrooms"] <= 9:
        a_rent_factors.append(e_predictions["area_rent_vs_mean"] * (1191.7 * math.exp(0.5114 * e_input_prepped["bathrooms"])))
    else:
        a_rent_factors.append(e_predictions["area_rent_vs_mean"] * (1191.7 * math.exp(0.5114 * 9)))
    
    if kwargs.get("b_debug"): print("\tpredicted_rent_mape", e_predictions["predicted_rent_mape"])
    #print("a_rent_factors", a_rent_factors)
    e_predictions["calculated_rent"] = mean([x for x in a_rent_factors[1:] if not pd.isnull(x) and x > 0], i_round=2)
    if kwargs.get("b_debug"): print("\tcalculated_rent", e_predictions["calculated_rent"])
    
    if d_sanity_adjustment_prod < 1:
        e_predictions["calculated_rent"] *= d_sanity_adjustment_prod
        if kwargs.get("b_debug"): print("\tcalculated_rent adjusted for sanity factors", e_predictions["calculated_rent"])
    elif d_sanity_adjustment_prod > 1:
        e_predictions["calculated_rent"] *= d_sanity_adjustment_prod ** 0.75
        if kwargs.get("b_debug"): print("\tcalculated_rent adjusted for sanity factors", e_predictions["calculated_rent"])
    e_predictions["calculated_rent"] = round(e_predictions["calculated_rent"], 2)
    e_predictions["predicted_rent"] = round(mean(
        [
            e_predictions["predicted_rent_mape"],
            e_predictions["predicted_rent_mape"],
            e_predictions["calculated_rent"]
        ],
        i_round=2) * db_tables.e_CONFIG.get("predict_adjust_ltr_rent", 1.0), 2)
    #if kwargs.get("b_debug"): print("\tpredicted_rent", e_predictions["predicted_rent"])
    
    d_rent_sanity_number = mean(a_rent_factors, i_round=2)
    if e_predictions.get("price"):
        #d_rent_cap_std = defaults.calc_rent_cap_std(e_predictions["price"]) * d_rent_sanity_number
        d_rent_cap_std = defaults.calc_rent_cap_std(e_predictions["price"]) * e_predictions["price"]
    else:
        d_rent_cap_std = 0.2 * d_rent_sanity_number
    
    if d_rent_sanity_number > 0:
        # use sanity check to clip predictions
        e_predictions["predicted_rent"] = min([
            e_predictions["predicted_rent"],
            #e_predictions["price"] * 0.5 if e_predictions.get("price") else e_predictions["predicted_rent"],
            round(d_rent_sanity_number * 2, 2),
            round(d_rent_sanity_number + d_rent_cap_std * 3, 2),
            (e_input_prepped["price"] * db_tables.e_CONFIG.get("predict_ltr_max_percent_of_price", 0.25)) / 12 if e_input_prepped.get("price") else e_predictions["predicted_rent"],
            
        ])
        e_predictions["predicted_rent"] = round(max([
            e_predictions["predicted_rent"],
            d_rent_sanity_number * 0.4,
        ]), 2)
        if kwargs.get("b_debug"): print("\tpredicted_rent adjusted for sanity", e_predictions["predicted_rent"])
        
    
    e_predictions["predicted_rent_annual"] = round(e_predictions["predicted_rent"] * 12, 2)
    
    e_predictions["predicted_days_on_market"] = round(defaults.calc_rent_to_expected_days(e_predictions["predicted_rent"]), 2) * db_tables.e_CONFIG.get("predict_adjust_ltr_days_on_market", 1.0)
    e_predictions["predicted_rent_annual_first"] = round(e_predictions["predicted_rent_annual"] * (max(0, 365 - e_predictions["predicted_days_on_market"]) / 365), 2)
    
    
    # needs adjustment on days_on_market
    e_predictions["predicted_rent_annual"] = round(e_predictions["predicted_rent"] * 12, 2)
    if kwargs.get("b_debug"): print("\tpredicted_rent", e_predictions["predicted_rent"])
    
    return e_predictions, a_str_improvements, a_ltr_improvements


@utils.timer
def store_prediction(
    o_db,
    e_address,
    e_predictions,
    i_request_id=None,
    e_monthly_data=None,
    e_local_data=None,
    e_scores=None,
    e_ltr_scores=None,
    e_revenue_financing=None,
    e_revenue_optimized_financing=None,
    e_rent_financing=None,
    e_input_prepped=None,
    e_request=None,
    **kwargs
):
    e_result = {
        "address": json.dumps(utils.json_dict_prep(e_address)),
        "predictions": json.dumps(utils.json_dict_prep(e_predictions)),
    }
    if i_request_id:
        e_result["request_id"] = i_request_id
    
    if e_request:
        e_result["request"] = json.dumps(utils.json_dict_prep(e_request))
        
        for k in ["contact_id", "contact[id]", "user_id", "userid"]:
            if e_request.get(k):
                e_result["user_id"] = e_request[k]
                break
        
        for k in ["email", "contact[email", "user_email"]:
            if e_request.get(k):
                e_result["user_email"] = e_request[k]
                break

    if e_monthly_data:
        e_result["monthly_predictions"] = json.dumps(utils.json_dict_prep(e_monthly_data))
    
    if e_local_data:
        e_result["local_data"] = json.dumps(utils.json_dict_prep({k: v for k, v in e_local_data.items() if v}))
    
    if e_revenue_financing or e_revenue_optimized_financing or e_rent_financing:
        e_result["financing"] = json.dumps(utils.json_dict_prep({
            "predicted_revenue": e_revenue_financing,
            "predicted_revenue_optimized": e_revenue_optimized_financing,
            "predicted_rent": e_rent_financing,
        }))
    
    if e_scores:
        e_result["scores"] = json.dumps(utils.json_dict_prep({k: v for k, v in e_scores.items() if v}))
    
    if e_ltr_scores:
        e_result["ltr_scores"] = json.dumps(utils.json_dict_prep({k: v for k, v in e_ltr_scores.items() if v}))
    
    if e_input_prepped:
        e_result["inputs"] = json.dumps(utils.json_dict_prep(e_input_prepped))
        
        if not e_input_prepped.get("price", 0) > 0 and e_input_prepped.get("price_actual", 0) > 0:
            e_result["price"] = e_input_prepped["price_actual"]
    
    if e_address:
        if e_address.get("address_components", {}).get("state", []):
            e_result["state"] = e_address["address_components"]["state"][0]
        if e_address.get("address_components", {}).get("city", []):
            e_result["city"] = e_address["address_components"]["city"][0]
        elif e_address.get("address_components", {}).get("locality", []):
            e_result["city"] = e_address["address_components"]["locality"][0]
        if e_address.get("address_components", {}).get("postal_code", []):
            e_result["zipcode"] = e_address["address_components"]["postal_code"][0]
        if e_address.get("lat"):
            e_result["latitude"] = e_address["lat"]
        if e_address.get("lng"):
            e_result["longitude"] = e_address["lng"]
    
    if e_predictions:
        if e_predictions.get("area_properties"):
            e_result["area_properties"] = e_predictions["area_properties"]
        if e_predictions.get("predicted_revenue"):
            e_result["predicted_revenue"] = e_predictions["predicted_revenue"]
        if e_predictions.get("predicted_revenue_optimized"):
            e_result["predicted_revenue_optimized"] = e_predictions["predicted_revenue_optimized"]
        if e_predictions.get("avg_rate_at_predicted_revenue"):
            e_result["predicted_rate"] = e_predictions["avg_rate_at_predicted_revenue"]
        if e_predictions.get("predicted_rent"):
            e_result["predicted_rent"] = e_predictions["predicted_rent"]
        if e_predictions.get("predicted_occupancy"):
            e_result["predicted_occupancy"] = e_predictions["predicted_occupancy"]
        if e_predictions.get("area_properties"):
            e_result["area_properties"] = e_predictions["area_properties"]
        
        if e_predictions.get("pred_vs_price"):
            e_result["pred_vs_price"] = e_predictions["pred_vs_price"]
    
    #print("store_prediction", e_result)
    try:
        result_id = o_db.insert(
            "predictor_results",
            e_result,
            return_id=True,)
    except Exception as e:
        s_trace = traceback.print_exc()
        print(s_trace)
        print(e)
        
        result_id = None
    return result_id, e_result


def process_single_property(e_input, logger=None, **kwargs):
    n_start = time()
    e_result = prep_single_property(e_input, **kwargs)
    if e_result["status"] != 200:
        e_result["message"]["Example inputs"] = """See /interactive/examples for examples of both the minimum required fields, and all the potential optional fields that could be included.
            Field names are all lowercase. Either address or latitude/longitude are required, but you don't have to include both.
            Excluded features and location values will default to whatever is most common in the property's area.
            Excluded amenities default to true.
            Excluded conditions default to false."""
        
        if logger:
            logger.warning(f"{utils.get_time()}\tAPI User: {g.s_USER}. Problem prepping predictor with input {i_status} {json.dumps(e_input_prepped)}")
        #close_app_db()
        return e_result
        
    e_input_prepped = e_result["data"]
    e_pai = e_result["pai"]
    e_address = e_result["address"]
    e_valid_financing = e_result["financing"]
    
    #print("e_input_prepped", e_input_prepped, "\n")
    
    e_local_data = {}
    e_local_data = area_data.get_local_data(e_address, **kwargs)
    
    if not e_input_prepped.get("zcta"):
        if e_local_data.get("city"):
            e_input_prepped["zcta_population"] = int(e_local_data['city']['population'] / e_local_data['city']['zipcode_count'])
            e_input_prepped["zcta_houses"] = int(e_local_data['city']['houses'] / e_local_data['city']['zipcode_count'])
            e_input_prepped["zcta_population_density"] = e_local_data['city']['population_density']
            e_input_prepped["zcta_housing_density"] = e_local_data['city']['housing_density']
        elif e_local_data.get("state"):
            e_input_prepped["zcta_population"] = 0
            e_input_prepped["zcta_houses"] = 0
            e_input_prepped["zcta_population_density"] = e_local_data['state']['population_density']
            e_input_prepped["zcta_housing_density"] = e_local_data['state']['housing_density']
        elif e_local_data.get("national"):
            e_input_prepped["zcta_population"] = 0
            e_input_prepped["zcta_houses"] = 0
            e_input_prepped["zcta_population_density"] = e_local_data['national']['population_density']
            e_input_prepped["zcta_housing_density"] = e_local_data['national']['housing_density']
        
    
    e_predictions, a_str_improvements, a_ltr_improvements = predict_single_property(e_input_prepped, e_pai, **kwargs)
    
    a_states = e_address.get("address_components", {}).get("state", [])
    s_state = a_states[0] if a_states else ""
    #print("state", s_state)
    
    
    e_monthly_data = area_data.get_monthly_area_data(
        e_input_prepped["latitude"],
        e_input_prepped["longitude"],
        e_predictions["predicted_occupancy_optimized"],
        e_predictions["avg_rate_at_predicted_revenue"],
        s_state=s_state,
        e_local_data=e_local_data,
        **kwargs)
    
    if e_local_data.get("national", {}).get("revenue") and e_predictions.get("area_revenue"):
        e_predictions["area_vs_mean"] = e_predictions["area_revenue"] / e_local_data["national"]["revenue"]
    #print("area_rent_vs_mean", e_local_data.get("national", {}).get("revenue"), e_predictions.get("area_revenue"))
    if e_local_data.get("national", {}).get("rent") and e_predictions.get("area_rent"):
        e_predictions["area_rent_vs_mean"] = e_predictions["area_rent"] / e_local_data["national"]["rent"]

    if e_local_data.get("area_scores", {}).get("score_overall"):
        e_predictions["area_score_overall"] = e_local_data["area_scores"]["score_overall"]
    
    a_monthly_occupancy = [e_monthly_data["monthly_occupancy"].get(f"occupancy_month_{i+1}", 0) for i in range(12)]
    #a_monthly_rate = [e_monthly_data["monthly_rate"][f"rate_month_{i+1}"] for i in range(12)]
    
    #print("e_input_prepped", e_input_prepped)
    e_per_month_costs, a_cost_vals = scoring.get_per_month_costs(
        s_state if s_state else "_all_",
        #d_revenue=e_predictions["predicted_revenue_optimized"],
        d_occupancy=e_predictions["predicted_occupancy_optimized"],
        d_living_area=e_input_prepped["living_area"],
        d_lot_size=e_input_prepped["lot_size"],
        d_bedrooms=e_input_prepped["bedrooms"],
        d_bathrooms=e_input_prepped["bathrooms"],
        d_latitude=e_input_prepped["latitude"],
        a_monthly_occupancy=a_monthly_occupancy,
        #b_pool=True if e_input_prepped.get("features", {}).get("pool") in {True, 1, "1", "Yes", "true", "True", "TRUE"} else False
        b_pool=True if e_input_prepped.get("feature_pool") in {True, 1, "1", "Yes", "true", "True", "TRUE"} else False,
        b_hot_tub=True if e_input_prepped.get("feature_jacuzzi") in {True, 1, "1", "Yes", "true", "True", "TRUE"} else False,
        **kwargs
    )
    for i_mon, e_mon_cost in e_per_month_costs.items():
        e_mon_cost["listing_fee"] = round(e_monthly_data.get("monthly_revenue", {}).get(f"revenue_month_{i_mon}", 0) * scoring.d_LISTING_FEE_RATE, 2)
        e_mon_cost["management_fee"] = round(e_monthly_data.get("monthly_revenue", {}).get(f"revenue_month_{i_mon}", 0) * scoring.d_MANAGEMENT_FEE_RATE, 2)
        e_mon_cost["total"] += (e_mon_cost["listing_fee"] + e_mon_cost["management_fee"])
    
    e_monthly_data["monthly_costs"] = {
        #f"cost_month_{i_mon}": round(e_mon_cost["total"] + e_monthly_data.get("monthly_revenue", {}).get(f"revenue_month_{i_mon}", 0) * scoring.d_LISTING_FEE_RATE, 2)
        f"cost_month_{i_mon}": round(e_mon_cost["total"], 2)
        for i_mon, e_mon_cost in e_per_month_costs.items()}
    d_annual_cost = sum(e_monthly_data["monthly_costs"].values())
    
    e_annual_costs = Counter()
    #print("e_per_month_costs", e_per_month_costs)
    for i_mon, e_mon_cost in e_per_month_costs.items():
        for k, v in e_mon_cost.items():
            if isinstance(v, (int, float)):
                e_annual_costs[k] += v
    
    if "utilities" not in e_annual_costs:
        a_utilities = [e_annual_costs.get(x, 0) for x in scoring.a_UTILITY_COSTS]
        if a_utilities:
            e_annual_costs["utilities"] = sum(a_utilities)
    
    e_annual_costs = {k: round(v, 2) for k, v in e_annual_costs.items()}
    e_avg_monthly_costs = {k: round(v / 12, 2) for k, v in e_annual_costs.items()}
    e_avg_monthly_long_term_costs = {k: round(v / 12, 2) for k, v in e_annual_costs.items() if k in scoring.a_LONG_TERM_COSTS}
    
    #for i_mon, e_mon_cost in e_per_month_costs.items():
    #    e_mon_cost["listing_fee"] = round(e_monthly_data.get("monthly_revenue", {}).get(f"revenue_month_{i_mon}", 0) * scoring.d_LISTING_FEE_RATE, 2)
    #    e_mon_cost["total"] += e_mon_cost["listing_fee"]
    
    d_long_term_costs = round(sum([
        sum([v for k,v in e_mon_cost.items() if k in scoring.a_LONG_TERM_COSTS])
        for i_mon, e_mon_cost in e_per_month_costs.items()]
    ), 2)
    
    e_predictions["annual_expenses"] = round(d_annual_cost, 2)
    e_predictions["annual_expenses_long_term"] = round(d_long_term_costs, 2)
        
    #e_finance_inputs = e_input_prepped.get("financing", {})
    e_finance_inputs = {k: v for k, v in e_valid_financing.items()}
    #print("e_finance_inputs", e_finance_inputs)
    if not e_finance_inputs.get("price"):
        e_finance_inputs["price"] = e_input_prepped.get("price_actual", e_input_prepped.get("price", 0))
    if not e_finance_inputs.get("state"):
        e_finance_inputs["state"] = s_state
    
    #print("e_finance_inputs", e_finance_inputs)
    e_finance_inputs["property_tax_rate"] = area_data.get_local_property_tax_rate(e_local_data)
    
    #if not e_finance_inputs.get("annual_expenses"):
    #    e_finance_inputs["annual_expenses"] = d_annual_cost
    #if not e_finance_inputs.get("annual_expenses_long_term"):
    #    e_finance_inputs["annual_expenses_long_term"] = d_long_term_costs
    
    e_revenue_financing = financing_webhook.calc_finances(
        annual_revenue=e_predictions["predicted_revenue"],
        #annual_expenses=d_annual_cost,
        monthly_expenses=e_avg_monthly_costs,
        expense_factor=db_tables.e_CONFIG.get("financing_short_term_expense_factor"),
        management_fee_rate=scoring.d_MANAGEMENT_FEE_RATE,
        listing_fee_rate=scoring.d_LISTING_FEE_RATE,
        **e_finance_inputs)
    e_revenue_optimized_financing = financing_webhook.calc_finances(
        annual_revenue=e_predictions["predicted_revenue_optimized"],
        #annual_expenses=d_annual_cost,
        monthly_expenses=e_avg_monthly_costs,
        expense_factor=db_tables.e_CONFIG.get("financing_short_term_expense_factor"),
        management_fee_rate=scoring.d_MANAGEMENT_FEE_RATE,
        listing_fee_rate=scoring.d_LISTING_FEE_RATE,
        **e_finance_inputs)
    e_rent_financing = financing_webhook.calc_finances(
        annual_revenue=e_predictions["predicted_rent_annual"],
        #annual_expenses=d_long_term_costs,
        monthly_expenses=e_avg_monthly_long_term_costs,
        expense_factor=db_tables.e_CONFIG.get("financing_long_term_expense_factor"),
        management_fee_rate=0,
        listing_fee_rate=0,
        **e_finance_inputs)
    
    e_scores = scoring.score_property(
        e_address,
        e_input_prepped,
        e_predictions,
        e_local_data,
        e_per_month_costs=e_per_month_costs,
        **kwargs)
    
    for k in ['area_listed_rate', 'area_listed_rate_per_bedroom', 'calc_revenue_listed_rate', 'predicted_avg_rate_mae', 'predicted_listed_rate_mae', 'predicted_revenue_listed_rate', 'predicted_revenue_mae',]:
        if k not in e_predictions: e_predictions[k] = None
    
    
    if e_local_data:
        e_local_data = {k: v for k, v in e_local_data.items() if k != "closest_zipcodes"}
        
        if e_local_data.get("zip").get("rent_days_on_market") and e_local_data.get("city").get("rent_days_on_market"):
            e_predictions["predicted_days_on_market"] = round(np.mean([e_predictions["predicted_days_on_market"], e_local_data["zip"]["rent_days_on_market"], e_local_data["city"]["rent_days_on_market"]]), 1)
        elif e_local_data.get("zip").get("rent_days_on_market") and e_local_data.get("state").get("rent_days_on_market"):
            e_predictions["predicted_days_on_market"] = round(np.mean([e_predictions["predicted_days_on_market"], e_local_data["zip"]["rent_days_on_market"], e_local_data["state"]["rent_days_on_market"]]), 1)
        elif e_local_data.get("city").get("rent_days_on_market") and e_local_data.get("state").get("rent_days_on_market"):
            e_predictions["predicted_days_on_market"] = round(np.mean([e_predictions["predicted_days_on_market"], e_local_data["city"]["rent_days_on_market"], e_local_data["state"]["rent_days_on_market"]]), 1)
        elif e_local_data.get("state").get("rent_days_on_market"):
            e_predictions["predicted_days_on_market"] = round(np.mean([e_predictions["predicted_days_on_market"], e_local_data["state"]["rent_days_on_market"]]), 1)
        elif e_local_data.get("national").get("rent_days_on_market"):
            e_predictions["predicted_days_on_market"] = round(np.mean([e_predictions["predicted_days_on_market"], e_local_data["national"]["rent_days_on_market"]]), 1)
        
        e_predictions["predicted_rent_annual_first"] = round(e_predictions["predicted_rent_annual"] * (max(0, 365 - e_predictions["predicted_days_on_market"]) / 365), 2)
    
    e_rent_optimization = {}
    for d_diff in [-0.2, -0.1, 0, 0.1, 0.2]:
        d_days_on_market = np.mean([
            max(0, calc_percent_diff_to_extra_days(d_diff)),
            max(0, calc_percent_diff_to_percent_extra_days(d_diff, d_days_on_market=e_predictions["predicted_days_on_market"]))])
        e_rent_optimization[f"rent_{d_diff:0.0%}"] = {
            "rent": round(e_predictions["predicted_rent"] * (1 + d_diff), 2),
            "days_on_market": round(d_days_on_market, 1),
            "rent_first_year": round(e_predictions["predicted_rent"] * (1 + d_diff) * 12 * (max(0, 365 - d_days_on_market) / 365), 2),
        }
    
    e_processed = {
        "status": 200,
        "address": e_address,
        "predictions": e_predictions,
        "improvements": {
            "short_term": a_str_improvements,
            "long_term": a_ltr_improvements,
        },
        "monthly_predictions": e_monthly_data,
        "per_month_costs": e_per_month_costs,
        "annual_costs": e_annual_costs,
        "avg_monthly_costs": e_avg_monthly_costs,
        "local_data": e_local_data,
        "scoring": e_scores["str"],
        "ltr_scoring": e_scores["ltr"],
        "financing": {
            "predicted_revenue": e_revenue_financing,
            "predicted_revenue_optimized": e_revenue_optimized_financing,
            "predicted_rent": e_rent_financing,
        },
        "rent_optimization": e_rent_optimization,
        "inputs": {
            k: v
            for k, v in e_input_prepped.items()
            if k in c_OUTPUT_COLS
        },
    }
    print("processing request took", utils.secondsToStr(time() - n_start))
    return e_processed
