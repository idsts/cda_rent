﻿#import pyximport; pyximport.install()

import os, sys, re, json, shutil
from time import time, sleep, ctime
import binascii, requests
from datetime import datetime, timedelta
from collections import defaultdict, Counter
from functools import wraps
import logging, traceback
import pprint
import numpy as np
import pandas as pd
from flask import Flask, g, Response, request, send_from_directory, send_file, redirect, flash, url_for, render_template, make_response
#from wtforms import Form, TextField, TextAreaField, validators, StringField, SubmitField
import flask_restful
from flask_restful import Api, reqparse
from flask_cors import CORS, cross_origin
from flask_restful.utils import cors
#from flask_autoindex import AutoIndex
from werkzeug.utils import secure_filename

try:
    from . import utils, paths, db_tables, interactive_inputs, interactive_exposed, property_comps, scoring, defaults
    from . import webhook_utils, activecampaign_webhook, aws_ses, activecampaign_api, financing_webhook, area_data, area_webhook
    from .log import LOGGER
    from . import endpoint_descriptions
    from .docdb_utils import DocDBConnector
except:
    import utils, paths, db_tables, interactive_inputs, interactive_exposed, property_comps, scoring, defaults
    import webhook_utils, activecampaign_webhook, aws_ses, activecampaign_api, financing_webhook, area_data, area_webhook
    from log import LOGGER
    import endpoint_descriptions
    from col_transform import TransformByGeoSearch
    from docdb_utils import DocDBConnector


#g.o_DB = None
#g.o_ACTIVECAMPAIGN = None

#with open(utils.get_local_path("model_variables_20210810.json"), "r", encoding="utf-8-sig") as f:
with open(utils.get_local_path("model_variables_20220207_v2.json"), "r", encoding="utf-8-sig") as f:
    e_VARS = json.load(f)
    
b_PROCESS_LIVE = True if os.environ.get("REVENUE_API_PROCESS_LIVE", False) in {True, "true", "TRUE", "True", 1, "yes", "YES", "Yes", "On", "ON"} else False
b_INTERACTIVE_LIVE = True if os.environ.get("REVENUE_API_INTERACTIVE_LIVE", True) in {True, "true", "TRUE", "True", 1, "yes", "YES", "Yes", "On", "ON"} else False
b_DOCDB_LIVE = True if os.environ.get("REVENUE_API_DOCDB_LIVE", True) in {True, "true", "TRUE", "True", 1, "yes", "YES", "Yes", "On", "ON"} else False

if b_PROCESS_LIVE:
    print("Run process_api_requests right after requests. No cron job required.")
    try:
        from . import process_api_requests
    except:
        import process_api_requests
else:
    print("Don't process_api_requests right after requests. Must have a cron job scheduled to process inputs.")

if b_INTERACTIVE_LIVE:
    print("Keep models and data in memory to quickly process single property analyses")
    try:
        from . import single_property_predictor
    except:
        import single_property_predictor
else:
    print("Don't provide single property interactive predictor")

if b_DOCDB_LIVE:
    print("Load connection to AWS DocumentDB")
    try:
        from . import docdb_utils
    except:
        import docdb_utils
else:
    print("Don't connect to AWS DocumentDB")



i_FILE_SIZE_LIMIT = 25

e_HARDCODED_API_KEYS = {
    "tim": os.environ.get("REVENUE_API_KEY_TIM", ''),
    "cory": os.environ.get("REVENUE_API_KEY_CORY", ''),
    "activecampaign": os.environ.get("REVENUE_API_KEY_AC", ''),
    "frontend": os.environ.get("REVENUE_API_KEY_FRONTEND", ''),
    #"leaked": '55a9-e986-7e7a-506b-5f3f-e440-0552-b0da',
}
e_API_KEYS = e_HARDCODED_API_KEYS.copy()
e_API_KEY_USERS = {v: k for k, v in e_API_KEYS.items()}

#g.dt_USERS_UPDATE = datetime.utcnow() - timedelta(days=1)
#g.s_USER = "unknown"

# resources that don't need a valid API key to access
c_OPEN_RESOURCES = {"EndPoints.get", "DevWebhook.post", "DevWebhook.put", "WebhookActiveCampaignContactProperty.post"}

e_DEFAULT_HEADER = {'Access-Control-Allow-Origin': '*'}
t_CORS_OPTIONS_RESPONSE = (
    {'Allow' : 'PUT,GET,POST,OPTIONS,DELETE'},
    200, 
    {
        'Access-Control-Allow-Origin': "*",
        'Access-Control-Allow-Methods' : 'PUT,GET,POST,OPTIONS,DELETE',
        'Access-Control-Allow-Headers': "*",
    })

ALLOWED_EXTENSIONS = {'.txt', '.xlsx', '.csv', '.gz', '.xz', '.bz2'}

s_APP_PATH = os.path.dirname(os.path.abspath(__file__))
print("s_APP_PATH=", s_APP_PATH)

#if g.o_DB is None:
if db_tables.a_TABLES is None: db_tables.load_db(**kwargs)
#g.o_DB = db_tables.o_DB
#g.o_DB = db_tables.get_connection()



def generate_key():
    s_key = binascii.hexlify(os.urandom(16)).decode()
    a_pieces = []
    for i_start in range(0, len(s_key), 4):
        a_pieces.append(s_key[i_start: i_start + 4])
    return "-".join(a_pieces)


def update_users(b_force_reload=False):
    if not hasattr(g, 'o_DB') or g.o_DB is None:
        g.o_DB = db_tables.get_connection()
    if not hasattr(g, 'dt_USERS_UPDATE'):
        g.dt_USERS_UPDATE = datetime.utcnow() - timedelta(days=1)
    if not hasattr(g, 's_USER'):
        g.s_USER = "unknown"
    
    global e_API_KEYS, e_API_KEY_USERS
    #global dt_USERS_UPDATE
    if b_force_reload: print("Force reload of users from api_key table")
    
    if g.dt_USERS_UPDATE < datetime.utcnow() - timedelta(minutes=15) or b_force_reload:
        try:
            a_valid_usernames = g.o_DB.execute(r"SELECT name, api_key FROM api_users WHERE enabled = 1;")
            e_API_KEYS = e_HARDCODED_API_KEYS.copy()
            e_API_KEYS.update(dict(a_valid_usernames))
            
            e_API_KEY_USERS = {v: k for k, v in e_API_KEYS.items()}
            
            g.dt_USERS_UPDATE = datetime.utcnow()
        except:
            print("Couldn't get api_keys and names from AWS DB api_users. Only access is hardcoded keys.")


def get_api_key(req, **kwargs):
    e_header = { k.lower().replace("-", "_"): v for k, v in req.headers.items()}
    s_key = e_header.get("api_key", "")
    if not s_key: s_key = e_header.get("api key", "")
    if not s_key: s_key = kwargs.get("api_key", "")
    if not s_key: 
        parser = reqparse.RequestParser()
        parser.add_argument("api_key", required=False)
        req_args = parser.parse_args()
        s_key = req_args.get("api_key", "")
    if s_key:
        s_key = re.sub(r"(^['\" ]+|['\" ]+$)", "", s_key)
    return s_key


def authenticate(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        #print("func.__name__", func.__name__,  func.__qualname__)
        if not getattr(func, 'authenticated', True) or func.__qualname__ in c_OPEN_RESOURCES or func.__qualname__.endswith("options"):
            return func(*args, **kwargs)
        
        #print("auth 1")
        LOGGER.info(f"{utils.get_time()}\tauth 1")
        if not hasattr(g, 'dt_USERS_UPDATE'):
            g.dt_USERS_UPDATE = datetime.utcnow() - timedelta(days=1)
        if not hasattr(g, 's_USER'):
            g.s_USER = "unknown"
        
        e_header = {
            k.lower().replace("-", "_"): v
            for k, v in request.headers.items()}
        #print('e_header', e_header)
        s_key = e_header.get("api_key", "")
        if not s_key: s_key = e_header.get("api key", "")
        #print("auth 2")
        LOGGER.info(f"{utils.get_time()}\tauth 2")
        
        if not s_key:
            s_key = kwargs.get("api_key", "")
            #print("kwargs", kwargs)
            
            parser = reqparse.RequestParser()
            parser.add_argument("api_key", required=False, location=["args", "headers"])
            req_args = parser.parse_args()
            #print("req_args", req_args)
            if not s_key: s_key = req_args.get("api_key", "")
        
        #print("auth 3")
        LOGGER.info(f"{utils.get_time()}\tauth 3")
        update_users()
        if s_key:
            #global g.s_USER
            s_key = re.sub(r"(^['\" ]+|['\" ]+$)", "", s_key)
            s_user = e_API_KEY_USERS.get(s_key)
            if s_user:
                g.s_USER = s_user
                LOGGER.info(f"{utils.get_time()}\tAPI User: {s_user}")
                g.o_DB.update("api_users", {"last_access": datetime.utcnow().isoformat()}, {"api_key": s_key})
                return func(*args, **kwargs)
                
            update_users(b_force_reload=True)
            s_user = e_API_KEY_USERS.get(s_key)
            if s_user:
                g.s_USER = s_user
                LOGGER.info(f"{utils.get_time()}\tAPI User: {s_user}")
                g.o_DB.update("api_users", {"last_access": datetime.utcnow().isoformat()}, {"api_key": s_key})
                return func(*args, **kwargs)
            
            print("The endpoint %s requires a valid {'api_key': 'your_api_key_here'} in the header. '%s' is not valid." % (request.path, s_key))
            LOGGER.error("The endpoint %s requires a valid {'api_key': 'your_api_key_here'} in the header. '%s' is not valid." % (request.path, s_key))
            return {"message": "The endpoint %s requires a valid {'api_key': 'your_api_key_here'} in the header. '%s' is not valid." % (request.path, s_key)}, 401
        else:
            print("The endpoint %s requires a valid {'api_key': 'your_api_key_here'} in the header." % (request.path), request.headers)
            LOGGER.error("The endpoint %s requires a valid {'api_key': 'your_api_key_here'} in the header." % (request.path))
            return {"message": "The endpoint %s requires a valid  {'api_key': 'your_api_key_here'} in the header." % (request.path)}, 401
        
        #print("auth 4")
        LOGGER.info(f"{utils.get_time()}\tauth 4")
        
        print(f"will abort via flask_restful.abort(401)")
        flask_restful.abort(401)
    return wrapper


class Resource(flask_restful.Resource):
    method_decorators = [  # applies to all inherited resources
        authenticate,
    ]
    decorators = [
    ]


class EndPoints(Resource):
    def options(self, headers=None, **kwargs):
        return t_CORS_OPTIONS_RESPONSE
    
    def get(self, headers=None, **kwargs):
        #LOGGER.info(f"{utils.get_time()}\tEndPoints.get")
        return endpoint_descriptions.e_ENDPOINTS, 200


class Variables(Resource):
    def options(self, headers=None, **kwargs):
        return t_CORS_OPTIONS_RESPONSE
    
    def get(self, headers=None, **kwargs):
        #add_db_to_app()
        e_variables = {
            #"REVENUE_API_PROCESS_LIVE": os.environ.get("REVENUE_API_PROCESS_LIVE"),
            "PROCESS_LIVE": b_PROCESS_LIVE,
            #"REVENUE_API_INTERACTIVE_LIVE": os.environ.get("REVENUE_API_INTERACTIVE_LIVE"),
            "INTERACTIVE_LIVE": b_INTERACTIVE_LIVE,
            "DOCDB_LIVE": b_DOCDB_LIVE,
            "FILE_SIZE_LIMIT": i_FILE_SIZE_LIMIT,
            "ALLOWED_EXTENSIONS": ", ".join(ALLOWED_EXTENSIONS),
            
            #"REVENUE_GOOGLE_MAPS_API_KEY": os.environ.get("REVENUE_GOOGLE_MAPS_API_KEY"),
            #"GOOGLE_MAPS_API_KEY": os.environ.get("GOOGLE_MAPS_API_KEY"),
            #"CDA_AWS_SES_USER": os.environ.get("CDA_AWS_SES_USER"),
            
            "CDA_AWS_DB": os.environ.get("CDA_AWS_DB"),
            "CDA_AWS_DB_USER": os.environ.get("CDA_AWS_DB_USER"),
            "CONFIG": utils.json_dict_prep(db_tables.e_CONFIG),
            
        }
        e_variables["environ"] = utils.json_dict_prep(dict(os.environ))
        #close_app_db()
        return e_variables, 200


class DevWebhook(Resource):
    def options(self, headers=None, **kwargs):
        return t_CORS_OPTIONS_RESPONSE
    
    def get(self, api_key="", **kwargs):
        #s_folder = os.path.join(os.getcwd(), "webhook_payloads")
        s_folder = paths.s_DEV_WEBHOOKS_PATH
        
        if not os.path.isdir(s_folder):
            return {"message": 'There are currently no webhook payloads'}, 404
        
        s_webhook_payloads, a_payloads = utils.zip_directory(s_folder, store_in_directory=True, exclude_extensions=[".zip"])
        
        LOGGER.info(f'DevWebhook.get: zipped {len(a_payloads)} webhook payloads into "{s_webhook_payloads}"')
        
        try:
            return send_file(s_webhook_payloads, as_attachment=True)
        except FileNotFoundError:
            return {"message": "Couldn't find any webhook payload files stored"}, 404
    
    def delete(self, api_key="", **kwargs):
        #s_folder = os.path.join(os.getcwd(), "webhook_payloads")
        s_folder = paths.s_DEV_WEBHOOKS_PATH
        if not os.path.isdir(s_folder):
            return {"message": 'There is currently no webhook payload directory'}, 404
        try:
            a_removed = utils.clear_sub_folders("webhook_payloads", os.getcwd())
            i_payloads = len([s for x in a_removed if s and s.lower().endswith(".txt")])
            LOGGER.info(f'DevWebhook.delete: removed {i_payloads} webhook payloads')
            return {"message": f'removed {len(a_removed)} webhook payloads'}, 200
        except Exception as e:
            LOGGER.exception(f"{utils.get_time()}\tAPI User: {g.s_USER}. Could not clear out webhook payloads: {e}")
            return {"message": f"Could not clear out webhook payloads: {e}"}, 500
    
    def post(self, **kwargs):
        ''' to see what a webhook is sending, record it's output to a txt file
            Arguments:
            Returns:
                {tuple} of Message, Status Code
        '''
        try:
            print(request)
            e_json = request.json
            if isinstance(e_json, str):
                print("Loading json payload")
                e_json = json.loads(e_json)
            elif e_json:
                e_json = request.get_json()
            
            print(f"{utils.get_time()}\tAPI User: {g.s_USER}. Webhook post payload")
            LOGGER.info(f"{utils.get_time()}\tAPI User: {g.s_USER}. Webhook post payload")
            if e_json is None: e_json = {}
            
            #s_folder = os.path.join(os.getcwd(), "webhook_payloads")
            s_folder = paths.s_DEV_WEBHOOKS_PATH
            s_file = "webhook_payload_{}.txt".format(datetime.utcnow().strftime("%Y%m%d_%H%M%S"))
            if not utils.create_folder(os.path.join(s_folder, s_file)):
                raise Exception(f"Couldn't create folder to store webhook payloads: \"{s_folder}\"")
            
            print(type(request.get_data()))
            LOGGER.info(type(request.get_data()))
            try:
                print(request.get_data())
                LOGGER.info(request.get_data())
            except:
                pass
            
            json.dump(
                {
                    'source_ip': request.remote_addr,
                    'path': request.path,
                    'method': request.method,
                    'user_agent': request.user_agent.string,
                    'args': dict(request.args),
                    'view_args': request.view_args,
                    'query_string': request.query_string.decode("utf-8"),
                    #'data': json.loads(request.get_data()),
                    'data_as_text': request.get_data(as_text=True),
                    #'text': request.text,
                    'json': e_json,
                },
                open(os.path.join(s_folder, s_file), "w", encoding="utf-8-sig"))
            """
            with open(os.path.join(s_folder, s_file), "w", encoding="utf-8-sig") as o_file:
                e_req = {
                    'source_ip': request.remote_addr,
                    #'url': request.url,
                    'path': request.path,
                    'method': request.method,
                    'user_agent': request.user_agent.string,
                    'args': dict(request.args),
                    'view_args': request.view_args,
                    'query_string': request.query_string.decode("utf-8"),
                    #'data': request.get_data(as_text=True),
                    'data_as_text': request.get_data(as_text=True),
                    'text': request.text,
                    'json': e_json,
                }
                
                o_file.write('{\r\n    "request": {')
                for i, (k, v) in enumerate(e_req.items()):
                    o_file.write(f'\r\n        "{k}": {json.dumps(v)}')
                    if i < len(e_req) - 1: o_file.write(",")
                o_file.write('\r\n},\r\n"payload": {')
                for k, v in e_json.items():
                    o_file.write(f'\r\n        "{k}": {json.dumps(v)}')
                    if i < len(e_req) - 1: o_file.write(",")
                o_file.write('\r\n    }\r\n}')
            """
            
            return {"message": f"Webhook payload recorded to \"{s_file}\""}, 200
        except Exception as e:
            LOGGER.exception(f"{utils.get_time()}\tAPI User: {g.s_USER}. Error trying to record sample webhook payload: {e}")
            return {"message": f"Error with code when trying to record sample webhook payload: {e}"}, 500
    
    def put(self, **kwargs):
        ''' to see what a webhook is sending, record it's output to a txt file
            Arguments:
            Returns:
                {tuple} of Message, Status Code
        '''
        try:
            print(request)
            e_json = request.json
            if isinstance(e_json, str):
                print("Loading json payload")
                e_json = json.loads(e_json)
            
            print(f"{utils.get_time()}\tAPI User: {g.s_USER}. Webhook put payload")
            LOGGER.info(f"{utils.get_time()}\tAPI User: {g.s_USER}. Webhook put payload")
            if e_json is None: e_json = {}
            
            #s_folder = os.path.join(os.getcwd(), "webhook_payloads")
            s_folder = paths.s_DEV_WEBHOOKS_PATH
            s_file = "webhook_payload_{}.txt".format(datetime.utcnow().strftime("%Y%m%d_%H%M%S"))
            if not utils.create_folder(os.path.join(s_folder, s_file)):
                raise Exception(f"Couldn't create folder to store webhook payloads: \"{s_folder}\"")
            
            with open(os.path.join(s_folder, s_file), "w", encoding="utf-8-sig") as o_file:
                e_req = {
                    'source_ip': request.remote_addr,
                    #'url': request.url,
                    'path': request.path,
                    'method': request.method,
                    'user_agent': request.user_agent.string,
                    'args': dict(request.args),
                    'view_args': request.view_args,
                    'query_string': request.query_string.decode("utf-8"),
                    'data': request.get_data(as_text=True),
                }
                
                o_file.write('[{\r\n    "request": {')
                for i, (k, v) in enumerate(e_req.items()):
                    o_file.write(f'\r\n        "{k}": {json.dumps(v)}')
                    if i < len(e_req) - 1: o_file.write(",")
                o_file.write('\r\n},\r\n"payload": {')
                for k, v in e_json.items():
                    o_file.write(f'\r\n        "{k}": {json.dumps(v)}')
                    if i < len(e_req) - 1: o_file.write(",")
                o_file.write('\r\n    }\r\n}]')
            
            return {"message": f"Webhook payload recorded to \"{s_file}\""}, 200
        except Exception as e:
            LOGGER.exception(f"{utils.get_time()}\tAPI User: {g.s_USER}. Error trying to record sample webhook payload: {e}")
            return {"message": f"Error with code when trying to record sample webhook payload: {e}"}, 500


class WebhookActiveCampaignContactProperty(Resource):
    def options(self, headers=None, **kwargs):
        return t_CORS_OPTIONS_RESPONSE
    
    def post(self, **kwargs):
        '''
            Arguments:
            Returns:
                {tuple} of Message, Status Code
        '''
        #db_tables.load_config()
        add_db_to_app()
        
        #global o_ACTIVECAMPAIGN
        try:
            s_data_as_text = request.get_data(as_text=True)
            e_data = webhook_utils.interpret_get_data(s_data_as_text)
            
            parser = reqparse.RequestParser()
            parser.add_argument("request_id")
            req_args = parser.parse_args()
            
            print(f"{utils.get_time()}\tAPI User: {g.s_USER}. Webhook for looking up one activecampaign contact's property")
            LOGGER.info(f"{utils.get_time()}\tAPI User: {g.s_USER}. Webhook for looking up one activecampaign contact's property")
            
            v_user_id, v_user_email = None, None
            if e_data:
                for k in ["contact_id", "contact[id]", "user_id", "userid"]:
                    if e_data.get(k):
                        v_user_id = e_data[k]
                        break
                
                for k in ["email", "contact[email", "user_email"]:
                    if e_data.get(k):
                        v_user_email = e_data[k]
                        break
            
            if req_args.get("request_id"):
                request_id = req_args["request_id"]
            else:
                s_api_key = get_api_key(request, **kwargs)
                request_id = g.o_DB.insert(
                    db_tables.interactive_requests,
                    {
                        "api_key": s_api_key,
                        "endpoint": "/interactive/webhooks/contact_manual_form",
                        "body": json.dumps(e_data),
                        "user_id": v_user_id,
                        "user_email": v_user_email,
                        "ip_address": request.remote_addr,
                    },
                    return_id=True,)
            
            e_input = activecampaign_webhook.convert_to_input(e_data)
            
            (e_input_prepped, e_pai, e_address, e_valid_financing), i_status = single_property_predictor.prep_single_property(e_input)
            
            if i_status != 200:
                print(f"{utils.get_time()}\tAPI User: {g.s_USER}. Problem with webhook input {i_status}. Contact timothy@idsts.com.", e_data)
                LOGGER.warning(f"{utils.get_time()}\tAPI User: {g.s_USER}. Problem with webhook input {i_status}. Contact timothy@idsts.com.")
                
                if "Invalid input" not in e_input_prepped:
                    e_input_prepped["Invalid input"] = """Problem with webhook input. Contact timothy@idsts.com"""
                
                contact_id = e_data.get("contact[id]")
                s_note = "Webhook problem:\n\n{}".format(pprint.pformat(e_input_prepped))
                s_note_status = ""
                if not contact_id:
                    s_note_status = "No ActiveCampaign contact id to create problem note"
                else:
                    if not hasattr(g, 'o_ACTIVECAMPAIGN') or g.o_ACTIVECAMPAIGN is None: g.o_ACTIVECAMPAIGN = activecampaign_api.ActiveCampaign()
                    g.o_ACTIVECAMPAIGN.create_note(s_note, subscriber=contact_id)
                    s_note_status = f"Problem note added to ActiveCampaign contact id {contact_id}"
                
                print(f"{utils.get_time()}\tAPI User: {g.s_USER}. {s_note_status}")
                LOGGER.info(f"{utils.get_time()}\tAPI User: {g.s_USER}. {s_note_status}")
                
                e_data["request_id"] = request_id
                return utils.json_dict_prep(e_data), i_status
    
            e_predictions, e_str_improvements, e_ltr_improvements = single_property_predictor.predict_single_property(e_input_prepped, e_pai)
            
            e_local_data = {}
            if kwargs.get("get_local_data", True):
                e_local_data = area_data.get_local_data(e_address, **kwargs)
                #print("area_vs_mean", e_local_data.get("national", {}).get("revenue"), e_predictions.get("area_revenue"))
                if e_local_data.get("national", {}).get("revenue") and e_predictions.get("area_revenue"):
                    e_predictions["area_vs_mean"] = e_predictions["area_revenue"] / e_local_data["national"]["revenue"]
                #print("area_rent_vs_mean", e_local_data.get("national", {}).get("revenue"), e_predictions.get("area_revenue"))
                if e_local_data.get("national", {}).get("rent") and e_predictions.get("area_rent"):
                    e_predictions["area_rent_vs_mean"] = e_predictions["area_rent"] / e_local_data["national"]["rent"]
                
                if e_local_data.get("area_scores", {}).get("score_overall"):
                    e_predictions["area_score_overall"] = e_local_data["area_scores"]["score_overall"]
            
            e_monthly_data = {}
            if kwargs.get("get_monthly_data", True):
                e_monthly_data = area_data.get_monthly_area_data(
                    e_input_prepped["latitude"],
                    e_input_prepped["longitude"],
                    e_predictions["predicted_occupancy"],
                    e_predictions["avg_rate_at_predicted_revenue"],
                    s_state=e_input_prepped.get("state", ""),
                    e_local_data=e_local_data,
                    **kwargs)
            
            s_subject, s_msg_plain, s_msg_html = activecampaign_webhook.results_into_template(
                e_data,
                e_address,
                e_predictions,
                e_input_prepped,
                e_monthly_data,
                e_local_data)
            
            print(f"{utils.get_time()}\tAPI User: {g.s_USER}. Ran predictions")
            LOGGER.info(f"{utils.get_time()}\tAPI User: {g.s_USER}. Ran predictions")
            try:
                if e_data.get('contact[email]'):
                    a_to = [e_data['contact[email]']]
                else:    
                    a_to = ["info@revr.com"]
                    s_subject = f"No email address: {s_subject}"
                
                e_email_result = aws_ses.send_email(
                    a_to,
                    s_subject,
                    s_msg_plain,
                    s_html=s_msg_html,
                    cc=["info@revr.com"],
                    bcc=["timothy@idsts.com"],
                    boto=True,
                )
                s_email_status = f"Emailed results: {'.'.join(a_to)}"
                print(f"{utils.get_time()}\tAPI User: {g.s_USER}. {s_email_status}")
                LOGGER.info(f"{utils.get_time()}\tAPI User: {g.s_USER}. {s_email_status}")
            except Exception as e:
                s_email_status = f"Problem with emailing results: {e}"
                print(f"{utils.get_time()}\tAPI User: {g.s_USER}. {s_email_status}")
                LOGGER.warning(f"{utils.get_time()}\tAPI User: {g.s_USER}. {s_email_status}")
            
            if s_email_status:
                contact_id = e_data.get("contact[id]")
                s_note = f"{s_email_status}\n\n{s_msg_plain}"
                s_note_status = ""
                if not contact_id:
                    s_note_status = "No ActiveCampaign contact id to create result note"
                else:
                    if not hasattr(g, 'o_ACTIVECAMPAIGN') or g.o_ACTIVECAMPAIGN is None: g.o_ACTIVECAMPAIGN = activecampaign_api.ActiveCampaign()
                    g.o_ACTIVECAMPAIGN.create_note(s_note, subscriber=contact_id)
                    s_note_status = f"Result note added to ActiveCampaign contact id {contact_id}"
                
                print(f"{utils.get_time()}\tAPI User: {g.s_USER}. {s_note_status}")
                LOGGER.info(f"{utils.get_time()}\tAPI User: {g.s_USER}. {s_note_status}")
            
            if v_user_id: e_input["user_id"] = v_user_id
            if v_user_email: e_input["user_email"] = v_user_email
            i_result_id, e_result = single_property_predictor.store_prediction(
                g.o_DB,
                e_address,
                e_predictions,
                i_request_id=request_id,
                e_monthly_data=e_monthly_data,
                e_local_data=e_local_data,
                #e_revenue_financing=e_revenue_financing,
                #e_revenue_optimized_financing=e_revenue_optimized_financing,
                #e_rent_financing=e_rent_financing,
                e_input_prepped=e_input_prepped,
                e_request=e_input,
                #a_pas=None,
                #e_pai=None,
                #**kwargs
            )
            
            #json.dump(e_result, open(os.path.join(paths.s_DEV_WEBHOOKS_PATH, "results.txt"), "w"))
            #return e_result, 200
            #close_app_db()
            
            return (
                {
                    "message": f"Webhook for predicting contact's property revenue processed successfully.",
                    "request_id": request_id
                },
                200)
        
        except Exception as e:
            traceback.print_exc()
            s_trace = ''.join(traceback.format_exception(None, e, e.__traceback__))
            print(s_trace)
            LOGGER.exception(f"{utils.get_time()}\tAPI User: {g.s_USER}. Error trying to predict property revenue: {e} {json.dumps(e_data)}")
            #close_app_db()
            return {"message": f"Error with code when trying to predict contact's property revenue: {e}", "trace": s_trace}, 500


class InputURL(Resource):
    def options(self, headers=None, **kwargs):
        return t_CORS_OPTIONS_RESPONSE
    
    def post(self, **kwargs):
        add_db_to_app()
        
        parser = reqparse.RequestParser()
        parser.add_argument("request_id")
        parser.add_argument("url", type=str, required=True)
        parser.add_argument("user_id")
        parser.add_argument("user_email", type=str)
        parser.add_argument("url", type=str, required=True)
        parser.add_argument("request_id")
        req_args = parser.parse_args()
    
        s_url = req_args.get("url", "")
        #print("s_url", s_url)
        LOGGER.info(f"{utils.get_time()}\t'Received url: {s_url}.")
        
        v_user_id, v_user_email = None, None
        if req_args:
            for k in ["contact_id", "contact[id]", "user_id", "userid"]:
                if req_args.get(k):
                    v_user_id = req_args[k]
                    break
            
            for k in ["email", "contact[email", "user_email"]:
                if req_args.get(k):
                    v_user_email = req_args[k]
                    break
        
        if req_args.get("request_id"):
            request_id = req_args["request_id"]
        else:
            s_api_key = get_api_key(request, **kwargs)
            request_id = g.o_DB.insert(
                db_tables.interactive_requests,
                {
                    "api_key": s_api_key,
                    "endpoint": "/input/url",
                    #"body": req_args,
                    "body": json.dumps(utils.json_dict_prep(req_args)),
                    "user_id": v_user_id,
                    "user_email": v_user_email,
                    "ip_address": request.remote_addr,
                },
                return_id=True,)
                
        if s_url == '':
            LOGGER.warning(f"{utils.get_time()}\t'No url included in request'")
            flash('No url')
            #close_app_db()
            return {"message": "No URL included in request"}, 400 #redirect(request.url)
        
        if not re.search(r"https://api.apify.com", s_url, flags=re.I):
            LOGGER.warning(f"{utils.get_time()}\t'Invalid domain: {s_url}'")
            flash('Invalid domain: {s_url}')
            #close_app_db()
            return {"message": f"Invalid domain:{s_url}"}, 400  #return redirect(request.url)
            
        
        a_datasets = re.findall(r"(?<=datasets/)[^/]+", s_url, flags=re.I) + []
        s_file = a_datasets[0] if a_datasets else datetime.utcnow().strftime("%Y%m%d_%H%M")
        #s_path = process_api_requests.download_apify_zillow_dataset(s_url, b_force_csv=True)
        with open(os.path.join(paths.s_URL_PATH, f"{s_file}.txt"), "w", encoding="utf-8-sig") as f:
            f.write(s_url)
        
        #close_app_db()
        return (utils.json_dict_prep({
            "message": "URL request succeeded",
            "url": s_url,
            "request_id": request_id,
            #"filename": os.path.join(paths.s_URL_PATH, f"{s_file}.txt"),
            #"size": f"{d_size:0.1f} Mb",
            #"url": url_for('get_input_file', path=filename, api_key=s_key)
        }), 200)
        #else:
        #    return {"message": "Unable to download dataset from Apify"}, 500


s_EXPOSED_OPTIONS = "\n".join([
    f"""<tr><td>{t_control[1]}:</td><td><select name={s_var}><option value=""></option><option value=false>False</option><option value=true selected="selected">True</option></select></td></tr>"""
    if t_control[2] == True else
    f"""<tr><td>{t_control[1]}:</td><td><select name={s_var}><option value=""></option><option value=false selected="selected">False</option><option value=true>True</option></select></td></tr>"""
    if t_control[2] == False else
    f"""<tr><td>{t_control[1]}:</td><td><select name={s_var}><option value=""></option><option value=false>False</option><option value=true>True</option></select></td></tr>"""
    for s_var, t_control in interactive_exposed.e_EXPOSED_OPTIONS.items()
    if t_control[0] == True])
s_EXPOSED_FEATURES = "\n".join([
    f"""<tr><td>{t_control[1]}:</td><td><select name={s_var}><option value=""></option><option value=false>False</option><option value=true selected="selected">True</option></select></td></tr>"""
    if t_control[2] == True else
    f"""<tr><td>{t_control[1]}:</td><td><select name={s_var}><option value=""></option><option value=false selected="selected">False</option><option value=true>True</option></select></td></tr>"""
    if t_control[2] == False else
    f"""<tr><td>{t_control[1]}:</td><td><select name={s_var}><option value=""></option><option value=false>False</option><option value=true>True</option></select></td></tr>"""
    for s_var, t_control in interactive_exposed.e_EXPOSED_FEATURES.items()
    if t_control[0] == True])
s_EXPOSED_LOCATIONS = "\n".join([
    f"""<tr><td>{t_control[1]}:</td><td><select name={s_var}><option value=""></option><option value=false>False</option><option value=true selected="selected">True</option></select></td></tr>"""
    if t_control[2] == True else
    f"""<tr><td>{t_control[1]}:</td><td><select name={s_var}><option value=""></option><option value=false selected="selected">False</option><option value=true>True</option></select></td></tr>"""
    if t_control[2] == False else
    f"""<tr><td>{t_control[1]}:</td><td><select name={s_var}><option value=""></option><option value=false>False</option><option value=true>True</option></select></td></tr>"""
    for s_var, t_control in interactive_exposed.e_EXPOSED_LOCATIONS.items()
    if t_control[0] == True])

a_PROPERTY_TYPES = [
    "House", "Rancher", "Duplex", "Cottage",
    "Farmhouse", "Cabin", 'Chateau', "Estate",
    'Townhouse', "Condo", "Apartment",
    'Tiny house', #'Earth house',
]
s_PROPERTY_TYPE_OPTIONS = [
    f"<option value=\"{s_type.lower()}\">{s_type}</option>\n"
    for s_type in a_PROPERTY_TYPES]

s_INTERACTIVE_FORM = '''
    <!doctype html>
    <title>{title}</title>
    <h1>{title}</h1>
    <form method=post enctype=multipart/form-data>
    <table border=0>
        <tr><td colspan=2><font color="#999999">Required</font></td></tr>
        <tr><td>Address:</td><td><input type=text name=address></td></tr>
        <tr><td>Latitude:</td><td><input type=text name=latitude></td></tr>
        <tr><td>Longitude:</td><td><input type=text name=longitude></td></tr>
        <tr><td>Bedrooms:</td><td><input type=text name=bedrooms required=True></td></tr>
        <tr><td>Bathrooms:</td><td><input type=text name=bathrooms required=True></td></tr>
        
        
        <tr><td colspan=2><font color="#999999">Optional Information</font></td></tr>
        <tr><td>Property Type:</td><td><select name=property_type>{property_type_options}</select></td></tr>
        <tr><td>Price ($):</td><td><input type=text name=price></td></tr>
        <tr><td>Living Area (sq.ft.):</td><td><input type=text name=living_area ></td></tr>
        <tr><td>Lot Size (acres):</td><td><input type=text name=lot_size></td></tr>
        <tr><td>Year Built:</td><td><input type=text name=year_built></td></tr>
        <tr><td>Rental Service:</td><td><select name=rental_service><option value="airbnb">AirBnB</option><option value="homeaway">HomeAway</option></select></td></tr>
        <tr><td>Max Guests:</td><td><input type=text name=max_guests></td></tr>
        {exposed_options}
        
        <tr><td colspan=2><font color="#999999">Optional Feature</font></td></tr>
        {exposed_features}
        <tr><td colspan=2><font color="#999999">Location Information</font></td></tr>
        {exposed_locations}
        
        {other_input}
       <tr><td><input type=submit value=Submit></td></tr>
    </table>
    
    {results}
    </form>
'''


class InteractivePredictor(Resource):
    def options(self, headers=None, **kwargs):
        return t_CORS_OPTIONS_RESPONSE
    
    def get(self, **kwargs):
        s_form = s_INTERACTIVE_FORM.format(
            title="Predict Potential Property Revenue",
            property_type_options=s_PROPERTY_TYPE_OPTIONS,
            exposed_options=s_EXPOSED_OPTIONS,
            exposed_features=s_EXPOSED_FEATURES,
            exposed_locations=s_EXPOSED_LOCATIONS,
            other_input="",
            results="",)
        r_response = make_response(s_form, 200)
        #print("response.headers", r_response.headers)
        return r_response
    
    def post(self, **kwargs):
        n_start = time()
        #db_tables.load_config()
        add_db_to_app()
        
        try:
            parser = reqparse.RequestParser()
            parser.add_argument("request_id")
            parser.add_argument("property_type", type=str, required=False)
            parser.add_argument("bedrooms", required=True)
            parser.add_argument("bathrooms", required=True)
            parser.add_argument("address", type=str, required=False)
            parser.add_argument("latitude", required=False)
            parser.add_argument("longitude", required=False)
            parser.add_argument("price", required=False)
            parser.add_argument("living_area", required=False)
            parser.add_argument("lot_size", required=False)
            parser.add_argument("rental_service", type=str, required=False)
            parser.add_argument("max_guests", required=False)
            parser.add_argument("year_built", required=False)
            
            for s_var, t_control in interactive_exposed.e_EXPOSED_OPTIONS.items():
                if t_control[0] == True:
                    parser.add_argument(s_var, type=str, required=False)
            for s_var, t_control in interactive_exposed.e_EXPOSED_FEATURES.items():
                if t_control[0] == True:
                    parser.add_argument(s_var, type=str, required=False)
            for s_var, t_control in interactive_exposed.e_EXPOSED_LOCATIONS.items():
                if t_control[0] == True:
                    parser.add_argument(s_var, type=str, required=False)
            
            req_args = parser.parse_args()
            
            try:
                e_json = request.get_json()
            except:
                print("Couldnt' process json from request")
                e_json = {}
            
            if "latitude" in req_args:
                if isinstance(req_args["latitude"], str):
                    if req_args["latitude"].strip() == "":
                        del req_args["latitude"]
                    else:
                        req_args["latitude"] = utils.to_float(req_args["latitude"])
            if "longitude" in req_args:
                if isinstance(req_args["longitude"], str):
                    if req_args["longitude"].strip() == "":
                        del req_args["longitude"]
                    else:
                        req_args["longitude"] = utils.to_float(req_args["longitude"])
            
            e_request = e_json if isinstance(e_json, dict) else {}
            e_request.update(req_args)
            
            v_user_id, v_user_email = None, None
            if e_request:
                for k in ["contact_id", "contact[id]", "user_id", "userid"]:
                    if e_request.get(k):
                        v_user_id = e_request[k]
                        break
                
                for k in ["email", "contact[email", "user_email"]:
                    if e_request.get(k):
                        v_user_email = e_request[k]
                        break
            
            if req_args.get("request_id"):
                request_id = req_args["request_id"]
            else:
                s_api_key = get_api_key(request, **kwargs)
                request_id = g.o_DB.insert(
                    db_tables.interactive_requests,
                    {
                        "api_key": s_api_key,
                        "endpoint": "/interactive",
                        #"body": json.dumps(req_args),
                        #"content": json.dumps(e_request),
                        "body": json.dumps(utils.json_dict_prep(req_args)),
                        "content": json.dumps(utils.json_dict_prep(e_request)),
                        "user_id": v_user_id,
                        "user_email": v_user_email,
                        "ip_address": request.remote_addr,
                    },
                    return_id=True,)
            
            e_result = single_property_predictor.process_single_property(e_request)
            e_result["request_id"] = request_id
            e_result["runtime"] = utils.secondsToStr(time() - n_start)
            
            if e_result.get("status", 200) != 200:
                
                LOGGER.warning(f"{utils.get_time()}\tAPI User: {g.s_USER}. Problem prepping predictor with input {e_result['status']} {json.dumps(e_result)}")
                if "message" not in e_result:
                    e_result["message"] = f"Failed to successfully process request"
                
                return (utils.json_dict_prep(e_result), e_result["status"])
            
            e_result["message"] = "Interactive request accepted"
            
            
            r_response = make_response(utils.json_dict_prep(e_result), 200)
            
            print("Interactive request took", utils.secondsToStr(time() - n_start))
            
            i_result_id, e_result = single_property_predictor.store_prediction(
                g.o_DB,
                e_result["address"],
                e_result["predictions"],
                i_request_id=request_id,
                e_monthly_data=e_result["monthly_predictions"],
                e_local_data=e_result["local_data"],
                e_scores=e_result["scoring"],
                e_ltr_scores=e_result["ltr_scoring"],
                e_revenue_financing=e_result["financing"]["predicted_revenue"],
                e_revenue_optimized_financing=e_result["financing"]["predicted_revenue_optimized"],
                e_rent_financing=e_result["financing"]["predicted_rent"],
                e_input_prepped=e_result["inputs"],
                e_request=e_request,
                **kwargs
            )
            
            #close_app_db()
            return r_response
        
        except Exception as e:
            traceback.print_exc()
            s_trace = ''.join(traceback.format_exception(None, e, e.__traceback__))
            print(s_trace)
            r_response = make_response(utils.json_dict_prep({
                "message": "Interactive request failed",
                "error": str(e),
                "trace": s_trace,
            }), 500)
            
            LOGGER.warning(f"{utils.get_time()}\tAPI User: {g.s_USER}. Interactive request failed: {str(e)}")
            #close_app_db()
            return r_response



s_FINANCE_FORM = '''
    <!doctype html>
    <title>{title}</title>
    <h1>{title}</h1>
    <form method=post enctype=multipart/form-data>
    <table border=0>
        <tr><td colspan=2><font color="#999999">Required</font></td></tr>
        <tr><td>Annual Revenue:</td><td><input type=text name=annual_revenue required=True></td></tr>
        <tr><td>House Price:</td><td><input type=text name=price required=True></td></tr>
        
        <tr><td colspan=2><font color="#999999">Optional Information</font></td></tr>
        
        <tr><td>Loan Principal:</td><td><input type=text name=principal></td></tr>
        <tr><td>Loan Down Payment:</td><td><input type=text name=down_payment ></td></tr>
        <tr><td>Loan APR:</td><td><input type=text name=apr></td></tr>
        <tr><td>Loan Term (# Years):</td><td><input type=text name=loan_years></td></tr>
        <tr><td>State:</td><td><input type=text name=state></td></tr>
        <tr><td>Property Tax Rate:</td><td><input type=text name=property_tax_rate></td></tr>
        
       <tr><td><input type=submit value=Submit></td></tr>
    </table>
    
    </form>
'''
class InteractiveFinancing(Resource):
    def options(self, headers=None, **kwargs):
        return t_CORS_OPTIONS_RESPONSE
    
    def get(self, **kwargs):
        s_form = s_FINANCE_FORM.format(
            title="Calculate Financing",)
        r_response = make_response(s_form, 200)
        #print("response.headers", r_response.headers)
        return r_response
    
    def post(self, **kwargs):
        '''
            Arguments:
            Returns:
                {tuple} of Message, Status Code
        '''
        n_start = time()
        #db_tables.load_config()
        add_db_to_app()
        
        try:
            parser = reqparse.RequestParser()
            parser.add_argument("request_id")
            parser.add_argument("annual_revenue", required=True)
            parser.add_argument("price", required=True)
            
            parser.add_argument("principal", required=False)
            parser.add_argument("down_payment", required=False)
            parser.add_argument("apr", required=False)
            parser.add_argument("loan_years", required=False)
            
            parser.add_argument("property_tax_rate", required=False)
            parser.add_argument("expense_factor", required=False)
            #parser.add_argument("monthly_expenses", required=False)
            #parser.add_argument("annual_expenses", required=False)
            
            parser.add_argument("state", required=False)
            
            req_args = parser.parse_args()
            
            try:
                e_json = request.get_json()
            except:
                print("Couldnt' process json from request")
                e_json = {}
            
            e_request = e_json if isinstance(e_json, dict) else {}
            e_request.update(req_args)
            
            c_numbers = set(["annual_revenue", "price", "principal", "down_payment", "apr", "loan_years", "property_tax_rate"])
            
            e_args = {
                k: utils.to_float(v) if k in c_numbers else v
                for k, v in e_request.items()
                if not pd.isnull(v) and v != ""
            }
            
            print(f"{utils.get_time()}\tAPI User: {g.s_USER}. Webhook for calculating financing", req_args)
            LOGGER.info(f"{utils.get_time()}\tAPI User: {g.s_USER}. Webhook for calculating financing: {json.dumps(req_args)}")
            
            v_user_id, v_user_email = None, None
            if e_request:
                for k in ["contact_id", "contact[id]", "user_id", "userid"]:
                    if e_request.get(k):
                        v_user_id = e_request[k]
                        break
                
                for k in ["email", "contact[email", "user_email"]:
                    if e_request.get(k):
                        v_user_email = e_request[k]
                        break
            
            if req_args.get("request_id"):
                request_id = req_args["request_id"]
            else:
                s_api_key = get_api_key(request, **kwargs)
                request_id = g.o_DB.insert(
                    db_tables.interactive_requests,
                    {
                        "api_key": s_api_key,
                        "endpoint": "/financing",
                        "body": json.dumps(req_args),
                        "content": json.dumps(e_request),
                        "user_id": v_user_id,
                        "user_email": v_user_email,
                        "ip_address": request.remote_addr,
                    },
                    return_id=True,)
            
            e_financing = financing_webhook.calc_finances(
                **e_args)
            
            #close_app_db()
            return (
                {
                    "message": f"Webhook for calculating financing processed successfully.",
                    "financing": utils.json_dict_prep(e_financing),
                    "request_id": request_id,
                    "runtime": utils.secondsToStr(time() - n_start),
                }, 200)
            print("finance calculator took", utils.secondsToStr(time() - n_start))
        
        except Exception as e:
            traceback.print_exc()
            s_trace = ''.join(traceback.format_exception(None, e, e.__traceback__))
            print(s_trace)
            LOGGER.exception(f"{utils.get_time()}\tAPI User: {g.s_USER}. Error trying to calculate financing: {e} {json.dumps(req_args)}")
            #close_app_db()
            return {"message": f"Error with code when trying to calculate financing: {e}", "trace": s_trace}, 500


s_OTHER_COMP_INPUT = """
<tr><td colspan=2><font color="#999999">Other</font></td></tr>
<tr><td>Show as HTML table:</td><td><input type="checkbox" name="show_as_table" checked></td></tr>"""

class InteractiveComps(Resource):
    def options(self, headers=None, **kwargs):
        return t_CORS_OPTIONS_RESPONSE
    
    def get(self, **kwargs):
        s_form = s_INTERACTIVE_FORM.format(
            title="Find Similar Properties",
            property_type_options=s_PROPERTY_TYPE_OPTIONS,
            exposed_options=s_EXPOSED_OPTIONS,
            exposed_features=s_EXPOSED_FEATURES,
            exposed_locations=s_EXPOSED_LOCATIONS,
            other_input=s_OTHER_COMP_INPUT,
            results="",)
        r_response = make_response(s_form, 200)
        #print("response.headers", r_response.headers)
        return r_response
    
    def post(self, **kwargs):
        n_start = time()
        #db_tables.load_config()
        add_db_to_app()
        
        try:
            parser = reqparse.RequestParser()
            parser.add_argument("request_id")
            parser.add_argument("show_as_table", type=bool, required=False)
            parser.add_argument("property_type", type=str, required=True)
            parser.add_argument("bedrooms", required=True)
            parser.add_argument("bathrooms", required=True)
            parser.add_argument("address", type=str, required=False)
            parser.add_argument("latitude", required=False)
            parser.add_argument("longitude", required=False)
            parser.add_argument("price", required=False)
            parser.add_argument("living_area", required=False)
            parser.add_argument("lot_size", required=False)
            parser.add_argument("rental_service", type=str, required=False)
            parser.add_argument("max_guests", required=False)
            parser.add_argument("year_built", required=False)
            
            for s_var, t_control in interactive_exposed.e_EXPOSED_OPTIONS.items():
                if t_control[0] == True:
                    parser.add_argument(s_var, type=str, required=False)
            for s_var, t_control in interactive_exposed.e_EXPOSED_FEATURES.items():
                if t_control[0] == True:
                    parser.add_argument(s_var, type=str, required=False)
            for s_var, t_control in interactive_exposed.e_EXPOSED_LOCATIONS.items():
                if t_control[0] == True:
                    parser.add_argument(s_var, type=str, required=False)
            
            req_args = parser.parse_args()
            
            if "latitude" in req_args:
                if isinstance(req_args["latitude"], str):
                    if req_args["latitude"].strip() == "":
                        del req_args["latitude"]
                    else:
                        req_args["latitude"] = utils.to_float(req_args["latitude"])
            if "longitude" in req_args:
                if isinstance(req_args["longitude"], str):
                    if req_args["longitude"].strip() == "":
                        del req_args["longitude"]
                    else:
                        req_args["longitude"] = utils.to_float(req_args["longitude"])
            
            try:
                e_json = request.get_json()
            except:
                print("Couldnt' process json from request")
                e_json = {}
            
            e_request = e_json if isinstance(e_json, dict) else {}
            e_request.update(req_args)
            
            v_user_id, v_user_email = None, None
            if e_request:
                for k in ["contact_id", "contact[id]", "user_id", "userid"]:
                    if e_request.get(k):
                        v_user_id = e_request[k]
                        break
                
                for k in ["email", "contact[email", "user_email"]:
                    if e_request.get(k):
                        v_user_email = e_request[k]
                        break
            
            if req_args.get("request_id"):
                request_id = req_args["request_id"]
            else:
                s_api_key = get_api_key(request, **kwargs)
                request_id = g.o_DB.insert(
                    db_tables.interactive_requests,
                    {
                        "api_key": s_api_key,
                        "endpoint": "/comps",
                        "body": req_args,
                        "content": json.dumps(e_request),
                        "user_id": v_user_id,
                        "user_email": v_user_email,
                        "ip_address": request.remote_addr,
                    },
                    return_id=True,)
            
            #print("req_args", req_args, "\n")
            #e_input = single_property_predictor.unstack_input_dict(req_args)
            #print("e_input", e_input, "\n")
            
            (e_input, e_pai, e_address), i_status = property_comps.prep_comp_property(e_request)
            
            if i_status != 200:
                e_input["Example inputs"] = """See /interactive/examples for examples of both the minimum required fields, and all the potential optional fields that could be included.
                    Field names are all lowercase. Either address or latitude/longitude are required, but you don't have to include both.
                    Excluded features and location values will default to whatever is most common in the property's area.
                    Excluded amenities default to true.
                    Excluded conditions default to false."""
                    
                #close_app_db()
                return utils.json_dict_prep(e_input), i_status
            
            #print("e_input", e_input, "\n")
            
            if req_args.get("show_as_table"):
                df_comps = property_comps.get_comps(e_input, as_df=True)
                s_form = s_INTERACTIVE_FORM.format(
                    title="Find Similar Properties",
                    property_type_options=s_PROPERTY_TYPE_OPTIONS,
                    exposed_options=s_EXPOSED_OPTIONS,
                    exposed_features=s_EXPOSED_FEATURES,
                    exposed_locations=s_EXPOSED_LOCATIONS,
                    other_input=s_OTHER_COMP_INPUT,
                    results="<p>{}</p>".format(df_comps.to_html(), na_rep="", float_format="{:,.2f}".format),
                )
                r_response = make_response(s_form, 200)
                print("interactive comps took", utils.secondsToStr(time() - n_start))
            else:
                a_comps = property_comps.get_comps(e_input, as_df=False)
            
                r_response = make_response(utils.json_dict_prep({
                    "message": "Interactive request accepted",
                    "request_id": request_id,
                    "address": e_address,
                    "comps": a_comps,
                    "search": e_input, #k: v for k, v in e_input_prepped.items() if k in single_property_predictor.e_VARS["train_cols"] or k in single_property_predictor.c_EXTRA_OUTPUTS},
                    "runtime": utils.secondsToStr(time() - n_start),
                }), 200)
                print("interactive comps took", utils.secondsToStr(time() - n_start))
            
            #close_app_db()
            return r_response
        
        except Exception as e:
            traceback.print_exc()
            s_trace = ''.join(traceback.format_exception(None, e, e.__traceback__))
            print(s_trace)
            #LOGGER.error(f"{utils.get_time()}\t'{str(e)}'")
            LOGGER.exception(f"{utils.get_time()}\tAPI User: {g.s_USER}. Error trying to find comps: {e} {json.dumps(req_args)}")
            r_response = make_response(utils.json_dict_prep({
                "message": "Interactive request failed",
                "error": str(e),
                "trace": s_trace,
            }), 500)
            
            #close_app_db()
            return r_response


class LocaleLookup(Resource):
    def options(self, headers=None, **kwargs):
        return t_CORS_OPTIONS_RESPONSE
    
    def post(self, **kwargs):
        n_start = time()
        #db_tables.load_config()
        add_db_to_app()
        
        try:
            parser = reqparse.RequestParser()
            parser.add_argument("market")
            parser.add_argument("city")
            parser.add_argument("county")
            parser.add_argument("state")
            parser.add_argument("zipcode")
            parser.add_argument("property_type")
            #parser.add_argument("bedrooms", type=int)
            parser.add_argument("bedrooms")
            #parser.add_argument("min_properties", type=int)
            parser.add_argument("min_properties")
            
            req_args = parser.parse_args()
            
            try:
                e_json = request.get_json()
            except:
                print("Couldnt' process json from request")
                e_json = {}
            
            e_request = e_json if isinstance(e_json, dict) else {}
            e_request.update(req_args)
            
            #print("e_request", e_request)
            
            v_user_id, v_user_email = None, None
            if req_args:
                for k in ["contact_id", "contact[id]", "user_id", "userid"]:
                    if req_args.get(k):
                        v_user_id = req_args[k]
                        break
                
                for k in ["email", "contact[email", "user_email"]:
                    if req_args.get(k):
                        v_user_email = req_args[k]
                        break
            
            if req_args.get("request_id"):
                request_id = req_args["request_id"]
            else:
                s_api_key = get_api_key(request, **kwargs)
                request_id = g.o_DB.insert(
                    db_tables.interactive_requests,
                    {
                        "api_key": s_api_key,
                        "endpoint": "/locale_lookup",
                        "body": json.dumps(e_request),
                        "user_id": v_user_id,
                        "user_email": v_user_email,
                        "ip_address": request.remote_addr,
                    },
                    return_id=True,)
            
            e_area = area_webhook.process_area_request(e_request)
            d_runtime = time() - n_start
            
            if e_area:
                r_response = make_response(utils.json_dict_prep({
                    "message": "Locale found",
                    "locale": e_request,
                    "area": e_area,
                    "request_id": request_id,
                    "runtime": utils.secondsToStr(d_runtime),
                }), 200)
            else:
                r_response = make_response(utils.json_dict_prep({
                    "message": "Locale not found",
                    "locale": e_request,
                    "area": e_area,
                    "request_id": request_id,
                    "runtime": utils.secondsToStr(d_runtime),
                }), 404)
            print("Locale lookup took", utils.secondsToStr(d_runtime))
            
            #close_app_db()
            return r_response
        
        except Exception as e:
            traceback.print_exc()
            s_trace = ''.join(traceback.format_exception(None, e, e.__traceback__))
            print(s_trace)
            #LOGGER.error(f"{utils.get_time()}\t'{str(e)}'")
            LOGGER.exception(f"{utils.get_time()}\tAPI User: {g.s_USER}. Error trying to lookup locale: {e} {json.dumps(utils.json_dict_prep(e_request))}")
            r_response = make_response(utils.json_dict_prep({
                "message": "Locale lookup request failed",
                "error": str(e),
                "trace": s_trace,
            }), 500)
            
            #close_app_db()
            return r_response


class LocalesLookup(Resource):
    def options(self, headers=None, **kwargs):
        return t_CORS_OPTIONS_RESPONSE
    
    def post(self, **kwargs):
        n_start = time()
        #db_tables.load_config()
        add_db_to_app()
        
        try:
            parser = reqparse.RequestParser()
            #parser.add_argument("locales", type=list, required=True)
            parser.add_argument("min_properties") #, type=int)
            
            req_args = parser.parse_args()
            
            try:
                e_json = request.get_json()
            except:
                print("Couldnt' process json from request")
                e_json = {}
            
            e_request = e_json if isinstance(e_json, dict) else {}
            e_request.update(req_args)
            
            #print("e_request", e_request)
            
            v_user_id, v_user_email = None, None
            if req_args:
                for k in ["contact_id", "contact[id]", "user_id", "userid"]:
                    if req_args.get(k):
                        v_user_id = req_args[k]
                        break
                
                for k in ["email", "contact[email", "user_email"]:
                    if req_args.get(k):
                        v_user_email = req_args[k]
                        break
            
            if req_args.get("request_id"):
                request_id = req_args["request_id"]
            else:
                s_api_key = get_api_key(request, **kwargs)
                request_id = g.o_DB.insert(
                    db_tables.interactive_requests,
                    {
                        "api_key": s_api_key,
                        "endpoint": "/locales_lookup",
                        "body": json.dumps(e_request),
                        "user_id": v_user_id,
                        "user_email": v_user_email,
                        "ip_address": request.remote_addr,
                    },
                    return_id=True,)
            
            a_locales = e_request.get("locales", [])
            
            #print("a_locales", type(a_locales), a_locales)
            #print("a_locales[0]", type(a_locales[0]), a_locales[0])
            
            if not a_locales:
                r_response = make_response(utils.json_dict_prep({
                    "message": 'No locales specified. Please pass in a list called "locales" of addresses as dictionaries containing some combination city/state/zipcode.',
                    "request": e_request,
                    "runtime": utils.secondsToStr(time() - n_start),
                }), 400)
            else:
                if e_request.get("min_properties"):
                    for e_locale in a_locales:
                        e_locale["min_properties"] = e_request["min_properties"]
                
                a_results = area_webhook.process_multiple_locales_mp(a_locales)
                
                r_response = make_response(utils.json_dict_prep({
                    "message": "Locales lookup request processed",
                    "locales": a_locales,
                    "areas": a_results,
                    "runtime": utils.secondsToStr(time() - n_start),
                }), 200)
                print("Locales lookup took", utils.secondsToStr(time() - n_start))
            
            #close_app_db()
            return r_response
        
        except Exception as e:
            traceback.print_exc()
            s_trace = ''.join(traceback.format_exception(None, e, e.__traceback__))
            print(s_trace)
            #LOGGER.error(f"{utils.get_time()}\t'{str(e)}'")
            LOGGER.exception(f"{utils.get_time()}\tAPI User: {g.s_USER}. Error trying to lookup locales: {e} {json.dumps(utils.json_dict_prep(e_request))}")
            r_response = make_response(utils.json_dict_prep({
                "message": "Locales lookup request failed",
                "error": str(e),
                "trace": s_trace,
            }), 500)
            
            #close_app_db()
            return r_response


class MetricLookup(Resource):
    def options(self, headers=None, **kwargs):
        return t_CORS_OPTIONS_RESPONSE
    
    def get(self, **kwargs):
        n_start = time()
        #db_tables.load_config()
        add_db_to_app()
        
        try:
            parser = reqparse.RequestParser()
            parser.add_argument("metric", type=str)
            parser.add_argument("market_type", type=str)
            parser.add_argument("property_type")
            parser.add_argument("bedrooms")
            parser.add_argument("min_properties")
            parser.add_argument("shrink")
            
            req_args = parser.parse_args()
            
            try:
                e_json = request.get_json()
            except:
                print("Couldnt' process json from request")
                e_json = {}
                
            
            e_request = e_json if isinstance(e_json, dict) else {}
            e_request.update(req_args)
        
            if not e_request.get("market_type"):
                e_request["market_type"] = ""
            if not e_request.get("metric"):
                e_request["metric"] = ""
            
            if e_request.get("min_properties"):
                try:
                    e_request["min_properties"] = int(utils.to_float(e_request["min_properties"]))
                except:
                    print(f"Invalid min_properties parameter: {e_request['min_properties']}")
                    e_request["min_properties"] = 0
            if e_request.get("bedrooms"):
                try:
                    e_request["bedrooms"] = int(utils.to_float(e_request["bedrooms"]))
                except:
                    print(f"Invalid bedrooms parameter: {e_request['bedrooms']}")
                    del e_request["bedrooms"]
            
            
            if req_args.get("request_id"):
                request_id = req_args["request_id"]
            else:
                s_api_key = get_api_key(request, **kwargs)
                request_id = g.o_DB.insert(
                    db_tables.interactive_requests,
                    {
                        "api_key": s_api_key,
                        "endpoint": "/metric_lookup",
                        "body": json.dumps(e_request),
                        "ip_address": request.remote_addr,
                    },
                    return_id=True,)
            
            s_error, a_results = "", []
            if not isinstance(e_request.get("market_type"), str) or not e_request["market_type"].strip() or e_request["market_type"].strip().lower() not in {"state", "city", "zipcode"}:
                s_error = f"Invalid market_type. Use state, city, or zipcode. Requested market_type = '{e_request.get('market_type', '')}'"
            elif not isinstance(e_request.get("metric"), str) or not e_request["metric"].strip() or e_request["metric"].strip().lower() not in db_tables.a_STATE_INFO_COLS:
                s_error = f"Invalid metric. Try 'revenue', 'rate', 'occupancy' or one of the other db number columns. Requested metric = '{e_request.get('metric', '')}'"
            else:
                try:
                    if e_request.get("shrink") in {True, 1, "True", "TRUE", "true", "1", "Yes", "YES", "yes"}:
                        f_get_metric = area_webhook.get_metric_from_db
                    else:
                        f_get_metric = area_webhook.get_shrunk_metric_from_db
                
                    a_results, a_cols = f_get_metric({
                        "metric": e_request["metric"].strip(),
                        "market_type": e_request["market_type"].strip(),
                        "property_type": e_request.get("property_type", ""),
                        "bedrooms": e_request.get("bedrooms", 0),
                        "min_properties": e_request.get("min_properties", 3),
                    })
                except Exception as e:
                    s_error = f"Failed to run metric lookup: {e}"
                    print(s_error)
                    LOGGER.warning(f"{utils.get_time()}\t'{s_error}'")
            
            if not s_error:
                r_response = make_response(utils.json_dict_prep({
                    "message": "Metric results",
                    "request": e_request,
                    "metric": a_results,
                    "columns": a_cols,
                    "request_id": request_id,
                    "runtime": utils.secondsToStr(time() - n_start),
                }), 200)
            else:
                r_response = make_response(utils.json_dict_prep({
                    "message": s_error,
                    "request": e_request,
                    "metric": a_results,
                    "columns": [],
                    "request_id": request_id,
                    "runtime": utils.secondsToStr(time() - n_start),
                }), 400)
            print("Metric lookup took", utils.secondsToStr(time() - n_start))
            
            #close_app_db()
            return r_response
        
        except Exception as e:
            traceback.print_exc()
            s_trace = ''.join(traceback.format_exception(None, e, e.__traceback__))
            print(s_trace)
            #LOGGER.error(f"{utils.get_time()}\t'{str(e)}'")
            LOGGER.exception(f"{utils.get_time()}\tAPI User: {g.s_USER}. Error trying to lookup metric: {e} {json.dumps(utils.json_dict_prep(e_request))}")
            r_response = make_response(utils.json_dict_prep({
                "message": "Metric lookup request failed",
                "error": str(e),
                "trace": s_trace,
            }), 500)
            
            #close_app_db()
            return r_response
    
    def post(self, **kwargs):
        n_start = time()
        #db_tables.load_config()
        add_db_to_app()
        
        try:
            parser = reqparse.RequestParser()
            parser.add_argument("metric", type=str)
            parser.add_argument("market_type", type=str)
            parser.add_argument("property_type")
            parser.add_argument("bedrooms")
            parser.add_argument("min_properties")
            parser.add_argument("shrink")
            req_args = parser.parse_args()
            
            try:
                e_json = request.get_json()
            except:
                print("Couldnt' process json from request")
                e_json = {}
            
            e_request = e_json if isinstance(e_json, dict) else {}
            e_request.update(req_args)
            
            if req_args.get("request_id"):
                request_id = req_args["request_id"]
            else:
                s_api_key = get_api_key(request, **kwargs)
                request_id = g.o_DB.insert(
                    db_tables.interactive_requests,
                    {
                        "api_key": s_api_key,
                        "endpoint": "/metric_lookup",
                        "body": json.dumps(e_request),
                        "ip_address": request.remote_addr,
                    },
                    return_id=True,)
        
            if not e_request.get("market_type"):
                e_request["market_type"] = ""
            if not e_request.get("metric"):
                e_request["metric"] = ""
        
            if e_request.get("min_properties"):
                try:
                    e_request["min_properties"] = int(utils.to_float(e_request["min_properties"]))
                except:
                    print(f"Invalid min_properties parameter: {e_request['min_properties']}")
                    e_request["min_properties"] = 0
            if e_request.get("bedrooms"):
                try:
                    e_request["bedrooms"] = int(utils.to_float(e_request["bedrooms"]))
                except:
                    print(f"Invalid bedrooms parameter: {e_request['bedrooms']}")
                    del e_request["bedrooms"]
            
            s_error, a_results = "", []
            if not isinstance(e_request.get("market_type"), str) or not e_request["market_type"].strip() or e_request["market_type"].strip().lower() not in {"state", "city", "zipcode"}:
                s_error = f"Invalid market_type. Use state, city, or zipcode. Requested market_type = '{e_request.get('market_type', '')}'"
            elif not isinstance(e_request.get("metric"), str) or not e_request["metric"].strip() or e_request["metric"].strip().lower() not in db_tables.a_STATE_INFO_COLS:
                s_error = f"Invalid metric. Try 'revenue', 'rate', 'occupancy' or one of the other db number columns. Requested metric = '{e_request.get('metric', '')}'"
            else:
                try:
                    if e_request.get("shrink") in {True, 1, "True", "TRUE", "true", "1", "Yes", "YES", "yes"}:
                        f_get_metric = area_webhook.get_metric_from_db
                    else:
                        f_get_metric = area_webhook.get_shrunk_metric_from_db
                
                    a_results, a_cols = f_get_metric({
                        "metric": e_request["metric"],
                        "market_type": e_request["market_type"],
                        "property_type": e_request.get("property_type", ""),
                        "bedrooms": e_request.get("bedrooms", 0),
                        "min_properties": e_request.get("min_properties", 3),
                    })
                except Exception as e:
                    s_error = f"Failed to run metric lookup: {e}"
                    print(s_error)
                    LOGGER.warning(f"{utils.get_time()}\t'{s_error}'")
            
            if not s_error:
                r_response = make_response(utils.json_dict_prep({
                    "message": "Metric results",
                    "locale": e_request,
                    "metric": a_results,
                    "columns": a_cols,
                    "request_id": request_id,
                    "runtime": utils.secondsToStr(time() - n_start),
                }), 200)
            else:
                r_response = make_response(utils.json_dict_prep({
                    "message": s_error,
                    "locale": e_request,
                    "metric": a_results,
                    "columns": [],
                    "request_id": request_id,
                    "runtime": utils.secondsToStr(time() - n_start),
                }), 400)
            print("Metric lookup took", utils.secondsToStr(time() - n_start))
            
            #close_app_db()
            return r_response
        
        except Exception as e:
            traceback.print_exc()
            s_trace = ''.join(traceback.format_exception(None, e, e.__traceback__))
            print(s_trace)
            r_response = make_response(utils.json_dict_prep({
                "message": "Metric lookup request failed",
                "error": str(e),
                "trace": s_trace,
            }), 500)
            
            #close_app_db()
            return r_response


class DocDBMinMax(Resource):
    def options(self, headers=None, **kwargs):
        return t_CORS_OPTIONS_RESPONSE
    
    def post(self, **kwargs):
        n_start = time()
        #db_tables.load_config()
        add_db_to_app()
        
        try:
            parser = reqparse.RequestParser()
            parser.add_argument("collection")
            req_args = parser.parse_args()
            
            try:
                e_json = request.get_json()
            except:
                print("Couldnt' process json from request")
                e_json = {}
            
            e_request = e_json if isinstance(e_json, dict) else {}
            e_request.update({k: v for k, v in req_args.items() if v})
            #print("e_request", e_request)
            
            if not b_DOCDB_LIVE:
                r_response = make_response(utils.json_dict_prep({
                    "message": "DocDB connection not loaded",
                    "request": e_request,
                    "runtime": utils.secondsToStr(time() - n_start),
                }), 404)
                return r_response
            
            if not e_request.get("collection") or not isinstance(e_request.get("collection"), str):
                e_request["collection"] = "properties"
            
            if not e_request.get("filter") or not isinstance(e_request.get("filter"), dict):
                e_request["filter"] = {}
            
            if not e_request.get("fields") or not isinstance(e_request.get("fields"), (list, tuple)):
                e_request["fields"] = [
                    "price",
                    "bedrooms",
                    "bathrooms",
                    "revr_score_overall",
                    #"revr_optimized_occupancy",
                    "revr_optimized_revenue",
                    "revr_optimized_cap",
                    "revr_long_term_rental_revenue",
                    "revr_long_term_cap",
                    "scraping_date",
                ]
                
            e_metrics = docdb_utils.get_property_metric_min_max(
                e_request["collection"].lower(),
                e_request["filter"],
                list(e_request["fields"]),
            )
            d_runtime = time() - n_start
            if d_runtime > 15:
                print(f"MinMax was slow: {utils.secondsToStr(d_runtime)}. Request: ", e_request)
                LOGGER.warning(f"MinMax was slow: {utils.secondsToStr(d_runtime)}. Request: {str(e_request)}", )
            
            if e_metrics:
                r_response = make_response(utils.json_dict_prep({
                    "message": "Min/Max values",
                    "metrics": e_metrics,
                    "runtime": utils.secondsToStr(d_runtime),
                }), 200)
            else:
                r_response = make_response(utils.json_dict_prep({
                    "message": "No records found matching filter and containing those fields",
                    "metrics": e_metrics,
                    "runtime": utils.secondsToStr(d_runtime),
                }), 404)
            print("Min/max lookup took", utils.secondsToStr(time() - n_start))
            
            #close_app_db()
            return r_response
        
        except Exception as e:
            traceback.print_exc()
            s_trace = ''.join(traceback.format_exception(None, e, e.__traceback__))
            print(s_trace)
            LOGGER.exception(f"{utils.get_time()}\tAPI User: {g.s_USER}. Error trying to lookup min/max values: {e} {json.dumps(utils.json_dict_prep(e_request))}")
            r_response = make_response(utils.json_dict_prep({
                "message": "Min/max lookup request failed",
                "error": str(e),
                "trace": s_trace,
            }), 500)
            
            #close_app_db()
            return r_response


class TopNearbyZipcodes(Resource):
    def options(self, headers=None, **kwargs):
        return t_CORS_OPTIONS_RESPONSE
    
    def post(self, **kwargs):
        n_start = time()
        #db_tables.load_config()
        add_db_to_app()
        
        try:
            parser = reqparse.RequestParser()
            parser.add_argument("market_id")
            parser.add_argument("latitude")
            parser.add_argument("longitude")
            parser.add_argument("min_score")
            parser.add_argument("max_distance")
            parser.add_argument("max_search")
            parser.add_argument("top_zipcodes")
            parser.add_argument("score_type")
            
            req_args = parser.parse_args()
            
            if "latitude" in req_args:
                if isinstance(req_args["latitude"], str):
                    if req_args["latitude"].strip() == "":
                        del req_args["latitude"]
                    else:
                        req_args["latitude"] = utils.to_float(req_args["latitude"])
            if "longitude" in req_args:
                if isinstance(req_args["longitude"], str):
                    if req_args["longitude"].strip() == "":
                        del req_args["longitude"]
                    else:
                        req_args["longitude"] = utils.to_float(req_args["longitude"])
            
            try:
                e_json = request.get_json()
            except:
                print("Couldnt' process json from request")
                e_json = {}
            
            e_request = e_json if isinstance(e_json, dict) else {}
            e_request.update({k: v for k, v in req_args.items() if v is not None})
                        
            if e_request.get("latitude") and isinstance(e_request.get("latitude"), str): e_request["latitude"] = float(e_request["latitude"])
            if e_request.get("longitude") and isinstance(e_request.get("longitude"), str): e_request["longitude"] = float(e_request["longitude"])

            if e_request.get("min_score"): e_request["min_score"] = float(e_request["min_score"])
            if e_request.get("max_distance"): e_request["max_distance"] = float(e_request["max_distance"])
            if e_request.get("max_search"): e_request["max_search"] = int(e_request["max_search"])
            if e_request.get("top_zipcodes"): e_request["top_zipcodes"] = int(e_request["top_zipcodes"])
                
            if e_request.get("fields"):
                if isinstance(e_request["fields"], (list, tuple, set)):
                    a_fields = list(e_request["fields"])
                elif isinstance(e_request["fields"], str):
                    a_fields = [s.strip() for s in kwargs["fields"].split(",")]
                else:
                    a_fields = db_tables.a_ZIP_INFO_COLS
            else:
                a_fields = db_tables.a_ZIP_INFO_COLS
            
            if "score_type" in e_request:
                if str(e_request["score_type"]).lower() in {"ltr", "long_term", "rent", "revr_long_term_cap", "rent_per_price", "revr_ltr_score_overall"}:
                    #s_score_key="revr_ltr_score_overall"
                    if "price" not in a_fields: a_fields.append("price")
                    if "rent" not in a_fields: a_fields.append("rent")
                    s_score_key = "rent_per_price"
                elif str(e_request["score_type"]).lower() in {"str", "short_term", "airbnb", "vacation", "score_overall", "revr_score_overall"}:
                    s_score_key="score_overall"
                else:
                    s_score_key="distance"
            else:
                s_score_key = "distance"
                
            #print("e_request", e_request)
            #print("s_score_key", s_score_key)
            
            if e_request.get("latitude") is not None and e_request.get("longitude") is not None:
                d_latitude, d_longitude = e_request["latitude"], e_request["longitude"]
            elif e_request.get("market_id") and e_request.get("market_id") not in {"undefined", "null", "None", "nan"}:
                t_gps = None
                try:
                    i_market_id = int(e_request["market_id"])
                    if not area_data.e_MARKET_GPS: area_data.load_data()
                    t_gps = area_data.e_MARKET_GPS.get(i_market_id)
                except Exception as e:
                    print(f"Couldn't lookup gps coordinates for market id '{e_request['market_id']}'.", e)
                if not t_gps or t_gps[0] is None or t_gps[1] is None:
                    return make_response(utils.json_dict_prep({
                        "message": "Unknown market_id",
                        "market_id": e_request.get("market_id"),
                        "runtime": utils.secondsToStr(time() - n_start),
                    }), 400)
                d_latitude, d_longitude = t_gps
            else:
                return make_response(utils.json_dict_prep({
                    "message": "No valid latitude/longitude or market_id in request",
                    "market_id": e_request.get("market_id"),
                    "runtime": utils.secondsToStr(time() - n_start),
                }), 400)
            
            e_sort = {}
            
            a_matched_rows, d_matched_distance = area_data.get_closest_zipcodes(
                d_latitude,
                d_longitude,
                **e_request
            )
            a_nearest = [
                dict(zip(["distance"] + a_fields, t_zip))
                for t_zip in a_matched_rows[:e_request.get("max_search", 100)] if t_zip[0] <= e_request.get("max_distance", 50)]
            
            d_max_distance = max([e_zip["distance"] for e_zip in a_nearest]) if a_nearest else e_request.get("max_distance", 50)
            
            e_sort["by"] = "sort_by"
            e_sort["default_value"] = e_request.get("sort_default_value", 0)
            e_sort["reverse"] = e_request.get("sort_reverse", True)
            
            for e_zipcode in a_nearest:
                if s_score_key == "rent_per_price":
                    #print("new rent_per_price", e_zipcode["rent_per_price"], (e_zipcode["rent"] * 12) / e_zipcode["price"])
                    e_zipcode["rent_per_price"] = (e_zipcode["rent"] * 12) / e_zipcode["price"]
                e_zipcode[e_sort["by"]] = area_data.get_zipcode_nearness_score(e_zipcode, s_score_key=s_score_key, **e_request)
            a_nearest.sort(key=lambda e_prop: e_prop.get(e_sort["by"], e_sort["default_value"]), reverse=e_sort["reverse"])
            
            #e_nearest["nearest"] = a_nearest[:e_request.get("top_zipcodes", 5)]
            
            r_response = make_response(utils.json_dict_prep({
                "message": "Top nearby zipcodes",
                "sort": e_sort,
                "max_distance": d_max_distance,
                "nearest": a_nearest[:e_request.get("top_zipcodes", 5)],
                "runtime": utils.secondsToStr(time() - n_start),
            }), 200)
            
            print("Top nearby zipcodes lookup took", utils.secondsToStr(time() - n_start))
            
            #close_app_db()
            return r_response
        
        except Exception as e:
            traceback.print_exc()
            s_trace = ''.join(traceback.format_exception(None, e, e.__traceback__))
            print(s_trace)
            #LOGGER.error(f"{utils.get_time()}\t'{str(e)}'")
            LOGGER.exception(f"{utils.get_time()}\tAPI User: {g.s_USER}. Error trying to lookup top nearby zipcodes: {e} {json.dumps(utils.json_dict_prep(e_request))}")
            r_response = make_response(utils.json_dict_prep({
                "message": "Top nearby zipcodes lookup request failed",
                "error": str(e),
                "trace": s_trace,
            }), 500)
            
            #close_app_db()
            return r_response


class TopNearbyProperties(Resource):
    def options(self, headers=None, **kwargs):
        return t_CORS_OPTIONS_RESPONSE
    
    def post(self, **kwargs):
        n_start = time()
        #db_tables.load_config()
        add_db_to_app()
        
        try:
            parser = reqparse.RequestParser()
            parser.add_argument("market_id")
            parser.add_argument("latitude")
            parser.add_argument("longitude")
            parser.add_argument("min_score")
            parser.add_argument("max_distance")
            parser.add_argument("max_search")
            parser.add_argument("top_properties")
            parser.add_argument("score_type")
            
            req_args = parser.parse_args()
            
            if "latitude" in req_args:
                if isinstance(req_args["latitude"], str):
                    if req_args["latitude"].strip() == "":
                        del req_args["latitude"]
                    else:
                        req_args["latitude"] = utils.to_float(req_args["latitude"])
            if "longitude" in req_args:
                if isinstance(req_args["longitude"], str):
                    if req_args["longitude"].strip() == "":
                        del req_args["longitude"]
                    else:
                        req_args["longitude"] = utils.to_float(req_args["longitude"])
            
            try:
                e_json = request.get_json()
            except:
                print("Couldnt' process json from request")
                e_json = {}
            
            e_request = e_json if isinstance(e_json, dict) else {}
            e_request.update({k: v for k, v in req_args.items() if v is not None})
            
            if e_request.get("latitude") and isinstance(e_request.get("latitude"), str): e_request["latitude"] = float(e_request["latitude"])
            if e_request.get("longitude") and isinstance(e_request.get("longitude"), str): e_request["longitude"] = float(e_request["longitude"])
            
            if e_request.get("min_score"): e_request["min_score"] = float(e_request["min_score"])
            if e_request.get("max_distance"): e_request["max_distance"] = float(e_request["max_distance"])
            if e_request.get("max_search"): e_request["max_search"] = int(e_request["max_search"])
            if e_request.get("top_properties"): e_request["top_properties"] = int(e_request["top_properties"])
            
            
            if "score_type" in e_request:
                if str(e_request["score_type"]).lower() in {"ltr", "long_term", "rent", "revr_long_term_cap", "revr_ltr_score_overall"}:
                    #s_score_key="revr_ltr_score_overall"
                    s_score_key="revr_long_term_cap"
                elif str(e_request["score_type"]).lower() in {"str", "short_term", "airbnb", "vacation", "revr_score_overall", "revr_score_overall"}:
                    s_score_key="revr_score_overall"
                else:
                    s_score_key="distance"
            else:
                s_score_key = "distance"
            
            #print("e_request", e_request)
            #print("s_score_key", s_score_key)
            
            if not b_DOCDB_LIVE:
                r_response = make_response(utils.json_dict_prep({
                    "message": "DocDB connection not loaded",
                    "request": e_request,
                    "runtime": utils.secondsToStr(time() - n_start),
                }), 404)
                return r_response
            
            if e_request.get("latitude") is not None and e_request.get("longitude") is not None:
                d_latitude, d_longitude = e_request["latitude"], e_request["longitude"]
            elif e_request.get("market_id") and e_request.get("market_id") not in {"undefined", "null", "None", "nan"}:
                t_gps = None
                try:
                    i_market_id = int(e_request["market_id"])
                    if not area_data.e_MARKET_GPS: area_data.load_data()
                    t_gps = area_data.e_MARKET_GPS.get(i_market_id)
                except Exception as e:
                    print(f"Couldn't lookup gps coordinates for market id '{e_request['market_id']}'.", e)
                if not t_gps or t_gps[0] is None or t_gps[1] is None:
                    return make_response(utils.json_dict_prep({
                        "message": "Unknown market_id",
                        "market_id": e_request["market_id"],
                        "runtime": utils.secondsToStr(time() - n_start),
                    }), 400)
                d_latitude, d_longitude = t_gps
            else:
                return make_response(utils.json_dict_prep({
                    "message": "No valid latitude/longitude or market_id in request",
                    "market_id": e_request["market_id"],
                    "runtime": utils.secondsToStr(time() - n_start),
                }), 400)
            
            e_nearest = docdb_utils.get_top_nearby_properties(
                "properties", #e_request["collection"].lower(),
                latitude=d_latitude,
                longitude=d_longitude,
                max_distance_miles=e_request.get("max_distance", 50),
                max_search=e_request.get("max_search", 100),
                sorter=docdb_utils.get_property_nearness_score,
                sort_reverse=e_request.get("sort_reverse", True),
                sort_default_value=e_request.get("sort_default_value",),
                s_score_key=s_score_key,
                filter=e_request.get("filter",),
                projection=e_request.get("projection",),
                fields=e_request.get("fields",),
            )
            
            d_max_distance = max([e_prop["distance"] for e_prop in e_nearest["nearest"]]) if e_nearest.get("nearest") else e_nearest.get('max_distance_in_meters', 0) / 1609.344
            if not d_max_distance: d_max_distance = e_request.get("max_distance", 50)
            
            e_nearest["nearest"] = e_nearest["nearest"][:int(e_request.get("top_properties", 100))]
            
            r_response = make_response(utils.json_dict_prep({
                "message": "Top nearby properties",
                "sort": e_nearest.get("sort", {}),
                "max_distance": d_max_distance,
                "nearest": e_nearest.get("nearest", []),
                "runtime": utils.secondsToStr(time() - n_start),
            }), 200)
            
            print("Top nearby properties lookup took", utils.secondsToStr(time() - n_start))
            
            #close_app_db()
            return r_response
        
        except Exception as e:
            traceback.print_exc()
            s_trace = ''.join(traceback.format_exception(None, e, e.__traceback__))
            print(s_trace)
            #LOGGER.error(f"{utils.get_time()}\t'{str(e)}'")
            LOGGER.exception(f"{utils.get_time()}\tAPI User: {g.s_USER}. Error trying to lookup top nearby properties: {e} {json.dumps(utils.json_dict_prep(e_request))}")
            r_response = make_response(utils.json_dict_prep({
                "message": "Top nearby property lookup request failed",
                "error": str(e),
                "trace": s_trace,
            }), 500)
            
            #close_app_db()
            return r_response


class NearestSchools(Resource):
    def options(self, headers=None, **kwargs):
        return t_CORS_OPTIONS_RESPONSE


    def get(self, **kwargs):
        add_mongodb_to_app()
        if kwargs.get('lon') and kwargs.get('lat'):
            lon, lat = kwargs['lon'], kwargs['lat']
        elif kwargs.get('property_id'):
            property = g.o_MONGODB['scraping']['properties'].find_one({'zpid': kwargs['property_id']})
            try:
                lon, lat = property['gps']['coordinates']
            except (KeyError, ValueError) as e:
                LOGGER.exception("property with id {} dosn't have proper coordinates".format(kwargs['property_id'], ))
                r_response = make_response(utils.json_dict_prep({
                    "message": "Property should have lat and lon in gps field",
                    "error": str(e),
                }), 500)
                return r_response
        else:
            LOGGER.exception("Request dosn't have nor property_id neither lon and lat")
            r_response = make_response(utils.json_dict_prep({
                    "message": "You should provide ither lon and lat or property id",
                }), 400)
            return r_response
        tbgs = TransformByGeoSearch(g.o_MONGODB, 'scraping', 'properties', 'schools')
        schools = tbgs.find_nearest_objects(lon, lat, 'gps')
        
        r_response = make_response(utils.json_dict_prep({ "message": "OK", "result": schools}), 200)
        return r_response 


class GetMarketData(Resource):
    def options(self, headers=None, **kwargs):
        return t_CORS_OPTIONS_RESPONSE
    
    def post(self, **kwargs):
        n_start = time()
        #db_tables.load_config()
        add_db_to_app()
        
        try:
            parser = reqparse.RequestParser()
            parser.add_argument("market_id")
            req_args = parser.parse_args()
            try:
                e_json = request.get_json()
            except:
                print("Couldnt' process json from request")
                e_json = {}
            
            e_request = e_json if isinstance(e_json, dict) else {}
            e_request.update({k: v for k, v in req_args.items() if v is not None})
            if not e_request.get("market_id") or e_request.get("market_id") in {"undefined", "null", "None", "nan"}:
                return make_response(utils.json_dict_prep({
                    "message": "market_id not specified or invalid",
                    "request": e_request,
                    "runtime": utils.secondsToStr(time() - n_start),
                }), 400)
            
            
            e_market_info = area_data.get_market_id_info(e_request.get("market_id"))
            e_market_summary = area_data.get_market_id_summary(e_request.get("market_id"))
            e_market_average = area_data.get_market_id_average(e_request.get("market_id"))
            
            e_rent_optimization = {}
            if e_market_info:
                for d_diff in [-0.2, -0.1, 0, 0.1, 0.2]:
                    d_diff_days_on_market = np.mean([
                        max(0, defaults.calc_percent_diff_to_extra_days(d_diff)),
                        max(0, defaults.calc_percent_diff_to_percent_extra_days(d_diff, d_days_on_market=e_market_info.get("rent_days_on_market", 22.8)))])
                    e_rent_optimization[f"rent_{d_diff:0.0%}"] = {
                        "rent": round(float(e_market_info.get("rent", 0)) * (1 + d_diff), 2),
                        "days_on_market": d_diff_days_on_market,
                        "rent_first_year": round(float(e_market_info.get("rent", 0)) * (1 + d_diff) * 12 * (max(0, 365 - d_diff_days_on_market) / 365), 2),
                    }
            
            r_response = make_response(utils.json_dict_prep({
                "message": "Market info",
                "info": e_market_info,
                "current_summary": e_market_summary,
                "average_summary": e_market_average,
                "rent_optimization": e_rent_optimization,
                "runtime": utils.secondsToStr(time() - n_start),
            }), 200)
            
            print("Market info and summary lookup took", utils.secondsToStr(time() - n_start))
            
            #close_app_db()
            return r_response
        
        except Exception as e:
            traceback.print_exc()
            s_trace = ''.join(traceback.format_exception(None, e, e.__traceback__))
            print(s_trace)
            #LOGGER.error(f"{utils.get_time()}\t'{str(e)}'")
            LOGGER.exception(f"{utils.get_time()}\tAPI User: {g.s_USER}. Error trying to lookup Market info and summary: {e} {json.dumps(utils.json_dict_prep(e_request))}")
            r_response = make_response(utils.json_dict_prep({
                "message": "Market info and summary lookup request failed",
                "error": str(e),
                "trace": s_trace,
            }), 500)
            
            #close_app_db()
            return r_response


class GetVars(Resource):
    def options(self, headers=None, **kwargs):
        return t_CORS_OPTIONS_RESPONSE
    
    def get(self, **kwargs):
        return utils.json_dict_prep(e_VARS), 200


##### LOAD APP #####
print("Creating flask app")
app = Flask(__name__, template_folder='templates')
app.config['JSONIFY_PRETTYPRINT_REGULAR'] = True
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
app.config['UPLOAD_FOLDER'] = paths.s_INPUT_PATH
app.config['SECRET_KEY'] = generate_key()
app.config['MAX_CONTENT_LENGTH'] = i_FILE_SIZE_LIMIT * (1024 * 1024)  # 15 megabyte filesize limit

def add_db_to_app():
    if not hasattr(g, 'o_DB') or g.o_DB is None:
        g.o_DB = db_tables.get_connection()
    if not hasattr(g, 'dt_USERS_UPDATE'):
        g.dt_USERS_UPDATE = datetime.utcnow() - timedelta(days=1)
    if not hasattr(g, 's_USER'):
        g.s_USER = "unknown"


def add_mongodb_to_app():
    if not hasattr(g, 'o_MONGODB') or g.o_MONGODB is None:
        mongodb_connector = DocDBConnector()
        mongodb_connector._load()
        g.o_MONGODB = mongodb_connector._client


@app.teardown_appcontext
def close_app_db(error=None):
    '''Closes the database connection at the end of request.'''    
    if hasattr(g, 'o_DB'):
        g.o_DB.close()
        g.o_DB = None


@app.teardown_appcontext
def close_app_mongodb(error=None):
    '''Closes the mongodb database connection at the end of request.'''    
    if hasattr(g, 'o_MONGODB'):
        g.o_MONGODB._close()
        g.o_MONGODB = None


api = Api(app)
#AutoIndex(app, browse_root="/predictions") 

print("Flask CORS")
cors = CORS(
    app,
    resources=r"/*",
    allow_headers=[
        "Content-Type",
        "Authorization",
        "Access-Control-Allow-Credentials",
        "api_key",
    ],
    supports_credentials=True)

api.add_resource(EndPoints, "/", "/endpoints", "/endpoints/")
api.add_resource(Variables, "/variables", "/variables/")
api.add_resource(GetVars, "/vars", "/vars/")

api.add_resource(DevWebhook, "/dev/webhook", "/interactive/webhooks/new_contact")

api.add_resource(WebhookActiveCampaignContactProperty, "/interactive/webhooks/contact_manual_form")
api.add_resource(InputURL, "/input/url", "/input/url/")
api.add_resource(InteractivePredictor, "/interactive", "/interactive/")
api.add_resource(InteractiveComps, "/comps", "/comps/")
api.add_resource(InteractiveFinancing, "/financing", "/financing/")
api.add_resource(LocaleLookup, "/locale_lookup", "/locale_lookup/")
api.add_resource(LocalesLookup, "/locales_lookup", "/locales_lookup/", "/locale_lookups", "/locale_lookups/", "/locales_lookups", "/locales_lookups/")
api.add_resource(MetricLookup, "/metric_lookup", "/metric_lookup/")
api.add_resource(DocDBMinMax, "/docdb/min_max", "/docdb/min_max/")

api.add_resource(TopNearbyZipcodes, "/nearby/best_zipcodes", "/nearby/best_zipcodes/", "/nearby/top_zipcodes", "/nearby/top_zipcodes/")
api.add_resource(TopNearbyProperties, "/nearby/top_properties", "/nearby/top_properties/", "/nearby/best_properties", "/nearby/best_properties/")
api.add_resource(GetMarketData, "/market_info", "/market_info/", "/market_summary", "/market_summary/")
api.add_resource(NearestSchools, "/...")

@app.route('/generate_api_key')
def make_keys(**kwargs):
    s_api_key = generate_key()
    return make_response(f"""<!doctype html>
        <title>API Key</title>
        <h3>API Key to use</h3><p>{s_api_key}</p>""", 200)


def allowed_file(filename):
    return os.path.splitext(filename)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/input/batch', methods=['GET', 'POST'])
@authenticate
def upload_zillow_file(**kwargs):

    add_db_to_app()
    
    e_header = { k.lower().replace("-", "_"): v for k, v in request.headers.items()}
    s_key = e_header.get("api_key", "")
    if not s_key: s_key = e_header.get("api key", "")
    if not s_key: s_key = kwargs.get("api_key", "")
    if not s_key: 
        parser = reqparse.RequestParser()
        parser.add_argument("api_key", required=False)
        req_args = parser.parse_args()
        s_key = req_args.get("api_key", "")
    if s_key:
        s_key = re.sub(r"(^['\" ]+|['\" ]+$)", "", s_key)

    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            LOGGER.warning(f"{utils.get_time()}\t'No file part'")
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        
        LOGGER.info(f"{utils.get_time()}\t'Received file: {file.filename}.")
        
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            LOGGER.warning(f"{utils.get_time()}\t'No selected file'")
            flash('No selected file')
            
            #close_app_db()
            return redirect(request.url)
        
        if not allowed_file(file.filename):
            LOGGER.warning(f"{utils.get_time()}\t'Invalid file extension: {os.path.splitext(file.filename)[1]}'")
            flash('Invalid file extension: {os.path.splitext(file.filename)[1]}')
            
            #close_app_db()
            return redirect(request.url)
            
        
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(path)
            LOGGER.info(f"{utils.get_time()}\t'Saving file to: {path}'")
            #LOGGER.info(f"redirect to '{url_for('get_input_file', path=filename)}'")
            d_size = os.path.getsize(path) /  (1024 * 1024)
            #return redirect(url_for('get_input_file', path=filename, api_key=s_key))
            
            #close_app_db()
            return (utils.json_dict_prep({
                "message": "File upload succeeded",
                "filename": filename,
                "size": f"{d_size:0.1f} Mb",
                #"url": url_for('get_input_file', path=filename, api_key=s_key)
                }), 200)
    
    #close_app_db()
    return '''
        <!doctype html>
        <title>Upload zillow csv file to run prediction on</title>
        <h1>Upload zillow csv for revenue prediction</h1>
        <form method=post enctype=multipart/form-data>
          <input type=file name=file>
          <input type=submit value=Upload>
        </form>
        '''


@app.route('/output/batch', methods=['GET', 'POST'])
@authenticate
def upload_prediction_result(**kwargs):
    add_db_to_app()
    
    e_header = { k.lower().replace("-", "_"): v for k, v in request.headers.items()}
    s_key = e_header.get("api_key", "")
    if not s_key: s_key = e_header.get("api key", "")
    if not s_key: s_key = kwargs.get("api_key", "")
    if not s_key: 
        parser = reqparse.RequestParser()
        parser.add_argument("api_key", required=False)
        req_args = parser.parse_args()
        s_key = req_args.get("api_key", "")
    if s_key:
        s_key = re.sub(r"(^['\" ]+|['\" ]+$)", "", s_key)

    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            LOGGER.warning(f"{utils.get_time()}\t'No file part'")
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        
        LOGGER.info(f"{utils.get_time()}\t'Received file: {file.filename}.")
        
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            LOGGER.warning(f"{utils.get_time()}\t'No selected file'")
            flash('No selected file')
            return redirect(request.url)
        
        if not allowed_file(file.filename):
            LOGGER.warning(f"{utils.get_time()}\t'Invalid file extension: {os.path.splitext(file.filename)[1]}'")
            flash('Invalid file extension: {os.path.splitext(file.filename)[1]}')
            return redirect(request.url)
            
        
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            path = os.path.join(s_APP_PATH, filename)
            file.save(path)
            LOGGER.info(f"{utils.get_time()}\t'Saving file to: {path}'")
            LOGGER.info(f"redirect to '{url_for('get_output_file', path=filename)}'")
            d_size = os.path.getsize(path) /  (1024 * 1024)
            #return redirect(url_for('get_output_file', path=filename, api_key=s_key))
            
            #close_app_db()
            return (utils.json_dict_prep({
                "message": "File upload succeeded",
                "filename": filename,
                "size": f"{d_size:0.1f} Mb",
                "url": url_for('get_output_file', path=filename, api_key=s_key)
                }), 200)
    
    #close_app_db()
    return '''
        <!doctype html>
        <title>Upload revenue prediction result csv file</title>
        <h1>Upload revenue prediction result csv file</h1>
        <form method=post enctype=multipart/form-data>
          <input type=file name=file>
          <input type=submit value=Upload>
        </form>
        '''


@app.route('/input')
@authenticate
def input_dirtree(**kwargs):
    s_api_key = get_api_key(request, **kwargs)
    if s_api_key: s_api_key = f"api_key={s_api_key}"
    return render_template(
        os.path.join(r"dirtree.html"),
        tree=make_tree(paths.s_INPUT_PATH, "input"),
        api_key=s_api_key,
    )

@app.route('/input/<path:path>')
@authenticate
def get_input_file(path):
    return send_from_directory(paths.s_INPUT_PATH, path)


@app.route('/working')
@authenticate
def working_dirtree(**kwargs):
    s_api_key = get_api_key(request, **kwargs)
    if s_api_key: s_api_key = f"api_key={s_api_key}"
    return render_template(
        os.path.join(r"dirtree.html"),
        tree=make_tree(paths.s_WORKING_PATH, "working"),
        api_key=s_api_key,
    )

@app.route('/working/<path:path>')
@authenticate
def get_working_file(path):
    return send_from_directory(paths.s_WORKING_PATH, path)

@app.route('/completed')
@authenticate
def completed_dirtree(**kwargs):
    s_api_key = get_api_key(request, **kwargs)
    if s_api_key: s_api_key = f"api_key={s_api_key}"
    #print('s_api_key', s_api_key)
    return render_template(
        os.path.join(r"dirtree.html"),
        tree=make_tree(paths.s_COMPLETE_PATH, "completed"),
        api_key=s_api_key,
    )

@app.route('/completed/<path:path>')
@authenticate
def get_completed_file(path):
    return send_from_directory(paths.s_COMPLETE_PATH, path)
    

@app.route('/predictions')
@authenticate
def predictions_dirtree(**kwargs):
    s_api_key = get_api_key(request, **kwargs)
    if s_api_key: s_api_key = f"api_key={s_api_key}"
    return render_template(
        os.path.join(r"dirtree.html"),
        tree=make_tree(paths.s_PREDICTION_PATH, "predictions"),
        api_key=s_api_key,
    )


@app.route('/predictions/<path:path>')
@authenticate
def get_prediction_file(path):
    return send_from_directory(paths.s_PREDICTION_PATH, path)


@app.route('/failed')
@authenticate
def failed_dirtree(**kwargs):
    s_api_key = get_api_key(request, **kwargs)
    if s_api_key: s_api_key = f"api_key={s_api_key}"
    return render_template(
        os.path.join(r"dirtree.html"),
        tree=make_tree(paths.s_FAILED_PATH, "failed"),
        api_key=s_api_key,
    )

@app.route('/failed/<path:path>')
@authenticate
def get_failed_file(path):
    return send_from_directory(paths.s_FAILED_PATH, path)


@app.route('/files')
@authenticate
def files_dirtree(**kwargs):
    s_api_key = get_api_key(request, **kwargs)
    if s_api_key: s_api_key = f"api_key={s_api_key}"
    return render_template(
        os.path.join(r"dirtree.html"),
        tree=make_tree(paths.s_FILES, "files"),
        api_key=s_api_key,
    )

@app.route('/files/<path:path>')
@authenticate
def get_file(path):
    return send_from_directory(paths.s_FILES, path)

@app.route('/interactive/examples')
@authenticate
def examples_dirtree(**kwargs):
    s_api_key = get_api_key(request, **kwargs)
    if s_api_key: s_api_key = f"api_key={s_api_key}"
    return render_template(
        os.path.join(r"dirtree.html"),
        tree=make_tree(paths.s_INTERACTIVE_EXAMPLES_PATH, "examples"),
        api_key=s_api_key,
    )
@app.route('/interactive/examples/<path:path>')
@authenticate
def get_example(path):
    return send_from_directory(paths.s_INTERACTIVE_EXAMPLES_PATH, path)


def make_tree(path, parent_path=""):
    #dt_time = os.path.getmtime(path)
    tree = dict(name=os.path.basename(path), children=[], ) #modified_dt=os.path.getmtime(os.path.basename(path)))
    try: lst = os.listdir(path)
    except OSError:
        pass #ignore errors
    else:
        for name in lst:
            fn = os.path.join(path, name)
            if os.path.isdir(fn):
                tree['children'].append(make_tree(fn, os.path.join(parent_path, name)))
            else:
                #print(os.path.join(path, name))
                s_meta = ""
                s_size = ""
                s_modified = ""
                s_local_path = os.path.join(path, name)
                timestamp = 0
                if os.path.isfile(s_local_path):
                    s_size = "{}KB".format(round(os.path.getsize(os.path.join(path, name)) / 1024, 2))
                    timestamp = os.path.getmtime(os.path.join(path, name))
                    dt_time = datetime.fromtimestamp(timestamp)
                    s_time = ctime(timestamp)
                    #print(dt_time, s_time)
                    s_meta = f"({s_size}, {s_time})"
                tree['children'].append(dict(
                    name=name,
                    url=os.path.join(parent_path, name),
                    size=s_size,
                    modified=s_time,
                    timestamp=timestamp,
                    #modified_dt=dt_time,
                    meta=s_meta,
                ))
    tree['children'].sort(key=lambda e_f: (e_f.get("timestamp", 0), e_f.get("name", "")))
    #print("\n\ntree", tree)
    return tree


print("Finished loading")

if __name__ == "__main__":
    args, kwargs = utils.parse_command_line_args(sys.argv)
    print("kwargs", kwargs)
    
    b_debug = kwargs.get("debug", True)
    s_host = kwargs.get("host", '0.0.0.0')
    i_port = kwargs.get("port", 5000)
    
    LOGGER.info(f"{utils.get_time()}\tStart revenue_api.py on {s_host}:{i_port}")
    print("Run app")
    
    if "process_live" in kwargs:
        b_PROCESS_LIVE = True if kwargs.get("process_live", False) in {True, "true", "TRUE", "True", 1, "yes", "YES", "Yes", "On", "ON"} else False
    
    if b_PROCESS_LIVE:
        print("Run process_api_requests right after requests. No cron job required.")
        try:
            from . import process_api_requests
        except:
            import process_api_requests
    else:
        print("Don't process_api_requests right after requests. Must have a cron job scheduled to process inputs.")
    
    if "interactive_live" in kwargs:
        b_INTERACTIVE_LIVE = True if kwargs.get("interactive_live", True) in {True, "true", "TRUE", "True", 1, "yes", "YES", "Yes", "On", "ON"} else False
    
    if b_INTERACTIVE_LIVE:
        print("Keep models and data in memory to quickly process single property analyses")
        try:
            from . import single_property_predictor
        except:
            import single_property_predictor
    else:
        print("Don't provide single property interactive predictor")

    if "docdb" in kwargs:
        b_DOCDB_LIVE = True if kwargs.get("docdb", True) in {True, "true", "TRUE", "True", 1, "yes", "YES", "Yes", "On", "ON"} else False
    if b_DOCDB_LIVE:
        print("Load connection to AWS DocumentDB")
        try:
            from . import docdb_utils
        except:
            import docdb_utils
    else:
        print("Don't connect to AWS DocumentDB")
            
    app.run(host=s_host, port=i_port, debug=b_debug, threaded=True, processes=1)
    print("App running")
