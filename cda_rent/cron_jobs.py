import os
import sys

import pymongo

from col_transform import TransformCollection, Extract, TransformByGeoSearch
from docdb_utils import DocDBConnector
from mysql_db import mySqlDbConn


def mongo_connect_decorator(func):
    def wrapper(*args, **kwargs):
        mongodb_connector = DocDBConnector()
        mongodb_connector._load()
        mongo_client = mongodb_connector._client
        res = func(*args, mongo_client=mongo_client, **kwargs)
        mongodb_connector._close()
        return res
    return wrapper


def mysql_connect_decorator(func):
    def wrapper(*args, **kwargs):
        mysql_connector = mySqlDbConn()
        mysql_connector.load()
        mysql_cursor = mysql_connector._client
        res = func(*args, mysql_cursor=mysql_cursor, **kwargs)
        mysql_connector.close()
        return res
    return wrapper


@mongo_connect_decorator
@mysql_connect_decorator
def create_realtor_zillow_format(mongo_client=None, mysql_cursor=None):
    tc = TransformCollection(
        mongo_client, os.getenv('MONGO_DB_NAME'), 'realtor', 'realtor_formatted', mysql_cursor,
        [("property_id", pymongo.ASCENDING), ("scraped_date", pymongo.ASCENDING)], 'property_id'
        )
    tc.transform(
        'transform_realtor_to_zillow', 
        sql_db='predictor', 
        sql_table='location_translation', 
        sql_fields=['standardized_name', 'state'], 
        limit=None, 
        skip=None
        )


@mongo_connect_decorator
def extract_schools_from_realtor(mongo_client=None):
    es = Extract(mongo_client, mongo_db=os.getenv('MONGO_DB_NAME'))
    es.run('schools')


@mongo_connect_decorator
def setup_schools_to_each_zillow_property(mongo_client=None):
    tbgs = TransformByGeoSearch(mongo_client, os.getenv('MONGO_DB_NAME'), 'zillow_scraped', 'schools')
    tbgs.run('longitude', 'latitude', 'gps', 'schools')


@mongo_connect_decorator
def test_extract_rent_us_from_sale_us(mongo_client=None):
    es = Extract(mongo_client, mongo_db=os.getenv('MONGO_DB_NAME'), collection='sale_us', new_collection='rent_us')
    es.run('docs_by_condition',  filter_args= {"home_status": "FOR_RENT"}) 


if __name__ == '__main__':
    dict_func = {
        'create_realtor_zillow_format': create_realtor_zillow_format,
        'extract_schools_from_realtor': extract_schools_from_realtor,
        'setup_schools_to_each_zillow_property': setup_schools_to_each_zillow_property,
        'test_extract_rent_us_from_sale_us': test_extract_rent_us_from_sale_us
        }
    args = sys.argv
    if len(args) <= 1:
        pass
    else:
        for funcname in args[1:]:
            dict_func[funcname]()

