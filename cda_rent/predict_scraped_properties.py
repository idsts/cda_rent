﻿import os, sys, re, random
import requests, json
from urllib.request import quote
from datetime import datetime, timedelta
from dateutil.parser import parse as dateparse
from time import time, sleep
from collections import defaultdict, Counter
import traceback
import pandas as pd
import numpy as np
import usaddress

import sshtunnel
import pymongo
from pymongo import DeleteOne, UpdateOne, InsertOne
from pymongo.bulk import ObjectId

try:
    from . import utils, docdb_utils, zillow_standardization, airbnb_standardization, realtor_standardization, us_states
    from . import single_property_predictor, prep_zillow, predict_zillow, db_tables, area_data
except ImportError:
    import utils, docdb_utils, zillow_standardization, airbnb_standardization, realtor_standardization, us_states
    import single_property_predictor, prep_zillow, predict_zillow, db_tables, area_data

s_COLL_SCRAPED_REQUESTED = "zillow_demand_daily"
s_COLL_SCRAPED_NATIONAL = "sale_us"
s_COLL_SCRAPED = "zillow_scraped"
s_COLL_FAILURES = "zillow_scraped_failed"
s_COLL_PROPERTIES = "properties"

s_COLL_SCRAPED_REQUESTED_RENTALS = "zillow_rent_demand_daily"
s_COLL_SCRAPED_RENT = "rent_us"
s_COLL_RENTALS = "rentals"
s_COLL_SCRAPED_REALTOR = "realtor"
s_COLL_FAILURES_REALTOR = "realtor_scraped_failed"

s_COLL_PREDICTIONS = "predictions"
a_EXISTING_PRED_PROJ = ["_id", "zpid", "realtor_id", "predictions", "financing", "scoring", "status", "created", "address", "market_ids", "listing"]
a_EXISTING_LISTING_PROJ = ["_id", "zpid", "realtor_id", "scraping_date", "added_date", "sources", "zillow", "realtor"]

i_REAPPLY_BATCH = 100
i_SCRAPE_BATCH = 100
i_PREDICTION_LIFESPAN = 6 # in days
i_RETRY_DELAY = 60 # in minutes

td_RECENT = timedelta(days=1, hours=0)
d_MARKET_MISSING_MARGIN = 0.02
d_MARKET_BROKEN_MARGIN = 0.2

d_DELAY_BETWEEN_SCANS = 300 # in seconds
d_DELAY_BETWEEN_SCANS_PROCESSED = 10 # in seconds
d_DELAY_BETWEEN_BATCHES = 10 # in seconds
d_DELAY_BETWEEN_PREDICTIONS = 0.01 # in seconds

#a_HOME_STATUS_TO_PROCESS = ["FOR_RENT", "OTHER", "SOLD", "PENDING"]
a_SALE_HOME_STATUS_TO_PROCESS = ["FOR_SALE", "for_sale"] #, "SOLD", "PENDING"]
a_RENT_HOME_STATUS_TO_PROCESS = ["FOR_RENT", "for_rent"]
a_SALE_PROPERTY_TYPE_EXCLUDES = ["Lot", "Apartment", 'Home Type Unknown', None, "mobile", 'investment', 'farm', 'coop', 'land', 'condop', 'condo_townhome_rowhome_coop', 'other']
a_RENT_PROPERTY_TYPE_EXCLUDES = ["Lot", 'Home Type Unknown', None, "mobile", 'investment', 'farm', 'coop', 'land', 'condop', 'condo_townhome_rowhome_coop', 'other']
a_SALE_PROPERTY_TYPE_INCLUDES = [
    'Multi Family', 'Townhouse', 'Manufactured', 'Single Family', 'Condo',
]
a_RENT_PROPERTY_TYPE_INCLUDES = [
    'Multi Family', 'Townhouse', 'Apartment', 'Manufactured', 'Single Family', 'Condo',
    'single_family', 'multi_family', 'condos', 'townhomes', 'duplex_triplex', 'apartment', 'condo_townhome',
]

e_STATE_MARKET_IDS, e_COUNTY_MARKET_IDS, e_CITY_MARKET_IDS, e_ZIPCODE_MARKET_IDS = {}, {}, {}, {}

d_MAX_RENT_PRICE = 12000
d_MIN_SALE_PRICE = 50000

re_UNIT = re.compile(r"(^,?(Ap(artmen)?t|S(ui)?te|R(oo)?m|Unit|#)[- :+/\\,]?|[-,_])", flags=re.I)

def load_config():
    global i_REAPPLY_BATCH, i_SCRAPE_BATCH, i_PREDICTION_LIFESPAN, i_RETRY_DELAY
    global td_RECENT, d_MARKET_MISSING_MARGIN, d_MARKET_BROKEN_MARGIN
    global d_DELAY_BETWEEN_SCANS, d_DELAY_BETWEEN_SCANS_PROCESSED, d_DELAY_BETWEEN_BATCHES, d_DELAY_BETWEEN_PREDICTIONS
    global e_STATE_MARKET_IDS, e_COUNTY_MARKET_IDS, e_CITY_MARKET_IDS, e_ZIPCODE_MARKET_IDS
    
    db_tables.load_config()
    
    i_REAPPLY_BATCH = db_tables.e_CONFIG.get("scrape_predictor_reapply_batch_size", i_SCRAPE_BATCH)
    i_SCRAPE_BATCH = db_tables.e_CONFIG.get("scrape_predictor_batch_size", i_REAPPLY_BATCH)
    i_PREDICTION_LIFESPAN = db_tables.e_CONFIG.get("scrape_predictor_prediction_lifespan_days", i_PREDICTION_LIFESPAN) # in days
    i_RETRY_DELAY = db_tables.e_CONFIG.get("scrape_predictor_retry_delay_minutes", i_RETRY_DELAY) # in minutes
    
    td_RECENT = timedelta(days=db_tables.e_CONFIG.get("scrape_predictor_requested_lifespan_days", 1), hours=db_tables.e_CONFIG.get("scrape_predictor_requested_lifespan_hours", 0))
    d_MARKET_MISSING_MARGIN = db_tables.e_CONFIG.get("scrape_predictor_market_missing_acceptable", 0.02)
    d_MARKET_BROKEN_MARGIN = db_tables.e_CONFIG.get("scrape_predictor_market_missing_broken", 0.2)
    
    d_DELAY_BETWEEN_SCANS = db_tables.e_CONFIG.get("scrape_predictor_delay_between_empty_scans", 600) # in seconds
    d_DELAY_BETWEEN_SCANS_PROCESSED = db_tables.e_CONFIG.get("scrape_predictor_delay_between_full_scans", 10) # in seconds
    d_DELAY_BETWEEN_BATCHES = db_tables.e_CONFIG.get("scrape_predictor_delay_between_batches", 10) # in seconds
    d_DELAY_BETWEEN_PREDICTIONS = db_tables.e_CONFIG.get("scrape_predictor_delay_between_predictions", 0.01) # in seconds
    
    o_db = db_tables.get_connection()
    
    a_state_market_ids = o_db.execute(r"SELECT state, id FROM revr.market where type = 'state';")
    e_STATE_MARKET_IDS = {state.upper(): market_id for (state, market_id) in a_state_market_ids}
    
    a_county_market_ids = o_db.execute(r"SELECT state, county, location, id FROM revr.market where type = 'county';")
    e_COUNTY_MARKET_IDS = {(state.upper(), county.lower()): market_id for (state, county, location, market_id) in a_county_market_ids}
    e_COUNTY_MARKET_IDS.update({(state.upper(), location.lower()): market_id for (state, county, location, market_id) in a_county_market_ids})
    
    a_city_market_ids = o_db.execute(r"SELECT state, location, id FROM revr.market where type = 'city';")
    e_CITY_MARKET_IDS = {(state.upper(), city.lower()): market_id for (state, city, market_id) in a_city_market_ids}
    
    a_zipcode_market_ids = o_db.execute(r"SELECT state, location, id FROM revr.market where type = 'zipcode';")
    e_ZIPCODE_MARKET_IDS = {(state.upper(), zipcode): market_id for (state, zipcode, market_id) in a_zipcode_market_ids}


def find_market_ids(e_property):
    if not e_STATE_MARKET_IDS: load_config()
    
    s_state = e_property.get("state", "")
    if not s_state or not isinstance(s_state, str): return {}
    
    s_state = s_state.upper()
    if len(s_state) > 2:
        s_state = us_states.e_STATE_ABBREV.get(s_state.lower()).upper()
    
    s_county = e_property.get("county")
    s_city = e_property.get("city")
    s_zipcode = e_property.get("zip")
    #print(s_state, s_county, s_city, s_zipcode)
    
    e_market_ids = {}
    i_state = e_STATE_MARKET_IDS.get(s_state) if s_state else None
    i_county = e_COUNTY_MARKET_IDS.get((s_state, s_county.lower())) if s_county else None
    i_city = e_CITY_MARKET_IDS.get((s_state, s_city.lower())) if s_city else None
    i_zip = e_ZIPCODE_MARKET_IDS.get((s_state, str(s_zipcode)[:5])) if s_zipcode else None
    
    if i_state: e_market_ids["state"] = i_state
    if i_county: e_market_ids["county"] = i_county
    if i_city: e_market_ids["city"] = i_city
    if i_zip: e_market_ids["zipcode"] = i_zip
    
    return e_market_ids


def get_input_property_type_name(e_inputs):
    a_ptypes = [
        k[14:]
        for k, v in e_inputs.items()
        if v and k.startswith("property_type_")
    ]
    return "/".join(a_ptypes) if a_ptypes else None


class ScrapedPropertyPredictor(object):
    def __init__(self, **kwargs):
        self._host = kwargs.get("host", docdb_utils.s_DOCDB_HOST)
        self._port = int(kwargs.get("port", docdb_utils.i_DOCDB_PORT))
        self._user = kwargs.get("user", docdb_utils.s_DOCDB_USER)
        self._password = kwargs.get("password", docdb_utils.s_DOCDB_PASS)
        self._schema = kwargs.get("schema", docdb_utils.s_DOCDB_SCHEMA)
        self._ca_file = kwargs.get("ca_file", docdb_utils.s_DOCDB_CA_FILE)
        assert self._host
        assert self._port
        assert self._user
        assert self._password
        assert self._schema
        assert self._ca_file and os.path.isfile(self._ca_file)
        
        self._local_host = "0.0.0.0"
        self._local_port = utils.free_port()
        assert self._local_port
        assert self._local_host
        
        self._ssh_host = os.environ.get("CDA_TUNNEL_HOST")
        self._ssh_user = os.environ.get("CDA_TUNNEL_USER")
        self._ssh_pkey = os.environ.get("CDA_TUNNEL_PKEY")
        if self._ssh_host:
            assert self._ssh_user
            assert self._ssh_pkey and os.path.isfile(self._ssh_pkey)
        
        self._tunnel, self._client, self._db = None, None, None
        self._coll_scraped = None
        self._coll_scraped_requested = None
        self._coll_scraped_national = None
        self._coll_properties = None
        self._coll_scraped_rent_requested = None
        self._coll_scraped_rent = None
        self._coll_scraped_realtor = None
        self._coll_rentals = None
        self._coll_predictions = None
        self._coll_failures = None
        self._coll_failures_realtor = None
    
    def _load_tunnel(self, **kwargs):
        if kwargs.get("reload_tunnel") or self._tunnel is None:
            if self._tunnel is not None:
                self._tunnel.stop()
                self._tunnel = None
                sleep(5)
            
            if self._ssh_host:
                self._tunnel = sshtunnel.open_tunnel(
                    (docdb_utils.s_SSH_HOST, 22),
                    ssh_username=docdb_utils.s_SSH_USER,
                    ssh_pkey=docdb_utils.s_SSH_PKEY,
                    #ssh_private_key_password=docdb_utils.s_SSH_PKEY_SECRET,
                    remote_bind_address=(docdb_utils.s_DOCDB_HOST, self._port),
                    local_bind_address=(self._local_host, self._local_port),
                )
                self._tunnel.start()
    
    def _load(self, **kwargs):
        if kwargs.get("b_debug"): print('opening connection to documentdb')
        if self._ssh_host:
            self._load_tunnel(**kwargs)
            s_host = "127.0.0.1"
            i_port = self._local_port
        else:
            s_host = self._host
            i_port = self._port
        
        if kwargs.get("reload_client") or self._client is None:
            self._client = pymongo.MongoClient(
                f'mongodb://{self._user}:{quote(self._password)}@{s_host}:{i_port}/?authMechanism=DEFAULT&tls=true&tlsCAFile={quote(self._ca_file)}&retryWrites=false',
                tls=True,
                tlsAllowInvalidCertificates=True,
                tlsCAFile=self._ca_file,
                connect=True,
                directConnection=True,
            )
        
        self._db = self._client[self._schema]
        self._coll_scraped = self._db[s_COLL_SCRAPED]
        self._coll_scraped_requested = self._db[s_COLL_SCRAPED_REQUESTED]
        self._coll_scraped_national = self._db[s_COLL_SCRAPED_NATIONAL]
        self._coll_properties = self._db[s_COLL_PROPERTIES]
        
        self._coll_scraped_rent_requested = self._db[s_COLL_SCRAPED_REQUESTED_RENTALS]
        self._coll_scraped_rent = self._db[s_COLL_SCRAPED_RENT]
        self._coll_scraped_realtor = self._db[s_COLL_SCRAPED_REALTOR]
        self._coll_rentals = self._db[s_COLL_RENTALS]
        self._coll_predictions = self._db[s_COLL_PREDICTIONS]
        self._coll_failures = self._db[s_COLL_FAILURES]
        self._coll_failures_realtor = self._db[s_COLL_FAILURES_REALTOR]
        
        
        if prep_zillow.o_AREA_LOOKUPS is None or prep_zillow.o_HOTELS is None or not prep_zillow.e_ZCTA:
            prep_zillow.load_data() #**kwargs)
        
        if predict_zillow.o_SCALER is None or predict_zillow.o_MAPE_MODEL is None:
            predict_zillow.load_data() #**kwargs)
    
    def _close(self, **kwargs):
        if kwargs.get("b_debug"): print('closing connection to documentdb')
        if self._tunnel is not None:
            self._tunnel.stop()
            sleep(5)
        self._tunnel = None
        self._client = None
        self._db = None
        self._coll_scraped = None
        self._coll_scraped_requested = None
        self._coll_scraped_national = None
        self._coll_properties = None
        self._coll_scraped_rent_requested = None
        self._coll_scraped_rent = None
        self._coll_scraped_realtor = None
        self._coll_rentals = None
        self._coll_predictions = None
        self._coll_failures = None
        self._coll_failures_realtor = None
    
    #def __enter__(self):
    #    self._load()
    
    def __exit__(self, exc_type, exc_value, traceback):
        self._close()
    
    def get_scraped_zillow_batch(self, i_batch_size=i_SCRAPE_BATCH, i_retry_delay=i_RETRY_DELAY, **kwargs):
        if kwargs.get("rentals"):
            if kwargs.get("requested"):
                o_coll = self._coll_scraped_rent_requested
            elif kwargs.get("realtor"):
                o_coll = self._coll_scraped_realtor
            elif kwargs.get("national"):
                o_coll = self._coll_scraped_rent
            else:
                o_coll = self._coll_scraped_rent
        else:
            if kwargs.get("requested"):
                o_coll = self._coll_scraped_requested
            elif kwargs.get("realtor"):
                o_coll = self._coll_scraped_realtor
            elif kwargs.get("national"):
                o_coll = self._coll_scraped_national
            else:
                o_coll = self._coll_scraped
        
        e_filter = {}
        e_extra_filter = {}
        
        if kwargs.get("market_id"):
            e_filter.update({
                "market_id": kwargs["market_id"]
            })
        
        if kwargs.get("rentals"):
            if kwargs.get("realtor"):
                e_filter.update({
                    "listing_status": {"$in": a_RENT_HOME_STATUS_TO_PROCESS},
                    "price": {"$lte": d_MAX_RENT_PRICE},
                    "property_type": {"$in": a_RENT_PROPERTY_TYPE_INCLUDES},
                })
            else:
                e_filter.update({
                    "home_status": {"$in": a_RENT_HOME_STATUS_TO_PROCESS},
                    "price": {"$lte": d_MAX_RENT_PRICE},
                    "property_type": {"$in": a_RENT_PROPERTY_TYPE_INCLUDES},
                })
        else:
            if kwargs.get("realtor"):
                e_filter.update({
                    "listing_status": {"$in": a_SALE_HOME_STATUS_TO_PROCESS},
                    "price": {"$gte": d_MIN_SALE_PRICE},
                    "property_type": {"$in": a_SALE_PROPERTY_TYPE_INCLUDES},
                })
            else:
                e_filter.update({
                    "home_status": {"$in": a_SALE_HOME_STATUS_TO_PROCESS},
                    "price": {"$gte": d_MIN_SALE_PRICE},
                    "property_type": {"$in": a_SALE_PROPERTY_TYPE_INCLUDES},
                })
        
        if kwargs.get("realtor"):
            e_filter.update({
                "bed": {"$gt": 0, "$lte": 16},
                "bath": {"$gt": 0, "$lte": 18},
            })
            e_extra_filter.update({
               "$and": [
                    {"latitude": {"$gte": -90, "$lte": 90}},
                    {"longitude": {"$gte": -180, "$lte": 180}},
                ],
                "property_id": {"$gt": "0"},
            })
        else:
            e_filter.update({
                "bedrooms": {"$gt": 0, "$lte": 16},
                "bathrooms": {"$gt": 0, "$lte": 18},
            })
            e_extra_filter.update({
                "$and": [
                    {"latitude": {"$gte": -90, "$lte": 90}},
                    {"longitude": {"$gte": -180, "$lte": 180}},
                ],
                #"zpid": {"$nin": [np.nan, None]},
                "zpid": {"$gt": 0},
            })
        
        if kwargs.get("no_filter"):
            pass
        elif kwargs.get("retry"):
            e_extra_filter.update({
                "$or": [
                    {"revr_date": None},
                    {"revr_date": {"$lte": datetime.utcnow() - timedelta(minutes=i_retry_delay)}},
                ],
                "revr_status": {"$ne": None},
            })
        else:
            e_extra_filter.update({
                "revr_status": None,
                #"revr_score_overall": {"$gt": 0},
            })
        
        if kwargs.get("b_debug"):
            print("get_scraped_zillow_batch e_filter", e_filter)
        
        a_steps = []
        if kwargs.get("realtor"):
            a_steps.append({"$sort": {"property_id": 1, "scraping_date": -1}}) #make sure there is an index for this
        else:
            a_steps.append({"$sort": {"zpid": 1, "scraping_date": -1}}) #make sure there is an index for this
        
        a_steps.append({"$match": e_filter})
        if e_extra_filter: a_steps.append({"$match": e_extra_filter})
        
        #if kwargs.get("national") or kwargs.get("rentals"):
        i_scraper_id = kwargs.get("scraper_id", 1)
        
        print(f"scraper id {i_scraper_id} skip {(int(i_scraper_id - 1) * 2) * i_batch_size}")
        if i_scraper_id > 1:
            a_steps.append({"$skip": (int(i_scraper_id - 1) * 2) * i_batch_size})
        
        a_steps.append({"$limit": max(0, i_batch_size)})
        
        z_batch = o_coll.aggregate(a_steps)
        #z_batch = o_coll.find(
        #    e_filter,
        #    limit=max(0, i_batch_size))
        
        return z_batch
    
    def count_scraped_zillow_batch(self, i_retry_delay=i_RETRY_DELAY, **kwargs):
        if kwargs.get("rentals"):
            if kwargs.get("requested"):
                o_coll = self._coll_scraped_rent_requested
            elif kwargs.get("realtor"):
                o_coll = self._coll_scraped_realtor
            elif kwargs.get("national"):
                o_coll = self._coll_scraped_rent
            else:
                o_coll = self._coll_scraped_rent
        else:
            if kwargs.get("requested"):
                o_coll = self._coll_scraped_requested
            elif kwargs.get("realtor"):
                o_coll = self._coll_scraped_realtor
            elif kwargs.get("national"):
                o_coll = self._coll_scraped_national
            else:
                o_coll = self._coll_scraped
        
        if kwargs.get("realtor"):
            e_filter = {
               "$and": [
                    {"bed": {"$gt": 0, "$lte": 16}},
                    {"bath": {"$gt": 0, "$lte": 18}},
                    {"latitude": {"$gte": -90, "$lte": 90}},
                    {"longitude": {"$gte": -180, "$lte": 180 }},
               ],
                #"property_id": {"$nin": [np.nan, None]},
                "property_id": {"$gt": '0'},
            }
        else:
            e_filter = {
               "$and": [
                    {"bedrooms": {"$gt": 0, "$lte": 16}},
                    {"bathrooms": {"$gt": 0, "$lte": 18}},
                    {"latitude": {"$gte": -90, "$lte": 90}},
                    {"longitude": {"$gte": -180, "$lte": 180 }},
               ],
                #"zpid": {"$nin": [np.nan, None]},
                "zpid": {"$gt": 0},
            }
            
        if kwargs.get("market_id"):
            e_filter["$and"].append({"market_id": kwargs["market_id"]})
        
        if kwargs.get("rentals"):
            if kwargs.get("realtor"):
                e_filter.update({
                    "listing_status": {"$in": a_RENT_HOME_STATUS_TO_PROCESS},
                    "price": {"$lte": d_MAX_RENT_PRICE},
                    "property_type": {"$in": a_RENT_PROPERTY_TYPE_INCLUDES},
                })
            else:
                e_filter.update({
                    "home_status": {"$in": a_RENT_HOME_STATUS_TO_PROCESS},
                    "price": {"$lte": d_MAX_RENT_PRICE},
                    "property_type": {"$in": a_RENT_PROPERTY_TYPE_INCLUDES},
                })
        else:
            e_filter.update({
                "home_status": {"$in": a_SALE_HOME_STATUS_TO_PROCESS},
                "price": {"$gte": d_MIN_SALE_PRICE},
                "property_type": {"$in": a_SALE_PROPERTY_TYPE_INCLUDES},
            })
        
        
        if kwargs.get("no_filter"):
            pass
        elif kwargs.get("retry"):
            e_filter.update({
                "$or": [
                    {"revr_date": None},
                    {"revr_date": {"$lte": datetime.utcnow() - timedelta(minutes=i_retry_delay)}},
                ],
                "revr_status": {"$ne": None},
            })
        else:
            e_filter.update({
                "revr_status": None,
            })
        
        if kwargs.get("b_debug"):
            print("count_scraped_zillow_batch e_filter", e_filter)
        i_count = o_coll.count_documents(e_filter)
        return i_count
    
    def get_properties_batch(self, i_batch_size=i_REAPPLY_BATCH, i_retry_delay=i_RETRY_DELAY, **kwargs):
        if kwargs.get("rentals"):
            o_coll = self._coll_rentals
        else:
            o_coll = self._coll_properties
        
        if kwargs.get("filters"):
            z_batch = o_coll.find(
                kwargs["filters"],
                limit=max(1, i_batch_size))
        elif kwargs.get("no_filter"):
            z_batch = o_coll.find(
                #{
                #    "zpid": {"$ne": np.nan},
                #},
                {},
                limit=max(1, i_batch_size))
        elif kwargs.get("retry"):
            z_batch = o_coll.find(
                {
                    "revr_status": {"$in": [0, 400, 500]},
                    "revr_date": {"$lte": datetime.utcnow() - timedelta(minutes=i_retry_delay)},
                    #"zpid": {"$ne": np.nan},
                },
                limit=max(1, i_batch_size))
        else:
            z_batch = o_coll.find(
                {"revr_status": None},
                limit=max(1, i_batch_size))
        return z_batch
    
    def count_properties_batch(self, i_retry_delay=i_RETRY_DELAY, **kwargs):
        if kwargs.get("rentals"):
            o_coll = self._coll_rentals
        else:
            o_coll = self._coll_properties
        if kwargs.get("no_filter"):
            i_count = o_coll.count_documents(
                #{
                #    "zpid": {"$ne": np.nan},
                #},
                {})
        elif kwargs.get("retry"):
            i_count = o_coll.count_documents(
                {
                    "revr_status": {"$in": [0, 400, 500]},
                    "revr_date": {"$lte": datetime.utcnow() - timedelta(minutes=i_retry_delay)},
                    #"zpid": {"$ne": np.nan},
                },)
        else:
            i_count = o_coll.count_documents(
                {"revr_status": None},)
        return i_count
    
    def count_bad_zillow_batch(self, **kwargs):
        if kwargs.get("rentals"):
            if kwargs.get("requested"):
                o_coll = self._coll_scraped_rent_requested
            elif kwargs.get("realtor"):
                o_coll = self._coll_scraped_realtor
            elif kwargs.get("national"):
                o_coll = self._coll_scraped_rent
            else:
                o_coll = self._coll_scraped_rent
        else:
            if kwargs.get("requested"):
                o_coll = self._coll_scraped_requested
            elif kwargs.get("realtor"):
                o_coll = self._coll_scraped_realtor
            elif kwargs.get("national"):
                o_coll = self._coll_scraped_national
            else:
                o_coll = self._coll_scraped
        
        if kwargs.get("realtor"):
            e_filter = {
                "$or": [
                    {"bed": {"$lt": 1}},
                    {"bed": {"$gt": 16}},
                    {"bath": {"$lt": 0.5}},
                    {"bath": {"$gt": 18}},
                    {"bed": np.nan},
                    {"bath": np.nan},
                    {"bed": None},
                    {"bath": None},
                    {"latitude": np.nan},
                    {"longitude": np.nan},
                    {"latitude": None},
                    {"longitude": None},
                    {"listing_status": {"$in": a_SALE_HOME_STATUS_TO_PROCESS}, "price": {"$lt": d_MIN_SALE_PRICE}},
                    {"listing_status": {"$in": a_RENT_HOME_STATUS_TO_PROCESS}, "price": {"$gte": d_MAX_RENT_PRICE}},
                    {"listing_status": {"$in": a_SALE_HOME_STATUS_TO_PROCESS}, "property_type": {"$in": a_SALE_PROPERTY_TYPE_EXCLUDES}},
                    {"listing_status": {"$in": a_RENT_HOME_STATUS_TO_PROCESS}, "property_type": {"$in": a_RENT_PROPERTY_TYPE_EXCLUDES}},
                    {"property_id": None},
                    {"property_id": np.nan},
                    {"listing_status": {"$nin": a_SALE_HOME_STATUS_TO_PROCESS + a_RENT_HOME_STATUS_TO_PROCESS}},
                ]
            }

        else:
            e_filter = {
                "$or": [
                    {"bedrooms": {"$lt": 1}},
                    {"bedrooms": {"$gt": 16}},
                    {"bathrooms": {"$lt": 0.5}},
                    {"bathrooms": {"$gt": 18}},
                    {"bedrooms": np.nan},
                    {"bathrooms": np.nan},
                    {"bedrooms": None},
                    {"bathrooms": None},
                    {"latitude": np.nan},
                    {"longitude": np.nan},
                    {"latitude": None},
                    {"longitude": None},
                    {"home_status": {"$in": a_SALE_HOME_STATUS_TO_PROCESS}, "price": {"$lt": d_MIN_SALE_PRICE}},
                    {"home_status": {"$in": a_RENT_HOME_STATUS_TO_PROCESS}, "price": {"$gte": d_MAX_RENT_PRICE}},
                    {"home_status": {"$in": a_SALE_HOME_STATUS_TO_PROCESS}, "property_type": {"$in": a_SALE_PROPERTY_TYPE_EXCLUDES}},
                    {"home_status": {"$in": a_RENT_HOME_STATUS_TO_PROCESS}, "property_type": {"$in": a_RENT_PROPERTY_TYPE_EXCLUDES}},
                    #{"$and": [{"zpid": None}, {"realtor_id": None}]},
                    #{"$and": [{"zpid": np.nan}, {"realtor_id": np.nan}]},
                    {"zpid": None},
                    {"zpid": np.nan},
                    {"home_status": {"$nin": a_SALE_HOME_STATUS_TO_PROCESS + a_RENT_HOME_STATUS_TO_PROCESS}},
                ]
            }
        
        if kwargs.get("market_id"):
            e_filter["$and"] = [{"market_id": kwargs["market_id"]}]
        
        i_count = o_coll.count_documents(e_filter)
        return i_count
    
    def move_bad_zillow_batch(self, **kwargs):
        o_coll_failures = self._coll_failures
        if kwargs.get("rentals"):
            if kwargs.get("requested"):
                o_coll = self._coll_scraped_rent_requested
            elif kwargs.get("realtor"):
                o_coll = self._coll_scraped_realtor
                o_coll_failures = self._coll_failures_realtor
            elif kwargs.get("national"):
                o_coll = self._coll_scraped_rent
            else:
                o_coll = self._coll_scraped_rent
        else:
            if kwargs.get("requested"):
                o_coll = self._coll_scraped_requested
            elif kwargs.get("realtor"):
                o_coll = self._coll_scraped_realtor
                o_coll_failures = self._coll_failures_realtor
            elif kwargs.get("national"):
                o_coll = self._coll_scraped_national
            else:
                o_coll = self._coll_scraped
        
        if kwargs.get("realtor"):
            e_filter = {
                "$or": [
                    {"bed": {"$lt": 1}},
                    {"bed": {"$gt": 16}},
                    {"bath": {"$lt": 0.5}},
                    {"bath": {"$gt": 18}},
                    {"bed": np.nan},
                    {"bath": np.nan},
                    {"bed": None},
                    {"bath": None},
                    {"latitude": np.nan},
                    {"longitude": np.nan},
                    {"latitude": None},
                    {"longitude": None},
                    {"listing_status": {"$in": a_SALE_HOME_STATUS_TO_PROCESS}, "price": {"$lt": d_MIN_SALE_PRICE}},
                    {"listing_status": {"$in": a_RENT_HOME_STATUS_TO_PROCESS}, "price": {"$gte": d_MAX_RENT_PRICE}},
                    {"listing_status": {"$in": a_SALE_HOME_STATUS_TO_PROCESS}, "property_type": {"$in": a_SALE_PROPERTY_TYPE_EXCLUDES}},
                    {"listing_status": {"$in": a_RENT_HOME_STATUS_TO_PROCESS}, "property_type": {"$in": a_RENT_PROPERTY_TYPE_EXCLUDES}},
                    {"property_id": None},
                    {"property_id": np.nan},
                    {"listing_status": {"$nin": a_SALE_HOME_STATUS_TO_PROCESS + a_RENT_HOME_STATUS_TO_PROCESS}},
                ]
            }

        else:
            e_filter = {
                "$or": [
                    {"bedrooms": {"$lt": 1}},
                    {"bedrooms": {"$gt": 16}},
                    {"bathrooms": {"$lt": 0.5}},
                    {"bathrooms": {"$gt": 18}},
                    {"bedrooms": np.nan},
                    {"bathrooms": np.nan},
                    {"bedrooms": None},
                    {"bathrooms": None},
                    {"latitude": np.nan},
                    {"longitude": np.nan},
                    {"latitude": None},
                    {"longitude": None},
                    {"home_status": {"$in": a_SALE_HOME_STATUS_TO_PROCESS}, "price": {"$lt": d_MIN_SALE_PRICE}},
                    {"home_status": {"$in": a_RENT_HOME_STATUS_TO_PROCESS}, "price": {"$gte": d_MAX_RENT_PRICE}},
                    {"home_status": {"$in": a_SALE_HOME_STATUS_TO_PROCESS}, "property_type": {"$in": a_SALE_PROPERTY_TYPE_EXCLUDES}},
                    {"home_status": {"$in": a_RENT_HOME_STATUS_TO_PROCESS}, "property_type": {"$in": a_RENT_PROPERTY_TYPE_EXCLUDES}},
                    #{"$and": [{"zpid": None}, {"realtor_id": None}]},
                    #{"$and": [{"zpid": np.nan}, {"realtor_id": np.nan}]},
                    {"zpid": None},
                    {"zpid": np.nan},
                    {"home_status": {"$nin": a_SALE_HOME_STATUS_TO_PROCESS + a_RENT_HOME_STATUS_TO_PROCESS}},
                ]
            }
        
        if kwargs.get("all_market_ids"):
            pass
        elif kwargs.get("market_id"):
            e_filter["$and"] = [{"market_id": kwargs["market_id"]}]
        else:
            e_filter["$and"] = [{"market_id": None}]
        
        try:
            docdb_utils.copy_between_collections(o_coll, o_coll_failures, e_filter, a_unique_ids=["zpid"])
            o_coll.delete_many(e_filter)
        except Exception as e:
            print(f"Couldn't copy bad zillow properties to failure collection:", e)
        
        #return (o_move_response, o_delete_response)
    
    #@utils.timer
    def find_existing_prediction(self, e_property, **kwargs):
        if kwargs.get("find_existing_by_id", True):
            a_to_check = []
            if not pd.isnull(e_property.get("_id")): a_to_check.append({"_id": e_property["_id"]})
            if not pd.isnull(e_property.get("zpid")): a_to_check.append({"zpid": e_property["zpid"]})
            #if not pd.isnull(e_property.get("property_id")): a_to_check.append({"property_id": e_property["property_id"]})
            if not pd.isnull(e_property.get("realtor_id")): a_to_check.append({"realtor_id": e_property["realtor_id"]})
            #print(a_to_check)
            if a_to_check:
                e_pred = self._coll_predictions.find_one({"$or": a_to_check}, projection=a_EXISTING_PRED_PROJ)
                if e_pred:
                    print("existing", e_pred["_id"], e_pred.get("zpid"), e_pred.get("realtor_id"))
                    return e_pred
        
        if kwargs.get("find_existing_by_location", True):
            
            if not pd.isnull(e_property.get("latitude")) and not pd.isnull(e_property.get("longitude")):
                i_beds = e_property.get("bedrooms", e_property.get("bed", 3))
                i_baths = e_property.get("bathrooms", e_property.get("bath", 2))
                e_query = {
                    "inputs.bedrooms": {"$gte": max(0, i_beds - 1), "$lte": min(16, i_beds + 1)},
                    #"inputs.bathrooms": {"$gte": max(0, i_baths - 1), "$lte": min(16, i_baths + 1)},
                    "inputs.bathrooms": i_baths,
                }
                
                if not pd.isnull(e_property.get("zpid")):
                    e_query["zpid"] = None
        
                #if not pd.isnull(e_property.get("property_id")):
                #    e_query["property_id"] = None
        
                if not pd.isnull(e_property.get("realtor_id")):
                    e_query["realtor_id"] = None
        
                s_address, s_unit = e_property.get("address"), None
                if s_address:
                    a_address_pieces = usaddress.parse(s_address)
                    for t in a_address_pieces:
                        if t[1] == 'OccupancyIdentifier':
                            s_unit = re_UNIT.sub("", t[0])
                            if s_unit: break
                    if not s_unit:
                        for t in a_address_pieces:
                            if t[1] == 'Recipient':
                                s_unit = re_UNIT.sub("", t[0])
                                if s_unit: break
                    if s_unit:
                        e_query["inputs.bedrooms"] = i_beds
                
                a_geo_matches = list(self._coll_predictions.aggregate([
                    {
                        "$geoNear": {
                            "near": {
                                "type":"Point",
                                "coordinates": [e_property["longitude"], e_property["latitude"]]},
                            "spherical": True,
                            "distanceField": "distance",
                            #"distanceMultiplier": 0.001,
                            #"query": e_query
                        },
                    },
                    {"$match": e_query},
                    {"$match": {"distance": {"$lte": 10}}},
                    {"$limit": 3},
                ], hint="gps_created_INDEX"))
                
                if not a_geo_matches: return None
                
                if s_unit and a_geo_matches:
                    a_unit_matches = []
                    for e_match in a_geo_matches:
                        s_match_unit = None
                        if e_match["address"].get("address_components", {}).get("subpremise", []):
                            s_match_unit = e_match["address"]["address_components"]["subpremise"]
                            if isinstance(s_match_unit, (list, tuple)): s_match_unit = s_match_unit[0]
                            s_match_unit = re_UNIT.sub("", s_match_unit)
                            if str(s_match_unit).lower() == s_unit.lower():
                                a_unit_matches.append(e_match)
                        elif e_match["address"].get("address_components", {}).get("formatted_address"):
                            a_address_pieces = usaddress.parse(e_match["address"]["address_components"]["formatted_address"])
        
                            for t in a_address_pieces:
                                if t[1] == 'OccupancyIdentifier':
                                    s_unit = re_UNIT.sub("", t[0])
                                    if s_unit: break
                            if not s_unit:
                                for t in a_address_pieces:
                                    if t[1] == 'Recipient':
                                        s_unit = re_UNIT.sub("", t[0])
                                        if s_unit: break
                            
                            if s_match_unit and str(s_match_unit).lower() == s_unit.lower():
                                a_unit_matches.append(e_match)
                    if a_unit_matches:
                        print("existing geo unit", a_unit_matches[0]["_id"], a_unit_matches[0].get("zpid"), a_unit_matches[0].get("realtor_id"))
                        return a_unit_matches[0]
                elif a_geo_matches:
                    print("existing geo", a_geo_matches[0]["_id"], a_geo_matches[0].get("zpid"), a_geo_matches[0].get("realtor_id"))
                    return a_geo_matches[0]
    
    ##### Start processing methods #####
    
    def predict_property(self, e_property, e_existing_prediction=None, e_prior_listing=None, **kwargs):
        
        e_converted = zillow_standardization.convert_zillow_row(e_property.copy(), keep_description_text=False)
        e_std = self.standardize_scraped(e_property, **kwargs)
        o_pred_id = None
        if e_existing_prediction and e_existing_prediction.get("_id"): o_pred_id = e_existing_prediction["_id"]
        if not o_pred_id and e_prior_listing and e_prior_listing.get("prediction_id"): o_pred_id = e_prior_listing["prediction_id"]
        
        dt_scraped = e_property.get("scraping_date")
        if not dt_scraped: dt_scrapted = datetime.utcnow()
        if isinstance(dt_scraped, str):
            e_property["scraping_date"] = dt_scraped = dateparse(dt_scraped)
        
        dt_added = None
        if e_prior_listing:
            dt_added = e_prior_listing.get("added_date")
            if not dt_added: dt_added = e_prior_listing.get("scraping_date")
            if isinstance(dt_added, str):
                e_property["added_date"] = dt_added = dateparse(dt_added)
        elif e_property.get("added_date"):
            dt_added = e_property["added_date"]
            if isinstance(dt_added, str):
                e_property["added_date"] = dt_added = dateparse(dt_added)
        elif kwargs.get("find_existing", True):
            o_coll = self._coll_rentals if kwargs.get("rentals") else self._coll_properties
            a_listing_lookups = []
            if e_property.get("_id"): a_listing_lookups.append({"_id": e_property["_id"]})
            if e_property.get("zpid"): a_listing_lookups.append({"zpid": e_property["zpid"]})
            if e_property.get("realtor_id"): a_listing_lookups.append({"realtor_id": e_property["realtor_id"]})
            #if e_property.get("property_id"): a_listing_lookups.append({"property_id": e_property["property_id"]})
            
            n_listing_start = time()
            if a_listing_lookups:
                e_prior_listing = o_coll.find_one(
                    {"$or": a_listing_lookups},
                    projection=a_EXISTING_LISTING_PROJ
                )
                if e_prior_listing:
                    if e_prior_listing.get("added_date"):
                        dt_added = e_prior_listing["added_date"]
                        if isinstance(dt_added, str):
                            e_property["added_date"] = dt_added = dateparse(dt_added)
                    
                    if e_prior_listing.get("prediction_id") and not e_existing_prediction:
                        e_existing_prediction = self._coll_predictions.find_one({"_id": e_prior_listing["prediction_id"]}, projection=a_EXISTING_PRED_PROJ)
                #print(f"listing_lookups took: {utils.secondsToStr(time() - n_listing_start)}")
        else:
            if not dt_added: dt_added = e_property.get("scraping_date", datetime.utcnow())
            if isinstance(dt_added, str):
                e_property["added_date"] = dt_added = dateparse(dt_added)
        if not dt_added: dt_added = datetime.utcnow()
        
        if not e_existing_prediction and kwargs.get("find_existing", True):
            n_find_pred_start = time()
            e_existing_prediction = self.find_existing_prediction(e_property, **kwargs)
            print(f"find_existing_prediction took: {utils.secondsToStr(time() - n_find_pred_start)}", bool(e_existing_prediction))
            if not o_pred_id and e_existing_prediction and e_existing_prediction.get("_id"): o_pred_id = e_existing_prediction["_id"]
        
        #print("e_existing_prediction", e_existing_prediction)
        
        #print("e_prior_listing", e_prior_listing)
        
        if e_converted.get("living_area"):
            e_converted["living_area"] = airbnb_standardization.clean_living_area(e_converted["living_area"])
        if e_converted.get("lot_size"):
            e_converted["lot_size"] = airbnb_standardization.clean_lot_size(e_converted["lot_size"])
        
        if "state" not in e_converted and "address_state" in e_converted:
            if isinstance(e_converted["address_state"], str):
                e_converted["state"] = us_states.e_STATE_ABBREV.get(e_converted["address_state"].lower(), e_converted["address_state"])
            else:
                e_converted["state"] = e_converted["address_state"]
        
        for k, v in e_converted.items():
            if k.startswith((
                "feature_", "location_",  "pets_allowed", "smoking_allowed",
                "lot_only", "condemned", "auction", "shared_ownership", "move_home",
                "fixer_upper", "renovated", "timeshare", "rent_control", "guesthouse")):
                e_converted[k] = bool(v)
        
        e_processed = single_property_predictor.process_single_property(
            e_converted,
            use_address=True,
            #skip_geocode=True,
            **kwargs)
        
        e_processed["listing"] = {
            "home_status": e_property.get("home_status"),
            "scraping_date": e_property.get("scraping_date"),}
        
        e_copy_predictions = {}
        #if e_existing_prediction:
        #    #e_copy_predictions["_id"] = e_existing_prediction["_id"]
        #    e_copy_predictions["prediction_id"] = e_existing_prediction["_id"]
        #elif e_property.get("prediction_id"):
        #    #e_copy_predictions["_id"] = e_property["prediction_id"]
        #    e_copy_predictions["prediction_id"] = e_property["prediction_id"]
        if not o_pred_id and e_property.get("prediction_id"):
            o_pred_id = e_property["prediction_id"]
        
        if not o_pred_id: o_pred_id = ObjectId()
        e_copy_predictions["prediction_id"] = o_pred_id
        
        if e_property.get("zpid"): e_processed["zpid"] = int(e_property["zpid"])
        if e_property.get("realtor_id"): e_processed["realtor_id"] = int(e_property["realtor_id"])
        #if e_property.get("zpid"): e_processed["zpid"] = int(e_property["zpid"])
        
        if "per_month_costs" in e_processed:
            e_processed["per_month_costs"] = {str(k): v for k, v in e_processed["per_month_costs"].items()}
        e_processed["created"] = datetime.utcnow()
        
        if e_existing_prediction and e_existing_prediction.get("market_ids"):
            e_market_ids = e_existing_prediction["market_ids"]
            e_processed["market_ids"] = e_market_ids
        else:
            e_market_ids = find_market_ids(e_property)
            e_processed["market_ids"] = e_market_ids
        #print("e_market_ids", e_market_ids)
        
        if e_processed.get("predictions"):
            s_ptype = e_processed["inputs"]["property_type"] if e_processed.get("inputs", {}).get("property_type") else get_input_property_type_name(e_processed.get("inputs"))
            
            e_copy_predictions.update({
                "revr_avg_daily_rate": e_processed["predictions"]["avg_rate_at_predicted_revenue"],
                "revr_optimized_revenue": e_processed["predictions"]["predicted_revenue_optimized"],
                "revr_optimized_cap": e_processed["financing"]["predicted_revenue_optimized"]["gross_cap_rate"],
                "revr_long_term_rental_revenue": e_processed["predictions"]["predicted_rent_annual"],
                "revr_long_term_cap": e_processed["financing"]["predicted_rent"]["gross_cap_rate"],
                "revr_score_overall": e_processed.get("scoring", {}).get("score_overall"),
                "revr_score_absolute": e_processed.get("scoring", {}).get("score_absolute"),
                "revr_score_relative": e_processed.get("scoring", {}).get("score_relative"),
                "revr_score_area": e_processed.get("scoring", {}).get("score_area"),
                "revr_ltr_score_overall": e_processed.get("ltr_scoring", {}).get("ltr_score_overall"),
                "revr_ltr_score_absolute": e_processed.get("ltr_scoring", {}).get("ltr_score_absolute"),
                "revr_ltr_score_relative": e_processed.get("ltr_scoring", {}).get("ltr_score_relative"),
                "revr_ltr_score_family": e_processed.get("ltr_scoring", {}).get("ltr_score_family"),
                "revr_ltr_score_family_area": e_processed.get("ltr_scoring", {}).get("ltr_score_family_area"),
                "revr_optimized_occupancy": e_processed["predictions"]["predicted_occupancy_optimized"],
                "revr_status": 200,
                "revr_date": datetime.utcnow(),
                "revr_property_type": s_ptype,
                "added_date": dt_added,
            })
            if e_market_ids:
                e_copy_predictions["market_ids"] = e_market_ids
            if e_prior_listing:
                for k in ["sources", "zpid", "realtor_id", "zillow", "realtor"]:
                    if k in e_prior_listing: e_copy_predictions[k] = e_prior_listing[k]
            if e_existing_prediction:
                for k in ["zpid", "realtor_id"]:
                    if k in e_existing_prediction: e_copy_predictions[k] = e_existing_prediction[k]
            
            if "sources" not in e_copy_predictions:
                e_copy_predictions["sources"] = {}
            if kwargs.get("realtor") or e_copy_predictions.get("realtor_id"):
                e_copy_predictions["sources"]["realtor"] = True
            elif not kwargs.get("realtor") or e_copy_predictions.get("zpid"):
                e_copy_predictions["sources"]["zillow"] = True
            
            if not kwargs.get("skip_upload"):
                if kwargs.get("rentals"):
                    o_coll_properties = self._coll_rentals
                    o_coll_failures = self._coll_failures
                    if kwargs.get("requested"):
                        o_coll_scraped = self._coll_scraped_rent_requested
                    elif kwargs.get("realtor"):
                        o_coll_scraped = self._coll_scraped_realtor
                        o_coll_failures = self._coll_failures_realtor
                    elif kwargs.get("national"):
                        o_coll_scraped = self._coll_scraped_rent
                    else:
                        o_coll_scraped = self._coll_scraped_rent
                else:
                    o_coll_properties = self._coll_properties
                    if kwargs.get("requested"):
                        o_coll_scraped = self._coll_scraped_requested
                    elif kwargs.get("realtor"):
                        o_coll_scraped = self._coll_scraped_realtor
                        o_coll_failures = self._coll_failures_realtor
                    elif kwargs.get("national"):
                        o_coll_scraped = self._coll_scraped_national
                    else:
                        o_coll_scraped = self._coll_scraped
                
                if kwargs.get("reapply"):
                    #if "_id" in e_copy_predictions: del e_copy_predictions["_id"]
                    o_coll_properties.update_one(
                        {"_id": e_property["_id"]},
                        utils.pymongo_dict_prep(e_copy_predictions, raw_dates=True),
                    )
                else:
                    #o_coll_scraped.update_one(
                    #    {"_id": e_property["_id"]},
                    #    {"$set": utils.pymongo_dict_prep(e_copy_predictions, raw_dates=True)},
                    #)
                    
                    if e_property.get("_id") and kwargs.get("move_from_scraped", True):
                        o_coll_scraped.delete_one({"_id": e_property["_id"]})
                    
                    e_std.update(e_copy_predictions)
                    if e_prior_listing and e_prior_listing.get("_id"):
                        o_coll_properties.update_one(
                            {"_id": e_prior_listing["_id"]},
                            {"$set": utils.pymongo_dict_prep(e_std, raw_dates=True)},
                            upsert=True,
                        )

                    else:
                        o_coll_properties.update_one(
                            {"_id": o_pred_id},
                            {"$set": utils.pymongo_dict_prep(e_std, raw_dates=True)},
                            upsert=True,
                        )
                
                self._coll_predictions.update_one(
                    #{"zpid": e_processed["zpid"]},
                    {"_id": o_pred_id},
                    {"$set": utils.pymongo_dict_prep(e_processed, raw_dates=True)},
                    upsert=True,
                )
            
        else:
            if e_processed.get("status") == 200:
                e_processed["status"] = 400
                e_copy_predictions["status"] = 400
            
        e_processed["_id"] = o_pred_id
        return e_processed, e_copy_predictions
    
    def standardize_scraped(self, e_raw, **kwargs):
        e_std = e_raw.copy()
        if "_id" in e_std:
            del e_std["_id"]
        
        #if not kwargs.get("national"):
        #    e_std["requested"] = datetime.utcnow()
            
        if e_std.get("scraping_date") and isinstance(e_std["scraping_date"], str):
            e_std["scraping_date"] = dateparse(e_std["scraping_date"])
        
        if not pd.isnull(e_std.get("date-sold_unix")) and isinstance(e_std["date-sold_unix"], (int, float)):
            e_std["date_sold"] = datetime.fromtimestamp(e_std["date-sold_unix"] / 1000)
        elif not pd.isnull(e_std.get("date_sold")) and isinstance(e_std["date_sold"], str):
            e_std["date_sold"] = dateparse(e_std["date_sold"])
        
        a_images = []
        e_std.get("images", "")
        if e_std.get("images", "") and isinstance(e_std["images"], str):
            a_images = e_std["images"].split(",")
        e_std["image_urls"] = a_images
        
        if not e_std.get("location") and e_std.get("latitude") and e_std.get("longitude"):
            d_latitude = float(e_std["latitude"])
            d_longitude = float(e_std["longitude"])
            if (-90 <= d_latitude <= 90) or (-180 <= d_longitude <= 180):
                e_std["gps"] = {
                  "type": "Point",
                  "coordinates": [d_longitude, d_latitude]
                }
        return e_std
    
    def process_batch(self, **kwargs):
        dt_prediction_expiration = datetime.utcnow() - timedelta(days=i_PREDICTION_LIFESPAN)
        self._load()
        
        if kwargs.get("rentals"):
            o_coll_properties = self._coll_rentals
            o_coll_failures = self._coll_failures
            if kwargs.get("requested"):
                o_coll_scraped = self._coll_scraped_rent_requested
            elif kwargs.get("realtor"):
                o_coll_scraped = self._coll_scraped_realtor
                o_coll_failures = self._coll_failures_realtor
            elif kwargs.get("national"):
                o_coll_scraped = self._coll_scraped_rent
            else:
                o_coll_scraped = self._coll_scraped_rent
        else:
            o_coll_properties = self._coll_properties
            o_coll_failures = self._coll_failures
            if kwargs.get("requested"):
                o_coll_scraped = self._coll_scraped_requested
            elif kwargs.get("realtor"):
                o_coll_scraped = self._coll_scraped_realtor
                o_coll_failures = self._coll_failures_realtor
            elif kwargs.get("national"):
                o_coll_scraped = self._coll_scraped_national
            else:
                o_coll_scraped = self._coll_scraped
        
        if kwargs.get("a_batch"):
            a_batch = kwargs["a_batch"]
        else:
            a_batch = list(self.get_scraped_zillow_batch(**kwargs))
        if not a_batch:
            print("No new scraped zillow properties to process")
            return False
        print(f"{len(a_batch):,} in batch")
        a_bulk_scraped_updates, a_bulk_properties_updates = [], []
        a_bulk_predictions_updates, a_bulk_failures_updates = [], []
        
        # mark the scraped records as having their processing begun
        a_batch_ids = [e_prop["_id"] for e_prop in a_batch]
        a_batch_zpids = [e_prop["zpid"] for e_prop in a_batch if e_prop.get("zpid")]
        if kwargs.get("realtor"):
            a_batch_realtor_ids = [e_prop["realtor_id"] for e_prop in a_batch if e_prop.get("realtor_id")]
            #a_batch_realtor_ids.extend([e_prop["property_id"] for e_prop in a_batch if e_prop.get("property_id")])
        else:
            a_batch_realtor_ids = []
        
        #print("marking scraped files as in process")
        n_in_process_start = time()
        o_coll_scraped.update_many(
            {"_id": {"$in": a_batch_ids}},
            {"$set": {"revr_status": 0, "revr_date": datetime.utcnow()}})
        #print(f"marking scraped files as in process took: {utils.secondsToStr(time() - n_in_process_start)}")
        
        # look for properties that already have predictions made
        a_or_ids = [{"_id": {"$in": a_batch_ids}}]
        if a_batch_zpids: a_or_ids.append({"zpid": {"$in": a_batch_zpids}})
        if a_batch_realtor_ids: a_or_ids.append({"realtor_id": {"$in": a_batch_realtor_ids}})
        #if a_batch_realtor_ids and kwargs.get("realtor"): a_or_ids.append({"property_id": {"$in": a_batch_realtor_ids}})
        
        print("finding any existing listings for batch")
        n_batch_prior_start = time()
        z_prior_listings = o_coll_properties.find(
            {"$or": a_or_ids,},
            projection=a_EXISTING_LISTING_PROJ
        )
        
        e_existing_listings = {e_prop["_id"]: e_prop for e_prop in z_prior_listings}
        e_existing_zpid_listings = {e_prop["zpid"]: e_prop for e_prop in e_existing_listings.values() if e_prop.get("zpid")}
        e_existing_realtor_listings = {e_prop["realtor_id"]: e_prop for e_prop in e_existing_listings.values() if e_prop.get("realtor_id")}
        print(f"\n{len(e_existing_listings):,} properties listed previously, {len(e_existing_zpid_listings):,} on zillow and {len(e_existing_realtor_listings):,} on realtor")
        print(f"finding batch prior listings took: {utils.secondsToStr(time() - n_batch_prior_start)}")
        
        print("finding any existing predictions for batch")
        n_batch_prior_start = time()
        z_existing = self._coll_predictions.find(
            {"$or": a_or_ids,},
            projection=a_EXISTING_PRED_PROJ
        )
        e_existing_predictions = {e_pred["_id"]: e_pred for e_pred in z_existing}
        e_existing_zpid_predictions = {e_pred["zpid"]: e_pred for e_pred in e_existing_predictions.values() if e_pred.get("zpid")}
        e_existing_realtor_predictions = {e_pred["realtor_id"]: e_pred for e_pred in e_existing_predictions.values() if e_pred.get("realtor_id")}
        print(f"finding batch existing predictions took: {utils.secondsToStr(time() - n_batch_prior_start)}")
        
        e_recent_predictions = {
            o_id: e_pred for o_id, e_pred in e_existing_predictions.items()
            if e_pred.get("revr_status") == 200
                and isinstance(e_pred.get("created"), datetime) and e_pred["created"] > dt_prediction_expiration
                and (e_pred.get("predictions", {}).get("pred_vs_price_optimized", 0) <= 0.5 and e_pred["created"] < datetime.utcnow() - timedelta(days=i_PREDICTION_LIFESPAN))
        }
        print(f"\n{len(e_existing_predictions):,} properties already predicted, {len(e_recent_predictions):,} recently")
        
        print(f"\n{len(a_batch) - len(e_recent_predictions):,} to predict")
        
        for i_prop, e_prop in enumerate(a_batch):
            e_pred_exist, e_pred_rec, e_prior_listing = None, None, None
            
            i_status = 400
            
            if kwargs.get("realtor"):
                e_prop = realtor_standardization.transform_realtor_to_zillow(e_prop)
            
            e_std = self.standardize_scraped(e_prop, **kwargs)
            
            if kwargs.get("realtor") and e_std.get("property_id") and not e_std.get("realtor_id"):
                e_std["realtor_id"] = e_std["property_id"]
            
            if e_prop.get("_id") and e_prop["_id"] in e_existing_predictions: e_pred_exist = e_existing_predictions[e_prop["zpid"]]
            if not e_pred_exist and e_prop.get("zpid") and e_prop["zpid"] in e_existing_zpid_predictions: e_pred_exist = e_existing_zpid_predictions[e_prop["zpid"]]
            if not e_pred_exist and e_prop.get("realtor_id") and e_prop["realtor_id"] in e_existing_realtor_predictions: e_pred_exist = e_existing_realtor_predictions[e_prop["realtor_id"]]
            #if not e_pred_exist and e_prop.get("property_id") and e_prop["property_id"] in e_existing_realtor_predictions: e_pred_exist = e_existing_realtor_predictions[e_prop["property_id"]]
            
            if e_prop.get("_id") and e_prop["_id"] in e_recent_predictions: e_pred_rec = e_recent_predictions[e_prop["_id"]]
            if not e_pred_rec and e_prop.get("zpid") and e_prop["zpid"] in e_recent_predictions: e_pred_rec = e_recent_predictions[e_prop["zpid"]]
            if not e_pred_rec and e_prop.get("realtor_id") and e_prop["realtor_id"] in e_recent_predictions: e_pred_rec = e_recent_predictions[e_prop["realtor_id"]]
            #if not e_pred_rec and e_prop.get("property_id") and e_prop["property_id"] in e_recent_predictions: e_pred_rec = e_recent_predictions[e_prop["property_id"]]
            
            
            if e_prop["_id"] in e_existing_listings:
                e_prior_listing = e_existing_listings[e_prop["_id"]]
            elif e_prop.get("zpid") and e_prop["zpid"] in e_existing_zpid_listings:
                e_prior_listing = e_existing_zpid_listings[e_prop["zpid"]]
            elif e_prop.get("realtor_id") and e_prop["realtor_id"] in e_existing_realtor_listings:
                e_prior_listing = e_existing_realtor_listings[e_prop["realtor_id"]]
            elif e_prop.get("property_id") and e_prop["property_id"] in e_existing_realtor_listings:
                e_prior_listing = e_existing_realtor_listings[e_prop["property_id"]]
            
            print(f"({i_prop + 1}/{len(a_batch)})", e_prop.get("_id"), e_prop.get("zpid"), e_prop.get("realtor_id"), bool(e_pred_exist), bool(e_pred_rec), bool(e_prior_listing))
            
            if e_prior_listing and e_prop.get("added_date"):
                dt_added = e_prior_listing["added_date"]
            elif e_prop.get("added_date"):
                dt_added = e_prop["added_date"]
            elif e_prop.get("scraping_date"):
                dt_added = e_prop["scraping_date"]
            else:
                dt_added = datetime.utcnow()
            if isinstance(dt_added, str): dt_added = dateparse(dt_added)
            
            if e_prior_listing:
                for k in ["sources", "zillow", "realtor", "zpid", "realtor_id", "property_id"]:
                    if k in e_prior_listing:
                        e_std[k] = e_prior_listing[k]
            if "sources" not in e_std:
                e_std["sources"] = {}
            if kwargs.get("realtor"):
                e_std["sources"]["realtor"] = True
            else:
                e_std["sources"]["zillow"] = True
            
            if e_pred_rec: # recent prediction, re-copy over
                e_prediction = e_pred_rec
                s_ptype = e_prediction["inputs"]["property_type"] if e_prediction.get("inputs", {}).get("property_type") else get_input_property_type_name(e_prediction.get("inputs"))
                e_std.update({
                    "revr_avg_daily_rate": e_prediction.get("predictions", {}).get("avg_rate_at_predicted_revenue"),
                    "revr_optimized_revenue": e_prediction.get("predictions", {}).get("predicted_revenue_optimized"),
                    "revr_optimized_cap": e_prediction.get("financing", {}).get("predicted_revenue_optimized", {}).get("gross_cap_rate"),
                    "revr_long_term_rental_revenue": e_prediction.get("predictions", {}).get("predicted_rent_annual"),
                    "revr_long_term_cap": e_prediction.get("financing", {}).get("predicted_rent", {}).get("gross_cap_rate"),
                    "revr_score_overall": e_prediction.get("scoring", {}).get("score_overall"),
                    "revr_score_absolute": e_prediction.get("scoring", {}).get("score_absolute"),
                    "revr_score_relative": e_prediction.get("scoring", {}).get("score_relative"),
                    "revr_score_area": e_prediction.get("scoring", {}).get("score_area"),
                    "revr_ltr_score_overall": e_prediction.get("ltr_scoring", {}).get("score_overall"),
                    "revr_ltr_score_absolute": e_prediction.get("ltr_scoring", {}).get("ltr_score_absolute"),
                    "revr_ltr_score_relative": e_prediction.get("ltr_scoring", {}).get("ltr_score_relative"),
                    "revr_ltr_score_family": e_prediction.get("ltr_scoring", {}).get("ltr_score_family"),
                    "revr_ltr_score_family_area": e_prediction.get("ltr_scoring", {}).get("ltr_score_family_area"),
                    "revr_optimized_occupancy": e_prediction.get("predictions", {}).get("predicted_occupancy_optimized"),
                    "revr_status": e_prediction.get("status", -1),
                    "revr_date": e_prediction.get("created", datetime.utcnow()),
                    "revr_property_type": s_ptype,
                    "prediction_id": e_pred_rec["_id"],
                    "added_date": dt_added,
                })
                
                e_cleaned_std= utils.pymongo_dict_prep(e_std, raw_dates=True)
                if e_prediction.get("scoring", {}).get("score_overall", 0) > 0: 
                    if e_prior_listing:
                        a_bulk_properties_updates.append(
                            UpdateOne({"_id": e_prior_listing["_id"]}, {"$set": e_cleaned_std}, upsert=True,))
                    else:
                        a_bulk_properties_updates.append(
                            UpdateOne({"_id": e_pred_rec["_id"]}, {"$set": e_cleaned_std}, upsert=True,))
                else:
                    a_bulk_failures_updates.append(
                        UpdateOne({"_id": e_pred_rec["_id"]}, {"$set": e_cleaned_std}, upsert=True,))
                
                if not kwargs.get("reapply") and e_prop.get("_id"):
                    a_bulk_scraped_updates.append(DeleteOne({"_id": e_prop["_id"]}))
                
                continue
            
            try:
                sleep(d_DELAY_BETWEEN_PREDICTIONS)
                e_address_dict = None
                if e_pred_exist:
                    e_prediction = e_pred_exist
                    e_address_dict = e_pred_exist.get("address")
                
                e_processed, e_copy_predictions = self.predict_property(
                    e_prop,
                    skip_upload=True,
                    skip_geocode=True,
                    address_dict=e_address_dict,
                    find_existing_by_id=False, #id check was already done so don't repeat
                    e_existing_prediction=e_pred_exist,
                    e_prior_listing=e_prior_listing,
                    **kwargs)
                i_status = e_processed.get("status", 400)
                e_std.update(e_copy_predictions)
                
                if not e_processed.get("address", {}).get("gps") and isinstance(e_processed["address"].get("latitude"), (int, float)) and isinstance(e_processed["address"].get("longitude"), (int, float)):
                    d_latitude = float(e_processed["address"]["latitude"])
                    d_longitude = float(e_processed["address"]["longitude"])
                    if (-90.0 <= d_latitude <= 90.0) and (-180.0 <= d_longitude <= 180.0):
                        e_processed["address"]["gps"] = {"type": "Point", "coordinates": [d_longitude, d_latitude]}
                
                #e_existing_predictions[e_prop["_id"]] = utils.pymongo_dict_prep(e_processed, raw_dates=True)
                e_cleaned_processed = utils.pymongo_dict_prep(e_processed, raw_dates=True)
                if e_std.get("zpid"): e_existing_zpid_predictions[e_std["zpid"]] = e_cleaned_processed
                if e_std.get("realtor_id"): e_existing_realtor_predictions[e_std["realtor_id"]] = e_cleaned_processed
                if e_std.get("property_id"): e_existing_realtor_predictions[e_std["property_id"]] = e_cleaned_processed
                
                a_bulk_predictions_updates.append(UpdateOne({"_id": e_processed["_id"]}, {"$set": e_cleaned_processed}, upsert=True))
                
                e_cleaned_std = utils.pymongo_dict_prep(e_std, raw_dates=True)
                if e_copy_predictions.get("revr_score_overall", 0) > 0: 
                    if e_prior_listing:
                        a_bulk_properties_updates.append(UpdateOne({"_id": e_prior_listing["_id"]}, {"$set": e_cleaned_std}, upsert=True))
                    else:
                        a_bulk_properties_updates.append(UpdateOne({"_id": e_processed["_id"]}, {"$set": e_cleaned_std}, upsert=True))
                else:
                
                    if e_processed.get("_id"):
                        a_bulk_failures_updates.append(UpdateOne({"_id": e_processed["_id"]}, {"$set": e_cleaned_std}, upsert=True))
                    elif e_prior_listing.get("_id"):
                        a_bulk_failures_updates.append(UpdateOne({"_id": e_prior_listing["_id"]}, {"$set": e_cleaned_std}, upsert=True))
                    elif  e_prop.get("zpid"):
                        a_bulk_failures_updates.append(UpdateOne({"zpid": e_prop["zpid"]}, {"$set": e_cleaned_std}, upsert=True))
                    elif  e_prop.get("realtor_id"):
                        a_bulk_failures_updates.append(UpdateOne({"realtor_id": e_prop["realtor_id"]}, {"$set": e_cleaned_std}, upsert=True))
                
                if not kwargs.get("reapply") and e_prop.get("_id") and kwargs.get("move_from_scraped", True):
                    a_bulk_scraped_updates.append(DeleteOne({"_id": e_prop["_id"]}))
                
                e_processed, e_copy_predictions = {}, {}
            
            except Exception as e:
                s_trace = ''.join(traceback.format_exception(None, e, e.__traceback__))
                print(f"failure on property {e_prop['_id']}", e)
                print(s_trace)
                if kwargs.get("reapply"):
                    a_bulk_properties_updates.append(
                        UpdateOne(
                            {"_id": e_prop["_id"]},
                            {"$set": {
                                "revr_status": i_status,
                                "revr_date": datetime.utcnow(),
                            }}
                        ))
                else:
                    a_bulk_scraped_updates.append(
                        UpdateOne(
                            {"_id": e_prop["_id"]},
                            {"$set": {
                                "revr_status": i_status,
                                "revr_date": datetime.utcnow(),
                            }}
                        ))
                    
        try:
            if a_bulk_predictions_updates:
                print(f"{len(a_bulk_predictions_updates):,} updates to predictions collection")
                self._coll_predictions.bulk_write(a_bulk_predictions_updates)
            if a_bulk_properties_updates:
                print(f"{len(a_bulk_properties_updates):,} updates to properties collection")
                o_coll_properties.bulk_write(a_bulk_properties_updates)
            if a_bulk_failures_updates:
                print(f"{len(a_bulk_failures_updates):,} updates to failures collection")
                o_coll_failures.bulk_write(a_bulk_failures_updates)
            if a_bulk_scraped_updates:
                print(f"{len(a_bulk_scraped_updates):,} updates to scraped collection")
                o_coll_scraped.bulk_write(a_bulk_scraped_updates)
            
        except Exception as e:
            s_trace = ''.join(traceback.format_exception(None, e, e.__traceback__))
            print(f"failure during batch upload", e)
            print(s_trace)
            
        #self._close()
        
        e_existing_predictions = {}
        a_bulk_scraped_updates = []
        a_bulk_properties_updates = []
        a_bulk_predictions_updates = []
        a_bulk_failures_updates = []
        a_batch_ids = []
        
        #print("e_LOCATION_STDS", len(single_property_predictor.e_LOCATION_STDS))
        #print("e_POTENTIAL_LOCATION_STDS", len(single_property_predictor.e_POTENTIAL_LOCATION_STDS))
        #print("e_NATIONAL_INFO", len(area_data.e_NATIONAL_INFO))
        #print("e_STATE_INFO_CACHE", len(area_data.e_STATE_INFO_CACHE))
        #print("e_CITY_INFO_CACHE", len(area_data.e_CITY_INFO_CACHE))
        #print("e_ZIPCODE_INFO_CACHE", len(area_data.e_ZIPCODE_INFO_CACHE))
        #print("e_MARKET_GPS", len(area_data.e_MARKET_GPS))
        #print("e_MARKET_INFO_ROWS", len(area_data.e_MARKET_INFO_ROWS))
        
        
        return True
    
    def reapply_batch(self, **kwargs):
        dt_prediction_expiration = datetime.utcnow() - timedelta(days=kwargs.get("prediction_lifespan", i_PREDICTION_LIFESPAN))
        
        self._load()
        
        a_batch = list(self.get_properties_batch(**kwargs))
        if not a_batch:
            print("No zillow properties that need their score reapplied")
            return False
        
        a_bulk_properties_updates = []
        a_bulk_predictions_updates = []
        a_bulk_failures_updates = []
    
        if kwargs.get("rentals"):
            o_coll_properties = self._coll_rentals
        else:
            o_coll_properties = self._coll_properties
        
        # mark the property records as having their processing begun
        a_batch_ids = [e_zillow["zpid"] for e_zillow in a_batch if e_zillow.get("zpid")]
        print("marking properties as in process")
        o_coll_properties.update_many(
            {"zpid": {"$in": a_batch_ids}},
            {"$set": {"revr_status": 0, "revr_date": datetime.utcnow()}})
        
        # look for properties that already have predictions made
        print("finding any recent existing predictions for batch")
        if kwargs.get("repredict_all"):
            e_existing_predictions, e_recent_predictions = {}, {}
        else:
            e_existing_predictions = {
                e_prediction["zpid"]: e_prediction
                for e_prediction in self._coll_predictions.find(
                    {
                        #"created": {"$gte": datetime.utcnow() - timedelta(days=i_PREDICTION_LIFESPAN)},
                        "zpid": {"$in": a_batch_ids},
                    },
                    projection=a_EXISTING_PRED_PROJ
                )
            }
            e_recent_predictions = {
                zpid: e_pred
                for zpid, e_pred in e_existing_predictions.items()
                if isinstance(e_pred.get("created"), datetime)
                    and e_pred["created"] > dt_prediction_expiration
                    and (e_pred.get("predictions", {}).get("pred_vs_price_optimized", 0) <= 0.5 and e_pred["created"] < datetime.utcnow() - timedelta(days=1))
            }
            print(f"\n{len(e_existing_predictions):,} properties already predicted, {len(e_recent_predictions):,} recently")
    
        print(f"\n{len(a_batch) - len(e_recent_predictions):,} to predict")
    
        for e_prop in a_batch:
            i_status = 400
    
            if e_prop.get("zpid") in e_recent_predictions:
                e_prediction = e_recent_predictions[e_prop["zpid"]]
                
                s_ptype = e_prediction["inputs"]["property_type"] if e_prediction.get("inputs", {}).get("property_type") else get_input_property_type_name(e_prediction.get("inputs"))
                
                e_copy_predictions = {
                    "revr_avg_daily_rate": e_prediction.get("predictions", {}).get("avg_rate_at_predicted_revenue"),
                    "revr_optimized_revenue": e_prediction.get("predictions", {}).get("predicted_revenue_optimized"),
                    "revr_optimized_cap": e_prediction.get("financing", {}).get("predicted_revenue_optimized", {}).get("gross_cap_rate"),
                    "revr_long_term_rental_revenue": e_prediction.get("predictions", {}).get("predicted_rent_annual"),
                    "revr_long_term_cap": e_prediction.get("financing", {}).get("predicted_rent", {}).get("gross_cap_rate"),
                    "revr_score_overall": e_prediction.get("scoring", {}).get("score_overall"),
                    "revr_score_absolute": e_prediction.get("scoring", {}).get("score_absolute"),
                    "revr_score_relative": e_prediction.get("scoring", {}).get("score_relative"),
                    "revr_score_area": e_prediction.get("scoring", {}).get("score_area"),
                    "revr_ltr_score_overall": e_prediction.get("ltr_scoring", {}).get("ltr_score_overall"),
                    "revr_ltr_score_absolute": e_prediction.get("ltr_scoring", {}).get("ltr_score_absolute"),
                    "revr_ltr_score_relative": e_prediction.get("ltr_scoring", {}).get("ltr_score_relative"),
                    "revr_ltr_score_family": e_prediction.get("ltr_scoring", {}).get("ltr_score_family"),
                    "revr_ltr_score_family_area": e_prediction.get("ltr_scoring", {}).get("ltr_score_family_area"),
                    "revr_optimized_occupancy": e_prediction.get("predictions", {}).get("predicted_occupancy_optimized"),
                    "revr_status": e_prediction.get("status", -1),
                    "revr_date": e_prediction.get("created", datetime.utcnow()),
                    "revr_property_type": s_ptype,
                }
                
                if e_copy_predictions.get("revr_score_overall", 0) > 0: 
                    a_bulk_properties_updates.append(
                        UpdateOne(
                            {"_id": e_prop["_id"]},
                            {"$set": utils.pymongo_dict_prep(e_copy_predictions, raw_dates=True)},
                        ))
                else:
                    a_bulk_failures_updates.append(
                        UpdateOne(
                            {"_id": e_prop["_id"]},
                            {"$set": utils.pymongo_dict_prep(e_prop, raw_dates=True)},
                            upsert=True,
                        ))
                    a_bulk_properties_updates.append(
                        DeleteOne({"_id": e_prop["_id"]}))
    
                continue
            
            try:
                sleep(d_DELAY_BETWEEN_PREDICTIONS)
                
                e_address_dict = None
                if e_prop.get("zpid") in e_existing_predictions:
                    e_prediction = e_existing_predictions[e_prop["zpid"]]
                    e_address_dict = e_prediction.get("address")
                
                e_processed, e_copy_predictions = self.predict_property(
                    e_prop,
                    skip_upload=True,
                    reapply=True,
                    #skip_geocode=True if kwargs.get("national") else False,
                    skip_geocode=True,
                    address_dict=e_address_dict,
                    e_existing_prediction=e_existing_prediction,
                    e_prior_listing=e_prop,
                    **kwargs)
                i_status = e_processed.get("status", 400)
    
                if not e_processed.get("address", {}).get("gps") and isinstance(e_processed["address"].get("latitude"), (int, float)) and isinstance(e_processed["address"].get("longitude"), (int, float)):
                    d_latitude = float(e_processed["address"]["latitude"])
                    d_longitude = float(e_processed["address"]["longitude"])
                    if (-90.0 <= d_latitude <= 90.0) and (-180.0 <= d_longitude <= 180.0):
                        e_processed["address"]["gps"] = {
                          "type": "Point",
                          "coordinates": [d_longitude, d_latitude]
                        }
                
                a_bulk_predictions_updates.append(
                    UpdateOne(
                        {"zpid": e_prop["zpid"]},
                        {"$set": utils.pymongo_dict_prep(e_processed, raw_dates=True)},
                        upsert=True,
                    ))
    
                if e_copy_predictions.get("revr_score_overall", 0) > 0:
                    a_bulk_properties_updates.append(
                        UpdateOne(
                            {"zpid": e_prop["zpid"]},
                            {"$set": utils.pymongo_dict_prep(e_copy_predictions, raw_dates=True)},
                            upsert=True,
                        ))
                else:
                    a_bulk_failures_updates.append(
                        UpdateOne(
                            {"zpid": e_prop["zpid"]},
                            {"$set": utils.pymongo_dict_prep(e_copy_predictions, raw_dates=True)},
                            upsert=True,
                        ))
                    a_bulk_properties_updates.append(
                        DeleteOne({"zpid": e_prop["zpid"]}))
                
                e_processed, e_copy_predictions = {}, {}
                    
            except Exception as e:
                s_trace = ''.join(traceback.format_exception(None, e, e.__traceback__))
                print(f"failure on _id {e_prop['_id']}", e)
                print(s_trace)
                a_bulk_properties_updates.append(
                    UpdateOne(
                        {"_id": e_prop["_id"]},
                        {"$set": {
                            "revr_status": i_status,
                            "revr_date": datetime.utcnow(),
                        }}
                    ))
    
        try:
            if a_bulk_predictions_updates:
                print(f"{len(a_bulk_predictions_updates):,} updates to predictions collection")
                self._coll_predictions.bulk_write(a_bulk_predictions_updates)
            if a_bulk_properties_updates:
                print(f"{len(a_bulk_properties_updates):,} updates to properties collection")
                o_coll_properties.bulk_write(a_bulk_properties_updates)
            if a_bulk_failures_updates:
                print(f"{len(a_bulk_failures_updates):,} updates to failures collection")
                self._coll_failures.bulk_write(a_bulk_failures_updates)
        
        except Exception as e:
            s_trace = ''.join(traceback.format_exception(None, e, e.__traceback__))
            print(f"failure during batch upload", e)
            print(s_trace)
    
        #self._close()
    
        return True
    
    ##### Requested Areas #####
    def check_requested_scraped(self, **kwargs):
        i_retry_delay = kwargs.get("i_retry_delay", i_RETRY_DELAY)
        a_scraped_areas = list(self._coll_scraped_requested.aggregate([
            {
                "$match": {
                    "$or": [
                        {"revr_date": None},
                        {"revr_date": {"$lte": datetime.utcnow() - timedelta(minutes=i_retry_delay)}},
                    ]
                }
            },
            {
                "$sort": {
                    "scraping_date": 1,
                }
            },
            {
                "$project": {
                    "state": 1,
                    "city": 1,
                    "county": 1,
                    "market_id": 1,
                    "scraping_date": {"$dateFromString": {"dateString": "$scraping_date"}},
                    "scraping_results": 1,
                }
            },
            {
                "$group": {
                    "_id": ["$market_id"],
                    "count": {"$sum": 1 },
                    "scraping_date": {"$max": "$scraping_date"},
                    "scraping_results": {"$last": "$scraping_results"},
                    "state": {"$addToSet": "$state"},
                    "county": {"$addToSet": "$county"},
                    "city": {"$addToSet": "$city"},
                    
                }
            },
        ]))
        return a_scraped_areas
    
    def check_requested_processed(self, td_recent=None, **kwargs):
        if not td_recent: td_recent = td_RECENT
        
        if kwargs.get("rentals"):
            o_coll_properties = self._coll_rentals
        else:
            o_coll_properties = self._coll_properties
        
        a_predicted_areas = list(o_coll_properties.aggregate([
            {
                "$match": {
                    "market_id": {"$gt": 0},
                    "scraping_date": {"$gt": datetime.utcnow() - td_recent},
                }
            },
            {
                "$sort": {
                    "scraping_date": 1,
                }
            },
            {
                "$project": {
                    "state": 1,
                    "city": 1,
                    "county": 1,
                    "market_id": 1,
                    "scraping_date": 1,
                    "scraping_results": 1,
                }
            },
            {
                "$group": {
                    "_id": ["$market_id"],
                    #"_id": ["$state"],
                    "count": {"$sum": 1 },
                    "scraping_date": {"$max": "$scraping_date"},
                    "scraping_results": {"$last": "$scraping_results"},
                    "state": {"$addToSet": "$state"},
                    "county": {"$addToSet": "$county"},
                    "city": {"$addToSet": "$city"},
                    
                }
            },
            
        ]))
        return a_predicted_areas
    
    def check_requested_failed(self, td_recent=None, **kwargs):
        if not td_recent: td_recent = td_RECENT
        a_failed_areas = list(self._coll_failures.aggregate([
            {
                "$match": {
                    "market_id": {"$gt": 0},
                    "$or": [
                        {"scraping_date": {"$gt": datetime.utcnow() - td_recent}},
                        {"scraping_date": {"$gt": (datetime.utcnow() - td_recent).isoformat()}},
                    ],
                }
            },
            {
                "$sort": {
                    "scraping_date": 1,
                }
            },
            {
                "$project": {
                    "state": 1,
                    "city": 1,
                    "county": 1,
                    "market_id": 1,
                    "scraping_date": 1,
                    "scraping_results": 1,
                }
            },
            {
                "$group": {
                    "_id": ["$market_id"],
                    #"_id": ["$state"],
                    "count": {"$sum": 1 },
                    "scraping_date": {"$max": "$scraping_date"},
                    "scraping_results": {"$last": "$scraping_results"},
                    "state": {"$addToSet": "$state"},
                    "county": {"$addToSet": "$county"},
                    "city": {"$addToSet": "$city"},
                    
                }
            },
            
        ]))
        return a_failed_areas

##### End ScrapedPropertyPredictor #####



##### Requested markets batch and loops #####
def get_requested_market_summaries(**kwargs):
    
    o_DB = db_tables.get_connection()
    #df_markets = o_DB.get_table_as_df("market", s_schema="revr")
    df_requested_markets = o_DB.get_table_as_df("requested_markets", s_schema="revr")
    
    if kwargs.get("o_predictor"):
        o_predictor = kwargs["o_predictor"]
    else:
        o_predictor = ScrapedPropertyPredictor(schema="scraping", **kwargs)
    
    o_predictor._load()
    
    a_scraped_areas = o_predictor.check_requested_scraped(**kwargs)
    e_scraped_areas = {e_area["_id"][0]: e_area for e_area in a_scraped_areas}
    #print(len(e_scraped_areas))
    
    a_predicted_areas = o_predictor.check_requested_processed(**kwargs)
    e_predicted_areas = {e_area["_id"][0]: e_area for e_area in a_predicted_areas}
    #print(len(e_predicted_areas))
    
    a_failed_areas = o_predictor.check_requested_failed(**kwargs)
    e_failed_areas = {e_area["_id"][0]: e_area for e_area in a_failed_areas}
    
    #o_predictor._close()
    #print(len(e_failed_areas))
    
    return df_requested_markets, e_scraped_areas, e_predicted_areas, e_failed_areas


def check_requested_market_progress(td_recent=None, **kwargs):
    if not td_recent: td_recent = td_RECENT
#    df_requested_markets, e_scraped_areas, e_predicted_areas, e_failed_areas = get_requested_market_summaries(td_recent=timedelta(days=1), **kwargs)
    df_requested_markets, e_scraped_areas, e_predicted_areas, e_failed_areas = get_requested_market_summaries(**kwargs)
    
    a_markets = []
    o_DB = db_tables.get_connection()
    
    for idx, row in df_requested_markets.sort_values("last_request").iterrows():
        e_row = {k: v for k, v in dict(row).items() if not pd.isnull(v)}
        
        e_sa = e_scraped_areas.get(e_row["market_id"], {})
        e_pa = e_predicted_areas.get(e_row["market_id"], {})
        e_fa = e_failed_areas.get(e_row["market_id"], {})
        
        i_expected = e_row.get("expected_from_zillow", 0)
        
        i_results = e_sa.get("scraping_results", 0)
        if not i_results:
            i_results = e_pa.get("scraping_results", 0)
        if not i_results:
            i_results = e_pa.get("scraping_results", 0)
        
        i_to_process = e_sa.get("count", 0)
        i_processed = e_pa.get("count", 0)
        i_ignored = e_fa.get("count", 0)
        i_total = i_to_process + i_processed + i_ignored
        i_unscraped = i_results - i_total
        
        dt_last = None
        b_started_scraping, b_finished_scraping, b_broken_scraping = False, False, False
        b_started_processing, b_finished_processing, b_broken_processing = False, False, False
        
        b_existing_scrape = False
        if e_row.get("last_scraped_at"):
            dt_last = e_row.get("last_scraped_at")
            if dt_last >= datetime.utcnow() - timedelta(days=1, hours=0):
                b_existing_scrape = True
        else:
            dt_last = e_row.get("updated_at")
        
        if not i_expected:
            if i_to_process:
                b_started_scraping = True
                print("\tScraper failed to mark start in database", {"expected_from_zillow": i_results, "id": e_row["market_id"]})
                o_DB.update("revr.market", {"expected_from_zillow": i_results}, {"id": e_row["market_id"]})
        else:
            if i_total:
                b_started_scraping = True
            else:
                b_started_scraping = False
        dt_last_scraped = e_sa.get("scraping_date")
        if dt_last_scraped:
            dt_last = max(dt_last, dt_last_scraped)
        
        dt_last_processed = e_pa.get("scraping_date")
        if dt_last_processed:
            dt_last = max(dt_last, dt_last_processed)
    
        dt_last_failed = e_fa.get("scraping_date")
        if dt_last_failed:
            if isinstance(dt_last_failed, str): dt_last_failed = dateparse(dt_last_failed)
            dt_last = max(dt_last, dt_last_failed)
    
        if i_total >= i_expected * (1 - d_MARKET_MISSING_MARGIN):
            b_finished_scraping = True
            b_broken_scraping = False
        elif i_total > 0:
                
            if dt_last and dt_last < datetime.utcnow() - td_recent:
                if i_total < i_expected * (1 - d_MARKET_BROKEN_MARGIN):
                    b_broken_scraping = True
                else:
                    b_finished_scraping = True
            else:
                b_broken_scraping = False
            b_finished_scraping = False
        else:
            b_finished_scraping = False
            
            dt_last = e_row.get("updated_at")
            if dt_last and dt_last < datetime.utcnow() - timedelta(days=1, hours=0):
                b_broken_scraping = True
            else:
                b_broken_scraping = False
        
        if (i_processed + i_ignored) > 0:
            b_started_processing = True
        
        if b_finished_scraping and not b_broken_scraping:
            if (i_processed + i_ignored) > 0 and i_to_process == 0:
                b_finished_processing = True
            elif i_to_process > 0 and e_sa.get("scraping_date") and  e_sa["scraping_date"]  > datetime.utcnow() - timedelta(days=1, hours=0):
                b_broken_processing = True
            
        e_updates = {}
        if b_finished_processing:
            e_updates = {
                k: v
                for k, v in {
                    "expected_from_zillow": 0,
                    "processed": i_total,
                    "completed": i_processed,
                    "last_scraped_at": dt_last,
                }.items() if v != e_row.get(k)
            }
        elif b_broken_scraping:
            e_updates = {
                k: v
                for k, v in {
                    "completed": None,
                    "processed": i_total,
                    "last_scraped_at": dt_last,
                }.items() if v != e_row.get(k)
            }
        else:
            e_updates = {
                k: v
                for k, v in {
                    "processed": i_total,
                    "last_scraped_at": dt_last,
                }.items() if v != e_row.get(k)
            }
        
        if e_updates:
            o_DB.update(
                "revr.market",
                e_updates,
                {"id": e_row["market_id"]})
        
        a_markets.append({
            "market_id": e_row["market_id"],
            "last_request": e_row["last_request"],
            "existing_scrape": b_existing_scrape,
            "expected": i_expected,
            "results": i_results,
            "unscraped": i_unscraped,
            "to_process": i_to_process,
            "processed": i_processed,
            "ignored": i_ignored,
            "total": i_total,
            "started_scraping": b_started_scraping,
            "finished_scraping": b_finished_scraping,
            "broken_scraping": b_broken_scraping,
            "started_processing": b_started_processing,
            "finished_processing": b_finished_processing,
            "broken_processing": b_broken_processing,
            "last": dt_last,
            #"just_finished": b_just_finished,
        })
        print(
            f'market:{e_row["market_id"]} requested on {e_row["last_request"].strftime("%Y-%m-%d")}.'
            f'\n\t{i_results:,} expected, {i_to_process:,} unprocessed, {i_processed:,} processed, {i_ignored:,} ignored, {i_total:,} total.'
            f'\n\tcomplete: {b_finished_processing}, broken_scraping: {b_broken_scraping}'
            f'\n\tdt_last:{dt_last}, dt_last_scraped:{dt_last_scraped}, dt_last_processed:{dt_last_processed}, dt_last_failed:{dt_last_failed}'
            )
    df_market_status = pd.DataFrame(a_markets)
    return df_market_status

def process_market(market_id, **kwargs):
    assert market_id
    #print(f"Processing market {market_id}")
    i_batch_size = int(kwargs.get("i_batch_size", kwargs.get("batch_size", i_SCRAPE_BATCH)))
    
    if i_batch_size and not kwargs.get("i_batch_size"):
        kwargs["i_batch_size"] = i_batch_size
    
    i_max_batches = max(1, int(kwargs.get("max_batches", kwargs.get("max_predictions", 10000) / i_batch_size)))
    
    if kwargs.get("retry_delay") and not kwargs.get("i_retry_delay"):
        kwargs["i_retry_delay"] = int(kwargs["retry_delay"])
    
    if kwargs.get("o_predictor"):
        o_predictor = kwargs["o_predictor"]
    else:
        o_predictor = ScrapedPropertyPredictor(schema="scraping", **kwargs)
    
    n_start = time()
    o_predictor._load()
    try:
        i_count = o_predictor.count_scraped_zillow_batch(requested=True, market_id=market_id, **{k: v for k, v in kwargs.items() if k not in ["national", "requested", "retry",]})
        i_retry_count = o_predictor.count_scraped_zillow_batch(requested=True, retry=True, market_id=market_id, **{k: v for k, v in kwargs.items() if k not in ["national", "requested", "retry",]})
        #i_bad_count = o_predictor.count_bad_zillow_batch(requested=True, market_id=market_id, **{k: v for k, v in kwargs.items() if k not in ["national", "requested"]})
        #print(f"There are {i_count:,} scraped records to be processed  and {i_retry_count:,} retried for market {market_id} and {i_bad_count:,} to filter out, {i_count + i_retry_count + i_bad_count:,} in total.")
        print(f"There are {i_count:,} scraped records to be processed  and {i_retry_count:,} retried for market {market_id}, {i_count + i_retry_count:,} in total (ignoring bad).")

        n_start = time()
        b_more_to_process = True
        i = 0
        while b_more_to_process and i < i_max_batches:
            i += 1
            print(f"batch {i:,} ({(i - 1) * i_batch_size + 1:,} - {i * i_batch_size:,})")
            
            if i_count:
                b_more_to_process = o_predictor.process_batch(requested=True, market_id=market_id, **{k: v for k, v in kwargs.items() if k not in ["national", "requested", "retry"]})
            elif i_retry_count:
                b_more_to_process = o_predictor.process_batch(requested=True, market_id=market_id, retry=True, **{k: v for k, v in kwargs.items() if k not in ["national", "requested", "retry"]})
            
            print(f"\tpausing after batch {i:,}")
            sleep(d_DELAY_BETWEEN_BATCHES)

        if kwargs.get("move_bad", True):
            #o_predictor._load()
            i_bad_properties = o_predictor.count_bad_zillow_batch(requested=True, market_id=market_id, **{k: v for k, v in kwargs.items() if k not in ["national", "requested"]})
            print(f"There are {i_bad_properties:,} bad properties that should be moved out of the raw scraped collection into failures")
            o_predictor.move_bad_zillow_batch(requested=True, market_id=market_id, **{k: v for k, v in kwargs.items() if k not in ["national", "requested"]})
    except Exception as e:
        print(f"Failed to process market {market_id}", e)
    
    #o_predictor._close()
    print("finished processing", utils.secondsToStr(time() - n_start))
    

##### General batch and loops #####
def process_scraped(**kwargs):
    load_config
    
    i_batch_size = int(kwargs.get("i_batch_size", kwargs.get("batch_size", i_SCRAPE_BATCH)))
    if i_batch_size and not kwargs.get("i_batch_size"):
        kwargs["i_batch_size"] = i_batch_size
    
    i_max_batches = max(1, int(kwargs.get("max_batches", kwargs.get("max_predictions", 10000) / i_batch_size)))
    
    if kwargs.get("retry_delay") and not kwargs.get("i_retry_delay"):
        kwargs["i_retry_delay"] = int(kwargs["retry_delay"])
    
    o_predictor = ScrapedPropertyPredictor(schema="scraping", **kwargs)
    
    o_predictor._load()
    i_count = o_predictor.count_scraped_zillow_batch(**kwargs)
    if kwargs.get("requested"):
        a_scraped_areas = o_predictor.check_requested_scraped(**kwargs)
        e_scraped_areas = {e_area["_id"][0]: e_area for e_area in a_scraped_areas}
        print(f"There are {i_count:,} scraped records in {len(a_scraped_areas):,} requested markets to be processed")
        
    elif kwargs.get("national"):
        print(f"There are {i_count:,} national scraped records to be processed")
    else:
        print(f"There are {i_count:,} scraped records to be processed")
    
    n_start = time()
    
    if kwargs.get("requested"):
        #a_market_ids = list(e_scraped_areas.keys())
        #random.shuffle(a_market_ids)
        c_tried_markets = set()
        while e_scraped_areas and len(c_tried_markets) < len(e_scraped_areas):
            i_market_id = random.choice(list(e_scraped_areas.keys()))
            while i_market_id in c_tried_markets:
                i_market_id = random.choice(list(e_scraped_areas.keys()))
            
            process_market(market_id=i_market_id, o_predictor=o_predictor, **kwargs)
            sleep(d_DELAY_BETWEEN_BATCHES)
            c_tried_markets.add(i_market_id)
            
            o_predictor._load(reload_client=True, reload_tunnel=True)
            i_count = o_predictor.count_scraped_zillow_batch(**kwargs)
            a_scraped_areas = o_predictor.check_requested_scraped(**kwargs)
            e_scraped_areas = {e_area["_id"][0]: e_area for e_area in a_scraped_areas}
            print(f"There are {i_count:,} scraped records in {len(a_scraped_areas):,} requested markets left to be processed")
        
        check_requested_market_progress(o_predictor=o_predictor, **kwargs)
    
    else:
        b_more_to_process = True
        i = 0
        while b_more_to_process and i < i_max_batches:
            i += 1
            print(f"batch {i:,} ({(i - 1) * i_batch_size + 1:,} - {i * i_batch_size:,})")
            b_more_to_process = o_predictor.process_batch(**kwargs)
            print(f"\tpausing after batch {i:,}")
            sleep(d_DELAY_BETWEEN_BATCHES)
            
        if kwargs.get("move_bad", False):
            o_predictor._load()
            i_bad_properties = o_predictor.count_bad_zillow_batch(**kwargs)
            print(f"There are {i_bad_properties:,} bad properties that should be moved out of the raw scraped collection into failures")
            o_predictor.move_bad_zillow_batch(**kwargs)
        
    o_predictor._close()
    print("finished processing", utils.secondsToStr(time() - n_start))


def keep_scanning(**kwargs):
    load_config()

    i_batch_size = int(kwargs.get("i_batch_size", kwargs.get("batch_size", i_SCRAPE_BATCH)))
    if i_batch_size and not kwargs.get("i_batch_size"):
        kwargs["i_batch_size"] = i_batch_size
        
    i_max_batches = max(1, int(kwargs.get("max_batches", kwargs.get("max_predictions", 50) / i_batch_size)))
    
    if kwargs.get("retry_delay") and not kwargs.get("i_retry_delay"):
        kwargs["i_retry_delay"] = int(kwargs["retry_delay"])
    
    print("initializing predictor")
    o_predictor = ScrapedPropertyPredictor(schema="scraping", **kwargs)
    i_scans = 0    
    while True:
        load_config()
        
        i_scans += 1
        print("opening connection")
        o_predictor._load()
        
        i_requested_count, i_requested_retry_count = 0, 0
        i_count, i_retry_count = 0, 0
        i_national_count, i_national_retry_count = 0, 0
        
        if kwargs.get("skip_count", False):
            print("skipping counts")
        else:
            print("counting requested")
            i_requested_count = o_predictor.count_scraped_zillow_batch(requested=True, **{k: v for k, v in kwargs.items() if k not in ["retry", "i_retry_delay", "national", "requested"]})
            i_requested_retry_count = o_predictor.count_scraped_zillow_batch(requested=True, retry=True, **{k: v for k, v in kwargs.items() if k not in ["retry", "national", "requested"]})
        
            if not kwargs.get("skip_count", False) and not kwargs.get("national") and not kwargs.get("requested"):
                print("counting scraped")
                i_count = o_predictor.count_scraped_zillow_batch(**{k: v for k, v in kwargs.items() if k not in ["retry", "i_retry_delay", "national", "requested"]})
                i_retry_count = o_predictor.count_scraped_zillow_batch(retry=True, **{k: v for k, v in kwargs.items() if k not in ["national", "requested"]})
            
            if not kwargs.get("skip_count", False) and kwargs.get("national") or (not i_count and not i_retry_count) and (not i_requested_count and not i_requested_retry_count):
                print("counting national")
                i_national_count = o_predictor.count_scraped_zillow_batch(national=True, **{k: v for k, v in kwargs.items() if k not in ["retry", "i_retry_delay", "national", "requested"]})
        
        b_records_processed_this_loop = False
        print("start looping")
        
        if i_requested_count > 0 or (kwargs.get("skip_count") and kwargs.get("requested") and not kwargs.get("retry")):
            if kwargs.get("skip_count"):
                print(f"Skip count and assumed there are requested market records to be processed")
            else:
                print(f"There are {i_requested_count:,} scraped requested market records to be processed")
            
            a_scraped_areas = o_predictor.check_requested_scraped(**kwargs)
            e_scraped_areas = {e_area["_id"][0]: e_area for e_area in a_scraped_areas}
            
            while e_scraped_areas:
                b_records_processed_this_loop = True
                i_market_id = random.choice(list(e_scraped_areas.keys()))
                process_market(market_id=i_market_id, o_predictor=o_predictor, **kwargs)
                sleep(d_DELAY_BETWEEN_BATCHES)
                
                #o_predictor._close()
                #o_predictor._load()
                i_count = o_predictor.count_scraped_zillow_batch(requested=True, **{k: v for k, v in kwargs.items() if k not in ["retry", "i_retry_delay", "national", "requested"]})
                a_scraped_areas = o_predictor.check_requested_scraped(**kwargs)
                e_scraped_areas = {e_area["_id"][0]: e_area for e_area in a_scraped_areas}
                print(f"There are {i_count:,} scraped records in {len(a_scraped_areas):,} requested markets left to be processed")
            
        elif i_requested_retry_count > 0 or (kwargs.get("skip_count") and kwargs.get("requested") and kwargs.get("retry")):
            if kwargs.get("skip_count"):
                print(f"Skip count and assumed there are requested market records to be retried")
            else:
                print(f"There are {i_requested_retry_count:,} scraped requested market records to be retried")
            
            a_scraped_areas = o_predictor.check_requested_scraped(**kwargs)
            e_scraped_areas = {e_area["_id"][0]: e_area for e_area in a_scraped_areas}
            
            if e_scraped_areas:
                b_records_processed_this_loop = True
                i_market_id = random.choice(list(e_scraped_areas.keys()))
                process_market(market_id=i_market_id, o_predictor=o_predictor, **kwargs)
                sleep(d_DELAY_BETWEEN_BATCHES)
                
                #o_predictor._close()
                #o_predictor._load()
                i_count = o_predictor.count_scraped_zillow_batch(requested=True, retry=True, **{k: v for k, v in kwargs.items() if k not in ["retry", "i_retry_delay", "national", "requested"]})
                a_scraped_areas = o_predictor.check_requested_scraped(**kwargs)
                e_scraped_areas = {e_area["_id"][0]: e_area for e_area in a_scraped_areas}
                print(f"There are {i_count:,} scraped records in {len(a_scraped_areas):,} requested markets left to be processed")
        
        elif i_count > 0 or (kwargs.get("skip_count") and not kwargs.get("requested") and not kwargs.get("national") and not kwargs.get("retry")):
            if kwargs.get("skip_count"):
                print(f"Skip count and assumed there are records to be processed")
            else:
                print(f"There are {i_count:,} scraped records to be processed")
            
            n_start = time()
            b_more_to_process = True
            i = 0
            while b_more_to_process and i < i_max_batches:
                i += 1
                print(f"batch {i:,} ({(i - 1) * i_batch_size + 1:,} - {i * i_batch_size:,})")
                b_more_to_process = o_predictor.process_batch(**{k: v for k, v in kwargs.items() if k not in ["retry", "i_retry_delay", "national", "requested"]})
                print(f"\tpausing after batch {i:,}")
                sleep(d_DELAY_BETWEEN_BATCHES)
            b_records_processed_this_loop = True
            print("finished processing", utils.secondsToStr(time() - n_start))
            
        elif i_retry_count > 0 or (kwargs.get("skip_count") and not kwargs.get("requested") and not kwargs.get("national") and kwargs.get("retry")):
            if kwargs.get("skip_count"):
                print(f"Skip count and assumed there are records to be retried")
            else:
                print(f"There are {i_retry_count:,} scraped records to be retried")
            
            n_start = time()
            b_more_to_process = True
            i = 0
            while b_more_to_process and i < i_max_batches:
                i += 1
                print(f"batch {i:,} ({(i - 1) * i_batch_size + 1:,} - {i * i_batch_size:,})")
                b_more_to_process = o_predictor.process_batch(retry=True, **{k: v for k, v in kwargs.items() if k not in ["retry", "national", "requested"]})
                print(f"\tpausing after batch {i:,}")
                sleep(d_DELAY_BETWEEN_BATCHES)
            
            b_records_processed_this_loop = True
            print("finished retrying", utils.secondsToStr(time() - n_start))
        
        elif i_national_count > 0 or (kwargs.get("skip_count") and kwargs.get("national")):
            if kwargs.get("skip_count"):
                print(f"Skip count and assumed there are national records to be processed")
            else:
                print(f"There are {i_national_count:,} scraped records to be processed")
            
            n_start = time()
            b_more_to_process = True
            i = 0
            while b_more_to_process and i < i_max_batches:
                i += 1
                print(f"batch {i:,} ({(i - 1) * i_batch_size + 1:,} - {i * i_batch_size:,})")
                if kwargs.get("national"):
                    b_more_to_process = o_predictor.process_batch(national=True, **{k: v for k, v in kwargs.items() if k not in ["national", "requested"]})
                else:
                    b_more_to_process = o_predictor.process_batch(national=True, **{k: v for k, v in kwargs.items() if k not in ["retry", "i_retry_delay", "national", "requested"]})
                print(f"\tpausing after batch {i:,}")
                sleep(d_DELAY_BETWEEN_BATCHES)
            
            b_records_processed_this_loop = True
            print("finished processing", utils.secondsToStr(time() - n_start))
        
        if kwargs.get("move_bad", False):
            o_predictor._load()
            i_bad_properties = o_predictor.count_bad_zillow_batch(**kwargs)
            print(f"There are {i_bad_properties:,} bad properties that should be moved out of the raw scraped collection into failures")
            o_predictor.move_bad_zillow_batch(**{k: v for k, v in kwargs.items() if k not in ["national"]})
        
        #d_DELAY_BETWEEN_RETRY_SCANS
        o_predictor._close()
        
        if b_records_processed_this_loop:
            print(f"Scan {i_scans:,} finished. Rescan in {utils.secondsToStr(d_DELAY_BETWEEN_SCANS_PROCESSED)}")
            sleep(d_DELAY_BETWEEN_SCANS_PROCESSED)
        else:
            print(f"Scan {i_scans:,} finished. Rescan in {utils.secondsToStr(d_DELAY_BETWEEN_SCANS)}")
            sleep(d_DELAY_BETWEEN_SCANS)
            
            check_requested_market_progress(o_predictor=o_predictor, **kwargs)


if __name__ == "__main__":
    args, kwargs = utils.parse_command_line_args(sys.argv)
    print("Running predict_scraped_properties.py from shell")
    
    if "rent" in kwargs and "rentals" not in kwargs: kwargs["rentals"] = kwargs["rent"]
    if "i_scraper_id" in kwargs and "scraper_id" not in kwargs: kwargs["scraper_id"] = kwargs["i_scraper_id"]
    
    if kwargs.get("keep_scanning"):
        keep_scanning(**kwargs)
    else:
        process_scraped(**kwargs)
