﻿from datetime import datetime, timedelta
import os, re
from math import sin, cos, sqrt, atan2, radians
from pprint import pprint
from typing import List, Optional, Union, Dict, Any, Tuple
from collections import defaultdict
from bson.son import SON
from dotenv import load_dotenv
import pandas as pd
from pymongo import cursor, ASCENDING, mongo_client, UpdateOne
import numpy as np

try:
    from .airbnb_standardization import e_PROPERTY_TYPES
    from .zillow_dicts import *
    from . import db_tables
except ImportError:
    from airbnb_standardization import e_PROPERTY_TYPES
    from zillow_dicts import *
    import db_tables

c_YES = {"Yes", "yes", "YES", "True", "true", "TRUE",  True, 1}
e_LOCATION_TRANSLATIONS = {}
dt_TRANSLATION_RELOAD = datetime.utcnow() + timedelta(hours=1)

def load_data(**kwargs):
    global e_LOCATION_TRANSLATIONS, dt_TRANSLATION_RELOAD
    if not e_LOCATION_TRANSLATIONS or datetime.utcnow() > dt_TRANSLATION_RELOAD:
        o_db = db_tables.get_connection()
        e_LOCATION_TRANSLATIONS = defaultdict(dict)
        for location_type, state, std_name, name in o_db.execute("SELECT `location_type`, state, standardized_name, name FROM location_translation WHERE standardized_name != name;"):
            if location_type == "state":
                e_LOCATION_TRANSLATIONS[location_type][name] = std_name
            else:
                e_LOCATION_TRANSLATIONS[location_type][(state, name)] = std_name
        o_db.close()
        o_db = None

def extract_id(arr: List[Any]) -> Optional[List[str]]:
    if not arr:
        return None
    ids = []
    for record in arr:
        try:
            ids.append(record.id)
        except:
            continue
    return ids


def convert_to_int(zip_code: Union[str, int]) -> Optional[int]:
    try:
        return int(zip_code)
    except (ValueError, TypeError):
        return None


def extract_features_from_str_array(a_values, keyword: str) -> str:
    if not a_values:
        return ""
    for record in a_values:
        if not isinstance(record, str): continue
        record = record.strip()
        if record.startswith(keyword):
            return record.replace(keyword, '').strip()
    return ''


def is_keyword_in_document(keywords: List[str], field: Union[str, List[Any], Dict[str, Any]], max_nesting_level: int=0, current_depth: int=0) -> bool:
    current_depth+= 1
    if current_depth > max_nesting_level:
        return False
    if isinstance(field, str):
        return bool(np.prod([keyword.lower() in field.lower() for keyword in keywords]))
    elif isinstance(field, list):
        return bool(sum([is_keyword_in_document(keywords, value, max_nesting_level, current_depth) for value in field]))
    elif isinstance(field, dict):
        return is_keyword_in_document(keywords, [key for key in field.keys()], max_nesting_level, current_depth) +\
              is_keyword_in_document(keywords, [value for value in field.values()], max_nesting_level, current_depth)
    else:
        return False


def merge_two_field(field1: List[Dict[str, Any]], field2: List[Dict[str, Any]], name_fields: List[str], value_field: str) -> Dict[str, Any]:
        result = {}
        data = []
        if field1:
            data = data + field1
        if field2:
            data = data + field2
        for record in data:
            name = "_".join([record[key] for key in name_fields])
            if not result.get(name):
                result[name] = []
            for value in record[value_field]:
                for existing_value in result[name]:
                    if np.prod([sub_value in existing_value for sub_value in value.split()]):
                        break
                else:
                    result[name].append(value.lower())
        for key in result.keys():
            result[key] = list(set(result[key]))
        return result


# some fields have nulls everywere, do it's not possible to defile their format
def transform_realtor_to_zillow(e_realtor):
    #if e_realtor.get("property_id"): return None
    load_data()
    
    e_zillow = {
        "_id": e_realtor.get('_id'),
        #"_id": 'realtor-' + str(e_realtor.get('property_id')), #TODO check that there is unique id for each property for zillow and realtors (by address: 1 address <=> 1 id) 
        #                       # and id format as zillow-... and realtor-... for now only this. Deduplicating - next step
        #                      # no duplicate of zpid in current zillow_scraped found 
        #                     #TODO: find dublicates for realtor, what should i do with them ? which one should i keep?
        "listing_id": e_realtor.get('listing_id'),
        "realtor_id": e_realtor.get('property_id'),
        "address": e_realtor.get('address'), # cleanup by location translation table from sql 
        "city": e_LOCATION_TRANSLATIONS.get("city", {}).get(e_realtor.get('city'),e_realtor.get('city')),
        "state": e_LOCATION_TRANSLATIONS.get("state", {}).get(e_realtor.get('state'),e_realtor.get('state')),
        "zip": convert_to_int(e_realtor.get('zip').strip()[:5]),
        "county": e_LOCATION_TRANSLATIONS.get("county", {}).get(e_realtor.get('county'),e_realtor.get('county')),
        "bathrooms": e_realtor.get('bath'),
        "bedrooms": e_realtor.get('bed'),
        "home_sqft": e_realtor.get('home_sqft'),
        "price": e_realtor.get('price'),
        "lot_size": e_realtor.get('acres'),
        "lor_area_value": e_realtor.get('acres'),  
        "sqft_price": e_realtor['acre_price'] / 43560 if e_realtor.get('acre_price') != None else None,
        "home_status": e_realtor.get('listing_status').title() if isinstance(e_realtor.get('listing_status'), str) else None,
        "listing_sub_status": e_realtor.get('listing_sub_status'),
        "property_type": e_ZILLOW_PROP_TYPE_STD.get(str(e_realtor.get("property_type")).lower(), e_realtor.get("property_type")), 
        "latitude": e_realtor.get('latitude'),
        "longitude": e_realtor.get('longitude'),
        "date_sold": e_realtor.get('listing_date'),
        "days_on_zillow": e_realtor.get('days_on_market'), 
        "last_sold_date": e_realtor.get('last_sold_date'),
        "last_sold_date_unix": datetime.strptime(e_realtor.get('last_sold_date'), "%Y-%m-%d").timestamp() * 1000 if e_realtor.get('last_sold_date') else None,
        "last_sold_price": e_realtor.get('last_sold_price'),
        "images": ','.join(e_realtor.get('picture_links')),
        "description": e_realtor.get('description'),
        "schools_id": extract_id(e_realtor.get('schools')),
        "agent_name": str(e_realtor.get('realtor_f_name')) + ' ' + str(e_realtor.get('realtor_l_name')) if e_realtor.get('realtor_f_name') and e_realtor.get('realtor_l_name') else None,
        "agent_email": e_realtor.get('realtor_email'),
        "agent_phone": e_realtor.get('realtor_phone'),
        "agent_website": e_realtor.get('realtor_website'),
        "price_history": e_realtor.get('history_property'),
        "year": e_realtor.get('year_built'),
        "construction_materials": e_realtor.get('construction'),
        "cooling": e_realtor.get('cooling'), 
        "exterior": e_realtor.get('exterior'), 
        "fireplace": e_realtor.get('fireplace'),
        "garage_parking_apacity": e_realtor.get('garage'),
        "has_garage": bool(e_realtor.get('garage')),
        "garage_type": e_realtor.get('garage_type'),
        "has_heating": bool(extract_features_from_str_array(e_realtor.get('heating'), "Heating:") in c_YES),
        "heating_features": extract_features_from_str_array(e_realtor.get('heating'), 'Heating Features:'), 
        "cooling_features":  extract_features_from_str_array(e_realtor.get('heating'), 'Cooling Features:'),
        "has_cooling":  bool(extract_features_from_str_array(e_realtor.get('heating'), 'Cooling Features:')),
        "has_private_pool": bool(e_realtor.get('pool') in c_YES),
        "roof_type": e_realtor.get('roofing'),
        "rooms": e_realtor.get('rooms'),
        "stories": e_realtor.get('stories'),
        "architectural_style": e_realtor.get('styles'),
        "property_sub_type": e_realtor.get('sub_type'),
        "interior_features": e_realtor.get('interior_features'),
        "flooring": extract_features_from_str_array(e_realtor.get('interior_features'), 'Flooring:'), 
        "elevation": None, #bool(extract_features_from_str_array(e_realtor.get('interior_features'), 'Elevator:') in c_YES), 
        "exterior_features": e_realtor.get('exterior_features'),
        "appliances": e_realtor.get('appliances'),
        "laundry_features": extract_features_from_str_array(e_realtor.get('appliances'), "Laundry Features:"),
        "utilities": e_realtor.get('utilities'),
        "garage_2": e_realtor.get('garage_2'),
        "has_open_parking": bool(extract_features_from_str_array(e_realtor.get('garage_2'), "Open Parking:") in c_YES),
        "amenities": e_realtor.get('amenities'),
        "has_pets_allowed": 'Yes' in extract_features_from_str_array(e_realtor.get('amenities'), "Pets Allowed:"),
        "features": merge_two_field(e_realtor.get('features'), e_realtor.get('details'), ['category', 'parent_category'], 'text'),
        "scraping_date": e_realtor.get('scraping_date'),
        "has_association": is_keyword_in_document(['association', 'home'], e_realtor.get('features'), max_nesting_level=4),
        # check tax information in realtor if exist then include (need date for tax value )
        # add real_estimate if it's looks like price map to z-estimate # TODO: don't find any estimate
    }
    if isinstance(e_zillow["realtor_id"], str) and re.search(r"^\d+$", e_zillow["realtor_id"]):
        e_zillow["realtor_id"] = int(e_zillow["realtor_id"])
    return e_zillow