from datetime import datetime
import os
from math import sin, cos, sqrt, atan2, radians
from pprint import pprint
from typing import List, Optional, Union, Dict, Any, Tuple

from bson.son import SON
from dotenv import load_dotenv
import pandas as pd
from pymongo import cursor, ASCENDING, mongo_client, UpdateOne
import numpy as np

from airbnb_standardization import e_PROPERTY_TYPES

load_dotenv()

DEBUG = os.getenv('DEBUG')


class TransformCollection:
    def __init__(
        self, 
        mongo_client: mongo_client.MongoClient, 
        db: str, 
        from_collection: str, 
        to_collection: str,
        mysql_cur,
        sorting_fields: List[Tuple[str, int]],
        unique_field: str,
        batch_size=100
        ):
        self.mongo_client = mongo_client
        self.db = db
        self.mysql_cur = mysql_cur
        self.from_collection = from_collection
        self.to_collection = to_collection
        self.from_col = self.mongo_client[self.db][self.from_collection]
        self.to_col = self.mongo_client[self.db][self.to_collection]
        self.sorting_fields = sorting_fields
        self.unique_field = unique_field
        self.batch_size = batch_size

    def transform_realtor_to_zillow(self, transform_function: str, sql_db: str, sql_table: str, sql_fields: str, limit: int, skip: int):
        dd = Deduplication(self.mongo_client, self.db, self.from_collection, self.batch_size)
        ufc = UnifyFormatCollectons(self.mysql_cur, self.mongo_client, self.db, self.from_col, self.to_col, self.batch_size)
        ufc.sql_data = ufc.get_data_from_sql(sql_db, sql_table, sql_fields)
        transfoem_function = ufc.transform_functions[transform_function]
        batches = dd.get_unique_by_latest(self.unique_field, self.sorting_fields, limit=limit, skip=skip)
        for batch in batches:
            new_batch = []
            for doc in batch:
                new_doc = transfoem_function(doc)
                new_batch.append(new_doc)
            self.to_col.insert_many(new_batch)


class TransformByGeoSearch:
    def __init__(self, mongo_client: mongo_client.MongoClient, db: str, col_property: str, col_objects: str):
        self.mongo_client = mongo_client
        self.db = db
        self.col_property = self.mongo_client[db][col_property]
        self.col_objects = self.mongo_client[db][col_objects]

    @staticmethod
    def calc_distance_by_lat_lon(lat1: float, lon1: float, lat2: float, lon2: float) -> int:
        R = 6373.0
        lat1 = radians(lat1)
        lon1 = radians(lon1)
        lat2 = radians(lat2)
        lon2 = radians(lon2)
        dlon = lon2 - lon1
        dlat = lat2 - lat1
        a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
        c = 2 * atan2(sqrt(a), sqrt(1 - a))
        distance = R * c * 1000 # meters
        return round(distance)

    def find_nearest_objects(self, lon, lat, field_obj):
        objects = []
        for obj in self.col_objects.find({
            field_obj: {"$near": {"$geometry" :{"type": "Point", "coordinates": [ lon , lat ]}, "$maxDistance": 50000 * 6378.1}}
            }).limit(6):
            distance = self.calc_distance_by_lat_lon(lat, lon, obj['gps']['coordinates'][1], obj['gps']['coordinates'][0])
            objects.append({'id': obj['_id'], 'distance': distance})
        return objects

    def run(self, lon_field: str, lat_field: str, field_obj: str, new_field_name: str):
        updates = []
        for property in self.col_property.find():
            lon, lat = property.get(lon_field),  property.get(lat_field)
            if not (lon and lat):
                continue
            objects = self.find_nearest_objects(lon, lat, field_obj)
            updates.append(UpdateOne({'_id': property['_id']}, {"$set": {new_field_name: objects}}, upsert=False))
        self.col_property.bulk_write(updates)


class Deduplication:
    def __init__(self, mongo_client, db: str, collection: str, batch_size=100):
        self.mongo_client = mongo_client
        self.db_name = db
        self.collection_name = collection
        self.col = self.mongo_client[self.db_name][self.collection_name]
        self.batch_size = batch_size
        self.doc_processed = 0

    def get_docs_dublicates_by_mongo(self, fields: List[str]) -> cursor.Cursor:
        field_name = '_'.join(fields)
        field_name_service = ''.join(['$'+ field for field in fields])
        coursor = self.col.aggregate([
            {"$group": { "_id": {field_name: field_name_service}, "uniqueIds": {"$addToSet": "$_id"}, "count": {"$sum": 1} }},
            {"$match": { "count": {"$gt": 1}}},
            {"$sort": {"count": -1}}
            ],
            allowDiskUse=True
            )
        return coursor

    def fill_null_values(self, field: str, default: str):
        self.col.update({field: {"$exists" : False}}, {"$set": {field: default}})

    def get_unique_by_latest_by_mongo(self, fields: List[str]) -> cursor.Cursor:
        field_name = '_'.join(fields)
        field_name_service = ''.join(['$'+ field for field in fields])
        coursor = self.col.aggregate([
            { "$match": {"scraped_date": {"$exists": 1}}},
            { "$project": {"date":  {"$dateFromString": { "dateString": {"$ifNull": ["$scraped_date", "2022-01-01T00:00:00"]}}}, "doc": "$$ROOT"}},
            { "$group": { "_id": "$doc.property_id" , "doc_with_max_ver": {"$first": "$$ROOT"}}},
            { "$sort": {"date": -1 } },
            {"$replaceWith": "$doc_with_max_ver" }
            ], 
            allowDiskUse=True 
            )
        return coursor

    def get_unique_by_latest(self, unique_field: str, fields: List[Tuple[str, int]], limit: int, skip: int) -> cursor.Cursor:
        find_kwargs = {}
        if limit:
            find_kwargs['limit'] = limit
        if skip:
            find_kwargs['skip'] = skip
        cursor = self.col.find(**find_kwargs).sort(fields) #fields = [("property_id", pymongo.ASCENDING), ("scraped_date", pymongo.ASCENDING)]
        batch = []
        within_property_id = [cursor.__next__()]
        current_property_id = within_property_id[0][unique_field]
        amount_in_current_batch = 0
        for doc in cursor:
            if doc[unique_field] != current_property_id:
                current_property_id = doc[unique_field]
                batch.append(within_property_id[-1])
                within_property_id = [doc]
                amount_in_current_batch += 1
                self.doc_processed += 1
            else:
                within_property_id.append(doc)
            if (len(within_property_id) == 1) and (amount_in_current_batch >= self.batch_size):
                amount_in_current_batch = 0
                yield batch
                batch = []


class UnifyFormatCollectons:
    def __init__(self, mysql_cur, mongo_client, mongo_db='scraping', collection='realtor', new_collection='realtor_formatted', batch_size=100):
        self.mysql_cur = mysql_cur
        self.mongo_client = mongo_client
        self.mongo_db = mongo_db
        self.collection = collection
        self.new_collection = new_collection
        self.transform_functions = {'transform_realtor_to_zillow': self.transform_realtor_to_zillow}
        self.batch_size = batch_size

    @staticmethod
    def convert_to_int(zip_code: Union[str, int]) -> Optional[int]:
        try:
            return int(zip_code)
        except (ValueError, TypeError):
            return None

    @staticmethod
    def exctract_id(arr: List[Any]) -> Optional[List[str]]:
        if not arr:
            return None
        ids = []
        for record in arr:
            try:
                ids.append(record.id)
            except:
                continue
        return ids

    @staticmethod
    def extract_element_from_str_array(arr, keyword: str) -> str:
        if not arr:
            return ''
        for record in arr:
            if not isinstance(record, str):
                continue
            record = record.strip()
            if record.startswith(keyword):
                return record.replace(keyword, '')
        return ''

    def is_keyword_in_document(self, keywords: List[str], field: Union[str, List[Any], Dict[str, Any]], max_nesting_level: int=0, current_depth: int=0) -> bool:
        current_depth+= 1
        if current_depth > max_nesting_level:
            return False
        if isinstance(field, str):
            return bool(np.prod([keyword.lower() in field.lower() for keyword in keywords]))
        elif isinstance(field, list):
            return bool(sum([self.is_keyword_in_document(keywords, value, max_nesting_level, current_depth) for value in field]))
        elif isinstance(field, dict):
            return self.is_keyword_in_document(keywords, [key for key in field.keys()], max_nesting_level, current_depth) +\
                  self.is_keyword_in_document(keywords, [value for value in field.values()], max_nesting_level, current_depth)
        else:
            return False

    def get_data_from_sql(self, database: str, table: str, fields: List[str]) -> pd.DataFrame:
        sql1 = '''
        SELECT {columns} FROM {database}.{table};
        '''.format(database=database, table=table, columns=','.join(fields))
        self.mysql_cur.execute(sql1)
        data = []
        for row in self.mysql_cur:
            data.append(dict(zip(fields, row)))
        return pd.DataFrame(data)

    def standartisation_sql(self, value: str, column: str) -> str:
        genuine_value = self.sql_data[column].loc[self.sql_data[column].apply(lambda x: x.lower()) == value.lower().strip()]
        if not genuine_value.empty:
            return genuine_value.values[0]
        return f'Undefined: {value}'
    
    @staticmethod
    def standartisation_dict(value: str, source: Dict[str, Any]):
        for key in source:
            value = value.lower()
            if value in [keyword.lower() for keyword in source[key]]:
                return key
        return f'Undefined: {value}'

    def address_formatting(address: str):
        address_template = "{hous_number} {street}, {city} {state}, {country}"
        # TODO: implement
        return address_template#.format(hous_number=hous_number, street=street, city=city, country=country)

    @staticmethod
    def merge_two_field(field1: List[Dict[str, Any]], field2: List[Dict[str, Any]], name_fields: List[str], value_field: str) -> Dict[str, Any]:
        result = {}
        data = []
        if field1:
            data = data + field1
        if field2:
            data = data + field2
        for record in data:
            name = "_".join([record[key] for key in name_fields])
            if not result.get(name):
                result[name] = []
            for value in record[value_field]:
                for existing_value in result[name]:
                    if np.prod([sub_value in existing_value for sub_value in value.split()]):
                        break
                else:
                    result[name].append(value.lower())
        for key in result.keys():
            result[key] = list(set(result[key]))
        return result

    # some fields have nulls everywere, do it's not possible to defile their format
    def transform_realtor_to_zillow(self, doc) -> Dict[str, Any]:
        return {
        "_id": 'realtor-' + str(doc['property_id']), #TODO check that there is unique id for each property for zillow and realtors (by address: 1 address <=> 1 id) 
                                # and id format as zillow-... and realtor-... for now only this. Deduplicating - next step
                                # no duplicate of zpid in current zillow_scraped found 
                                #TODO: find dublicates for realtor, what should i do with them ? which one should i keep?
        "listing_id": doc['listing_id'],
        "property_id": doc['property_id'],
        "address": doc['address'], # cleanup by location translation table from sql 
        "city": self.standartisation_sql(doc['city'], 'standardized_name'),
        "state": self.standartisation_sql(doc['state'], 'state'),
        "zip": self.convert_to_int(doc['zip'].strip()[:5]),
        "county": doc['county'], # cleanup by location translation table from sql TODO: there is no info about county in location_tranalation table
        "bathrooms": doc['bath'],
        "bedrooms": doc['bed'],
        "home_sqft": doc['home_sqft'],
        "price": doc['price'],
        "lot_size": doc['acres'],
        "lor_area_value": doc['acres'],  
        "sqft_price": doc['acre_price'] / 43560 if doc['acre_price'] != None else None,
        "home_status": doc['listing_status'].title() if isinstance(doc['listing_status'], str) else None,
        "listing_sub_status": doc['listing_sub_status'],
        "property_type": self.standartisation_dict(' '.join(doc['property_type'].split('_')), e_PROPERTY_TYPES) if doc['property_type'] else None, 
        "latitude": doc['latitude'],
        "longitude": doc['longitude'],
        "date_sold": doc['listing_date'],
        "dayes_on_zillow": doc['days_on_market'], 
        "last_sold_date": doc['last_sold_date'],
        "last_sold_date_unix": datetime.strptime(doc['last_sold_date'], "%Y-%m-%d").timestamp() * 1000 if doc['last_sold_date'] else None,
        "last_sold_price": doc['last_sold_price'],
        "images": ','.join(doc['picture_links']),
        "description": doc['description'],
        "schools_id": self.exctract_id(doc['schools']),
        "agent_name": str(doc['realtor_f_name']) + ' ' + str(doc['realtor_l_name']) if doc['realtor_f_name'] and doc['realtor_l_name'] else None,
        "agent_email": doc['realtor_email'],
        "agent_phone": doc['realtor_phone'],
        "agent_website": doc['realtor_website'],
        "price_hostory": doc['history_property'],
        "year": doc['year_built'],
        "constraction_materials": doc['construction'],
        "cooling": doc['cooling'], 
        "exterior": doc['exterior'], 
        "fireplace": doc['fireplace'],
        "garage_parking_apacity": doc['garage'],
        "has_garage": bool(doc['garage']),
        "garage_type": doc['garage_type'],
        "has_heating": bool(self.extract_element_from_str_array(doc['heating'], "Heating:") == 'Yes'),
        "heating_features": self.extract_element_from_str_array(doc['heating'], 'Heating Features:'), 
        "cooling_features":  self.extract_element_from_str_array(doc['heating'], 'Cooling Features:'),
        "has_cooling":  bool(self.extract_element_from_str_array(doc['heating'], 'Cooling Features:')),
        "has_private_pool": bool(doc['pool']),
        "roof_type": doc['roofing'],
        "rooms": doc['rooms'],
        "stories": doc['stories'],
        "architectural_style": doc['styles'],
        "property_sub_type": doc['sub_type'],
        "interior_features": doc['interior_features'],
        "flooring": self.extract_element_from_str_array(doc['interior_features'], 'Flooring:'), 
        "elevation": bool(self.extract_element_from_str_array(doc['interior_features'], 'Elevator:') == "Yes"), 
        "exterior_features": doc['exterior_features'],
        "appliances": doc['appliances'],
        "laundry_features": self.extract_element_from_str_array(doc['appliances'], "Laundry Features:"),
        "utilities": doc['utilities'],
        "garage_2": doc['garage_2'],
        "has_open_parking": bool(self.extract_element_from_str_array(doc['garage_2'], "Open Parking:") == "Yes"),
        "amenities": doc['amenities'],
        "has_pets_allowed": 'Yes' in self.extract_element_from_str_array(doc['amenities'], "Pets Allowed:"),
        "features": self.merge_two_field(doc['features'], doc['details'], ['category', 'parent_category'], 'text'),
        "scraping_date": doc.get('scraped_date'),
        "has_assosiation": self.is_keyword_in_document(['association', 'home'], doc['features'], max_nesting_level=4)
        # check tax information in realtor if exist then include (need date for tax value )
        # add real_estimate if it's looks like price map to z-estimate # TODO: don't find any estimate
    }

    def run_single_doc(self, doc: Dict[str, Any], transform_function):
        return transform_function(doc)
            
    def run_single_doc_debug(self, doc: Dict[str, Any], id_list: List[str], transform_function) -> Tuple[Dict[str, Any], List[str]]:
        if doc['property_id'] in id_list:
            return {}, id_list
        id_list.append(id_list)
        return transform_function(doc), id_list
        


    def run(
            self, 
            transform_function_name: str='transform_realtor_to_zillow', 
            limit: Optional[int]=None, 
            sql_db: str='', 
            sql_table: str='', 
            sql_fields: List[str]= []
        ):
        db = self.mongo_client[self.mongo_db]
        col = db[self.collection]
        if DEBUG:
            id_list = [doc['id'] for doc in db[self.new_collection].find({})]
            processed = len(id_list)
        else:
            processed = db[self.new_collection].count_documents({})
        self.sql_data = self.get_data_from_sql(sql_db, sql_table, sql_fields)
        kwargs = {}
        if processed:
            kwargs['skip'] = int(processed)
        if limit:
            kwargs['limit'] = int(limit)
        cursor = col.find(**kwargs)
        doc_in_batch_processed = 0
        transform_function = self.transform_functions[transform_function_name]
        docs_to_insert = []
        for document in cursor:
            if DEBUG:
                new_document, id_list = self.run_single_doc_debug(document, id_list, transform_function)
            else:
                new_document = self.run_single_doc(document, transform_function)
            if new_document:
                docs_to_insert.append(new_document)
            doc_in_batch_processed += 1
            if doc_in_batch_processed >= self.batch_size:
                x = db[self.new_collection].insert_many(docs_to_insert)
                if DEBUG:
                    print(x.inserted_ids)
                docs_to_insert = []
                doc_in_batch_processed = 0
        if docs_to_insert:
            db[self.new_collection].insert_many(docs_to_insert)


class Extract:
    def __init__(self, mongo_client, mongo_db='scraping', collection='realtor', new_collection='schools', batch_size=100):
        self.mongo_client = mongo_client
        self.mongo_db = mongo_db
        self.collection = collection
        self.new_collection = new_collection
        self.objscts_id = []
        self.objscts = []
        self.batch_size = batch_size
        self.target_func = {'schools': self.extract_schools_from_single_doc, 'docs_by_condition': self.extract_docs_by_condition}

    def get_existing_ids(self, new_col):
        for _id in new_col.find({}, {'_id': 1}):
            self.objscts_id.append(_id['_id'])

    def exctract_single_school(self, school: Dict[str, Any]):
        coordinates = None
        if school.get('coordinate'):
            coordinate = school.get('coordinate')
            if coordinate.get('lon') and coordinate.get('lat'):
                coordinates =  { "type": "Point", "coordinates": [coordinate['lon'], coordinate['lat']] }
        return {
            "_id": str(school['id']),
            "gps": coordinates,
            "district": school.get('district'),
            "education_levels": school.get('education_levels'),
            "funding_type": school.get('funding_type'),
            "grades": school.get('grades'),
            "greatschools_id": school.get('greatschools_id'),
            "name": school.get('name'),
            "nces_code": school.get('nces_code'),
            "parent_rating": school.get('parent_rating'),
            "rating": school.get('rating'),
            "review_count": school.get('review_count'),
            "slug_id": school.get('slug_id'),
            "student_count": school.get('student_count'),
        }

    def extract_schools_from_single_doc(self, doc: Dict[str, Any]):
        schools = doc.get('schools')
        if not schools:
            return None
        for school in schools:
            if (not school.get('id')) or  (school.get('id') in self.objscts_id):
                continue
            new_school = self.exctract_single_school(school)
            self.objscts.append(new_school)
            self.objscts_id.append(school['id'])

    def extract_docs_by_condition(self, doc: Dict[str, Any]):
        if doc['_id'] in self.objscts_id:
            return
        self.objscts.append(doc)
        self.objscts_id.append(doc['_id'])

    def run(self, target, skip: int=0, limit: int=0, filter_args: Dict[str, Any] = {}, on_delete=False):
        db = self.mongo_client[self.mongo_db]
        col = db[self.collection]
        new_col = db[self.new_collection]
        self.get_existing_ids(new_col)
        find_kwargs = {}
        if limit:
            find_kwargs['limit'] = limit
        if skip:
            find_kwargs['skip'] = skip
        cursor = col.find(filter_args, **find_kwargs)
        count_in_batch = 0
        extract_function = self.target_func[target]
        for doc in cursor:
            extract_function(doc)
            count_in_batch += 1
            if count_in_batch >= self.batch_size:
                if self.objscts:
                    try:
                        new_col.insert_many(self.objscts)
                        if on_delete:
                            obj_to_delete = self.objscts_id[-count_in_batch:]
                            col.delete_many({"_id": {"$in": obj_to_delete}})
                    except:
                        pass
                count_in_batch = 0
                self.objscts = []
