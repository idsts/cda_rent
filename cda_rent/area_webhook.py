﻿import os, sys, re
from time import time, sleep, perf_counter
from datetime import datetime, timedelta
from math import cos, radians
from decimal import Decimal
import threading
import pandas as pd
import numpy as np

try:
    from . import utils, mysql_db, db_tables, us_states, location_analysis, google_maps, scoring
except:
    import utils, mysql_db, db_tables, us_states, location_analysis, google_maps, scoring

i_DEFAULT_MIN_AREA_PROPERTIES = 3

c_NO_SHRINK = {
    'id',
    'latitude',
    'longitude',
    'country',
    'state',
    'state_name',
    'dataset_quarter',
    'created',
    'updated',
    'population',
    'houses',
    'land_area',
    'water_area',
    'population_density',
    'housing_density',
    'records',
    'area_properties',
    'zillow_properties',
    'short_rental_penetration',
    'short_rental_penetration_normalized',
    'first_date',
    'last_date',
    'reserved_days',
    'available_days',
    'blocked_days',
    'days',
    'score_overall',
    'score_absolute',
    'score_relative',
    'score_area',
    'score_absolute_revenue',
    'score_absolute_net',
    'score_absolute_rate',
    'score_absolute_occupancy',
    'score_relative_roi',
    'score_relative_cap',
    'score_relative_expected',
    'score_area_cap',
    'score_area_opportunity',
    'score_area_size',
    'score_area_seasonality',
    'score_area_optimization',
    'score_area_rate',
    'score_area_penetration',
}

e_AREA_METRIC_PRECISION = {
    'living_area': 0,
    'living_area_median': 0,
    'lot_size': 2,
    'lot_size_median': 2,
    'year_built': 1,
    'bedrooms': 1,
    'bathrooms': 1,
    'max_guests': 1,
    'number_of_reviews': 0,
    'reserved_days': 0,
    'available_days': 0,
    'blocked_days': 0,
    'days': 0,
    'price': 2,
    'price_median': 2,
    'revenue': 2,
    'raw_revenue': 2,
    'revenue_median': 2,
    'rent': 2,
    'rate': 2,
    'rate_median': 2,
    
    'reservations': 1,
    'reservation_length': 1,
    'revenue_per_reservation': 2,
    'monthly_hoa_fee': 2,
    'property_tax_rate': 2,
    'tax_annual_amount': 2,
    
    'demand_season_length': 1,
    'demand_season_peak_month': 0,
    'demand_season_max_month': 0,
    'availability_season_length': 1,
    'availability_season_peak_month': 0,
    
    'type_revenue': 2,
    'type_rev_diff': 2,
    'rev_per_bedroom': 2,
    'rev_per_bathroom': 2,
    'rev_per_sqft': 2,
    'rev_per_acre': 2,
    'rev_per_price': 4,
    'rate_per_bedroom': 2,
    'rate_per_bathroom': 2,
    'rate_per_sqft': 2,
    'rate_per_acre': 2,
    'rate_per_price': 4,
    'rent_per_bedroom': 2,
    'rent_per_bathroom': 2,
    'rent_per_sqft': 2,
    'rent_per_acre': 2,
    'rent_per_price': 4,
    'price_per_bedroom': 2,
    'price_per_bathroom': 2,
    'price_per_sqft': 2,
    'price_per_acre': 2,

    'rate_month_1': 2,
    'rate_month_2': 2,
    'rate_month_3': 2,
    'rate_month_4': 2,
    'rate_month_5': 2,
    'rate_month_6': 2,
    'rate_month_7': 2,
    'rate_month_8': 2,
    'rate_month_9': 2,
    'rate_month_10': 2,
    'rate_month_11': 2,
    'rate_month_12': 2,
    'nearest_hotel_dist_0': 1,
    'nearest_hotel_rate_0': 2,
    'nearest_hotel_dist_1': 1,
    'nearest_hotel_rate_1': 2,
    'nearest_hotel_dist_2': 1,
    'nearest_hotel_rate_2': 2,
    'estimated_costs': 2,
    'estimated_net': 2,
}


def int_or_none(v):
    if not pd.isnull(v):
        if isinstance(v, (int, float)):
            return int(v)
        elif isinstance(v, str):
            return int(utils.to_float(v))
        else:
            return None


def check_locale_market_type(e_locale, e_results=None, **kwargs):
    s_market_type = e_locale.get("market_type", "market")
    #if s_market_type != "market": print(f"incoming market type {s_market_type}")

    s_market = e_locale.get("market", e_locale.get("address", ""))
    s_state = e_locale.get("state", "")
    if isinstance(s_state, str) and s_state:
        if '"' in s_state:
            s_state = s_state.replace('"', "")
            e_locale["state"] = s_state
        
        if s_state.lower() in us_states.e_ABBREV_STATE:
            pass
        elif s_state.lower() in us_states.e_STATE_ABBREV:
            s_state = us_states.e_STATE_ABBREV[s_state.lower()]
        else:
            s_state = ""
    else:
        s_state = ""
    
    s_zip = e_locale.get("zipcode", "")
    s_city = e_locale.get("city", "")
    s_county = e_locale.get("county", "")
    
    if isinstance(s_zip, str) and '"' in s_zip:
        s_zip = s_zip.replace('"', "").strip()
        e_locale["zipcode"] = s_zip
    if isinstance(s_city, str) and '"' in s_city:
        s_city = s_city.replace('"', "")
        e_locale["city"] = s_city
    if isinstance(s_county, str) and '"' in s_county:
        s_county = s_county.replace('"', "")
        e_locale["county"] = s_county
    
    
    if e_locale.get("market_type") == "zipcode" and s_zip:
        return e_locale
    elif e_locale.get("market_type") == "city" and s_city:
        a_addresses = google_maps.address_to_coordinates(f"{s_city}, {s_state}", **kwargs)
        if not a_addresses or not a_addresses[0].get('address_components', {}).get("city") or not a_addresses[0].get('address_components', {}).get("state"):
            print("Didn't find city or state in google maps lookup", a_addresses)
        elif a_addresses:
            s_addr_city = a_addresses[0].get('address_components', {}).get("city", [None])[0]
            s_addr_state = a_addresses[0].get('address_components', {}).get("state", [None])[0]
            s_addr_zip = a_addresses[0].get('address_components', {}).get("postal_code", [None])[0]
        
            if s_addr_zip: e_locale["zipcode"] =  s_addr_zip
            if s_addr_city: e_locale["city"] =  s_addr_city
            if s_addr_state: e_locale["state"] =  s_addr_state
            
        return e_locale
    elif e_locale.get("market_type") == "county" and s_county:
        return e_locale
    elif e_locale.get("market_type") == "state" and s_state:
        return e_locale
    
    if s_market_type != "market": print("Couldn't match specified market_type with data in separate locale fields")
    
    if s_zip:
        s_market_type = "zipcode"
    elif s_city:
        a_addresses = google_maps.address_to_coordinates(f"{s_city}, {s_state}", **kwargs)
        if not a_addresses or not a_addresses[0].get('address_components', {}).get("city") or not a_addresses[0].get('address_components', {}).get("state"):
            print("Didn't find city or state in google maps lookup", a_addresses)
        elif a_addresses:
            s_addr_city = a_addresses[0].get('address_components', {}).get("city", [None])[0]
            s_addr_state = a_addresses[0].get('address_components', {}).get("state", [None])[0]
            s_addr_zip = a_addresses[0].get('address_components', {}).get("postal_code", [None])[0]

            if s_addr_zip: e_locale["zipcode"] =  s_addr_zip
            if s_addr_city: e_locale["city"] =  s_addr_city
            if s_addr_state: e_locale["state"] =  s_addr_state
            s_market_type = "city"
    elif s_county:
        s_market_type = "county"
    elif s_state:
        s_market_type = "state"
    elif s_market:
        a_addresses = google_maps.address_to_coordinates(s_market, **kwargs)
        if a_addresses:
            s_addr_zip = a_addresses[0].get('address_components', {}).get("postal_code", [None])[0]
            s_addr_city = a_addresses[0].get('address_components', {}).get("city", [None])[0]
            s_addr_state = a_addresses[0].get('address_components', {}).get("state", [None])[0]

            if s_addr_zip: e_locale["zipcode"] =  s_addr_zip
            if s_addr_city: e_locale["city"] =  s_addr_city
            if s_addr_state: e_locale["state"] =  s_addr_state
            
            if re.search(r"\d{5,5}", s_market) and s_addr_zip:
                s_market_type = "zipcode"
            elif re.search(r"[a-z]{2,} *, *[a-z]{2,20}", s_market, flags=re.I) and s_addr_city and s_addr_state:
                s_market_type = "city"
            elif "," not in s_market and s_market.lower() not in us_states.e_ABBREV_STATE and s_market.lower() not in us_states.e_STATE_ABBREV:
                s_market_type = "city"
            elif s_addr_state:
                s_market_type = "state"

    e_locale["market_type"] = s_market_type
    return e_locale


s_ORDER = r"ORDER BY dataset_quarter DESC, area_properties DESC LIMIT 1"
def process_area_request(e_locale, e_results=None, **kwargs):
    e_found_result = {}
    
    if not e_locale:
        if isinstance(e_results, dict): e_results[e_locale["idx"]] = e_found_result
        return e_found_result
    
    if isinstance(e_results, dict): assert isinstance(e_locale.get("idx"), int)
    
    i_min_properties = int_or_none(e_locale.get("min_properties"))
    if not i_min_properties or i_min_properties < 1: i_min_properties = 3
    
    o_db = mysql_db.mySqlDbConn(
        s_host=mysql_db._host,
        s_schema="predictor",
        s_user=mysql_db._user,
        s_pass=mysql_db._password,)
    
    e_locale = check_locale_market_type(e_locale)
    s_market_type = e_locale.get("market_type")
    #print(f"after check market type {s_market_type}")
    
    if s_market_type in {"market", "", None}:
        print("Couldn't find address or identify market_type")
        return e_found_result
        
    
    s_market = e_locale.get("market", "")
    s_state = e_locale.get("state", "")
    if isinstance(s_state, str) and s_state:
        if isinstance(s_state, str): s_state = s_state.replace('"', "")
        if s_state.lower() in us_states.e_ABBREV_STATE:
            pass
        elif s_state.lower() in us_states.e_STATE_ABBREV:
            s_state = us_states.e_STATE_ABBREV[s_state.lower()]
        else:
            s_state = ""
    else:
        s_state = ""
    
    s_zip = e_locale.get("zipcode", "")
    s_city = e_locale.get("city", "")
    s_county = e_locale.get("county", "")
    #v_bedrooms = e_locale.get("bedrooms")
    #if v_bedrooms:
    #    if isinstance(v_bedrooms, (int, float)):
    #        i_bedrooms = int(v_bedrooms)
    #    elif isinstance(v_bedrooms, str):
    #        i_bedrooms = int(utils.to_float(v_bedrooms))
    #    else:
    #        i_bedrooms = None
    
    i_bedrooms = int_or_none(e_locale.get("bedrooms"))
    #print("i_bedrooms", i_bedrooms)
    
    s_ptype = str(e_locale.get("property_type", "")).strip() if e_locale.get("property_type", "") else ""
    if s_ptype in {"None", "nan", "null"}: s_ptype = ""
    
    #if s_ptype and not i_bedrooms:
     #   print(f"Can't filter on just property type={s_ptype} without a #bedrooms because we don't have those db tables yet. Do regular area search")
      #  s_ptype = ""
    
    if i_bedrooms and s_ptype:
        #print("bedrooms and property type")
        if s_market_type == "zipcode" and s_zip:
            if isinstance(s_zip, str): s_zip = s_zip.replace('"', "")
            s_sql = f'''
                SELECT * FROM {db_tables.zipcode_ptype_bedrooms}
                WHERE zipcode = "{s_zip}"
                    AND property_type = "{s_ptype}"
                    AND bedrooms = "{i_bedrooms}"
                    {s_ORDER};'''
            #print(f"zipcode property_type and bedrooms sql: {s_sql}")
            a_rows = [
                dict(zip(db_tables.a_ZIP_PTYPE_BEDROOM_COLS, t))
                for t in o_db.execute(s_sql)]
            if a_rows and a_rows[0] and a_rows[0]["area_properties"] >= i_min_properties:
                e_found_result = a_rows[0]
                e_found_result["filter_type"] = "property_type/bedroom"
                e_found_result["market_type"] = s_market_type
        elif s_market_type == "city" and s_city:
            s_sql = f'''
                SELECT * FROM {db_tables.city_ptype_bedrooms}
                WHERE (city = "{s_city}" or alt_city_name = "{s_city}")
                    AND state = "{s_state}"
                    AND property_type = "{s_ptype}"
                    AND bedrooms = "{i_bedrooms}"
                {s_ORDER};'''
            #print(f"city property_type and bedrooms sql: {s_sql}")
            a_rows = [
                dict(zip(db_tables.a_CITY_PTYPE_BEDROOM_COLS, t))
                for t in o_db.execute(s_sql)]
            if a_rows and a_rows[0] and a_rows[0]["area_properties"] >= i_min_properties:
                e_found_result = a_rows[0]
                e_found_result["filter_type"] = "property_type/bedroom"
                e_found_result["market_type"] = s_market_type
        elif s_market_type == "county" and s_county:
            s_sql = f'''
                SELECT * FROM {db_tables.county_ptype_bedrooms}
                WHERE (county = "{s_county}" or alt_county_name = "{s_county}")
                    AND state = "{s_state}"
                    AND property_type = "{s_ptype}"
                    AND bedrooms = "{i_bedrooms}"
                {s_ORDER};'''
            #print(f"city property_type and bedrooms sql: {s_sql}")
            a_rows = [
                dict(zip(db_tables.a_COUNTY_PTYPE_BEDROOM_COLS, t))
                for t in o_db.execute(s_sql)]
            if a_rows and a_rows[0] and a_rows[0]["area_properties"] >= i_min_properties:
                e_found_result = a_rows[0]
                e_found_result["filter_type"] = "property_type/bedroom"
                e_found_result["market_type"] = s_market_type
        elif s_market_type == "state" and s_state:
            if s_state:
                s_sql = f'''
                    SELECT * FROM {db_tables.state_ptype_bedrooms}
                    WHERE state = "{s_state}"
                        AND property_type = "{s_ptype}"
                        AND bedrooms = "{i_bedrooms}"
                    {s_ORDER};'''
                #print(f"state property_type and bedrooms sql: {s_sql}")
                a_rows = [
                    dict(zip(db_tables.a_STATE_PTYPE_BEDROOM_COLS, t))
                    for t in o_db.execute(s_sql)]
                if a_rows and a_rows[0] and a_rows[0]["area_properties"] >= i_min_properties:
                    e_found_result = a_rows[0]
                    e_found_result["filter_type"] = "property_type/bedroom"
                    e_found_result["market_type"] = s_market_type
    
    elif i_bedrooms:
        #print("bedrooms, no property type")
        if s_market_type == "zipcode" and s_zip:
            #if isinstance(s_zip, str): s_zip = s_zip.replace('"', "")
            s_sql = f'''
                SELECT * FROM {db_tables.zipcode_bedrooms}
                WHERE zipcode = "{s_zip}"
                    AND bedrooms = "{i_bedrooms}"
                    {s_ORDER};'''
            #print(f"zipcode bedrooms sql: {s_sql}")
            a_rows = [
                dict(zip(db_tables.a_ZIP_BEDROOM_COLS, t))
                for t in o_db.execute(s_sql)]
            if a_rows and a_rows[0] and a_rows[0]["area_properties"] >= i_min_properties:
                e_found_result = a_rows[0]
                e_found_result["filter_type"] = "bedroom"
                e_found_result["market_type"] = s_market_type
        elif s_market_type == "city" and s_city:
            s_sql = f'''
                SELECT * FROM {db_tables.city_bedrooms}
                WHERE (city = "{s_city}" or alt_city_name = "{s_city}")
                    AND state = "{s_state}"
                    AND bedrooms = "{i_bedrooms}"
                {s_ORDER};'''
            #print(f"city bedrooms sql: {s_sql}")
            a_rows = [
                dict(zip(db_tables.a_CITY_BEDROOM_COLS, t))
                for t in o_db.execute(s_sql)]
            if a_rows and a_rows[0] and a_rows[0]["area_properties"] >= i_min_properties:
                e_found_result = a_rows[0]
                e_found_result["filter_type"] = "bedroom"
                e_found_result["market_type"] = s_market_type
        elif s_market_type == "county" and s_county:
            s_sql = f'''
                SELECT * FROM {db_tables.county_bedrooms}
                WHERE (county = "{s_county}" or alt_county_name = "{s_county}")
                    AND state = "{s_state}"
                    AND bedrooms = "{i_bedrooms}"
                {s_ORDER};'''
            #print(f"city bedrooms sql: {s_sql}")
            a_rows = [
                dict(zip(db_tables.a_COUNTY_BEDROOM_COLS, t))
                for t in o_db.execute(s_sql)]
            if a_rows and a_rows[0] and a_rows[0]["area_properties"] >= i_min_properties:
                e_found_result = a_rows[0]
                e_found_result["filter_type"] = "bedroom"
                e_found_result["market_type"] = s_market_type
        elif s_market_type == "state" and s_state:
            if s_state:
                s_sql = f'''
                    SELECT * FROM {db_tables.state_bedrooms}
                    WHERE state = "{s_state}"
                        AND bedrooms = "{i_bedrooms}"
                    {s_ORDER};'''
                #print(f"state bedrooms sql: {s_sql}")
                a_rows = [
                    dict(zip(db_tables.a_STATE_BEDROOM_COLS, t))
                    for t in o_db.execute(s_sql)]
                if a_rows and a_rows[0] and a_rows[0]["area_properties"] >= i_min_properties:
                    e_found_result = a_rows[0]
                    e_found_result["filter_type"] = "bedroom"
                    e_found_result["market_type"] = s_market_type
    
    
    elif s_ptype:
        #print("property type, no bedrooms")
        #print(s_market_type, s_city, s_state, s_zip)
        if s_market_type == "zipcode" and s_zip:
            if isinstance(s_zip, str): s_zip = s_zip.replace('"', "")
            s_sql = f'''
                SELECT * FROM {db_tables.zipcode_ptypes}
                WHERE zipcode = "{s_zip}"
                    AND property_type = "{s_ptype}"
                    {s_ORDER};'''
            #print(f"zipcode property_type sql: {s_sql}")
            a_rows = [
                dict(zip(db_tables.a_ZIP_PTYPE_COLS, t))
                for t in o_db.execute(s_sql)]
            if a_rows and a_rows[0] and a_rows[0]["area_properties"] >= i_min_properties:
                e_found_result = a_rows[0]
                e_found_result["filter_type"] = "property_type"
                e_found_result["market_type"] = s_market_type
        elif s_market_type == "city" and s_city:
            s_sql = f'''
                SELECT * FROM {db_tables.city_ptypes}
                WHERE (city = "{s_city}" or alt_city_name = "{s_city}")
                    AND state = "{s_state}"
                    AND property_type = "{s_ptype}"
                {s_ORDER};'''
            #print(f"city property_type sql: {s_sql}")
            a_rows = [
                dict(zip(db_tables.a_CITY_PTYPE_COLS, t))
                for t in o_db.execute(s_sql)]
            if a_rows and a_rows[0] and a_rows[0]["area_properties"] >= i_min_properties:
                e_found_result = a_rows[0]
                e_found_result["filter_type"] = "property_type"
                e_found_result["market_type"] = s_market_type
        elif s_market_type == "county" and s_county:
            s_sql = f'''
                SELECT * FROM {db_tables.county_ptypes}
                WHERE (county = "{s_county}" or alt_county_name = "{s_county}")
                    AND state = "{s_state}"
                    AND property_type = "{s_ptype}"
                {s_ORDER};'''
            #print(f"county property_type sql: {s_sql}")
            a_rows = [
                dict(zip(db_tables.a_COUNTY_PTYPE_COLS, t))
                for t in o_db.execute(s_sql)]
            if a_rows and a_rows[0] and a_rows[0]["area_properties"] >= i_min_properties:
                e_found_result = a_rows[0]
                e_found_result["filter_type"] = "property_type"
                e_found_result["market_type"] = s_market_type
        elif s_market_type == "state" and s_state:
            if s_state:
                s_sql = f'''
                    SELECT * FROM {db_tables.state_ptypes}
                    WHERE state = "{s_state}"
                        AND property_type = "{s_ptype}"
                    {s_ORDER};'''
                #print(f"state property_type sql: {s_sql}")
                a_rows = [
                    dict(zip(db_tables.a_STATE_PTYPE_COLS, t))
                    for t in o_db.execute(s_sql)]
                if a_rows and a_rows[0] and a_rows[0]["area_properties"] >= i_min_properties:
                    e_found_result = a_rows[0]
                    e_found_result["filter_type"] = "property_type"
                    e_found_result["market_type"] = s_market_type
    else:
        #print("no bedrooms or property type")
        if s_market_type == "zipcode" and s_zip:
            s_sql = f'''
                SELECT * FROM `{db_tables.zipcode_info}`
                WHERE zipcode = "{s_zip}"
                {s_ORDER};'''
            #print(s_sql)
            
            a_rows = list(o_db.execute(s_sql))
            if a_rows and len(a_rows[0]) != len(db_tables.a_ZIP_INFO_COLS):
                db_tables.load_db(reload=True)
            a_rows = [
                dict(zip(db_tables.a_ZIP_INFO_COLS, t))
                for t in a_rows]
            
            if a_rows and a_rows[0] and a_rows[0]["area_properties"] >= i_min_properties:
                e_found_result = a_rows[0]
                e_found_result["filter_type"] = ""
                e_found_result["market_type"] = s_market_type
        elif s_market_type == "city" and s_city:
            s_sql = f'''
                SELECT * FROM `{db_tables.city_info}`
                WHERE (city = "{s_city}" or alt_city_name = "{s_city}")
                    AND state = "{s_state}"
                {s_ORDER};'''
            #print(s_sql)
            
            a_rows = list(o_db.execute(s_sql))
            if a_rows and len(a_rows[0]) != len(db_tables.a_CITY_INFO_COLS):
                db_tables.load_db(reload=True)
            a_rows = [
                dict(zip(db_tables.a_CITY_INFO_COLS, t))
                for t in a_rows]
            
            if a_rows and a_rows[0] and a_rows[0]["area_properties"] >= i_min_properties:
                e_found_result = a_rows[0]
                e_found_result["filter_type"] = ""
                e_found_result["market_type"] = s_market_type
            #else:
            #    print("Didn't find city or state in google maps lookup", a_addresses[0])
            #    return {}
        elif s_market_type == "county" and s_county:
            s_sql = f'''
                SELECT * FROM `{db_tables.county_info}`
                WHERE (county = "{s_county}" or alt_county_name = "{s_county}")
                    AND state = "{s_state}"
                {s_ORDER};'''
            #print(s_sql)
            a_rows = list(o_db.execute(s_sql))
            if a_rows and len(a_rows[0]) != len(db_tables.a_COUNTY_INFO_COLS):
                db_tables.load_db(reload=True)
            a_rows = [
                dict(zip(db_tables.a_COUNTY_INFO_COLS, t))
                for t in a_rows]
            
            if a_rows and a_rows[0] and a_rows[0]["area_properties"] >= i_min_properties:
                e_found_result = a_rows[0]
                e_found_result["filter_type"] = ""
                e_found_result["market_type"] = s_market_type
        elif s_market_type == "state" and s_state:
            if s_state:
                s_sql = f'''SELECT * FROM `{db_tables.state_info}` WHERE state = "{s_state}";'''
                #print(s_sql)
                a_rows = list(o_db.execute(s_sql))
                if a_rows and len(a_rows[0]) != len(db_tables.a_STATE_INFO_COLS):
                    db_tables.load_db(reload=True)
                a_rows = [
                    dict(zip(db_tables.a_STATE_INFO_COLS, t))
                    for t in a_rows]
                
                if a_rows and a_rows[0] and a_rows[0]["area_properties"] >= i_min_properties:
                    e_found_result = a_rows[0]
                    e_found_result["filter_type"] = ""
                    e_found_result["market_type"] = s_market_type

    if isinstance(e_results, dict):
        e_results[e_locale["idx"]] = e_found_result
    return e_found_result


def process_multiple_locales(a_locales):
    n_start = perf_counter()
    a_results = []
    e_results = {}
    # process each locale request in order
    for idx, e_locale in enumerate(a_locales):
        if e_locale.get("idx") is None: e_locale["idx"] = idx
        e_locale_result = process_area_request(e_locale, e_results)
        #a_results.append((e_locale, e_locale_result))
        a_results.append(e_locale_result)
        #break
    n_end = perf_counter()
    #print(f'Finding locales took {n_end - n_start :0.2f} second(s).')
    return a_results


def process_locale_batch(a_locales, e_results=None):
    n_start = perf_counter()
    a_results = []
    if e_results is None: e_results = {}
    # process each locale request in order
    for idx, e_locale in enumerate(a_locales):
        if e_locale.get("idx") is None: e_locale["idx"] = idx
        e_locale_result = process_area_request(e_locale, e_results)
        #a_results.append((e_locale, e_locale_result))
        a_results.append(e_locale_result)
        #break
    n_end = perf_counter()
    #print(f'Finding locales took {n_end - n_start :0.2f} second(s).')
    return a_results


def split_list(a_list, i_parts=2):
    p = len(a_list) // i_parts
    if len(a_list) - p > 0:
        return [a_list[:p]] + split_list(a_list[p:], i_parts - 1)
    else:
        return [a_list]


def process_multiple_locale_batches(a_locales, i_threads=8):
    n_start = perf_counter()
    
    for idx, e_locale in enumerate(a_locales):
        if isinstance(e_locale.get("idx"), str) and re.search(r"^\d+$", e_locale["idx"]):
            e_locale["idx"] = int(e_locale["idx"])
        if not isinstance(e_locale.get("idx"), (int, float)):
            e_locale["idx"] = idx
    
    a_locale_lists = split_list(a_locales, i_parts=min(i_threads, len(a_locales)))
    
    # create and start threads to process each locale request
    a_threads = []
    e_results = {}
        
    for a_part in a_locale_lists:
        t = threading.Thread(target=process_locale_batch, args=(a_part, e_results))
        a_threads.append(t)
        t.start()
    
    # wait for the threads to complete
    for t in a_threads:
        t.join()
    #if not e_results: return []
    
    for e_locale in a_locales:
        if e_locale["idx"] not in e_results: e_results[e_locale["idx"]] = {}
    
    #print("result keys", {i: True if v else False for i, v in e_results.items()})
    #print(e_results.keys())
    
    assert len(e_results) == len(a_locales)
    _, a_results = zip(*sorted(e_results.items(), key=lambda t:t[0]))
    
    n_end = perf_counter()
    #print(f'Finding locales took {n_end- n_start :0.2f} second(s).')
    return list(a_results)


def process_multiple_locales_mp(a_locales):
    n_start = perf_counter()
    
    # create and start threads to process each locale request
    a_threads = []
    e_results = {}
    for idx, e_locale in enumerate(a_locales):
        if isinstance(e_locale.get("idx"), str) and re.search(r"^\d+$", e_locale["idx"]):
            e_locale["idx"] = int(e_locale["idx"])
        if not isinstance(e_locale.get("idx"), (int, float)):
            e_locale["idx"] = idx
        
        t = threading.Thread(target=process_area_request, args=(e_locale, e_results))
        a_threads.append(t)
        t.start()
    
    # wait for the threads to complete
    for t in a_threads:
        t.join()
    
    if not e_results: return []
    
    for e_locale in a_locales:
        if e_locale["idx"] not in e_results: e_results[e_locale["idx"]] = {}
    
    #print("result keys", {i: True if v else False for i, v in e_results.items()})
    #print(e_results.keys())
    
    assert len(e_results) == len(a_locales)
    _, a_results = zip(*sorted(e_results.items(), key=lambda t:t[0]))
    
    n_end = perf_counter()
    #print(f'Finding locales took {n_end- n_start :0.2f} second(s).')
    return list(a_results)


def get_metric_from_db(e_metric_lookup, e_results=None, **kwargs):
    s_metric = e_metric_lookup.get("metric", "revenue")
    s_market_type = e_metric_lookup.get("market_type", "state")
    i_bedrooms = e_metric_lookup.get("bedrooms", 0)
    s_property_type = e_metric_lookup.get("property_type", "")
    i_min_properties = e_metric_lookup.get("min_properties", 3)

    assert s_metric.lower() in db_tables.a_STATE_INFO_COLS
    a_where = ["dataset_quarter ="]
    #a_where_variables = [db_tables.e_CONFIG["latest_dataset_quarter"].strftime("%Y-%m-%d")]
    a_where_variables = [db_tables.e_CONFIG["latest_dataset_quarter"]]
    
    a_cols = [s_metric, "latitude", "longitude",]
    
    if s_property_type and not i_bedrooms:
        print(f"Can't filter on just property type={s_property_type} without a #bedrooms because we don't have those db tables yet. Do regular area search")
        s_property_type = ""
    
    if i_bedrooms:
        a_where.append("bedrooms =")
        a_where_variables.append(i_bedrooms)

    if s_property_type:
        a_where.append("property_type =")
        a_where_variables.append(s_property_type)
    
    if i_min_properties and i_min_properties > 0:
        a_where.append("area_properties >=")
        a_where_variables.append(i_min_properties)

    if s_market_type == "state":
        a_cols.append("state")
        a_extra_cols = ", state"
        #s_where = f"{s_where} AND state != '_all_'"
        a_where.append("state !=")
        a_where_variables.append("_all_")
    elif s_market_type == "city":
        a_cols.extend(["state", "city"])
        a_extra_cols = ", state, city"
    elif s_market_type == "zipcode":
        a_cols.extend(["state", "zipcode"])
        a_extra_cols = ", state, zipcode"
    
    s_where = " AND ".join([f"{col} %s" for col in a_where])

    assert s_market_type in {"state", "city", "zipcode"}

    if i_bedrooms and s_property_type:
        s_from = f"{s_market_type}_ptype_bedrooms_new"
    elif i_bedrooms:
        s_from = f"{s_market_type}_bedrooms_new"
    else:
        s_from = f"{s_market_type}_info_new"
    
    if kwargs.get("area_properties"):
        a_cols.append("area_properties")
    
    if s_where.strip(): s_where = f"WHERE {s_where}"
    s_sql = f"""
        SELECT {','.join(a_cols)}
        FROM {s_from}
        {s_where}
        ;
    """
    if kwargs.get("b_debug"): print(s_sql)
    
    o_db = mysql_db.mySqlDbConn(
        s_host=mysql_db._host,
        s_schema="predictor",
        s_user=mysql_db._user,
        s_pass=mysql_db._password,)

    a_metric = [
        tuple(x if not isinstance(x, Decimal) else float(x) for x in t)
        #t
        for t in o_db.execute(s_sql, *a_where_variables, b_debug=True)
    ]
    
    if isinstance(e_results, dict):
       # e_results[(s_metric, s_market_type, i_bedrooms, s_property_type)]
        e_results[e_metric_lookup["idx"]] = a_metric, a_cols
    
    return a_metric, a_cols


def process_multiple_metrics_mp(a_metric_lookups, **kwargs):
    n_start = perf_counter()
    
    # create and start threads to process each metric request
    threads = []
    e_results = {}
    for idx, e_metric_lookup in enumerate(a_metric_lookups):
        if isinstance(e_metric_lookup.get("idx"), str) and re.search(r"^\d+$", e_metric_lookup["idx"]):
            e_metric_lookup["idx"] = int(e_metric_lookup["idx"])
        if not isinstance(e_metric_lookup.get("idx"), (int, float)):
            e_metric_lookup["idx"] = idx
        
        t = threading.Thread(target=get_metric_from_db, args=(e_metric_lookup, e_results), kwargs=kwargs)
        threads.append(t)
        t.start()
    
    # wait for the threads to complete
    for t in threads:
        t.join()
    
    if not e_results: return []
    
    for e_metric_lookup in a_metric_lookups:
        if e_metric_lookup["idx"] not in e_results: e_results[e_metric_lookup["idx"]] = {}
    
    #print("result keys", {i: True if v else False for i, v in e_results.items()})
    #print(e_results.keys())
    
    assert len(e_results) == len(a_metric_lookups)
    _, a_results = zip(*sorted(e_results.items(), key=lambda t:t[0]))
    
    n_end = perf_counter()
    #print(f'Finding metrics took {n_end- n_start :0.2f} second(s).')
    return a_results


#def shrink(i_area_properties, d_value, d_average):
#    d_conf = get_area_property_confidence(i_area_properties)
#    d_shrunk = (d_value * d_conf) + (d_average * (1 - d_conf))
#    return d_shrunk


def get_shrunk_metric_from_db(e_metric_lookup, **kwargs):
    s_metric = e_metric_lookup.get("metric", "revenue")
    s_market_type = e_metric_lookup.get("market_type", "state")
    i_bedrooms = e_metric_lookup.get("bedrooms", 0)
    s_property_type = e_metric_lookup.get("property_type", "")
    i_min_properties = e_metric_lookup.get("min_properties", 1)

    assert s_metric.lower() in db_tables.a_STATE_INFO_COLS
    if s_market_type == "state" or s_metric.lower() in c_NO_SHRINK:
        return get_metric_from_db(e_metric_lookup, **kwargs)
    else:
        e_state_lookup = e_metric_lookup.copy()
        e_state_lookup["market_type"] = "state"
        e_metric_lookup["idx"] = 0
        e_state_lookup["idx"] = 1
        
        kwargs["area_properties"] = True
        
        a_metric_results = process_multiple_metrics_mp([e_metric_lookup, e_state_lookup], **kwargs)
        assert len(a_metric_results) == 2
        assert len(a_metric_results[0]) == 2
        assert len(a_metric_results[1]) == 2
        (a_metric, a_cols), (a_state_metric, a_state_cols) = a_metric_results
        
        i_state_st_col = a_state_cols.index("state")
        i_state_mt_col = a_state_cols.index(s_metric)
        e_state_vals = {
            a_row[i_state_st_col]: a_row[i_state_mt_col]
            for a_row in a_state_metric
        }
        
        i_st_col = a_cols.index("state")
        i_ap_col = a_cols.index("area_properties")
        i_mt_col = a_cols.index(s_metric)
        a_shrunk = []
        for a_row in a_metric:
            a_row = list(a_row)
            a_row[i_mt_col] = round(
                scoring.shrink(a_row[i_ap_col], a_row[i_mt_col], e_state_vals.get(a_row[i_st_col], a_row[i_mt_col])),
                e_AREA_METRIC_PRECISION.get(s_metric.lower(), 4)
            )
            a_shrunk.append(a_row)
        
        return a_shrunk, a_cols
