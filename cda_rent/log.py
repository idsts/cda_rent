﻿import os
import logging
import traceback

try:
    from . import utils, paths
except:
    import utils, paths


print("Checking api log file")
#filename = os.path.splitext(__file__)[0]
#s_LOG_FILE = os.path.join(os.getcwd(), f'revenue_api.log')
s_LOG_FILE = os.path.join(paths.s_LOG_PATH, f'revenue_api.log')
print("s_LOG_FILE", s_LOG_FILE)
utils.check_log_file(s_LOG_FILE)

logging.basicConfig(filename=s_LOG_FILE, level=os.environ.get("LOGLEVEL", "INFO"))
LOGGER = logging.getLogger("IDSTS_CDA_API")