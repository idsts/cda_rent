﻿import re
import json
from decimal import Decimal
from datetime import datetime
import pandas as pd
import numpy as np

try:
    from . import utils, defaults, airbnb_standardization, zillow_standardization, interactive_exposed
except:
    import utils, defaults, airbnb_standardization, zillow_standardization, interactive_exposed


c_NULL = {"", 0, "0", "0.0", Decimal(0)}

c_EXTRA_OUTPUTS = {
    "-3_area_scale", "-2_area_scale", "-1_area_scale", "0_area_scale", "1_area_scale", "2_area_scale", "3_area_scale",
    "days_on_market", "insufficient_description", "manufactured_home", "starter_home", "potential_issues",
    "no_rentals", "move_home", "shared_ownership", "timeshare", "fixer_upper", "condemned", "auction",
    "property_type", "property_type_Apartment",
}

e_INPUT_RENAMES = {
    "allows_smoking": "smoking_allowed",
    "option_allows_smoking": "option_smoking_allowed",
}

e_PROPERTY_INPUTS = json.load(open(utils.get_local_path("example_property_input.json"), "r"))

a_SECTIONS = [
    ('options', "option_"),
    ('conditions', "condition_"),
    ('amenities', "amenity_"),
    ('features', "feature_"),
    ('locations', "location_"),
    ('financing', "financing_"),
]
i_CURRENT_YEAR = datetime.utcnow().year


# required information in order to make any prediction
e_REQUIRED_INPUTS_DTYPES = {
    #"property_type": zillow_standardization.e_ZILLOW_PROP_TYPE_STD, #str,  #House, Rancher, Chateau, Cottage, Cabin, Estate, Townhouse, Farmhouse, Condo, Apartment, Duplex
    "property_type": [
        s_pt.lower() for s_pt in zillow_standardization.e_ZILLOW_PROPERTY_TYPES
    ] + [
        s_ver.lower() for s_pt, a_vars in zillow_standardization.e_ZILLOW_PROPERTY_TYPES.items() for s_ver in a_vars
    ],
    #str,  #House, Rancher, Chateau, Cottage, Cabin, Estate, Townhouse, Farmhouse, Condo, Apartment, Duplex

    "bedrooms": int, # Integer of number of bedrooms. 1+
    "bathrooms": float, # Integer of number of bathrooms. 0+
    
    
    #"latitude": float,  # Floating point latitude coordinate
    #"longitude": float,  # Floating point latitude coordinate
}

e_OPTIONAL_INPUTS_DTYPES = {
    "rental_service": ["airbnb", "homeaway"],
    
    "price": float, # Floating point of the sale price or assessed value or estimated value (not taking condition into account)
    "living_area": float, # Floating point area of living space in square feet,
    "lot_size": float, # Floating point area of property size in acres,
    "year_built": int, # Floating point year of building,
    "elevation": float,
    
    "owner_properties": int,  # 1,  # how many properties the owner has. May help to distinguish between performance of investor and casual owners. Default is 100 for max prediction.
    "availability": float,  # 0.95,  # Floating point percentage of year that property will be availalable. Default is 0.95 since that rate has the highest average actual revenue.
    "overall_rating": int,  # 100,  # Integer expected review rating of owner/property. Default is 100.
    "response_rate": int,  # 100, # Integer expected responsiveness of owner to prospective renters. Default is 100.
    "number_of_photos": int,  # 75,  # Integer number of photos that will be taken on site. Fewer photos will decrease potential revenue.
    "number_of_reviews": int,  # 75,  # Integer number of expected user reviews. Fewer reviews will decrease potential revenue.
    "max_guests": int,  # 3, # Integer maximum number of guests. Usually >= number of bedrooms depending on things like shared rooms, pullout sofas, etc. Will default to average max based on number of bedrooms.
    "levels": int,  # 1,  # Integer number of stories in the building
    "pets_allowed": bool,
    "smoking_allowed": bool,
    
    "days_on_market": int,
    "property_condition": float,
    "fixer_upper": bool,
    "guesthouse": bool,
    "manufactured_home": bool,
    "new_construction": bool,
    "potential_issues": bool,
    "renovated": bool,
    "rent_control": bool,
    "starter_home": bool,
    "timeshare": bool,
    "insufficient_description": float,
}

e_FINANCING_INPUTS_DTYPES = {
    "price": float, # the price being financed. Should be market value unless the house is an auction/foreclosure/family-deal/etc
    "principal": float, # the amount still due on the loan after the down-payment
    "down_payment": float, # the down payment amount or percentage
    "apr": float, # the annual percentage rate of the loan
    "loan_years": float, # the number of years of the loan term
    "property_tax_rate": float, # the total property tax rate for the locality of the property
    "state": str, # the two letter state code if property_tax_rate isn't present
}


e_INFO_INPUTS_DEFAULTS = {    
    "rental_service": "airbnb", #"airbnb" or "homeaway",
    "owner_properties": 1,  # how many properties the owner has. May help to distinguish between performance of investor and casual owners. Default is 100 for max prediction.
    "availability": defaults.d_PREDICT_AVAILABILITY,  # Floating point percentage of year that property will be availalable. Default is 0.94 since that rate has the highest average actual revenue.
    "overall_rating": 100,  # Integer expected review rating of owner/property. Default is 100.
    "response_rate": 100, # Integer expected responsiveness of owner to prospective renters. Default is 100.
    "number_of_photos": 75,  # Integer number of photos that will be taken on site. Fewer photos will decrease potential revenue.
    "number_of_reviews": 75,  # Integer number of expected user reviews. Fewer reviews will decrease potential revenue.
    #"max_guests": 3, # Integer maximum number of guests. Usually >= number of bedrooms depending on things like shared rooms, pullout sofas, etc. Will default to average max based on number of bedrooms.
    "levels": 1,  # Integer number of stories in the building
    "availability_seasonality": defaults.d_PREDICT_AVAILABILITY_SEASONALITY,
    "availability_season_length": 12,
    "availability_season_peak_month": -1,
    "availability_season_type_Month": 0,
    "availability_season_type_Quarter": 0,
    "records": 6,
    #'reserved_days': 100,
    #'available_days': 243,
    #'blocked_days': 22,
    #'days': 182,
    'smoking_allowed': False,
    "property_condition": defaults.d_PREDICT_DEFAULT_PROPERTY_CONDITION,
}


def to_float(s_number):
    if s_number is None:
        return np.nan
    elif isinstance(s_number, (float, int, np.int32, np.float32, np.int64, np.float64)):
        return float(s_number)
    return utils.to_float(re.sub(r"[^-\d,/\.%]", "", str(s_number)))


def to_int(s_number):
    if s_number is None:
        return 0
    elif isinstance(s_number, (int, np.int32,np.int64)):
        return int(s_number)
    elif isinstance(s_number, (float, np.float32, np.float64)):
        return int(round(s_number))
    
    return int(round(utils.to_float(re.sub(r"[^-\d,/\.%]", "", s_number))))


def stack_input_dict(e_raw_input):
    for s_old_name, s_new_name in e_INPUT_RENAMES.items():
        if s_old_name in e_raw_input and s_new_name not in e_raw_input:
            e_raw_input[s_new_name] = e_raw_input[s_old_name]
            del e_raw_input[s_old_name]
    
    for k, v in e_raw_input.items():
        if isinstance(v, dict):
            for s_old_name, s_new_name in e_INPUT_RENAMES.items():
                if s_old_name in v and s_new_name not in v:
                    v[s_new_name] = v[s_old_name]
                    del v[s_old_name]
    
    e_input = {}
    c_used = set()
    for s_section, s_sect_prefix in a_SECTIONS:
        e_input[s_section] = {
            e_INPUT_RENAMES.get(k, k): v
            for k, v in e_raw_input.items()
            if e_INPUT_RENAMES.get(k, k) in e_PROPERTY_INPUTS.get(s_section, {})}
        c_used.update(e_input[s_section].keys())
        
        for k, v in e_raw_input.items():
            if k.startswith(s_sect_prefix):
                s_col = k.replace(s_sect_prefix, "")
                e_input[s_section][s_col] = v
                c_used.add(k)
    
    e_input.update({
            k: v for k, v in e_raw_input.items() if k not in c_used})
    return e_input


def check_required_input(e_input):
    if "latitude" in e_input:
        if isinstance(e_input["latitude"], str):
            if e_input["latitude"].strip() == "":
                del e_input["latitude"]
    #        else:
    #            e_input["latitude"] = utils.to_float(e_input["latitude"])
    
    if "longitude" in e_input:
        if isinstance(e_input["longitude"], str):
            if e_input["longitude"].strip() == "":
                del e_input["longitude"]
    #        else:
    #            e_input["longitude"] = utils.to_float(e_input["longitude"])
    
    if e_input.get("property_type") in {None, False, 0, "", "None", "Null", "False", "none", "null", "false", "0"}:
        e_input["property_type"] = "house"
    
    e_missing_req = {}
    for s_col, dtype in e_REQUIRED_INPUTS_DTYPES.items():
        if s_col not in e_input:
            if dtype == str:
                e_missing_req[s_col] = f"Missing required input \"{s_col}\". Should be a string/text value."
            elif dtype == int:
                e_missing_req[s_col] = f"Missing required input \"{s_col}\". Should be an integer value."
            elif dtype == float:
                e_missing_req[s_col] = f"Missing required input \"{s_col}\". Should be a floating point value."
            elif isinstance(dtype, (list, set, dict)):
                s_types = ', '.join([f'"{x}"' for x in dtype.keys()]) if isinstance(dtype, dict) else ', '.join([f'"{x}"' for x in dtype])
                e_missing_req[s_col] = f"Missing required input \"{s_col}\". Should be one of these values: [{s_types}]"
            else:
                e_missing_req[s_col] = f"Missing required input \"{s_col}\"."
        
        elif isinstance(dtype, (list, set, dict)) and (e_input[s_col] not in dtype and str(e_input[s_col]).lower() not in dtype and str(e_input[s_col]).title() not in dtype):
            s_types = ', '.join([f'"{x}"' for x in dtype.keys()]) if isinstance(dtype, dict) else ', '.join([f'"{x}"' for x in dtype])
            e_missing_req[s_col] = f"Invalid value type for required input \"{s_col}\". Should be one of these values: [{s_types}]. {e_input.get(s_col)}"
        elif dtype == str and not e_input.get(s_col):
            e_missing_req[s_col] = f"Missing required input \"{s_col}\"."
        elif dtype == str and not isinstance(e_input.get(s_col), str):
            e_missing_req[s_col] = f"Invalid value type for required input \"{s_col}\". Should be a string/text value. {e_input.get(s_col)}"
        elif (dtype == int or dtype == float) and not isinstance(e_input.get(s_col), (int, float)):
            if e_input[s_col] is None:
                e_missing_req[s_col] = f"Invalid value type for required input \"{s_col}\". Should be a numeric {dtype.__name__} value. {e_input.get(s_col)}"
            elif e_input[s_col] not in {0, "0"} and to_float(e_input[s_col]) == 0:
                e_missing_req[s_col] = f"Invalid value type for required input \"{s_col}\". Should be a numeric {dtype.__name__} value. {e_input.get(s_col)}"
    
    if not ("latitude" in e_input and "longitude" in e_input) and not "address" in e_input:
            e_missing_req["location"] = f"Missing required location input of either \"address\" (string/text value) or both \"latitude\" and \"longitude\" (floating point values)."    
    
    if e_input.get("latitude") and not isinstance(e_input.get("latitude"), (int, float)):
        e_missing_req["latitude"] = f"Invalid value type for required input \"latitude\". Should be a numeric floating point value."
    if e_input.get("longitude") and not isinstance(e_input.get("longitude"), (int, float)):
        e_missing_req["longitude"] = f"Invalid value type for required input \"longitude\". Should be a numeric floating point value."
    if e_input.get("address") and not isinstance(e_input.get("address"), str):
        e_missing_req["address"] = f"Invalid value type for required input \"address\". Should be a string/text value."
            
    return e_missing_req


def check_optional_input(e_input):
    #e_section = e_input.get(s_section, {})
    #e_section.update({k: v for k, v in e_input.items() if k.startswith(col_prefix)})
    
    e_section = e_input.get("options", {})
    e_section.update({
        k: v for k, v in e_input.items()
        if (v is not None and v != "") and
        (k.startswith("options_")
        or k in e_OPTIONAL_INPUTS_DTYPES
        or k in interactive_exposed.e_EXPOSED_OPTIONS 
        or k in e_INFO_INPUTS_DEFAULTS)
    })
    
    e_valid_optional, e_invalid_optional = {}, {}
    for s_col, dtype in e_OPTIONAL_INPUTS_DTYPES.items():
        if s_col not in e_section or pd.isnull(e_section[s_col]):
            continue
        elif isinstance(dtype, (list, set, dict)) and (e_section[s_col] not in dtype and str(e_section[s_col]).lower() not in dtype and str(e_section[s_col]).title() not in dtype):
            s_types = ', '.join([f'"{x}"' for x in dtype.keys()]) if isinstance(dtype, dict) else ', '.join([f'"{x}"' for x in dtype])
            e_invalid_optional[s_col] = f"Invalid value type for optional input \"{s_col}\". Should be missing, null, or one of these values: [{s_types}]. '{e_section.get(s_col)}'."
        elif dtype == str and not isinstance(e_section.get(s_col), str):
            e_invalid_optional[s_col] = f"Invalid value type for optional input \"{s_col}\". Should be missing, null or a string/text value. '{e_section.get(s_col)}'."
        elif (dtype == int or dtype == float) and not isinstance(e_section.get(s_col), (int, float)):
            if e_section[s_col] in {"", None}:
                del e_section[s_col]
            elif e_section[s_col] in {"0", "0.0"}:
                e_valid_optional[s_col] = 0.0
            else:
                v_val = to_int(e_section[s_col]) if dtype == int else to_float(e_section[s_col])
                if v_val:
                    e_valid_optional[s_col] = v_val
                else:
                    e_invalid_optional[s_col] = f"Invalid value type for optional input \"{s_col}\". Should be a numeric {dtype.__name__} value. '{e_section.get(s_col)}'."
                    
        elif dtype == bool and not isinstance(e_section.get(s_col), bool):
            if e_section[s_col] in {"", None}:
                del e_section[s_col]
            elif e_section[s_col] in {"true", "True", "TRUE", "1", "1.0", "yes", "Yes", "YES"}:
                e_valid_optional[s_col] = 1
            elif e_section[s_col] in {"false", "false", "FALSE", "0", "0.0", "no", "No", "NO"}:
                e_valid_optional[s_col] = 0
            else:
                e_invalid_optional[s_col] = f"Invalid value type for optional input \"{s_col}\". Should be a true/false {dtype.__name__} value. '{e_section.get(s_col)}'."
        else:
            e_valid_optional[s_col] = e_section[s_col]
    
    return e_valid_optional, e_invalid_optional


def check_financing_input(e_input):
    
    e_section = e_input.get("financing", {})
    e_section.update({
        k: v for k, v in e_input.items()
        if (v is not None and v != "") and
        (k.startswith("financing_")
        or (k in e_FINANCING_INPUTS_DTYPES and k not in e_section))
    })
    
    e_valid_financing, e_invalid_financing = {}, {}
    for s_col, dtype in e_FINANCING_INPUTS_DTYPES.items():
        if s_col not in e_section or pd.isnull(e_section[s_col]):
            continue
        elif isinstance(dtype, (list, set, dict)) and (e_section[s_col] not in dtype and str(e_section[s_col]).lower() not in dtype and str(e_section[s_col]).title() not in dtype):
            s_types = ', '.join([f'"{x}"' for x in dtype.keys()]) if isinstance(dtype, dict) else ', '.join([f'"{x}"' for x in dtype])
            e_invalid_financing[s_col] = f"Invalid value type for financing input \"{s_col}\". Should be missing, null, or one of these values: [{s_types}]"
        elif dtype == str and not isinstance(e_section.get(s_col), str):
            e_invalid_financing[s_col] = f"Invalid value type for financing input \"{s_col}\". Should be missing, null or a string/text value. '{e_section.get(s_col)}'."
        elif (dtype == int or dtype == float) and not isinstance(e_section.get(s_col), (int, float)):
            if e_section[s_col] in {"", None}:
                del e_section[s_col]
            elif e_section[s_col] in {"0", "0.0"}:
                e_valid_financing[s_col] = 0.0
            else:
                v_val = to_int(e_section[s_col]) if dtype == int else to_float(e_section[s_col])
                if v_val:
                    e_valid_financing[s_col] = v_val
                else:
                    e_invalid_financing[s_col] = f"Invalid value type for financing input \"{s_col}\". Should be a numeric {dtype.__name__} value. '{e_section.get(s_col)}'."
                    
        elif dtype == bool and not isinstance(e_section.get(s_col), bool):
            if e_section[s_col] in {"", None}:
                del e_section[s_col]
            elif e_section[s_col] in {"true", "True", "TRUE", "1", "1.0", "yes", "Yes", "YES"}:
                e_valid_financing[s_col] = 1
            elif e_section[s_col] in {"false", "false", "FALSE", "0", "0.0", "no", "No", "NO"}:
                e_valid_financing[s_col] = 0
            else:
                e_invalid_financing[s_col] = f"Invalid value type for financing input \"{s_col}\". Should be a true/false {dtype.__name__} value. '{e_section.get(s_col)}'."
        else:
            e_valid_financing[s_col] = e_section[s_col]
    
    return e_valid_financing, e_invalid_financing


def check_feature_input(e_input, s_section="features", section_prefix="feature_", col_prefix="feature_"):
    e_section = e_input.get(s_section, {})
    e_section.update({re.sub("^{section_prefix}", "", k): v for k, v in e_input.items() if k.startswith(section_prefix)})
    
    e_valid_features, e_invalid_features = {}, {}
    
    for s_col, v_value in e_section.items():
        s_input_col = s_col if s_col.startswith(col_prefix) else f"{col_prefix}{s_col}"
    
        if isinstance(v_value, bool):
            e_valid_features[s_input_col] = int(v_value)
        elif isinstance(v_value, (list, tuple)):
            print(f"Invalid input format for {s_input_col}: '{v_value}'")
            e_invalid_features[s_input_col] = f"Invalid value type for {s_section} input \"{s_col}\". Should be a boolean True/False value. '{v_value}'."
        elif v_value in {True, 1, "1", "true", "TRUE", "True", "yes", "YES", "Yes", "on", "ON", "On", "present", "PRESENT", "Present"}:
            e_valid_features[s_input_col] = 1
        elif v_value in {False, 0, -1, "0", "-1", "false", "FALSE", "False", "no", "NO", "No", "off", "OFF", "Off", "absent", "ABSENT", "Absent"}:
            e_valid_features[s_input_col] = 0
        elif v_value in {None, "", "na", "NA", "n/a", "N/A", "unknown", "UNKNOWN", "Unknown", "blank", "BLANK", "Blank"}:
            pass
        else:
            e_invalid_features[s_input_col] = f"Invalid value type for {s_section} input \"{s_col}\". Should be a boolean True/False value. '{v_value}'."
    
    return e_valid_features, e_invalid_features


def add_optional_defaults(e_input, b_fill_missing=True, **kwargs):
    if b_fill_missing:
        e_input.update({k: v for k, v in e_INFO_INPUTS_DEFAULTS.items() if pd.isnull(e_input.get(k))})
    else:
        e_input.update(e_INFO_INPUTS_DEFAULTS)

    return e_input


def standardize_input(e_input, *e_valid_sections):
    e_std = {
        "property_type": zillow_standardization.e_ZILLOW_PROP_TYPE_STD.get(str(e_input["property_type"]).lower()),
        "bedrooms": to_int(e_input["bedrooms"]),
        "bathrooms": to_float(e_input["bathrooms"]),
        #"latitude": float(e_input["latitude"]),
        #"longitude": float(e_input["longitude"]),
    }
    if re.search(r"(manufactured|mfr)", str(e_input["property_type"]).lower()):
        e_std["manufactured_home"] = True
    
    if not pd.isnull(e_input.get("latitude")):
        e_std["latitude"] = float(e_input["latitude"])
    if not pd.isnull(e_input.get("longitude")):
        e_std["longitude"] = float(e_input["longitude"])
    
    if not pd.isnull(e_input.get("price")) and e_input["price"] not in c_NULL:
        #print("input price", e_input["price"])
        e_std["price"] = to_float(e_input["price"])
    
    if not pd.isnull(e_input.get("living_area")) and e_input["living_area"] not in c_NULL:
        e_std["living_area"] = to_float(e_input["living_area"])
    if not pd.isnull(e_input.get("lot_size")) and e_input["lot_size"] not in c_NULL:
        e_std["lot_size"] = to_float(e_input["lot_size"])
    
    if not pd.isnull(e_input.get("year_built")) and e_input["year_built"] not in c_NULL:
        e_std["year_built"] = max(1492, min(i_CURRENT_YEAR, to_int(e_input["year_built"])))
        
    if not pd.isnull(e_input.get("max_guests")) and e_input["max_guests"] not in c_NULL:
        e_std["max_guests"] = to_int(e_input["max_guests"])
    
    e_std["property_category"] = airbnb_standardization.e_PROP_CAT_STD.get(str(e_std["property_type"]).lower(), e_std["property_type"])
    if "rental_service" in e_input:
        if str(e_input["rental_service"]) == "airbnb":
            e_std["airbnb"] = 1
            e_std["homeaway"] = 0
            e_std["vrbo"] = 0
        elif str(e_input["rental_service"]) == "homeaway":
            e_std["airbnb"] = 0
            e_std["homeaway"] = 1
            e_std["vrbo"] = 0
        elif str(e_input["rental_service"]) == "vrbo":
            e_std["airbnb"] = 0
            e_std["homeaway"] = 0
            e_std["vrbo"] = 1
    
    e_output = {}
    for e_valid in e_valid_sections:
        e_output.update(e_valid)
    e_output.update(e_std)
    
    for s_out, a_in in zillow_standardization.e_COMBINE.items():
        e_output[s_out] = 1 if e_output.get(s_out) or any([e_output.get(col, False) for col in a_in]) else 0
    
    for s_out, a_in in zillow_standardization.e_SUBSETS.items():
        e_output[s_out] = 1 if e_output.get(s_out) or any([e_output.get(col, False) for col in a_in]) else 0
    
    return e_output
