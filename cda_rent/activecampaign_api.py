﻿import re, os, sys
from time import time, sleep
from datetime import datetime, timedelta
import requests, json, urllib
import logging, traceback


s_TOKEN = os.environ.get("CDA_ACTIVE_CAMPAIGN_TOKEN", "eb24319ec50699449aa71c9ef048ce721a9ec417907ea4ef641a9373a7383e618af70f4a")
assert s_TOKEN

class ActiveCampaign:
    _base_url =  "https://revr.api-us1.com"
    _get_header = {
        "Content-type": "application/json",
        #"Authorization": f"Api-Token {s_TOKEN}",
        "Api-Token": f"{s_TOKEN}",
    }
    _post_header = {
        "Content-type": "application/json",
        "Accept": "application/json",
        #"Authorization": f"Api-Token {s_TOKEN}",
        "Api-Token": f"{s_TOKEN}",
    }
    
    _max_limit = 1000
    _batch_wait_seconds = 0.1
    _max_retries = 0  # how many times to retry a get/put/post
    _retry_delay = 30  # how many seconds to wait before retrying
    _throttle = .05
    
    _contacts_url = f"{_base_url}/api/3contacts"
    _notes_url = f"{_base_url}/api/3/notes"
    
    def __init__(self, token=s_TOKEN, **kwargs):
        if token:
            self._set_token(token)
        else:
            self._set_token(
                os.environ.get("CDA_ACTIVECAMPAIGN_TOKEN", s_TOKEN))
        
        self._last_request = time()
        
        print("ActiveCampaign Token:", self._token)
    
    ##### NOTES #####
    def create_note(self, note, subscriber=None, deal=None, activity=None, **kwargs):
        s_url = f"{self._notes_url}"
        assert note
        e_parameters = {}
        e_data = {
            "note": {
                "note": note,
            }
        }
        if subscriber is not None:
            e_data["note"]["reltype"] = "Subscriber"
            e_data["note"]["relid"] = subscriber
        elif subscriber is not None:
            e_data["note"]["reltype"] = "Deal"
            e_data["note"]["relid"] = deal
        elif activity is not None:
            e_data["note"]["reltype"] = "Activity"
            e_data["note"]["relid"] = activity
        
        assert e_data["note"].get("relid")
        
        #if b_created_by_me: e_parameters["my"] = 1
        return self._post_item(s_url, e_parameters, e_data=e_data, **kwargs)
    
    def get_note(self, note_id, **kwargs):
        assert note_id
        s_url = f"{self._notes_url}/{note_id}"
        e_parameters = {}
        return self._get_item(s_url, e_parameters, **kwargs)
    
    def delete_note(self, note_id, **kwargs):
        assert note_id
        s_url = f"{self._notes_url}/{note_id}"
        e_parameters = {}
        return self._delete_response(s_url, e_parameters, e_data=e_data, **kwargs)
    
    def update_note(self, note_id, note, subscriber=None, deal=None, activity=None, **kwargs):
        assert note_id
        s_url = f"{self._notes_url}/{note_id}"
        assert note
        e_parameters = {}
        e_data = {
            "note": {
                "note": note,
            }
        }
        if subscriber is not None:
            e_data["note"]["reltype"] = "Subscriber"
            e_data["note"]["relid"] = subscriber
        elif subscriber is not None:
            e_data["note"]["reltype"] = "Deal"
            e_data["note"]["relid"] = deal
        elif activity is not None:
            e_data["note"]["reltype"] = "Activity"
            e_data["note"]["relid"] = activity
        
        assert e_data["note"].get("relid")
        
        #if b_created_by_me: e_parameters["my"] = 1
        return self._post_item(s_url, e_parameters, e_data=e_data, **kwargs)
    
    
    ##### CONNECTION #####
    
    def _set_token(self, token):
        assert token
        self._token = token
        #self._get_header["Authorization"] = f"Api-Token {self._token}"
        #self._post_header["Authorization"] = f"Api-Token {self._token}"
        self._get_header["Api-Token"] = f"{self._token}"
        self._post_header["Api-Token"] = f"{self._token}"
    
    def _get_response(self, s_endpoint, e_parameters, **kwargs):
        
        #self.get_access_token(**kwargs)
        s_parameters = urllib.parse.urlencode(e_parameters)
        s_url = f"{s_endpoint}?{s_parameters}"
        
        if kwargs.get("b_debug"): print(s_url)
        
        e_header = self._get_header
        if kwargs.get("header"):
            e_header = e_header.copy()
            e_header.update(kwargs["header"])
        
        if time() - self._last_request < self._throttle:
            if kwargs.get("b_debug"):
                print("throttle", time() - self._last_request)
            sleep(self._throttle)
        response = requests.get(url=s_url, headers=e_header)
        self._last_request = time()
        
        i_remaining_retries = self._max_retries
        while i_remaining_retries > 0 and response.status_code in {401, 408, 409, 423, 425, 429, 500, 502, 503, 504}:
            print(f"Get error: {response.status_code} {response.text}\n Remaining retries: {i_remaining_retries}")
            sleep(self._retry_delay)
            if response.status_code == 401:
                print(f"Get error: {response.status_code} {response.text}\n Remaining retries: {i_remaining_retries}")
                #self.get_access_token(**kwargs)
                break
            elif response.status_code == 429:
                print(f"Get error: {response.status_code} {response.text}\n Remaining retries: {i_remaining_retries}")
                sleep(60)
            #elif response.status_code in {500, 502, 503, 504}:
            elif response.status_code in {502, 503, 504}:
                print(f"Get error: {response.status_code} {response.text}\n Remaining retries: {i_remaining_retries}")
                sleep(120)
            i_remaining_retries -= 1
            response = requests.get(url=s_url, headers=e_header)
            self._last_request = time()
        return response
    
    def _delete_response(self, s_endpoint, e_parameters, **kwargs):
        #self.get_access_token(**kwargs)
        s_parameters = urllib.parse.urlencode(e_parameters)
        s_url = f"{s_endpoint}?{s_parameters}"
        e_header = self._post_header
        if kwargs.get("header"):
            e_header = e_header.copy()
            e_header.update(kwargs["header"])
        
        if time() - self._last_request < self._throttle:
            if kwargs.get("b_debug"):
                print("throttle", time() - self._last_request)
            sleep(self._throttle)
        response = requests.delete(url=s_url, headers=e_header)
        self._last_request = time()
        
        i_remaining_retries = self._max_retries
        while i_remaining_retries > 0 and response.status_code in {401, 408, 409, 423, 425, 429, 500, 502, 503, 504}:
            print(f"Delete error: {response.status_code} {response.text}\n Remaining retries: {i_remaining_retries}")
            sleep(self._retry_delay)
            if response.status_code == 401:
                print(f"Get error: {response.status_code} {response.text}\n Remaining retries: {i_remaining_retries}")
                #self.get_access_token(**kwargs)
                break
            elif response.status_code == 429:
                print(f"Get error: {response.status_code} {response.text}\n Remaining retries: {i_remaining_retries}")
                sleep(60)
            elif response.status_code in {500, 502, 503, 504}:
                print(f"Get error: {response.status_code} {response.text}\n Remaining retries: {i_remaining_retries}")
                sleep(120)
            i_remaining_retries -= 1
            response = requests.delete(url=s_url, headers=e_header)
            self._last_request = time()
        return response
    
    def _post_response(self, s_endpoint, e_parameters, e_data, **kwargs):
        #self.get_access_token(**kwargs)
        s_parameters = urllib.parse.urlencode(e_parameters)
        s_url = f"{s_endpoint}?{s_parameters}"
        if kwargs.get("b_debug"): print(s_url)
        
        e_header = self._post_header
        if kwargs.get("header"):
            e_header = e_header.copy()
            e_header.update(kwargs["header"])
        
        if time() - self._last_request < self._throttle:
            if kwargs.get("b_debug"):
                print("throttle", time() - self._last_request)
            sleep(self._throttle)
        response = requests.post(url=s_url, json=e_data, headers=e_header)
        self._last_request = time()
        
        i_remaining_retries = self._max_retries
        while i_remaining_retries > 0 and response.status_code in {401, 408, 409, 423, 425, 429, 500, 502, 503, 504}:
            print(f"Post error: {response.status_code} {response.text}\n Remaining retries: {i_remaining_retries}")
            sleep(self._retry_delay)
            if response.status_code == 401:
                print(f"Get error: {response.status_code} {response.text}\n Remaining retries: {i_remaining_retries}")
                #self.get_access_token(**kwargs)
                break
            elif response.status_code == 429:
                print(f"Get error: {response.status_code} {response.text}\n Remaining retries: {i_remaining_retries}")
                sleep(60)
            elif response.status_code in {500, 502, 503, 504}:
                print(f"Get error: {response.status_code} {response.text}\n Remaining retries: {i_remaining_retries}")
                sleep(120)
            i_remaining_retries -= 1
            response = requests.post(url=s_url, json=e_data, headers=e_header)
            self._last_request = time()
        return response
    
    def _put_response(self, s_endpoint, e_parameters, e_data, **kwargs):
        #self.get_access_token(**kwargs)
        s_parameters = urllib.parse.urlencode(e_parameters)
        s_url = f"{s_endpoint}?{s_parameters}"
        if kwargs.get("b_debug"): print(s_url)
        
        e_header = self._post_header
        if kwargs.get("header"):
            e_header = e_header.copy()
            e_header.update(kwargs["header"])
        
        if time() - self._last_request < self._throttle:
            if kwargs.get("b_debug"):
                print("throttle", time() - self._last_request)
            sleep(self._throttle)
        response = requests.put(url=s_url, json=e_data, headers=e_header)
        self._last_request = time()
        
        i_remaining_retries = self._max_retries
        while i_remaining_retries > 0 and response.status_code in {401, 408, 409, 423, 425, 429, 500, 502, 503, 504}:
            print(f"Put error: {response.status_code} {response.text}\n Remaining retries: {i_remaining_retries}")
            sleep(self._retry_delay)
            if response.status_code == 401:
                print(f"Get error: {response.status_code} {response.text}\n Remaining retries: {i_remaining_retries}")
                #self.get_access_token(**kwargs)
                break
            elif response.status_code == 429:
                print(f"Get error: {response.status_code} {response.text}\n Remaining retries: {i_remaining_retries}")
                sleep(60)
            elif response.status_code in {500, 502, 503, 504}:
                print(f"Get error: {response.status_code} {response.text}\n Remaining retries: {i_remaining_retries}")
                sleep(120)
            i_remaining_retries -= 1
            response = requests.put(url=s_url, json=e_data, headers=e_header)
            self._last_request = time()
        
        return response
    
    def _get_all_items(self, s_url, e_parameters, **kwargs):
        a_items = []
        i_page, b_done = 0, False
        i_total = -1
        
        s_offset = kwargs.get("offset_key", "offset")
        s_limit = kwargs.get("limit_key", "limit")
        i_downloaded = 0
        
        if s_offset:
            e_parameters[s_offset] = kwargs.get(s_offset, 0)
            e_parameters[s_limit] = min(self._max_limit, kwargs.get(s_limit, self._max_limit))
            
            #i_downloaded = (e_parameters[s_offset] - 1) * e_parameters[s_limit]
            i_downloaded = e_parameters[s_offset]
            
        if kwargs.get("b_debug"): print("e_parameters", e_parameters)
        while not b_done:
            i_page += 1
            try:
                o_response = self._get_response(s_url, e_parameters, **kwargs)
                if not o_response or not o_response.ok:
                    if kwargs.get("break_on_bad_page"):
                        return o_response
                    break
                e_response = json.loads(o_response.text)
                e_data = e_response["data"] if isinstance(e_response, dict) and e_response.get("data") else e_response
                
                #return o_response, e_response
                
                if kwargs.get("b_debug"):
                    print(f"page {i_page:,} response", {k:v for k, v in e_data.items() if k != "items"})
                
                a_new_items = e_data.get("items", [])
                if a_new_items:
                    a_items.extend(a_new_items)
                
                if not a_new_items or e_parameters[s_offset] == e_data.get("offset"):
                    b_done = True
                else:
                    e_parameters[s_offset] = e_data.get("offset", 0)
                
                if i_total == -1: # and kwargs.get("b_debug"):
                    i_total = e_data.get("total", -1)
                    print(f"{i_total - i_downloaded:,} items to download from {s_url} ({i_downloaded:,} before current page)")
                #if kwargs.get("b_debug"):
                print(f"{len(a_items):,} items downloaded")
                
                if len(a_items) >= e_data.get("i_total", -1):
                    b_done = True
                if len(a_items) >= kwargs.get("max_results", 9999999):
                    b_done = True
                if i_page >= kwargs.get("max_pages", 99999):
                    b_done = True
                
                if not b_done:
                    if kwargs.get("b_debug"): print("Throttle with batch_wait_seconds") #, time() - self._last_request)
                    sleep(self._batch_wait_seconds)
            
            except Exception as e:
                print(e, traceback.print_exc())
                b_done = True
            #if i_page % 10 == 0:
            #    if kwargs.get("b_debug"): print("i_page", i_page)
                
        else:
            o_response = self._get_response(s_url, e_parameters, **kwargs)
            if o_response and o_response.ok:
                #print("o_response.ok", o_response.ok, o_response.status_code)
                e_response = json.loads(o_response.text)
                #print(type(e_response), len(e_response))
                if isinstance(e_response, dict):
                    e_data = e_response["data"] if isinstance(e_response, dict) and e_response.get("data") else e_response
                    a_items = e_data.get("items", []) 
                elif isinstance(e_response, (list, tuple)):
                    a_items = list(e_response)
        return a_items
    
    def _get_item(self, s_url, e_parameters={}, **kwargs):
        o_response = None
        try:
            o_response = self._get_response(s_url, e_parameters, **kwargs)
            if not o_response or not o_response.ok: return o_response
            if o_response.text:
                e_item = json.loads(o_response.text)
                return e_item
            else:
                return True
        except:
            return o_response
    
    def _post_item(self, s_url, e_parameters={}, e_data={}, **kwargs):
        o_response = None
        try:
            o_response = self._post_response(s_url, e_parameters=e_parameters, e_data=e_data, **kwargs)
            if not o_response or not o_response.ok: return o_response
            if o_response.text:
                e_item = json.loads(o_response.text)
                return e_item
            else:
                return True
        except:
            return o_response
    
    def _put_item(self, s_url, e_parameters={}, e_data={}, **kwargs):
        o_response = None
        try:
            o_response = self._put_response(s_url, e_parameters=e_parameters, e_data=e_data, **kwargs)
            if not o_response or not o_response.ok: return o_response
            if o_response.text:
                e_item = json.loads(o_response.text)
                return e_item
            else:
                return True
        except:
            return o_response