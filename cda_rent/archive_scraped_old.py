import os, sys, re
from datetime import datetime, timedelta
import pytz
from time import time, sleep

try:
    from . import utils, docdb_utils
except:
    import utils, docdb_utils

i_MAX_SALE_AGE = 60
i_MAX_RENTAL_AGE = 60


def archive_old_sale_scrapings(i_days=i_MAX_SALE_AGE):
    o_DOC = docdb_utils.DocDBConnector(read_only=False)
    o_DOC._load()
    i_to_archive = o_DOC._db.properties.count_documents({
        "scraping_date": {"$lt": datetime.utcnow() - timedelta(days=i_days)}
    })
    print(f"There are {i_to_archive:,} sale properties more than {i_days:,} days old to archive to `properties_old`.")
    e_copied_results = docdb_utils.copy_between_collections(
        o_DOC._db.properties,
        o_DOC._db.properties_old,
        {
                "scraping_date": {"$lt": datetime.utcnow() - timedelta(days=i_days)}
        },
        i_copy_batch=1000,
        b_drop_id=False,
    )
    print(f"\tCopied old sale scrapings from the `properties` collection to the `properties_old` collection.")
    
    if not e_copied_results.get("failed"):
        o_DOC._db.properties.delete_many({
            "scraping_date": {"$lt": datetime.utcnow() - timedelta(days=i_days)},
        })
        print(f"\tDelete old sale scrapings from the `properties` collection.")
    
    o_DOC._close()

def archive_old_rental_scrapings(i_days=i_MAX_RENTAL_AGE):
    o_DOC = docdb_utils.DocDBConnector(read_only=False)
    o_DOC._load()
    i_to_archive = o_DOC._db.properties.count_documents({
        "scraping_date": {"$lt": datetime.utcnow() - timedelta(days=i_days)}
    })
    print(f"There are {i_to_archive:,} rental properties more than {i_days:,} days old to archive to `rentals_old`.")
    e_copied_results = docdb_utils.copy_between_collections(
        o_DOC._db.rentals,
        o_DOC._db.rentals_old,
        {
                "scraping_date": {"$lt": datetime.utcnow() - timedelta(days=i_days)}
        },
        i_copy_batch=1000,
        b_drop_id=False,
    )
    
    print(f"\tCopied old rentals scrapings from the `rentals` collection to the `rentals_old` collection.")
    
    if not e_copied_results.get("failed"):
        o_DOC._db.rentals.delete_many({
            "scraping_date": {"$lt": datetime.utcnow() - timedelta(days=i_days)},
        })
        print(f"\tDelete old rental scrapings from the `rentals` collection.")
    o_DOC._close()


if __name__ == "__main__":
    args, kwargs = utils.parse_command_line_args(sys.argv)
    print("Running archive_scraped_old.py from shell")
    
    sleep(2)
    archive_old_sale_scrapings(i_days=kwargs.get("max_sale_age", i_MAX_SALE_AGE))
    sleep(5)
    archive_old_rental_scrapings(i_days=kwargs.get("max_rental_age", i_MAX_RENTAL_AGE))
    sleep(10)
    
