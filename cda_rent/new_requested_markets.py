﻿import os, sys, re
import requests, json
from time import time, sleep
from datetime import datetime, timedelta

try:
    from . import utils, db_tables, area_data
except:
    import utils, db_tables, area_data

i_PROJ_ID = 623146
s_SPIDER = "zillow"

i_REQUEST_DAYS = 1
i_REQUEST_HOURS = 0
i_REQUEST_MINUTES = 0
i_SCRAPED_DAYS = 1
i_SCRAPED_HOURS = 0
i_SCRAPED_MINUTES = 0

i_SCAN_DELAY_MINUTES = 5

o_DB = None


def market_id_to_scrape_str(market_id):
    s_table, e_market = kwargs["e_market"] if kwargs.get("e_market") else area_data.market_id_to_name(market_id)
    if e_market is None:
        #e_market = {}
        return None
    s_str = "|".join([
        e_market.get("city", "0"),
        e_market.get("county", "0"),
        e_market.get("state", "0"),
        e_market.get("zipcode", "0"),
        str(market_id),
    ])
    return s_str


def call_scraper_for_market(market_id):
    s_location = market_id_to_scrape_str(market_id)
    if s_location:
        o_start_response = requests.post(
            "https://app.scrapinghub.com/api/run.json",
            data={
                "project": i_PROJ_ID,
                "spider": s_SPIDER,
                "location": s_location,
            },
            auth = requests.auth.HTTPBasicAuth(os.environ.get("CDA_SCRAPING_HUB_API_KEY", '3869aadc43e24c4996de2e45e7bac623'), '')
        )
        #print(o_start_response.status_code)
        if not o_start_response.ok:
            print(f"Scrapinghub job request failed for market_id {market_id}, location: '{s_location}'")
            return False
        
        e_job_start_response = o_start_response.json()
        
        if e_job_start_response.get("status") != "ok":
            print(f"Scrapinghub job request failed for market_id {market_id}, location: '{s_location}', status: {e_job_start_response.get('status')}")
            
        s_job_id = e_job_start_response.get("jobid")
        o_DB.update("revr.market", {"scrape_triggered": 1, "jobid": s_job_id}, {"id": market_id})
        return s_job_id
    else:
        print(f"Couldn't turn market_id {market_id} into a location string for scrapinghub")
        return False


def scan_for_new_markets(b_start_scrapes=True, **kwargs):
    n_start = time()
    global o_DB
    o_DB = db_tables.get_connection()
    
    s_last_request_range = (
        datetime.utcnow() - timedelta(days=kwargs.get("request_days", i_REQUEST_DAYS), hours=kwargs.get("request_hours", i_REQUEST_HOURS), minutes=kwargs.get("request_minutes", i_REQUEST_MINUTES))
    ).strftime("%Y-%m-%d %H:%M")
    s_last_day_range = (
        datetime.utcnow() - timedelta(days=kwargs.get("scraped_days", i_SCRAPED_DAYS), hours=kwargs.get("scraped_hours", i_SCRAPED_HOURS), minutes=kwargs.get("scraped_minutes", i_SCRAPED_MINUTES))
    ).strftime("%Y-%m-%d %H:%M")
    
    print(f"Start scanning for newly requested areas requested since {s_last_request_range}")
    df_recent_user_markets = o_DB.get_table_as_df(
        "requested_markets",
        s_schema="revr",
        where=f"scrape_triggered = 0 AND last_request >= DATE('{s_last_request_range}') AND (last_scraped_at is null or last_scraped_at < DATE('{s_last_day_range}'))",
        #b_debug=True
    )
    print(f"There are {len(df_recent_user_markets):,} new requested markets")
    
    if b_start_scrapes:
        for idx, row in df_recent_user_markets.iterrows():
            print(f"Calling scraper for {row['market']} market {row['market_id']}: {row['location']}, {row['state']}")
            s_job_id = call_scraper_for_market(row["market_id"])
            if s_job_id:
                print(f"\tscrape jobid: {s_job_id}")
                
    o_DB.close()
    o_DB = None
    print(f"Scanning for newly requested areas took: {utils.secondsToStr(time() - n_start)}")
    return df_recent_user_markets


def keep_scanning(**kwargs):
    i_scans, n_scan_start = 0, time()
    i_scan_delay_minutes = kwargs.get("i_scan_delay_minutes", i_SCAN_DELAY_MINUTES)
    
    while True:
        i_scans += 1
        scan_for_new_markets(**kwargs)
        print(f"pausing after new market scan {i_scans:,} for {i_scan_delay_minutes:,} minutes")
        sleep(max(10, i_scan_delay_minutes * 60))
        

if __name__ == "__main__":
    args, kwargs = utils.parse_command_line_args(sys.argv)
    print("Running new_requested_markets.py from shell")
    
    if kwargs.get("keep_scanning"):
        keep_scanning(**kwargs)
    else:
        scan_for_new_markets(**kwargs)
    