﻿import os, sys, re
from datetime import datetime, timedelta
from time import time
import traceback

try:
    from . import utils, docdb_utils, db_tables
except:
    import utils, docdb_utils, db_tables


o_DOCDB = None
o_DB = None
i_SALE_CURRENT_DAYS = 14
i_SALE_AVERAGE_DAYS = 60
i_SALE_ARCHIVE_DAYS = 365
i_RENT_CURRENT_DAYS = 14
i_RENT_AVERAGE_DAYS = 60
i_RENT_ARCHIVE_DAYS = 365

d_MAX_BEDROOMS = 8
d_MAX_BATHROOMS = 8
d_MIN_RENT_PRICE = 50
d_MAX_RENT_PRICE = 12_000
d_MIN_SALE_PRICE = 50_000
d_MAX_SALE_PRICE = 20_000_000

a_STATE_GROUPS = [
    ("state", ["$market_ids.state"]),
    ("state_ptype", ["$market_ids.state", "$revr_property_type"]),
    ("state_bedrooms", ["$market_ids.state", "$bedrooms"]),
    ("state_ptype_bedrooms", ["$market_ids.state", "$revr_property_type", "$bedrooms"],),
]
a_COUNTY_GROUPS = [
    ("county", ["$market_ids.county"]),
    ("county_ptype", ["$market_ids.county", "$revr_property_type"]),
    ("county_bedrooms", ["$market_ids.county", "$bedrooms"]),
    ("county_ptype_bedrooms", ["$market_ids.county", "$revr_property_type", "$bedrooms"]),
]
a_CITY_GROUPS = [
    ("city", ["$market_ids.city"]),
    ("city_ptype", ["$market_ids.city", "$revr_property_type"]),
    ("city_bedrooms", ["$market_ids.city", "$bedrooms"]),
    ("city_ptype_bedrooms", ["$market_ids.city", "$revr_property_type", "$bedrooms"]),
]
a_ZIPCODE_GROUPS = [
    ("zipcode", ["$market_ids.zipcode"]),
    ("zipcode_ptype", ["$market_ids.zipcode", "$revr_property_type"]),
    ("zipcode_bedrooms", ["$market_ids.zipcode", "$bedrooms"]),
    ("zipcode_ptype_bedrooms", ["$market_ids.zipcode", "$revr_property_type", "$bedrooms"]),
]
a_KEY_COLS =  ["market_id", "property_type", "bedrooms"]

a_SALE_UPDATE_COLS = [
    'for_sale_count',
    'for_sale_price',
    'for_sale_days_on_market',
    'for_sale_str_score',
    'for_sale_str_score_absolute',
    'for_sale_str_score_relative',
    'for_sale_str_score_area',
    "for_sale_str_revenue",
    'for_sale_str_cap',
    'for_sale_str_daily_rate',
    'for_sale_last_date',
    'for_sale_ltr_score',
    'for_sale_ltr_score_absolute',
    'for_sale_ltr_score_relative',
    'for_sale_ltr_score_family',
    'for_sale_ltr_score_area',
    'for_sale_ltr_score_family_area',
    'for_sale_ltr_revenue',
    'for_sale_ltr_cap'
]

a_RENT_UPDATE_COLS = [
    'for_rent_count',
    'for_rent_days_on_market',
    'for_rent_last_date',
    'for_rent_price',
    'for_rent_ltr_score',
    'for_rent_ltr_score_absolute',
    'for_rent_ltr_score_relative',
    'for_rent_ltr_score_family',
    'for_rent_ltr_score_area',
    'for_rent_ltr_score_family_area',
]


def sale_properties_turnover(s_group, i_days, a_group_cols, e_extra_filter, **kwargs):
    result = None
    
    dt_start = dt_now - timedelta(days=i_days)
    dt_end = dt_now
    
    z_old_removed = o_DOCDB._db.properties.aggregate([
        {
            "$match": {
                "scraping_date": {"$lte": dt_now - timedelta(days=i_days)},
                "days_on_zillow": {"$gt": i_days},
                "price": {"$gte": d_MIN_SALE_PRICE, "$lte": d_MAX_SALE_PRICE},
            }
        },
        {
            "$group": {
                "_id": ["$market_ids.city"],
                "count": {"$sum": 1},
                "price": {"$avg": "$price"},
            }
        }
    ])
    
    a_sale_groups = list(z_sale_groups)
    print(f"Extracting {len(a_sale_groups):,} sale turnover groups for {s_group} took: {utils.secondsToStr(time() - n_group_start)}")
    
    n_update_start = time()
    if a_group_cols[1:] == ["$revr_property_type", "$bedrooms"]:
        a_keys = [tuple(e_row["_id"]) for e_row in a_sale_groups]
    elif a_group_cols[1:] == ["$revr_property_type"]:
        a_keys = [tuple(e_row["_id"][:2] + [-1]) for e_row in a_sale_groups]
    elif a_group_cols[1:] == ["$bedrooms"]:
        a_keys = [tuple(e_row["_id"][:1] + ["_all_"] + e_row["_id"][-1:]) for e_row in a_sale_groups]
    else:
        a_keys = [tuple(e_row["_id"][:1] + ["_all_", -1]) for e_row in a_sale_groups]
    
    a_sale_updates = [
        (
            e_row.get("count", 0),
            round(e_row.get("price", 0), 2) if e_row.get("price", 0) else 0,
            round(e_row.get("days_on_zillow", 0), 2) if e_row.get("days_on_zillow", 0) else 0,
            round(e_row.get("revr_score_overall", 0), 8) if e_row.get("revr_score_overall", 0) else 0,
            round(e_row.get("revr_optimized_revenue", 0), 2) if e_row.get("revr_optimized_revenue", 0) else 0,
            round(e_row.get("revr_optimized_cap", 0), 8) if e_row.get("revr_optimized_cap", 0) else 0,
            round(e_row.get("revr_avg_daily_rate", 0), 2) if e_row.get("revr_avg_daily_rate", 0) else 0,
            e_row.get("last_listing", None),
            round(e_row.get("revr_ltr_score_overall", 0), 8) if e_row.get("revr_ltr_score_overall", 0) else 0,
            round(e_row.get("revr_long_term_rental_revenue", 0), 2) if e_row.get("revr_long_term_rental_revenue", 0) else 0,
            round(e_row.get("revr_long_term_cap", 0), 2) if e_row.get("revr_long_term_cap", 0) else 0,
        ) + t_key
        for e_row, t_key in zip(a_sale_groups, a_keys)]
    
    if a_sale_updates:
        o_db.reload()
        result = o_db.upsert_batch("revr.current_market_summaries", a_SALE_UPDATE_COLS, a_KEY_COLS, a_sale_updates, b_debug=False)
        print(f"\tUploading {len(a_sale_updates):,} sale information updates to {s_group} took: {utils.secondsToStr(time() - n_update_start)}")
    
    return result


def sale_properties_group_main(s_group, a_group_cols, e_extra_filter, o_db, o_docdb, **kwargs):
    n_group_start = time()
    result = None
    b_long_term = kwargs.get("b_long_term", False)
    b_archived = kwargs.get("b_archived", False)
    if b_archived:
        o_collection = o_docdb._db.properties_old
        i_recent_days = kwargs.get("sale_archive_days", i_SALE_ARCHIVE_DAYS)
    elif b_long_term:
        o_collection = o_docdb._db.properties
        i_recent_days = kwargs.get("sale_average_days", i_SALE_AVERAGE_DAYS)
    else:
        o_collection = o_docdb._db.properties
        i_recent_days = kwargs.get("sale_current_days", i_SALE_CURRENT_DAYS)
    
    z_sale_groups = o_collection.aggregate([
        {"$match": {"scraping_date": {"$gte": datetime.utcnow() - timedelta(days=i_recent_days)},}},
        {"$match": e_extra_filter},
        {
            "$project": {
                'market_ids': 1,
                "revr_property_type": 1,
                "bedrooms": 1,
                "bathrooms": 1,

                'scraping_date': 1,
                'days_on_zillow': 1,
                'price': 1,
                'revr_score_overall': 1,
                'revr_score_absolute': 1,
                'revr_score_relative': 1,
                'revr_score_area': 1,
                'revr_optimized_revenue': 1,
                'revr_avg_daily_rate': 1,
                'revr_optimized_cap': 1,
                'revr_long_term_cap': 1,
                'revr_long_term_rental_revenue': 1,
                'revr_ltr_score_overall': 1,
                'revr_ltr_score_absolute': 1,
                'revr_ltr_score_relative': 1,
                'revr_ltr_score_family': 1,
                'revr_ltr_score_area': 1,
                'revr_ltr_score_family_area': 1,
            }
        },
        {
            "$group": {
                "_id": a_group_cols,
                "count": {"$sum": 1},
                "days_on_zillow": {"$avg": "$days_on_zillow"},
                "first_listing": {"$min": "$scraping_date"},
                "last_listing": {"$max": "$scraping_date"},
                "price": {"$avg": "$price"},
                "revr_score_overall": {"$avg": "$revr_score_overall"},
                "revr_score_absolute": {"$avg": "$revr_score_absolute"},
                "revr_score_relative": {"$avg": "$revr_score_relative"},
                "revr_score_area": {"$avg": "$revr_score_area"},
                "revr_avg_daily_rate": {"$avg": "$revr_avg_daily_rate"},
                "revr_optimized_cap": {"$avg": "$revr_optimized_cap"},
                "revr_optimized_revenue": {"$avg": "$revr_optimized_revenue"},
                "revr_long_term_cap": {"$avg": "$revr_long_term_cap"},
                "revr_long_term_rental_revenue": {"$avg": "$revr_long_term_rental_revenue"},
                "revr_ltr_score_overall": {"$avg": "$revr_ltr_score_overall"},
                "revr_ltr_score_absolute": {"$avg": "$revr_ltr_score_absolute"},
                "revr_ltr_score_relative": {"$avg": "$revr_ltr_score_relative"},
                "revr_ltr_score_family": {"$avg": "$revr_ltr_score_family"},
                "revr_ltr_score_area": {"$avg": "$revr_ltr_score_area"},
                "revr_ltr_score_family_area": {"$avg": "$revr_ltr_score_family_area"},
            }
        }
    ])
    a_sale_groups = list(z_sale_groups)
    print(f"Extracting {len(a_sale_groups):,} sale information groups for {s_group} took: {utils.secondsToStr(time() - n_group_start)}")
    
    n_update_start = time()
    if a_group_cols[1:] == ["$revr_property_type", "$bedrooms"]:
        a_keys = [tuple(e_row["_id"]) for e_row in a_sale_groups]
    elif a_group_cols[1:] == ["$revr_property_type"]:
        a_keys = [tuple(e_row["_id"][:2] + [-1]) for e_row in a_sale_groups]
    elif a_group_cols[1:] == ["$bedrooms"]:
        a_keys = [tuple(e_row["_id"][:1] + ["_all_"] + e_row["_id"][-1:]) for e_row in a_sale_groups]
    else:
        a_keys = [tuple(e_row["_id"][:1] + ["_all_", -1]) for e_row in a_sale_groups]
    
    a_sale_updates = [
        (
            e_row.get("count", 0),
            round(e_row.get("price", 0), 2) if e_row.get("price", 0) else 0,
            round(e_row.get("days_on_zillow", 0), 2) if e_row.get("days_on_zillow", 0) else 0,
            round(e_row.get("revr_score_overall", 0), 8) if e_row.get("revr_score_overall", 0) else 0,
            round(e_row.get("revr_score_absolute", 0), 8) if e_row.get("revr_score_absolute", 0) else 0,
            round(e_row.get("revr_score_relative", 0), 8) if e_row.get("revr_score_relative", 0) else 0,
            round(e_row.get("revr_score_area", 0), 8) if e_row.get("revr_score_area", 0) else 0,
            round(e_row.get("revr_optimized_revenue", 0), 2) if e_row.get("revr_optimized_revenue", 0) else 0,
            round(e_row.get("revr_optimized_cap", 0), 8) if e_row.get("revr_optimized_cap", 0) else 0,
            round(e_row.get("revr_avg_daily_rate", 0), 2) if e_row.get("revr_avg_daily_rate", 0) else 0,
            e_row.get("last_listing", None),
            round(e_row.get("revr_ltr_score_overall", 0), 8) if e_row.get("revr_ltr_score_overall", 0) else 0,
            round(e_row.get("revr_ltr_score_absolute", 0), 8) if e_row.get("revr_ltr_score_absolute", 0) else 0,
            round(e_row.get("revr_ltr_score_relative", 0), 8) if e_row.get("revr_ltr_score_relative", 0) else 0,
            round(e_row.get("revr_ltr_score_family", 0), 8) if e_row.get("revr_ltr_score_family", 0) else 0,
            round(e_row.get("revr_ltr_score_area", 0), 8) if e_row.get("revr_ltr_score_area", 0) else 0,
            round(e_row.get("revr_ltr_score_family_area", 0), 8) if e_row.get("revr_ltr_score_family_area", 0) else 0,
            round(e_row.get("revr_long_term_rental_revenue", 0), 2) if e_row.get("revr_long_term_rental_revenue", 0) else 0,
            round(e_row.get("revr_long_term_cap", 0), 2) if e_row.get("revr_long_term_cap", 0) else 0,
        ) + t_key
        for e_row, t_key in zip(a_sale_groups, a_keys)]
    
    if a_sale_updates:
        o_db.reload()
        if b_archived:
            o_db.execute(r"""UPDATE revr.archive_market_summaries SET for_sale_count = 0 WHERE for_sale_count IS NULL or for_sale_count > 0;""", commit=True)
            result = o_db.upsert_batch("revr.archive_market_summaries", a_SALE_UPDATE_COLS, a_KEY_COLS, a_sale_updates, b_debug=False)
            print(f"\tUploading {len(a_sale_updates):,} sale archive updates for {s_group} to revr.archive_market_summaries took: {utils.secondsToStr(time() - n_update_start)}")
        elif b_long_term:
            o_db.execute(r"""UPDATE revr.average_market_summaries SET for_sale_count = 0 WHERE for_sale_count IS NULL or for_sale_count > 0;""", commit=True)
            result = o_db.upsert_batch("revr.average_market_summaries", a_SALE_UPDATE_COLS, a_KEY_COLS, a_sale_updates, b_debug=False)
            print(f"\tUploading {len(a_sale_updates):,} sale average updates for {s_group} to revr.average_market_summaries  took: {utils.secondsToStr(time() - n_update_start)}")
        else:
            o_db.execute(r"""UPDATE revr.current_market_summaries SET for_sale_count = 0 WHERE for_sale_count IS NULL or for_sale_count > 0;""", commit=True)
            result = o_db.upsert_batch("revr.current_market_summaries", a_SALE_UPDATE_COLS, a_KEY_COLS, a_sale_updates, b_debug=False)
            print(f"\tUploading {len(a_sale_updates):,} sale current updates for {s_group} to revr.current_market_summaries  took: {utils.secondsToStr(time() - n_update_start)}")
    
    return result


def update_sale_summaries(**kwargs):
    global o_DOCDB, o_DB
    if kwargs.get("o_docdb"):
        o_docdb = kwargs["o_docdb"]
    else:
        if o_DOCDB is None: o_DOCDB = docdb_utils.DocDBConnector(read_only=True)
        o_docdb = o_DOCDB
    o_docdb._load()
    
    if kwargs.get("o_db"):
        o_db = kwargs["o_db"]
    else:
        if o_DB is None: o_DB = db_tables.get_connection()
        o_db = o_DB
    
    try:
        a_sale_groups = []
        if not kwargs.get("skip_state"): a_sale_groups.extend(a_STATE_GROUPS)
        if not kwargs.get("skip_county"): a_sale_groups.extend(a_COUNTY_GROUPS)
        if not kwargs.get("skip_city"): a_sale_groups.extend(a_CITY_GROUPS)
        if not kwargs.get("skip_zipcode"): a_sale_groups.extend(a_ZIPCODE_GROUPS)
        
        print("Start processing sale information")
        
        for s_group, a_group_cols in a_sale_groups:
            e_extra_filter = {
                "price": {"$gte": d_MIN_SALE_PRICE, "$lte": d_MAX_SALE_PRICE},
            }
            if a_group_cols[1:] == ["$revr_property_type", "$bedrooms"]:
                e_extra_filter.update({
                    "revr_property_type": {"$ne": ""},
                    "bedrooms": {"$gte": 0, "$lte": d_MAX_BEDROOMS},
                })
            elif a_group_cols[1:] == ["$revr_property_type",]:
                e_extra_filter.update({
                    "revr_property_type": {"$ne": ""},
                })
            elif a_group_cols[1:] == ["$bedrooms",]:
                e_extra_filter.update({
                    "bedrooms": {"$gte": 0, "$lte": d_MAX_BEDROOMS},
                })
            
            if kwargs.get("current", True):
                sale_main_current_result  = sale_properties_group_main(s_group, a_group_cols, e_extra_filter, b_long_term=False, o_db=o_db, o_docdb=o_docdb, **kwargs)
            if kwargs.get("average", True):
                sale_main_average_result  = sale_properties_group_main(s_group, a_group_cols, e_extra_filter, b_long_term=True, o_db=o_db, o_docdb=o_docdb, **kwargs)
            if kwargs.get("archive"):
                sale_main_archive_result  = sale_properties_group_main(s_group, a_group_cols, e_extra_filter, b_archived=True, o_db=o_db, o_docdb=o_docdb, **kwargs)
    
    except Exception as e:
            s_trace = ''.join(traceback.format_exception(None, e, e.__traceback__))
            print(f"Failed on update_sale_summaries")
            print(s_trace)
    
    o_docdb._close()
    o_db.close()
    

def rent_properties_group_main(s_group, a_group_cols, e_extra_filter, o_db, o_docdb, **kwargs):
    n_group_start = time()
    result = None
    b_long_term = kwargs.get("b_long_term", False)
    b_archived = kwargs.get("b_archived", False)
    if b_archived:
        o_collection = o_docdb._db.rentals_old
        i_recent_days = kwargs.get("rent_archive_days", i_RENT_ARCHIVE_DAYS)
    elif b_long_term:
        o_collection = o_docdb._db.rentals
        i_recent_days = kwargs.get("rent_average_days", i_RENT_AVERAGE_DAYS)
    else:
        o_collection = o_docdb._db.rentals
        i_recent_days = kwargs.get("rent_current_days", i_RENT_CURRENT_DAYS)
    
    z_current_rental_groups = o_collection.aggregate([
        {"$match": {"scraping_date": {"$gte": datetime.utcnow() - timedelta(days=i_recent_days)}}},
        {"$match": e_extra_filter},
        {
            "$project": {
                'state': 1,
                'county': 1,
                'city': 1,
                'zip': 1,
                'market_ids': 1,
                "revr_property_type": 1,
                "bedrooms": 1,

                'scraping_date': 1,
                'days_on_zillow': 1,
                'price': 1,
                'revr_ltr_score_overall': 1,
                'revr_ltr_score_absolute': 1,
                'revr_ltr_score_relative': 1,
                'revr_ltr_score_family': 1,
                'revr_ltr_score_area': 1,
                'revr_ltr_score_family_area': 1,
            }
        },
        {
            "$group": {
                "_id": a_group_cols,
                "count": {"$sum": 1},
                "days_on_zillow": {"$avg": "$days_on_zillow"},
                "first_listing": {"$min": "$scraping_date"},
                "last_listing": {"$max": "$scraping_date"},
                "price": {"$avg": "$price"},
                "revr_ltr_score_overall": {"$avg": "$revr_ltr_score_overall"},
                "revr_ltr_score_absolute": {"$avg": "$revr_ltr_score_absolute"},
                "revr_ltr_score_relative": {"$avg": "$revr_ltr_score_relative"},
                "revr_ltr_score_family": {"$avg": "$revr_ltr_score_family"},
                "revr_ltr_score_area": {"$avg": "$revr_ltr_score_area"},
                "revr_ltr_score_family_area": {"$avg": "$revr_ltr_score_family_area"},
            }
        }
    ])
    a_current_rental_groups = list(z_current_rental_groups)
    print(f"Extracting {len(a_current_rental_groups):,} rental information groups for {s_group} took: {utils.secondsToStr(time() - n_group_start)}")
    
    n_update_start = time()
    if a_group_cols[1:] == ["$revr_property_type", "$bedrooms"]:
        a_keys = [tuple(e_row["_id"]) for e_row in a_current_rental_groups]
    elif a_group_cols[1:] == ["$revr_property_type"]:
        a_keys = [tuple(e_row["_id"][:2] + [-1]) for e_row in a_current_rental_groups]
    elif a_group_cols[1:] == ["$bedrooms"]:
        a_keys = [tuple(e_row["_id"][:1] + ["_all_"] + e_row["_id"][-1:]) for e_row in a_current_rental_groups]
    else:
        a_keys = [tuple(e_row["_id"][:1] + ["_all_", -1]) for e_row in a_current_rental_groups]
    
    a_rental_updates = [
        (
            e_row.get("count", 0),
            round(e_row.get("days_on_zillow", 0), 2) if e_row.get("days_on_zillow", 0) else 0,
            e_row.get("last_listing", None),
            round(e_row.get("price", 0), 2) if e_row.get("price", 0) else 0,
            round(e_row.get("revr_ltr_score_overall", 0), 8) if e_row.get("revr_ltr_score_overall", 0) else 0,
            round(e_row.get("revr_ltr_score_absolute", 0), 8) if e_row.get("revr_ltr_score_absolute", 0) else 0,
            round(e_row.get("revr_ltr_score_relative", 0), 8) if e_row.get("revr_ltr_score_relative", 0) else 0,
            round(e_row.get("revr_ltr_score_family", 0), 8) if e_row.get("revr_ltr_score_family", 0) else 0,
            round(e_row.get("revr_ltr_score_area", 0), 8) if e_row.get("revr_ltr_score_overall_area", 0) else 0,
            round(e_row.get("revr_ltr_score_family_area", 0), 8) if e_row.get("revr_ltr_score_family_area", 0) else 0,
        ) + t_key
        for e_row, t_key in zip(a_current_rental_groups, a_keys)]
    
    if a_rental_updates:
        o_db = db_tables.get_connection()
        if b_archived:
            #o_db.update("revr.archive_market_summaries", {"for_rent_count": 0})
            o_db.execute(r"""UPDATE revr.archive_market_summaries SET for_rent_count = 0 WHERE for_rent_count IS NULL or for_rent_count > 0;""", commit=True)
            result = o_db.upsert_batch("revr.archive_market_summaries", a_RENT_UPDATE_COLS, a_KEY_COLS, a_rental_updates, b_debug=False)
            print(f"\tUploading {len(a_rental_updates):,} rental archive updates for {s_group} to revr.archive_market_summaries took: {utils.secondsToStr(time() - n_update_start)}")
        elif b_long_term:
            #o_db.update("revr.archive_market_summaries", {"for_rent_count": 0})
            o_db.execute(r"""UPDATE revr.average_market_summaries SET for_rent_count = 0 WHERE for_rent_count IS NULL or for_rent_count > 0;""", commit=True)
            result = o_db.upsert_batch("revr.average_market_summaries", a_RENT_UPDATE_COLS, a_KEY_COLS, a_rental_updates, b_debug=False)
            print(f"\tUploading {len(a_rental_updates):,} rental averages updates for {s_group} to revr.average_market_summaries took: {utils.secondsToStr(time() - n_update_start)}")
        else:
            o_db.execute(r"""UPDATE revr.current_market_summaries SET for_rent_count = 0 WHERE for_rent_count IS NULL or for_rent_count > 0;""", commit=True)
            result = o_db.upsert_batch("revr.current_market_summaries", a_RENT_UPDATE_COLS, a_KEY_COLS, a_rental_updates, b_debug=False)
            print(f"\tUploading {len(a_rental_updates):,} rental current updates for {s_group} to revr.current_market_summaries took: {utils.secondsToStr(time() - n_update_start)}")
    return result


def realtor_rent_properties_group_main(s_group, a_group_cols, e_extra_filter, o_db, o_docdb, **kwargs):
    n_group_start = time()
    result = None
    b_long_term = kwargs.get("b_long_term", False)
    b_archived = kwargs.get("b_archived", False)
    if b_archived:
        o_collection = o_docdb._db.realtor
        i_recent_days = kwargs.get("rent_archive_days", i_RENT_ARCHIVE_DAYS)
    elif b_long_term:
        o_collection = o_docdb._db.realtor
        i_recent_days = kwargs.get("rent_average_days", i_RENT_AVERAGE_DAYS)
    else:
        o_collection = o_docdb._db.realtor
        i_recent_days = kwargs.get("rent_current_days", i_RENT_CURRENT_DAYS)
    
    z_realtor_rental_groups = o_collection.aggregate([
        {"$match": {"scraping_date": {"$gte": datetime.utcnow() - timedelta(days=i_recent_days)}}},
        {"$match": e_extra_filter},
        {
            "$project": {
                'state': 1,
                'county': 1,
                'city': 1,
                'zip': 1,
                'market_ids': 1,
                "revr_property_type": 1,
                "bedrooms": 1,

                'scraping_date': 1,
                'days_on_zillow': 1,
                'price': 1,
                'revr_ltr_score_overall': 1,
            }
        },
        {
            "$group": {
                "_id": a_group_cols,
                "count": {"$sum": 1},
                "days_on_zillow": {"$avg": "$days_on_zillow"},
                "first_listing": {"$min": "$scraping_date"},
                "last_listing": {"$max": "$scraping_date"},
                "price": {"$avg": "$price"},
                "for_rent_ltr_score": {"$avg": "$for_rent_ltr_score"},
            }
        }
    ])
    a_current_rental_groups = list(z_current_rental_groups)
    print(f"Extracting {len(a_current_rental_groups):,} rental information groups for {s_group} took: {utils.secondsToStr(time() - n_group_start)}")
    
    n_update_start = time()
    if a_group_cols[1:] == ["$revr_property_type", "$bedrooms"]:
        a_keys = [tuple(e_row["_id"]) for e_row in a_current_rental_groups]
    elif a_group_cols[1:] == ["$revr_property_type"]:
        a_keys = [tuple(e_row["_id"][:2] + [-1]) for e_row in a_current_rental_groups]
    elif a_group_cols[1:] == ["$bedrooms"]:
        a_keys = [tuple(e_row["_id"][:1] + ["_all_"] + e_row["_id"][-1:]) for e_row in a_current_rental_groups]
    else:
        a_keys = [tuple(e_row["_id"][:1] + ["_all_", -1]) for e_row in a_current_rental_groups]
    
    a_rental_updates = [
        (
            e_row.get("count", 0),
            round(e_row.get("days_on_zillow", 0), 2) if e_row.get("days_on_zillow", 0) else 0,
            e_row.get("last_listing", None),
            round(e_row.get("price", 0), 2) if e_row.get("price", 0) else 0,
            round(e_row.get("revr_ltr_score_overall", 0), 8) if e_row.get("revr_ltr_score_overall", 0) else 0,
        ) + t_key
        for e_row, t_key in zip(a_current_rental_groups, a_keys)]
    
    if a_rental_updates:
        o_db = db_tables.get_connection()
        if b_archived:
            #o_db.update("revr.archive_market_summaries", {"for_rent_count": 0})
            o_db.execute(r"""UPDATE revr.archive_market_summaries SET for_rent_count = 0 WHERE for_rent_count IS NULL or for_rent_count > 0;""", commit=True)
            result = o_db.upsert_batch("revr.archive_market_summaries", a_RENT_UPDATE_COLS, a_KEY_COLS, a_rental_updates, b_debug=False)
            print(f"\tUploading {len(a_rental_updates):,} rental archive updates for {s_group} to revr.archive_market_summaries took: {utils.secondsToStr(time() - n_update_start)}")
        elif b_long_term:
            #o_db.update("revr.archive_market_summaries", {"for_rent_count": 0})
            o_db.execute(r"""UPDATE revr.average_market_summaries SET for_rent_count = 0 WHERE for_rent_count IS NULL or for_rent_count > 0;""", commit=True)
            result = o_db.upsert_batch("revr.average_market_summaries", a_RENT_UPDATE_COLS, a_KEY_COLS, a_rental_updates, b_debug=False)
            print(f"\tUploading {len(a_rental_updates):,} rental averages updates for {s_group} to revr.average_market_summaries took: {utils.secondsToStr(time() - n_update_start)}")
        else:
            o_db.execute(r"""UPDATE revr.current_market_summaries SET for_rent_count = 0 WHERE for_rent_count IS NULL or for_rent_count > 0;""", commit=True)
            result = o_db.upsert_batch("revr.current_market_summaries", a_RENT_UPDATE_COLS, a_KEY_COLS, a_rental_updates, b_debug=False)
            print(f"\tUploading {len(a_rental_updates):,} rental current updates for {s_group} to revr.current_market_summaries took: {utils.secondsToStr(time() - n_update_start)}")
    return result


def update_rent_summaries(**kwargs):
    global o_DOCDB, o_DB
    if kwargs.get("o_docdb"):
        o_docdb = kwargs["o_docdb"]
    else:
        if o_DOCDB is None: o_DOCDB = docdb_utils.DocDBConnector(read_only=True)
        o_docdb = o_DOCDB
    o_docdb._load()
    
    if kwargs.get("o_db"):
        o_db = kwargs["o_db"]
    else:
        if o_DB is None: o_DB = db_tables.get_connection()
        o_db = o_DB
    
    try:
        a_rent_groups = []
        if not kwargs.get("skip_state"): a_rent_groups.extend(a_STATE_GROUPS)
        if not kwargs.get("skip_county"): a_rent_groups.extend(a_COUNTY_GROUPS)
        if not kwargs.get("skip_city"): a_rent_groups.extend(a_CITY_GROUPS)
        if not kwargs.get("skip_zipcode"): a_rent_groups.extend(a_ZIPCODE_GROUPS)
        
        print("Start processing rent information")
        
        for s_group, a_group_cols in a_rent_groups:
            e_extra_filter = {
                "price": {"$gte": d_MIN_RENT_PRICE, "$lte": d_MAX_RENT_PRICE},
            }
            e_realtor_filter = {
                "listing_status": "for_rent",
                "price": {"$gte": d_MIN_RENT_PRICE, "$lte": d_MAX_RENT_PRICE},
            }
            if a_group_cols[1:] == ["$revr_property_type", "$bedrooms"]:
                e_extra_filter.update({
                    "revr_property_type": {"$ne": ""},
                    "bedrooms": {"$gte": 0, "$lte": d_MAX_BEDROOMS},
                })
            elif a_group_cols[1:] == ["$revr_property_type",]:
                e_extra_filter.update({
                    "revr_property_type": {"$ne": ""},
                })
            elif a_group_cols[1:] == ["$bedrooms",]:
                e_extra_filter.update({
                    "bedrooms": {"$gte": 0, "$lte": d_MAX_BEDROOMS},
                })
            
            #print("e_extra_filter", e_extra_filter)
            if kwargs.get("current", True):
                rent_main_current_result  = rent_properties_group_main(s_group, a_group_cols, e_extra_filter, b_long_term=False, o_db=o_db, o_docdb=o_docdb, **kwargs)
            if kwargs.get("average", True):
                rent_main_average_result  = rent_properties_group_main(s_group, a_group_cols, e_extra_filter, b_long_term=True, o_db=o_db, o_docdb=o_docdb, **kwargs)
            if kwargs.get("archive", True):
                rent_main_archive_result  = rent_properties_group_main(s_group, a_group_cols, e_extra_filter, b_archived=True, o_db=o_db, o_docdb=o_docdb, **kwargs)

    except Exception as e:
            s_trace = ''.join(traceback.format_exception(None, e, e.__traceback__))
            print(f"Failed on update_rent_summaries")
            print(s_trace)
    
    o_docdb._close()
    o_db.close()


def update_area_summaries(**kwargs):
    global o_DOCDB
    o_DOCDB = docdb_utils.DocDBConnector()
    
    global o_DB
    o_DB = db_tables.get_connection()
    
    if kwargs.get("sales", True):
        update_sale_summaries(**kwargs)
    
    if kwargs.get("rentals", True):
        update_rent_summaries(**kwargs)
    
    print("\n\nFinished")


if __name__ == "__main__":
    args, kwargs = utils.parse_command_line_args(sys.argv)
    print("Running update_scraped_area_summaries.py from shell")
    
    update_area_summaries(**kwargs)
