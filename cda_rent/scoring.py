﻿import os, re, math
from datetime import datetime, timedelta
from decimal import Decimal
from collections import defaultdict
import pandas as pd
import numpy as np

try:
    from . import utils, db_tables, us_states, state_property_tax, defaults
except:
    import utils, db_tables, us_states, state_property_tax, defaults

e_STR_SCORE_GROUP_WEIGHTS = {
    "relative": 5,
    "absolute": 2,
    "area": 3,
}
e_STR_SCORE_ABSOLUTE_WEIGHTS = {
    "revenue": 2.5,
    "rate": 1.5,
    "net": 5,
    "occupancy": 1,
}
e_STR_SCORE_RELATIVE_WEIGHTS = {
    "roi": 6,
    "expected": 2,
    "cap": 3,
}
e_STR_SCORE_AREA_WEIGHTS = {
    "cap": 2.5,
    "rate": 2.5,
    "opportunity": 3,
    "size": 0.5,
    "seasonality": 1,
    "optmization": 0.5,
}


e_LTR_SCORE_GROUP_WEIGHTS = {
    "relative": 5,
    "absolute": 2,
    "area": 3,
    "family": 3,
    "family_area": 3,
}
e_LTR_SCORE_ABSOLUTE_WEIGHTS = {
    "rent": 1,
    "net": 1,
}
e_LTR_SCORE_RELATIVE_WEIGHTS = {
    "roi": 1,
    "expected": 1,
    "cap": 1,
}
e_LTR_SCORE_FAMILY_WEIGHTS = {
    "lot_size": 1,
    "pool": 1,
    "rec_room": 1,
    "house_type": 0,
}
e_LTR_SCORE_AREA_WEIGHTS = {
    "cap": 1,
    "rent": 1,
    "rent_vs_price": 1,
    "size": 1,
    "absorption": 1,
    "penetration": 1,
}
e_LTR_SCORE_FAMILY_AREA_WEIGHTS = {
    "type_ratio": 1,
    "school_ratings": 1,
    "playgrounds": 1,
    "crime_rate": 0,
}


a_TOTAL_COLS_COSTS = [
    "total", "utilities"
]
a_UTILITY_COSTS = [
    "cable", "electricity", "gas", "internet", "trash", "lawn",
]
a_LONG_TERM_COSTS = [
    "water", "sewer", "property_tax", "homeowners_insurance",
]

d_LISTING_FEE_RATE = 0.03
d_LTR_MANAGEMENT_FEE_RATE = 0.1  # typical is 8 to 12 percent
d_LTR_MAINTENANCE_FEE = 0.105  # typical is 8.3 to 12.5 percent

i_MAX_CLEANS = 52
i_MIN_CLEANS = 6
d_CLEANING_FEE_PER_ROOM = 35
d_CLEANING_FEE_PER_SQFT = 0.085
d_CLEANING_OCCUPANCY_FACTOR = 60

d_AVG_SALARY = 36000
i_STR_MAX_PROPS_TO_MANAGE = 20
i_LTR_MAX_PROPS_TO_MANAGE = 30

i_RETURN_YEARS_TIMEFRAME = 5
i_SCORE_PRECISION = 8

d_AVG_OCCUPANCY = 0.46
d_AVG_RATE = 251.4
d_AVG_RENT = 2000
d_AVG_PRICE = 482540
d_STR_AVG_REV = 30474.76
d_AVG_PRICE_SQFT = 314.25
d_STR_AVG_REV_SQFT = 29.37
d_STR_AVG_NET = 43398.5
d_STR_AVG_NET_ROI = 0.0899376
d_STR_AVG_CAP = 0.0934471
d_STR_AVG_PENETRATION = 0.46  # normalized score, not raw percentage
d_AVG_INSURANCE = 1800
d_AVG_SHORT_TERM_INSURANCE_MULTIPLIER = 1.5

d_LTR_AVG_RENT_SQFT = 1.19
d_AVG_SALE_TURNOVER = 0.2
d_AVG_RENT_TURNOVER = 0.1957
d_AVG_SALE_DAYS_ON_MARKET = 50
d_AVG_RENT_DAYS_ON_MARKET = 25
e_PT_AVERAGES = {
    'property_type_House': 0.5727167675497205,
    'property_type_Cabin': 0.1768013520949603,
    'property_type_Cottage': 0.11936089930036839,
    'property_type_Farmhouse': 0.027588239918245383,
    'property_type_Chateau': 0.011194088515053869,
    'property_type_Estate': 0.002980897728166025,
    'property_type_Townhouse': 0.03452637371275835,
    'property_type_Condo': 0.04836333621570606,
    'property_type_Tinyhouse': 0.006222781227890898,
}
d_MIN_PRICE_EXP = 0.90 # the minimum an area price can be in relation to the state's average

a_FAMILY_PTS = ["property_type_House"]
a_NEUTRAL_FAMILY_PTS = [
    "property_type_Cottage", "property_type_Estate", "property_type_Townhouse", "property_type_Farmhouse"]
a_NON_FAMILY_PTS = [
    "property_type_Cabin", "property_type_Chateau", "property_type_Condo", "property_type_Tinyhouse"]
    
    
a_MONTH_DAYS = [
    (1, 31), (2, 59), (3, 90), (4, 120), (5, 151), (6, 181), (7, 212), (8, 243), (9, 273), (10, 304), (11, 334), (12, 365)]

o_DB = None

e_STATE_UTILITY_COSTS = {}

e_STR_SCORE_WEIGHTS = {}
e_LTR_SCORE_WEIGHTS = {}
dt_SCORE_UPDATED = datetime.now()

e_SCORE_CONFIG = {}
dt_SCORE_CONFIG_LOADED = datetime.utcnow()
dt_SCORE_CONFIG_RELOAD = dt_SCORE_CONFIG_LOADED + timedelta(hours=db_tables.e_CONFIG.get("score_config_reload_hours", 24), minutes=db_tables.e_CONFIG.get("score_config_reload_minutes", 0))

def load_data(**kwargs):
    global o_DB
    if o_DB is None:
        if db_tables is None: db_tables.load_db(**kwargs)
        o_DB = db_tables.o_DB
load_data()


def load_config(**kwargs):
    global e_SCORE_CONFIG
    global dt_SCORE_CONFIG_LOADED, dt_SCORE_CONFIG_RELOAD
    if not e_SCORE_CONFIG or not dt_SCORE_CONFIG_LOADED or not dt_SCORE_CONFIG_RELOAD or dt_SCORE_CONFIG_RELOAD < dt_SCORE_CONFIG_LOADED:
        dt_SCORE_CONFIG_LOADED = datetime.utcnow()
        dt_SCORE_CONFIG_RELOAD = dt_SCORE_CONFIG_LOADED + timedelta(hours=db_tables.e_CONFIG.get("score_config_reload_hours", 24), minutes=db_tables.e_CONFIG.get("score_config_reload_minutes", 0))
        
        db_tables.load_config(b_reload=True)
        
        e_SCORE_CONFIG = {k: v for k, v in db_tables.e_CONFIG.items() if k.startswith(("score", "scoring", "cost"))}
    
        global i_MAX_CLEANS, i_MIN_CLEANS, d_CLEANING_FEE_PER_ROOM, d_CLEANING_FEE_PER_SQFT, d_CLEANING_OCCUPANCY_FACTOR
        global d_LISTING_FEE_RATE, d_MANAGEMENT_FEE_RATE, d_AVG_SALARY, i_STR_MAX_PROPS_TO_MANAGE
        
        global i_RETURN_YEARS_TIMEFRAME, i_SCORE_PRECISION
        global d_AVG_OCCUPANCY, d_AVG_RATE
        global d_AVG_PRICE, d_AVG_PRICE_SQFT, d_MIN_PRICE_EXP
        global d_STR_AVG_REV, d_STR_AVG_REV_SQFT, d_STR_AVG_CAP
        global d_AVG_INSURANCE, d_AVG_SHORT_TERM_INSURANCE_MULTIPLIER
        global d_STR_AVG_NET, d_STR_AVG_NET_ROI
        global d_STR_AVG_PENETRATION
        
        global d_LTR_MANAGEMENT_FEE_RATE, d_LTR_MAINTENANCE_FEE
        
        d_LISTING_FEE_RATE = e_SCORE_CONFIG.get("cost_listing_fee_rate", 0.03)
        d_MANAGEMENT_FEE_RATE = e_SCORE_CONFIG.get("cost_management_fee_rate", 0.2)
        i_MAX_CLEANS = e_SCORE_CONFIG.get("cost_max_cleans", 52)
        i_MIN_CLEANS = e_SCORE_CONFIG.get("cost_min_cleans", 6)
        d_CLEANING_FEE_PER_ROOM = e_SCORE_CONFIG.get("cost_cleaning_fee_per_room", 35)
        d_CLEANING_FEE_PER_SQFT = e_SCORE_CONFIG.get("cost_cleaning_fee_per_sqft", 0.085)
        d_CLEANING_OCCUPANCY_FACTOR = e_SCORE_CONFIG.get("cost_cleaning_occupancy_factor", 60)
        d_AVG_SALARY = e_SCORE_CONFIG.get("cost_average_salary", 36000)
        i_STR_MAX_PROPS_TO_MANAGE = max(1, e_SCORE_CONFIG.get("cost_max_properties_to_manage", 20))
        
        i_RETURN_YEARS_TIMEFRAME = e_SCORE_CONFIG.get("scoring_return_timeframe_in_years", 5)
        i_SCORE_PRECISION = e_SCORE_CONFIG.get("scoring_precision", 8)
        
        d_AVG_OCCUPANCY = e_SCORE_CONFIG.get("scoring_avg_occupancy", 0.47)
        d_AVG_RATE = e_SCORE_CONFIG.get("scoring_avg_rate", 251.4)
        d_AVG_PRICE = e_SCORE_CONFIG.get("scoring_avg_price", 482540)
        d_STR_AVG_REV = e_SCORE_CONFIG.get("scoring_avg_revenue", 30474.76)
        d_AVG_PRICE_SQFT = e_SCORE_CONFIG.get("scoring_avg_price_per_sqft", 314.25)
        d_STR_AVG_REV_SQFT = e_SCORE_CONFIG.get("scoring_avg_rev_per_sqft", 29.37)
        d_STR_AVG_NET = e_SCORE_CONFIG.get("scoring_avg_net", 43398.5)
        d_STR_AVG_NET_ROI = e_SCORE_CONFIG.get("scoring_avg_net_roi", 0.0899376)
        d_STR_AVG_CAP = e_SCORE_CONFIG.get("scoring_avg_cap", 0.0934471)
        d_AVG_INSURANCE = e_SCORE_CONFIG.get("scoring_avg_insurance", 1800)
        d_AVG_SHORT_TERM_INSURANCE_MULTIPLIER = e_SCORE_CONFIG.get("scoring_short_term_insurance_multiplier", 1.5)
        d_STR_AVG_PENETRATION = e_SCORE_CONFIG.get("scoring_avg_penetration_norm", 0.46)  # normalized score, not raw percentage
        
        d_MIN_PRICE_EXP = e_SCORE_CONFIG.get("scoring_min_price_exp", 0.90) # the minimum an area price can be in relation to the state's average
        
        
        d_LTR_MANAGEMENT_FEE_RATE = e_SCORE_CONFIG.get("cost_ltr_management_fee_rate", d_LTR_MANAGEMENT_FEE_RATE)
        d_LTR_MAINTENANCE_FEE = e_SCORE_CONFIG.get("cost_ltr_maintenance_cost_rate", d_LTR_MAINTENANCE_FEE)

load_config()


#@utils.timer
def get_score_weights():
    global e_STR_SCORE_WEIGHTS, e_LTR_SCORE_WEIGHTS
    global dt_SCORE_UPDATED
    
    if not e_STR_SCORE_WEIGHTS or not dt_SCORE_UPDATED or datetime.now() - timedelta(minutes=5) > dt_SCORE_UPDATED:
        
        df_score_weights = o_DB.get_table_as_df(db_tables.score_weights, where="score_type='short_term_rental'")
        e_STR_SCORE_WEIGHTS = defaultdict(dict)
        for idx, row in df_score_weights.iterrows():
            e_STR_SCORE_WEIGHTS[row["score_group"]][row["score_name"]] = max(0, float(row["weight"]))
        
        if not e_STR_SCORE_WEIGHTS.get("overall"):
            e_STR_SCORE_WEIGHTS["overall"] = e_STR_SCORE_GROUP_WEIGHTS

        if not e_STR_SCORE_WEIGHTS.get("absolute"):
            e_STR_SCORE_WEIGHTS["absolute"] = e_STR_SCORE_ABSOLUTE_WEIGHTS

        if not e_STR_SCORE_WEIGHTS.get("relative"):
            e_STR_SCORE_WEIGHTS["relative"] = e_STR_SCORE_RELATIVE_WEIGHTS

        if not e_STR_SCORE_WEIGHTS.get("area"):
            e_STR_SCORE_WEIGHTS["area"] = e_STR_SCORE_AREA_WEIGHTS
    
        dt_SCORE_UPDATED = datetime.now()
    
    if not e_LTR_SCORE_WEIGHTS or not dt_SCORE_UPDATED or datetime.now() - timedelta(minutes=5) > dt_SCORE_UPDATED:
        
        df_score_weights = o_DB.get_table_as_df(db_tables.score_weights, where="score_type='long_term_rental'")
        e_LTR_SCORE_WEIGHTS = defaultdict(dict)
        for idx, row in df_score_weights.iterrows():
            e_LTR_SCORE_WEIGHTS[row["score_group"]][row["score_name"]] = max(0, float(row["weight"]))
        
        if not e_LTR_SCORE_WEIGHTS.get("overall"):
            e_LTR_SCORE_WEIGHTS["overall"] = e_LTR_SCORE_GROUP_WEIGHTS

        if not e_LTR_SCORE_WEIGHTS.get("absolute"):
            e_LTR_SCORE_WEIGHTS["absolute"] = e_LTR_SCORE_ABSOLUTE_WEIGHTS

        if not e_LTR_SCORE_WEIGHTS.get("relative"):
            e_LTR_SCORE_WEIGHTS["relative"] = e_LTR_SCORE_RELATIVE_WEIGHTS

        if not e_LTR_SCORE_WEIGHTS.get("area"):
            e_LTR_SCORE_WEIGHTS["area"] = e_LTR_SCORE_AREA_WEIGHTS
    
        dt_SCORE_UPDATED = datetime.now()
    
    return e_STR_SCORE_WEIGHTS, e_LTR_SCORE_WEIGHTS


#@utils.timer
def combine_scores(e_scores, e_weights):
    d_max = max(e_weights.values())
    if d_max <= 0:
        e_weights = {k: 1 for k in e_scores.keys()}
        d_max = 1
    
    d_avg = sum([
        v * e_weights.get(k, 0)
        for k, v in e_scores.items()]) / sum(e_weights.values())
    
    a_prod = []
    for k, v in e_scores.items():
        d_impact = e_weights.get(k, 0) / d_max
        x = v * d_impact + (1 - d_impact)
        #print(k, v, d_impact, x)
        a_prod.append(x)
    d_prod = np.product(a_prod)
    
    return np.mean([d_avg, d_prod])


#@utils.timer
def simple_combine_scores(e_scores):
    d_avg = np.mean(list(e_scores.values()))
    d_prod = np.product(list(e_scores.values()))
    return np.mean([d_avg, d_prod])


def long_term_rental_penetration_normalizer(x):
    if x < 0: return 0
    if x > 1: return 1
    return x ** 0.1


#@utils.timer
def get_state_factors(s_state):
    if s_state in {"", "all", "_all_"}: return 1, 1, d_STR_AVG_NET, d_STR_AVG_NET_ROI, d_STR_AVG_CAP, 0.01, d_AVG_INSURANCE
    #s_state = e_address.get("address_components", {}).get("state", [""])[0]
    #s_state = us_states.e_STATE_ABBREV.get(s_state.lower(), s_state.lower())
    #print(state)
    
    d_state_wage_rate = us_states.e_STATE_WAGE_FACTORS.get(s_state.lower(), 1)
    d_state_price_rate = us_states.e_STATE_PRICE_FACTORS.get(s_state.lower(), 1)
    d_state_rent_rate = us_states.e_STATE_RENT_FACTORS.get(s_state.lower(), 1)
    
    #print(state_wage_rate, state_price_rate, state_rent_rate)
    d_state_factor = np.mean([d_state_wage_rate, d_state_wage_rate, d_state_price_rate, d_state_rent_rate])
    
    d_state_net = us_states.e_STATE_NET.get(s_state.lower(), d_STR_AVG_NET)
    d_state_roi = us_states.e_STATE_ROI.get(s_state.lower(), d_STR_AVG_NET_ROI)
    d_state_cap = us_states.e_STATE_CAP.get(s_state.lower(), d_STR_AVG_CAP)
    d_state_insurance = us_states.e_STATE_INSURANCE.get(s_state.lower(), d_AVG_INSURANCE)
    
    d_state_property_tax = state_property_tax.e_MEDIAN_PROPERTY_TAX.get(s_state.lower(), 0.01)
    #print(d_state_factor)
    return d_state_factor, d_state_wage_rate, d_state_net, d_state_roi, d_state_cap, d_state_property_tax, d_state_insurance


def get_area_property_confidence(i_area_properties):
    #return min(1, (np.log(i_area_properties + 1) / 4.6) ** 1.5) if i_area_properties >= 0 else 0
    return min(1, (np.log(i_area_properties + 1) / 3.5) ** 1.5) if i_area_properties >= 0 else 0

def get_state_confidence(i_state_properties):
    #return min(1, (np.log(i_state_properties + 1) / 8.5) ** 2) if i_state_properties >= 0 else 0
    return min(1, (np.log(i_state_properties + 1) / 6) ** 2) if i_state_properties >= 0 else 0

def get_rating_confidence(i_ratings):
    return min(1, (np.log(i_ratings + 1) / 4) ** 2) if i_ratings >= 0 else 0

def shrink(i_area_properties, d_value, d_average):
    d_conf = get_area_property_confidence(i_area_properties)
    d_shrunk = (d_value * d_conf) + (d_average * (1 - d_conf))
    return d_shrunk

def shrink_state(i_state_properties, d_value, d_average):
    d_conf = get_state_confidence(i_state_properties)
    d_shrunk = (d_value * d_conf) + (d_average * (1 - d_conf))
    return d_shrunk

def shrink_rating(i_ratings, d_value, d_average):
    d_conf = get_rating_confidence(i_ratings)
    d_shrunk = (d_value * d_conf) + (d_average * (1 - d_conf))
    return d_shrunk


#@utils.timer
def estimate_price(d_rev):
    d_low_rev_price = round(28298 * d_rev ** 0.322811, 2)
    d_high_rev_price = round(4.0281 * d_rev + 736295, 2)
    
    if d_rev < 25000:
        d_rev_price = d_low_rev_price
    elif d_rev > 75000:
        d_rev_price = d_high_rev_price
    else:
        d_low_share = 1 - ((d_rev - 25000) / 50000)
        d_high_share = ((d_rev - 25000) / 50000)
        d_rev_price = round(
            d_low_rev_price * d_low_share +
            d_high_rev_price * d_high_share,
            2)
    return d_rev_price


#@utils.timer
def get_market_rate_score(
    d_zip_rate,
    d_city_rate,
    d_state_rate,
    i_area_properties=0,
    i_zip_properties=0,
    i_city_properties=0,
    i_state_properties=0,
):
    if not i_zip_properties: i_zip_properties = i_area_properties
    if not i_city_properties: i_city_properties = i_area_properties

    if 0 < i_state_properties < 5000:
        d_state_rate = shrink_state(i_state_properties, d_state_rate, d_AVG_RATE)

    if 0 < i_zip_properties < 100:
        if d_zip_rate > 0:
            d_zip_rate = shrink(i_zip_properties, d_zip_rate, d_state_rate)
    if 0 < i_city_properties < 100:
        if d_city_rate > 0:
            d_city_rate = shrink(i_city_properties, d_city_rate, d_state_rate)
    
    if d_zip_rate and d_city_rate:
        d_area_rate = (d_zip_rate + d_city_rate) / 2
    elif d_zip_rate:
        d_area_rate = d_zip_rate
    elif d_city_rate:
        d_area_rate = d_city_rate
    else:
        d_area_rate = d_state_rate
    
    d_abs_rate_score = max(0, min(1, 0 if d_area_rate <= 0 else np.log10((d_area_rate + 100) / 160))) ** 0.5 #0.75
    return d_abs_rate_score


def get_market_rent_score(
    d_zip_rent,
    d_city_rent,
    d_state_rent,
    i_area_properties=0,
    i_zip_properties=0,
    i_city_properties=0,
    i_state_properties=0,
):
    if not i_zip_properties: i_zip_properties = i_area_properties
    if not i_city_properties: i_city_properties = i_area_properties

    if 0 < i_state_properties < 5000:
        d_state_rate = shrink_state(i_state_properties, d_state_rent, d_AVG_RENT)

    if 0 < i_zip_properties < 100:
        if d_zip_rent > 0:
            d_zip_rent = shrink(i_zip_properties, d_zip_rent, d_state_rent)
    if 0 < i_city_properties < 100:
        if d_city_rent > 0:
            d_city_rent = shrink(i_city_properties, d_city_rent, d_state_rent)
    
    if d_zip_rent and d_city_rent:
        d_area_rent = (d_zip_rent + d_city_rent) / 2
    elif d_zip_rent:
        d_area_rent = d_zip_rent
    elif d_city_rent:
        d_area_rent = d_city_rent
    else:
        d_area_rent = d_state_rent
    
    d_abs_rent_score = max(0, min(1, 0 if d_area_rent <= 0 else np.log10((d_area_rent + 100) / 650))) #** 0.5 #0.75
    return d_abs_rent_score


#@utils.timer
def get_market_score(
    d_zip_price_sqft,
    d_city_price_sqft,
    d_state_price_sqft,
    d_zip_rev_sqft,
    d_city_rev_sqft,
    d_state_rev_sqft,
    d_zip_price,
    d_city_price,
    d_state_price,
    d_zip_rev,
    d_city_rev,
    d_state_rev,
    i_area_properties=0,
    i_zip_properties=0,
    i_city_properties=0,
    i_state_properties=0,
    i_zillow_properties=0,
    i_zip_zillow_properties=0,
    i_city_zillow_properties=0,
    i_state_zillow_properties=0,
):
    if not i_zillow_properties: i_zillow_properties = i_area_properties
    if not i_zip_properties: i_zip_properties = i_area_properties
    if not i_city_properties: i_city_properties = i_area_properties
    if not i_zip_zillow_properties: i_zip_zillow_properties = i_zillow_properties
    if not i_city_zillow_properties: i_city_zillow_properties = i_zillow_properties
    
    if 0 < i_state_properties < 5000:
        #d_area_penetration = shrink(i_area_properties, d_area_penetration, d_state_penetration)
        d_state_price_sqft = shrink_state(min(i_state_properties, i_state_zillow_properties), d_state_price_sqft, d_AVG_PRICE_SQFT)
        d_state_rev_sqft = shrink_state(i_state_properties, d_state_rev_sqft, d_STR_AVG_REV_SQFT)
    
    if 0 < i_zip_properties < 100:
        if d_zip_price_sqft > 0:
            d_zip_price_sqft = shrink(min(i_zip_properties, i_zip_zillow_properties), d_zip_price_sqft, d_state_price_sqft)
        if d_zip_rev_sqft > 0:
            d_zip_rev_sqft = shrink(i_zip_properties, d_zip_rev_sqft, d_state_rev_sqft)
    if 0 < i_city_properties < 100:
        if d_city_price_sqft > 0:
            d_city_price_sqft = shrink(min(i_city_properties, i_city_zillow_properties), d_city_price_sqft, d_state_price_sqft)
        if d_city_rev_sqft > 0:
            d_city_rev_sqft = shrink(i_city_properties, d_city_rev_sqft, d_state_rev_sqft)
    
    if d_zip_price_sqft and d_city_price_sqft:
        d_price_sqft = (d_zip_price_sqft + d_city_price_sqft) / 2
    elif d_zip_price_sqft:
        d_price_sqft = d_zip_price_sqft
    elif d_city_price_sqft:
        d_price_sqft = d_city_price_sqft
    else:
        d_price_sqft = d_state_price_sqft
        
    if d_zip_rev_sqft and d_city_rev_sqft:
        d_rev_sqft = (d_zip_rev_sqft + d_city_rev_sqft) / 2
    elif d_zip_rev_sqft:
        d_rev_sqft = d_zip_rev_sqft
    elif d_city_rev_sqft:
        d_rev_sqft = d_city_rev_sqft
    else:
        d_rev_sqft = d_state_rev_sqft
    
    if d_price_sqft > 0 and d_rev_sqft > 0:
        #print(f"rev sqft:{d_rev_sqft}, price sqft:{d_price_sqft}")
        #d_price_per_rev_score = min(1, max(0, ((d_rev_sqft / d_price_sqft) - 6) / 40))
        d_price_per_rev_score = min(1, max(0, d_rev_sqft / d_price_sqft))
        return min(1, (d_price_per_rev_score ** 0.2) * 1)
    else:
        # if no sq ft available, use total price/revenue
        
        if 0 < i_state_properties < 5000:
            #d_area_penetration = shrink(i_area_properties, d_area_penetration, d_state_penetration)
            d_state_price = shrink_state(min(i_state_properties, i_state_zillow_properties), d_state_price, d_AVG_PRICE)
            d_state_rev = shrink_state(i_state_properties, d_state_rev, d_STR_AVG_REV)
        
        if 0 < i_zip_properties < 100:
            if d_zip_price > 0:
                d_zip_price = shrink(min(i_zip_properties, i_zip_zillow_properties), d_zip_price, d_state_price)
            if d_zip_rev > 0:
                d_zip_rev = shrink(i_zip_properties, d_zip_rev, d_state_rev)
                
        if 0 < i_city_properties < 100:
            if d_city_price > 0:
                d_city_price = shrink(min(i_city_properties, i_city_zillow_properties), d_city_price, d_state_price)
            if d_city_rev > 0:
                d_city_rev = shrink(i_city_properties, d_city_rev, d_state_rev)
    
    if d_zip_price and d_city_price:
        d_price = round((d_zip_price + d_city_price) / 2, 2)
    elif d_zip_price:
        d_price = d_zip_price
    elif d_city_price:
        d_price = d_city_price
    else:
        d_price = d_state_price
        
    if d_zip_rev and d_city_rev:
        d_rev = (d_zip_rev + d_city_rev) / 2
    elif d_zip_rev:
        d_rev = d_zip_rev
    elif d_city_rev:
        d_rev = d_city_rev
    else:
        d_rev = d_state_rev
    
    if d_price > 0:
        #d_price_per_rev_score = min(1, max(0, ((d_rev / d_price) - 6) / 40))
        #print(f"rev:{d_rev}, price:{d_price}")
        
        d_price_per_rev_score = min(1, max(0, d_rev / d_price) * 1)
        return min(1, (d_price_per_rev_score ** 0.2) * 1)
    else:
        return 0

#@utils.timer
def get_ltr_market_score(
    d_zip_price_sqft,
    d_city_price_sqft,
    d_state_price_sqft,
    d_zip_rent_sqft,
    d_city_rent_sqft,
    d_state_rent_sqft,
    d_zip_price,
    d_city_price,
    d_state_price,
    d_zip_rent,
    d_city_rent,
    d_state_rent,
    i_area_rent_properties=0,
    i_zip_rent_properties=0,
    i_city_rent_properties=0,
    i_state_rent_properties=0,
    i_area_sale_properties=0,
    i_zip_sale_properties=0,
    i_city_sale_properties=0,
    i_state_sale_properties=0,
):
    if not i_zip_rent_properties: i_zip_rent_properties = i_area_rent_properties
    if not i_city_rent_properties: i_city_rent_properties = i_area_rent_properties
    
    if not i_zip_sale_properties: i_zip_sale_properties = i_area_sale_properties
    if not i_city_sale_properties: i_city_sale_properties = i_area_sale_properties
    
    if 0 < i_state_sale_properties < 5000:
        d_state_price_sqft = shrink_state(i_state_sale_properties, d_state_price_sqft, d_AVG_PRICE_SQFT)
    if 0 < i_state_rent_properties < 5000:
        d_state_rent_sqft = shrink_state(i_state_rent_properties, d_state_rent_sqft, d_LTR_AVG_RENT_SQFT)
    
    if 0 < i_zip_sale_properties < 100:
        if d_zip_price_sqft > 0:
            d_zip_price_sqft = shrink(i_zip_sale_properties, d_zip_price_sqft, d_state_price_sqft)
    if 0 < i_zip_rent_properties < 100:
        if d_zip_rent_sqft > 0:
            d_zip_rent_sqft = shrink(i_zip_rent_properties, d_zip_rent_sqft, d_state_rent_sqft)
    if 0 < i_city_sale_properties < 100:
        if d_city_price_sqft > 0:
            d_city_price_sqft = shrink(i_city_sale_properties, d_city_price_sqft, d_state_price_sqft)
    if 0 < i_city_rent_properties < 100:
        if d_city_rent_sqft > 0:
            d_city_rent_sqft = shrink(i_city_rent_properties, d_city_rent_sqft, d_state_rent_sqft)
    
    if d_zip_price_sqft and d_city_price_sqft:
        d_price_sqft = (d_zip_price_sqft + d_city_price_sqft) / 2
    elif d_zip_price_sqft:
        d_price_sqft = d_zip_price_sqft
    elif d_city_price_sqft:
        d_price_sqft = d_city_price_sqft
    else:
        d_price_sqft = d_state_price_sqft
        
    if d_zip_rent_sqft and d_city_rent_sqft:
        d_rent_sqft = (d_zip_rent_sqft + d_city_rent_sqft) / 2
    elif d_zip_rent_sqft:
        d_rent_sqft = d_zip_rent_sqft
    elif d_city_rent_sqft:
        d_rent_sqft = d_city_rent_sqft
    else:
        d_rent_sqft = d_state_rent_sqft
    
    if d_price_sqft > 0 and d_rent_sqft > 0:
        #d_price_comparison = np.tanh(d_price_sqft / d_state_price_sqft) if d_state_price_sqft > 0 else 1
        #d_rent_comparison = np.tanh(d_rent_sqft / d_state_rent_sqft) if d_state_rent_sqft > 0 else 1
        #d_rent_vs_price_score = ((d_rent_comparison - d_price_comparison) / 2) + 0.5
        #return max(0, min(1, d_rent_vs_price_score))
        
        d_price_per_rent_score = min(1, max(0, d_rent_sqft / d_price_sqft))
        return min(1, (d_price_per_rent_score ** 0.135) * 1)
    else:
        # if no sq ft available, use total price/rent
        
        if 0 < i_state_sale_properties < 5000:
            #d_area_penetration = shrink(i_area_properties, d_area_penetration, d_state_penetration)
            d_state_price = shrink_state(i_state_sale_properties, d_state_price, d_AVG_PRICE)
        if 0 < i_state_rent_properties < 5000:
            d_state_rent = shrink_state(i_state_rent_properties, d_state_rent, d_STR_AVG_RENT)
        
        if 0 < i_zip_sale_properties < 100:
            if d_zip_price > 0:
                d_zip_price = shrink(i_zip_sale_properties, d_zip_price, d_state_price)
            if d_zip_rent > 0:
                d_zip_rent = shrink(i_zip_sale_properties, d_zip_rent, d_state_rent)
                
        if 0 < i_city_properties < 100:
            if d_city_price > 0:
                d_city_price = shrink(i_city_sale_properties, d_city_price, d_state_price)
            if d_city_rent > 0:
                d_city_rent = shrink(i_city_sale_properties, d_city_rent, d_state_rent)
    
    if d_zip_price and d_city_price:
        d_price = round((d_zip_price + d_city_price) / 2, 2)
    elif d_zip_price:
        d_price = d_zip_price
    elif d_city_price:
        d_price = d_city_price
    else:
        d_price = d_state_price
        
    if d_zip_rent and d_city_rent:
        d_rent = (d_zip_rent + d_city_rent) / 2
    elif d_zip_rent:
        d_rent = d_zip_rent
    elif d_city_rent:
        d_rent = d_city_rent
    else:
        d_rent = d_state_rent
    
    if d_price > 0:
        #d_price_comparison = np.tanh(d_price / d_state_price) if d_state_price > 0 else 1
        #d_rent_comparison = np.tanh(d_rent / d_state_rent) if d_state_rent > 0 else 1
        #d_rent_vs_price_score = ((d_rent_comparison - d_price_comparison) / 2) + 0.5
        #return max(0, min(1, d_rent_vs_price_score))
        
        d_price_per_rent_score = min(1, max(0, d_rent / d_price))
        return min(1, (d_price_per_rent_score ** 0.135))
    else:
        return 0


#@utils.timer
def get_market_opportunity_score(
    d_area_penetration,
    d_state_penetration,
    d_area_occupancy,
    d_state_occupancy, 
    i_area_houses,
    i_area_properties=0,
    i_zip_properties=0,
    i_city_properties=0,
    i_state_properties=0,
):
    if 0 < i_state_properties < 5000:
        #d_area_penetration = shrink(i_area_properties, d_area_penetration, d_state_penetration)
        d_state_occupancy = shrink_state(i_state_properties, d_state_occupancy, d_AVG_OCCUPANCY)
    
    if 0 < i_area_properties < 50:
        #d_area_penetration = shrink(i_area_properties, d_area_penetration, d_state_penetration)
        d_area_occupancy = shrink(i_area_properties, d_area_occupancy, d_state_occupancy)
    
    d_occupancy_score = min(1, d_area_occupancy / 0.95) ** 0.9
    d_penetration_score = 1 - d_area_penetration
    #d_market_size_score = max(0, min(1, (np.log10(i_area_houses) - 1) / 5)) if i_area_houses > 0 else 0
    d_market_size_score = max(0, min(1, (np.log(i_area_houses)) / 7)) if i_area_houses >= 1 else 0
    
    #d_market_opportunity_score = ((d_occupancy_score * d_penetration_score) + (d_occupancy_score + d_penetration_score) / 2) / 2
    d_market_opportunity_score = ((d_occupancy_score * d_penetration_score) + (d_occupancy_score + d_penetration_score) / 2) / 2
    #d_market_opportunity_score *= d_market_size_score
    return max(0, min(1, d_market_opportunity_score)) ** 0.6, d_market_size_score


#@utils.timer
def get_ltr_market_opportunity_score(
    d_zip_price,
    d_city_price,
    d_state_price,
    d_zip_rent,
    d_city_rent,
    d_state_rent,
    #d_area_sale_penetration,
    #d_state_sale_penetration,
    d_zip_rent_penetration,
    d_city_rent_penetration,
    d_state_rent_penetration,
    #d_area_sale_turnover,
    #d_state_sale_turnover,
    #d_area_rent_turnover,
    #d_state_rent_turnover,
    d_zip_sale_days_on_market,
    d_city_sale_days_on_market,
    d_state_sale_days_on_market,
    d_zip_rent_days_on_market,
    d_city_rent_days_on_market,
    d_state_rent_days_on_market,
    i_area_houses,
    i_area_sale_properties=0,
    i_zip_sale_properties=0,
    i_city_sale_properties=0,
    i_state_sale_properties=0,
    i_area_rent_properties=0,
    i_zip_rent_properties=0,
    i_city_rent_properties=0,
    i_state_rent_properties=0,
):
    if 0 < d_state_price < 5000:
        d_state_price = shrink_state(i_state_sale_properties, d_state_price, d_AVG_PRICE)
    if 0 < i_state_rent_properties < 5000:
        d_state_rent = shrink_state(i_state_rent_properties, d_state_rent, d_AVG_RENT)
    
    if d_zip_price and d_city_price:
        d_area_price = (d_zip_price + d_city_price) / 2
    elif d_zip_price:
        d_area_price = d_zip_price
    elif d_city_price:
        d_area_price = d_city_price
    else:
        d_area_price = d_state_price
    
    if d_zip_rent and d_city_rent:
        d_area_rent = (d_zip_rent + d_city_rent) / 2
    elif d_zip_rent:
        d_area_rent = d_zip_rent
    elif d_city_rent:
        d_area_rent = d_city_rent
    else:
        d_area_rent = d_state_rent
    
    if d_zip_rent_penetration and d_city_rent_penetration:
        d_area_rent_penetration = (d_zip_rent_penetration + d_city_rent_penetration) / 2
    elif d_zip_rent_penetration:
        d_area_rent_penetration = d_zip_rent_penetration
    elif d_city_rent_penetration:
        d_area_rent_penetration = d_city_rent_penetration
    else:
        d_area_rent_penetration = d_state_rent_penetration
    if not d_area_rent_penetration: d_area_rent_penetration = 0
    
    if d_zip_sale_days_on_market and d_city_sale_days_on_market:
        d_area_sale_days_on_market = (d_zip_sale_days_on_market + d_city_sale_days_on_market) / 2
    elif d_zip_sale_days_on_market:
        d_area_sale_days_on_market = d_zip_sale_days_on_market
    elif d_city_sale_days_on_market:
        d_area_sale_days_on_market = d_city_sale_days_on_market
    else:
        d_area_sale_days_on_market = d_state_sale_days_on_market
        
    if d_zip_rent_days_on_market and d_city_rent_days_on_market:
        d_area_rent_days_on_market = (d_zip_rent_days_on_market + d_city_rent_days_on_market) / 2
    elif d_zip_rent_days_on_market:
        d_area_rent_days_on_market = d_zip_rent_days_on_market
    elif d_city_rent_days_on_market:
        d_area_rent_days_on_market = d_city_rent_days_on_market
    else:
        d_area_rent_days_on_market = d_state_rent_days_on_market
    
    # how the area's price and rent compare to the state average
    d_price_comparison = np.tanh(d_area_price / d_state_price) if d_state_price and d_state_price > 0 else 1
    d_rent_comparison = np.tanh(d_area_rent / d_state_rent) if d_state_rent and d_state_rent > 0 else 1
    d_rent_vs_price_score = ((d_rent_comparison - d_price_comparison) / 2) + 0.5
    
    #if 0 < i_state_sale_properties < 5000:
    #    d_state_sale_turnover = shrink_state(i_state_sale_properties, d_state_sale_turnover, d_AVG_SALE_TURNOVER)
    #if 0 < i_state_rent_properties < 5000:
    #    d_state_rent_turnover = shrink_state(i_state_rent_properties, d_state_rent_turnover, d_AVG_RENT_TURNOVER)
    
    if 0 < i_state_sale_properties < 5000:
        d_state_sale_days_on_market = shrink_state(i_state_sale_properties, d_state_sale_days_on_market, d_AVG_SALE_DAYS_ON_MARKET)
    if 0 < i_state_rent_properties < 5000:
        d_state_rent_days_on_market = shrink_state(i_state_rent_properties, d_state_rent_days_on_market, d_AVG_RENT_DAYS_ON_MARKET)
    
    #print("d_area_sale_days_on_market", d_area_sale_days_on_market, "d_area_rent_days_on_market", d_area_rent_days_on_market)
    #print("d_state_sale_days_on_market", d_state_sale_days_on_market, "d_state_rent_days_on_market", d_state_rent_days_on_market)
    
    d_sale_days_comparison = np.tanh(d_state_sale_days_on_market / d_area_sale_days_on_market) if d_area_sale_days_on_market and d_state_sale_days_on_market else 1
    d_rent_days_comparison = np.tanh(d_state_rent_days_on_market / d_area_rent_days_on_market) if d_area_rent_days_on_market and d_state_rent_days_on_market else 1
    d_rent_vs_sale_days_score = ((d_rent_days_comparison - d_sale_days_comparison) / 2) + 0.5
    
    d_penetration_score = 1 - d_area_rent_penetration
    
    #print("i_area_houses", i_area_houses)
    #d_market_size_score = max(0, min(1, (np.log10(i_area_houses) - 1) / 5)) if i_area_houses > 0 else 0
    d_market_size_score = max(0, min(1, (np.log(i_area_houses)) / 10)) if i_area_houses >= 1 else 0
    
    #d_market_opportunity_score = ((d_occupancy_score * d_penetration_score) + (d_occupancy_score + d_penetration_score) / 2) / 2
    #d_market_opportunity_score = ((d_rent_vs_price_score * d_rent_vs_sale_days_score * d_penetration_score) + (d_rent_vs_price_score * d_rent_vs_sale_days_score * d_penetration_score) / 3) / 2
    #d_market_opportunity_score *= d_market_size_score
    #return max(0, min(1, d_market_opportunity_score)) ** 0.6, d_market_size_score
    return (
        max(0, min(1, d_rent_vs_price_score)) ** 1.15,
        #max(0, min(1, d_rent_vs_sale_days_score)) ** 1,
        d_rent_vs_sale_days_score ** 0.6,
        d_market_size_score ** 2)


def get_ltr_market_family_area_score(e_local_data,):
    d_market_family_ptype_score = 0.5
    d_market_family_school_score = 0.5
    #d_family_lot_size_score = 0.5
    #d_family_rec_room_score = 0.5
    #d_family_pool_score = 0.25
    d_market_family_playground_score = 0.25
    #d_family_score = 0.5
    
    
    if "zip" in e_local_data and "city" in e_local_data:
        i_zip_properties = e_local_data["zip"].get("area_properties", 0)
        i_city_properties = e_local_data["city"].get("area_properties", 0)
        d_family_friendly = sum([
            (shrink(i_zip_properties, e_local_data["zip"].get(pt, 0), e_PT_AVERAGES.get(pt, 0))
             + shrink(i_city_properties, e_local_data["city"].get(pt, 0), e_PT_AVERAGES.get(pt, 0))) / 2
            for pt in a_FAMILY_PTS])
        d_neutral_family = sum([
            (shrink(i_zip_properties, e_local_data["zip"].get(pt, 0), e_PT_AVERAGES.get(pt, 0))
             + shrink(i_city_properties, e_local_data["city"].get(pt, 0), e_PT_AVERAGES.get(pt, 0))) / 2
            for pt in a_NEUTRAL_FAMILY_PTS])
        d_not_family_friendly = sum([
            (shrink(i_zip_properties, e_local_data["zip"].get(pt, 0), e_PT_AVERAGES.get(pt, 0))
             + shrink(i_city_properties, e_local_data["city"].get(pt, 0), e_PT_AVERAGES.get(pt, 0))) / 2
            for pt in a_NON_FAMILY_PTS])
        d_market_family_school_score = (
            e_local_data["zip"].get("school_ranking", 0) + e_local_data["city"].get("school_ranking", 0)
            + e_local_data["zip"].get("school_rating", 0) + e_local_data["city"].get("school_rating", 0)) / 4
        #d_area_lot_size = (e_local_data["zip"].get("lot_size", 0) + e_local_data["city"].get("lot_size", 0)) / 2
        #d_family_rec_room_score = (e_local_data["zip"].get("location_recreation_center", 0) + e_local_data["city"].get("location_recreation_center", 0)) / 2
        #d_family_pool_score = (e_local_data["zip"].get("location_pool", 0) + e_local_data["city"].get("location_pool", 0)) / 2
        d_market_family_playground_score = (e_local_data["zip"].get("location_playground", 0) + e_local_data["city"].get("location_playground", 0)) / 2
    elif "zip" in e_local_data:
        i_zip_properties = e_local_data["zip"].get("area_properties", 0)
        d_family_friendly = sum([shrink(i_zip_properties, e_local_data["zip"].get(pt, 0), e_PT_AVERAGES.get(pt, 0)) for pt in a_FAMILY_PTS])
        d_neutral_family = sum([shrink(i_zip_properties, e_local_data["zip"].get(pt, 0), e_PT_AVERAGES.get(pt, 0)) for pt in a_NEUTRAL_FAMILY_PTS])
        d_not_family_friendly = sum([shrink(i_zip_properties, e_local_data["zip"].get(pt, 0), e_PT_AVERAGES.get(pt, 0)) for pt in a_NON_FAMILY_PTS])
        d_market_family_school_score = (
            e_local_data["zip"].get("school_ranking", 0) + e_local_data["zip"].get("school_rating", 0)) / 2
        #d_area_lot_size = e_local_data["zip"].get("lot_size", 0)
        #d_family_rec_room_score = e_local_data["zip"].get("location_recreation_center", 0)
        #d_family_pool_score = e_local_data["zip"].get("location_pool", 0)
        d_market_family_playground_score = e_local_data["zip"].get("location_playground", 0)
    elif "city" in e_local_data:
        i_city_properties = e_local_data["city"].get("area_properties", 0)
        d_family_friendly = sum([shrink(i_city_properties, e_local_data["city"].get(pt, 0), e_PT_AVERAGES.get(pt, 0)) for pt in a_FAMILY_PTS])
        d_neutral_family = sum([shrink(i_zip_properties, e_local_data["zip"].get(pt, 0), e_PT_AVERAGES.get(pt, 0)) for pt in a_NEUTRAL_FAMILY_PTS])
        d_not_family_friendly = sum([shrink(i_city_properties, e_local_data["city"].get(pt, 0), e_PT_AVERAGES.get(pt, 0)) for pt in a_NON_FAMILY_PTS])
        d_market_family_school_score = (
            e_local_data["city"].get("school_ranking", 0) + e_local_data["city"].get("school_rating", 0)) / 2
        #d_area_lot_size = e_local_data["city"].get("lot_size", 0)
        #d_family_rec_room_score = e_local_data["city"].get("location_recreation_center", 0)
        #d_family_pool_score = e_local_data["city"].get("location_pool", 0)
        d_market_family_playground_score = e_local_data["city"].get("location_playground", 0)
    elif "state" in e_local_data:
        d_family_friendly = sum([e_local_data["state"].get(pt, 0) for pt in a_FAMILY_PTS])
        d_neutral_family = sum([shrink(i_zip_properties, e_local_data["zip"].get(pt, 0), e_PT_AVERAGES.get(pt, 0)) for pt in a_NEUTRAL_FAMILY_PTS])
        d_not_family_friendly = sum([e_local_data["state"].get(pt, 0) for pt in a_NON_FAMILY_PTS])
        d_market_family_school_score = (
            e_local_data["state"].get("school_ranking", 0) + e_local_data["state"].get("school_rating", 0)) / 2
        #d_area_lot_size = e_local_data["state"].get("lot_size", 0)
        #d_family_rec_room_score = e_local_data["state"].get("location_recreation_center", 0)
        #d_family_pool_score = e_local_data["state"].get("location_pool", 0)
        d_market_family_playground_score = e_local_data["state"].get("location_playground", 0)
    else:
        d_family_friendly, d_neutral_family, d_not_family_friendly = 0, 0, 0
        d_market_family_school_score, d_market_family_playground_score = 0, 0
        d_market_family_lot_size_score, d_market_family_rec_room_score, d_market_family_pool_score = 0, 0, 0
    
    d_market_family_ptype_score = np.tanh((d_family_friendly + d_neutral_family * 0.5) / (d_not_family_friendly + d_neutral_family * 0.5)) ** 6 if d_not_family_friendly > 0 else 1
    d_market_family_school_score = d_market_family_school_score ** 1.4
    d_market_family_playground_score = d_market_family_playground_score ** 0.25
    #if d_area_lot_size < 0.3:
    #    d_family_lot_size_score = max(0, ((d_area_lot_size - 0.1) / 0.2))
    #elif d_area_lot_size > 10:
    #    d_family_lot_size_score = max(0.0, 1 - ((d_area_lot_size - 10) ** 0.5) / 20)
    #else:
    #    d_family_lot_size_score = 1
    
    #return d_market_family_ptype_score, d_market_family_school_score, d_family_lot_size_score, d_family_rec_room_score, d_family_pool_score, d_market_family_playground_score
    return d_market_family_ptype_score, d_market_family_school_score, d_market_family_playground_score


def get_ltr_market_family_score(e_local_data,):
    d_family_ptype_score = 0.5
    #d_family_school_score = 0.5
    d_family_lot_size_score = 0.5
    d_family_rec_room_score = 0.5
    d_family_pool_score = 0.25
    #d_family_playground_score = 0.25
    #d_family_score = 0.5
    
    #print("get_ltr_market_family_score e_local_data.keys()", e_local_data.keys())
    if "zip" in e_local_data and "city" in e_local_data:
        i_zip_properties = e_local_data["zip"]["area_properties"]
        i_city_properties = e_local_data["city"]["area_properties"]
        d_family_friendly = sum([
            (shrink(i_zip_properties, e_local_data["zip"].get(pt, 0), e_PT_AVERAGES.get(pt, 0))
             + shrink(i_city_properties, e_local_data["city"].get(pt, 0), e_PT_AVERAGES.get(pt, 0))) / 2
            for pt in a_FAMILY_PTS])
        d_neutral_family = sum([
            (shrink(i_zip_properties, e_local_data["zip"].get(pt, 0), e_PT_AVERAGES.get(pt, 0))
             + shrink(i_city_properties, e_local_data["city"].get(pt, 0), e_PT_AVERAGES.get(pt, 0))) / 2
            for pt in a_NEUTRAL_FAMILY_PTS])
        d_not_family_friendly = sum([
            (shrink(i_zip_properties, e_local_data["zip"].get(pt, 0), e_PT_AVERAGES.get(pt, 0))
             + shrink(i_city_properties, e_local_data["city"].get(pt, 0), e_PT_AVERAGES.get(pt, 0))) / 2
            for pt in a_NON_FAMILY_PTS])
        #d_market_family_school_score = (
        #    e_local_data["zip"].get("school_ranking", 0) + e_local_data["city"].get("school_ranking", 0)
        #    + e_local_data["zip"].get("school_rating", 0) + e_local_data["city"].get("school_rating", 0)) / 4
        d_lot_size = (e_local_data["zip"].get("lot_size", 0) + e_local_data["city"].get("lot_size", 0)) / 2
        d_family_rec_room_score = (e_local_data["zip"].get("location_recreation_center", 0) + e_local_data["city"].get("location_recreation_center", 0)) / 2
        d_family_pool_score = (e_local_data["zip"].get("location_pool", 0) + e_local_data["city"].get("location_pool", 0)) / 2
        #d_family_playground_score = (e_local_data["zip"].get("location_playground", 0) + e_local_data["city"].get("location_playground", 0)) / 2
    elif "zip" in e_local_data:
        i_zip_properties = e_local_data["zip"]["area_properties"]
        d_family_friendly = sum([shrink(i_zip_properties, e_local_data["zip"].get(pt, 0), e_PT_AVERAGES.get(pt, 0)) for pt in a_FAMILY_PTS])
        d_neutral_family = sum([shrink(i_zip_properties, e_local_data["zip"].get(pt, 0), e_PT_AVERAGES.get(pt, 0)) for pt in a_NEUTRAL_FAMILY_PTS])
        d_not_family_friendly = sum([shrink(i_zip_properties, e_local_data["zip"].get(pt, 0), e_PT_AVERAGES.get(pt, 0)) for pt in a_NON_FAMILY_PTS])
        #d_market_family_school_score = (
        #    e_local_data["zip"].get("school_ranking", 0) + e_local_data["zip"].get("school_rating", 0)) / 2
        d_lot_size = e_local_data["zip"].get("lot_size", 0)
        d_family_rec_room_score = e_local_data["zip"].get("location_recreation_center", 0)
        d_family_pool_score = e_local_data["zip"].get("location_pool", 0)
        #d_family_playground_score = e_local_data["zip"].get("location_playground", 0)
    elif "city" in e_local_data:
        i_city_properties = e_local_data["city"]["area_properties"]
        d_family_friendly = sum([shrink(i_city_properties, e_local_data["city"].get(pt, 0), e_PT_AVERAGES.get(pt, 0)) for pt in a_FAMILY_PTS])
        d_neutral_family = sum([shrink(i_city_properties, e_local_data["city"].get(pt, 0), e_PT_AVERAGES.get(pt, 0)) for pt in a_NEUTRAL_FAMILY_PTS])
        d_not_family_friendly = sum([shrink(i_city_properties, e_local_data["city"].get(pt, 0), e_PT_AVERAGES.get(pt, 0)) for pt in a_NON_FAMILY_PTS])
        #d_market_family_school_score = (
        #    e_local_data["city"].get("school_ranking", 0) + e_local_data["city"].get("school_rating", 0)) / 2
        d_lot_size = e_local_data["city"].get("lot_size", 0)
        d_family_rec_room_score = e_local_data["city"].get("location_recreation_center", 0)
        d_family_pool_score = e_local_data["city"].get("location_pool", 0)
        #d_family_playground_score = e_local_data["city"].get("location_playground", 0)
    elif "state" in e_local_data:
        d_family_friendly = sum([e_local_data["state"].get(pt, 0) for pt in a_FAMILY_PTS])
        d_neutral_family = sum([e_local_data["state"].get(pt, 0) for pt in a_NEUTRAL_FAMILY_PTS])
        d_not_family_friendly = sum([e_local_data["state"].get(pt, 0) for pt in a_NON_FAMILY_PTS])
        #d_market_family_school_score = (
        #    e_local_data["state"].get("school_ranking", 0) + e_local_data["state"].get("school_rating", 0)) / 2
        d_lot_size = e_local_data["state"].get("lot_size", 0)
        d_family_rec_room_score = e_local_data["state"].get("location_recreation_center", 0)
        d_family_pool_score = e_local_data["state"].get("location_pool", 0)
        #d_family_playground_score = e_local_data["state"].get("location_playground", 0)
    else:
        d_family_friendly, d_neutral_family, d_not_family_friendly = 0, 0, 0
        #d_family_school_score, d_family_playground_score = 0, 0
        d_family_lot_size_score, d_family_rec_room_score, d_family_pool_score = 0, 0, 0
    
    #d_family_ptype_score = np.tanh(d_family_friendly / d_not_family_friendly) ** 6 if d_not_family_friendly > 0 else 1
    d_family_ptype_score = np.tanh((d_family_friendly + d_neutral_family * 0.5) / (d_not_family_friendly + d_neutral_family * 0.5)) ** 6 if d_not_family_friendly > 0 else 1
    
    if d_lot_size < 0.5:
        d_family_lot_size_score = max(0, ((d_lot_size - 0.15) / 0.35))
    elif d_lot_size > 5:
        d_family_lot_size_score = max(0.0, 1 - ((d_lot_size - 5) ** 0.5) / 20)
    else:
        d_family_lot_size_score = 1
    
    d_family_lot_size_score = d_family_lot_size_score ** 6
    d_family_rec_room_score = d_family_rec_room_score ** 0.2
    d_family_pool_score = d_family_pool_score ** 0.2
    
    return d_family_ptype_score , d_family_lot_size_score, d_family_rec_room_score, d_family_pool_score


def get_ltr_family_score(e_inputs,):

    d_family_friendly = sum([e_inputs.get(pt, 0) for pt in a_FAMILY_PTS])
    d_neutral_family = sum([e_inputs.get(pt, 0) for pt in a_NEUTRAL_FAMILY_PTS])
    d_not_family_friendly = sum([e_inputs.get(pt, 0) for pt in a_NON_FAMILY_PTS])
    
    #d_family_ptype_score = np.tanh(d_family_friendly / d_not_family_friendly) if d_not_family_friendly > 0 else 1
    d_family_ptype_score = np.tanh((d_family_friendly + d_neutral_family * 0.5) / (d_not_family_friendly + d_neutral_family * 0.5)) ** 6 if d_not_family_friendly > 0 else 1
    
    d_lot_size = e_inputs.get("lot_size", 0)
    if d_lot_size < 0.5:
        d_family_lot_size_score = max(0, ((d_lot_size - 0.15) / 0.35))
    elif d_lot_size > 10:
        d_family_lot_size_score = max(0.1, 1 - ((d_lot_size - 10) ** 0.5) / 20)
    else:
        d_family_lot_size_score = 1
    
    d_family_rec_room_score = max([
        float(e_inputs.get("feature_pool_table", 0)),
        float(e_inputs.get("feature_extra_room", 0)),
        float(e_inputs.get("feature_game_room", 0)),
        float(e_inputs.get("feature_living_room", 0)),
        float(e_inputs.get("feature_rec_room", 0)),
        ])
    d_family_pool_score = float(e_inputs.get("feature_pool", 0))
    
    return d_family_ptype_score, d_family_lot_size_score, d_family_rec_room_score, d_family_pool_score

#@utils.timer
def get_market_seasonality_score(
    a_zip_per_month_rate,
    a_zip_per_month_occupancy,
    a_city_per_month_rate,
    a_city_per_month_occupancy,
    a_state_per_month_rate,
    a_state_per_month_occupancy,
    **kwargs
):
    if a_zip_per_month_occupancy and a_city_per_month_occupancy:
        a_per_month_occupancy = [
            np.mean([z if not pd.isnull(z) else 0, c if not pd.isnull(c) else 0])
            for z, c in zip(a_zip_per_month_occupancy, a_city_per_month_occupancy)]
    elif a_zip_per_month_occupancy:
        a_per_month_occupancy = a_zip_per_month_occupancy
    elif a_city_per_month_occupancy:
        a_per_month_occupancy = a_city_per_month_occupancy
    else:
        a_per_month_occupancy = a_state_per_month_occupancy
    
    if a_zip_per_month_rate and a_city_per_month_rate:
        a_per_month_rates = [
            np.mean([z, c]) if not pd.isnull(z) and not pd.isnull(c)
            else z if pd.isnull(c)
            else c if pd.isnull(z)
            else 0
            for z, c in zip(a_zip_per_month_rate, a_city_per_month_rate)]
    elif a_zip_per_month_rate:
        a_per_month_rates = a_zip_per_month_rate
    elif a_city_per_month_rate:
        a_per_month_rates = a_city_per_month_rate
    else:
        a_per_month_rates = a_state_per_month_rate
    
    return get_seasonality_score(a_per_month_occupancy, a_per_month_rates, **kwargs)


#@utils.timer
def get_seasonality_score(a_per_month_occupancy, a_per_month_rates, **kwargs):
    a_non_zero_occ = [x for x in a_per_month_occupancy if isinstance(x, (int, float)) and x > 0]
    a_non_zero_rates = [x for x in a_per_month_rates if isinstance(x, (int, float)) and x > 0]
    
    #print(a_per_month_occupancy)
    i_empty_months = 12 - len(a_non_zero_occ)
    
    d_vacancy_score = (1 - (i_empty_months / 12)) ** 1.5
    d_non_seasonality = (max(0, utils.value_list_similarity(a_per_month_occupancy)) ** 2) ** 0.75
    d_price_dynamism = (max(0, utils.value_list_similarity(a_non_zero_rates)) ** 2) ** 1.25
    d_seasonality_score = np.mean([d_vacancy_score, d_non_seasonality])
    if kwargs.get("b_debug"):
        print(f"Vacancy score: {d_vacancy_score}")
        print(f"Non-seasonality: {d_non_seasonality}")
        print(f"Seasonality score: {d_seasonality_score}")
        print(f"Price dynamism score: {d_price_dynamism}")
    return d_seasonality_score, d_price_dynamism


#@utils.timer
def score_area(e_area, s_type="", **kwargs):
    #e_address, e_inputs, e_predictions, e_local_data, **kwargs
    i_bedrooms = e_area["bedrooms"]
    i_bathrooms = e_area["bathrooms"]
    d_living_area = e_area["living_area"]
    d_lot_size = e_area["lot_size"]
    
    if pd.isnull(i_bedrooms) or i_bedrooms <= 0: i_bedrooms = 3
    if pd.isnull(i_bathrooms) or i_bathrooms <= 0: i_bathrooms = 2
    if pd.isnull(d_living_area) or d_living_area <= 0: d_living_area = 2000
    if pd.isnull(d_lot_size) or d_lot_size <= 0: d_lot_size = .5
    
    i_area_houses = e_area.get("houses", 1)
    #print("score_area.i_area_houses", i_area_houses)
    i_area_properties = e_area.get("area_properties", 1)
    i_zip_properties, i_city_properties = 0, 0
    if "zip" in s_type:
        i_zip_properties = i_area_properties
    if "city" in s_type:
        i_city_properties = i_area_properties
    if "state" in s_type:
        i_state_properties = i_area_properties
    else:
        i_state_properties = kwargs["state_info"].get("area_properties") if kwargs.get("state_info", {}) else 0
    
    i_zillow_properties = e_area.get("zillow_properties", 1)
    i_zip_zillow_properties, i_city_zillow_properties = 0, 0
    if "zip" in s_type:
        i_zip_zillow_properties = i_zillow_properties
    if "city" in s_type:
        i_city_zillow_properties = i_zillow_properties
    if "state" in s_type:
        i_state_zillow_properties = i_zillow_properties
    else:
        i_state_zillow_properties = kwargs["state_info"].get("zillow_properties") if kwargs.get("state_info", {}) else 0
    
    i_area_sale_properties = e_area.get("sale_properties", 1)
    i_zip_sale_properties, i_city_sale_properties = 0, 0
    if "zip" in s_type:
        i_zip_sale_properties = i_area_sale_properties
    if "city" in s_type:
        i_city_sale_properties = i_area_sale_properties
    if "state" in s_type:
        i_state_sale_properties = i_area_sale_properties
    else:
        i_state_sale_properties = kwargs["state_info"].get("sale_properties") if kwargs.get("state_info", {}) else 0
    
    i_area_sale_properties = e_area.get("rent_properties", 1)
    i_zip_rent_properties, i_city_rent_properties = 0, 0
    if "zip" in s_type:
        i_zip_rent_properties = i_zillow_properties
    if "city" in s_type:
        i_city_rent_properties = i_zillow_properties
    if "state" in s_type:
        i_state_rent_properties = i_zillow_properties
    else:
        i_state_rent_properties = kwargs["state_info"].get("rent_properties") if kwargs.get("state_info", {}) else 0
    d_price = e_area["price_median"] if e_area.get("price_median", 0) > 0 else e_area["price"]
    d_revenue = e_area["revenue"]
    d_occupancy = e_area.get("occupancy", 0)
    d_avg_rate = e_area["rate"]
    d_avg_rent = e_area["rent"]
    
    s_state = us_states.e_STATE_ABBREV.get(e_area["state"].lower(), e_area["state"].lower())
    d_state_factor, d_state_wage_rate, d_state_net, d_state_roi, d_state_cap, d_state_property_tax, d_state_insurance = get_state_factors(s_state)
    if kwargs.get("b_debug"):
        print(e_area["state"], "State net", d_state_net)
        print(e_area["state"], "State ROI", d_state_roi)
        print(e_area["state"], "State CAP", d_state_cap)
        print(e_area["state"], "State Property Tax", d_state_property_tax)
    
    d_area_price = e_area.get("price")
    d_state_price = kwargs["state_info"].get("price") if kwargs.get("state_info", {}).get("price") else d_area_price
    d_area_price_sqft = e_area.get("price_per_sqft")
    d_state_price_sqft = kwargs["state_info"].get("price_per_sqft") if kwargs.get("state_info", {}).get("price_per_sqft") else d_area_price_sqft
    
    d_price = round(shrink(min(i_area_properties, i_zillow_properties), d_price, d_state_price), 2)
    if kwargs.get("b_debug"): print("d_price", d_price)
    if d_state_price ** d_MIN_PRICE_EXP > d_price:
        if kwargs.get("b_debug"): print(f"use lower limit for {s_state} of {d_state_price ** d_MIN_PRICE_EXP} instead of {d_price}")
        d_price = round(max(d_price, d_state_price ** d_MIN_PRICE_EXP), 2)
    
    d_area_rev = e_area.get("revenue")
    d_area_rent = e_area.get("rent")
    
    d_state_rev = kwargs["state_info"].get("revenue") if kwargs.get("state_info", {}).get("revenue") else d_area_rev
    d_area_rev_sqft = e_area.get("rev_per_sqft")
    d_state_rev_sqft = kwargs["state_info"].get("rev_per_sqft") if kwargs.get("state_info", {}).get("rev_per_sqft") else d_area_rev_sqft
    
    d_area_rent_sqft = e_area.get("rent_per_sqft")
    d_state_rent_sqft = kwargs["state_info"].get("rent_per_sqft") if kwargs.get("state_info", {}).get("rent_per_sqft") else d_area_rent_sqft
    
    d_state_rate = kwargs["state_info"].get("rate") if kwargs.get("state_info", {}).get("rate") else d_AVG_RATE
    d_state_rent = kwargs["state_info"].get("rent") if kwargs.get("state_info", {}).get("rent") else d_AVG_RENT
    
    d_estimated_price = estimate_price(d_area_rev)
    if d_price < d_estimated_price * 0.5 and 0 < i_zillow_properties < 25:
        d_price = round((d_price + d_estimated_price) / 2, 2)
    
    
    d_area_penetration = e_area.get("short_rental_penetration_normalized", 0)
    d_state_penetration = kwargs["state_info"].get("short_rental_penetration_normalized") if kwargs.get("state_info", {}) else d_area_penetration
    d_area_ltr_penetration = e_area.get("long_rental_penetration_normalized", 0)
    d_state_ltr_penetration = kwargs["state_info"].get("long_rental_penetration_normalized") if kwargs.get("state_info", {}) else d_area_ltr_penetration
    #d_area_sale_penetration = e_area.get("sale_penetration_normalized", 0)
    #d_state_sale_penetration = kwargs["state_info"].get("sale_penetration_normalized") if kwargs.get("state_info", {}) else d_area_ltr_penetration
    
    d_area_sale_days_on_market = e_area.get("sale_days_on_market", 0)
    d_state_sale_days_on_market = kwargs["state_info"].get("sale_days_on_market") if kwargs.get("state_info", {}) else d_area_sale_days_on_market
    d_area_ltr_days_on_market = e_area.get("rent_days_on_market", 0)
    d_state_ltr_days_on_market = kwargs["state_info"].get("rent_days_on_market") if kwargs.get("state_info", {}) else d_area_ltr_days_on_market
    d_area_ltr_turnover = e_area.get("rent_absorption_rate", 0)
    d_state_ltr_turnover = kwargs["state_info"].get("rent_absorption_rate") if kwargs.get("state_info", {}) else d_area_ltr_turnover
    
    d_area_occupancy = e_area.get("occupancy", 0)
    d_state_occupancy = kwargs["state_info"].get("occupancy") if kwargs.get("state_info") else d_area_occupancy
    
    a_zip_per_month_rate = []
    a_zip_per_month_occupancy = []
    a_city_per_month_rate = []
    a_city_per_month_occupancy = []
    a_state_per_month_rate = []
    a_state_per_month_occupancy = []
    
    if "zip" in s_type:
        a_zip_per_month_rate = [
            e_area.get(f"rate_month_{i}", 0)
            for i in range(1, 12 + 1)]
        a_zip_per_month_occupancy = [
            e_area.get(f"occupancy_month_{i}", 0)
            for i in range(1, 12 + 1)]
    if "city" in s_type:
        a_city_per_month_rate = [
            e_area.get(f"rate_month_{i}", 0)
            for i in range(1, 12 + 1)]
        a_city_per_month_occupancy = [
            e_area.get(f"occupancy_month_{i}", 0)
            for i in range(1, 12 + 1)]
    if "state" in s_type:
        a_state_per_month_rate = [
            e_area.get(f"rate_month_{i}", 0)
            for i in range(1, 12 + 1)]
        a_state_per_month_occupancy = [
            e_area.get(f"occupancy_month_{i}", 0)
            for i in range(1, 12 + 1)]
    
    
    e_str_score = score_str(
        i_bedrooms=i_bedrooms,
        i_bathrooms=i_bathrooms,
        d_living_area=d_living_area,
        d_lot_size=d_lot_size,
        d_price=d_price,
        d_revenue=d_revenue,
        d_avg_rate=d_avg_rate,
        d_occupancy=d_occupancy,
        d_state_factor=d_state_factor,
        d_state_wage_rate=d_state_wage_rate,
        
        d_zip_price_sqft=d_area_price_sqft,
        d_city_price_sqft=d_area_price_sqft,
        d_state_price_sqft=d_state_price_sqft,
        d_zip_rev_sqft=d_area_rev_sqft,
        d_city_rev_sqft=d_area_rev_sqft,
        d_state_rev_sqft=d_state_rev_sqft,
        
        d_zip_price=d_area_price,
        d_city_price=d_area_price,
        d_state_price=d_state_price,
        d_zip_rev=d_area_rev,
        d_city_rev=d_area_rev,
        d_state_rev=d_state_rev,
        
        d_zip_rate=d_avg_rate,
        d_city_rate=d_avg_rate,
        d_state_rate=d_state_rate,
        
        a_zip_per_month_rate=a_zip_per_month_rate,
        a_zip_per_month_occupancy=a_zip_per_month_occupancy,
        a_city_per_month_rate=a_city_per_month_rate,
        a_city_per_month_occupancy=a_city_per_month_occupancy,
        a_state_per_month_rate=a_state_per_month_rate,
        a_state_per_month_occupancy=a_state_per_month_occupancy,
        
        d_area_penetration=d_area_penetration,
        d_state_penetration=d_state_penetration,
        d_area_occupancy=d_area_occupancy,
        d_state_occupancy=d_state_occupancy,
        d_state_net=d_state_net,
        d_state_roi=d_state_roi,
        d_state_cap=d_state_cap,
        d_state_property_tax=d_state_property_tax,
        
        i_area_properties=i_area_properties,
        i_zip_properties=i_zip_properties,
        i_city_properties=i_city_properties,
        i_state_properties=i_state_properties,
        i_zillow_properties=i_zillow_properties,
        i_zip_zillow_properties=i_zip_zillow_properties,
        i_city_zillow_properties=i_city_zillow_properties,
        i_state_zillow_properties=i_state_zillow_properties,
        i_area_houses=i_area_houses,
        s_state=s_state,
        e_local_data={"zip": e_area, "city": e_area, "state": kwargs["state_info"]},
        **kwargs
    )
    
    e_ltr_score = score_ltr(
        i_bedrooms=i_bedrooms,
        i_bathrooms=i_bathrooms,
        d_living_area=d_living_area,
        d_lot_size=d_lot_size,
        d_price=d_price,
        d_rent=d_avg_rent,
        d_state_factor=d_state_factor,
        d_state_wage_rate=d_state_wage_rate,
        
        d_zip_price_sqft=d_area_price_sqft,
        d_city_price_sqft=d_area_price_sqft,
        d_state_price_sqft=d_state_price_sqft,
        d_zip_rent_sqft=d_area_rent_sqft,
        d_city_rent_sqft=d_area_rent_sqft,
        d_state_rent_sqft=d_state_rent_sqft,
        
        d_zip_price=d_area_price,
        d_city_price=d_area_price,
        d_state_price=d_state_price,
        d_zip_rent=d_area_rent,
        d_city_rent=d_area_rent,
        d_state_rent=d_state_rent,
        
        d_area_rent_penetration=d_area_ltr_penetration,
        d_zip_rent_penetration=d_area_ltr_penetration,
        d_city_rent_penetration=d_area_ltr_penetration,
        d_state_rent_penetration=d_state_ltr_penetration,
        
        #d_area_sale_penetration=d_area_sale_penetration,
        #d_zip_sale_penetration=d_area_sale_penetration,
        #d_city_sale_penetration=d_area_sale_penetration,
        #d_state_sale_penetration=d_state_sale_penetration,
        
        d_area_rent_days_on_market=d_area_ltr_days_on_market,
        d_zip_rent_days_on_market=d_area_ltr_days_on_market,
        d_city_rent_days_on_market=d_area_ltr_days_on_market,
        d_state_rent_days_on_market=d_state_ltr_days_on_market,
        
        d_area_sale_days_on_market=d_area_sale_days_on_market,
        d_zip_sale_days_on_market=d_area_sale_days_on_market,
        d_city_sale_days_on_market=d_area_sale_days_on_market,
        d_state_sale_days_on_market=d_state_sale_days_on_market,
        
        d_area_turnover=d_area_ltr_turnover,
        d_zip_sale_turnover=d_area_ltr_turnover,
        d_city_sale_turnover=d_area_ltr_turnover,
        d_state_turnover=d_state_ltr_turnover,
        
        d_state_net=d_state_net,
        d_state_roi=d_state_roi,
        d_state_cap=d_state_cap,
        d_state_property_tax=d_state_property_tax,
        
        i_area_houses=i_area_houses,
        s_state=s_state,
        
        i_area_sale_properties=i_area_sale_properties,
        i_zip_sale_properties=i_zip_sale_properties,
        i_city_sale_properties=i_city_sale_properties,
        i_state_sale_properties=i_state_sale_properties,
        
        i_area_rent_properties=i_area_sale_properties,
        i_zip_rent_properties=i_zip_rent_properties,
        i_city_rent_properties=i_city_rent_properties,
        i_state_rent_properties=i_state_rent_properties,
        e_area=e_area,
        e_inputs=e_area,
        e_local_data={"city": e_area, "zip": e_area, "state": kwargs["state_info"]},
        **kwargs
    )
    return {"str": e_str_score, "ltr": e_ltr_score}


#@utils.timer
def score_property(e_address, e_inputs, e_predictions, e_local_data, e_outputs={}, **kwargs):
    
    i_bedrooms = e_inputs["bedrooms"]
    i_bathrooms = e_inputs["bathrooms"]
    d_living_area = e_inputs["living_area"]
    d_lot_size = e_inputs["lot_size"]
    d_price = e_inputs["price"]
    d_revenue = e_predictions["predicted_revenue_optimized"]
    d_occupancy = e_predictions.get("predicted_occupancy", 0)
    d_avg_rate = e_predictions["avg_rate_at_predicted_revenue"]
    d_rent = e_predictions["predicted_rent"]
    
    assert not pd.isnull(i_bedrooms) and i_bedrooms >= 0
    assert not pd.isnull(i_bathrooms) and i_bathrooms >= 0
    assert not pd.isnull(d_living_area) and d_living_area > 0
    assert not pd.isnull(d_lot_size) and d_lot_size > 0
    assert not pd.isnull(d_price) and d_price > 0
    assert not pd.isnull(d_revenue)
    assert not pd.isnull(d_occupancy)
    assert not pd.isnull(d_avg_rate)
    assert not pd.isnull(d_rent)
    
    s_state = e_address.get("address_components", {}).get("state", [""])[0]
    s_state = us_states.e_STATE_ABBREV.get(s_state.lower(), s_state.lower())
    d_state_factor, d_state_wage_rate, d_state_net, d_state_roi, d_state_cap, d_state_property_tax, d_state_insurance = get_state_factors(s_state)
    
    d_zip_price_sqft = e_local_data["zip"]["price_per_sqft"] if e_local_data.get("zip", {}).get("price_per_sqft", 0) else 0
    d_city_price_sqft = e_local_data["city"]["price_per_sqft"] if e_local_data.get("city", {}).get("price_per_sqft", 0) else 0
    d_state_price_sqft = e_local_data["state"]["price_per_sqft"] if e_local_data.get("state", {}).get("price_per_sqft", 0) else d_AVG_PRICE_SQFT
    
    d_zip_rev_sqft = e_local_data["zip"]["rev_per_sqft"] if e_local_data.get("zip", {}).get("rev_per_sqft", 0) else 0
    d_city_rev_sqft = e_local_data["city"]["rev_per_sqft"] if e_local_data.get("city", {}).get("rev_per_sqft", 0) else 0
    d_state_rev_sqft = e_local_data["state"]["rev_per_sqft"] if e_local_data.get("state", {}).get("rev_per_sqft", 0) else d_STR_AVG_REV_SQFT
    
    d_zip_rent_sqft = e_local_data["zip"]["rent_per_sqft"] if e_local_data.get("zip", {}).get("rent_per_sqft", 0) else 0
    d_city_rent_sqft = e_local_data["city"]["rent_per_sqft"] if e_local_data.get("city", {}).get("rent_per_sqft", 0) else 0
    d_state_rent_sqft = e_local_data["state"]["rent_per_sqft"] if e_local_data.get("state", {}).get("rent_per_sqft", 0) else d_LTR_AVG_RENT_SQFT
    
    d_zip_price = e_local_data["zip"]["price"] if e_local_data.get("zip", {}).get("price", 0) else 0
    d_city_price = e_local_data["city"]["price"] if e_local_data.get("city", {}).get("price", 0) else 0
    d_state_price = e_local_data["state"]["price"] if e_local_data.get("state", {}).get("price", 0) else d_AVG_PRICE
    
    d_zip_rev = e_local_data["zip"]["revenue"] if e_local_data.get("zip", {}).get("revenue", 0) else 0
    d_city_rev = e_local_data["city"]["revenue"] if e_local_data.get("city", {}).get("revenue", 0) else 0
    d_state_rev = e_local_data["state"]["revenue"] if e_local_data.get("state", {}).get("revenue", 0) else d_STR_AVG_REV
    
    d_zip_rate = e_local_data["zip"]["rate"] if e_local_data.get("zip", {}).get("rate", 0) else 0
    d_city_rate = e_local_data["city"]["rate"] if e_local_data.get("city", {}).get("rate", 0) else 0
    d_state_rate = e_local_data["state"]["rate"] if e_local_data.get("state", {}).get("rate", 0) else d_AVG_RATE
        
    i_zip_properties = e_local_data["zip"]["area_properties"] if e_local_data.get("zip", {}).get("area_properties", 0) else 0
    i_city_properties = e_local_data["city"]["area_properties"] if e_local_data.get("city", {}).get("area_properties", 0) else 0
    i_state_properties = e_local_data["state"]["area_properties"] if e_local_data.get("state", {}).get("area_properties", 0) else 0
    
    i_zip_zillow_properties = e_local_data["zip"]["zillow_properties"] if e_local_data.get("zip", {}).get("zillow_properties", 0) else 0
    i_city_zillow_properties = e_local_data["city"]["zillow_properties"] if e_local_data.get("city", {}).get("zillow_properties", 0) else 0
    i_state_zillow_properties = e_local_data["state"]["zillow_properties"] if e_local_data.get("state", {}).get("zillow_properties", 0) else 0
    
    i_zip_houses = e_local_data["zip"]["houses"] if e_local_data.get("zip", {}).get("houses", 0) else 0
    i_city_houses = e_local_data["city"]["houses"] if e_local_data.get("city", {}).get("houses", 0) else 0
     
    d_area_penetration = e_local_data.get("zip", {}).get("short_rental_penetration_normalized", e_local_data.get("city", {}).get("short_rental_penetration_normalized", 0))
    d_state_penetration = e_local_data.get("state", {}).get("short_rental_penetration_normalized", 0)
    d_area_occupancy = e_local_data.get("zip", {}).get("occupancy", e_local_data.get("city", {}).get("occupancy", 0))
    d_state_occupancy = e_local_data.get("state", {}).get("occupancy", 0)
    
    d_area_sale_days_on_market = e_local_data.get("zip", {}).get("sale_days_on_market", e_local_data.get("city", {}).get("sale_days_on_market", 0))
    d_state_sale_days_on_market = e_local_data.get("state", {}).get("sale_days_on_market", 0)
    d_area_sale_turnover = e_local_data.get("zip", {}).get("sale_absorption_rate", e_local_data.get("city", {}).get("sale_absorption_rate", 0))
    d_area_sale_state_turnover = e_local_data.get("state", {}).get("sale_absorption_rate", 0)
    
    #LTR specific area values
    d_area_ltr_penetration = e_local_data.get("zip", {}).get("long_rental_penetration_normalized", e_local_data.get("city", {}).get("long_rental_penetration_normalized", 0))
    d_state_ltr_penetration = e_local_data.get("state", {}).get("long_rental_penetration_normalized", 0)
    
    d_area_ltr_days_on_market = e_local_data.get("zip", {}).get("rent_days_on_market", e_local_data.get("city", {}).get("rent_days_on_market", 0))
    d_state_ltr_days_on_market = e_local_data.get("state", {}).get("rent_days_on_market", 0)
    d_area_ltr_turnover = e_local_data.get("zip", {}).get("rent_absorption_rate", e_local_data.get("city", {}).get("rent_absorption_rate", 0))
    d_state_ltr_turnover = e_local_data.get("state", {}).get("rent_absorption_rate", 0)
    
    d_zip_rent = e_local_data["zip"]["rent"] if e_local_data.get("zip", {}).get("rent", 0) else 0
    d_city_rent = e_local_data["city"]["rent"] if e_local_data.get("city", {}).get("rent", 0) else 0
    d_state_rent = e_local_data["state"]["rent"] if e_local_data.get("state", {}).get("rent", 0) else d_AVG_RENT
    
    if "zip" in e_local_data:
        a_zip_per_month_rate = [
            e_local_data["zip"].get(f"rate_month_{i}", 0)
            for i in range(1, 12 + 1)]
        a_zip_per_month_occupancy = [
            e_local_data["zip"].get(f"occupancy_month_{i}", 0)
            for i in range(1, 12 + 1)]
    if "city" in e_local_data:
        a_city_per_month_rate = [
            e_local_data["city"].get(f"rate_month_{i}", 0)
            for i in range(1, 12 + 1)]
        a_city_per_month_occupancy = [
            e_local_data["city"].get(f"occupancy_month_{i}", 0)
            for i in range(1, 12 + 1)]
    if "state" in e_local_data:
        a_state_per_month_rate = [
            e_local_data["state"].get(f"rate_month_{i}", 0)
            for i in range(1, 12 + 1)]
        a_state_per_month_occupancy = [
            e_local_data["state"].get(f"occupancy_month_{i}", 0)
            for i in range(1, 12 + 1)]
    
    e_str_score = score_str(
        i_bedrooms=i_bedrooms,
        i_bathrooms=i_bathrooms,
        d_living_area=d_living_area,
        d_lot_size=d_lot_size,
        d_price=d_price,
        d_revenue=d_revenue,
        d_avg_rate=d_avg_rate,
        d_occupancy=d_occupancy,
        d_state_factor=d_state_factor,
        d_state_wage_rate=d_state_wage_rate,
        
        d_zip_price_sqft=d_zip_price_sqft,
        d_city_price_sqft=d_city_price_sqft,
        d_state_price_sqft=d_state_price_sqft,
        d_zip_rev_sqft=d_zip_rev_sqft,
        d_city_rev_sqft=d_city_rev_sqft,
        d_state_rev_sqft=d_state_rev_sqft,
        
        d_zip_price=d_zip_price,
        d_city_price=d_city_price,
        d_state_price=d_state_price,
        d_zip_rev=d_zip_rev,
        d_city_rev=d_city_rev,
        d_state_rev=d_state_rev,
        
        d_zip_rate=d_zip_rate,
        d_city_rate=d_city_rate,
        d_state_rate=d_state_rate,
        
        a_zip_per_month_rate=a_zip_per_month_rate,
        a_zip_per_month_occupancy=a_zip_per_month_occupancy,
        a_city_per_month_rate=a_city_per_month_rate,
        a_city_per_month_occupancy=a_city_per_month_occupancy,
        a_state_per_month_rate=a_state_per_month_rate,
        a_state_per_month_occupancy=a_state_per_month_occupancy,
        
        d_area_penetration=d_area_penetration,
        d_state_penetration=d_state_penetration,
        d_area_occupancy=d_area_occupancy,
        d_state_occupancy=d_state_occupancy,
        d_state_net=d_state_net,
        d_state_roi=d_state_roi,
        d_state_cap=d_state_cap,
        d_state_property_tax=d_state_property_tax,
        
        i_area_properties=(i_zip_properties + i_city_properties) / 2,
        i_zip_properties=i_zip_properties,
        i_city_properties=i_city_properties,
        i_state_properties=i_state_properties,
        i_zillow_properties=(i_zip_zillow_properties + i_city_zillow_properties) / 2,
        i_zip_zillow_properties=i_zip_zillow_properties,
        i_city_zillow_properties=i_city_zillow_properties,
        i_state_zillow_properties=i_state_zillow_properties,
        i_area_houses=(i_zip_houses + i_city_houses) / 2,
        s_state=s_state,
        **kwargs
    )
    
    e_ltr_score = score_ltr(
        i_bedrooms=i_bedrooms,
        i_bathrooms=i_bathrooms,
        d_living_area=d_living_area,
        d_lot_size=d_lot_size,
        d_price=d_price,
        d_rent=d_rent,
        d_state_factor=d_state_factor,
        d_state_wage_rate=d_state_wage_rate,
        
        d_zip_price_sqft=d_zip_price_sqft,
        d_city_price_sqft=d_city_price_sqft,
        d_state_price_sqft=d_state_price_sqft,
        d_zip_rent_sqft=d_zip_rent_sqft,
        d_city_rent_sqft=d_city_rent_sqft,
        d_state_rent_sqft=d_state_rent_sqft,
        
        d_zip_price=d_zip_price,
        d_city_price=d_city_price,
        d_state_price=d_state_price,
        d_zip_rent=d_zip_rent,
        d_city_rent=d_city_rent,
        d_state_rent=d_state_rent,
        
        d_area_rent_penetration=d_area_ltr_penetration,
        d_zip_rent_penetration=e_local_data.get("zip", {}).get("long_rental_penetration_normalized", 0),
        d_city_rent_penetration=e_local_data.get("city", {}).get("long_rental_penetration_normalized", 0),
        d_state_rent_penetration=d_state_ltr_penetration,
        
        d_area_rent_days_on_market=d_area_ltr_days_on_market,
        d_zip_rent_days_on_market=e_local_data.get("zip", {}).get("rent_days_on_market", d_state_ltr_days_on_market),
        d_city_rent_days_on_market=e_local_data.get("city", {}).get("rent_days_on_market", d_state_ltr_days_on_market),
        d_state_rent_days_on_market=d_state_ltr_days_on_market,
        
        d_area_sale_days_on_market=d_area_sale_days_on_market,
        d_zip_sale_days_on_market=e_local_data.get("zip", {}).get("sale_days_on_market", d_state_sale_days_on_market),
        d_city_sale_days_on_market=e_local_data.get("city", {}).get("sale_days_on_market", d_state_sale_days_on_market),
        d_state_sale_days_on_market=d_state_sale_days_on_market,
        
        d_area_turnover=d_area_ltr_turnover,
        d_zip_sale_turnover=e_local_data.get("zip", {}).get("rent_absorption_rate", d_state_ltr_turnover),
        d_city_sale_turnover=e_local_data.get("city", {}).get("rent_absorption_rate", d_state_ltr_turnover),
        d_state_turnover=d_state_ltr_turnover,
        
        d_state_net=d_state_net,
        d_state_roi=d_state_roi,
        d_state_cap=d_state_cap,
        d_state_property_tax=d_state_property_tax,
        
        
        i_area_sale_properties=(i_zip_properties + i_city_properties) / 2,
        i_zip_sale_properties=e_local_data.get("zip", {}).get("sale_properties", 0),
        i_city_sale_properties=e_local_data.get("city", {}).get("sale_properties", 0),
        i_state_sale_properties=e_local_data.get("state", {}).get("sale_properties", 0),
        
        i_area_rent_properties=(i_zip_zillow_properties + i_city_zillow_properties) / 2,
        i_zip_rent_properties=e_local_data.get("zip", {}).get("rent_properties", 0),
        i_city_rent_properties=e_local_data.get("city", {}).get("rent_properties", 0),
        i_state_rent_properties=e_local_data.get("state", {}).get("rent_properties", 0),
        
        i_area_houses=(i_zip_houses + i_city_houses) / 2,
        s_state=s_state,
        e_local_data=e_local_data,
        e_inputs=e_inputs,
        **kwargs
    )
    return {"str": e_str_score, "ltr": e_ltr_score}


#@utils.timer
def score_str(
    i_bedrooms,
    i_bathrooms,
    d_living_area,
    d_lot_size,
    d_price,
    d_revenue,
    d_avg_rate,
    d_occupancy,
    d_state_factor,
    d_state_wage_rate,
    d_zip_price_sqft,
    d_city_price_sqft,
    d_state_price_sqft,
    d_zip_rev_sqft,
    d_city_rev_sqft,
    d_state_rev_sqft,
    d_zip_price,
    d_city_price,
    d_state_price,
    d_zip_rev,
    d_city_rev,
    d_state_rev,
    
    d_zip_rate,
    d_city_rate,
    d_state_rate,
    
    a_zip_per_month_rate,
    a_zip_per_month_occupancy,
    a_city_per_month_rate,
    a_city_per_month_occupancy,
    a_state_per_month_rate,
    a_state_per_month_occupancy,
    
    d_area_penetration,
    d_state_penetration,
    d_area_occupancy,
    d_state_occupancy,
    d_state_net,
    d_state_roi,
    d_state_cap,
    d_state_property_tax,
    e_per_month_costs=None,
    
    **kwargs):
    
    get_score_weights()
    
    #e_address
    #e_inputs
    #e_predictions
    #e_local_data = kwargs.get("local_data", {})
    i_area_properties = kwargs.get("i_area_properties", 0)
    i_zip_properties = kwargs.get("i_zip_properties", 0)
    i_city_properties = kwargs.get("i_city_properties", 0)
    i_state_properties = kwargs.get("i_state_properties", 0)
    i_area_houses = kwargs.get("i_area_houses", 0)
    i_zillow_properties = kwargs.get("i_zillow_properties", 0)
    i_zip_zillow_properties = kwargs.get("i_zip_zillow_properties", 0)
    i_city_zillow_properties = kwargs.get("i_city_zillow_properties", 0)
    i_state_zillow_properties = kwargs.get("i_state_zillow_properties", 0)
    
    #print("i_area_properties", i_area_properties)
    
    d_average_salary = d_AVG_SALARY * d_state_wage_rate
    #print(d_average_salary)
    d_salary_cost = round(d_average_salary / i_STR_MAX_PROPS_TO_MANAGE, 2)
    if kwargs.get("b_debug"): print("Time cost:", d_salary_cost)
    
    d_property_tax_cost = round(d_state_property_tax * d_price, 2)
    if kwargs.get("b_debug"): print("Property tax:", d_property_tax_cost)
    
    if e_per_month_costs:
        for i_month, e_mon in e_per_month_costs.items():
            if "property_tax" not in e_mon.items():
                e_mon["property_tax"] = round(d_property_tax_cost / 12, 2)
            
            e_mon["total"] = round(sum([v for k, v in e_mon.items() if k not in {"total"} and isinstance(v, (int, float))]), 2)
        d_other_costs = sum([e_mon["total"] for e_mon in e_per_month_costs.values()])
    
    else:
    
        d_cleanings_per_year = max(i_MIN_CLEANS, min(i_MAX_CLEANS, d_occupancy * d_CLEANING_OCCUPANCY_FACTOR))
        #print(d_cleanings_per_year)
        d_cost_per_clean = (((d_CLEANING_FEE_PER_ROOM * (i_bedrooms + i_bathrooms)) + (d_living_area * d_CLEANING_FEE_PER_SQFT)) / 2) * d_state_factor
        #print(d_cost_per_clean)
        d_cleaning_cost = round(d_cleanings_per_year * d_cost_per_clean, 2)
        if kwargs.get("b_debug"): print("Cleaning cost:", d_cleaning_cost)
        
        if d_lot_size <= 1:
            d_lawn_size = (d_lot_size * 43560) - (d_living_area * 1.25)
        else:
            d_lawn_size = (min(10, d_lot_size) ** 0.25 * 43560) - (d_living_area * 1.25)
        if kwargs.get("b_debug"): print("Lawn size:", d_lawn_size)
        if d_lawn_size > 0:
            d_lawn_cost = round((0.09 * d_state_factor) * (d_lawn_size ** 0.95), 2)
        else:
            d_lawn_cost = 0
        if kwargs.get("b_debug"): print("Lawn cost:", d_lawn_cost)
        #d_lawn_cost = (0.11 * d_state_wage_rate) * (min(1, d_lot_size) * 43560)
        s_state = kwargs.get("s_state")
        e_rates = get_state_utilities(s_state)
        
        d_utility_costs = round(sum([v for k, v in e_rates.items() if k not in {"total"} and isinstance(v, (int, float))]) * 12, 2)
        
        d_other_costs = d_cleaning_cost + d_lawn_cost + d_utility_costs + d_property_tax_cost
        
    if 0 < i_state_properties < 5000:
        if kwargs.get("b_debug"): print("state vs us net", d_state_net, d_STR_AVG_NET)
        d_state_net = round(shrink_state(i_state_properties, d_state_net, d_STR_AVG_NET), 2)
    
    #d_net = round(d_revenue - d_other_costs - d_salary_cost, 2)
    
    d_listing_cost = d_revenue * d_LISTING_FEE_RATE
    if kwargs.get("b_debug"): print("Listing cost", d_listing_cost)
    #d_management_cost = d_revenue * d_MANAGEMENT_FEE_RATE
    #if kwargs.get("b_debug"): print("Management cost", d_management_cost)
    
    d_net = round(d_revenue - d_other_costs - d_listing_cost, 2)
    
    d_orig_net = d_net
    if 0 < i_area_properties < 50:
        d_net = round(shrink(i_area_properties, d_net, d_state_net), 2)
        if kwargs.get("b_debug"): print(f"old net {d_orig_net}, new net {d_net}")
    if kwargs.get("b_debug"): print("Net", d_net)

    #d_abs_high_rev_score = 0 if d_revenue <= 1000 else min(1, (np.log10(d_revenue / 1000) / 2.695) ** 1.5) if d_revenue < 500000 else 1
    #d_abs_low_rev_score = 0 if d_revenue <= 100 else min(1, (np.log10(d_revenue / 1000) / 1.7) ** 2) if d_revenue < 50000 else 1
    #d_abs_rev_score = (d_abs_low_rev_score + d_abs_high_rev_score) / 2
    d_abs_rev_log_score = d_revenue / 330000 if d_revenue <= 100000 else min(1, (np.log10((d_revenue - 95000) / 1000) / 3)) if d_revenue < 1000000 else 1
    d_abs_rev_exp_score = min(1, d_revenue ** 0.75 / 32000) if d_revenue > 0 else 0
    d_abs_rev_linear_score = min(1, d_revenue / 350000) if d_revenue > 0 else 0
    d_high_low_share = min(1, max(0, d_revenue / 1000000))
    d_abs_rev_score = min(1, max(0, np.mean([d_abs_rev_exp_score, (d_abs_rev_log_score * (1 - d_high_low_share)) + (d_abs_rev_linear_score * d_high_low_share)]))) ** 0.3
    
    
    #d_abs_high_net_score = 0 if d_net <= 1000 else min(1, (np.log10(d_net / 1000) / 2.695) ** 1.5) if d_net < 500000 else 1
    #d_abs_low_net_score = 0 if d_net <= 100 else min(1, (np.log10(d_net / 1000) / 1.7) ** 2) if d_net < 50000 else 1
    #d_abs_net_score = ((d_abs_low_net_score + d_abs_high_net_score) / 2) ** 0.5
    d_abs_net_log_score = d_net / 330000 if d_net <= 100000 else min(1, (np.log10((d_net - 95000) / 1000) / 3)) if d_net < 1000000 else 1
    d_abs_net_exp_score = min(1, d_net ** 0.75 / 32000) if d_net > 0 else 0
    d_abs_net_linear_score = min(1, d_net / 500000) if d_net > 0 else 0
    d_abs_rev_linear_score = min(1, d_revenue / 350000) if d_revenue > 0 else 0
    d_high_low_share = min(1, max(0, d_revenue / 1000000))
    d_abs_net_score = min(1, max(0, np.mean([d_abs_net_exp_score, (d_abs_net_log_score * (1 - d_high_low_share)) + (d_abs_net_linear_score * d_high_low_share)]))) ** 0.2
    
    
    #print(d_net, round(d_abs_low_rev_score, 3), round(d_abs_high_rev_score, 3), round(d_abs_rev_score, 3))
    if kwargs.get("b_debug"): print("\nAbs Revenue Score", round(d_abs_rev_score, 3))
    if kwargs.get("b_debug"): print("\nAbs Net Score", round(d_abs_net_score, 3))
    
    d_abs_occupancy_score = d_occupancy ** 0.8 #0.6
    
    d_expected_cap = defaults.predict_price_revenue_median(d_price) / (d_price / i_RETURN_YEARS_TIMEFRAME)
    d_actual_cap = d_revenue / (d_price / i_RETURN_YEARS_TIMEFRAME)
    
    d_cap_performance = d_actual_cap / d_expected_cap
    d_cap_performance_score = np.tanh(d_cap_performance) ** 0.8
    #if 0 < i_area_properties < 50:
    #    d_cap_performance_score = shrink(i_area_properties, d_cap_performance_score, 0.5)
    if kwargs.get("b_debug"): print("Cap vs Expected Score", round(d_cap_performance_score, 3))
    
    #d_abs_rate_score = 0 if d_avg_rate <= 50 else np.log10((d_avg_rate + 50) / 100) if d_avg_rate < 950 else 1
    if not d_state_rate: d_state_rate = d_AVG_RATE
    if 0 < i_area_properties < 50:
        d_avg_rate = shrink(i_area_properties, d_avg_rate, d_state_rate)
    d_abs_rate_score = max(0, min(1, 0 if d_avg_rate <= 0 else np.log10((d_avg_rate + 100) / 160))) ** 0.75
    if kwargs.get("b_debug"): print("Abs Daily rate score", round(d_abs_rate_score, 3))
    
    d_market_score = get_market_score(
        d_zip_price_sqft,
        d_city_price_sqft,
        d_state_price_sqft,
        d_zip_rev_sqft,
        d_city_rev_sqft,
        d_state_rev_sqft,
        d_zip_price,
        d_city_price,
        d_state_price,
        d_zip_rev,
        d_city_rev,
        d_state_rev,
        i_area_properties=i_area_properties,
        i_zip_properties=i_zip_properties,
        i_city_properties=i_city_properties,
        i_state_properties=i_state_properties,
        i_zillow_properties=i_zillow_properties,
        i_zip_zillow_properties=i_zip_zillow_properties,
        i_city_zillow_properties=i_city_zillow_properties,
        i_state_zillow_properties=i_state_zillow_properties,
    )
    if kwargs.get("b_debug"): print("Market price/rev score", round(d_market_score, 3))
    
    d_market_rate_score = get_market_rate_score(
        d_zip_rate=d_zip_rate,
        d_city_rate=d_city_rate,
        d_state_rate=d_state_rate,
    )
    if kwargs.get("b_debug"): print("Market rate score", round(d_market_rate_score, 3))
        
    d_market_opportunity_score, d_market_size_score = get_market_opportunity_score(
        d_area_penetration,
        d_state_penetration,
        d_area_occupancy,
        d_state_occupancy,
        i_area_houses,
        i_area_properties=i_area_properties,
        i_state_properties=i_state_properties,
    )
    if kwargs.get("b_debug"): print("Market opportunity score", round(d_market_opportunity_score, 3))
    
    #d_area_penetration = shrink(min(i_area_properties, i_zillow_properties), d_area_penetration, d_state_penetration)
    d_market_penetration_score = d_area_penetration ** 0.75
    if kwargs.get("b_debug"): print("Market penetration score", round(d_market_penetration_score, 3))
    
    d_market_seasonality_score, d_market_price_dynamism_score = get_market_seasonality_score(
        a_zip_per_month_rate,
        a_zip_per_month_occupancy,
        a_city_per_month_rate,
        a_city_per_month_occupancy,
        a_state_per_month_rate,
        a_state_per_month_occupancy,
        **kwargs)
    
    
    if 0 < i_state_properties < 5000:
        d_state_roi = shrink_state(i_state_properties, d_state_roi, d_STR_AVG_NET_ROI)
        d_state_cap = shrink_state(i_state_properties, d_state_cap, d_STR_AVG_CAP)
    
    d_raw_roi = d_net / d_price if d_price > 0 else 1
    d_roi = d_net / (d_price / i_RETURN_YEARS_TIMEFRAME) if d_price > 0 else 1
    if 0 < min(i_area_properties, i_zillow_properties) < 50:
        d_roi = shrink(min(i_area_properties, i_zillow_properties), d_roi, (d_state_roi * i_RETURN_YEARS_TIMEFRAME))
    
    d_roi_score = np.tanh(d_roi) ** 0.3 if d_roi > 0 else 0
    if kwargs.get("b_debug"): print("ROI score", round(d_roi_score, 3))
    
    d_cap = d_revenue / (d_price / i_RETURN_YEARS_TIMEFRAME)
    if 0 < min(i_area_properties, i_zillow_properties) < 50:
        d_cap = shrink(min(i_area_properties, i_zillow_properties), d_cap, (d_state_cap * i_RETURN_YEARS_TIMEFRAME))
    d_cap_score = np.tanh(d_cap) ** 0.5 if d_cap > 0 else 0
    if kwargs.get("b_debug"): print("CAP score", round(d_cap_score, 3))
    #d_relative_score = (np.mean([
    #    d_roi_score,
    #    d_cap_performance_score
    #]) + (d_roi_score * d_roi_score)) / 2
    
    d_relative_score = combine_scores(
        {
            "roi": d_roi_score,
            "cap": d_cap_score,
            "expected": d_cap_performance_score,
        },
        e_STR_SCORE_WEIGHTS["relative"]) ** 0.7
    
    
    d_absolute_score = combine_scores(
        {
            "revenue": d_abs_rev_score,
            "rate": d_abs_rate_score,
            "net": d_abs_net_score,
            "occupancy": d_abs_occupancy_score,
        },
        e_STR_SCORE_WEIGHTS["absolute"]) ** 0.5
    
    d_area_score = combine_scores(
        {
            "cap": d_market_score,
            "rate": d_market_rate_score,
            "penetration": d_market_penetration_score,
            "opportunity": d_market_opportunity_score,
            "size": d_market_size_score,
            "seasonality": d_market_seasonality_score,
            "optimization": d_market_price_dynamism_score,
        },
        e_STR_SCORE_WEIGHTS["area"]) ** 0.75
    
    d_overall_score = combine_scores(
        {
            "relative": d_relative_score,
            "absolute": d_absolute_score,
            "area": d_area_score,
        },
        e_STR_SCORE_WEIGHTS["overall"]) ** 0.8
    
    if 0 < i_area_properties < 100:
        d_absolute_score = shrink(i_area_properties, d_absolute_score, 0.4)
    if kwargs.get("b_debug"): print("Overall score", round(d_overall_score, 3))
    
    return {
        "revenue_for_score": d_revenue,
        "price_for_score": d_price,
        "estimated_costs": d_other_costs, # + d_salary_cost,
        "time_cost": d_salary_cost,
        #"net": d_orig_net,
        "estimated_net": d_net,
        "estimated_roi": d_raw_roi,
        f"roi_over_{int(round(i_RETURN_YEARS_TIMEFRAME))}_years": d_roi,
        f"cap_over_{int(round(i_RETURN_YEARS_TIMEFRAME))}_years": d_cap,
        
        "score_overall": round(d_overall_score, i_SCORE_PRECISION),
        
        "score_relative": round(d_relative_score, i_SCORE_PRECISION),
        "score_absolute": round(d_absolute_score, i_SCORE_PRECISION),
        "score_area": round(d_area_score, i_SCORE_PRECISION),
        
        "score_relative_roi": round(d_roi_score, i_SCORE_PRECISION),
        "score_relative_cap": round(d_cap_score, i_SCORE_PRECISION),
        "score_relative_expected": round(d_cap_performance_score, i_SCORE_PRECISION),
        "score_absolute_revenue": round(d_abs_rev_score, i_SCORE_PRECISION),
        "score_absolute_net": round(d_abs_net_score, i_SCORE_PRECISION),
        "score_absolute_rate": round(d_abs_rate_score, i_SCORE_PRECISION),
        "score_absolute_occupancy": round(d_abs_occupancy_score, i_SCORE_PRECISION),
        "score_area_cap": round(d_market_score, i_SCORE_PRECISION),
        "score_area_rate": round(d_market_rate_score, i_SCORE_PRECISION),
        "score_area_penetration": round(d_market_penetration_score, i_SCORE_PRECISION),
        "score_area_opportunity": round(d_market_opportunity_score, i_SCORE_PRECISION),
        "score_area_size": round(d_market_size_score, i_SCORE_PRECISION),
        "score_area_seasonality": round(d_market_seasonality_score, i_SCORE_PRECISION),
        "score_area_optimization": round(d_market_price_dynamism_score, i_SCORE_PRECISION),
    }


#@utils.timer
def score_ltr(
    i_bedrooms,
    i_bathrooms,
    d_living_area,
    d_lot_size,
    d_price,
    d_rent,
    d_state_factor,
    d_state_wage_rate,
    d_zip_price_sqft,
    d_city_price_sqft,
    d_state_price_sqft,
    d_zip_rent_sqft,
    d_city_rent_sqft,
    d_state_rent_sqft,
    d_zip_price,
    d_city_price,
    d_state_price,
    
    d_zip_rent,
    d_city_rent,
    d_state_rent,
    
    d_area_rent_penetration,
    d_zip_rent_penetration,
    d_city_rent_penetration,
    d_state_rent_penetration,
    #d_area_sale_penetration,
    #d_state_sale_penetration,
    d_area_rent_days_on_market,
    d_zip_sale_days_on_market,
    d_city_sale_days_on_market,
    d_state_sale_days_on_market,
    d_area_sale_days_on_market,
    d_zip_rent_days_on_market,
    d_city_rent_days_on_market,
    d_state_rent_days_on_market,
    d_area_turnover,
    d_state_turnover,

    #d_zip_sale_turnover,
    #d_city_sale_turnover,
    #d_state_sale_turnover,
    #d_zip_rent_turnover,
    #d_city_rent_turnover,
    #d_state_rent_turnover,
    i_area_houses,
    #i_area_sale_properties,
    i_zip_sale_properties,
    i_city_sale_properties,
    i_state_sale_properties,
    #i_area_rent_properties,
    i_zip_rent_properties,
    i_city_rent_properties,
    i_state_rent_properties,
    
    d_state_net,
    d_state_roi,
    d_state_cap,
    
    d_state_property_tax,
    e_per_month_costs=None,
    
**kwargs):
    
    e_local_data = kwargs.get("e_local_data", {})
    e_inputs = kwargs.get("e_inputs", {})
    
    d_revenue = d_rent * 12
    
    get_score_weights()
    
    i_area_sale_properties = kwargs.get("i_area_sale_properties", 0)
    #i_zip_properties = kwargs.get("i_zip_sale_properties", 0)
    #i_city_properties = kwargs.get("i_city_sale_properties", 0)
    #i_state_properties = kwargs.get("i_state_sale_properties", 0)
    #i_area_houses = kwargs.get("i_area_houses", 0)
    i_area_rent_properties = kwargs.get("i_area_rent_properties", 0)
    #i_zip_rent_properties = kwargs.get("i_zip_rent_properties", 0)
    #i_city_rent_properties = kwargs.get("i_city_rent_properties", 0)
    #i_state_rent_properties = kwargs.get("i_state_rent_properties", 0)
    
    #print("i_area_sale_properties", i_area_sale_properties)
    
    d_average_salary = d_AVG_SALARY * d_state_wage_rate
    #print(d_average_salary)
    d_salary_cost = round(d_average_salary / i_LTR_MAX_PROPS_TO_MANAGE, 2)
    if kwargs.get("b_debug"): print("Time cost:", d_salary_cost)
    
    d_property_tax_cost = round(d_state_property_tax * d_price, 2)
    if kwargs.get("b_debug"): print("Property tax:", d_property_tax_cost)
    
    #d_listing_cost = d_revenue * d_LISTING_FEE_RATE
    #if kwargs.get("b_debug"): print("Listing cost", d_listing_cost)
    d_management_cost = d_revenue * d_LTR_MANAGEMENT_FEE_RATE
    if kwargs.get("b_debug"): print("Management cost", d_management_cost)
    d_maintenance_cost = d_revenue * d_LTR_MAINTENANCE_FEE
    
    d_input_property_tax_cost = 0
    d_input_management_fee = 0
    d_input_maintenance_fee = 0
    if e_per_month_costs:
        d_input_property_tax_cost = sum([0] + [
            e_mon.get("property_tax", 0) for e_mon in e_per_month_costs.values()])
        if d_input_property_tax_cost: d_property_tax_cost = d_input_property_tax_cost
        
        #d_input_management_fee = sum([0] + [
        #    e_mon.get("management_fee", 0) for e_mon in e_per_month_costs.items()])
        #if d_management_cost: d_management_cost = d_input_management_fee
        
        d_input_maintenance_fee = sum([0] + [
            e_mon.get("maintenance_fee", 0) for e_mon in e_per_month_costs.values()])
        if d_input_maintenance_fee: d_maintenance_cost = d_input_maintenance_fee
        
    #if 0 < i_state_properties < 5000:
    #    if kwargs.get("b_debug"): print("state vs us net", d_state_net, d_LTR_AVG_NET)
    #    d_state_net = round(shrink_state(i_state_properties, d_state_net, d_LTR_AVG_NET), 2)
    
    d_net = round(d_revenue - d_property_tax_cost - d_management_cost - d_maintenance_cost, 2)
    
    #d_orig_net = d_net
    #if 0 < i_area_sale_properties < 50:
    #    d_net = round(shrink(i_area_sale_properties, d_net, d_state_net), 2)
    #    if kwargs.get("b_debug"): print(f"old net {d_orig_net}, new net {d_net}")
    #if kwargs.get("b_debug"): print("Net", d_net)
    
    d_abs_rev_log_score = d_revenue / 48000 if d_revenue <= 20000 else min(1, (np.log10((d_revenue - 10000) / 500) / 3)) if d_revenue < 400000 else 1
    d_abs_rev_exp_score = min(1, d_revenue ** 0.25 / 25) if d_revenue > 0 else 0
    d_abs_rev_linear_score = min(1, d_revenue / 100000) if d_revenue > 0 else 0
    d_high_low_share = min(1, max(0, d_revenue / 400000))
    d_abs_rent_score = min(1, max(0, np.mean([d_abs_rev_exp_score, (d_abs_rev_log_score * (1 - d_high_low_share)) + (d_abs_rev_linear_score * d_high_low_share)]))) ** 0.9
    
    
    #d_abs_high_net_score = 0 if d_net <= 1000 else min(1, (np.log10(d_net / 1000) / 2.695) ** 1.5) if d_net < 500000 else 1
    #d_abs_low_net_score = 0 if d_net <= 100 else min(1, (np.log10(d_net / 1000) / 1.7) ** 2) if d_net < 50000 else 1
    #d_abs_net_score = ((d_abs_low_net_score + d_abs_high_net_score) / 2) ** 0.5
    d_abs_net_log_score = 0 if d_revenue < 0 else d_revenue / 28000 if d_revenue <= 15000 else min(1, (np.log10((d_revenue - 6000) / 200) / 3)) if d_revenue < 300000 else 1
    d_abs_net_exp_score = min(1, d_revenue ** 0.24 / 19) if d_revenue > 0 else 0
    d_abs_net_linear_score = min(1, d_net / 80000) if d_net > 0 else 0
    d_high_low_share = min(1, max(0, d_revenue / 400000))
    d_abs_net_score = min(1, max(0, np.mean([d_abs_net_exp_score, (d_abs_net_log_score * (1 - d_high_low_share)) + (d_abs_net_linear_score * d_high_low_share)]))) ** 1.2
    
    
    #print(d_net, round(d_abs_low_rev_score, 3), round(d_abs_high_rev_score, 3), round(d_abs_rent_score, 3))
    if kwargs.get("b_debug"): print("\nAbs Rent Score", round(d_abs_rent_score, 3))
    if kwargs.get("b_debug"): print("\nAbs Net Score", round(d_abs_net_score, 3))
    
    #d_abs_occupancy_score = d_occupancy ** 0.8 #0.6
    
    d_expected_cap = (defaults.predict_price_rent(d_price) * 12) / (d_price / i_RETURN_YEARS_TIMEFRAME)
    d_actual_cap = d_revenue / (d_price / i_RETURN_YEARS_TIMEFRAME)
    
    d_cap_performance = d_actual_cap / d_expected_cap
    d_cap_performance_score = np.tanh(d_cap_performance) ** 4
    #if 0 < i_area_sale_properties < 50:
    #    d_cap_performance_score = shrink(i_area_sale_properties, d_cap_performance_score, 0.5)
    if kwargs.get("b_debug"): print("Cap vs Expected Score", round(d_cap_performance_score, 3))
    
    d_market_score = get_ltr_market_score(
        d_zip_price_sqft,
        d_city_price_sqft,
        d_state_price_sqft,
        d_zip_rent_sqft,
        d_city_rent_sqft,
        d_state_rent_sqft,
        d_zip_price,
        d_city_price,
        d_state_price,
        d_zip_rent,
        d_city_rent,
        d_state_rent,
        i_area_rent_properties=i_area_rent_properties,
        i_zip_rent_properties=i_zip_rent_properties,
        i_city_rent_properties=i_city_rent_properties,
        i_state_rent_properties=i_state_rent_properties,
        i_area_sale_properties=i_area_sale_properties,
        i_zip_sale_properties=i_zip_sale_properties,
        i_city_sale_properties=i_city_sale_properties,
        i_state_sale_properties=i_state_sale_properties,
    )
    if kwargs.get("b_debug"): print("Market score", round(d_market_score, 3))
    
    d_market_rent_score = get_market_rent_score(
        d_zip_rent=d_zip_rent,
        d_city_rent=d_city_rent,
        d_state_rent=d_state_rent,
    )
    if kwargs.get("b_debug"): print("Market rent score", round(d_market_rent_score, 3))
        
    d_market_rent_vs_price_score, d_market_rent_vs_sale_days_score, d_market_size_score = get_ltr_market_opportunity_score(
        d_zip_price=d_zip_price,
        d_city_price=d_city_price,
        d_state_price=d_state_price,
        d_zip_rent=d_zip_rent,
        d_city_rent=d_city_rent,
        d_state_rent=d_state_rent,
        #d_area_sale_penetration,
        #d_state_sale_penetration,
        d_zip_rent_penetration=d_zip_rent_penetration,
        d_city_rent_penetration=d_city_rent_penetration,
        d_state_rent_penetration=d_state_rent_penetration,
        #d_area_sale_turnover,
        #d_state_sale_turnover,
        #d_area_rent_turnover,
        #d_state_rent_turnover,
        d_zip_sale_days_on_market=d_zip_sale_days_on_market,
        d_city_sale_days_on_market=d_city_sale_days_on_market,
        d_state_sale_days_on_market=d_state_sale_days_on_market,
        d_zip_rent_days_on_market=d_zip_rent_days_on_market,
        d_city_rent_days_on_market=d_city_rent_days_on_market,
        d_state_rent_days_on_market=d_state_rent_days_on_market,
        i_area_houses=i_area_houses,
        i_area_sale_properties=i_area_sale_properties,
        i_zip_sale_properties=i_zip_sale_properties,
        i_city_sale_properties=i_city_sale_properties,
        i_state_sale_properties=i_state_sale_properties,
        i_area_rent_properties=i_area_rent_properties,
        i_zip_rent_properties=i_zip_rent_properties,
        i_city_rent_properties=i_city_rent_properties,
        i_state_rent_properties=i_state_rent_properties,
    )
    #if kwargs.get("b_debug"): print("Market opportunity score", round(d_market_opportunity_score, 3))
    if kwargs.get("b_debug"): print("Market rent vs price score", round(d_market_rent_vs_price_score, 3))
    if kwargs.get("b_debug"): print("Market days to rent vs days to sell score", round(d_market_rent_vs_sale_days_score, 3))
    
    #d_area_penetration = shrink(min(i_area_properties, i_zillow_properties), d_area_penetration, d_state_penetration)
    if not d_area_rent_penetration: d_area_rent_penetration = 0
    d_market_penetration_score = d_area_rent_penetration ** 0.75
    if kwargs.get("b_debug"): print("Market penetration score", round(d_market_penetration_score, 3))
    
    d_market_family_ptype_score, d_market_family_school_score, d_market_family_playground_score = get_ltr_market_family_area_score(e_local_data)
    if e_inputs:
        #print("using get_ltr_family_score")
        d_family_ptype_score , d_family_lot_size_score, d_family_rec_room_score, d_family_pool_score = get_ltr_family_score(e_inputs)
    else:
        #print("using get_ltr_market_family_score")
        d_family_ptype_score , d_family_lot_size_score, d_family_rec_room_score, d_family_pool_score = get_ltr_market_family_score(e_local_data)
    
    
    if 0 < i_state_rent_properties < 5000:
        d_state_roi = shrink_state(i_state_rent_properties, d_state_roi, d_STR_AVG_NET_ROI)
        d_state_cap = shrink_state(i_state_rent_properties, d_state_cap, d_STR_AVG_CAP)
    
    d_raw_roi = d_net / d_price if d_price > 0 else 1
    #d_roi = d_net / (d_price / i_RETURN_YEARS_TIMEFRAME) if d_price > 0 else 1
    #if 0 < min(i_area_properties, i_zillow_properties) < 50:
    #    d_roi = shrink(min(i_area_properties, i_zillow_properties), d_roi, (d_state_roi * i_RETURN_YEARS_TIMEFRAME))
    
    d_roi_score = np.tanh(d_raw_roi) ** 0.22 if d_raw_roi > 0 else 0
    if kwargs.get("b_debug"): print("ROI score", round(d_roi_score, 3))
    
    #d_cap = d_revenue / (d_price / i_RETURN_YEARS_TIMEFRAME)
    d_cap = d_revenue / d_price if d_price > 0 else 1
    #if 0 < min(i_area_properties, i_zillow_properties) < 50:
    #    d_cap = shrink(min(i_area_properties, i_zillow_properties), d_cap, (d_state_cap * i_RETURN_YEARS_TIMEFRAME))
    d_cap_score = np.tanh(d_cap) ** 0.25 if d_cap > 0 else 0
    if kwargs.get("b_debug"): print("CAP score", round(d_cap_score, 3))
    
    d_relative_score = combine_scores(
        {
            "roi": d_roi_score,
            "cap": d_cap_score,
            "expected": d_cap_performance_score,
        },
        e_LTR_SCORE_WEIGHTS["relative"]) ** 0.64
    
    d_absolute_score = combine_scores(
        {
            "rent": d_abs_rent_score,
            "net": d_abs_net_score,
        },
        e_LTR_SCORE_WEIGHTS["absolute"]) ** 0.66
    
    d_family_score = combine_scores(
        {
            "lot_size": d_family_lot_size_score,
            "house_type": d_family_ptype_score,
            "rec_room": d_family_rec_room_score,
            "pool": d_family_pool_score,
        },
        e_LTR_SCORE_WEIGHTS["family"]) ** 0.44
    d_family_score = max(0, min(1, (((d_family_score + 0.5) ** 2) - 0.5)))
    
    d_area_score = combine_scores(
        {
            "cap": d_market_score,
            "rent": d_market_rent_score,
            "penetration": d_market_penetration_score,
            "rent_vs_price": d_market_rent_vs_price_score,
            "absorption": d_market_rent_vs_sale_days_score,
            "size": d_market_size_score,
        },
        e_LTR_SCORE_WEIGHTS["area"]) ** 0.6
    d_area_score = max(0, min(1, (((d_area_score + 0.5) ** 2) - 0.5)))
    
    d_family_area_score = combine_scores(
        {
            "type_ratio": d_market_family_ptype_score,
            "school_ratings": d_market_family_school_score,
            "playgrounds": d_market_family_playground_score,
            #"crime_rate": ,
        },
        e_LTR_SCORE_WEIGHTS["family_area"]) ** 0.75
    
    d_overall_score = combine_scores(
        {
            "relative": d_relative_score,
            "absolute": d_absolute_score,
            "family": d_family_score,
            "area": d_area_score,
            "family_area": d_family_area_score,
        },
        e_LTR_SCORE_WEIGHTS["overall"]) ** 0.64
    d_overall_score = max(0, min(1, (((d_overall_score + 0.5) ** 1.5) - 0.5)))
    
    if 0 < i_area_rent_properties < 100:
        d_absolute_score = shrink(i_area_rent_properties, d_absolute_score, 0.4)
    if kwargs.get("b_debug"): print("Overall score", round(d_overall_score, 3))
    
    return {
        "annual_rent_for_score": d_revenue,
        "price_for_score": d_price,
        "ltr_estimated_costs": d_property_tax_cost - d_management_cost - d_maintenance_cost,
        "ltr_time_cost": d_salary_cost,
        #"net": d_orig_net,
        "ltr_estimated_net": d_net,
        "ltr_estimated_roi": d_raw_roi,
        #f"roi_over_{int(round(i_RETURN_YEARS_TIMEFRAME))}_years": d_roi,
        #f"cap_over_{int(round(i_RETURN_YEARS_TIMEFRAME))}_years": d_cap,
        
        "ltr_score_overall": round(d_overall_score, i_SCORE_PRECISION),
        
        "ltr_score_relative": round(d_relative_score, i_SCORE_PRECISION),
        "ltr_score_absolute": round(d_absolute_score, i_SCORE_PRECISION),
        "ltr_score_family": round(d_family_score, i_SCORE_PRECISION),
        "ltr_score_area": round(d_area_score, i_SCORE_PRECISION),
        "ltr_score_family_area": round(d_family_area_score, i_SCORE_PRECISION),
        
        "ltr_score_relative_roi": round(d_roi_score, i_SCORE_PRECISION),
        "ltr_score_relative_cap": round(d_cap_score, i_SCORE_PRECISION),
        "ltr_score_relative_expected": round(d_cap_performance_score, i_SCORE_PRECISION),
        "ltr_score_absolute_rent": round(d_abs_rent_score, i_SCORE_PRECISION),
        "ltr_score_absolute_net": round(d_abs_net_score, i_SCORE_PRECISION),
        
        "ltr_score_area_cap": round(d_market_score, i_SCORE_PRECISION),
        "ltr_score_area_rent": round(d_market_rent_score, i_SCORE_PRECISION),
        "ltr_score_area_penetration": round(d_market_penetration_score, i_SCORE_PRECISION),
        "ltr_score_area_rent_vs_price": round(d_market_rent_vs_price_score, i_SCORE_PRECISION),
        "ltr_score_area_days_to_rent_vs_sell": round(d_market_rent_vs_sale_days_score, i_SCORE_PRECISION),
        "ltr_score_area_size": round(d_market_size_score, i_SCORE_PRECISION),
        
        "ltr_score_family_area_type_ratio": round(d_market_family_ptype_score, i_SCORE_PRECISION),
        "ltr_score_family_area_school_ratings": round(d_market_family_school_score, i_SCORE_PRECISION),
        "ltr_score_family_area_playgrounds": round(d_market_family_playground_score, i_SCORE_PRECISION),
        
        "ltr_score_family_house_type": round(d_family_ptype_score, i_SCORE_PRECISION),
        "ltr_score_family_lot_size": round(d_family_lot_size_score, i_SCORE_PRECISION),
        "ltr_score_family_rec_room": round(d_family_rec_room_score, i_SCORE_PRECISION),
        "ltr_score_family_pool": round(d_family_pool_score, i_SCORE_PRECISION),
        
        #"score_area_seasonality": round(d_market_seasonality_score, i_SCORE_PRECISION),
        #"score_area_optimization": round(d_market_price_dynamism_score, i_SCORE_PRECISION),
    }


#@utils.timer
def get_state_utilities(s_state):
    global e_STATE_UTILITY_COSTS
    if not e_STATE_UTILITY_COSTS:
        df_state_rates = o_DB.get_table_as_df(db_tables.state_utility_costs)
        df_state_rates.drop(columns=["country"]).set_index('state').to_dict(orient="index")
        df_state_rates["state"] = df_state_rates["state"].str.lower()
        e_STATE_UTILITY_COSTS = df_state_rates.drop(columns=["country"]).set_index('state').to_dict(orient="index")
    e_util_costs = {k: v for k, v in e_STATE_UTILITY_COSTS.get(s_state.lower(), e_STATE_UTILITY_COSTS["_all_"]).items()}
    return e_util_costs
    

#@utils.timer
def get_utility_factors(state, day=None, lat=None):
    if lat is None:
        lat, lng = us_states.e_STATE_COORDS.get(state.lower(), (32.815529, -98.827382))
    if day is None:
        day = datetime.today()
    
    if isinstance(day, (int, float, Decimal)):
        i_day = int(day)
    else:
        i_day = day.timetuple().tm_yday
    
    d_month = i_day / 30.4166667
    
    #d_latitude_seasonality_factor = 1 - math.cos(math.radians(latitude))
    if abs(lat) <= 15:
        d_latitude_seasonality_factor = 0
    elif lat >= 15:
        d_latitude_seasonality_factor = (1 - max(0, math.cos(math.radians(lat - 15) * 1.5))) ** 0.5
    elif lat <= -15:
        d_latitude_seasonality_factor = (1 - max(0, math.cos(math.radians(lat + 15) * 1.5))) ** 0.5
    
    #d_heating_latitude_factor = (math.cos(math.radians(60 - max(0, lat - 30))) - 0.5) * 2
    #d_cooling_latitude_factor = math.cos(math.radians(min(90, lat * 1.5)))
    #print(latitude, round(d_heating_latitude_factor, 3), round(d_cooling_latitude_factor
    
    d_electric_season_factor = 1 + (math.cos(math.radians((i_day - 30) * (360 / 365) * 2)) * 0.4) * d_latitude_seasonality_factor
    d_electric_summer_factor = 1 + (math.cos(math.radians(((i_day - 30) + 182.5) * (360 / 365))) / 12) * d_latitude_seasonality_factor
    d_electric_cost = d_electric_season_factor * d_electric_summer_factor
    
    d_gas_cost = (1 + (math.cos(math.radians((i_day - 15) * (360 / 365)))) * 0.5 * d_latitude_seasonality_factor) ** .75
    
    d_lawn_cost = max(0, (1 + (math.cos(math.radians((i_day + 182.5) * (360 / 365)))) * 0.5 * d_latitude_seasonality_factor))

    return d_electric_cost, d_gas_cost, d_lawn_cost


#@utils.timer
def get_per_month_costs(
    s_state,
    d_occupancy=0.4724, #0.2776,
    d_living_area=1850,
    d_lot_size=0.5,
    d_bedrooms=2.75,
    d_bathrooms=2,
    d_latitude=None,
    a_monthly_occupancy=[],
    b_pool=False,
    b_hot_tub=False,
    **kwargs
):
    s_state = us_states.e_STATE_ABBREV.get(s_state.lower(), s_state.lower())
    #print("s_state", s_state)
    d_state_factor, d_state_wage_rate, d_state_net, d_state_roi, d_state_cap, d_state_property_tax, d_state_insurance = get_state_factors(s_state)
    
    e_rates = get_state_utilities(s_state)
    #print("e_rates", e_rates)
    if d_latitude is None:
        d_latitude, _ = us_states.e_STATE_COORDS.get(s_state, (32.815529, -98.827382))

    d_living_area_factor = (d_living_area / 2000) ** 0.8
    d_bedroom_factor = (d_bedrooms / 3) ** 0.5
    d_bathroom_factor = (d_bathrooms / 2) ** 0.5
    
    e_rates["electricity"] *= ((d_living_area_factor + d_bedroom_factor) / 2)
    e_rates["gas"] *= d_bedroom_factor
    
    d_base_rate = e_rates["water"]
    e_rates["water"] = round(d_base_rate * ((d_bathroom_factor + d_living_area_factor) / 2), 2)
    if b_pool:
        e_rates["water"] += d_base_rate * 1.5
    if b_hot_tub:
        e_rates["water"] += d_base_rate * 0.10
    
    e_rates["trash"] = 14
    
    
    d_rental_insurance = round((d_state_insurance + d_AVG_INSURANCE) / 2 * d_AVG_SHORT_TERM_INSURANCE_MULTIPLIER, 2)
    e_rates["homeowners_insurance"] = round((d_state_insurance + d_AVG_INSURANCE) / 2 / 12, 2)
    e_rates["short_rental_insurance"] = round(d_rental_insurance / 12, 2)
    
    if d_lot_size <= 1:
        d_lawn_size = (d_lot_size * 43560) - (d_living_area * 1.25)
    else:
        d_lawn_size = (min(10, d_lot_size) ** 0.25 * 43560) - (d_living_area * 1.25)
    if kwargs.get("b_debug"): print("Lawn size:", d_lawn_size)
    if d_lawn_size > 0:
        e_rates["lawn"] = round(((0.09 * d_state_factor) * (d_lawn_size ** 0.95)) / 12, 2)
    else:
        e_rates["lawn"] = 0
    
    if a_monthly_occupancy:
        assert len(a_monthly_occupancy) == 12
        d_total_occupancy = sum(a_monthly_occupancy)
    else:
        d_total_occupancy = d_occupancy * 12
    
    d_cleanings_per_year = max(i_MIN_CLEANS, min(i_MAX_CLEANS, d_occupancy * d_CLEANING_OCCUPANCY_FACTOR))
    #print(d_cleanings_per_year)
    d_cost_per_clean = (((d_CLEANING_FEE_PER_ROOM * (d_bedrooms + d_bathrooms)) + (d_living_area * d_CLEANING_FEE_PER_SQFT)) / 2) * d_state_factor
    #print(d_cost_per_clean)
    
    d_cleaning_cost = round(d_cleanings_per_year * d_cost_per_clean, 2)
    e_rates["cleaning"] = round(d_cleanings_per_year * d_cost_per_clean / 12, 2)
    
    a_vals = []
    e_monthly_costs = {}
    for idx, (i_month, i_day) in enumerate(a_MONTH_DAYS):
        e_month_rates = e_rates.copy()
        d_electric_cost, d_gas_cost, d_lawn_cost = get_utility_factors(s_state, day=i_day, lat=d_latitude)
        
        #d_electric_cost = round(e_rates["electricity"] * d_electric_cost, 2)
        #d_gas_cost = round(e_rates["gas"] * d_gas_cost, 2)
        #d_total = d_electric_cost + d_gas_cost + e_rates["water"] + e_rates["internet"] + e_rates["cable"] + e_rates["cable"]
        
        e_month_rates["electricity"] = round(e_month_rates["electricity"] * d_electric_cost, 2)
        e_month_rates["gas"] = round(e_month_rates["gas"] * d_gas_cost, 2)
        e_month_rates["lawn"] = round(e_month_rates["lawn"] * d_lawn_cost, 2)
        
        if a_monthly_occupancy and d_total_occupancy > 0:
            e_month_rates["cleaning"] = round(d_cleaning_cost * (a_monthly_occupancy[idx] / d_total_occupancy), 2)
        
        #print(i_day, e_month_rates)
        e_month_rates["total"] = d_total = round(sum([v for k, v in e_month_rates.items() if k not in {"total"} and isinstance(v, (int, float))]), 2)
        e_month_rates["month"] = utils.e_MONTH_NAMES[i_month]
        e_monthly_costs[idx + 1] = e_month_rates
        
        a_vals.append((i_day, i_month, d_total, e_month_rates["electricity"], e_month_rates["gas"], e_month_rates["cleaning"], e_month_rates["lawn"]))
    
    return e_monthly_costs, a_vals

