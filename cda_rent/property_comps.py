﻿import os, sys, re, math
from math import sin, cos, sqrt, atan2, radians
from decimal import Decimal
from datetime import datetime, timedelta
from time import time
import dateutil
import requests, json
import pandas as pd
import numpy as np
import logging
import traceback

from collections import defaultdict, Counter, namedtuple

try:
    from . import utils, db_tables, google_maps, interactive_inputs
except:
    import utils, db_tables, google_maps, interactive_inputs


d_INITIAL_RANGE_IN_DEGREES = db_tables.e_CONFIG.get("search_range_initial_in_degrees", 0.5) # 69 miles. How far to search from latitude and longitude on first attempt
d_MAX_RANGE_IN_DEGREES = db_tables.e_CONFIG.get("search_range_max_in_degrees", 8) # how far to expand search before giving up
d_EXPAND_RANGE = db_tables.e_CONFIG.get("search_range_expansion", 2) # how much to expand search each time if insufficient closest options are found
d_DEG_TO_M = 69.172

o_DB = None

def load_data(**kwargs):
    global o_DB
    if o_DB is None:
        if db_tables is None: db_tables.load_db(**kwargs)
        o_DB = db_tables.o_DB
load_data()


tn_METRIC = namedtuple("comp_metric", [
    "column",
    #"exact"
    "amount",
    "ratio",
    "positive_match",  # same value, both positive
    "negative_match",  # same value, both negative
    "no_match",  # both search and db have a value, but they are different by at least "amount" and "ratio"
    "not_in_search",  # adjust score if search is null and db has a value
    "not_in_db",  # adjust score if search has a value and db is null
])

e_COMPARISON_IMPORTANCE = {
    "property_type": tn_METRIC(column="property_type", amount=1, ratio=0, positive_match=1.0, negative_match=0.0, no_match=-1.0, not_in_search=0, not_in_db=-0.75),
    "bedrooms": tn_METRIC(column="bedrooms", amount=1, ratio=0, positive_match=1.0, negative_match=0.0, no_match=-1.0, not_in_search=-0, not_in_db=-0.75),
    "bathrooms": tn_METRIC(column="bathrooms", amount=1, ratio=0, positive_match=1.0, negative_match=0.0, no_match=-1.0, not_in_search=-0, not_in_db=-0.75),
    "price": tn_METRIC(column="price", amount=0, ratio=0.3, positive_match=1.0, negative_match=0.0, no_match=-5.0, not_in_search=-0, not_in_db=-0.25),
    "living_area": tn_METRIC(column="living_area", amount=0, ratio=0.5, positive_match=3.0, negative_match=0.0, no_match=-3.0, not_in_search=-0, not_in_db=-1.0),
    "lot_size": tn_METRIC(column="lot_size", amount=0, ratio=0.75, positive_match=3.0, negative_match=0.0, no_match=-3.0, not_in_search=-0, not_in_db=-1.0),
    "year_built": tn_METRIC(column="year_built", amount=10, ratio=0, positive_match=1.0, negative_match=0.0, no_match=-0.5, not_in_search=-0, not_in_db=-0.05),
    "fixer_upper": tn_METRIC(column="fixer_upper", amount=1, ratio=0, positive_match=1.0, negative_match=0.0, no_match=-0.5, not_in_search=-0, not_in_db=-0),
    #"pets_allowed": tn_METRIC(column="pets_allowed", amount=1, ratio=0, positive_match=1.0, negative_match=1.0, no_match=-2, not_in_search=-0, not_in_db=-0),
    #"smoking_allowed": tn_METRIC(column="smoking_allowed", amount=1, ratio=0, positive_match=1.0, negative_match=1.0, no_match=-2, not_in_search=-0, not_in_db=-0),
    
    "feature_pool": tn_METRIC(column="feature_pool", amount=0,  ratio=0, positive_match=2.0, negative_match=0.0, no_match=-2.0, not_in_search=-0, not_in_db=-1),
    "feature_jacuzzi": tn_METRIC(column="feature_jacuzzi", amount=0,  ratio=0, positive_match=1.5, negative_match=0.0, no_match=-1.0, not_in_search=-0, not_in_db=-0.75),
    "feature_fireplace": tn_METRIC(column="feature_fireplace", amount=0,  ratio=0, positive_match=1.0, negative_match=0.0, no_match=-0.8, not_in_search=-0, not_in_db=-0.7),
    "feature_gated_community": tn_METRIC(column="feature_gated_community", amount=0, ratio=0, positive_match=1.0, negative_match=0.0, no_match=-2, not_in_search=-0, not_in_db=-0.5),
    "feature_ski_in_ski_out": tn_METRIC(column="feature_ski_in_ski_out", amount=0, ratio=0, positive_match=1.0, negative_match=0.0, no_match=-1.5, not_in_search=-0, not_in_db=-0.5),
    "feature_elevator": tn_METRIC(column="feature_elevator", amount=0,  ratio=0, positive_match=2.0, negative_match=0.0, no_match=-2.0, not_in_search=-0, not_in_db=-0.5),
    #"feature_wheelchair_accessible": tn_METRIC(column="feature_wheelchair_accessible", amount=0,  ratio=0, positive_match=4.0, negative_match=0.0, no_match=-4.0, not_in_search=-0, not_in_db=-0.5),
    "feature_ev_charger": tn_METRIC(column="feature_ev_charger", amount=0, ratio=0, positive_match=1.0, negative_match=0.0, no_match=-0.8, not_in_search=-0, not_in_db=-0.5),
    "feature_office": tn_METRIC(column="feature_office", amount=0, ratio=0, positive_match=0.75, negative_match=0.0, no_match=-0.75, not_in_search=-0, not_in_db=-0.4),
    "feature_boat_slip": tn_METRIC(column="feature_boat_slip", amount=0, ratio=0, positive_match=0.75, negative_match=0.0, no_match=-0.75, not_in_search=-0, not_in_db=-0.4),
    "feature_kitchenette": tn_METRIC(column="feature_kitchenette", amount=0, ratio=0, positive_match=0.75, negative_match=0.0, no_match=-0.75, not_in_search=-0, not_in_db=-0.4),
    
    "feature_patio_or_balcony": tn_METRIC(column="feature_patio_or_balcony", amount=0, ratio=0, positive_match=0.75, negative_match=0.0, no_match=-0.75, not_in_search=-0, not_in_db=-0.4),
    
    "location_downtown": tn_METRIC(column="location_downtown", amount=0, ratio=0, positive_match=3.0, negative_match=0.25, no_match=-2.5, not_in_search=-0, not_in_db=-0.25),
    "location_rural": tn_METRIC(column="location_rural", amount=0, ratio=0, positive_match=0.5, negative_match=0.0, no_match=-0.5, not_in_search=-0, not_in_db=-0.25),
    "location_waterfront": tn_METRIC(column="location_waterfront", amount=0, ratio=0, positive_match=5.0, negative_match=0.25, no_match=-4.0, not_in_search=-0, not_in_db=-3),
    "location_mountain_view": tn_METRIC(column="location_mountain_view", amount=0, ratio=0, positive_match=1.5, negative_match=0.0, no_match=-1.5, not_in_search=-0, not_in_db=-0.5),
    "location_water_view": tn_METRIC(column="location_water_view", amount=0, ratio=0, positive_match=2.5, negative_match=0.0, no_match=-2.5, not_in_search=-0, not_in_db=-1.5),
    "location_resort": tn_METRIC(column="location_resort", amount=0,  ratio=0, positive_match=1.0, negative_match=0.0, no_match=-0.9, not_in_search=-0, not_in_db=-0.5),
    "location_playground": tn_METRIC(column="location_resort", amount=0,  ratio=0, positive_match=.5, negative_match=0.0, no_match=-0.5, not_in_search=-0, not_in_db=-0.25),
    "location_library": tn_METRIC(column="location_resort", amount=0,  ratio=0, positive_match=.25, negative_match=0.0, no_match=-0.5, not_in_search=-0, not_in_db=-0.25),
    
    "location_museums": tn_METRIC(column="location_museums", amount=0, ratio=0, positive_match=0.5, negative_match=0.0, no_match=-0.5, not_in_search=-0, not_in_db=-0.25),
    "location_recreation_center": tn_METRIC(column="location_recreation_center", amount=0, ratio=0, positive_match=0.5, negative_match=0.0, no_match=-0.5, not_in_search=-0, not_in_db=-0.25),
    "location_wildlife_viewing": tn_METRIC(column="location_wildlife_viewing", amount=0, ratio=0, positive_match=0.5, negative_match=0.0, no_match=-0.5, not_in_search=-0, not_in_db=-0.25),
    #"location_sight_seeing": tn_METRIC(column="location_sight_seeing", amount=0, ratio=0, positive_match=0.5, negative_match=0.0, no_match=-0.5, not_in_search=-0, not_in_db=-0.25),
    "location_golf": tn_METRIC(column="location_golf", amount=0, ratio=0, positive_match=0.5, negative_match=0.0, no_match=-0.5, not_in_search=-0, not_in_db=-0.25),
    
    #"occupancy": tn_METRIC(column="occupancy", amount=0, ratio=1.25, positive_match=1.0, negative_match=, no_match=-1.0, not_in_search=0, not_in_db=-0.25),
    #"revenue": tn_METRIC(column="revenue", amount=0, ratio=1.25, positive_match=1.0, negative_match=, no_match=-1.0, not_in_search=0, not_in_db=-0.25),
    
    "rate": tn_METRIC(column="rate", amount=0, ratio=0.33, positive_match=1.0, negative_match=0.0, no_match=-1.0, not_in_search=0, not_in_db=-0.25),
    #"listed_rate": tn_METRIC(column="listed_rate", amount=0, ratio=0.33, positive_match=1.0, negative_match=0.0, no_match=-1.0, not_in_search=0, not_in_db=-0.25),
    "rate_per_sqft": tn_METRIC(column="rate_per_sqft", amount=0, ratio=0.5, positive_match=1.0, negative_match=0.0, no_match=-1.0, not_in_search=0, not_in_db=-0.25),
    "rate_per_acre": tn_METRIC(column="rate_per_acre", amount=0, ratio=0.5, positive_match=1.0, negative_match=0.0, no_match=-1.0, not_in_search=0, not_in_db=-0.25),
    "rate_per_bedroom": tn_METRIC(column="rate_per_bedroom", amount=0, ratio=0.5, positive_match=1.0, negative_match=0.0, no_match=-1.0, not_in_search=0, not_in_db=-0.25),
    
    "price_per_sqft": tn_METRIC(column="price_per_sqft", amount=0, ratio=0.5, positive_match=1.0, negative_match=0.0, no_match=-1.0, not_in_search=0, not_in_db=-0.25),
    "price_per_acre": tn_METRIC(column="price_per_acre", amount=0, ratio=0.5, positive_match=1.0, negative_match=0.0, no_match=-1.0, not_in_search=0, not_in_db=-0.25),
    
    "nearest_hotel_dist_0": tn_METRIC(column="nearest_hotel_dist_0", amount=0, ratio=0.5, positive_match=1.0, negative_match=0.0, no_match=-0.6, not_in_search=0, not_in_db=-0.25),
    "nearest_hotel_rate_0": tn_METRIC(column="nearest_hotel_rate_0", amount=0, ratio=0.25, positive_match=1.0, negative_match=0.0, no_match=-0.6, not_in_search=0, not_in_db=-0.25),
    "nearest_hotel_dist_1": tn_METRIC(column="nearest_hotel_dist_1", amount=0, ratio=0.5, positive_match=1.0, negative_match=0.0, no_match=-0.5, not_in_search=0, not_in_db=-0.25),
    "nearest_hotel_rate_1": tn_METRIC(column="nearest_hotel_rate_1", amount=0, ratio=0.25, positive_match=1.0, negative_match=0.0, no_match=-0.5, not_in_search=0, not_in_db=-0.25),
    #"nearest_hotel_dist_2": tn_METRIC(column="nearest_hotel_dist_2", amount=0, ratio=0.5, positive_match=1.0, negative_match=0.0, no_match=-0.4, not_in_search=0, not_in_db=-0.25),
    #"nearest_hotel_rate_2": tn_METRIC(column="nearest_hotel_rate_2", amount=0, ratio=0.25, positive_match=1.0, negative_match=0.0, no_match=-0.4, not_in_search=0, not_in_db=-0.25),
    #"nearest_hotel_dist_3": tn_METRIC(column="nearest_hotel_dist_3", amount=0, ratio=0.5, positive_match=1.0, negative_match=0.0, no_match=-0.3, not_in_search=0, not_in_db=-0.25),
    #"nearest_hotel_rate_3": tn_METRIC(column="nearest_hotel_rate_3", amount=0, ratio=0.25, positive_match=1.0, negative_match=0.0, no_match=-0.3, not_in_search=0, not_in_db=-0.25),
   
}

for col in db_tables.a_PROP_INFO_COLS:
    if col.startswith(("feature_", "location_")) and col not in e_COMPARISON_IMPORTANCE:
        e_COMPARISON_IMPORTANCE[col] = tn_METRIC(
            column=col,
            amount=0,
            ratio=0.0,
            positive_match=0.25,
            negative_match=0.0,
            no_match=-0.2,
            not_in_search=-0,
            not_in_db=-0)


def score_difference(e_search, e_db, **kwargs):
    e_factors = {}
    a_factors = []
    a_potential = []
    #a_pos, a_neg = [], []
    for col, tn_metric in e_COMPARISON_IMPORTANCE.items():
        v_search = e_search.get(tn_metric.column)
        v_db = e_db.get(tn_metric.column)
        
        if v_search is None and v_db is None:
            #print("skip", col)
            continue
        
        if v_search is None:
            if tn_metric.not_in_search:
                a_factors.append(tn_metric.not_in_search)
                e_factors[col] = tn_metric, tn_metric.not_in_search
                a_potential.append(0)
        elif v_db is None:
            if tn_metric.not_in_db:
                a_factors.append(tn_metric.not_in_db)
                e_factors[col] = tn_metric, v_search, v_db, tn_metric.not_in_db
                a_potential.append(0)
        else:
            if (not tn_metric.amount and not tn_metric.ratio) or not isinstance(v_search, (float, int, Decimal)) or not isinstance(v_db, (float, int, Decimal)):  # exact only
                if isinstance(v_search, Decimal):
                    v_search = float(v_search)
                if isinstance(v_db, Decimal):
                    v_db = float(v_db)
                
                if v_search == v_db:
                    if v_search:
                        if tn_metric.positive_match:
                            a_factors.append(tn_metric.positive_match)
                            e_factors[col] = tn_metric, v_search, v_db, tn_metric.positive_match
                            a_potential.append(max(tn_metric.positive_match, tn_metric.negative_match))
                    else:
                        if tn_metric.negative_match:
                            a_factors.append(tn_metric.negative_match)
                            e_factors[col] = tn_metric, v_search, v_db, tn_metric.negative_match
                            a_potential.append(max(tn_metric.positive_match, tn_metric.negative_match))
                else:
                    if tn_metric.no_match:
                        a_factors.append(tn_metric.no_match)
                        e_factors[col] = tn_metric, v_search, v_db, tn_metric.no_match
                        a_potential.append(max(tn_metric.positive_match, tn_metric.negative_match))
            else:
                if isinstance(v_search, Decimal):
                    v_search = float(v_search)
                if isinstance(v_db, Decimal):
                    v_db = float(v_db)
                
                if tn_metric.amount:
                    d_abs_diff = abs(v_search - v_db)
                    if kwargs.get("b_debug"): print(col, "abs_diff", d_abs_diff)
                    d_abs = min(1, d_abs_diff / tn_metric.amount)
                    d_abs_pos = 1 - d_abs
                else:
                    d_abs, d_abs_pos = 0.0, 0.0
                
                if tn_metric.ratio:
                    d_max = max(abs(v_search), abs(v_db))
                    d_rel_diff = abs(v_search - v_db) / d_max if v_search or v_db else 1.0
                    if kwargs.get("b_debug"): print(col, "rel_diff", d_rel_diff)
                    d_rel = min(1, d_rel_diff / tn_metric.ratio)
                    d_rel_pos = 1 - d_rel
                else:
                    d_rel, d_rel_pos = 0.0, 0.0
                
                d_pos_score = (d_abs_pos + d_rel_pos) * tn_metric.positive_match
                #if d_pos_score > 0: a_pos.append(d_pos_score)
                    
                d_neg_score = (d_abs + d_rel) * tn_metric.no_match
                #if d_neg_score < 0: a_neg.append(d_neg_score)
                
                #if d_pos_score + d_neg_score != 0:
                if d_pos_score or d_neg_score:
                    if d_pos_score: a_factors.append(d_pos_score)
                    if d_neg_score: a_factors.append(d_neg_score)
                    
                    a_potential.append(tn_metric.positive_match)
                    
                    e_factors[col] = (tn_metric, v_search, v_db, d_pos_score + d_neg_score) #(d_abs_pos * tn_metric.positive_match, d_rel_pos * tn_metric.positive_match, d_abs * tn_metric.no_match, d_rel * tn_metric.no_match))
        
    #print("a_pos", a_pos)
    #print("a_neg", a_neg)
    if kwargs.get("b_debug"): print("a_factors", a_factors)
    #print("e_factors", e_factors)
    
    #d_comp = (sum(a_pos) + sum(a_neg)) / (len(a_pos) + len(a_neg))
    d_comp = min(1, max(-1, sum(a_factors) / len(a_factors))) if a_factors else 1
    
    d_comp = min(1, max(-1, sum(a_factors) / sum(a_potential))) if a_factors else 1
    return d_comp, e_factors


def get_closest(lat, long, e_row, i_min_closest=25, i_max_closest=1000, **kwargs):
    i_matches = 0
    a_closest = []
    
    if kwargs.get("s_select"):
        s_select = kwargs.get("s_select")
    else:
        s_select = f"{db_tables.property_info}.*"
    
    d_range = kwargs.get("initial_range", d_INITIAL_RANGE_IN_DEGREES)
    d_expansion = kwargs.get("expand_range", d_EXPAND_RANGE)
    d_max_range = max(d_range, kwargs.get("max_range", d_MAX_RANGE_IN_DEGREES))
    if kwargs.get("b_debug"):
        print("d_range", d_range)
        print("d_expansion", d_expansion)
        print("d_max_range", d_max_range)
    
    dataset_quarter = kwargs.get("dataset_quarter", db_tables.e_CONFIG.get("latest_dataset_quarter"))
    
    a_other_where = [f"AND dataset_quarter = '{dataset_quarter}'"]
    #a_other_where = []
    
    #if e_row.get("property_type"):
    #    a_other_where.append("AND property_type = '{}'".format(e_row["property_type"]))

    if e_row.get("bedrooms"):
        i_beds = e_row["bedrooms"]
        a_other_where.append(f"AND bedrooms BETWEEN {i_beds - 1} and {i_beds + 1}")

    if e_row.get("bathrooms"):
        i_baths = e_row["bathrooms"]
        a_other_where.append(f"AND bathrooms BETWEEN {i_baths - 1} and {i_baths + 1}")

    if e_row.get("living_area"):
        d_min, d_max = e_row["living_area"] * 0.5, e_row["living_area"] * 2
        a_other_where.append(f"AND living_area BETWEEN {d_min} and {d_max}")

    if e_row.get("lot_size"):
        if e_row["lot_size"] < 1:
            d_min, d_max = e_row["lot_size"] * 0.5, e_row["lot_size"] * 3
        elif e_row["lot_size"] < 5:
            d_min, d_max = e_row["lot_size"] * 0.33, e_row["lot_size"] * 4
        elif e_row["lot_size"] < 10:
            d_min, d_max = e_row["lot_size"] * 0.25, e_row["lot_size"] * 6
        elif e_row["lot_size"] < 25:
            d_min, d_max = e_row["lot_size"] * 0.2, e_row["lot_size"] * 10
        else:
            d_min, d_max = e_row["lot_size"] * 0.1, e_row["lot_size"] * 20
        
        a_other_where.append(f"AND lot_size BETWEEN {d_min} and {d_max}")
    
    s_other_where = "\n".join(a_other_where) if a_other_where else ""
    
    while i_matches < i_min_closest and d_range <= d_max_range:
        min_lat = (lat - d_range) * 10000
        max_lat = (lat + d_range) * 10000
        min_long = (long - (d_range * cos(radians(lat)))) * 10000
        max_long = (long + (d_range * cos(radians(lat)))) * 10000

        s_sql = f"""
        SELECT GCDistDeg(latitude, longitude, {lat}, {long}) * 69.172 as distance, {s_select}
        FROM {db_tables.property_info}
        WHERE
            long_hash BETWEEN {min_long} AND {max_long}
            AND lat_hash BETWEEN {min_lat} AND {max_lat}
            {s_other_where}
        ORDER BY distance ASC
        LIMIT {i_max_closest};
        """
        if kwargs.get("b_debug"): print(s_sql)

        n_start = time()
        a_closest = o_DB.execute(s_sql)
        if kwargs.get("b_debug"):
            print(f"Finding {len(a_closest):,} matches within {d_range * 69.172:0.1f} took: {utils.secondsToStr(time() - n_start)}")
    
        i_matches = len(a_closest)
        d_range *= d_expansion
    
    return a_closest, d_range


def get_best(lat, long, e_search, d_min_feature_similarity=0.20, i_min_matches=20, i_max_results=10, **kwargs):
    d_range = kwargs.get("initial_range", d_INITIAL_RANGE_IN_DEGREES)
    d_expansion = kwargs.get("expand_range", d_EXPAND_RANGE)
    d_max_range = kwargs.get("max_range", d_MAX_RANGE_IN_DEGREES)
    d_max_range_miles = d_max_range * d_DEG_TO_M
    
    if "i_min_closest" not in kwargs:
        kwargs["i_min_closest"] = int(i_min_matches * 1.5)
    
    i_matches = 0
    df_closest = None
    while i_matches < i_min_matches and d_range <= d_max_range:
        
        a_closest, d_range = get_closest(lat, long, e_search, initial_range=d_range, **kwargs)
        
        df_closest = pd.DataFrame(a_closest, columns=["distance"] + db_tables.a_PROP_INFO_COLS)
        
        a_db_scores, a_db_factors = [], []
        for i, e_db_row in enumerate(df_closest.to_dict(orient="records")):
            d_comp, e_factors = score_difference(e_search, e_db_row, **kwargs)
            #d_comp = score_difference(e_search, e_db_row, b_debug=False)
            a_db_scores.append(d_comp)
            a_db_factors.append(e_factors)
    
        #if "feature_similarity" not in df_closest.columns:
        df_closest.insert(1, "feature_similarity", a_db_scores)
        #else:
        #    df_closest["feature_similarity"] = a_db_scores
        
        df_closest = df_closest[df_closest["feature_similarity"] >= d_min_feature_similarity].copy()
        i_matches = df_closest.shape[0]
        if i_matches < i_min_matches and d_range < d_max_range:
            print(f"{i_matches:,} at {d_range * d_DEG_TO_M:0.1f}mi ({d_range} deg). Search at {d_range * d_expansion * d_DEG_TO_M:0.1f}mi ({d_range * d_expansion} deg).")
            d_range *= d_expansion
            continue
        
        #if "distance_similarity" not in df_closest.columns:
        df_closest.insert(
            list(df_closest.columns).index("distance") + 1,
            "distance_similarity",
            df_closest["distance"].apply(lambda x: 1 - (x ** 0.5 / d_max_range_miles ** 0.5))
        )
        #else:
        #    df_closest["distance_similarity"] = df_closest["distance"].apply(lambda x: 1 - (x ** 0.5 / d_max_range_miles ** 0.5))
            
        #if "similarity" not in df_closest.columns:
        df_closest.insert(
            0,
            "similarity",
            df_closest["feature_similarity"] * df_closest["distance_similarity"]
        )
        #else:
        #    df_closest["similarity"] = df_closest["feature_similarity"] * df_closest["distance_similarity"]
        df_closest.sort_values("similarity", ascending=False, inplace=True)
    
        df_closest = df_closest[:i_max_results].copy()
    
    return df_closest


def get_comps(e_search, as_df=True, **kwargs):
    df_best = get_best(e_search["latitude"], e_search["longitude"], e_search, **kwargs)
    if as_df:
        return df_best
    
    if df_best is None or df_best.shape[0] == 0:
        return []
    
    a_results = []
    for e_row in df_best.to_dict(orient="records"):
        #e_scores = {}
        a_results.append(e_row)
    
    return a_results


def prep_comp_property(e_input):
    if e_input is None:
        e_missing_req = {
            s_col: f"Missing required input \"{s_col}\". Should be a {dtype}." 
            for s_col, dtype in interactive_inputs.e_REQUIRED_INPUTS_DTYPES.items()}
    
        return ({"No input": e_missing_req}, [], {}, {}), 400
    e_missing_req = interactive_inputs.check_required_input(e_input)
    if e_missing_req:
        return ({"Invalid input": e_missing_req}, [], {}, {}), 400
    
    e_address, i_status = google_maps.check_coordinates_address(e_input)
    if i_status != 200:
        return ({"Invalid input": e_address}, [], {}, {}), i_status
    elif pd.isnull(e_address.get("lat")) or pd.isnull(e_address.get("lng")):
        return ({"Invalid input": e_address}, [], {}, {}), i_status
    
    if pd.isnull(e_input.get("latitude")) or e_input["latitude"] in {"", "0", 0}:
        e_input["latitude"] = e_address["lat"]
    if pd.isnull(e_input.get("longitude")) or e_input["longitude"] in {"", "0", 0}:
        e_input["longitude"] = e_address["lng"]
    
    assert e_input.get("latitude")
    assert e_input.get("longitude")
    
    e_input = interactive_inputs.stack_input_dict(e_input)
    
    e_valid_optional, e_invalid_optional = interactive_inputs.check_optional_input(e_input)
    e_valid_amenities, e_invalid_amenities = interactive_inputs.check_feature_input(e_input, "amenities", section_prefix="amenity_", col_prefix="feature_")
    e_valid_features, e_invalid_features = interactive_inputs.check_feature_input(e_input, "features", section_prefix="feature_", col_prefix="feature_")
    e_valid_locations, e_invalid_locations = interactive_inputs.check_feature_input(e_input, "locations", section_prefix="location_", col_prefix="location_")
    e_valid_conditions, e_invalid_conditions = interactive_inputs.check_feature_input(e_input, "conditions", section_prefix="condition_", col_prefix="")
    
    #e_valid_optional = add_optional_defaults(e_valid_optional, b_fill_missing=True)
    e_valid_optional = {}
    
    if e_invalid_optional: print("\n", "e_invalid_optional", e_invalid_optional)
    if e_invalid_amenities: print("\n", "e_invalid_amenities", e_invalid_amenities)
    if e_invalid_features: print("\n", "e_invalid_features", e_invalid_features)
    if e_invalid_locations: print("\n", "e_invalid_locations", e_invalid_locations)
    if e_invalid_conditions: print("\n", "e_invalid_conditions", e_invalid_conditions)
    
    e_std = interactive_inputs.standardize_input(e_input, e_valid_optional, e_valid_amenities, e_valid_features, e_valid_locations, e_valid_conditions)
    
    
    #assert e_std.get("latitude")
    #assert e_std.get("longitude")
    #print("e_std", e_std, "\n")
    
    return (e_std, [], {}, e_address), 200
    
    if not isinstance(e_std.get("max_guests"), (int, float)) or e_std["max_guests"] < e_std["bedrooms"]:
        #print("e_std max_guests", e_std.get("max_guests"), "bedrooms", e_std["bedrooms"])
        e_std["max_guests"] = e_VARS["bedroom_guest"].get(e_std["bedrooms"], e_std["bedrooms"] * 2)
    
    b_guess_price = True if not e_std.get("price") or pd.isnull(e_std["price"]) or e_std["price"] <= 0 else False
    b_guess_lot_size = True if not e_std.get("lot_size") or pd.isnull(e_std["lot_size"]) or e_std["lot_size"] <= 0 else False
    b_guess_living_area = True if not e_std.get("living_area") or pd.isnull(e_std["living_area"]) or e_std["living_area"] <= 0 else False
    
    #df_zillow_inputs = airbnb_standardization.prep_for_dummies(df_zillow_inputs[[col for col in df_zillow_inputs if col in e_VARS["allowed_cols"]]])
    
    #print("e_std", e_std.keys())
    #print("e_std lat/lng", e_std["latitude"], e_std["longitude"])
    e_output, a_pas, e_pai = location_analysis.add_area_info_to_dict(e_std, prep_zillow.o_AREA_LOOKUPS)
    #assert e_output.get("latitude")
    #assert e_output.get("longitude")
    
    for col in e_VARS["train_cols"]:
        if col not in e_output and col.startswith("property_type"):
            e_output[col] = 0
    
    e_hotel_input = prep_zillow.o_HOTELS.find_nearest_hotels_to_by_coordinates(e_std["latitude"], e_std["longitude"])
    e_output.update(e_hotel_input)
    
    #e_output["Latitude"] = e_input["latitude"]
    #e_output["Longitude"] = e_input["longitude"]
    
    e_lat_subs, e_long_subs = airbnb_standardization.subdivide_latitude_longitude_dict(
        e_output,
        e_VARS["gps_min_latitude"],
        e_VARS["gps_max_latitude"],
        e_VARS["gps_min_longitude"],
        e_VARS["gps_max_longitude"],
        n_subdivisions=e_VARS["gps_subdivisions"])
    e_output.update(e_lat_subs)
    e_output.update(e_long_subs)
    
    #print("b_guess_price", b_guess_price)
    if b_guess_price and (b_guess_lot_size or b_guess_living_area):
        e_output["price"] = location_analysis.guess_price(e_output)
        if b_guess_lot_size:
            e_output["lot_size"] = location_analysis.guess_lot_size(e_output)
        if b_guess_living_area:
            e_output["living_area"] = location_analysis.guess_living_area(e_output)
        e_output["price"] = location_analysis.guess_price(e_output)
    else:
        if b_guess_price:
            e_output["price"] = location_analysis.guess_price(e_output)
        if b_guess_lot_size:
            e_output["lot_size"] = location_analysis.guess_lot_size(e_output)
        if b_guess_living_area:
            e_output["living_area"] = location_analysis.guess_living_area(e_output)
    
    #print("e_output max_guests", e_output["max_guests"])
    return (e_output, a_pas, e_pai, e_address), 200


