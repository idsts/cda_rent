﻿import os, sys, re, json, math
from time import time
from operator import itemgetter
from collections import defaultdict, Counter, namedtuple
from datetime import datetime, timedelta
import pandas as pd
import numpy as np


try:
    from . import utils, db_tables, us_states
except:
    import utils, db_tables, us_states


e_PREC_SIZE = {-3: 'nationwide',
 -2: '11,100 Km2',
 -1: '1,110 Km2',
 0: '111 Km2',
 1: '11.1 Km2',
 2: '1,110 m2',
 3: '111 m2',
 4: '11 m2'}

e_PREC_SIZE_IMP = {-3: 'nationwide',
 -2: '6,900 mi2',
 -1: '690 mi2',
 0: '69 mi2',
 1: '6.9 mi2',
 2: '1,214 yd2',
 3: '121.4 yd2',
 4: '12.1 yd2'}

e_RAW_AREAS = {
    -3: 31579452.85,
    -2: 31579452.85,
    -1: 478476.5584,
    0: 4784.765584,
    1: 47.84765584,
    2: 0.47847655840,
    3: 0.00478476558,
    4: 0.00004784765,
}

tn_GROUP_AREA = namedtuple(
    "GroupArea",
    [
        "precision",
        "lat_area",
        "long_area",
        "properties",
        "parent_lat_area",
        "parent_long_area",
        "properties_parent_area",
        "area_revenue",
        "pt_area_revenue",
        "pt_area_rev_diff",
        "rev_per_bedroom",
        "rev_per_bathroom",
        "rate_per_bedroom",
    ]
)

e_STATE_INFO = {}

e_LTR_RENAMES = {}
for i in range(-3, 3 + 1):
    e_LTR_RENAMES[f'{i}_area_bedrooms'] = f'{i}_area_ltr_bedrooms'
    e_LTR_RENAMES[f'{i}_area_bathrooms'] = f'{i}_area_ltr_bathrooms'
    e_LTR_RENAMES[f'{i}_area_precision'] = f'{i}_area_ltr_precision'
    e_LTR_RENAMES[f'{i}_area_scale'] = f'{i}_area_ltr_scale'
    e_LTR_RENAMES[f'{i}_area_properties'] = f'{i}_area_ltr_properties'
    e_LTR_RENAMES[f'{i}_reliable_properties'] = f'{i}_area_ltr_reliable_properties'
    e_LTR_RENAMES[f'{i}_area_bedroom_vs_mean'] = f'{i}_area_ltr_bedroom_vs_mean'
    e_LTR_RENAMES[f'{i}_area_bathroom_vs_mean'] = f'{i}_area_ltr_bathroom_vs_mean'
    e_LTR_RENAMES[f'{i}_area_occupancy'] = f'{i}_area_ltr_occupancy'
    e_LTR_RENAMES[f'{i}_area_availability'] = f'{i}_area_ltr_availability'
    e_LTR_RENAMES[f'{i}_area_density'] = f'{i}_area_ltr_area_density'
e_LTR_RENAMES[f'area_precision'] = f'area_ltr_precision'
e_LTR_RENAMES[f'area_properties'] = f'area_ltr_properties'
e_LTR_RENAMES[f'area_density'] = f'area_ltr_density'
e_LTR_RENAMES[f'occupancy'] = f'ltr_occupancy'
e_LTR_RENAMES[f'availability'] = f'ltr_availability'


c_SEPARATE_STR_COLS = {
    'area_raw_revenue',
    'area_revenue',
    'area_availability',
    'area_occupancy',
    'area_occupancy_vs_available',
    'records',
    'reserved_days',
    'available_days',
    'blocked_days',
    'days',
    'first_date',
    'last_date',
    'area_reservation_length',
    'revenue_per_reservation',
    'owner_properties',
    'overall_rating',
    'response_rate',
    'number_of_reviews',
    'pets_allowed',
    'smoking_allowed',
    'reservations',
}

c_PREFERRED_LTR_COLS = {
    'area_rent',
    'area_rent_per_bedroom',
    'area_rent_per_bathroom',
    'area_rent_per_sqft',
    'area_rent_per_acre',
    'area_rent_per_price',
}

e_SEPARATE_LTR_COLS = {
    'area_properties': 'area_ltr_properties',
    'pets_allowed': 'ltr_pets_allowed',
    'smoking_allowed': 'ltr_smoking_allowed',
    'area_availability': 'area_ltr_availability',
    'area_occupancy': 'area_ltr_occupancy',
    'area_occupancy_vs_available': 'area_ltr_occupancy_vs_available',
    'cap_sqft': 'ltr_cap_sqft',
    'area_occupancy_vs_mean': 'area_ltr_occupancy_vs_mean',
    'area_bedroom_vs_mean': 'area_ltr_bedroom_vs_mean',
    'area_bathroom_vs_mean': 'area_ltr_bathroom_vs_mean',
    'area_cap_vs_mean': 'area_ltr_cap_vs_mean',
    'days_on_market': 'ltr_days_on_market',
}

def load_data(**kwargs):
    if db_tables.o_DB is None:
        db_tables.load_db()
    
    dataset_quarter = kwargs.get("dataset_quarter", db_tables.e_CONFIG.get("latest_dataset_quarter"))
    
    df_states = db_tables.o_DB.get_table_as_df(db_tables.state_info, where=f"dataset_quarter = '{dataset_quarter}'")
    
    global e_STATE_INFO
    e_STATE_INFO = defaultdict(dict)
    
    for idx, row in df_states.iterrows():
        state = row["state"].lower()
        e_STATE_INFO[state]["bedrooms"] = float(row["bedrooms"])
        e_STATE_INFO[state]["bathrooms"] = float(row["bathrooms"])
        e_STATE_INFO[state]["living_area"] = float(row["living_area"])
        e_STATE_INFO[state]["lot_size"] = float(row["lot_size"])
        e_STATE_INFO[state]["price"] = float(row["price"])
        e_STATE_INFO[state]["rent"] = float(row["rent"])
        e_STATE_INFO[state]["sqft_per_bedroom"] = float(row["living_area"]) / float(row["bedrooms"])
        e_STATE_INFO[state]["sqft_per_bathroom"] = float(row["living_area"]) / float(row["bathrooms"])
        e_STATE_INFO[state]["price_per_bedroom"] = float(row["price"]) / float(row["bedrooms"])
        e_STATE_INFO[state]["price_per_bathroom"] = float(row["price"]) / float(row["bathrooms"])
        e_STATE_INFO[state]["rev_per_acre"] = float(row["rev_per_acre"])
        e_STATE_INFO[state]["rate_per_acre"] = float(row["rate_per_acre"])
        e_STATE_INFO[state]["rent_per_acre"] = float(row["rent_per_acre"])
        e_STATE_INFO[state]["price_per_acre"] = float(row["price_per_acre"])
        e_STATE_INFO[state]["rev_per_sqft"] = float(row["rev_per_sqft"])
        e_STATE_INFO[state]["rate_per_sqft"] = float(row["rate_per_sqft"])
        e_STATE_INFO[state]["rent_per_sqft"] = float(row["rent_per_sqft"])
        e_STATE_INFO[state]["price_per_sqft"] = float(row["price_per_sqft"])
        e_STATE_INFO[state]["rent_per_price"] = float(row["rent"]) / float(row["price"])
        e_STATE_INFO[state]["price_per_rent"] = float(row["price"]) / float(row["rent"])


def get_scale_area(precision, latitude=0):
    d_sqm = e_RAW_AREAS[precision]
    if precision == -3: return d_sqm
    d_lat_scale = math.cos(math.radians(latitude))
    return d_sqm * d_lat_scale


def get_density(precision, properties, **kwargs):
    d_overall_density = properties / get_scale_area(precision, **kwargs)
    if precision < 2 and properties > 10:
        d_local_density = 10 / get_scale_area(precision + 1, **kwargs)
        if d_local_density < d_overall_density:
            return (d_overall_density + d_local_density) / 2
        else:
            return d_overall_density
        #return min(d_overall_density, d_local_density)
    else:
        return d_overall_density 


a_ALL_PRECISION_COLS = [
    #"area_precision",
    "area_scale",
    "area_properties",
    
    "area_revenue",
    "area_rate",
    #"area_listed_rate",
    "area_rent",
    "area_price",
    
    "area_bedrooms",
    "area_bathrooms",
    "area_living_area",
    "area_lot_size",
    "area_year_built",
    
    "area_rev_per_bedroom",
    "area_rev_per_bathroom",
    "area_rev_per_sqft",
    "area_rev_per_acre",
    "area_rev_per_price",
    "area_rate_per_bedroom",
    "area_rate_per_bathroom",
    "area_rate_per_sqft",
    "area_rate_per_acre",
    "area_rate_per_price",
    #"area_listed_rate_per_bedroom",
    #"area_listed_rate_per_bathroom",
    #"area_listed_rate_per_sqft",
    #"area_listed_rate_per_acre",
    #"area_listed_rate_per_price",
    "area_rent_per_bedroom",
    "area_rent_per_bathroom",
    "area_rent_per_sqft",
    "area_rent_per_acre",
    "area_rent_per_price",
    #"area_price_per_bedroom",
    #"area_price_per_bathroom",
    "area_price_per_sqft",
    "area_price_per_acre",
    "area_price_per_bedroom",
    "area_price_per_bathroom",
    
    #"area_rate_per_bathroom",
    "area_rate_vs_mean",
    #"area_occupancy_vs_mean",
    "area_availability",
    "area_occupancy",
    "area_occupancy_vs_available",
    
    #"area_bedroom_vs_mean",
    "area_demand_seasonality",
    "area_demand_seasonality_normalized",
    "area_demand_season_type",
    "area_demand_season_length",
    "area_demand_season_peak_month",
    "area_demand_season_max_month",
    
    "area_population_density",
    "area_housing_density",
    
    "area_reservation_length",
    "area_monthly_hoa_fee",
    "area_property_tax_rate",
    "area_reservation_length",
]
c_ALL_PRECISION_COLS = set(a_ALL_PRECISION_COLS)

e_DEFAULT_AGG_TYPES = {
    "id": False,
    "zpid": False,
    "address_street": False,
    "dataset_quarter": False,
    "created": "min",
    "updated": "max",
    "Property ID": False,
    "property_id": False,
    "property_type": "frequency",
    'property_type_Cabin': "mean",
    'property_type_Chateau': "mean",
    'property_type_Condo': "mean",
    'property_type_Cottage': "mean",
    'property_type_Estate': "mean",
    'property_type_Farmhouse': "mean",
    'property_type_House': "mean",
    'property_type_Tiny house': "mean",
    'property_type_Earth house': "mean",
    'property_type_Townhouse': "mean",
    'property_type_Apartment': "mean",
    'property_type_Other': "mean",
    'property_type_Duplex': "mean",
    'property_type_Other': "mean",
    "property_category": "frequency",
    'property_category_House': "mean",
    'property_category_Condo': "mean",
    'property_category_Townhouse': "mean",
    'property_category_Duplex': "mean",
    'property_category_Vacation': "mean",
    'property_category_Vacation home': "mean",
    'property_category_Other': "mean",
    'property_category_Apartment': "mean",
    'property_category_Vacation home': "mean",
    "properties": "sum",
    "rows": "sum",
    "reliable_rows": "sum",
    "records": "sum",
    "zillow_properties": "sum",
    "Owner Properties": "mean",
    "owner_properties": "mean",
    "first_date": "min",
    "last_date": "max",
    "last_sold_date": "max",
    "sold_date": "max",
    "reserved_days": "sum",
    "available_days": "sum",
    "blocked_days": "sum",
    "days": "sum",
    "open_days": "sum",
    "days_on_market": "mean",
    "adj_revenue": "mean",
    "revenue": "mean",
    "revenue_full_availability": "mean",
    "revenue_median": "median",
    "revenue_full_availability_median": "median",
    "rev_per_bedroom": "mean",
    "rev_per_bathroom": "mean",
    "availability": "mean",
    "Latitude": False,
    "Longitude": False,
    "latitude": False,
    "longitude": False,
    "lat_area": False,
    "long_area": False,
    "lat_hash": False,
    "long_hash": False,
    "overall_rating": "mean",
    "response_rate": "mean",
    "published_nightly_rate": "mean_non_zero",
    #"listed_rate": "mean_non_zero",
    "avg_rate": "mean_non_zero",
    "actual_rate": "mean_non_zero",
    "annual_revenue_ltm": "mean",
    "occupancy": "mean",
    "occupancy_vs_available": "mean",
    "occupancy_rate_ltm": "mean",
    "number_of_reviews": "mean",
    "number_of_photos": "mean",
    #"area_precision": "mean",
    #"area_properties": "mean",
    #"area_revenue": "mean",
    "closest_hotel_km": "mean_non_zero",
    "closest_hotel_usd": "mean_non_zero",
    "rev_vs_area": "mean",
    "rev_vs_median": "mean",
    "lat_area": "mean",
    "long_area": "mean",
    "latitude": "mean",
    "longitude": "mean",
    "max_guests": "mean",
    "bedrooms": "mean",
    "bathrooms": "mean",
    
    "living_area": "mean_non_zero",
    "lot_size": "mean_non_zero",
    "living_area_median": "median_non_zero",
    "lot_size_median": "median_non_zero",
    "levels": "mean_non_zero",
    "price": "mean_non_zero",
    "price_median": "median_non_zero",
    "price_actual": "mean_non_zero",
    "price_actual_median": "median_non_zero",
    "zestimate": "mean_non_zero",
    "tax_assessed_value": "mean_non_zero",
    "last_sold_price": "mean_non_zero",
    "calc_price": "mean_non_zero",
    
    "actual_rent": "mean_non_zero",
    "rent": "mean_non_zero",
    "rent_actual": "mean_non_zero",
    "rent_actual_median": "median_non_zero",
    "rent_estimate": "mean_non_zero",
    "last_rent": "mean_non_zero",
    "avg_rent": "mean_non_zero",
    "pt_area_difference": "mean",
    "rate": "mean_non_zero",
    "rate_median": "median_non_zero",
    
    "year_built": "mean_non_zero",
    "fixer_upper": "mean",
    "exact_location": "mean",
    
    "auction": "mean",
    "condemned": "mean",
    "fixer_upper": "mean",
    "renovated": "mean",
    "rent_control": "mean",
    "timeshare": "mean",
    "manufactured_home": "mean",
    "potential_issues": "mean",
    "no_rentals": "mean",
    "new_construction": "mean",
    "starter_home": "mean",
    "shared_ownership": "mean",
    "move_home": "mean",
    "lot_only": "mean",
    
    "demand_seasonality": "mean",
    "demand_seasonality_normalized": "mean",
    "demand_season_type": "frequency",
    "demand_season_type_Month": "10",
    "demand_season_type_Quarter": "10",
    "demand_season_length": "mean",
    "demand_season_peak_month": "frequency",
    "demand_season_max_month": "frequency",
    
    "availability_seasonality": "mean",
    "availability_seasonality_normalized": "mean",
    "availability_season_type": "frequency",
    "availability_season_type_Month": "10",
    "availability_season_type_Quarter": "10",
    "availability_season_length": "mean",
    "availability_season_peak_month": "frequency",
    "availability_season_max_month": "frequency",
    
    "area_demand_seasonality": "mean",
    "area_demand_seasonality_normalized": "mean",
    "area_demand_season_type": "frequency",
    "area_demand_season_type_Month": "10",
    "area_demand_season_type_Quarter": "10",
    "area_demand_season_length": "mean",
    "area_demand_season_peak_month": "frequency",
    "area_demand_season_max_month": "frequency",
    
    "area_availability_seasonality": "mean",
    "area_availability_seasonality_normalized": "mean",
    "area_availability_season_type": "frequency",
    "area_availability_season_type_Month": "10",
    "area_availability_season_type_Quarter": "10",
    "area_availability_season_length": "mean",
    "area_availability_season_peak_month": "frequency",
    "area_availability_season_max_month": "frequency",
    
    "occupancy_month_1": "mean",
    "occupancy_month_2": "mean",
    "occupancy_month_3": "mean",
    "occupancy_month_4": "mean",
    "occupancy_month_5": "mean",
    "occupancy_month_6": "mean",
    "occupancy_month_7": "mean",
    "occupancy_month_8": "mean",
    "occupancy_month_9": "mean",
    "occupancy_month_10": "mean",
    "occupancy_month_11": "mean",
    "occupancy_month_12": "mean",
    
    "rate_month_1": "mean_non_zero",
    "rate_month_2": "mean_non_zero",
    "rate_month_3": "mean_non_zero",
    "rate_month_4": "mean_non_zero",
    "rate_month_5": "mean_non_zero",
    "rate_month_6": "mean_non_zero",
    "rate_month_7": "mean_non_zero",
    "rate_month_8": "mean_non_zero",
    "rate_month_9": "mean_non_zero",
    "rate_month_10": "mean_non_zero",
    "rate_month_11": "mean_non_zero",
    "rate_month_12": "mean_non_zero",
    
    "area_population": "sum",
    "area_houses": "sum",
    "area_population_density": "mean_non_zero",
    "area_housing_density": "mean_non_zero",
    "population": "sum",
    "houses": "sum",
    "population_density": "mean_non_zero",
    "housing_density": "mean_non_zero",
    "land_area": "sum",
    "land_sqmi": "sum",
    "water_area": "sum",
    "land_sqmi": "sum",
    
    "State": "frequency",
    "state": "frequency",
    "zipcode": "frequency",
    "address_zipcode": "frequency",
    "address_zip_code": "frequency",
    "address_zcta": "frequency",
    "address_city": "frequency",
    "address_state": "frequency",
    "city": "frequency",
    "address": None,
    
    "city_zipcodes": None,
    "county_zipcodes": None,
    
    #"area_rev_per_bedroom": "mean",
    #"area_rev_per_bathroom": "mean",
    #"area_bedroom_vs_mean": "mean",
    #"area_bathroom_vs_mean": "mean",
    #"rev_vs_bedroom_mean": False,
    #"rev_vs_area_bedroom_mean": "mean",
    "rev_per_bedroom": False,
    "rev_per_bathroom": False,
    "rate_per_bedroom": "mean_non_zero",
    "rate_per_bathroom": "mean_non_zero",
    
    "nearest_hotel_dist_0": "mean_non_zero",
    "nearest_hotel_rate_0": "mean_non_zero",
    "nearest_hotel_dist_1": "mean_non_zero",
    "nearest_hotel_rate_1": "mean_non_zero",
    "nearest_hotel_dist_2": "mean_non_zero",
    "nearest_hotel_rate_2": "mean_non_zero",
    "nearest_hotel_dist_3": "mean_non_zero",
    "nearest_hotel_rate_3": "mean_non_zero",
    "nearest_hotel_dist_4": "mean_non_zero",
    "nearest_hotel_rate_4": "mean_non_zero",
    "nearest_hotel_dist_5": "mean_non_zero",
    "nearest_hotel_rate_5": "mean_non_zero",
    
    'pt_area_revenue': 'mean',
    'pt_area_rev_diff': 'mean',
    'rev_vs_pt_area': 'mean',
    'rev_vs_mean': 'mean',
    'pt_revenue': 'mean',
    'rev_vs_pt': 'mean',
    'area_vs_mean': 'mean',
    'pt_area_vs_mean': 'mean',
    'pt_rent': 'mean',
    
    "rev_per_bedroom": 'mean_non_zero',
    "rev_per_bathroom": 'mean_non_zero',
    "rev_per_sqft": 'mean_non_zero',
    "rev_per_acre": 'mean_non_zero',
    "rev_per_price": 'mean_non_zero',
    
    "rent_per_bedroom": 'mean_non_zero',
    "rent_per_bathroom": 'mean_non_zero',
    "rent_per_sqft": 'mean_non_zero',
    "rent_per_acre": 'mean_non_zero',
    "rent_per_price": 'mean_non_zero',
    
    "price_per_bedroom": 'mean_non_zero',
    "price_per_bathroom": 'mean_non_zero',
    "price_per_sqft": 'mean_non_zero',
    "price_per_acre": 'mean_non_zero',
    
    "rate_per_bedroom": 'mean_non_zero',
    "rate_per_bathroom": 'mean_non_zero',
    "rate_per_sqft": 'mean_non_zero',
    "rate_per_acre": 'mean_non_zero',
    "rate_per_price": 'mean_non_zero',
    
    #"listed_rate_per_bedroom": 'mean_non_zero',
    #"listed_rate_per_bathroom": 'mean_non_zero',
    #"listed_rate_per_sqft": 'mean_non_zero',
    #"listed_rate_per_acre": 'mean_non_zero',
    #"listed_rate_per_price": 'mean_non_zero',
    "sqft_cap": 'mean_non_zero',
    "area_sqft_cap": 'mean_non_zero',
    "cap_sqft": 'mean_non_zero',
    "area_cap_sqft": 'mean_non_zero',
    "rent_cap_sqft": 'mean_non_zero',
    "area_rent_cap_sqft": 'mean_non_zero',

    "airbnb": "mean",
    "homeaway": "mean",
    "pets_allowed": "mean",
    "guesthouse": "mean",
    "gps_distance": "mean_non_zero",
    "elevation": "mean_non_zero",
    "property_tax_rate": "mean_non_zero",
    "tax_annual_amount": "mean_non_zero",
    "monthly_hoa_fee": "mean_non_zero",
    "hoa_monthly_fee": "mean_non_zero",
    "reservation_length": "mean_non_zero",
    "revenue_per_reservation": "mean_non_zero",
    
    "property_condition": "mean",
    
    "ztca_houses": "mean",
    "ztca_housing_density": "mean_non_zero",
    "ztca_population": "mean",
    "ztca_population_density": "mean_non_zero",
    
}

def agg_avg(series):
    d_avg = np.mean(series.replace(np.inf, np.nan).dropna())
    return round(d_avg, 8) if not np.isinf(d_avg) and not np.isnan(d_avg) else 0
    #return d_avg if not np.isinf(d_avg) and not np.isnan(d_avg) else 0

def agg_avg_non_zero(series):
    a_vals = [x for x in series.replace(np.inf, np.nan).dropna() if x != 0]
    if not a_vals: return np.nan
    d_avg = np.mean(a_vals)
    return round(d_avg, 8) if not np.isinf(d_avg) and not np.isnan(d_avg) else np.nan
    #return d_avg if not np.isinf(d_avg) and not np.isnan(d_avg) else np.nan

def agg_median_non_zero(series):
    a_vals = [x for x in series.replace(np.inf, np.nan).dropna() if x != 0]
    if not a_vals: return np.nan
    d_avg = np.median(a_vals)
    return round(d_avg, 8) if not np.isinf(d_avg) and not np.isnan(d_avg) else np.nan
    #return d_avg if not np.isinf(d_avg) and not np.isnan(d_avg) else np.nan

def agg_10(series):
    d_med = np.median(series.replace(np.inf, np.nan).dropna())
    return round(d_med) if not np.isinf(d_med) and not np.isnan(d_med) else 0

def agg_freq(series):
    e_freqs = Counter(series.replace(np.inf, np.nan).dropna())
    return max(e_freqs.items(), key=itemgetter(1))[0] if e_freqs else None


def select_aggregators(df, **kwargs):
    e_col_aggs = {
        col: agg
        for col, agg in e_DEFAULT_AGG_TYPES.items()
        if col in df.columns}
    
    for i, col in enumerate(df.columns):
        if col in e_col_aggs: continue
        #print(col)
        e_col_vals = dict(df[col].value_counts())
        if e_col_vals:
            if all(x in {1, 0} for x in e_col_vals):
                if kwargs.get("b_debug"): print("10 col", col)
                e_col_aggs[col] = "mean"
            elif all(x in {True, False} for x in e_col_vals):
                if kwargs.get("b_debug"): print("bool col", col)
                e_col_aggs[col] = "boolean"
            #elif all(x in {1, 0} for x in e_col_vals):
            #    e_col_aggs[col] = "10"
            elif any(isinstance(x, str) for x in e_col_vals):
                if kwargs.get("b_debug"): print("freq col", col)
                e_col_aggs[col] = "frequency"
            else:
                if kwargs.get("b_debug"): print("mean col", col)
                e_col_aggs[col] = "mean"
    #return e_col_aggs
    e_col_aggregators = {
        col:
            kind if kind in {"median", "sum", "min", "max"} else
            agg_median_non_zero if kind in {"median_non_zero"} else
            agg_avg_non_zero if kind in {"mean_non_zero"} else
            agg_avg if kind in {"mean"} else
            agg_10 if kind in {"10", "boolean"} else
            agg_freq if kind == "frequency" else
            agg_mean if col.startswith(("location_", "feature_",)) else
            agg_freq
        for col, kind in e_col_aggs.items()
        if kind != False
    }
    return e_col_aggregators


class PTypeAnalysis:
    def __init__(self, df_property_types_expanded, rev_col="avg_revenue", count_col="count"):
        self.rev_col = rev_col
        self.count_col = count_col
        e_prop_types = defaultdict(Counter)
        a_val_cols = [self.rev_col, self.count_col]
        print("a_val_cols", a_val_cols)
        a_type_cols = [col for col in df_property_types_expanded.columns if col not in a_val_cols]
        print("a_type_cols", a_type_cols)
        i_offset = 1 + len(a_type_cols)
        for t_row in df_property_types_expanded[a_type_cols + a_val_cols].itertuples():
            t_type = t_row[1: 1 + len(a_type_cols)]
            for i, s_val_col in enumerate(a_val_cols):
                d_val = t_row[i_offset + i]
                e_prop_types[s_val_col][t_type] = d_val
            #break
        self._prop_types = dict(e_prop_types)
        
        self._property_type_vals = None
        self._present_property_types, self._key_cols = None, None
    
    #def _lookup_type_revenue(self, s_type, i_bedrooms, i_bathrooms, i_pool, i_fireplace):
        #return self._prop_types.get("avg_revenue", {}).get((s_type, i_bedrooms, i_bathrooms, i_pool, i_fireplace))
    def _lookup_type_revenue(self, s_type, i_bedrooms, i_bathrooms, i_pool):
        return self._prop_types.get(self.rev_col, {}).get((s_type, i_bedrooms, i_bathrooms, i_pool))
    
    #def _lookup_type_count(self, s_type, i_bedrooms, i_bathrooms, i_pool, i_fireplace):
        #return self._prop_types.get("count", {}).get((s_type, i_bedrooms, i_bathrooms, i_pool, i_fireplace))
    def _lookup_type_count(self, s_type, i_bedrooms, i_bathrooms, i_pool):
        return self._prop_types.get(self.count_col, {}).get((s_type, i_bedrooms, i_bathrooms, i_pool))
        
    def _get_property_type_key(self, t_row):
        s_type = "House"
        for (s_pt_col, s_pt), v_val in zip(self._present_property_types, t_row[1:]):
            #print((s_pt_col, s_pt), v_val, type(v_val))
            if isinstance(v_val, str):
                s_type = v_val
            elif v_val > 0:
                s_type = s_pt
                #print(s_pt)
                break
        return (s_type,) + t_row[-self._key_cols_count:]
        
    def analyze(self, df_input):
        a_property_type_cols = [col for col in df_input.columns if col.startswith("property_type")]
        self._property_type_vals = {
            pt: re.sub(r"property[-_ ]type[-_ ]", "", pt, flags=re.I)
            for pt in a_property_type_cols}
        #print(self._property_type_vals)
        
        self._present_property_types = [
            (col, self._property_type_vals[col])
            for col in df_input.columns
            if col in self._property_type_vals]
        self._present_property_types_count = len(self._present_property_types)
        print(f"{self._present_property_types_count} present property types:", self._present_property_types)
        
        #self._key_cols = ["bedrooms", "bathrooms", "feature_pool", "feature_elevator"]
        self._key_cols = ["bedrooms", "bathrooms", "feature_pool",]
        self._key_cols_count = len(self._key_cols)
        print(f"{self._key_cols_count} key cols:", self._key_cols)
    
        print([t[0] for t in self._present_property_types] + self._key_cols)
        n_start = time()
        a_type_labels = [
            self._get_property_type_key(t_row)
            for t_row in df_input[[t[0] for t in self._present_property_types] + self._key_cols].itertuples()]
        print("Property Type Label Selections took:", utils.secondsToStr(time() - n_start))
        
        n_start = time()
        a_type_revenues = [
            self._lookup_type_revenue(*t_key)
            for t_key in a_type_labels]
        print("Property Type Revenue Lookups took:", utils.secondsToStr(time() - n_start))
        
        return a_type_labels, a_type_revenues


def get_reliable_properties(df_set, d_percentage=0.75, d_min_records=4, **kwargs):
    #df_set["reliability"] = df_set["occupancy_rate_ltm"] * df_set["availability"]
    df_set["known_price"]  = df_set["price"].apply(lambda x: 1 if x > 150000 and x < 7500000 else 0.5 if x > 75000 else 0.1)
    
    if "rate" in df_set.columns:
        df_set["good_rate"]  = df_set["rate"].apply(lambda x: 1 if 20 < x < 7500 else 0.25)
    if "rent" in df_set.columns:
        df_set["good_rent"]  = df_set["rent"].apply(lambda x: 1 if 500 <= x < 7500 else 0.25)
    
    df_set["good_living_area"]  = df_set["living_area"].apply(lambda x: 1 if 600 < x < 6500 else 0.5)
    
    if "occupancy" in df_set.columns and "availability" in df_set.columns:
        d_min_reliability = kwargs.get("d_min_reliability_score", 0.1)
        if "rate" in df_set.columns:
            df_set["reliability"] = df_set["occupancy"] * df_set["availability"] * df_set["known_price"] * df_set["good_rate"] * df_set["good_living_area"]
        elif "rent" in df_set.columns:
            df_set["reliability"] = df_set["occupancy"] * df_set["availability"] * df_set["known_price"] * df_set["good_rent"] * df_set["good_living_area"]
        else:
            df_set["reliability"] = df_set["occupancy"] * df_set["availability"] * df_set["known_price"] * df_set["good_living_area"]
    else:
        d_min_reliability = kwargs.get("d_min_reliability_score", 0.25)
        if "rate" in df_set.columns:
            df_set["reliability"] = df_set["known_price"] * df_set["good_rate"] * df_set["good_living_area"]
        elif "rent" in df_set.columns:
            df_set["reliability"] = df_set["known_price"] * df_set["good_rent"] * df_set["good_living_area"]
        else:
            df_set["reliability"] = df_set["known_price"] * df_set["good_living_area"]
        
    df_reliable_set = df_set[df_set["reliability"] >= d_min_reliability]
    i_records = min(df_reliable_set.shape[0], max(d_min_records, int(df_reliable_set.shape[0] * d_percentage)))
    #print(i_records, len(df_set))
    if "availability" in df_set.columns:
        return df_reliable_set.sort_values(["reliability", "availability"], ascending=False)[:i_records]
    else:
        return df_reliable_set.sort_values(["reliability"], ascending=False)[:i_records]


class AreaAggByPrecision:
    def __init__(self, df, **kwargs):
        self._start_precision, self._max_precision = kwargs.get("min_precision", -2), kwargs.get("max_precision", 4)

        self._min_properties, self._min_reliable_properties = kwargs.get("min_properties", 5), kwargs.get("min_reliable_properties", 2)
        self._target_properties, self._target_reliable_properties = kwargs.get("target_properties", 20), kwargs.get("target_reliable_properties", 10)
        self._max_properties, self._max_reliable_properties = kwargs.get("max_properties", 50), kwargs.get("max_reliable_properties", 20)
        
        self._areas, self._areas_sizes = defaultdict(dict), defaultdict(dict)

        self.df = df.copy()
        if "rows" not in self.df.columns:
            self.df["rows"] = 1
        if "reliable_rows" not in self.df.columns:
            self.df["reliable_rows"] = 0
        
        self._col_aggregators = select_aggregators(self.df)
        self.cols = [col for col in self.df if col in self._col_aggregators]
        
    def subdivide(self,):
        self._areas, self._areas_sizes = defaultdict(dict), defaultdict(dict)
        n_start = time()
        
        dc_total_aggs = self.df.agg(self._col_aggregators)
        dc_total_aggs["lat_area"], dc_total_aggs["long_area"] = (0, 0)
        dc_total_aggs["area_precision"] = -3
        self._areas[-3][(0, 0)] = dc_total_aggs
        
        for i_prec in range(self._start_precision, self._max_precision + 1):
            print(f"Creatings area summaries at precision: {i_prec}")
            #if i_prec > 0: break

            #df_test[f"lat_area_{i_prec}"] = df_test["Latitude"].round()
            self.df[f"lat_area"] = self.df["latitude"].round(i_prec)
            self.df[f"long_area"] = self.df["longitude"].round(i_prec)
            
            for t_area, df_area in self.df.groupby(["lat_area", "long_area"]):
                i_size = len(df_area)
                if i_size < self._min_properties:
                    #print(f"{i_prec}, {t_area}: not enough properties to become an area: {i_size}", )
                    continue
                if self._areas_sizes.get(i_prec, {}).get(t_area) == i_size:
                    #print(f"{i_prec}, {t_area}: area was already processed: {i_size}",)
                    continue
                
                df_reliable = get_reliable_properties(df_area, d_min_records=self._target_reliable_properties)
                
                self._areas_sizes[i_prec][t_area] = i_size
                
                if df_reliable.shape[0] >=  self._min_reliable_properties:
                    dc_aggs = df_reliable.agg(self._col_aggregators)
                elif df_area.shape[0] >=  self._min_properties:
                    dc_aggs = df_area.agg(self._col_aggregators)
                else:
                    p#rint(f"{i_prec}, {t_area}: not enough reliable or total properties to aggregate: {df_area.shape[0]}", )
                    continue
                
                #print(f"{i_prec}, {t_area}")
                dc_aggs["rows"] = df_area.shape[0]
                dc_aggs["reliable_rows"] = df_reliable.shape[0]
                
                dc_aggs["area_precision"] = i_prec
                dc_aggs["lat_area"], dc_aggs["long_area"] = t_area
                self._areas[i_prec][t_area] = dc_aggs
                #break
                
            print(f"Creatings area summaries at precision: {i_prec}", utils.secondsToStr(time() - n_start))
            
        print(utils.secondsToStr(time() - n_start))
        return self._areas, self._areas_sizes
    
    def get_areas(self):
        if not self._areas: self.subdivide()
        
        a_area_summaries = []
        for i_prec, e_areas in self._areas.items():
            df_area_summaries = pd.DataFrame(
                e_areas.values(),
                columns=["area_precision", "lat_area", "long_area"] + self.cols)
            a_area_summaries.append(df_area_summaries)
        df_areas_summaries = pd.concat(a_area_summaries) if a_area_summaries else None
        
        df_areas_summaries.rename(
            columns={
                "revenue": "area_raw_revenue",
                "adj_revenue": "area_revenue",
                "rate": "area_rate",
                #"published_nightly_rate": "area_listed_rate",
                #"listed_rate": "area_listed_rate",
                "pt_revenue": "pt_area_revenue",
                "pt_area_difference": "pt_area_rev_diff",
                
                #"price": "area_price",
                "price": "area_price",
                #"rent_estimate": "area_rent",
                "rent": "area_rent",
                
                "rows": "properties",
                "reliable_rows": "reliable_properties",
                
                "rev_per_bedroom": "area_rev_per_bedroom",
                "rev_per_bathroom": "area_rev_per_bathroom",
                "rev_per_sqft": "area_rev_per_sqft",
                "rev_per_acre": "area_rev_per_acre",
                
                "rate_per_bedroom": "area_rate_per_bedroom",
                "rate_per_bathroom": "area_rate_per_bathroom",
                "rate_per_sqft": "area_rate_per_sqft",
                "rate_per_acre": "area_rate_per_acre",
                
                "rent_per_bedroom": "area_rent_per_bedroom",
                "rent_per_bathroom": "area_rent_per_bathroom",
                "rent_per_sqft": "area_rent_per_sqft",
                "rent_per_acre": "area_rent_per_acre",
                
                "price_per_bedroom": "area_price_per_bedroom",
                "price_per_bathroom": "area_price_per_bathroom",
                "price_per_sqft": "area_price_per_sqft",
                "price_per_acre": "area_price_per_acre",
                
                "reservation_length": "area_reservation_length",

                "demand_seasonality": "area_demand_seasonality",
                "demand_season_type": "area_demand_season_type",
                "demand_season_type_Month": "area_demand_season_type_Month",
                "demand_season_type_Quarter": "area_demand_season_type_Quarter",
                "demand_season_length": "area_demand_season_length",
                "demand_season_peak_month": "area_demand_season_peak_month",
                "demand_season_max_month": "area_demand_season_max_month",
                
                "availability_seasonality": "area_availability_seasonality",
                "availability_season_type": "area_availability_season_type",
                "availability_season_type_Month": "area_availability_season_type_Month",
                "availability_season_type_Quarter": "area_availability_season_type_Quarter",
                "availability_season_length": "area_availability_season_length",
                "availability_season_peak_month": "area_availability_season_peak_month",
                "availability_season_max_month": "area_availability_season_max_month",
                
                "zcta_population": "area_population",
                "zcta_houses": "area_houses",
                "zcta_population_density": "area_population_density",
                "zcta_housing_density": "area_housing_density",
                
                
                "monthly_hoa_fee": "area_monthly_hoa_fee",
                "property_tax_rate": "area_property_tax_rate",
                #"sqft_cap": "area_sqft_cap",
                "cap_sqft": "area_cap_sqft",
                "tax_annual_amount": "area_tax_annual_amount",
                
                "property_tax_rate": "area_property_tax_rate",
                "tax_annual_amount": "area_tax_annual_amount",
                "monthly_hoa_fee": "area_monthly_hoa_fee",
                "reservation_length": "area_reservation_length",
            },
            inplace=True,
        )
        for col in ["area_revenue", "pt_area_revenue", "area_rent", "pt_area_rent", "pt_area_rev_diff", "pt_area_rent_diff"]:
            if col in df_areas_summaries.columns:
                df_areas_summaries[col] = df_areas_summaries[col].round(1)
        
        if "area_revenue" in df_areas_summaries.columns:
            df_areas_summaries["rev_per_bedroom"] = (df_areas_summaries["area_revenue"] / df_areas_summaries["bedrooms"]).replace(np.inf, np.nan).fillna(0).round(2)
            df_areas_summaries["rev_per_bathroom"] = (df_areas_summaries["area_revenue"] / df_areas_summaries["bathrooms"]).replace(np.inf, np.nan).fillna(df_areas_summaries["rev_per_bedroom"]).round(2)
            df_areas_summaries["rev_per_sqft"] = (df_areas_summaries["area_revenue"] / df_areas_summaries["living_area"]).replace(np.inf, np.nan).fillna(0).round(2)
            df_areas_summaries["rev_per_acre"] = (df_areas_summaries["area_revenue"] / df_areas_summaries["lot_size"]).replace(np.inf, np.nan).fillna(0).round(2)
            df_areas_summaries["rev_per_price"] = (df_areas_summaries["area_revenue"] / df_areas_summaries["area_price"]).replace(np.inf, np.nan).fillna(0).round(2)
        
        if "area_rate" in df_areas_summaries.columns:
            df_areas_summaries["rate_per_bedroom"] = (df_areas_summaries["area_rate"] / df_areas_summaries["bedrooms"]).replace(np.inf, np.nan).fillna(0).round(2)
            df_areas_summaries["rate_per_bathroom"] = (df_areas_summaries["area_rate"] / df_areas_summaries["bathrooms"]).replace(np.inf, np.nan).fillna(df_areas_summaries["rate_per_bedroom"]).round(2)
            df_areas_summaries["rate_per_sqft"] = (df_areas_summaries["area_rate"] / df_areas_summaries["living_area"]).replace(np.inf, np.nan).fillna(0).round(2)
            df_areas_summaries["rate_per_acre"] = (df_areas_summaries["area_rate"] / df_areas_summaries["lot_size"]).replace(np.inf, np.nan).fillna(0).round(2)
            df_areas_summaries["rate_per_price"] = (df_areas_summaries["area_rate"] / df_areas_summaries["area_price"]).replace(np.inf, np.nan).fillna(0).round(4)
        
        #df_areas_summaries["listed_rate_per_bedroom"] = (df_areas_summaries["area_listed_rate"] / df_areas_summaries["bedrooms"]).replace(np.inf, np.nan).fillna(0).round(2)
        #df_areas_summaries["listed_rate_per_bathroom"] = (df_areas_summaries["area_listed_rate"] / df_areas_summaries["bathrooms"]).replace(np.inf, np.nan).fillna(df_areas_summaries["listed_rate_per_bedroom"]).round(2)
        #df_areas_summaries["listed_rate_per_sqft"] = (df_areas_summaries["area_listed_rate"] / df_areas_summaries["living_area"]).replace(np.inf, np.nan).fillna(0).round(2)
        #df_areas_summaries["listed_rate_per_acre"] = (df_areas_summaries["area_listed_rate"] / df_areas_summaries["lot_size"]).replace(np.inf, np.nan).fillna(0).round(2)
        
        if "area_rent" in df_areas_summaries.columns:
            df_areas_summaries["rent_per_bedroom"] = (df_areas_summaries["area_rent"] / df_areas_summaries["bedrooms"]).replace(np.inf, np.nan).fillna(0).round(2)
            df_areas_summaries["rent_per_bathroom"] = (df_areas_summaries["area_rent"] / df_areas_summaries["bathrooms"]).replace(np.inf, np.nan).fillna(df_areas_summaries["rent_per_bedroom"]).round(2)
            df_areas_summaries["rent_per_sqft"] = (df_areas_summaries["area_rent"] / df_areas_summaries["living_area"]).replace(np.inf, np.nan).fillna(0).round(2)
            df_areas_summaries["rent_per_acre"] = (df_areas_summaries["area_rent"] / df_areas_summaries["lot_size"]).replace(np.inf, np.nan).fillna(0).round(2)
            df_areas_summaries["rent_per_price"] = (df_areas_summaries["area_rent"] / df_areas_summaries["area_price"]).replace(np.inf, np.nan).fillna(0).round(4)
        
        
        if "area_price" in df_areas_summaries.columns:
            df_areas_summaries["price_per_bedroom"] = (df_areas_summaries["area_price"] / df_areas_summaries["bedrooms"]).replace(np.inf, np.nan).fillna(0).round(2)
            df_areas_summaries["price_per_bathroom"] = (df_areas_summaries["area_price"] / df_areas_summaries["bathrooms"]).replace(np.inf, np.nan).fillna(df_areas_summaries["price_per_bedroom"]).round(2)
            df_areas_summaries["price_per_sqft"] = (df_areas_summaries["area_price"] / df_areas_summaries["living_area"]).replace(np.inf, np.nan).fillna(0).round(2)
            df_areas_summaries["price_per_acre"] = (df_areas_summaries["area_price"] / df_areas_summaries["lot_size"]).replace(np.inf, np.nan).fillna(0).round(2)
        
        #df_areas_summaries["rate_per_bedroom"] = (df_areas_summaries["published_nightly_rate"] / df_areas_summaries["bedrooms"]).replace(np.inf, np.nan).fillna(0).round(2)
        #df_areas_summaries["rate_per_bedroom"] = (df_areas_summaries["area_rate"] / df_areas_summaries["bedrooms"]).replace(np.inf, np.nan).fillna(0).round(2)
        #df_areas_summaries["listed_rate_per_bedroom"] = (df_areas_summaries["area_listed_rate"] / df_areas_summaries["bedrooms"]).replace(np.inf, np.nan).fillna(0).round(2)
        
        return df_areas_summaries.sort_values(["area_precision", "properties"])


class AreaLookups:
    def __init__(self, s_area_file, i_min_precision=-3, i_max_precision=4):
        self.default_location = (-3, 0.0, 0.0)
        df_areas = pd.read_csv(s_area_file)
        
        for s_col in a_ALL_PRECISION_COLS:
            s_plain = re.sub("^area_", "", s_col)
            if s_plain in df_areas.columns and s_col not in df_areas.columns:
                #print("rename", s_plain, s_col)
                df_areas.rename(columns={s_plain: s_col}, inplace=True)
                
        self.min_prec = max(i_min_precision, int(df_areas["area_precision"].min()))
        self.max_prec = min(i_max_precision, int(df_areas["area_precision"].max()))
        
        dc_index = pd.MultiIndex.from_tuples(
            list(map(tuple, zip(
                df_areas["area_precision"].round(),
                df_areas["lat_area"].round(self.max_prec + 1),
                df_areas["long_area"].round(self.max_prec + 1),
            ))),
            names=["precision", "lat", "long"])
        df_areas.index = dc_index
        
        self._areas_df = df_areas
        self.areas = df_areas.to_dict(orient="index")
        
        self.prec_range = list(reversed(range(self.min_prec, self.max_prec + 1)))
        self.prec_range_fwd = list(range(self.min_prec, self.max_prec + 1))
        print(f"{len(self.areas):,} areas")
    
    def find_area(self, latitude, longitude, i_max_precision=4):
        for i_prec in self.prec_range:
            if i_prec > i_max_precision: continue
            try:
                t_area = (i_prec, round(latitude, i_prec), round(longitude, i_prec))
                #print(t_area)
                #row = self.areas.loc[t_area]
                #return t_area, dict(row)
                e_area = self.areas[t_area]
                return t_area, e_area
            except:
                continue
        return ((-3, 0, 0), {})
    
    def get_areas(self, latitude, longitude, b_label_scale=False, b_normalize=True, **kwargs):
        a_areas = []
        for i_prec in self.prec_range_fwd:
            try:
                t_area = (i_prec, round(latitude, i_prec), round(longitude, i_prec))
                #print(t_area)
                #row = self.areas.loc[t_area]
                #a_areas.append((t_area, dict(row)))
                e_area = self.areas[t_area]
                
                if b_label_scale:
                    e_area["area_scale"] = e_PREC_SIZE_IMP.get(e_area["area_precision"], "nationwide")
                
                if b_normalize:
                    #print(e_area.get("area_demand_seasonality"))
                    if e_area.get("area_demand_seasonality", 0) > 0:
                        e_area["area_demand_seasonality"] = min(1, e_area["area_demand_seasonality"])
                        e_area["area_demand_seasonality_normalized"] = max(0, 0.368 * math.log(e_area["area_demand_seasonality"]) + 0.9771)
                    #if e_area.get("short_rental_penetration"): 
                    #    e_area["short_rental_penetration_normalized"] = e_area["short_rental_penetration"]
                
                a_areas.append((t_area, e_area))
            except Exception as e:
                #print(e)
                break
        if not a_areas:
            if b_label_scale:
                a_areas = [((-3, 0, 0), {"area_scale": "nationwide"})]
            else:
                a_areas = [((-3, 0, 0), {})]
        return a_areas


def get_area_col_sets(a_property_areas):
    e_property_area_info = {}
    for i_prec_set, (t_area, e_area) in enumerate(a_property_areas):
        s_prefix = f"{t_area[0]}_"
        for s_col in a_ALL_PRECISION_COLS:
            if s_col in e_area:
                e_property_area_info[f"{s_prefix}{s_col}"] = e_area[s_col]
    return e_property_area_info


def get_merged_area_col_sets(a_str_property_areas, a_ltr_property_areas):
    e_property_area_info = {}
    
    if len(a_str_property_areas) < len(a_ltr_property_areas):
        a_str_property_areas = a_str_property_areas + [(None, {})] * (len(a_ltr_property_areas) - len(a_str_property_areas))
    elif len(a_str_property_areas) > len(a_ltr_property_areas):
        a_ltr_property_areas = a_ltr_property_areas + [(None, {})] * (len(a_str_property_areas) - len(a_ltr_property_areas))
    #len(a_str_property_areas), len(a_ltr_property_areas)
    
    for i_prec_set, ((t_str_area, e_str_area), (t_ltr_area, e_ltr_area)) in enumerate(zip(a_str_property_areas, a_ltr_property_areas)):
        if t_str_area:
            s_prefix = f"{t_str_area[0]}_"
        else:
            s_prefix = f"{t_ltr_area[0]}_"
        
        if e_str_area and e_ltr_area:
            i_str_props = e_str_area.get("area_properties", 0)
            i_ltr_props = e_ltr_area.get("area_properties", 0)
            i_properties = i_str_props + i_ltr_props
            d_str = i_str_props / i_properties
            d_ltr = i_ltr_props / i_properties
        elif e_str_area:
            d_str, d_ltr = 1, 0
        elif e_ltr_area:
            d_str, d_ltr = 0, 1
        else:
            d_str, d_ltr = 0.5, 0.5
        
        for s_col in a_ALL_PRECISION_COLS:
            if s_col in e_SEPARATE_LTR_COLS:
                e_property_area_info[f"{s_prefix}{s_col}"] = e_str_area.get(s_col)
                e_property_area_info[f"{s_prefix}{e_SEPARATE_LTR_COLS[s_col]}"] = e_ltr_area.get(s_col)
            elif s_col in c_SEPARATE_STR_COLS or d_str == 1 or isinstance(e_str_area.get(s_col), str) or e_ltr_area.get(s_col) is None:
                e_property_area_info[f"{s_prefix}{s_col}"] = e_str_area.get(s_col)
            elif s_col in c_PREFERRED_LTR_COLS:
                e_property_area_info[f"{s_prefix}{s_col}"] = e_ltr_area.get(s_col, e_str_area.get(s_col))
            elif d_ltr == 1 or e_str_area.get(s_col) is None:
                e_property_area_info[f"{s_prefix}{s_col}"] = e_ltr_area.get(s_col)
            else:
                e_property_area_info[f"{s_prefix}{s_col}"] = (e_str_area.get(s_col) * d_str) + (e_ltr_area.get(s_col) * d_ltr)
            
            #if s_col in e_ltr_area and (not e_str_area or col in {} ):
            #    e_property_area_info[f"{s_prefix}{s_col}"] = e_area[s_col]
    
    return e_property_area_info


def expand_pai(e_pai, i_min_precision=-3, i_max_precision=3, a_cols=None):
    e_pai_filled = e_pai.copy()
    a_precs = list(range(i_min_precision, i_max_precision + 1))
    
    if not a_cols: a_cols = a_ALL_PRECISION_COLS
    
    for i, j in zip(a_precs, a_precs[1:]):
        if j > i_max_precision: continue
        for s_col in a_cols:
            if s_col == "area_properties":
                if f"{j}_{s_col}" not in e_pai_filled:
                    e_pai_filled[f"{j}_{s_col}"] = 0
            elif s_col == "area_scale":
                if f"{j}_{s_col}" not in e_pai_filled:
                    e_pai_filled[f"{j}_{s_col}"] = e_PREC_SIZE_IMP.get(j, "nationwide")
            else:
                if f"{j}_{s_col}" not in e_pai_filled and f"{i}_{s_col}" in e_pai_filled:
                    e_pai_filled[f"{j}_{s_col}"] = e_pai_filled[f"{i}_{s_col}"]
            
        if f"{j}_area_properties" not in e_pai_filled and f"{i}_area_properties" in e_pai_filled:
            e_pai_filled[f"{j}_area_properties"] = e_pai_filled[f"{i}_area_properties"]
            
    return e_pai_filled


def add_area_info(df_input, o_area_lookups, s_lat_col="latitude", s_long_col="longitude"):
    df_input = df_input.copy()
    n_start = time()
    a_property_areas_info = []
    a_property_areas_info_dfs = []
    i_batch = 0
    i_batch_size = 25000
    
    for s_col in a_ALL_PRECISION_COLS:
        s_plain = re.sub("^area_", "", s_col)
        if s_plain in df_input.columns and s_col not in df_input.columns:
            df_input.rename(columns={s_plain: s_col}, inplace=True)
    
    for idx, latitude, longitude in df_input[[s_lat_col, s_long_col]].itertuples():
        a_pas = o_area_lookups.get_areas(latitude, longitude, b_label_scale=True)
        e_pai = get_area_col_sets(a_pas)
        e_pai["Property ID"] = idx
        
        a_property_areas_info.append(e_pai)

        if len(a_property_areas_info) == i_batch_size:
            i_batch += 1
            print(f"Batch {i_batch:,}, {utils.secondsToStr(time() - n_start)}")
            df = pd.DataFrame(a_property_areas_info).set_index("Property ID")
            a_property_areas_info_dfs.append(df)
            a_property_areas_info = []
            #break
        #break
    
    #print("# a_property_areas_info", len(a_property_areas_info))

    if len(a_property_areas_info) > 0:
        i_batch += 1
        print(f"Batch {i_batch:,}, {utils.secondsToStr(time() - n_start)}")
        df = pd.DataFrame(a_property_areas_info).set_index("Property ID")
        a_property_areas_info_dfs.append(df)
        a_property_areas_info = []
    
    df_property_areas_info = pd.concat(a_property_areas_info_dfs)
    print(utils.secondsToStr(time() - n_start))
    return df_property_areas_info


def add_multi_area_info_to_dict(e_input, o_str_area_lookups, o_ltr_area_lookups, **kwargs):
    e_output = {k: v for k, v in e_input.items() if not pd.isnull(v)}
    
    latitude, longitude = e_input[kwargs.get("s_lat_col", "latitude")], e_input[kwargs.get("s_long_col", "longitude")]
    
    a_str_pas = o_str_area_lookups.get_areas(latitude, longitude, b_label_scale=True)
    a_ltr_pas = o_ltr_area_lookups.get_areas(latitude, longitude, b_label_scale=True)
    
    e_pai = get_merged_area_col_sets(a_str_pas, a_ltr_pas)
    e_expanded_pai = expand_pai(e_pai, i_max_precision=kwargs.get("i_max_precision", 3))
    
    e_str_output, e_str_pai = add_area_info_to_dict(
        e_input,
        o_str_area_lookups,
        b_merge_input=False,
        a_pas=a_str_pas,
        e_pai=e_pai,
        e_expanded_pai=e_expanded_pai,
        b_include_availability=True,
        b_include_seasonality=True,
        **kwargs)
    e_ltr_output, e_ltr_pai = add_area_info_to_dict(
        e_input,
        o_ltr_area_lookups,
        b_merge_input=False,
        a_pas=a_ltr_pas,
        e_pai=e_pai,
        e_expanded_pai=e_expanded_pai,
        b_include_availability=False,
        b_include_seasonality=False,
        **kwargs)
    
    e_output.update({k: v for k, v in e_str_output.items() if k not in e_output})
    
    for k, v in e_ltr_output.items():
        if k in e_input:
            continue
        elif pd.isnull(v):
            continue
        elif k in e_LTR_RENAMES:
            e_output[e_LTR_RENAMES[k]] = v
        else:
            v_pre = e_output.get(k)
            if pd.isnull(v_pre):
                e_output[k] = v
            elif isinstance(v_pre, str):
                continue
            else:
                e_output[k] = (v_pre + v) / 2
                #print(f"{k}, pre:{v_pre}, ltr:{v}, output:{e_output[k]}")
    return e_output, e_expanded_pai

        
def add_area_info_to_dict(e_input, o_area_lookups, s_lat_col="latitude", s_long_col="longitude", i_min_precision=-1, i_max_precision=3, b_include_availability=True, b_include_seasonality=True, b_merge_input=True, **kwargs):
    n_start = time()
    
    latitude, longitude = e_input[s_lat_col], e_input[s_long_col]
    
    if kwargs.get("a_pas"):
        a_pas = kwargs["a_pas"]
    else:
        a_pas = o_area_lookups.get_areas(latitude, longitude, b_label_scale=True)
        
    if kwargs.get("e_pai"):
        e_pai = kwargs["e_pai"]
    else:
        e_pai = get_area_col_sets(a_pas)
    
    if kwargs.get("e_expanded_pai"):
        e_expanded_pai = kwargs["e_expanded_pai"]
    else:
        e_expanded_pai = expand_pai(e_pai, i_max_precision=i_max_precision).items()
    
    if b_merge_input:
        e_output = {k: v for k, v in e_input.items() if not pd.isnull(v)}
    else:
        e_output = {}
    c_precs = set(map(str, range(i_min_precision, i_max_precision + 1)))
    
    e_output.update({
        k: v
        for k, v in e_expanded_pai.items()
        if re.search(r"^-?\d+", k).group() in c_precs
    })
    
    if a_pas:
        e_output.update({
            k: round(v, 4) if k.startswith((
                'feature_',
                'location_',
                #'rent_control',
                #'timeshare',
                #'condemned',
                #'renovated',
                #'airbnb',
                #'homeaway',
                #'guesthouse',
                #'fixer_upper',
                #'property_condition',
            )) else v
            for k, v in a_pas[-1][1].items()
            if
                not k.startswith("property_type")
                and k not in e_input
                and f"feature_{k}" not in e_input
                and f"location_{k}" not in e_input
        })
        
        e_output["area_precision"] = a_pas[-1][1]["area_precision"]
        e_output["area_properties"] = a_pas[-1][1]["area_properties"] if e_output["area_precision"] > -1 else 0
        e_output["area_density"] = get_density(e_output["area_precision"], e_output["area_properties"], latitude=latitude)
        
        if pd.isnull(e_output.get("rent")):
            e_output["rent"] = a_pas[-1][1]["area_rent"]
        
        if pd.isnull(e_output.get("year_built")):
            e_output["year_built"] = max(1492, min(datetime.utcnow().year, a_pas[-1][1]["area_year_built"]))
    
    #a_occupancies = [
    #    e_output[f"{i}_area_occupancy"]
    #    for i in range(-3, 4 + 1)
    #    if e_output.get(f"{i}_area_occupancy")][-3:]
    a_occupancies = [
        e_area_stats[f"area_occupancy"]
        for t_area, e_area_stats in a_pas
        if e_area_stats.get(f"area_occupancy")]
    a_occupancies = a_occupancies[3:6] if len(a_occupancies) >= 4 else a_occupancies[-1:]
    #print(a_occupancies)
    e_output['occupancy'] = (sum(a_occupancies) / len(a_occupancies)) #** 0.65 #if a_occupancies else 0.26396
    
    if not e_output.get("bedrooms") and e_output.get("area_bedrooms"):
        e_output["bedrooms"] = round(e_output["area_bedrooms"])
    if not e_output.get("bathrooms") and e_output.get("area_bathrooms"):
        e_output["bathrooms"] = round(e_output["area_bathrooms"] * 2) / 2
    
    if b_include_availability:
        e_output['days'] = round(db_tables.e_CONFIG["predict_records"] * 30.4)
        e_output['available_days'] = round(db_tables.e_CONFIG["predict_records"] * 30.4 * e_output.get("availability", db_tables.e_CONFIG["predict_availability"]))
        e_output['blocked_days'] = e_output['days'] - e_output['available_days']
        e_output['reserved_days'] = min(round(e_output['days'] * e_output['occupancy']), e_output['available_days'])
        if e_output.get('area_reservation_length', 0) > 0:
            #e_output['reservations'] = round(e_output['reserved_days'] / ((e_output['area_reservation_length'] + e_output['area_reservation_length'] + 9) / 3))
            e_output['reservations'] = round(e_output['reserved_days'] / e_output['area_reservation_length'])
        else:
            e_output['reservations'] = round(e_output['reserved_days'] / 9)
    
    if b_include_seasonality:
        a_seasonalities = [
            (
                max(0, min(1, e_area_stats[f"area_demand_seasonality"])),
                e_area_stats[f"area_demand_season_length"],
                e_area_stats[f"area_demand_season_peak_month"],
                e_area_stats[f"area_demand_season_max_month"],
                e_area_stats[f"area_demand_season_type_Month"],
                e_area_stats[f"area_demand_season_type_Quarter"],
            )
            for t_area, e_area_stats in a_pas]
        #a_seasonalities = a_seasonalities[2:5] if len(a_seasonalities) >= 3 else a_seasonalities[-1:]
        a_seasonalities = a_seasonalities[-3:] #if len(a_seasonalities) >= 3 else a_seasonalities[-1:]
        
        e_output['demand_seasonality'] = sum([t[0] if not pd.isnull(t[0]) else 0 for t in a_seasonalities]) / len(a_seasonalities)
        
        e_output['demand_seasonality_normalized'] = max(0, min(1, 0.368 * math.log(e_output['demand_seasonality'] + 0.071) + 0.9771) if e_output['demand_seasonality'] > 0 else 0.0)
        
        e_output['demand_season_length'] = sum([t[1] if not pd.isnull(t[1]) else 12 for t in a_seasonalities]) / len(a_seasonalities)
        
        a_season_peak_months = [t[2] for t in a_seasonalities if not pd.isnull(t[2]) and t[2] > 0]
        e_output['demand_season_peak_month'] = round(np.mean(a_season_peak_months)) if a_season_peak_months else -1
        
        a_season_max_months = [t[3] for t in a_seasonalities[:-1] if not pd.isnull(t[3]) and t[3] > 0]
        e_output['demand_season_max_month'] = a_season_max_months[-1]
        
        e_output['demand_season_type_Month'] = round(sum([t[4] for t in a_seasonalities]) / len(a_seasonalities), 0)
        e_output['demand_season_type_Quarter'] = round(sum([t[5] for t in a_seasonalities]) / len(a_seasonalities), 0)
    
    
    #e_output['demand_season_type_Month'] = round([
    #    e_output[f"{i}_area_demand_season_type_Month"]
    #    for i in range(-3, 0 + 1)
    #    if e_output.get(f"{i}_area_demand_season_type_Month")][-1], 0)
    #
    #e_output['demand_season_type_Quarter'] = round([
    #    e_output[f"{i}_area_demand_season_type_Quarter"]
    #    for i in range(-3, 0 + 1)
    #    if e_output.get(f"{i}_area_demand_season_type_Quarter")][-1], 0)
    
    return e_output, e_pai


def guess_lot_size(e_row, **kwargs):
    if not e_STATE_INFO: load_data()
    
    s_st = e_row.get("state", "")
    e_st_info = e_STATE_INFO.get(us_states.e_STATE_ABBREV.get(s_st.lower(), s_st).upper(), {})
    
    e_factors = {}
    a_expected = [0.5]
    
    if e_st_info.get("lot_size"):
        e_factors["state"] = e_st_info["lot_size"]
        a_expected.append(e_factors["state"])
    
    if e_row.get("0_area_lot_size"):
        e_factors["area_acre"] = e_row["0_area_lot_size"]
        a_expected.append(min(25, e_factors["area_acre"]))
    
    d_cap = np.mean(a_expected) * 1.25
    if kwargs.get("b_debug"): print(d_cap, a_expected)
    
    if e_row.get("3_area_lot_size"):
        e_factors["acre"] = e_row["3_area_lot_size"]
    elif e_row.get("2_area_lot_size"):
        e_factors["acre"] = e_row["2_area_lot_size"]
    elif e_row.get("1_area_lot_size"):
        e_factors["acre"] = e_row["1_area_lot_size"]
    
    if e_row.get("adj_revenue", 0) > 0:
        if e_st_info.get("rev_per_acre"):
            e_factors["state_revenue"] = min(d_cap, e_row["adj_revenue"] / e_st_info["rev_per_acre"])
        if e_row.get("-1_area_rev_per_acre"):
            e_factors["area_revenue"] = min(d_cap * 1.5, e_row["adj_revenue"] / e_row["-1_area_rev_per_acre"])
        
        if e_row.get("3_area_rev_per_acre"):
            e_factors["revenue"] = min(d_cap * 2, e_row["adj_revenue"] / e_row["3_area_rev_per_acre"])
        elif e_row.get("2_area_rev_per_acre"):
            e_factors["revenue"] = min(d_cap * 2, e_row["adj_revenue"] / e_row["2_area_rev_per_acre"])
        elif e_row.get("1_area_rev_per_acre"):
            e_factors["revenue"] = min(d_cap * 2, e_row["adj_revenue"] / e_row["1_area_rev_per_acre"])
        elif e_row.get("0_area_rev_per_acre"):
            e_factors["revenue"] = min(d_cap * 1.5, e_row["adj_revenue"] / e_row["0_area_rev_per_acre"])
    
    if e_row.get("rate", 0) > 0:
        if e_st_info.get("rate_per_acre"):
            e_factors["state_rate"] = min(d_cap, e_row["rate"] / e_st_info["rate_per_acre"])
        if e_row.get("-1_area_rate_per_acre"):
            e_factors["area_rate"] = min(5, e_row["rate"] / e_row["-1_area_rate_per_acre"])
        
        if e_row.get("3_area_rate_per_acre"):
            e_factors["rate"] = min(d_cap * 2, e_row["rate"] / e_row["3_area_rate_per_acre"])
        elif e_row.get("2_area_rate_per_acre"):
            e_factors["rate"] = min(d_cap * 2, e_row["rate"] / e_row["2_area_rate_per_acre"])
        elif e_row.get("1_area_rate_per_acre"):
            e_factors["rate"] = min(d_cap * 2, e_row["rate"] / e_row["1_area_rate_per_acre"])
        elif e_row.get("0_area_rate_per_acre"):
            e_factors["rate"] = min(d_cap * 1.5, e_row["rate"] / e_row["0_area_rate_per_acre"])
    
    if e_row.get("rent", 0) > 0:
        if e_st_info.get("rent_per_acre"):
            e_factors["state_rent"] =  min(d_cap, e_row["rent"] / e_st_info["rent_per_acre"])
        if e_row.get("-1_area_rent_per_acre"):
            e_factors["area_rent"] =  min(d_cap * 1.5, e_row["rent"] / e_row["-1_area_rent_per_acre"])
            
        if e_row.get("3_area_rent_per_acre"):
            e_factors["rent"] = min(d_cap * 2, e_row["rent"] / e_row["3_area_rent_per_acre"])
        elif e_row.get("2_area_rent_per_acre"):
            e_factors["rent"] = min(d_cap * 2, e_row["rent"] / e_row["2_area_rent_per_acre"])
        elif e_row.get("1_area_rent_per_acre"):
            e_factors["rent"] = min(d_cap * 2, e_row["rent"] / e_row["1_area_rent_per_acre"])
        elif e_row.get("0_area_rent_per_acre"):
            e_factors["rent"] = min(d_cap * 1.5, e_row["rent"] / e_row["0_area_rent_per_acre"])
        
    if e_row.get("price", 0) > 0:
        if e_st_info.get("price_per_acre"):
            e_factors["state_price"] = min(d_cap, e_row["price"] / e_st_info["price_per_acre"])
        if e_row.get("-1_area_price_per_acre"):
            e_factors["area_price"] = min(d_cap * 1.5, e_row["price"] / e_row["-1_area_price_per_acre"])
        
        if e_row.get("3_area_price_per_acre"):
            e_factors["price"] = min(d_cap * 2, e_row["price"] / e_row["3_area_price_per_acre"])
        elif e_row.get("2_area_price_per_acre"):
            e_factors["price"] = min(d_cap * 2, e_row["price"] / e_row["2_area_price_per_acre"])
        elif e_row.get("1_area_price_per_acre"):
            e_factors["price"] = min(d_cap * 2, e_row["price"] / e_row["1_area_price_per_acre"])
        elif e_row.get("0_area_price_per_acre"):
            e_factors["price"] = min(d_cap * 2, e_row["price"] / e_row["0_area_price_per_acre"])
    
    if e_row.get("living_area", 0) > 0:
        if e_st_info.get("lot_size") and e_st_info.get("living_area"):
            e_factors["state_sqft"] = min(d_cap, min(5000, e_row["living_area"]) / (e_st_info["living_area"] / e_st_info["lot_size"]))
        if e_row.get("-1_area_living_area") and e_row.get("-1_area_lot_size"):
            e_factors["area_sqft"] = min(d_cap * 1.5, min(5000, e_row["living_area"]) / (e_row["-1_area_living_area"] / e_row["-1_area_lot_size"]))
        
        if e_row.get("3_area_living_area") and e_row.get("3_area_lot_size"):
            e_factors["sqft"] = min(d_cap * 2, min(5000, e_row["living_area"]) / (e_row["3_area_living_area"] / e_row["3_area_lot_size"]))
        elif e_row.get("2_area_living_area") and e_row.get("2_area_lot_size"):
            e_factors["sqft"] = min(d_cap * 2, min(5000, e_row["living_area"]) / (e_row["2_area_living_area"] / e_row["2_area_lot_size"]))
        elif e_row.get("1_area_living_area") and e_row.get("1_area_lot_size"):
            e_factors["sqft"] = min(d_cap * 2, min(5000, e_row["living_area"]) / (e_row["1_area_living_area"] / e_row["1_area_lot_size"]))
        elif e_row.get("0_area_living_area") and e_row.get("0_area_lot_size"):
            e_factors["sqft"] = min(d_cap * 2, min(5000, e_row["living_area"]) / (e_row["0_area_living_area"] / e_row["0_area_lot_size"]))
    
    if kwargs.get("b_debug"): print(e_factors)
    #d_estimated_lot_size = round(np.mean(list(e_factors.values())), 3) if e_factors else np.nan
    d_estimated_lot_size = round(np.mean([x for x in e_factors.values() if not pd.isnull(x)]), 3) if e_factors else np.nan
    return d_estimated_lot_size


def guess_living_area(e_row, **kwargs):
    if not e_STATE_INFO: load_data()
    s_st = e_row.get("state", "")
    e_st_info = e_STATE_INFO.get(us_states.e_STATE_ABBREV.get(s_st.lower(), s_st).upper(), {})
    
    e_factors = {}
    a_expected = [2000]
    
    if e_st_info.get("living_area"):
        e_factors["state"] = e_st_info["living_area"]
        a_expected.append(e_factors["state"])
    if e_row.get("0_area_living_area"):
        e_factors["area_sqft"] = e_row["0_area_living_area"]
        a_expected.append(min(10000, e_factors["area_sqft"]))
        
    d_cap = np.mean(a_expected) * 1.25
        
    if kwargs.get("b_debug"): print(d_cap, a_expected)
        
    if e_st_info.get("sqft_per_bedroom"):
        e_factors["bedrooms"] = e_row["bedrooms"] * e_st_info["sqft_per_bedroom"]
        e_factors["bedrooms1"] = e_row["bedrooms"] * e_st_info["sqft_per_bedroom"]
    if e_st_info.get("sqft_per_bathroom"):
        e_factors["bathrooms"] = e_row["bathrooms"] * e_st_info["sqft_per_bathroom"]
    
    if e_row.get("3_area_living_area"):
        e_factors["sqft"] = e_row["3_area_living_area"]
    elif e_row.get("2_area_living_area"):
        e_factors["sqft"] = e_row["2_area_living_area"]
    elif e_row.get("1_area_living_area"):
        e_factors["sqft"] = e_row["1_area_living_area"]
    #elif e_row.get("0_area_living_area"):
    #    e_factors["sqft"] = e_row["0_area_living_area"]
        
    if e_row.get("adj_revenue"):
        if e_st_info.get("rev_per_sqft"):
            e_factors["state_rev"] = min(d_cap, e_row["adj_revenue"] / e_st_info["rev_per_sqft"])
        if e_row.get("-1_area_rev_per_sqft"):
            e_factors["area_revenue"] = min(d_cap * 1.25, e_row["adj_revenue"] / e_row["-1_area_rev_per_sqft"])
            
        if e_row.get("3_area_rev_per_sqft"):
            e_factors["revenue"] = min(d_cap * 2, e_row["adj_revenue"] / e_row["3_area_rev_per_sqft"])
        elif e_row.get("2_area_rev_per_sqft"):
            e_factors["revenue"] = min(d_cap * 2, e_row["adj_revenue"] / e_row["2_area_rev_per_sqft"])
        elif e_row.get("1_area_rev_per_sqft"):
            e_factors["revenue"] = min(d_cap * 2, e_row["adj_revenue"] / e_row["1_area_rev_per_sqft"])
        elif e_row.get("0_area_rev_per_sqft"):
            e_factors["revenue"] = min(d_cap * 1.5, e_row["adj_revenue"] / e_row["0_area_rev_per_sqft"])
    
    if e_row.get("rate"):
        if e_st_info.get("rate_per_sqft"):
            e_factors["state_rate"] = min(d_cap, e_row["rate"] / e_st_info["rate_per_sqft"])
        if e_row.get("-1_area_rate_per_sqft"):
            e_factors["area_rate"] = min(d_cap * 1.25, e_row["rate"] / e_row["-1_area_rate_per_sqft"])
        
        if e_row.get("3_area_rate_per_sqft"):
            e_factors["rate"] = min(d_cap * 2, e_row["rate"] / e_row["3_area_rate_per_sqft"])
        elif e_row.get("2_area_rate_per_sqft"):
            e_factors["rate"] = min(d_cap * 2, e_row["rate"] / e_row["2_area_rate_per_sqft"])
        elif e_row.get("1_area_rate_per_sqft"):
            e_factors["rate"] = min(d_cap * 2, e_row["rate"] / e_row["1_area_rate_per_sqft"])
        elif e_row.get("0_area_rate_per_sqft"):
            e_factors["rate"] = min(d_cap * 1.5, e_row["rate"] / e_row["0_area_rate_per_sqft"])
    
    if not pd.isnull(e_row.get("rent")):
        if e_st_info.get("rent_per_sqft"):
            e_factors["state_rent"] = min(d_cap, e_row["rent"] / e_st_info["rent_per_sqft"])
        if e_row.get("-1_area_rent_per_sqft"):
            e_factors["area_rent"] = min(d_cap * 1.25, e_row["rent"] / e_row["-1_area_rent_per_sqft"])
        
        if e_row.get("3_area_rent_per_sqft"):
            e_factors["rent"] = min(d_cap * 1.5, e_row["rent"] / e_row["3_area_rent_per_sqft"])
        elif e_row.get("2_area_rent_per_sqft"):
            e_factors["rent"] = min(d_cap * 1.5, e_row["rent"] / e_row["2_area_rent_per_sqft"])
        elif e_row.get("1_area_rent_per_sqft"):
            e_factors["rent"] = min(d_cap * 1.5, e_row["rent"] / e_row["1_area_rent_per_sqft"])
        elif e_row.get("0_area_rent_per_sqft"):
            e_factors["rent"] = min(d_cap * 1.25, e_row["rent"] / e_row["0_area_rent_per_sqft"])
        
    if not pd.isnull(e_row.get("price")):
        if e_st_info.get("price_per_sqft"):
            e_factors["state_price"] = min(d_cap, e_row["price"] / e_st_info["price_per_sqft"])
        if e_row.get("-1_area_price_per_sqft"):
            e_factors["area_price"] = min(d_cap * 1.25, e_row["price"] / e_row["-1_area_price_per_sqft"])
        
        if e_row.get("3_area_price_per_sqft"):
            e_factors["price"] = min(d_cap * 1.5, e_row["price"] / e_row["3_area_price_per_sqft"])
        elif e_row.get("2_area_price_per_sqft"):
            e_factors["price"] = min(d_cap * 1.5, e_row["price"] / e_row["2_area_price_per_sqft"])
        elif e_row.get("1_area_price_per_sqft"):
            e_factors["price"] = min(d_cap * 1.5, e_row["price"] / e_row["1_area_price_per_sqft"])
        elif e_row.get("0_area_price_per_sqft"):
            e_factors["price"] = min(d_cap * 1.25, e_row["price"] / e_row["0_area_price_per_sqft"])
    
    if not pd.isnull(e_row.get("lot_size")):
        if e_st_info.get("lot_size") and e_st_info.get("living_area"):
            e_factors["state_acre"] = min(d_cap, min(1, e_row["lot_size"]) / (e_st_info["lot_size"] / e_st_info["living_area"]))
        if e_row.get("-1_area_living_area") and e_row.get("-1_area_lot_size"):
            e_factors["area_acre"] = min(d_cap * 1.25, min(1, e_row["lot_size"]) / (e_row["-1_area_lot_size"] / e_row["-1_area_living_area"]))
        
        if e_row.get("1_area_living_area") and e_row.get("1_area_lot_size"):
            e_factors["acre"] = min(d_cap * 1.5, min(1, e_row["lot_size"]) / (e_row["1_area_lot_size"] / e_row["1_area_living_area"]))
        elif e_row.get("0_area_living_area") and e_row.get("0_area_lot_size"):
            e_factors["acre"] = min(d_cap * 1.25, min(1, e_row["lot_size"]) / (e_row["0_area_lot_size"] / e_row["0_area_living_area"]))
    
    if kwargs.get("b_debug"): print(e_factors)
    #assert e_factors
    d_estimated_living_area = round(np.mean([x for x in e_factors.values() if not pd.isnull(x)]), 3) if e_factors else np.nan
    return d_estimated_living_area


def guess_price(e_row, **kwargs):
    if not e_STATE_INFO: load_data()
    e_st_info = e_STATE_INFO.get(us_states.e_STATE_ABBREV.get(e_row["state"].lower(), e_row["state"]).upper(), {})
    
    e_factors = {}
    a_expected = [500000]
    
    if e_st_info.get("price"):
        e_factors["state"] = e_st_info["price"]
        a_expected.append(e_factors["state"])
    if e_row.get("0_area_price"):
        e_factors["area_price"] = e_row["0_area_price"]
        a_expected.append(min(10000000, e_factors["area_price"]))
        
    d_cap = np.mean(a_expected) * 1.5
    if kwargs.get("b_debug"): print(d_cap, a_expected)
        
    if e_st_info.get("price_per_bedroom"):
        e_factors["bedrooms"] = e_row["bedrooms"] * e_st_info["price_per_bedroom"]
        e_factors["bedrooms1"] = e_row["bedrooms"] * e_st_info["price_per_bedroom"]
    if e_st_info.get("price_per_bathroom"):
        e_factors["bathrooms"] = e_row["bathrooms"] * e_st_info["price_per_bathroom"]
    
    if e_row.get("3_area_price"):
        e_factors["price"] = min(d_cap * 2, e_row["3_area_price"])
    elif e_row.get("2_area_price"):
        e_factors["price"] = min(d_cap * 2, e_row["2_area_price"])
    elif e_row.get("1_area_price"):
        e_factors["price"] = min(d_cap * 1.5, e_row["1_area_price"])
    #elif e_row.get("0_area_price"):
    #    e_factors["price"] = min(d_cap * 1.25, e_row["0_area_price"])
    #elif e_row.get("-1_area_price"):
    #    e_factors["price"] = min(d_cap * 1.25, e_row["-1_area_price"])
        
    if e_row.get("adj_revenue", 0) > 0:
        if e_st_info.get("rev_per_price"):
            e_factors["state_rev"] = min(d_cap, e_row["adj_revenue"] / e_st_info["rev_per_price"])
        if e_row.get("-1_area_rev_per_price"):
            e_factors["area_revenue"] = min(d_cap * 1.25, e_row["adj_revenue"] / e_row["-1_area_rev_per_price"])
            
        if e_row.get("3_area_rev_per_price"):
            e_factors["revenue"] = min(d_cap * 2, e_row["adj_revenue"] / e_row["3_area_rev_per_price"])
        elif e_row.get("2_area_rev_per_price"):
            e_factors["revenue"] = min(d_cap * 2, e_row["adj_revenue"] / e_row["2_area_rev_per_price"])
        elif e_row.get("1_area_rev_per_price"):
            e_factors["revenue"] = min(d_cap * 2, e_row["adj_revenue"] / e_row["1_area_rev_per_price"])
        elif e_row.get("0_area_rev_per_price"):
            e_factors["revenue"] = min(d_cap * 2, e_row["adj_revenue"] / e_row["0_area_rev_per_price"])
    
    if e_row.get("rate", 0) > 0:
        if e_st_info.get("rate_per_price"):
            e_factors["state_rate"] = min(d_cap, e_row["rate"] / e_st_info["rate_per_price"])
        if e_row.get("-1_area_rate_per_price"):
            e_factors["area_rate"] = min(d_cap * 1.25, e_row["rate"] / e_row["-1_area_rate_per_price"])
        
        if e_row.get("3_area_rate_per_price"):
            e_factors["rate"] = min(d_cap * 2, e_row["rate"] / e_row["3_area_rate_per_price"])
        elif e_row.get("2_area_rate_per_price"):
            e_factors["rate"] = min(d_cap * 2, e_row["rate"] / e_row["2_area_rate_per_price"])
        elif e_row.get("1_area_rate_per_price"):
            e_factors["rate"] = min(d_cap * 2, e_row["rate"] / e_row["1_area_rate_per_price"])
        elif e_row.get("0_area_rate_per_price"):
            e_factors["rate"] = min(d_cap * 2, e_row["rate"] / e_row["0_area_rate_per_price"])
    
    if e_row.get("rent", 0) > 0:
        if e_st_info.get("rent_per_price"):
            e_factors["state_rent"] = min(d_cap, e_row["rent"] / e_st_info["rent_per_price"])
        if e_row.get("-1_area_rent_per_price"):
            e_factors["area_rent"] = min(d_cap * 1.25, e_row["rent"] / e_row["-1_area_rent_per_price"])
        
        if e_row.get("3_area_rent_per_price"):
            e_factors["rent"] = min(d_cap * 1.5, e_row["rent"] / e_row["3_area_rent_per_price"])
        elif e_row.get("2_area_rent_per_price"):
            e_factors["rent"] = min(d_cap * 1.5, e_row["rent"] / e_row["2_area_rent_per_price"])
        elif e_row.get("1_area_rent_per_price"):
            e_factors["rent"] = min(d_cap * 1.5, e_row["rent"] / e_row["1_area_rent_per_price"])
        elif e_row.get("0_area_rent_per_price"):
            e_factors["rent"] = min(d_cap * 1.25, e_row["rent"] / e_row["0_area_rent_per_price"])
        
    if e_row.get("living_area", 0) > 0:
        if e_st_info.get("price_per_sqft"):
            e_factors["state_sqft"] = min(d_cap, e_row["living_area"] * e_st_info["price_per_sqft"])
        if e_row.get("-1_area_price_per_sqft"):
            e_factors["area_sqft"] = min(d_cap * 1.25, e_row["living_area"] * e_row["-1_area_price_per_sqft"])
        
        if e_row.get("3_area_price_per_sqft"):
            e_factors["sqft"] = min(d_cap * 1.5, e_row["living_area"] * e_row["3_area_price_per_sqft"])
        elif e_row.get("2_area_price_per_sqft"):
            e_factors["sqft"] = min(d_cap * 1.5, e_row["living_area"] * e_row["2_area_price_per_sqft"])
        elif e_row.get("1_area_price_per_sqft"):
            e_factors["sqft"] = min(d_cap * 1.5, e_row["living_area"] * e_row["1_area_price_per_sqft"])
        elif e_row.get("0_area_price_per_sqft"):
            e_factors["sqft"] = min(d_cap * 1.25, e_row["living_area"] * e_row["0_area_price_per_sqft"])
        
    if e_row.get("lot_size", 0) > 0:
        if e_st_info.get("price_per_acre"):
            e_factors["state_acre"] = min(d_cap, e_row["lot_size"] * e_st_info["price_per_acre"])
        if e_row.get("-1_area_price_per_acre"):
            e_factors["area_acre"] = min(d_cap * 1.25, e_row["lot_size"] * e_row["-1_area_price_per_acre"])
        
        if e_row.get("3_area_price_per_acre"):
            e_factors["acre"] = min(d_cap * 1.5, e_row["lot_size"] * e_row["3_area_price_per_acre"])
        elif e_row.get("2_area_price_per_acre"):
            e_factors["acre"] = min(d_cap * 1.5, e_row["lot_size"] * e_row["2_area_price_per_acre"])
        elif e_row.get("1_area_price_per_acre"):
            e_factors["acre"] = min(d_cap * 1.5, e_row["lot_size"] * e_row["1_area_price_per_acre"])
        elif e_row.get("0_area_price_per_acre"):
            e_factors["acre"] = min(d_cap * 1.25, e_row["lot_size"] * e_row["0_area_price_per_acre"])
    
    #print("guess_price factors", e_factors)
    #d_estimated_price = round(np.mean(list(e_factors.values())), 0) if e_factors else np.nan
    d_estimated_price = round(np.mean([x for x in e_factors.values() if not pd.isnull(x)]), 0) if e_factors else np.nan
    return d_estimated_price


def guess_rent(e_row, **kwargs):
    
    if not e_STATE_INFO: load_data()
    e_st_info = e_STATE_INFO.get(us_states.e_STATE_ABBREV.get(e_row["state"].lower(), e_row["state"]).upper(), {})
    
    e_factors = {}
    a_expected = [2000]
    
    if e_st_info.get("rent"):
        e_factors["state"] = e_st_info["rent"]
        a_expected.append(e_factors["state"])
    if e_row.get("0_area_rent"):
        e_factors["area_rent"] = e_row["0_area_rent"]
        a_expected.append(min(10000, e_factors["area_rent"]))
        
    d_cap = np.mean(a_expected) * 1.5
        
    if kwargs.get("b_debug"): print(d_cap, a_expected)
        
    if e_st_info.get("rent_per_bedroom"):
        e_factors["bedrooms"] = e_row["bedrooms"] * e_st_info["rent_per_bedroom"]
        e_factors["bedrooms1"] = e_row["bedrooms"] * e_st_info["rent_per_bedroom"]
    if e_st_info.get("rent_per_bathroom"):
        e_factors["bathrooms"] = e_row["bathrooms"] * e_st_info["rent_per_bathroom"]
    
    if e_row.get("3_area_rent"):
        e_factors["rent"] = min(d_cap * 2, e_row["3_area_rent"])
    elif e_row.get("2_area_rent"):
        e_factors["rent"] = min(d_cap * 2, e_row["2_area_rent"])
    elif e_row.get("1_area_rent"):
        e_factors["rent"] = min(d_cap * 1.5, e_row["1_area_rent"])
    #elif e_row.get("0_area_rent"):
    #    e_factors["rent"] = min(d_cap * 1.25, e_row["0_area_rent"])
    #elif e_row.get("-1_area_rent"):
    #    e_factors["rent"] = min(d_cap * 1.25, e_row["-1_area_rent"])
        
    if e_row.get("adj_revenue"):
        if e_st_info.get("rev_per_rent"):
            e_factors["state_rev"] = min(d_cap, e_row["adj_revenue"] / e_st_info["rev_per_rent"])
        if e_row.get("-1_area_rev_per_rent"):
            e_factors["area_revenue"] = min(d_cap * 1.25, e_row["adj_revenue"] / e_row["-1_area_rev_per_rent"])
            
        if e_row.get("3_area_rev_per_rent"):
            e_factors["revenue"] = min(d_cap * 2, e_row["adj_revenue"] / e_row["3_area_rev_per_rent"])
        elif e_row.get("2_area_rev_per_rent"):
            e_factors["revenue"] = min(d_cap * 2, e_row["adj_revenue"] / e_row["2_area_rev_per_rent"])
        elif e_row.get("1_area_rev_per_rent"):
            e_factors["revenue"] = min(d_cap * 2, e_row["adj_revenue"] / e_row["1_area_rev_per_rent"])
        elif e_row.get("0_area_rev_per_rent"):
            e_factors["revenue"] = min(d_cap * 1.25, e_row["adj_revenue"] / e_row["0_area_rev_per_rent"])
    
    if e_row.get("rate"):
        if e_st_info.get("rate_per_rent"):
            e_factors["state_rate"] = min(d_cap, e_row["rate"] / e_st_info["rate_per_rent"])
        if e_row.get("-1_area_rate_per_rent"):
            e_factors["area_rate"] = min(d_cap * 1.25, e_row["rate"] / e_row["-1_area_rate_per_rent"])
        
        if e_row.get("3_area_rate_per_rent"):
            e_factors["rate"] = min(d_cap * 2, e_row["rate"] / e_row["3_area_rate_per_rent"])
        elif e_row.get("2_area_rate_per_rent"):
            e_factors["rate"] = min(d_cap * 2, e_row["rate"] / e_row["2_area_rate_per_rent"])
        elif e_row.get("1_area_rate_per_rent"):
            e_factors["rate"] = min(d_cap * 2, e_row["rate"] / e_row["1_area_rate_per_rent"])
        if e_row.get("0_area_rate_per_rent"):
            e_factors["rate"] = min(d_cap * 1.25, e_row["rate"] / e_row["0_area_rate_per_rent"])
    
    if not pd.isnull(e_row.get("rent")):
        if e_st_info.get("rent_per_price"):
            e_factors["state_price"] = min(d_cap, e_row["price"] / e_st_info["rent_per_price"])
        if e_row.get("-1_area_rent_per_price"):
            e_factors["area_price"] = min(d_cap * 1.25, e_row["price"] / e_row["-1_area_rent_per_price"])
        
        if e_row.get("3_area_rent_per_price"):
            e_factors["price"] = min(d_cap * 1.5, e_row["price"] / e_row["3_area_rent_per_price"])
        elif e_row.get("2_area_rent_per_price"):
            e_factors["price"] = min(d_cap * 1.5, e_row["price"] / e_row["2_area_rent_per_price"])
        elif e_row.get("1_area_rent_per_price"):
            e_factors["price"] = min(d_cap * 1.5, e_row["price"] / e_row["1_area_rent_per_price"])
        elif e_row.get("0_area_rent_per_price"):
            e_factors["price"] = min(d_cap * 1.25, e_row["price"] / e_row["0_area_rent_per_price"])
        
    if not pd.isnull(e_row.get("living_area")):
        if e_st_info.get("rent_per_sqft"):
            e_factors["state_sqft"] = min(d_cap, e_row["living_area"] * e_st_info["rent_per_sqft"])
        if e_row.get("-1_area_rent_per_sqft"):
            e_factors["area_sqft"] = min(d_cap * 1.25, e_row["living_area"] * e_row["-1_area_rent_per_sqft"])
        
        if e_row.get("3_area_rent_per_sqft"):
            e_factors["sqft"] = min(d_cap * 1.5, e_row["living_area"] * e_row["3_area_rent_per_sqft"])
        elif e_row.get("2_area_rent_per_sqft"):
            e_factors["sqft"] = min(d_cap * 1.5, e_row["living_area"] * e_row["2_area_rent_per_sqft"])
        elif e_row.get("1_area_rent_per_sqft"):
            e_factors["sqft"] = min(d_cap * 1.5, e_row["living_area"] * e_row["1_area_rent_per_sqft"])
        elif e_row.get("0_area_rent_per_sqft"):
            e_factors["sqft"] = min(d_cap * 1.25, e_row["living_area"] * e_row["0_area_rent_per_sqft"])
        
    if not pd.isnull(e_row.get("lot_size")):
        if e_st_info.get("rent_per_acre"):
            e_factors["state_acre"] = min(d_cap, e_row["lot_size"] * e_st_info["rent_per_acre"])
        if e_row.get("-1_area_rent_per_acre"):
            e_factors["area_acre"] = min(d_cap * 1.25, e_row["lot_size"] * e_row["-1_area_rent_per_acre"])
        
        if e_row.get("3_area_rent_per_acre"):
            e_factors["acre"] = min(d_cap * 1.5, e_row["lot_size"] * e_row["3_area_rent_per_acre"])
        elif e_row.get("2_area_rent_per_acre"):
            e_factors["acre"] = min(d_cap * 1.5, e_row["lot_size"] * e_row["2_area_rent_per_acre"])
        elif e_row.get("1_area_rent_per_acre"):
            e_factors["acre"] = min(d_cap * 1.5, e_row["lot_size"] * e_row["1_area_rent_per_acre"])
        elif e_row.get("0_area_rent_per_acre"):
            e_factors["acre"] = min(d_cap * 1.25, e_row["lot_size"] * e_row["0_area_rent_per_acre"])
    
    if kwargs.get("b_debug"): print(e_factors)
    #d_estimated_rent = round(np.mean(list(e_factors.values())), 3) if e_factors else np.nan
    d_estimated_rent = round(np.mean([x for x in e_factors.values() if not pd.isnull(x)]), 3) if e_factors else np.nan
    return d_estimated_rent
