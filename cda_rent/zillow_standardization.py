﻿import re
from collections import Counter, namedtuple, defaultdict
from datetime import datetime, timedelta
from time import time
import dateutil
import pandas as pd
import numpy as np
#import FwValues

try:
    from . import airbnb_standardization, utils
    #from .airbnb_standardization import *
    from . import zillow_dicts
    from .zillow_dicts import *
except ImportError:
    import airbnb_standardization, utils
    #from airbnb_standardization import *
    import zillow_dicts
    from zillow_dicts import *


dt_TODAY = datetime.now()
d_REAL_ESTATE_INFLATION = 0.05

re_CLEAN_CONDITION = re.compile(r"[^a-z0-9%]+", flags=re.I)

#e_ZILLOW_PROP_TYPE_STD = {}
#for s_std, a_vals in e_ZILLOW_PROPERTY_TYPES.items():
#    for s_val in a_vals:
#        e_ZILLOW_PROP_TYPE_STD[s_val.lower()] = s_std


#c_PROPERTY_TYPE_ORDERS = set(airbnb_standardization.a_PROPERTY_TYPE_ORDER)
#print(c_PROPERTY_TYPE_ORDERS - set(e_ZILLOW_PROPERTY_TYPES.keys()))
assert not airbnb_standardization.c_PROPERTY_TYPE_ORDERS - set(e_ZILLOW_PROPERTY_TYPES.keys())


#def get_desc_prop_type(e_description):
#    for s_type in a_PROPERTY_TYPE_ORDER:
#        if e_description.get(s_type): return s_type
#    return None


e_ZILLOW_MAPS = {
    "Property ID": "zpid",
    "latitude": "latitude",
    "longitude": "longitude",
    "elevation": "elevation",
    #"address": {"address_address", "address/address", "address"},
    #"address_state": {"address_state", "address/state", "state"},
    #"address_zipcode": {"address_zipcode", "address/zipcode", "zipcode", "zip"},
    #"address_city": {"address_city", "address/city", "city"},
    "bedrooms": "bedrooms",
    "bathrooms": "bathrooms",
    "price": "price",
    "zestimate": "zestimate",
    "rent_estimate": ("rentZestimate", "rentzestimate"),
    "rent_control": {"features/hasRentControl", "has_rent_control"},
    "monthly_hoa_fee": ("monthly_hoa_fee", "hoa_monthly_fee"),
    #"actual_rent": "features/totalActualRent",
    "rent": ("features/totalActualRent", "totalActualRent", "total_actual_rent", "rent"),
    "last_rent": "last_rent",
    "avg_rent": "avg_rent",
    "tax_assessed_value": ("tax_assessed_value", "taxAssessedValue"),
    'property_tax_rate': ('property_tax_rate', 'propertyTaxRate'),
    "last_sold_price": "last_sold_price",
    "last_sold_date": ('last_sold_date', 'date_sold',),
    
    #"living_area": "features/livingArea",
    
    #"levels": "features/levels",
    "levels": ("features/stories", "stories"),
    "year_built": ("features/yearBuilt", "year"),
    "days_on_market": ("days_on_zillow", "days-on-zillow", "DaysOnMarket", "Days On Market", "DaysonMarket", "Days on Market"),
    
    "pets_allowed": {"features/hasPetsAllowed", "has_pets_allowed"},
    #"smoking_allowed": ,
    
    "feature_fencing": {"features/hasFencing", "fencing"},
    "feature_garage": {
        "features/hasGarage", "garage_parking_apacity", "garage_parking_capacity", "covered_parking_capacity", "has_garage",},
    "feature_free_parking": {
        "features/hasOpenParking",},
    "feature_fireplace": "features/hasFireplace",
    #"feature_private_entrance"
    "feature_patio_or_balcony": {
        "features/patioAndPorchFeatures",
        "features/patioAndPorchFeatures/0",
        "features/patioAndPorchFeatures/1",
        "features/patioAndPorchFeatures/2",
        "features/patioAndPorchFeatures/3",
        "features/patioAndPorchFeatures/4",},
   
    "feature_pool": {"features/hasPrivatePool", "has_private_pool"},
    "feature_jacuzzi": {"features/hasSpa", "has_spa",},
    "location_water_view": { #"location_waterfront"
        "features/hasWaterfrontView",
        'features/waterView',
        'features/waterViewYN',
        'features/waterfrontFeatures/1',
        'has_waterfront_view',
    },
    "location_mountain_view": {
        "features/hasView",
        "has_view",},
    
    "feature_heating": {"features/hasHeating", "has_heating", 'heating'},
    "feature_ac": {"features/hasCooling", "has_cooling", "cooling"},
    "feature_fireplace": {"fireplaces", "has_fireplace",},
    
    "appliances": {"appliances", "features/appliances"},
    
    "feature_washer": "features/appliances/Washer",
    "feature_dryer": "features/appliances/Electric Dryer",
    "feature_dishwasher": "features/appliances/Dishwasher",
    "feature_oven": {"features/appliances/Range/Oven", "features/appliances/Cooktop", "features/appliances/Range/RO Combo-Gas/Elec"},
    "feature_stove": {"features/appliances/Range/Oven", "features/appliances/Range/RO Combo-Gas/Elec"},
    "feature_filtered_water": "features/appliances/Water Softener",
    "feature_freezer": "features/appliances/Freezer",
    "feature_refrigerator": "features/appliances/Refrigerator",
    "feature_hot_water": {"features/appliances/Electric Water Heater", "features/appliances/Gas Water Heater",},
    "feature_microwave": "features/appliances/Microwave",
    "feature_dining": {"feature_dining", "feature_dining_checkbox_dining"},
    
    "feature_furnished": "furnished",
    "location_hoa": {"has_association", "has_hoa"},
    
    "feature_horses": {"can_raise_horses", "horse_amenities", "horse_YN",},
}

e_ROOM_TYPES = {
    "DiningRoom": "feature_dining_area",
    "LivingRoom": "feature_living_room",
    "Kitchen": "feature_kitchen",
    "Kitchenette": "feature_kitchenette",
    "Office": "feature_office",
}

e_COMBINE = {
    "feature_ceilings_vaulted_beamed": ["feature_ceilings_vaulted", "feature_ceilings_beamed"],
    "feature_counters_upgraded": ["feature_counters_wood", "feature_counters_corian"],
    #"": ["", "", "", "", ""],
    #"": ["", "", "", "", ""],
}
e_SUBSETS = {
    "feature_high_ceilings": ["feature_ceilings_tray", "feature_ceilings_vaulted_beamed", "feature_ceilings_vaulted", "feature_ceilings_beamed", "feature_ceilings_coffered"],
    "feature_closet": ["feature_closet_walkin", "feature_closet_cedar", "feature_closet_linens", "feature_closet_dual"],
    "feature_pantry": ["feature_pantry_walkin"],
    "feature_cabinets": ["feature_cabinets", "feature_cabinets_wood"],
    "feature_counters_upgraded": ["feature_counters_stone", "feature_counters_metal",],
    "feature_extra_room": ["feature_powder_room", "feature_workshop", "feature_sun_room", "feature_sitting_room", "feature_bonus_room", "feature_media_room", "feature_rec_room"],
    "feature_extra_wiring": ["feature_wired_data", "feature_wired_audio", "feature_wired_tv", "feature_smart_home", "feature_wired_alarm_system"],
    "feature_basement": ["feature_basement_finished"],
    "feature_parking": ["feature_garage"],
}

c_LOT_ONLY = {"LOT", "lot", "LOT_ONLY", "lot_only", "LOT ONLY", "lot only", "VacantLand", "vacantland"}
c_MANUFACTURED = {"MANUFACTURED", "manufactured", "MOBILE", "mobile", "Mobile Home", "MOBILE HOME", "mobile_home"}

re_AGES = re.compile(f"(((?P<first>(\d+|1o+))([- +]*y(ea)?rs?\.?)?[- ]+)?(?P<second>(\d+|1o+))([- +]*|[- +]*plus[- +]*)(y(ea)?rs?\.?|or more)|age[- :]*(?P<age>\d+))", flags=re.I)
re_YEARS = re.compile(f"((?P<first>(16|17|18|19|20)\d\d)[- ]+)?(?P<second>(16|17|18|19|20)\d\d)", flags=re.I)
def find_year_in_condition(s_condition):
    o_ages = re_AGES.search(s_condition)
    o_years = re_YEARS.search(s_condition)
    i_current_year = datetime.utcnow().year
    i_year = None
    if o_ages:
        s_first = o_ages.group("first")
        s_second = o_ages.group("second")
        s_age = o_ages.group("age")
        if s_first: s_first = s_first.replace("o", "0")
        if s_second: s_second = s_second.replace("o", "0")
        
        if s_age:
            i_year = i_current_year - int(s_age)
            #print(f'{i}, "{s}", {s_age} years, {i_year}')
        
        elif s_first and s_second:
            i_year = i_current_year - round(np.mean([int(s_first), int(s_second)]))
            #print(f'{i}, "{s}", {s_first}-{s_second} years, {i_year}')
            
        elif s_second:
            i_year = i_current_year - int(s_second)
            #print(f'{i}, "{s}", {s_second} years, {i_year}')
        else:
            pass
    elif o_years:
        s_first = o_years.group("first")
        s_second = o_years.group("second")
        if s_first and s_second:
            i_year = round(np.mean([int(s_first), int(s_second)]))
            #print(f'{i}, "{s}", {i_year}')
        elif s_second:
            i_year = int(s_second)
            #print(f'{i}, "{s}", {i_year}')
        else:
            pass
    #elif re.search(r"\d+(?![%\d])", s_condition):
    #    #print(f'\t{i}, "{s}"')
    #    pass
    return i_year


def split_features(v_features, b_split_on_comma=False):
    if not v_features or (isinstance(v_features, float) and pd.isnull(v_features)):
        return []

    if isinstance(v_features, str):
        if v_features[0] == "[" and v_features[-1] == "]":
            v_features = eval(v_features)
        else:
            v_features = [v_features]

    if b_split_on_comma:
        a_features = [
            val
            for x in v_features
            for val in x.split(",")
            if val]
    else:
        a_features = [
            x
            for x in v_features
            if x ]
    return a_features


def get_value_features(value, e_REs):
    if not value: return None
    c_features = set()
    #print("value", value)
    
    if isinstance(value, str):
        value = [value]
    
    for s_val in value:
        if not s_val: continue
        for s_feature, re_feature in e_REs.items():
            #print("s_feature", s_feature, re_feature)
            if re_feature.search(s_val):
                c_features.add(s_feature)
    return c_features


def convert_zillow_row(e_zillow_property, **kwargs):
    n_start = time()
    e_row = {k: v for k, v in e_zillow_property.items() if v is not None and (not isinstance(v, float) or not pd.isnull(v))}
    if "bedrooms" not in e_row: e_row["bedrooms"] = 0
    if "bathrooms" not in e_row: e_row["bathrooms"] = 0
    
    for i in range(0, 20):
        s_label = e_row.get(f"facts/{i}/factLabel")
        if s_label:
            e_row[f"fact/{s_label}"] = e_row.get(f"facts/{i}/factValue")
    
    for i in range(0, 20):
        s_val = e_row.get(f"features/appliances/{i}")
        if s_val:
            e_row[f"appliances/{s_val}"] = True

    for i in range(0, 5):
        s_val = e_row.get(f"features/otherStructures/{i}")
        if s_val and s_val not in {"See Remarks"}:
            e_row[f"otherStructures/{s_val}"] = True

    e_conv = {}
    
    for key, col in e_ZILLOW_MAPS.items():
        if isinstance(col, tuple):
            a_numbers = [e_row[x] for x in col if not pd.isnull(e_row.get(x))]
            if a_numbers:
                e_conv[key] = a_numbers[0]
        elif isinstance(col, list):
            e_conv[key] = all(e_row.get(x) for x in col)
        elif isinstance(col, set):
            e_conv[key] = any(e_row.get(x) for x in col)
        else:
            e_conv[key] = e_row.get(col)
    
    for i in range(0, 20):
        s_val = str(e_row.get(f"features/rooms/{i}/roomType"))
        #if s_val: print("roomtype", s_val, e_ROOM_TYPES.get(s_val))
        if s_val and s_val in e_ROOM_TYPES:
            e_conv[e_ROOM_TYPES[s_val]] = True
            
    for i in range(0, 10):
        s_val = str(e_row.get(f"features/parkingFeatures/{i}", ""))
        if s_val and re.search("garage", s_val, flags=re.I):
            e_conv["feature_garage"] = True
    if not e_conv.get("feature_garage") and e_row.get(f"parking_features"):
        s_val = str(e_row.get(f"parking_features", ""))
        if s_val and re.search("garage", s_val, flags=re.I):
            e_conv["feature_garage"] = True
    
    
    for i in range(0, 10):
        s_val = str(e_row.get(f"features/exteriorFeatures/{i}", ""))
        if s_val and re.search("(boat|dock|pier)", s_val, flags=re.I):
            e_conv["location_boating"] = True
    
    for i in range(0, 20):
        s_val = str(e_row.get(f"features/communityFeatures/{i}", ""))
        if s_val:
            if re.search("(gated($|community|subdivision|: *(yes|true))|security[- ]*gate)", s_val, flags=re.I):
                e_conv["feature_gated_community"] = True
            elif re.search("pool", s_val, flags=re.I):
                e_conv["location_pool"] = True
            elif re.search("boat", s_val, flags=re.I):
                e_conv["location_boating"] = True
            elif re.search("(rec(reation)?[- ]*center|club-?house)", s_val, flags=re.I):
                e_conv["location_recreation_center"] = True
            elif re.search("tennis", s_val, flags=re.I):
                e_conv["location_tennis"] = True
            elif re.search("fishing", s_val, flags=re.I):
                e_conv["location_fishing"] = True
                #'location_freshwater_fishing',
                #'location_pier_fishing',
                #'location_deepsea_fishing',
            elif re.search("(bbq|barbeque)", s_val, flags=re.I):
                e_conv["feature_bbq_area"] = True
            elif re.search("fitness[- ]*center", s_val, flags=re.I):
                e_conv["location_fitness_center"] = True
            elif re.search("(hiking|hike)", s_val, flags=re.I):
                e_conv["location_hiking"] = True
            elif re.search("horse trails", s_val, flags=re.I):
                e_conv["location_horseback_riding"] = True
            elif re.search("mountain", s_val, flags=re.I):
                e_conv["location_mountain_view"] = True
            elif re.search("(biking|cycling|bike trail|bicyle trail)", s_val, flags=re.I):
                e_conv["location_cycling"] = True
            elif re.search("water sports", s_val, flags=re.I):
                e_conv["location_water_view"] = True
                e_conv["location_swimming"] = True
                e_conv["location_water_tubing"] = True
                e_conv["location_water_skiing"] = True
            elif re.search("playground", s_val, flags=re.I):
                e_conv["location_playground"] = True
            elif re.search("lake", s_val, flags=re.I):
                e_conv["location_kayaking"] = True
            elif re.search("laundry", s_val, flags=re.I):
                e_conv["location_laundry"] = True
    
    for i in range(0, 2):
        s_val = str(e_row.get(f"features/property SubType/{i}", ""))
        if s_val:
            if s_val.lower() in {"unimproved land", "unimprovedland", "to be built", "tobebuilt", "vacantland", "vacant land"}:
                e_conv["lot_only"] = "Unimproved Land"
            elif s_val.lower() in {"manufactured home"}:
                e_conv["manufactured_home"] = "Manufactured Home"
    
    e_desc = airbnb_standardization.parse_description(e_row.get("description", ""))
    
    if not e_desc:
        e_conv["insufficient_description"] = 1
    elif len(e_desc) == 1:
        e_conv["insufficient_description"] = 0.75
    elif len(e_desc) == 2:
        e_conv["insufficient_description"] = 0.55
    elif len(e_desc) == 3:
        e_conv["insufficient_description"] = 0.40
    elif len(e_desc) == 4:
        e_conv["insufficient_description"] = 0.25
    elif len(e_desc) == 5:
        e_conv["insufficient_description"] = 0.10
    elif len(e_desc) == 6:
        e_conv["insufficient_description"] = 0.05
    
    #if e_conv.get("insufficient_description"):
    #    print('e_conv["insufficient_description"]', e_conv["insufficient_description"], e_desc)
    
    if e_row.get(f"address/state"):
        e_conv["state"] = e_row["address/state"]
    elif e_row.get(f"address_state"):
        e_conv["state"] = e_row["address_state"]
    elif e_row.get(f"state"):
        e_conv["state"] = e_row["state"]
    if e_row.get(f"address/city"):
        e_conv["city"] = e_row["address/city"]
    elif e_row.get(f"address_city"):
        e_conv["city"] = e_row["address_city"]
    elif e_row.get(f"city"):
        e_conv["city"] = e_row["city"]
    if e_row.get(f"address/zipcode"):
        e_conv["zipcode"] = e_row["address/zipcode"]
    elif e_row.get(f"address_zipcode"):
        e_conv["zipcode"] = e_row["address_zipcode"]
    elif e_row.get(f"zipcode"):
        e_conv["zipcode"] = e_row["zipcode"]
    elif e_row.get(f"zip"):
        e_conv["zipcode"] = e_row["zip"]
    
    if e_row.get(f"address/address"):
        e_conv["address"] = e_row["address/address"]
    elif e_row.get(f"address/street"):
        e_conv["address"] = e_row["address/street"]
    elif e_row.get(f"address_street"):
        e_conv["address"] = e_row["address_street"]
    elif e_row.get(f"street_address"):
        e_conv["address"] = e_row["street_address"]
    elif e_row.get(f"address"):
        e_conv["address"] = e_row["address"]
    elif e_row.get(f"street"):
        e_conv["address"] = e_row["street"]
        
    if e_conv.get("address"):
        e_address_tags = airbnb_standardization.parse_address(e_row["address"])
        e_conv.update(e_conv)
    
    s_lot_size = e_row.get(f"features/lotSize")
    if not s_lot_size: s_lot_size = e_row.get(f"lotSize")
    if not s_lot_size: s_lot_size = e_row.get(f"lotsize")
    if not s_lot_size: s_lot_size = e_row.get(f"lot_size")
    #e_conv["lot_size"] = s_lot_size
    
    s_living_area = e_row.get(f"features/livingArea")
    if not s_living_area: s_living_area = e_row.get(f"livingArea")
    if not s_living_area: s_living_area = e_row.get(f"living_area")
    if not s_living_area: s_living_area = e_row.get(f"area")
    #e_conv["living_area"] = s_living_area
    
    if any(
        x in c_LOT_ONLY if isinstance(x, str) else any(y in c_LOT_ONLY for y in x if isinstance(y, str))
        for x in [e_row.get("homeType"), e_row.get("features/homeType"), e_row.get("home_type"), e_row.get("architectural_style"), e_row.get("property_type")]
        if x
    ):
        e_conv["lot_only"] = True
        
    if any(
        x in c_MANUFACTURED if isinstance(x, str) else any(y in c_MANUFACTURED for y in x if isinstance(y, str))
        for x in [e_row.get("homeType"), e_row.get("features/homeType"), e_row.get("home_type"), e_row.get("architectural_style"), e_row.get("property_type")] if x):
        e_conv["manufactured_home"] = True
        
    if e_row.get("homeType"):
        e_conv["property_type"] = e_ZILLOW_PROP_TYPE_STD.get(str(e_row.get("homeType")).lower())
    elif e_row.get("home_type"):
        e_conv["property_type"] = e_ZILLOW_PROP_TYPE_STD.get(str(e_row.get("home_type")).lower())
    elif e_row.get("features/homeType"):
        e_conv["property_type"] = e_ZILLOW_PROP_TYPE_STD.get(str(e_row.get("features/homeType")).lower())
        
    
    if not e_conv.get("property_type") and e_row.get("features/structureType"):
        e_conv["property_type"] = e_ZILLOW_PROP_TYPE_STD.get(str(e_row.get("features/structureType")).lower())
    elif not e_conv.get("property_type") and e_row.get("structure_type"):
        e_conv["property_type"] = e_ZILLOW_PROP_TYPE_STD.get(str(e_row.get("structure_type")).lower())
    if not e_conv.get("property_type") and e_row.get("features/architecturalStyle"):
        e_conv["property_type"] = e_ZILLOW_PROP_TYPE_STD.get(str(e_row.get("features/architecturalStyle")).lower())
    elif not e_conv.get("property_type") and e_row.get("architectural_style"):
        e_conv["property_type"] = e_ZILLOW_PROP_TYPE_STD.get(str(e_row.get("architectural_style")).lower())
    if not e_conv.get("property_type") and e_row.get("property_type"):
        e_conv["property_type"] = e_ZILLOW_PROP_TYPE_STD.get(str(e_row.get("property_type")).lower())
    
    
    if pd.isnull(e_conv["property_type"]) or not e_conv["property_type"] or e_conv["property_type"] in {"House", "Apartment"}:
        s_desc_type = airbnb_standardization.get_desc_prop_type(e_desc)
        if s_desc_type: e_conv["property_type"] = s_desc_type
        
    if e_conv["property_type"] == "Rancher":
        e_conv["property_type"] = "House"
        e_conv["feature_single_level_home"] = True
    
    if not e_conv.get("location_waterfront"):
        s_front = str(e_row.get("frontageType")).lower()
        if s_front in c_WATERFRONTS:
            e_conv["location_waterfront"] = True
            
    #if e_conv["property_type"]:
    e_conv["property_category"] = airbnb_standardization.e_PROP_CAT_STD.get(str(e_conv["property_type"]).lower(), e_conv["property_type"])
    
    e_conv["manufactured_home"] = True if "manufactured" in str(e_row.get("fact/Type")).lower() else None
    
    if not e_conv.get("levels") and isinstance(e_row.get("features/levels"), str):
        e_conv["levels"] = e_LEVEL_STORIES.get(e_row["features/levels"], None)
    
    if str(e_row.get("home_status")).lower() in {"for_rent", "for-rent", "rent", "rental"} and e_row.get("price"):
        if pd.isnull(e_row.get("rent")) or e_row["rent"] <= 0:
            e_row["rent"] = e_row["price"]
        if pd.isnull(e_conv.get("rent")) or e_conv["rent"] <= 0:
            e_conv["rent"] = e_row["price"]
        e_row["price"] = None
        e_conv["price"] = None
    
    a_price_inputs = []
    if e_row.get("price") and e_row["price"] > 25000 and e_row.get("home_status") != "FOR_RENT":
        a_price_inputs.append((e_row["price"], 10))
    
        
    if e_row.get("taxAssessedValue") and e_row["taxAssessedValue"] > 25000:
        a_price_inputs.append((e_row["taxAssessedValue"], 4))
    elif e_row.get("tax_assessed_value") and e_row["tax_assessed_value"] > 25000:
        a_price_inputs.append((e_row["tax_assessed_value"], 4))
    if e_row.get("zestimate") and e_row["zestimate"] > 25000:
        a_price_inputs.append((e_row["zestimate"], 3))
    
    if e_row.get("last_sold_price") and e_row["last_sold_price"] > 25000:
        dt_sold = None
        if e_row.get("last_sold_date"):
            try:
                dt_sold = dateutil.parser.parse(e_row["last_sold_date"])
            except:
                pass
        if dt_sold is None and e_row.get("date_sold"):
            try:
                dt_sold = dateutil.parser.parse(e_row["date_sold"])
            except:
                pass
        if dt_sold is None and e_row.get("date-sold_unix", 0) > 0:
            try:
                dt_sold = datetime.fromtimestamp(e_row["date-sold_unix"] / 1000)
            except:
                pass
        if dt_sold is None:
            dt_sold = datetime(year=datetime.today().year - 20, month=1, day=1)
            
        i_age = dt_TODAY.year - dt_sold.year
        a_price_inputs.append((
            e_row["last_sold_price"] * ((1 + d_REAL_ESTATE_INFLATION) ** i_age),
            max(20 - min(19, i_age), 1)
        ))
    
    for i in range(10):
        if e_row.get(f"priceHistory/{i}/price") and e_row.get(f"priceHistory/{i}/event") in {"Sold", "Listed for sale"}:
            d_price = e_row[f"priceHistory/{i}/price"]
            dt_price = e_row.get("priceHistory/{i}/date")
            #print("dt_price", dt_price, i, d_price, e_row.get(f"priceHistory/{i}/event"))
            if dt_price is not None:
                if isinstance(dt_price, str):
                    dt_price = dateutil.parser.parse(dt_price)
                
                i_age = dt_TODAY.year - dt_price.year
                if dt_TODAY.year - dt_price.year > timedelta(days=365):
                    dt_price *= (1 + d_REAL_ESTATE_INFLATION) ** i_age
            else:
                i_age = 0
            
            a_price_inputs.append((d_price, 5 if i_age <=1 else  4 if i_age < 3 else 3 if i_age < 6 else 2 if i_age < 12 else 1))
    
    if isinstance(e_row.get("priceHistory"), str) and e_row["priceHistory"] and e_row["priceHistory"][0] == "[":
        a_history = eval(e_row["priceHistory"])
        a_buy_price_history = [
            e_price
            for e_price in a_history
            if e_price.get("price") and e_price["price"] > 0
            and e_price.get("postingIsRental") in {False, "False", "false", "false", "No", "no", "NO"}
            and e_price.get("event") != "Listing removed"
        ]
        a_sold_price_history = [
            e_price
            for e_price in a_buy_price_history
            if e_price.get("event") == "Sold"
            #and datetime.utcfromtimestamp(e_price["time"] / 1000) > datetime(year=2010, day=1, month=1)
            and dateutil.parser.parse(e_price["date"]).year >= datetime.today().year - 19
        ]
        if a_sold_price_history:
            for e_price in a_sold_price_history:
                dt_sold = dateutil.parser.parse(e_price["date"])
                
                a_price_inputs.append((
                    e_price["price"],
                    20 - min(19, (datetime.today().year - dt_sold.year))
                ))
        elif a_buy_price_history:
            for e_price in a_buy_price_history:
                a_price_inputs.append((
                    e_price["price"],
                    5 - min(4, (datetime.today().year - dt_sold.year))
                ))
    
    if a_price_inputs:
        try:
            a_prices, a_price_weights = zip(*a_price_inputs)
            d_price = sum([t[0] * t[1] for t in a_price_inputs]) / sum(a_price_weights)
            e_conv["calc_price"] = round(d_price, 2)
        except Exception as e:
            print(a_price_inputs)
            raise Exception(f'Problem with price inputs {e}')

    
    if e_row.get("features/specialListingConditions"):
        if re.search(r"auction", str(e_row[f"features/specialListingConditions"]), flags=re.I):
            e_conv["auction"] = True
    
    
    if kwargs.get("keep_description_text", True):
        for col, a_val in e_desc.items():
            if col in airbnb_standardization.c_PROPERTY_TYPE_ORDERS: continue
            
            a_val = [x for x in a_val if x]
            if a_val:
                e_conv[col] = ", ".join(a_val)
    else:
        for col, a_val in e_desc.items():
            if col in airbnb_standardization.c_PROPERTY_TYPE_ORDERS: continue
            if any(a_val):
                e_conv[col] = True
    
    v_lot_features = e_row.get("lot_features")
    #print("v_lot_features", v_lot_features, type(v_lot_features))
    if v_lot_features:
        #if isinstance(v_lot_features, str):
        #    a_lot_features = [x.strip() for x in v_lot_features.split(",")]
        #else:
        #    a_lot_features = [x.strip() for x in v_lot_features]
        
        a_lot_features = split_features(v_lot_features)
        c_lot_features = get_value_features(a_lot_features, e_REs=zillow_dicts.e_LOT_FEATURES_RE)
        #print("c_lot_features", c_lot_features)
        e_conv.update({
            feature: True for feature in c_lot_features})
            
    
    v_appliances = e_row.get("appliances")
    #print("v_lot_features", v_lot_features, type(v_lot_features))
    if v_lot_features:
        a_appliance_features = split_features(v_lot_features)
        c_appliance_features = get_value_features(a_appliance_features, e_REs=zillow_dicts.e_APPLIANCE_FEATURES_RE)
        e_conv.update({
            feature: True for feature in c_appliance_features})
    
    
    v_conditions = e_row.get("property_condition")
    #if v_conditions: print("v_conditions", v_conditions, type(v_conditions))
    if v_conditions:
        if isinstance(v_conditions, str) and re_CLEAN_CONDITION.sub(" ", v_conditions.lower()).strip() in zillow_dicts.c_CONDITION_TBB:
            e_conv["to_be_built"] = True
            e_conv["property_condition"] = 0
        #elif isinstance(v_conditions, (list, tuple)) and len(v_conditions) == 1 and re_CLEAN_CONDITION.sub(" ", v_conditions[0].lower()).strip() in zillow_dicts.c_CONDITION_TBB:
        #    e_conv["lot_only"] = True
        else:
            a_conditions = split_features(v_conditions, b_split_on_comma=True)
            #if a_conditions:
            #print("a_conditions", a_conditions)
            a_condition_levels = []
            a_condition_indicators = []
            
            if e_conv.get("new_construction"):
                a_condition_levels.append(1)
                a_condition_indicators.append("new_construction")
            
            if e_conv.get("rehabbed"):
                a_condition_levels.append(2)
                a_condition_indicators.append("rehabbed")
            
            for s_val in a_conditions:
                s_clean = re_CLEAN_CONDITION.sub(" ", s_val.lower()).strip()
                if s_clean in zillow_dicts.c_CONDITION_JUNK: continue
                if s_clean in zillow_dicts.c_CONDITION_TBB:
                    e_conv["to_be_built"] = True
                    e_conv["property_condition"] = 0
                    a_condition_indicators.append("tbb")
                    break
#                    continue
            
                i_year = find_year_in_condition(s_val)
                if i_year:
                    if not e_conv.get("year_built"):
                        e_conv["year_built"] = max(1492, min(datetime.utcnow().year, i_year))
                elif s_clean in zillow_dicts.e_FIXER:
                    #e_property_condition_details[s_val] += i_count
                    a_condition_levels.append(zillow_dicts.e_FIXER[s_clean])
                    a_condition_indicators.append(s_clean)

            if e_conv.get("year_built"):
                i_age = dt_TODAY.year - e_conv["year_built"]
                a_condition_indicators.append(f"age {i_age}")
                if i_age > 120:
                    a_condition_levels.append(8)
                elif i_age > 80:
                    a_condition_levels.append(7)
                elif i_age > 60:
                    a_condition_levels.append(6)
                elif i_age > 40:
                    a_condition_levels.append(5)
                elif i_age > 20:
                    a_condition_levels.append(4)
                elif i_age > 10:
                    a_condition_levels.append(3)
                elif i_age > 2:
                    a_condition_levels.append(2)
                else:
                    a_condition_levels.append(1)
            
            if a_condition_levels:
                e_conv["property_condition_levels"] = a_condition_levels
                #e_conv["property_condition"] = round((max(a_condition_levels) + np.mean(a_condition_levels) + np.median(a_condition_levels)) / 3)
                e_conv["property_condition"] = round((max(a_condition_levels) + np.mean(a_condition_levels)) / 2)
                e_conv["property_condition_indicators"] = a_condition_indicators
            
    v_interior_features = e_row.get("interior_features")
    if v_interior_features:
        a_interior_features = split_features(v_interior_features)
        c_interior_features = get_value_features(a_interior_features, e_REs=zillow_dicts.e_INTERIOR_FEATURES_RE)
        e_conv.update({
            feature: True for feature in c_interior_features})
    
    if "auction" not in e_conv:
        for i in range(0, 20):
            s_label = str(e_row.get(f"features/otherFacts/{i}/name"))
            if s_label and re.search(r"listing", s_label, flags=re.I) and e_row.get(f"features/otherFacts/{i}/value"):
                if re.search(r"auction", str(e_row[f"features/otherFacts/{i}/value"]), flags=re.I):
                    e_conv["auction"] = True
    
    if "auction" in str(e_row.get(f"broker_name", "")).lower():
        e_conv["auction"] = True
        
    elif "auction" not in e_conv and e_conv.get("price"):
        e_conv["auction"] = True if 0 < e_conv.get("price", 0) <= 1000 else False
    
    #e_conv["feature_ceilings_vaulted_beamed"] = any([e_conv.get(col, False) for col in ["feature_ceilings_vaulted", "feature_ceilings_beamed"]])
    #e_conv["feature_counters_upgraded"] = any([e_conv.get(col, False) for col in ["feature_counters_wood", "feature_counters_corian"]])
    
    for s_out, a_in in e_COMBINE.items():
        e_conv[s_out] = True if e_conv.get(s_out) or any([e_conv.get(col, False) for col in a_in]) else False
    
    for s_out, a_in in e_SUBSETS.items():
        e_conv[s_out] = True if e_conv.get(s_out) or any([e_conv.get(col, False) for col in a_in]) else False
    
    
    for col in [
        "address_zipcode", "address_zip_code", "address_postal_code", "address_post_code",
        "address/zipcode", "address/zip_code", "address/zip code", "address/postal_code", "address/post_code",
        "zipcode", "zip_code", "postal_code", "post_code",
    ]:
        if e_row.get(col):
            e_conv["address_zipcode"] = e_row.get(col)
    
    #if str(e_row.get("fact/Type")) not in {None, "Single Family Residence"}:
    #    print(str(e_row.get("fact/Type")))
    #for col in ["fixer_upper", "renovated"]:
    #    if e_desc.get(col):
    #        #print(e_desc[col])
    #        e_conv[col] = ", ".join(e_desc[col])
    #    else:
    #        e_conv[col] = None

    if e_conv.get("property_condition", 0) >= 9:
        e_conv["condemned"] = True
        e_conv["fixer_upper"] = True
    elif e_conv.get("condemned") and not e_conv.get("renovated") and not e_conv.get("to_be_built") and (not e_conv.get("property_condition") or e_conv.get("property_condition", 0) < 8):
        e_conv["property_condition"] = 9
    
    if 6 <= e_conv.get("property_condition", 0) <= 8:
        e_conv["fixer_upper"] = True
    elif e_conv.get("fixer_upper") and not e_conv.get("renovated") and not e_conv.get("to_be_built") and (not e_conv.get("property_condition") or e_conv.get("property_condition", 0) < 6):
        e_conv["property_condition"] = 7
    
    #print("s_living_area", s_living_area, type(s_living_area))
    #print("s_lot_size", s_lot_size, type(s_lot_size))
    
    d_living_area = airbnb_standardization.clean_living_area(s_living_area) if s_living_area else 0
    d_lot_size = airbnb_standardization.clean_lot_size(s_lot_size) if s_lot_size else 0
    d_price = utils.to_float(e_row.get("price", 0))
    
    if d_living_area > 0 or isinstance(e_conv.get("living_area"), str):
        e_conv["living_area"] = d_living_area
    if d_lot_size > 0 or isinstance(e_conv.get("lot_size"), str):
        e_conv["lot_size"] = d_lot_size
    
    #if e_conv.get("living_area", 0) > 0 and e_conv.get("lot_size", 0) * 43560 < e_conv["living_area"]:
    #print("living_area", e_conv.get("living_area"), type(e_conv.get("living_area")))
    #print("lot_size", e_conv.get("lot_size"), type(e_conv.get("lot_size")))
    if e_conv.get("living_area", 0) > 0 and e_conv.get("lot_size", 0) * 43560 < e_conv["living_area"]:
        e_conv["lot_size"] = round(e_conv["living_area"] / 43560, 8)
    
    if (
        0 < d_price < 200000
        and 0 < d_living_area < 2560
        and e_row.get("year_built", 1950) >= datetime.utcnow().year - 5
        and e_conv["property_type"] not in {'Tiny house'}
        and not e_conv.get("fixer_upper")
        and not e_conv.get("condemned")
        and not e_conv.get("auction")
        and not e_conv.get("shared_ownership")
        and not e_conv.get("timeshare")
        and not e_conv.get("lot_only")
        and not e_conv.get("condemned")
        and not e_conv.get("feature_basement_finished")
        and not e_conv.get("feature_basement")
        and (d_price / d_living_area) < 120
        and e_conv.get("stories")
    ):
        e_conv["manufactured_home"] = True
    
    #print(f"convert_zillow_row took: {utils.secondsToStr(time() - n_start)}")
    return e_conv


def convert_zillow(df_zillow):
    if df_zillow.index.name and df_zillow.index.name.lower() == "zpid":
        df_zillow = df_zillow.reset_index()
    
    a_converted_rows = []
    for idx, e_row in df_zillow.to_dict(orient="index").items():
        e_conv = convert_zillow_row(e_row)
        
        a_converted_rows.append(e_conv)
    
    df_converted_zillow = pd.DataFrame(a_converted_rows).set_index("Property ID")
    df_converted_zillow.rename(columns={"Property Type": "property_type"}, inplace=True)
    
    if "living_area" in df_converted_zillow.columns:
        df_converted_zillow["living_area"] = df_converted_zillow["living_area"].apply(airbnb_standardization.clean_living_area)
    if "lot_size" in df_converted_zillow.columns:
        df_converted_zillow["lot_size"] = df_converted_zillow["lot_size"].apply(airbnb_standardization.clean_lot_size)
    
    return df_converted_zillow
    
    
def topo_pieces(s_topo):
    s_clean = re.sub(r" *[-,/\\\(\)]+ *", "|", s_topo.lower())
    #a_pieces = [e_TOPO_REPLACE.get(x.strip(), x.strip()) for x in s_clean.split("|") if x not in c_TOPO_IGNORE]
    a_pieces = [x for x in [e_TOPO_REPLACE.get(x.strip(), None) for x in s_clean.split("|") if x not in c_TOPO_IGNORE] if x]
    return a_pieces

def standardize_topography(s_topo):
    if pd.isnull(s_topo) or not isinstance(s_topo, str) or not s_topo.strip(): return None
    a_pieces = topo_pieces(s_topo)
    if not a_pieces: return None
    a_pieces.sort(key=lambda x: e_TOPO_PRIORITY.get(x, -1))
    return a_pieces[0].replace(" ", "_") if a_pieces else None
