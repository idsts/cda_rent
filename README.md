# ### CDA Rent Predictor ### #

Predict potential rent for zillow properties if they used for full time AirBnB/Homeaway.

Example usage:

```
python predict_zillow.py "/path/to/input/zillow_file.csv" --output_file:"/path/to/output/prediction_file.csv"
```

```
#!python

from cda_rent import predict_zillow

df_output = predict_zillow.predict_zillow_file_revenue(s_input_zillow_file_path)
```