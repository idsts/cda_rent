﻿'''
Test cases for FwFuzzy/Fuzzy
'''
from __future__ import absolute_import
import os
from cda_rent.process_api_requests import process_one_input

s_TEST_FILE = os.path.join(os.path.dirname(os.path.abspath(__file__)), "test.csv")

class TestPredict():
    ''' Test library for medium length string fuzzy match '''
    def test_cour_de_alene(self):
        print(s_TEST_FILE)
        df_output = process_one_input(s_TEST_FILE)
        
